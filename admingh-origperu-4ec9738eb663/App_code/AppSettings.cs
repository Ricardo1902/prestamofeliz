﻿using System;
using System.Configuration;

public static class AppSettings
{
    static AppSettings()
    {

    }

    public static string VersionPub
    {
        get
        {
            return DateTime.Now.ToString("yy.MM.dd");
        }
    }

    /// <summary>
    /// Obtiene el valor de las llaves DirectorioLog que se encuentran el archivo de configuración, separados por comas.
    /// </summary>
    public static string DirectorioLog
    {
        get
        {
            string direcorioLog = @"C:\Logs";
            try
            {
                string temp = ConfigurationManager.AppSettings.Get("DirectorioLog");
                if (temp != null) direcorioLog = temp;
            }
            catch (Exception)
            {
            }
            return direcorioLog;
        }
    }
}