﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for GoogleMapsAPI
/// </summary>
public class GoogleMapsAPI
{
    static string apiKey = "AIzaSyASUxxHlv5SmW_-o3yPI5ZpxKomtVKwEQM"; //Configurar - Api Key del proyecto

    public GoogleMapsAPI()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// Metodo utilizado para obtener la ubicacion (latitud y longitud) a partir de la direccion.
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    public static MAGResponse GetLocation(string address)
    {
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" + apiKey);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            ResponseMAG responseApi;
            using (Stream dataStream = request.GetRequestStream())
            {
                using (WebResponse wResponse = request.GetResponse())
                {
                    using (Stream dataStreamResponse = wResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            String response = tReader.ReadToEnd();
                            responseApi = (ResponseMAG)js.Deserialize(response, typeof(ResponseMAG));
                        }
                    }
                }
            }

            if (responseApi.status == "OK")
            {
                return new MAGResponse
                {
                    success = true,
                    message = "Location calculated",
                    location = responseApi.results[0].geometry.location
                };
            }
            else
                throw new Exception(responseApi.status);
        }
        catch (Exception e)
        {
            return new MAGResponse
            {
                success = false,
                message = e.Message
            };
        }
    }

    /// <summary>
    /// Metodo utilizado para obtener la direccion a partir de la latitud y longitud.
    /// Tambien como resultado devuelve la direccion en partes.
    /// </summary>
    /// <param name="lat"></param>
    /// <param name="lng"></param>
    /// <returns></returns>
    public static object GetAddress(decimal lat, decimal lng)
    {
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=" + apiKey);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            ResponseMARG responseApi;
            using (Stream dataStream = request.GetRequestStream())
            {
                using (WebResponse wResponse = request.GetResponse())
                {
                    using (Stream dataStreamResponse = wResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            String response = tReader.ReadToEnd();
                            responseApi = (ResponseMARG)js.Deserialize(response, typeof(ResponseMARG));
                        }
                    }
                }
            }

            return new MARGResponse
            {
                success = 1,
                message = "Address calculated",
                address_components = responseApi.results[0].address_components,
                address = responseApi.results[0].formatted_address
            };
        }
        catch (Exception e)
        {
            return new MARGResponse
            {
                success = 0,
                message = e.Message
            };
        }
    }

    public static object GetOptimizedRoute(ResponseMALocation origin, ResponseMALocation[] locations)
    {
        try
        {
            string waypoints = "optimize:true";

            foreach (ResponseMALocation location in locations)
            {
                waypoints += "|" + location.lat + "," + location.lng;
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://maps.googleapis.com/maps/api/directions/json?origin=" + origin.lat + "," + origin.lng + "&destination=" + (origin.lat - 0.000001) + "," + origin.lng + "&waypoints=" + waypoints + "&key=" + apiKey);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            ResponseMAWP responseApi;
            using (Stream dataStream = request.GetRequestStream())
            {
                using (WebResponse wResponse = request.GetResponse())
                {
                    using (Stream dataStreamResponse = wResponse.GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            String response = tReader.ReadToEnd();
                            responseApi = (ResponseMAWP)js.Deserialize(response, typeof(ResponseMAWP));
                        }
                    }
                }
            }

            ResponseMALocation[] locations_in_order = new ResponseMALocation[locations.Count()];
            int position = 0;
            foreach (var i in responseApi.routes[0].waypoint_order)
            {
                locations_in_order[position] = locations[i];
                position += 1;
            }

            return new MAWPResponse
            {
                success = 1,
                message = "Address calculated",
                locations_information = responseApi.routes[0].legs,
                locations_in_order = locations_in_order,
                locations_order = responseApi.routes[0].waypoint_order
            };
        }
        catch (Exception e)
        {
            return new MAWPResponse
            {
                success = 0,
                message = e.Message
            };
        }
    }
}

public class ResponseMALocation
{
    public double lat { get; set; }
    public double lng { get; set; }
}

public class ResponseMAGResults
{
    public ResponseMAGGeometry geometry { get; set; }
}

public class ResponseMAGGeometry
{
    public ResponseMALocation location { get; set; }
}

public class ResponseMAG
{
    public ResponseMAGResults[] results { get; set; }
    public string status { get; set; }
}

public class MAGResponse
{
    public bool success { get; set; }
    public string message { get; set; }
    public ResponseMALocation location { get; set; }

    public MAGResponse()
    {
        this.location = new ResponseMALocation { lat = 0, lng = 0 };
    }
}

public class ResponseMARG
{
    public ResponseMARGResults[] results { get; set; }
    public string status { get; set; }
}

public class ResponseMARGResults
{
    public ResponseMARGAddressComponents[] address_components { get; set; }
    public string formatted_address { get; set; }
}

public class ResponseMARGAddressComponents
{
    public string long_name { get; set; }
    public string short_name { get; set; }
    public string[] types { get; set; }
}

public class MARGResponse
{
    public int success { get; set; }
    public string message { get; set; }
    public ResponseMARGAddressComponents[] address_components { get; set; }
    public string address { get; set; }

    public MARGResponse()
    {
        this.address = "";
    }
}

public class ResponseMAWP
{
    public ResponseMAWPRoutes[] routes { get; set; }
}

public class ResponseMAWPRoutes
{
    public ResponseMAWPLegs[] legs { get; set; }
    public int[] waypoint_order { get; set; }
}

public class ResponseMAWPLegs
{
    public ResponseMAWPLegValue distance { get; set; }
    public ResponseMAWPLegValue duration { get; set; }
    public ResponseMALocation start_location { get; set; }
    public ResponseMALocation end_location { get; set; }
}

public class ResponseMAWPLegValue
{
    public int value { get; set; }
}

public class MAWPResponse
{
    public int success { get; set; }
    public string message { get; set; }
    public ResponseMAWPLegs[] locations_information { get; set; }
    public int[] locations_order { get; set; }
    public ResponseMALocation[] locations_in_order { get; set; }
}