﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Xml;
using System.Data.SqlTypes;



/// <summary>
/// Summary description for cls_Base_de_datos
/// </summary>
public class cls_Base_de_datos
{
    public cls_Base_de_datos()
    {
        //
        // TODO: Add constructor logic here
        //
        
    }

    /// <summary>
    /// ADO.NET data access using the SQL Server Managed Provider.
    /// </summary>
    public class cls_Coneccion : IDisposable
    {
        // connection to data source
        private SqlConnection con;
        private SqlCommand command;
        private SqlCommand Valida;
        private SqlCommand BuscaObjetoId;
        public int result_number = -1;
        public string result_msg = "";
        /// <summary>
        /// Open the connection.
        /// </summary>
        public void Open()
        {
            string strKey = ConfigurationManager.AppSettings["CADENACONEXIONSQL"].ToString();
            var conString = ConfigurationManager.ConnectionStrings[strKey];
            string strConnString = conString.ConnectionString;

            // open connection
            if (con == null)
            {
                con = new SqlConnection(strConnString);

                con.Open();
                con.InfoMessage += new SqlInfoMessageEventHandler(SqlInfoMessage);
            }
        }


        private void SqlInfoMessage(object sender, SqlInfoMessageEventArgs e)
        {					
            string[] strMensaje;
            strMensaje = e.Message.Split('§');

						if (e.Errors[0].Number == 8153)
						{
							result_number = -1;
							result_msg = "";
							return;
						}

            if (strMensaje.Length == 1)
            {
                result_number = 0;
                result_msg = strMensaje[0];
            }
            else
            {
                result_number = Convert.ToInt32(strMensaje[0]);
                result_msg = strMensaje[1];
            }
        }
        /// <summary>
        /// Close the connection.
        /// </summary>
        public void Close()
        {
            if (con != null)
                con.Close();
        }


        static void connection_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            // this gets the print statements (maybe the error statements?)
            var outputFromStoredProcedure = e.Message;
            var outputFromStoredProcedureE = e.Source;
        }
        /// <summary>
        /// Release resources.
        /// </summary>
        public void Dispose()
        {
            // make sure connection is closed
            if (con != null)
            {
                con.Dispose();
                con = null;
            }
        }

        /// <summary>
        /// Asigna Comando a ejecutar
        /// </summary>
        /// <param name="Comando"></param>
        public void SetCommand(string Comando)
        {
            if (command == null)
            {
                this.Open();
                command = new SqlCommand(Comando, con);
                command.CommandTimeout = 36000;
            }
            else
            {
                try
                { command.Cancel(); }
                catch (Exception)
                { }
                try
                { command.Parameters.Clear(); }
                catch (Exception)
                { }
            }
            command.CommandText = Comando;
            command.CommandType = CommandType.StoredProcedure;
        }


        #region Parametros
        /// <summary>
        /// Crea Parametro de Salida de tipo Caracter
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="strParameter"></param>
        /// <param name="nLenght"></param>
        /// <returns></returns>
        public SqlParameter CreateOutputParameter(string strParameterName, string strParameter, int nLenght)
        {
            SqlParameter parameter = new SqlParameter(strParameterName, System.Data.SqlDbType.NVarChar, nLenght);
            parameter.Value = strParameter;
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Crea Parametro de Salida de tipo Entero
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="nParameter"></param>
        /// <returns></returns>
        public SqlParameter CreateOutputParameter(string strParameterName, int nParameter)
        {
            SqlParameter parameter = new SqlParameter(strParameterName, System.Data.SqlDbType.Int, 4);
            parameter.Value = nParameter;
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Crea Parametro de Salida de tipo Lógico
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="bParameter"></param>
        /// <returns></returns>
        public SqlParameter CreateOutputParameter(string strParameterName, bool bParameter)
        {
            SqlParameter parameter = new SqlParameter(strParameterName, System.Data.SqlDbType.Bit, 1);
            parameter.Value = bParameter;
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Crea Valor de Retorno de tipo Entero
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <returns></returns>
        public SqlParameter CreateReturnValueParameter(string strParameterName)
        {
            SqlParameter parameter = new SqlParameter(strParameterName, System.Data.SqlDbType.Int);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Crea Parametro de entrada de tipo Entero
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="nParameter"></param>
        public void CreateParameter(string strParameterName, int nParameter)
        {
            command.Parameters.Add(strParameterName, System.Data.SqlDbType.Int, 4).Value = nParameter;
        }

        /// <summary>
        /// Crea Parametro de entrada de tipo Entero largo
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="nParameter"></param>
        public void CreateParameter(string strParameterName, double nParameter)
        {
            command.Parameters.Add(strParameterName, System.Data.SqlDbType.Money, 8).Value = nParameter;
        }

        /// <summary>
        /// Crea Parametro de entrada de tipo Caracter
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="strParameter"></param>
        /// <param name="nLenght"></param>
        public void CreateParameter(string strParameterName, string strParameter, int nLenght)
        {
            //Si la longitud es Cero ponerlo como Text
            if (nLenght == 0)
            {
                command.Parameters.Add(strParameterName, System.Data.SqlDbType.NText).Value = strParameter;
            }
            else
            {
                command.Parameters.Add(strParameterName, System.Data.SqlDbType.NVarChar, nLenght).Value = strParameter;
            }
        }

        /// <summary>
        /// Crea Parametro de entrada de tipo Fecha
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="dateTimeParameter"></param>
        public void CreateParameter(string strParameterName, DateTime dateTimeParameter)
        {
            command.Parameters.Add(strParameterName, System.Data.SqlDbType.DateTime, 8).Value = dateTimeParameter;
        }

        /// <summary>
        /// Crea Parametro de entrada de tipo Flotante
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="fParameter"></param>
        public void CreateParameter(string strParameterName, float fParameter)
        {
            command.Parameters.Add(strParameterName, System.Data.SqlDbType.Float, 8).Value = fParameter;
        }

        /// <summary>
        /// Crea Parametro de entrada de tipo Lógico
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="bParameter"></param>
        public void CreateParameter(string strParameterName, bool bParameter)
        {
            command.Parameters.Add(strParameterName, System.Data.SqlDbType.Bit, 1).Value = bParameter;
        }

        /// <summary>
        /// Crea Parametro de entrada de tipo Imagen
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="imgParameter"></param>
        public void CreateParameter(string strParameterName, byte[] imgParameter)
        {
            command.Parameters.Add(strParameterName, SqlDbType.Image).Value = imgParameter;
        }

        /// <summary>
        /// Parametro XML
        /// </summary>
        /// <param name="strParameterName"></param>
        /// <param name="nParameter"></param>
        public void CreateParameter(string strParameterName, XmlDocument nParameter)
        {
            command.Parameters.Add(strParameterName, System.Data.SqlDbType.Xml).Value = new SqlXml(new XmlTextReader(nParameter.InnerXml, XmlNodeType.Document, null));

        }
        /// <summary>
        /// Parametros default esta desactivado
        /// </summary>
        public void AddParametersSys()
        {
            //string pool = HttpContext.Current.Application.ToString();
            //string y = string.Empty, ipAddress = string.Empty;
            //int i, sistemasId = 0, usuarioId = 0, objetoId = 0;

            //try
            //{
            //    Valida = new SqlCommand("exec [Auditoria].Log_ValidaParametros '" + command.CommandText.ToString() + "'", con);
            //    Valida.CommandTimeout = 36000;

            //    i = Convert.ToInt32(Valida.ExecuteScalar());
            //    try
            //    {
            //        y = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.ToString();
            //        if (HttpContext.Current.Session["SistemaId"] != null)
            //            int.TryParse(HttpContext.Current.Session["SistemaId"].ToString(), out sistemasId);
            //        if (HttpContext.Current.Session["Usuarioid"] != null)
            //            int.TryParse(HttpContext.Current.Session["Usuarioid"].ToString(), out usuarioId);
            //        if (HttpContext.Current.Session["IPAddress"] != null)
            //            ipAddress = HttpContext.Current.Session["IPAddress"].ToString();
            //    }
            //    catch { }

            //    BuscaObjetoId = new SqlCommand("exec [Auditoria].Log_ObjetoId " + sistemasId.ToString() + ",'" + y.ToString() + "'", con);
            //    BuscaObjetoId.CommandTimeout = 36000;
            //    objetoId = Convert.ToInt32(BuscaObjetoId.ExecuteScalar());


            //    if (i == 1)
            //    {
            //        command.Parameters.Add("@Sy_UsuarioId", System.Data.SqlDbType.Int, 0).Value = usuarioId;
            //        command.Parameters.Add("@Sy_IP", System.Data.SqlDbType.VarChar, 15).Value = ipAddress;
            //        command.Parameters.Add("@Sy_ObjetoId", System.Data.SqlDbType.Int, 0).Value = objetoId;
            //    }
            //    if (i == 2)
            //    {
            //        if (usuarioId > 0)
            //        {
            //            command.Parameters.Add("@Sy_UsuarioId", System.Data.SqlDbType.Int, 4).Value = usuarioId;
            //        }

            //    }
            //}
            //catch { /*Implementar log de Error*/ }
        }
        #endregion


        #region Resultados
        /// <summary>
        /// Ejecuta comando
        /// </summary>
        public void ExecuteNonQuery()
        {

            if (con.State == ConnectionState.Closed)
            { con.Open(); }
            con.InfoMessage += new SqlInfoMessageEventHandler(SqlInfoMessage);
            AddParametersSys();
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Ejecuta Escalar
        /// </summary>
        public object getScalar()
        {


            if (con.State == ConnectionState.Closed)
            { con.Open(); }
            con.InfoMessage += new SqlInfoMessageEventHandler(SqlInfoMessage);

            AddParametersSys();
            return command.ExecuteScalar();
        }

        /// <summary>
        /// Run stored procedure.
        /// </summary>
        /// <param name="sql">Name of stored procedure.</param>
        /// <returns>Stored procedure return value.</returns>
        public bool ExecuteNonQuery(string sql)
        {
            this.Open();
            AddParametersSys();
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandTimeout = 36000;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            this.Close();
            return true;
        }

        

        
        /// <summary>
        /// Run stored procedure.
        /// </summary>
        /// <param name="sql">Name of stored procedure.</param>
        /// <param name="DataTable">Return result of procedure.</param>
        public DataTable getDataTable(string sql)
        {
            this.Open();

            AddParametersSys();
            SqlDataAdapter da = new SqlDataAdapter(sql, con);
            da.SelectCommand.CommandTimeout = 3600;
            DataTable dt = new DataTable();
            int rows = da.Fill(dt);
            dt.Dispose();
            this.Close();
            return dt;
        }

        /// <summary>
        /// Run stored procedure.
        /// </summary>
        /// <param name="sql">Name of stored procedure.</param>
        /// <param name="DataSet">Return result of procedure.</param>
        public DataSet getDataSet(string sql)
        {
            this.Open();
            AddParametersSys();
            SqlDataAdapter da = new SqlDataAdapter(sql, con);
            da.SelectCommand.CommandTimeout = 3600;
            DataSet ds = new DataSet();
            da.Fill(ds);
            ds.Dispose();
            this.Close();
            return ds;

        }

        /// <summary>
        /// Regresa DataSet
        /// </summary>
        /// <param name="DataSet">Return result of procedure.</param>
        public DataSet getDataSet()
        {
            DataSet data = new DataSet();
            try
            {

                if (con.State == ConnectionState.Closed)
                { con.Open(); }
                con.InfoMessage += new SqlInfoMessageEventHandler(SqlInfoMessage);

                AddParametersSys();
                SqlDataAdapter dataAdapterSearch = new SqlDataAdapter();

                dataAdapterSearch.SelectCommand = command;
                dataAdapterSearch.Fill(data);
                dataAdapterSearch.Dispose();


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                con.Close();
            }

            return data;


        }

        /// <summary>
        /// Run stored procedure.
        /// </summary>
        /// <param name="sql">Name of stored procedure.</param>
        /// <param name="dataReader">Return result of procedure.</param>
        public SqlDataReader getDataReader(string sql)
        {
            this.Open();
            AddParametersSys();
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandTimeout = 3600;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(); //System.Data.CommandBehavior.CloseConnection);
            cmd.Dispose();
            this.Close();

            return dr;
        }
        /// <summary>
        /// Run stored procedure.
        /// </summary>
        /// <param name="sql">Name of stored procedure.</param>
        /// <param name="dataReader">Return result of procedure.</param>
        public SqlDataReader getDataReader(string sql, int to)
        {
            this.Open();
            AddParametersSys();
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandTimeout = to;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(); //System.Data.CommandBehavior.CloseConnection);
            cmd.Dispose();
            //this.Close();

            return dr;
        }
        /// <summary>
        /// Regresa Reader
        /// </summary>
        /// <param name="dataReader">Return result of procedure.</param>
        public SqlDataReader getDataReader()
        {

            if (con.State == ConnectionState.Closed)
            { con.Open(); }
            con.InfoMessage += new SqlInfoMessageEventHandler(SqlInfoMessage);
            AddParametersSys();
            con.Close();
            return command.ExecuteReader();
        }

        #endregion





    }

}
