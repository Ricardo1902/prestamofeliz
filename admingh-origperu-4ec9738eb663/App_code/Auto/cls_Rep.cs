﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Collections;

/// <summary>
/// Summary description for cls_Rep
/// </summary>
public class cls_Rep
{
    private cls_Base_de_datos.cls_Coneccion conBD = new cls_Base_de_datos.cls_Coneccion();
    public cls_Rep()
    {
    }

    public int result_number;
    public string result_msg;

    public DataSet Rep_Catalagos(int Usuarioid, int Companiaid, string Tipo)
    {
        conBD.SetCommand("Rep_Catalagos");

        conBD.CreateParameter("@Usuarioid", Usuarioid);

        conBD.CreateParameter("@Companiaid", Companiaid);
        conBD.CreateParameter("@Tipo", Tipo, Tipo.Length);


        return conBD.getDataSet();

    }


    public DataSet Rep_ArchivoXML(int Usuario, int ReporteId, string SP)
    {
        conBD.SetCommand("Rep_EnXmlRuta");

        conBD.CreateParameter("@Usuario", Usuario);

        conBD.CreateParameter("@Reporte", ReporteId);
        conBD.CreateParameter("@SP", SP, SP.Length);


        return conBD.getDataSet();

    }
    //public DataSet Rep_Reportes(int Usuarioid, int SistemaId)//, int Companiaid)
    //{
    //    conBD.SetCommand("[Reportes].ConsultaReportes");
    //    conBD.CreateParameter("@Usuario", Usuarioid);
    //    if (SistemaId != 0)
    //    {
    //        conBD.CreateParameter("@Sistema", SistemaId);
    //    }

    //    return conBD.getDataSet();

    //}

    public DataSet Rep_Reporte(int Usuarioid, int SistemaId, int ReporteId)//, int Companiaid)
    {
        conBD.SetCommand(ConfigurationManager.AppSettings["Esquema"].ToString()+".ConsultaReporte");
        conBD.CreateParameter("@Usuario", Usuarioid);
        if (SistemaId != 0)
        {
            conBD.CreateParameter("@Sistema", SistemaId);
        }
        if (ReporteId != 0)
        {
            conBD.CreateParameter("@ReporteId", ReporteId);
        }

        return conBD.getDataSet();

    }

    public DataSet Rep_Ejecuta(string Query, int @ReporteId, int UsuarioId, int SistemaId)
    {
        DataSet ds = new DataSet();
        conBD.SetCommand(ConfigurationManager.AppSettings["Esquema"].ToString() + ".Ejecuta");
        conBD.CreateParameter("@Query", Query, Query.Length);
        conBD.CreateParameter("@ReporteId", ReporteId);
        conBD.CreateParameter("@usuarioId", UsuarioId);
        if (SistemaId != 0)
        {
            conBD.CreateParameter("@SistemaId", SistemaId);
        }
        ds = conBD.getDataSet();
        result_number = conBD.result_number;
        result_msg = conBD.result_msg;
        return ds;
    }
    public DataSet Rep_Campos(int Reporte)
    {
        conBD.SetCommand(ConfigurationManager.AppSettings["Esquema"].ToString() + ".VerCampos");
        conBD.CreateParameter("@ReporteId", Reporte);
        return conBD.getDataSet();
    }

    public DataSet Rep_Actualiza(int Reporte)
    {
        conBD.SetCommand(ConfigurationManager.AppSettings["Esquema"].ToString() + ".Actualiza");
        conBD.CreateParameter("@ReporteId", Reporte);
        return conBD.getDataSet();

    }

    public DataSet Con_MenuOpcion(int Usuario, int Padre, int SistemaId)
    {
            conBD.SetCommand(ConfigurationManager.AppSettings["Esquema"].ToString() + ".[Sel_MenuOpcion2]");
        conBD.CreateParameter("@id_Usuario", Usuario);
        conBD.CreateParameter("@Padre", Padre);
        if (SistemaId != 0)
        {
            conBD.CreateParameter("@SistemaId", SistemaId);
        }
        return conBD.getDataSet();

    }
    public DataSet Val_Usuario(string usuario, string Password, int Id_Usuario, int accion, int Token)
    {
        conBD.SetCommand(ConfigurationManager.AppSettings["Esquema"].ToString() + ".[Usuarios]");
        conBD.CreateParameter("@Usuario", usuario, usuario.Length);
        conBD.CreateParameter("@Pass", Password, Password.Length);
        conBD.CreateParameter("@Id_Usuario", Id_Usuario);
        conBD.CreateParameter("@Accion", accion);
        conBD.CreateParameter("@Token", Token);


        return conBD.getDataSet();

    }

    public DataSet Con_Avisos(int Id_Usuario, int TipoAccion)
    {
        conBD.SetCommand(ConfigurationManager.AppSettings["Esquema"].ToString() + ".[SP_BlogNoticias_CON]");
        conBD.CreateParameter("@Usuario_Id", Id_Usuario);
        conBD.CreateParameter("@TipoAccion", TipoAccion);


        return conBD.getDataSet();

    }

    public void Rep_Error(int Usuario, string Pagina, string Error)
    {
        conBD.SetCommand(ConfigurationManager.AppSettings["Esquema"].ToString() + ".ErrorLOG");
        conBD.CreateParameter("@IDUsuario", Usuario);
        conBD.CreateParameter("@URL", Pagina, 1000);
        conBD.CreateParameter("@Error", Error, 1000);
        conBD.ExecuteNonQuery();
    }


    public SqlDataReader Rep_Ejecuta(string Query)
    {

        Query = ConfigurationManager.AppSettings["Esquema"].ToString() + ".Ejecuta" + " " + Query;
        SqlDataReader ds = conBD.getDataReader(Query, 999999);
       result_number = conBD.result_number;
        result_msg = conBD.result_msg;
        return ds;
    }


    public void  Rep_Close()
    {
        if (conBD != null)
            conBD.Close();
     
    }

}
