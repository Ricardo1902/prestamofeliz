﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

/// <summary>
/// Descripción breve de DataTableAjaxPostModel
/// </summary>
namespace DataTables
{
    public class DataTableAjaxPostModel
    {
        // properties are not capital due to json mapping
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public List<Column> columns { get; set; }
        public Search search { get; set; }
        public List<Order> order { get; set; }
        public DataTableAjaxPostModel()
        {
        }
    }
    public class Column
    {
        public string data { get; set; }
        public string name { get; set; }
        public bool searchable { get; set; }
        public bool orderable { get; set; }
        public Search search { get; set; }
        public Column()
        {
        }
    }

    public class Search
    {
        public string value { get; set; }
        public string regex { get; set; }
        public Search()
        {
        }
    }

    public class Order
    {
        public int column { get; set; }
        public string dir { get; set; }
        public Order()
        {
        }
    }

    public class jQueryDataTableParamModel
    {
        /// <summary>
        /// Request sequence number sent by DataTable, same value must be returned in response
        /// </summary>       
        public int sEcho { get; set; }

        /// <summary>
        /// Text used for filtering
        /// </summary>
        public string sSearch { get; set; }
        /// <summary>
        /// Text used for filtering
        /// </summary>
        //public string sSearch_1 { get; set; }
        //public string sSearch1 { get; set; }



        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>
        public int iDisplayLength { get; set; }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>
        public int iDisplayStart { get; set; }

        /// <summary>
        /// Number of columns in table
        /// </summary>
        public int iColumns { get; set; }

        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>
        public int? iSortingCols { get; set; }

        /// <summary>
        /// Comma separated list of column names
        /// </summary>
        public string sColumns { get; set; }

        //public string bSearchable { get; set; }
        public ReadOnlyCollection<bool> bSearchable_ { get; set; }

        public ReadOnlyCollection<string> sSearch_ { get; set; }

        public ReadOnlyCollection<int> iSortCol_ { get; set; }

        public ReadOnlyCollection<string> sSortDir_ { get; set; }
    }
}