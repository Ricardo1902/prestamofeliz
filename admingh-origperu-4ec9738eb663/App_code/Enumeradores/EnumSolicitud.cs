﻿using System.ComponentModel;

public enum EstatusSolicitud
{
    [Description("Activa")]
    Activa = 1,
    [Description("Cancelada por Cliente")]
    CanceladaCliente = 2,
    [Description("Declinada por Analista")]
    DeclinadaAnalista = 3,
    [Description("Pendiente")]
    Pendiente = 4,
    [Description("Operada")]
    Operada = 5,
    [Description("Condicionada")]
    Condicionada = 6,
    [Description("Declinada por Sistema")]
    DeclinadaSistema = 7
}