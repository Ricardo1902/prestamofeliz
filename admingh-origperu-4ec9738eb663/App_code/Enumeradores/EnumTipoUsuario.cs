﻿using System.ComponentModel;

public enum TipoUsuario
{
    [Description("Promotor")]
    Promotor = 2,
    [Description("Mesa de Control")]
    MesaControl = 4,
    [Description("Gerente Mesa de Control")]
    GerenteMesaControl = 5,
    [Description("Gerente de Cobranza")]
    GerenteCobranza = 7,
    [Description("Gestor Telefonico")]
    GestorTelefonico = 26,
    [Description("Gestor Campo")]
    GestorCampo = 27,
    [Description("Gestor Agencia")]
    GestorAgencia = 28,
    [Description("Jefe Cobranza")]
    JefeCobranza = 29,
    [Description("Auxiliar Cobranza")]
    AuxiliarCobranza = 29,
    [Description("Gerente Regional")]
    GerenteRegional = 38,
    [Description("Gerente Sucursal")]
    GerenteSucursal = 41,
    [Description("Auxiliar Sucursal")]
    AuxiliarSucursal = 42,
    [Description("Administrador")]
    Administrador = 99
}