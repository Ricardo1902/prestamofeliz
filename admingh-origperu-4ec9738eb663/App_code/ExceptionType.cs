﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CalculoLiquidacionException : Exception
{
    public CalculoLiquidacionException(string Message) : base(Message)
    { }    
}

public class LiquidacionException : Exception
{
    public LiquidacionException(string Message) : base(Message)
    { }
}