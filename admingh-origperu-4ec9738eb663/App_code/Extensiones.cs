﻿using System.Web;
public static class Extensiones
{
    public static int ValidarUsuario(this HttpContext context)
    {
        int idUsuario = 0;
        if (context.Session["UsuarioId"] != null)
            int.TryParse(context.Session["UsuarioId"].ToString(), out idUsuario);
        if (idUsuario >= 0)
        {
            return idUsuario;
        }
        return -1;
    }

    public static int UsuarioActual(this System.Web.SessionState.HttpSessionState sesion)
    {

        if (sesion["UsuarioId"] != null)
        {
            int idUsuario;
            int.TryParse(sesion["UsuarioId"].ToString(), out idUsuario);
            return idUsuario;
        }
        return -1;
    }

    public static string QueryString(this HttpContext context, string parametro)
    {
        string valor = string.Empty;
        if (string.IsNullOrEmpty(parametro)) return valor;
        if (context.Request.QueryString[parametro] != null)
        {
            valor = context.Request.QueryString[parametro].ToString();
        }
        return valor;
    }
}