﻿using System;
/// <summary>
/// Descripción breve de Gestion
/// </summary>
public class Gestion
{
    public int Id { get; set; }
    public int IdCuenta { get; set; }
    public int IdGestor { get; set; }
    public string Comentario { get; set; }
    public int IdResultadoGestion { get; set; }
    public int IdContacto { get; set; }
    public int IdCampania { get; set; }
    public DateTime? FechaPromesa { get; set; }
    public decimal? MontoPromesa { get; set; }
    public DateTime? FechaLlamada { get; set; }
    public string[] Avisos { get; set; }
    public int IdTipoGestion { get; set; }
    public decimal? MontoDomiciliar { get; set; }
    public DateTime? FechaDomiciliar { get; set; }
    public int IdTipoCobro { get; set; }
    public Gestion()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }
}