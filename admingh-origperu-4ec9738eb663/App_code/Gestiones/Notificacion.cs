﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using ViewModels;
using wsSOPF;

/// <summary>
/// Clase que genera el documento de notificación para gestiones
/// </summary>
public class Notificacion
{
    private const int posXMax = 612;
    private const int posYMax = 1008;

    private static string[] titulosFormato = { "Señor",  "(a)", "(ita):", "Dirección:", "Referencia", "Domiciliaria:",
        "Señor",  "(a)", "(ita):", "Dirección:", "Referencia", "Domiciliaria:",
        "Deuda",  "Impaga", "a", "la", "fecha", "de", "emisión", "carta:",
        "Numero",  "cuotas", "vencidas:", "la", "ID", "cliente:",
        "Dirección",  "laboral:", "Dependencia", "y", "Ubicación", "Centro", "Laboral:"
    };

    public Notificacion()
    {

    }

    /**
        Notificacion	                Clave	Descripcion
        Aviso de Cobranza	            AC	    Documento de notificación de cobranza.
        Aviso de Pago	                AP	    Documento de notificación de cobranza.
        Notificación Prejudicial	    NP	    Documento de notificación de cobranza.
        Ultima Notificación Prejudicial UNP	    Documento de notificación de cobranza.
        Citación Prejudicial	        CP	    Documento de notificación de cobranza.
        Promoción de Reducción de Mora	PRM	    ocumento opcional de promoción de cobranza con el concepto de descuento de mora.
    **/
    public static GenerarNotificacionResultado GenerarNotificacion(NotificacionRequest notif, string nombreDocumento, bool actualizar = false)
    {
        GenerarNotificacionResultado resultado = new GenerarNotificacionResultado();
        NotificacionRequest notifiLocal = notif;
        DateTime? DatoFechaHora = null;
        DateTime? DatoFechaVigencia = null;
        if (notifiLocal.Clave == "CP")
        {
            DatoFechaHora = ValidarFechaHoraCitacion(notif.FechaCitacion, notif.HoraCitacion);
            if (!DatoFechaHora.HasValue)
            {
                resultado.Error = true;
                resultado.MensajeOperacion = "Los formatos de fecha u hora son inválidos.";
                return resultado;
            }
        }
        else if (notifiLocal.Clave == "PRM")
        {
            DatoFechaVigencia = ValidarFechaHoraCitacion(notif.FechaVigencia, "23:59");
            if (!DatoFechaVigencia.HasValue)
            {
                resultado.Error = true;
                resultado.MensajeOperacion = "El formato de fecha de vigencia es inválido.";
                return resultado;
            }
        }
        if (notifiLocal.IdUsuario <= 0 || notifiLocal.IdSolicitud <= 0
            || string.IsNullOrEmpty(notifiLocal.Clave) || notifiLocal.IdDestino <= 0) return null;
        byte[] notificacion = null;
        try
        {
            string cadena = string.Format(@"\Site\PlantillasPDF\{0}.pdf", nombreDocumento);
            string plantillaNotificacion = HostingEnvironment.ApplicationPhysicalPath + cadena;

            using (wsSOPF.GestionClient ws = new wsSOPF.GestionClient())
            {
                switch (notifiLocal.Clave)
                {
                    case "AC":
                        wsSOPF.DatosAvisoCobranzaRequest petiDatosAvisoCobranza = new wsSOPF.DatosAvisoCobranzaRequest()
                        {
                            IdUsuario = notifiLocal.IdUsuario,
                            IdSolicitud = notifiLocal.IdSolicitud,
                            Clave = notifiLocal.Clave,
                            IdDestino = notifiLocal.IdDestino,
                            Actualizar = actualizar
                        };
                        wsSOPF.DatosAvisoCobranzaResponse respDatosAvisoCobranza = ws.DatosAvisoCobranza(petiDatosAvisoCobranza);
                        if (respDatosAvisoCobranza.Error)
                            throw new Exception(respDatosAvisoCobranza.MensajeOperacion);
                        notificacion = EscribirDatosPlantilla(plantillaNotificacion, notifiLocal, respDatosAvisoCobranza, typeof(wsSOPF.DatosAvisoCobranzaResponse));
                        break;
                    case "AP":
                        wsSOPF.DatosAvisoPagoRequest petiDatosAvisoPago = new wsSOPF.DatosAvisoPagoRequest()
                        {
                            IdUsuario = notifiLocal.IdUsuario,
                            IdSolicitud = notifiLocal.IdSolicitud,
                            Clave = notifiLocal.Clave,
                            IdDestino = notifiLocal.IdDestino,
                            Actualizar = actualizar
                        };
                        wsSOPF.DatosAvisoPagoResponse respDatosAvisoPago = ws.DatosAvisoPago(petiDatosAvisoPago);
                        if (respDatosAvisoPago.Error)
                            throw new Exception(respDatosAvisoPago.MensajeOperacion);
                        notificacion = EscribirDatosPlantilla(plantillaNotificacion, notifiLocal, respDatosAvisoPago, typeof(wsSOPF.DatosAvisoPagoResponse));
                        break;
                    case "NP":
                        wsSOPF.DatosNotificacionPrejudicialRequest petiDatosNotificacionPrejudicial = new wsSOPF.DatosNotificacionPrejudicialRequest()
                        {
                            IdUsuario = notifiLocal.IdUsuario,
                            IdSolicitud = notifiLocal.IdSolicitud,
                            Clave = notifiLocal.Clave,
                            IdDestino = notifiLocal.IdDestino,
                            Actualizar = actualizar
                        };
                        wsSOPF.DatosNotificacionPrejudicialResponse respDatosNotificacionPrejudicial = ws.DatosNotificacionPrejudicial(petiDatosNotificacionPrejudicial);
                        if (respDatosNotificacionPrejudicial.Error)
                            throw new Exception(respDatosNotificacionPrejudicial.MensajeOperacion);
                        notificacion = EscribirDatosPlantilla(plantillaNotificacion, notifiLocal, respDatosNotificacionPrejudicial, typeof(wsSOPF.DatosNotificacionPrejudicialResponse));
                        break;
                    case "UNP":
                        wsSOPF.DatosUltimaNotificacionPrejudicialRequest petiDatosUltimaNotificacionPrejudicial = new wsSOPF.DatosUltimaNotificacionPrejudicialRequest()
                        {
                            IdUsuario = notifiLocal.IdUsuario,
                            IdSolicitud = notifiLocal.IdSolicitud,
                            Clave = notifiLocal.Clave,
                            IdDestino = notifiLocal.IdDestino,
                            Actualizar = actualizar
                        };
                        wsSOPF.DatosUltimaNotificacionPrejudicialResponse respDatosUltimaNotificacionPrejudicial = ws.DatosUltimaNotificacionPrejudicial(petiDatosUltimaNotificacionPrejudicial);
                        if (respDatosUltimaNotificacionPrejudicial.Error)
                            throw new Exception(respDatosUltimaNotificacionPrejudicial.MensajeOperacion);
                        notificacion = EscribirDatosPlantilla(plantillaNotificacion, notifiLocal, respDatosUltimaNotificacionPrejudicial, typeof(wsSOPF.DatosUltimaNotificacionPrejudicialResponse));
                        break;
                    case "CP":
                        wsSOPF.DatosCitacionPrejudicialRequest petiDatosCitacionPrejudicial = new wsSOPF.DatosCitacionPrejudicialRequest()
                        {
                            IdUsuario = notifiLocal.IdUsuario,
                            IdSolicitud = notifiLocal.IdSolicitud,
                            Clave = notifiLocal.Clave,
                            IdDestino = notifiLocal.IdDestino,
                            FechaHoraCitacion = DatoFechaHora.Value,
                            Actualizar = actualizar
                        };
                        wsSOPF.DatosCitacionPrejudicialResponse respDatosCitacionPrejudicial = ws.DatosCitacionPrejudicial(petiDatosCitacionPrejudicial);
                        if (respDatosCitacionPrejudicial.Error)
                            throw new Exception(respDatosCitacionPrejudicial.MensajeOperacion);
                        notificacion = EscribirDatosPlantilla(plantillaNotificacion, notifiLocal, respDatosCitacionPrejudicial, typeof(wsSOPF.DatosCitacionPrejudicialResponse));
                        break;
                    case "PRM":
                        wsSOPF.DatosPromocionReduccionMoraRequest petiDatosPromocionReduccionMora = new wsSOPF.DatosPromocionReduccionMoraRequest()
                        {
                            IdUsuario = notifiLocal.IdUsuario,
                            IdSolicitud = notifiLocal.IdSolicitud,
                            Clave = notifiLocal.Clave,
                            IdDestino = notifiLocal.IdDestino,
                            DescuentoDeuda = notifiLocal.DescuentoDeuda,
                            FechaVigencia = DatoFechaVigencia.Value,
                            Actualizar = actualizar
                        };
                        wsSOPF.DatosPromocionReduccionMoraResponse respDatosPromocionReduccionMora = ws.DatosPromocionReduccionMora(petiDatosPromocionReduccionMora);
                        if (respDatosPromocionReduccionMora.Error)
                            throw new Exception(respDatosPromocionReduccionMora.MensajeOperacion);
                        notificacion = EscribirDatosPlantilla(plantillaNotificacion, notifiLocal, respDatosPromocionReduccionMora, typeof(wsSOPF.DatosPromocionReduccionMoraResponse));
                        break;
                    default:
                        throw new Exception("Clave de notificación inválida.");
                }
                resultado.Resultado = notificacion;
            }
        }
        catch (Exception e)
        {
            Utilidades.EscribirLog("Error: " + e.Message, e.StackTrace);
            resultado.Error = true;
            resultado.Resultado = null;
            resultado.MensajeOperacion = "Error: " + e.Message;
        }
        return resultado;
    }

    private static byte[] EscribirDatosPlantilla(string plantilla, NotificacionRequest notificacion, object datos, Type tipoDatos)
    {
        int idSolicitud = notificacion.IdSolicitud;
        bool esDomicilio = notificacion.IdDestino == 1;
        BaseFont arial = BaseFont.CreateFont(HostingEnvironment.ApplicationPhysicalPath + "fonts\\arial.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
        BaseFont arialNeg = BaseFont.CreateFont(HostingEnvironment.ApplicationPhysicalPath + "fonts\\arialbd.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
        BaseFont tahoma = BaseFont.CreateFont(HostingEnvironment.ApplicationPhysicalPath + "fonts\\tahoma.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);

        PdfReader pdfReader = new PdfReader(plantilla);
        Rectangle tamanioOrientacion = pdfReader.GetPageSizeWithRotation(1);
        int posMaxY = (int)tamanioOrientacion.Height;

        using (MemoryStream ms = new MemoryStream())
        {
            Document documento = new Document(tamanioOrientacion);
            PdfWriter pdfWriter = PdfWriter.GetInstance(documento, ms);
            documento.Open();
            PdfContentByte contenido = pdfWriter.DirectContent;
            PdfImportedPage pagina = pdfWriter.GetImportedPage(pdfReader, 1);
            contenido.AddTemplate(pagina, 0, 0);
            contenido.BeginText();
            string dia = string.Empty;
            string mes = string.Empty;
            string anio = string.Empty;
            string fecha = string.Empty;
            int dif = 0;
            int espaciado = 0;
            int renglonesMultiLinea = 0;
            switch (tipoDatos.ToString())
            {
                case "wsSOPF.DatosAvisoCobranzaResponse":
                    DatosAvisoCobranzaResponse dac = (DatosAvisoCobranzaResponse)datos;
                    dia = dac.Resultado.FechaRegistro.Day.ToString("00");
                    mes = Utilidades.MesFecha(dac.Resultado.FechaRegistro);
                    anio = dac.Resultado.FechaRegistro.Year.ToString();
                    fecha = dia + " de " + mes + " del " + anio;
                    dif = 142;
                    espaciado = 15;
                    renglonesMultiLinea = 1;
                    EscribirTexto(contenido, fecha + ".", 102, posMaxY - 82, 12, arial, PdfContentByte.ALIGN_LEFT);
                    renglonesMultiLinea = EscribirTexto3(contenido, "Señor (a) (ita): " + dac.Resultado.NombreCliente + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Dirección" + (!esDomicilio ? " laboral" : "") + ": " + dac.Resultado.Direccion + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, (esDomicilio ? "Referencia Domiciliaria: " : "Dependencia y Ubicación Centro Laboral: ") + dac.Resultado.ReferenciaDomicilio + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Deuda Impaga a la fecha de la emisión de la carta: " + dac.Resultado.TotalDeuda.ToString("C", CultureInfo.CurrentCulture) + " (no incluye intereses moratorios)" + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Numero de cuotas vencidas: " + dac.Resultado.CuotasVencidas.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "ID de cliente: " + dac.Resultado.IdCliente.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    break;
                case "wsSOPF.DatosAvisoPagoResponse":
                    DatosAvisoPagoResponse dap = (DatosAvisoPagoResponse)datos;
                    dia = dap.Resultado.FechaRegistro.Day.ToString("00");
                    mes = Utilidades.MesFecha(dap.Resultado.FechaRegistro);
                    anio = dap.Resultado.FechaRegistro.Year.ToString();
                    fecha = dia + " de " + mes + " del " + anio;
                    dif = 142;
                    espaciado = 15;
                    renglonesMultiLinea = 1;
                    EscribirTexto(contenido, fecha + ".", 102, posMaxY - 82, 12, arial, PdfContentByte.ALIGN_LEFT);
                    renglonesMultiLinea = EscribirTexto3(contenido, "Señor (a) (ita): " + dap.Resultado.NombreCliente + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Dirección" + (!esDomicilio ? " laboral" : "") + ": " + dap.Resultado.Direccion + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, (esDomicilio ? "Referencia Domiciliaria: " : "Dependencia y Ubicación Centro Laboral: ") + dap.Resultado.ReferenciaDomicilio + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Deuda Impaga a la fecha de la emisión de la carta: " + dap.Resultado.TotalDeuda.ToString("C", CultureInfo.CurrentCulture) + " (no incluye intereses moratorios)" + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Numero de cuotas vencidas: " + dap.Resultado.CuotasVencidas.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "ID de cliente: " + dap.Resultado.IdCliente.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    break;
                case "wsSOPF.DatosNotificacionPrejudicialResponse":
                    DatosNotificacionPrejudicialResponse dnp = (DatosNotificacionPrejudicialResponse)datos;
                    dia = dnp.Resultado.FechaRegistro.Day.ToString("00");
                    mes = Utilidades.MesFecha(dnp.Resultado.FechaRegistro);
                    anio = dnp.Resultado.FechaRegistro.Year.ToString();
                    fecha = dia + " de " + mes + " del " + anio;
                    dif = 142;
                    espaciado = 15;
                    renglonesMultiLinea = 1;
                    EscribirTexto(contenido, fecha + ".", 102, posMaxY - 82, 12, arial, PdfContentByte.ALIGN_LEFT);
                    renglonesMultiLinea = EscribirTexto3(contenido, "Señor (a) (ita): " + dnp.Resultado.NombreCliente + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Dirección" + (!esDomicilio ? " laboral" : "") + ": " + dnp.Resultado.Direccion + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, (esDomicilio ? "Referencia Domiciliaria: " : "Dependencia y Ubicación Centro Laboral: ") + dnp.Resultado.ReferenciaDomicilio + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Deuda Impaga a la fecha de la emisión de la carta: " + dnp.Resultado.TotalDeuda.ToString("C", CultureInfo.CurrentCulture) + " (no incluye intereses moratorios)" + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Numero de cuotas vencidas: " + dnp.Resultado.CuotasVencidas.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "ID de cliente: " + dnp.Resultado.IdCliente.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    break;
                case "wsSOPF.DatosUltimaNotificacionPrejudicialResponse":
                    DatosUltimaNotificacionPrejudicialResponse dunp = (DatosUltimaNotificacionPrejudicialResponse)datos;
                    dia = dunp.Resultado.FechaRegistro.Day.ToString("00");
                    mes = Utilidades.MesFecha(dunp.Resultado.FechaRegistro);
                    anio = dunp.Resultado.FechaRegistro.Year.ToString();
                    fecha = dia + " de " + mes + " del " + anio;
                    dif = 142;
                    espaciado = 15;
                    renglonesMultiLinea = 1;
                    EscribirTexto(contenido, fecha + ".", 102, posMaxY - 82, 12, arial, PdfContentByte.ALIGN_LEFT);
                    renglonesMultiLinea = EscribirTexto3(contenido, "Señor (a) (ita): " + dunp.Resultado.NombreCliente + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Dirección" + (!esDomicilio ? " laboral" : "") + ": " + dunp.Resultado.Direccion + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, (esDomicilio ? "Referencia Domiciliaria: " : "Dependencia y Ubicación Centro Laboral: ") + dunp.Resultado.ReferenciaDomicilio + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Deuda Impaga a la fecha de la emisión de la carta: " + dunp.Resultado.TotalDeuda.ToString("C", CultureInfo.CurrentCulture) + " (no incluye intereses moratorios)" + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Numero de cuotas vencidas: " + dunp.Resultado.CuotasVencidas.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "ID de cliente: " + dunp.Resultado.IdCliente.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    break;
                case "wsSOPF.DatosCitacionPrejudicialResponse":
                    DatosCitacionPrejudicialResponse dcp = (DatosCitacionPrejudicialResponse)datos;
                    dia = dcp.Resultado.FechaRegistro.Day.ToString("00");
                    mes = Utilidades.MesFecha(dcp.Resultado.FechaRegistro);
                    anio = dcp.Resultado.FechaRegistro.Year.ToString();
                    fecha = dia + " de " + mes + " del " + anio;
                    dif = 142;
                    espaciado = 15;
                    renglonesMultiLinea = 1;
                    EscribirTexto(contenido, fecha + ".", 102, posMaxY - 82, 12, arial, PdfContentByte.ALIGN_LEFT);
                    renglonesMultiLinea = EscribirTexto3(contenido, "Señor (a) (ita): " + dcp.Resultado.NombreCliente + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Dirección" + (!esDomicilio ? " laboral" : "") + ": " + dcp.Resultado.Direccion + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, (esDomicilio ? "Referencia Domiciliaria: " : "Dependencia y Ubicación Centro Laboral: ") + dcp.Resultado.ReferenciaDomicilio + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Deuda Impaga a la fecha de la emisión de la carta: " + dcp.Resultado.TotalDeuda.ToString("C", CultureInfo.CurrentCulture) + " (no incluye intereses moratorios)" + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Numero de cuotas vencidas: " + dcp.Resultado.CuotasVencidas.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "ID de cliente: " + dcp.Resultado.IdCliente.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    if (dcp.Resultado.FechaHoraCitacion != null)
                    {
                        EscribirTexto(contenido, dcp.Resultado.FechaHoraCitacion.ToString("dd/MM/yyyy"), 120, posMaxY - 380, 18, arial, PdfContentByte.ALIGN_LEFT);
                        EscribirTexto(contenido, dcp.Resultado.FechaHoraCitacion.ToString("HH:mm"), 130, posMaxY - 402, 18, arial, PdfContentByte.ALIGN_LEFT);
                    }
                    break;
                case "wsSOPF.DatosPromocionReduccionMoraResponse":
                    DatosPromocionReduccionMoraResponse dprm = (DatosPromocionReduccionMoraResponse)datos;
                    dia = dprm.Resultado.FechaRegistro.Day.ToString("00");
                    mes = Utilidades.MesFecha(dprm.Resultado.FechaRegistro);
                    anio = dprm.Resultado.FechaRegistro.Year.ToString();
                    fecha = dia + " de " + mes + " del " + anio;
                    dif = 142;
                    espaciado = 15;
                    renglonesMultiLinea = 1;
                    EscribirTexto(contenido, fecha + ".", 102, posMaxY - 82, 12, arial, PdfContentByte.ALIGN_LEFT);
                    renglonesMultiLinea = EscribirTexto3(contenido, "Señor (a) (ita): " + dprm.Resultado.NombreCliente + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Dirección" + (!esDomicilio ? " laboral" : "") + ": " + dprm.Resultado.Direccion + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, (esDomicilio ? "Referencia Domiciliaria: " : "Dependencia y Ubicación Centro Laboral: ") + dprm.Resultado.ReferenciaDomicilio + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Deuda Impaga a la fecha de la emisión de la carta: " + dprm.Resultado.TotalDeuda.ToString("C", CultureInfo.CurrentCulture) + " (no incluye intereses moratorios)" + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "Numero de cuotas vencidas: " + dprm.Resultado.CuotasVencidas.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    dif = dif + (espaciado * renglonesMultiLinea);
                    renglonesMultiLinea = 1;
                    renglonesMultiLinea = EscribirTexto3(contenido, "ID de cliente: " + dprm.Resultado.IdCliente.ToString() + ".", 72, posMaxY - dif, 10, arial, posMaxY, dif, PdfContentByte.ALIGN_LEFT);
                    if (dprm.Resultado.FechaVigencia != null)
                    {
                        //EscribirTexto(contenido, dprm.Resultado.DescuentoDeuda.ToString(), 110, posMaxY - 407, 10, arial, PdfContentByte.ALIGN_LEFT);
                        //EscribirTexto(contenido, dprm.Resultado.DescuentoDeuda.ToString(), 319, posMaxY - 452, 14, arial, PdfContentByte.ALIGN_LEFT);
                        EscribirTexto(contenido, dprm.Resultado.FechaVigencia.ToString("dd/MM/yyyy"), 378, posMaxY - 517, 14, arial, PdfContentByte.ALIGN_LEFT);
                    }
                    break;
                default:
                    break;
            }
            /* CODIGO PARA DIBUJAR LA CUADRICULA, NO BORRAR */
            for (int ai = 0; ai < posYMax; ai += 5)
            {
                EscribirTexto(contenido, PosY(ai).ToString(), 2, ai, 4, tahoma, 0);
                for (int j = 10; j < posXMax; j += 10)
                {
                    EscribirTexto2(contenido, j.ToString(), j, PosY(ai), 5, tahoma, 0);
                }
            }
            contenido.EndText();
            documento.Close();
            pdfReader.Close();
            return ms.ToArray();
        }
    }

    private static int EscribirTexto3(PdfContentByte contenido, string texto, int x, int y, int tamanioLetra, BaseFont fuente, int posMaxY, int diferenciaEnY, int alineacion = PdfContentByte.ALIGN_LEFT)
    {
        if (string.IsNullOrEmpty(texto)) texto = "";
        int linea = 0;
        float ancho = 0;
        BaseFont fuenteLocal = fuente;
        contenido.SetFontAndSize(fuenteLocal, tamanioLetra);
        string[] palabras = texto.Split(' ');
        linea = posMaxY - diferenciaEnY;
        int columna = 72;
        int posMax = 515;
        int renglones = 1;
        foreach (string palabra in palabras)
        {
            if (columna + (palabra.Length * 6) > posMax)
            {
                linea -= 15;
                columna = 72;
                renglones++;
            }
            if (titulosFormato.Contains(palabra))
            {
                fuenteLocal = BaseFont.CreateFont(HostingEnvironment.ApplicationPhysicalPath + "fonts\\arialbd.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
            }
            else
            {
                fuenteLocal = fuente;
            }
            ancho = fuenteLocal.GetWidthPoint(palabra, tamanioLetra);
            EscribirTexto(contenido, palabra, columna, linea, tamanioLetra, fuenteLocal, alineacion);
            columna += Convert.ToInt32(ancho) + 4;
        }
        return renglones;
    }

    private static void EscribirTexto2(PdfContentByte contenido, string texto, int x, int y, int tamanioLetra, BaseFont fuente, int alineacion = PdfContentByte.ALIGN_LEFT)
    {
        if (string.IsNullOrEmpty(texto)) texto = "";
        contenido.SetFontAndSize(fuente, tamanioLetra);
        contenido.SetRGBColorFill(200, 100, 200);
        contenido.ShowTextAligned(alineacion, texto, x, y, 0);
    }

    private static void EscribirTexto(PdfContentByte contenido, string texto, int x, int y, int tamanioLetra, BaseFont fuente, int alineacion = PdfContentByte.ALIGN_LEFT)
    {
        if (string.IsNullOrEmpty(texto)) texto = "";
        contenido.SetFontAndSize(fuente, tamanioLetra);
        contenido.ShowTextAligned(alineacion, texto, x, y, 0);
    }

    private static int PosY(int posY)
    {
        return posYMax - posY;
    }

    public static string ObtenerNombreArchivo(string clave)
    {
        using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
        {
            var respNombreArchivo = wsG.ObtenerNombreArchivoPorClave(new wsSOPF.ObtenerNombreArchivoPorClaveRequest { Accion = "GET_NOMBRE_ARCHIVO_POR_CLAVE", Clave = clave });
            if (respNombreArchivo != null && respNombreArchivo.Resultado != null && respNombreArchivo.Resultado.Notificacion != null)
            {
                return respNombreArchivo.Resultado.Notificacion.NombreArchivo;
            }
            else
            {
                return "Error";
            }
        }
    }

    public static DateTime? ValidarFechaHoraCitacion(string fecha, string hora)
    {
        Regex rg = new Regex(@"^(?:[01][0-9]|2[0-3]):[0-5][0-9]$");
        if (rg.IsMatch(hora))
        {
            string[] formats = { "dd/MM/yyyy HH:mm" };
            DateTime FechaHoraFinal;
            string fechaHora = string.Format("{0} {1}", fecha, hora);
            if (DateTime.TryParseExact(fechaHora, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaHoraFinal))
            {
                return FechaHoraFinal;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
}
