﻿using System.Runtime.Serialization;
/// <summary>
/// Descripción breve de PagoRequest
/// </summary>
public class FallidosPagoKushkiRequest
{

    public int Linea { get; set; }
    public string Solicitud { get; set; }
    public double Monto { get; set; }
    public string NoTarjeta { get; set; }
    public string NombreTarjetahabiente { get; set; }
    public string Telefono { get; set; }
    public string Mensaje { get; set; }
}