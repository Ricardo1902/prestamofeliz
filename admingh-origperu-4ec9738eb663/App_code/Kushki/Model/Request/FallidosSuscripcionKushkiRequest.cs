﻿using System.Runtime.Serialization;
/// <summary>
/// Descripción breve de PagoRequest
/// </summary>
public class FallidosSuscripcionKushkiRequest
{

    public int Linea { get; set; }
    public string NoTarjeta { get; set; }
    public string MesExpiracion { get; set; }
    public string AnioExpiracion { get; set; }
    public string NombreTarjetahabiente { get; set; }
    public string Cvv { get; set; }
    public string TipoDocumento { get; set; }
    public string Documento { get; set; }
    public string email { get; set; }
    public string Apellido { get; set; }
    public string Telefono { get; set; }
    public string Mensaje { get; set; }
}