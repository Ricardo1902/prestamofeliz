﻿using System;

namespace Kushki.Model.Response
{
    

    public class IdComercioResponsekushki
    {
        public string Moneda { get; set; }
        public string IdComercio { get; set; }
    }
}