﻿using System;

namespace Kushki.Model.Response
{
    public class PagoResponseKushki : Respuesta
    {
        public PagoResultadokushki Resultado { get; set; }

        public PagoResponseKushki()
        {
            Resultado = new PagoResultadokushki();
        }
    }

    public class PagoResultadokushki
    {
        public int IdPeticion { get; set; }
        public DateTime FechaTransaccion { get; set; }
        public string[][] rowdev { get; set; }
        public int ndatos { get; set; }
    }
}