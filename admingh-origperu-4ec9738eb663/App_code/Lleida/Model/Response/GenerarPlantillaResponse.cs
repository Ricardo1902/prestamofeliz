﻿using System.Collections.Generic;
using wsSOPF;

namespace Visanet.Model.Response
{
    public class PlantillaResponse : Respuesta
    {
        public List<GeneraPlantillaResult> Resultado { get; set; }

        public PlantillaResponse()
        { 
            Resultado = new List<GeneraPlantillaResult>();
        }
    }

    public class PlantillaResultado
    {
        public string IdPlantilla { get; set; }
    }
}
