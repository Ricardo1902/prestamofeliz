﻿public class ObtenerLiquidacionRequest
{
    public int IdSolicitud { get; set; }
    public string FechaLiquidacion { get; set; }

    public ObtenerLiquidacionRequest()
    {
    }
}