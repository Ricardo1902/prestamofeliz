﻿using System;

public class NotificacionRequest
{
    public int IdUsuario { get; set; }
    public int IdSolicitud { get; set; }
    public string Clave { get; set; }
    public int IdDestino { get; set; }
    public string FechaCitacion { get; set; }
    public string HoraCitacion { get; set; }
    public decimal? DescuentoDeuda { get; set; }
    public string FechaVigencia { get; set; }
    public bool Actualizar { get; set; }
    public bool Correo { get; set; }
    public bool Courier { get; set; }

    public NotificacionRequest()
    {
    }
}