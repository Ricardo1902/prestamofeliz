﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

/// <summary>
/// Descripción breve de Utilidades
/// </summary>
public static class Utilidades
{
    private static bool[] _lookup;
    private static readonly string nombreSistema = "OrigPeru";

    static Utilidades()
    {
        _lookup = new bool[65536];
        for (char c = '0'; c <= '9'; c++) _lookup[c] = true;
        for (char c = 'A'; c <= 'Z'; c++) _lookup[c] = true;
        for (char c = 'a'; c <= 'z'; c++) _lookup[c] = true;
        _lookup[' '] = true;
        _lookup['á'] = true;
        _lookup['Á'] = true;
        _lookup['é'] = true;
        _lookup['í'] = true;
        _lookup['Í'] = true;
        _lookup['ó'] = true;
        _lookup['Ó'] = true;
        _lookup['ú'] = true;
        _lookup['Ú'] = true;
        _lookup['ñ'] = true;
        _lookup['Ñ'] = true;
    }

    /// <summary>
    /// Verifica si un directorio existe, si no existe lo crea, si no es posible crear el directorio devuelve falso.
    /// </summary>
    /// <param name="directorio"></param>
    /// <returns></returns>
    public static bool VerificarDirectorio(string directorio)
    {
        try
        {
            if (!Directory.Exists(directorio))
            {
                Directory.CreateDirectory(directorio);
            }
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static string EntidadAQueryString<T>(T entidad, params string[] parametrosAQueryString)
    {
        List<string> argumentos = new List<string>();
        string query = string.Empty;
        if (entidad != null && parametrosAQueryString != null && parametrosAQueryString.Length > 0)
        {
            foreach (var val in entidad.GetType().GetProperties())
            {
                string valor = string.Empty;
                if (parametrosAQueryString.Contains(val.Name))
                {
                    if (val.PropertyType == typeof(DateTime))
                    {
                        argumentos.Add(val.Name + "=" + DateTime.Parse(val.GetValue(entidad, null).ToString()).ToString("yyyy-MM-dd"));
                    }
                    else
                    {
                        argumentos.Add(val.Name + "=" + val.GetValue(entidad, null));
                    }
                }
            }
            query = string.Join("&", argumentos);
        }
        return query;
    }

    /// <summary>
    /// Escribe un log de la aplicación en un archivo de texto
    /// </summary>
    /// <param name="mensaje"></param>
    public static void EscribirLog(string mensaje)
    {
        string rutaLog = Path.Combine(AppSettings.DirectorioLog, nombreSistema);
        string nombreArchivo = string.Format("{0}_{1}.txt", nombreSistema, DateTime.Now.ToString("yyyyMMdd"));

        if (!string.IsNullOrEmpty(Path.GetExtension(rutaLog)))
            rutaLog = Path.GetDirectoryName(rutaLog);

        if (!string.IsNullOrEmpty(rutaLog))
        {
            var strFecha = DateTime.Now.Year.ToString() + "\\" + DateTime.Now.Month.ToString("00");
            rutaLog = string.Format("{0}\\{1}\\{2}", rutaLog, strFecha, nombreArchivo);
        }

        if (!Directory.Exists(Path.GetDirectoryName(rutaLog)))
            Directory.CreateDirectory(Path.GetDirectoryName(rutaLog));
        StringBuilder contenido = new StringBuilder();
        try
        {
            contenido.AppendLine(string.Format("[{0}]", DateTime.Now.ToString("HH:mm:ss")));
            contenido.AppendLine(mensaje);
            using (StreamWriter w = File.AppendText(rutaLog))
            {
                w.WriteLine(contenido.ToString());
            }
        }
        catch { }
    }

    /// <summary>
    /// Escribe un log de la aplicación en un archivo de texto
    /// </summary>
    /// <param name="mensaje"></param>
    /// <param name="stackTrace"></param>
    public static void EscribirLog(string mensaje, string stackTrace)
    {
        string rutaLog = Path.Combine(AppSettings.DirectorioLog, nombreSistema);
        string nombreArchivo = string.Format("{0}_{1}.txt", nombreSistema, DateTime.Now.ToString("yyyyMMdd"));

        if (!string.IsNullOrEmpty(Path.GetExtension(rutaLog)))
            rutaLog = Path.GetDirectoryName(rutaLog);

        if (!string.IsNullOrEmpty(rutaLog))
        {
            var strFecha = DateTime.Now.Year.ToString() + "\\" + DateTime.Now.Month.ToString("00");
            rutaLog = string.Format("{0}\\{1}\\{2}", rutaLog, strFecha, nombreArchivo);
        }

        if (!Directory.Exists(Path.GetDirectoryName(rutaLog)))
            Directory.CreateDirectory(Path.GetDirectoryName(rutaLog));
        StringBuilder contenido = new StringBuilder();
        try
        {
            contenido.AppendLine(string.Format("[{0}]", DateTime.Now.ToString("HH:mm:ss")));
            contenido.AppendLine(string.Format("Mensaje: {0}", mensaje));
            contenido.AppendLine(string.Format("StackTrace: {0}", stackTrace));
            using (StreamWriter w = File.AppendText(rutaLog))
            {
                w.WriteLine(contenido.ToString());
            }
        }
        catch { }
    }

    /// <summary>
    /// Obtiene el nombre del mes de una fecha
    /// </summary>
    /// <param name="fecha"></param>
    /// <returns></returns>
    public static string MesFecha(DateTime fecha)
    {
        string nombreMes = string.Empty;
        if (!fecha.Equals(new DateTime()))
        {
            int noMes = fecha.Month - 1;
            string[] meses = new string[] {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
                    "Septiembre", "Octubre", "Noviembre", "Diciembre" };
            nombreMes = meses[noMes];
        }
        return nombreMes;
    }

    public static string RemoverAcentos(this string text)
    {
        if (string.IsNullOrEmpty(text)) return text;
        var normalizedString = text.Normalize(NormalizationForm.FormD);
        var stringBuilder = new StringBuilder();

        foreach (var c in normalizedString)
        {
            var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
            if (unicodeCategory != UnicodeCategory.NonSpacingMark)
            {
                stringBuilder.Append(c);
            }
        }

        return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
    }

    public static string RemoverCarateresEsp(this string texto)
    {
        if (string.IsNullOrEmpty(texto)) return texto;
        char[] buffer = new char[texto.Length];
        int index = 0;
        foreach (char c in texto)
        {
            if (_lookup[c])
            {
                buffer[index] = c;
                index++;
            }
        }
        return new string(buffer, 0, index);
    }

    /// <summary>
    /// Crea una entidad con las propiedades contenidas en la entidad origen de tipo T
    /// </summary>
    /// <typeparam name="T">Entidad Destino</typeparam>
    /// <param name="entidad">Entidad Origen</param>
    /// <returns></returns>
    public static T Convertir<T>(this Object entidad)
    {
        if (entidad == null) return default(T);
        Type entidadType = entidad.GetType();
        Type destinoType = typeof(T);
        var entidadDestino = Activator.CreateInstance(destinoType, false);

        IEnumerable<MemberInfo> propEntidad = from p in entidadType.GetMembers().ToList()
                                              where p.MemberType == MemberTypes.Property
                                              select p;
        IEnumerable<MemberInfo> propDestino = from p in destinoType.GetMembers().ToList()
                                              where p.MemberType == MemberTypes.Property
                                              select p;
        List<MemberInfo> propiedades = propEntidad
            .Where(p => propDestino
                .Select(p1 => p1.Name).ToList()
                .Contains(p.Name))
            .ToList();

        PropertyInfo propiedad;
        object valor;
        propiedades.ForEach(p =>
        {
            propiedad = typeof(T).GetProperty(p.Name);
            valor = entidad.GetType().GetProperty(p.Name).GetValue(entidad, null);
            propiedad.SetValue(entidadDestino, valor, null);
        });
        return (T)entidadDestino;
    }

    public static string CargarCotenido(string rutaArchivo)
    {
        string contenido = string.Empty;
        if (string.IsNullOrEmpty(rutaArchivo)) return contenido;
        try
        {
            if (!File.Exists(rutaArchivo)) return contenido;
            contenido = File.ReadAllText(rutaArchivo, Encoding.GetEncoding("iso-8859-1"));
        }
        catch (Exception ex)
        {
            Utilidades.EscribirLog(ex.Message + Environment.NewLine + "Método: " + MethodBase.GetCurrentMethod().DeclaringType.Name + "\\" + MethodBase.GetCurrentMethod().Name +
                  Environment.NewLine + "Datos: " + JsonConvert.SerializeObject(rutaArchivo), JsonConvert.SerializeObject(ex));
        }
        return contenido;
    }
}
