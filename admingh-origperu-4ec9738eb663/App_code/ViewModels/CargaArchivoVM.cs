﻿public class CargaArchivoVM
{
    public string NombreArchivo { get; set; }
    public string TipoArchivo { get; set; }
    public string Contenido { get; set; }
    public int Tamanio { get; set; }
    public string Comentario { get; set; }
}