﻿/// <summary>
/// Clase generica para devolver las respuestas del metodo GenerarNotificacion en frmGestiones.aspx.cs
/// </summary>
namespace ViewModels
{
    public class GenerarNotificacionResultado
    {
        public byte[] Resultado;
        public bool Error;
        public string MensajeOperacion;

        public GenerarNotificacionResultado()
        {
            Resultado = null;
            Error = false;
            MensajeOperacion = string.Empty;
        }
    }
}