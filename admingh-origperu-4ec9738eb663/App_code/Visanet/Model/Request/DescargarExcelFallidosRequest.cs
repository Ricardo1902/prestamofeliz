﻿/// <summary>
/// Descripción breve de DescargarExcelFallidosRequest
/// </summary>
namespace Visanet.Model.Request
{
    public class DescargarExcelFallidosRequest
    {
        public int IdPagoMasivo { get; set; }
        public string NombreArchivo { get; set; }
    }
}