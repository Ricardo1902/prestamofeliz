﻿/// <summary>
/// Descripción breve de PagoRequest
/// </summary>
namespace Visanet.Model.Request
{
    public class GenerarPagoRequest
    {
        public int IdUsuario { get; set; }
        public string Cuenta { get; set; }
        public decimal Monto { get; set; }
        public string Tarjeta { get; set; }
        public int MesExp { get; set; }
        public int AnioExp { get; set; }
    }
}