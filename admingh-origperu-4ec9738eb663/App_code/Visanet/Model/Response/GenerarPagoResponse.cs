﻿using System;

namespace Visanet.Model.Response
{
    public class PagoResponse : Respuesta
    {
        public PagoResultado Resultado { get; set; }

        public PagoResponse()
        {
            Resultado = new PagoResultado();
        }
    }

    public class PagoResultado
    {
        public string IdTransaccion { get; set; }
        public DateTime FechaTransaccion { get; set; }
        public string Terminal { get; set; }
        public string NumeroRastreo { get; set; }
        public string Estatus { get; set; }
        public string CodigoAutorizacion { get; set; }
        public string DescripcionAcccion { get; set; }
    }
}