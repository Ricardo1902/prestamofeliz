﻿namespace Visanet.Model
{
    public class Respuesta
    {
        public bool Error { get; set; }
        public int StatusCode { get; set; }
        public string CodigoError { get; set; }
        public string MensajeOperacion { get; set; }
        public int TotalRegistros { get; set; }

        public Respuesta()
        {
            Error = false;
            StatusCode = 200;
        }
    }
}