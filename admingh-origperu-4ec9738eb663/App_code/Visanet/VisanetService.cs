﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Visanet.Model.Response;

namespace Visanet
{
    public static class VisanetService
    {
        public static HttpClient apiVisanet;
        public static string Url
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings.Get("WsPfVisanet");
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        static VisanetService()
        {
            apiVisanet = new HttpClient
            {
                BaseAddress = new Uri(Url)
            };
            apiVisanet.DefaultRequestHeaders.Accept.Clear();
            apiVisanet.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            apiVisanet.Timeout = new TimeSpan(0, 1, 0);
        }

        public static PagoResponse GenerarPagoVisanet(PagoRequest peticion)
        {
            PagoResponse respuestaPago = new PagoResponse();
            string urlMetodo = Url + "/v1/pagos";
            string strRespuesta = string.Empty;

            try
            {
                string contenido = JsonConvert.SerializeObject(peticion);
                var buffer = Encoding.UTF8.GetBytes(contenido);
                var byteContenido = new ByteArrayContent(buffer);
                byteContenido.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");

                HttpResponseMessage httpResponse = apiVisanet.PostAsync(urlMetodo, byteContenido).Result;
                strRespuesta = httpResponse.Content.ReadAsStringAsync().Result;
                respuestaPago = JsonConvert.DeserializeObject<PagoResponse>(strRespuesta);
            }
            catch (Exception e)
            {
                respuestaPago.Error = true;
                respuestaPago.MensajeOperacion = "Ocurrió un error al momento de intentar realizar el pago.";
            }

            return respuestaPago;
        }
    }
}