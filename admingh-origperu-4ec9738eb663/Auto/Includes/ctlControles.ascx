﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ctlControles.ascx.cs" Inherits="Includes_ctlControles" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<link href="/Auto/Includes/Styles.css" rel="stylesheet" />





<script type="text/javascript" language="javascript">
    $(function () {
        $('input').tooltip();
        $('select').tooltip();
    });
        

</script>

<style type="text/css">


</style>



 <asp:Label ID="TTitulo" runat="server" Text="" CssClass="Titulo"></asp:Label>
<div
    id="DTexbox"
    visible="false"
    runat="server">
    <asp:TextBox
        class="form-control"
        ID="TTextBox"
        runat="server"
        OnTextChanged="CambioTexto_Text"
        AutoPostBack="FALSE" />
</div>
<div id="DCombo" visible="false" runat="server">
    <asp:DropDownList
        ID="TCombo"
        runat="server"
        OnSelectedIndexChanged="CambioTexto_Combo"
        EmptyText="Seleccione"
        MenuWidth="300"
        AutoPostBack="FALSE"
        FilterType="Contains"
        class="form-control" />
</div>

<div id="DGrid" visible="false" runat="server">
    <asp:GridView name="Datos" ID="Tgrid" runat="server" AllowPaging="true" PageSize="30" CssClass="gridview" Style="overflow: auto;">
        <HeaderStyle CssClass="HeaderStyle" />
        <FooterStyle CssClass="FooterStyle" />
        <RowStyle CssClass="RowStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <PagerStyle CssClass="PagerStyle" />
    </asp:GridView>
</div>

<div id="DCompa" visible="false" runat="server" class="styled-select">
    <select name="TCompa" runat="server" id="TCompa" class="form-control"></select>
</div>

<div class="form-group">
<div
    id="DFecha"
    visible="false"
    class="input-group"
    runat="server"
    ondisposed="CambioTexto_Fecha">
    <asp:TextBox ID="TFecha" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
    <span class="input-group-addon">
        <asp:ImageButton ID="Image1" ImageUrl="/Auto/imgs/calendario.png" ImageAlign="Bottom"
            runat="server" />
        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" PopupButtonID="Image1" TargetControlID="TFecha" runat="server"
            Format="yyyy-MM-dd" CssClass="cal_Theme1">
        </ajaxToolkit:CalendarExtender>
    </span>
</div>
</div>
<div id="DXML"
    visible="false"
    runat="server">

    <asp:Label runat="server" ID="Label2" Text="Subir Archivo Con encabezado:" class="ctrl_label"></asp:Label>
    <asp:Label
        class="ctrl_tb8"
        ID="Columnas2" Visible="false"
        ReadOnly="true" 
        runat="server" />
    <br />
    <div class="form-group">
        <asp:FileUpload ID="TFileUpload" runat="server" class="upload col-xs-12 col-md-7 btn btn-primary btn-sm"></asp:FileUpload>
    </div>

    <div id="DivFile2" style="visibility: hidden; height: 80px; overflow: auto; display: none;">
        <asp:TextBox ID="TFile2" runat="server" TextMode="MultiLine" Text="Pegar Archivo con separador de comas" class="ctrl_File">
        </asp:TextBox>
        <asp:TextBox
            class="ctrl_tb7"
            ID="TCadena2"
            runat="server" />
        <input runat="server" id='SubirFile2' type='button' value='Subir' class='btn btn-primary' onclick="subir2" />
    </div>




    <br />

</div>
<div id="DFILE" visible="false"
    runat="server">


    <asp:Label runat="server" ID="Label1" Text="Subir Registros separado por coma cada columna y por un enter cada renglon Con encabezado:" class="ctrl_label"></asp:Label>
    <asp:TextBox
        class="ctrl_tb8"
        ID="Columnas" Visible="false"
        ReadOnly="true"
        runat="server" />
    <br />
    <asp:TextBox ID="TFile" runat="server" TextMode="MultiLine" Text="Pegar Archivo con separador de comas" class="ctrl_File">
    </asp:TextBox>
    <div id="DivFile" style="visibility: hidden; height: 80px; overflow: auto; display: none;">
        <asp:TextBox
            class="ctrl_tb7"
            ID="TCadena"
            runat="server" />
    </div>

    <input runat="server" id='SubirFile' type='button' value='Subir' class='btn btn-primary' onclick="subir" />
    <br />
</div>
<div id="DTextarea" visible="false" runat="server">
    <asp:TextBox
        class="form-control"
        ID="TTextarea"
        runat="server"
        OnTextChanged="CambioTexto_Text"
        AutoPostBack="FALSE" TextMode="MultiLine"/>
</div>
<div id="DTabla" visible="false" runat="server" class="table-responsive">
  <asp:Table ID="TTabla" runat="server" class="table table-bordered table-striped"></asp:Table>
</div>
<div id="DTextboxP" runat="server" visible="false">
  <asp:TextBox
        class="form-control"
        ID="TTextBoxP"
        runat="server"
        OnTextChanged="CambioTexto_Text"
        AutoPostBack="FALSE" TextMode="Password"/>
</div>
<div id="DArchivoDrive" visible="false" runat="server">
    <asp:FileUpload runat="server" ID="TArchivoDrive" class="upload col-xs-12 col-md-7 btn btn-primary btn-sm"/>
    <asp:HiddenField runat="server" ID="gdIDDirectorio" />
    <asp:HiddenField runat="server" ID="gdIDArchivo" />
    <asp:HiddenField runat="server" ID="gdURL" />
</div>

<%--     </td>
    </tr>
</table>
--%>