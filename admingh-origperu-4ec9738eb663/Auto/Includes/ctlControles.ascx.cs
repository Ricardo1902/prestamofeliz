﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;

using System.Xml;
using System.Text;
using System.IO;


public partial class Includes_ctlControles : System.Web.UI.UserControl
{
    cls_Rep Obj_Rep = new cls_Rep();

    #region Variables
    /*Variables de DS*/
    ///*Valores */
    ///*Salida*/
    #endregion
    #region Atributos
    #region Variables de DS
    private DataSet DS;
    public DataSet VDS
    {
        get { return DS; }
        set { DS = value; }
    }
    #endregion
    #region Valores

    private String ComboPadre;
    public String VComboPadre
    {
        get { return ComboPadre; }
        set { ComboPadre = value; }
    }


    private String Titulo;
    public String VTitulo
    {
        get { return Titulo; }
        set { Titulo = value; }
    }
    private String Nombre;
    public String VNombre
    {
        get { return Nombre; }
        set { Nombre = value; }
    }

    private String Tipo;
    public String VTipo
    {
        get { return Tipo; }
        set { Tipo = value; }
    }

    private String Orden;
    public String VOrden
    {
        get { return Orden; }
        set { Orden = value; }
    }

    private String ValorTipo;
    public String VValorTipo
    {
        get { return ValorTipo; }
        set { ValorTipo = value; }
    }

    private String ValorDefault;
    public String VValorDefault
    {
        get { return ValorDefault; }
        set { ValorDefault = value; }
    }

    private String VXML;
    public String VVXML
    {
        get { return VXML; }
        set { VXML = value; }
    }

    private String Vfile;
    public String VVfile
    {
        get { return Vfile; }
        set { Vfile = value; }
    }

    private String Vgrid;
    public String VVgrid
    {
        get { return Vgrid; }
        set { Vgrid = value; }
    }

    private int Longitud;
    public int VLongitud
    {
        get { return Longitud; }
        set { Longitud = value; }
    }

    private int Ancho;
    public int VAncho
    {
        get { return Ancho; }
        set { Ancho = value; }
    }

    private string FuncionJS;
    public string VFuncionJS
    {
        get { return FuncionJS; }
        set { FuncionJS = value; }
    }

    private string Parsley;
    public string VParsley
    {
        get { return Parsley; }
        set { Parsley = value; }
    }

    private bool SoloLectura;
    public bool VSoloLectura
    {
        get { return SoloLectura; }
        set { SoloLectura = value; }
    }



    #endregion
    #region Variables SP

    private String SP;
    public String VSP
    {
        get { return SP; }
        set { SP = value; }
    }

    private String SP_Tipo;
    public String VSP_Tipo
    {
        get { return SP_Tipo; }
        set { SP_Tipo = value; }
    }


    private String SP_VarCon;
    public String VSP_VarCon
    {
        get { return SP_VarCon; }
        set { SP_VarCon = value; }
    }
    #endregion
    #region Salida
    private String Valor;
    public String VValor
    {
        get { return Valor; }
        set { Valor = value; }
    }
    private String RID;
    public String VRID
    {
        get { return RID; }
        set { RID = value; }
    }



    #endregion
    #endregion

    public Includes_ctlControles()
    {
    }

    public Includes_ctlControles(string parameter1, string parameter2)
    {
        this.Tipo = parameter1;
        this.Valor = parameter2;
    }



    #region Load
    protected void Page_Load(object sender, EventArgs e)
    {
        {

            if ((Request.Params["__EVENTTARGET"] != "btnAgregaMasivo"))
            {
                /*Hace invisibles los divs*/
                this.DTexbox.Visible = false;
                this.DCombo.Visible = false;
                this.TTitulo.Text = Nombre.ToString();
                this.DXML.Visible = false;

                this.DTextarea.Visible = false;
                this.DTabla.Visible = false;                
                this.DTextboxP.Visible = false;
                
                string[] CadenaFuncionJS;
                string[] CadenaParsley;
                if (Tipo.ToUpper() != "XML" && Tipo.ToUpper() != "ARF")
                {
                    Par_SP_VARCON();
                }
                #region TipoControl
                switch (Tipo.ToUpper())
                {
                    #region TextBox
                    case "TXT":
                        this.DTexbox.Visible = true;
                        this.TTextBox.Attributes.Add("style", "text-transform: uppercase");
                        TTextBox.Text = TTextBox.Text.ToUpper();

                        if (VParsley != "" && VParsley != null)
                        {
                            CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                            string AtributoParsley;
                            string ValorParsley;
                            for (int i = 0; i < CadenaParsley.Length; i++)
                            {
                                string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                                AtributoParsley = RegParsley[0].ToString();
                                ValorParsley = RegParsley[1].ToString();
                                this.TTextBox.Attributes.Add(AtributoParsley, ValorParsley);

                            }
                        }

                        if (VFuncionJS != "" && VFuncionJS != null)
                        {
                            CadenaFuncionJS = VFuncionJS.Split(Convert.ToChar("§"));
                            string AtributoFuncionJS;
                            string ValorFuncionJS;
                            for (int i = 0; i < CadenaFuncionJS.Length; i++)
                            {
                                string[] RegFuncionJS = CadenaFuncionJS[i].Split(Convert.ToChar(","));
                                AtributoFuncionJS = RegFuncionJS[0].ToString();
                                ValorFuncionJS = RegFuncionJS[1].ToString();
                                this.TTextBox.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);
                            }
                        }
                        if (TTextBox.ID != RID)
                        {
                            RID = Titulo.ToString() + "_TTextBox";
                            TTextBox.ID = RID;
                            TTextBox.MaxLength = VLongitud;
                            if (VAncho > 0)
                            {
                                TTextBox.Width = VAncho;
                            }
                            string variJavaScript = "";
                        }
                        if (ValorDefault != null && VValorDefault != "")
                        {
                            TTextBox.Text = ValorDefault.ToString();
                            Valor = ValorDefault.ToString();
                        }

                        break;
                    #endregion
                    #region Combo
                    case "CMB":
                        this.DCombo.Visible = true;
                        RID = Titulo.ToString() + "_TCombo";
                        TCombo.Enabled = VSoloLectura;
                        TCombo.ID = RID;
                        if (SP != null)
                        {
                            string Text = SP + " " + SP_VarCon.ToString();
                            DS = Obj_Rep.Rep_Ejecuta(Text.ToString(), 0, Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(Session["SistemaId"].ToString()));

                            this.TCombo.DataSource = null;
                            this.TCombo.Items.Clear();
                            TCombo.DataBind();

                            if (VParsley != "" && VParsley != null)
                            {
                                CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                                string AtributoParsley;
                                string ValorParsley;
                                for (int i = 0; i < CadenaParsley.Length; i++)
                                {
                                    string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                                    AtributoParsley = RegParsley[0].ToString();
                                    ValorParsley = RegParsley[1].ToString();
                                    this.TCombo.Attributes.Add(AtributoParsley, ValorParsley);

                                }
                            }

                            if (VFuncionJS != "" && VFuncionJS != null)
                            {
                                CadenaFuncionJS = VFuncionJS.Split(Convert.ToChar("§"));
                                string AtributoFuncionJS;
                                string ValorFuncionJS;
                                for (int i = 0; i < CadenaFuncionJS.Length; i++)
                                {
                                    string[] RegFuncionJS = CadenaFuncionJS[i].Split(Convert.ToChar(","));
                                    AtributoFuncionJS = RegFuncionJS[0].ToString();
                                    ValorFuncionJS = RegFuncionJS[1].ToString();
                                    this.TCombo.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);

                                }
                            }

                            if (DS.Tables.Count != 0)
                            {
                                this.TCombo.DataSource = DS.Tables[0];
                                this.TCombo.DataTextField = DS.Tables[0].Columns[1].ColumnName;
                                this.TCombo.DataValueField = DS.Tables[0].Columns[0].ColumnName;
                                if (VAncho > 0)
                                {
                                    this.TCombo.Width = VAncho;
                                }
                                if (VValorDefault != null && VValorDefault != "")
                                {
                                    this.TCombo.SelectedValue = VValorDefault;
                                }
                                else
                                {
                                    this.TCombo.SelectedIndex = -1;
                                }

                                TCombo.DataBind();

                                Valor = this.TCombo.SelectedValue.ToString();
                            }
                        }
                        else
                        {
                            if (DS != null)
                            {

                                TCombo.DataSource = null;
                                TCombo.Items.Clear();
                                TCombo.DataBind();
                                TCombo.DataSource = this.DS.Tables[0];
                                TCombo.DataTextField = DS.Tables[0].Columns[1].ColumnName;
                                TCombo.DataValueField = DS.Tables[0].Columns[0].ColumnName;
                                if (VAncho > 0)
                                {
                                    TCombo.Width = VAncho;
                                }
                                TCombo.DataBind();


                                Valor = this.TCombo.SelectedValue.ToString();
                            }
                        }
                        break;
                    #endregion
                    #region Compa
                    case "CMP":
                        this.DCompa.Visible = true;
                        RID = Titulo.ToString() + "_TCompa";
                        TCompa.ID = RID;
                        TCompa.Disabled = VSoloLectura;
                        if (VFuncionJS != "" && VFuncionJS != null)
                        {
                            CadenaFuncionJS = VFuncionJS.Split(Convert.ToChar("§"));
                            string AtributoFuncionJS;
                            string ValorFuncionJS;
                            for (int i = 0; i < CadenaFuncionJS.Length; i++)
                            {
                                string[] RegFuncionJS = CadenaFuncionJS[i].Split(Convert.ToChar(","));
                                AtributoFuncionJS = RegFuncionJS[0].ToString();
                                ValorFuncionJS = RegFuncionJS[1].ToString();
                                this.TCompa.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);

                            }
                        }
                        if (VParsley != "" && VParsley != null)
                        {
                            CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                            string AtributoParsley;
                            string ValorParsley;
                            for (int i = 0; i < CadenaParsley.Length; i++)
                            {
                                string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                                AtributoParsley = RegParsley[0].ToString();
                                ValorParsley = RegParsley[1].ToString();
                                this.TCompa.Attributes.Add(AtributoParsley, ValorParsley);

                            }
                        }
                        if (VAncho > 0)
                        {
                            this.TCompa.Attributes.Add("style", "Width:" + VAncho + "px");
                        }
                        if (SP != null)
                        {
                            string Text = SP + " " + SP_VarCon.ToString();
                            DS = Obj_Rep.Rep_Ejecuta(Text.ToString(), 0, Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(Session["SistemaId"].ToString()));


                            Type cstype = this.GetType();

                            ClientScriptManager cs = Page.ClientScript;
                            StringBuilder sb = new StringBuilder();

                            string NC = Titulo.ToString() + "_" + RID.ToString();
                            string nombreScript = NC;



                            if (DS.Tables.Count != 0)
                            {
                                if (VComboPadre == "")
                                {
                                    sb.AppendLine(@"$(document).ready(function(){$(""#ctl00_MainContent_" + NC.ToString() + @""").empty();");
                                    for (int y = 0; y < DS.Tables[0].Rows.Count; y++)
                                    {
                                        if (VValorDefault == DS.Tables[0].Rows[y][0].ToString())
                                        {
                                            sb.AppendLine(@"$(""<option selected  value='" + DS.Tables[0].Rows[y][0].ToString() + @"'>" + DS.Tables[0].Rows[y][1].ToString() + @"</option>"").appendTo(""#ctl00_MainContent_" + NC.ToString() + @""");");
                                        }
                                        else
                                        {
                                            sb.AppendLine(@"$(""<option value='" + DS.Tables[0].Rows[y][0].ToString() + @"'>" + DS.Tables[0].Rows[y][1].ToString() + @"</option>"").appendTo(""#ctl00_MainContent_" + NC.ToString() + @""");");
                                        }
                                    }
                                }
                                else
                                {
                                    sb.AppendLine(@"
$(document).ready
    (
        function(){
                    $(""#ctl00_MainContent_" + VComboPadre.ToString() + "_" + VComboPadre.ToString() + @"_TCompa"").change
                        (
                            function(){
                                        var selected = $(""#ctl00_MainContent_" + VComboPadre.ToString() + "_" + VComboPadre.ToString() + @"_TCompa option:selected"");
                                        $(""#ctl00_MainContent_" + NC.ToString() + @""").empty();"
                                        );


                                    for (int y = 0; y < DS.Tables[0].Rows.Count; y++)
                                    {

                                        if (y != 0)
                                        {
                                            if (DS.Tables[0].Rows[y][2].ToString() != DS.Tables[0].Rows[y - 1][2].ToString())
                                            {
                                                sb.AppendLine(@"if(selected.val() == """ + DS.Tables[0].Rows[y][2].ToString() + @"""){");
                                            }
                                        }
                                        else
                                        {
                                            sb.AppendLine(@"if(selected.val() == """ + DS.Tables[0].Rows[y][2].ToString() + @"""){");
                                        }
                                        if (VValorDefault == DS.Tables[0].Rows[y][0].ToString())
                                        {

                                            sb.AppendLine(@"$(""<option  selected value='" + DS.Tables[0].Rows[y][0].ToString() + @"'>" + DS.Tables[0].Rows[y][1].ToString() + @"</option>"").appendTo(""#ctl00_MainContent_" + NC.ToString() + @""");");
                                        }
                                        else
                                        {
                                            sb.AppendLine(@"$(""<option value='" + DS.Tables[0].Rows[y][0].ToString() + @"'>" + DS.Tables[0].Rows[y][1].ToString() + @"</option>"").appendTo(""#ctl00_MainContent_" + NC.ToString() + @""");");
                                        }



                                        if (y == DS.Tables[0].Rows.Count - 1)
                                        {
                                            sb.AppendLine(@"}");
                                        }
                                        else
                                        {

                                            if (DS.Tables[0].Rows[y][2].ToString() != DS.Tables[0].Rows[y + 1][2].ToString())
                                            {
                                                sb.AppendLine(@"}");
                                            }
                                        }


                                    }
                                }
                                if (VValorDefault != null && VValorDefault != "")
                                {
                                    sb.AppendLine(@"$(""#ctl00_MainContent_" + NC.ToString() + @""").val(" + VValorDefault.ToString() + ")" + @"; $(""#ctl00_MainContent_" + NC.ToString() + @""").change();");
                                }
                                else
                                {
                                    sb.AppendLine(@"$(""#ctl00_MainContent_" + NC.ToString() + @""")[0].selectedIndex =0; $(""#ctl00_MainContent_" + NC.ToString() + @""").change();");
                                }

                                if (VComboPadre.ToString() == "")
                                { sb.AppendLine(@"});"); }
                                else
                                {
                                    if (VValorDefault != null && VValorDefault != "")
                                    {
                                        sb.AppendLine(@"});$(""#ctl00_MainContent_" + NC.ToString() + "_" + NC.ToString() + @"_TCompa"").val(" + VValorDefault.ToString() + ")" + @";

                                              $(""#ctl00_MainContent_" + VComboPadre.ToString() + "_" + VComboPadre.ToString() + @"_TCompa"").change();
                                              });");
                                    }
                                    else
                                    {
                                        sb.AppendLine(@"});$(""#ctl00_MainContent_" + VComboPadre.ToString() + "_" + VComboPadre.ToString() + @"_TCompa"")[0].selectedIndex = 0;

                                                $(""#ctl00_MainContent_" + VComboPadre.ToString() + "_" + VComboPadre.ToString() + @"_TCompa"").change();
                                                 });");
                                    }

                                }

                                if (!cs.IsStartupScriptRegistered(nombreScript))
                                {
                                    cs.RegisterStartupScript(cstype, nombreScript, sb.ToString(), true);
                                }
                            }
                        }
                        else
                        {
                            if (DS != null)
                            {

                                TCombo.DataSource = null;
                                TCombo.Items.Clear();
                                TCombo.DataBind();
                                TCombo.DataSource = this.DS.Tables[0];
                                TCombo.DataTextField = DS.Tables[0].Columns[1].ColumnName;
                                TCombo.DataValueField = DS.Tables[0].Columns[0].ColumnName;
                                if (VAncho > 0)
                                {
                                    TCombo.Width = VAncho;
                                }
                                TCombo.DataBind();


                                Valor = this.TCombo.SelectedValue.ToString();
                            }
                        }
                        break;
                    #endregion
                    #region Fecha
                    case "FCH":
                        DFecha.Visible = true;
                        RID = "";
                        RID = Titulo.ToString() + "_TFecha";
                        TTextBox.ID = RID;
                        TTextBox.ID = RID.ToString();
                        int anio = DateTime.Today.Year;
                        int mes = DateTime.Today.Month;
                        int Dia = DateTime.Today.Day;
                        string fecha = "";

                        fecha = anio.ToString() + "-";
                        if (mes < 10) { fecha = fecha.ToString() + "0" + mes.ToString() + "-"; } else { fecha = fecha.ToString() + mes.ToString() + "-"; }
                        if (Dia < 10) { fecha = fecha.ToString() + "0" + Dia.ToString(); } else { fecha = fecha.ToString() + Dia.ToString(); }

                        if (VFuncionJS != "" && VFuncionJS != null)
                        {
                            CadenaFuncionJS = VFuncionJS.Split(Convert.ToChar("§"));
                            string AtributoFuncionJS;
                            string ValorFuncionJS;
                            for (int i = 0; i < CadenaFuncionJS.Length; i++)
                            {
                                string[] RegFuncionJS = CadenaFuncionJS[i].Split(Convert.ToChar(","));
                                AtributoFuncionJS = RegFuncionJS[0].ToString();
                                ValorFuncionJS = RegFuncionJS[1].ToString();
                                this.TFecha.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);

                            }
                        }
                        if (VParsley != "" && VParsley != null)
                        {
                            CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                            string AtributoParsley;
                            string ValorParsley;
                            for (int i = 0; i < CadenaParsley.Length; i++)
                            {
                                string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                                AtributoParsley = RegParsley[0].ToString();
                                ValorParsley = RegParsley[1].ToString();
                                this.TFecha.Attributes.Add(AtributoParsley, ValorParsley);

                            }
                        }


                        if (ValorDefault != null && VValorDefault != "")
                        {
                            string fech;
                            TFecha.Text = Convert.ToString(VValorDefault);
                            fech = Convert.ToDateTime(TFecha.Text).Year + "-" + Convert.ToDateTime(TFecha.Text).Month + "-" + Convert.ToDateTime(TFecha.Text).Day;
                            TFecha.Text = fech.ToString();
                            Valor = fech.ToString();
                        }

                        Type cstype2 = this.GetType();

                        ClientScriptManager cs2 = Page.ClientScript;
                        StringBuilder sb2 = new StringBuilder();
                        string s = Titulo.ToString() + "Cal";


                        if (!cs2.IsStartupScriptRegistered(s))
                        {
                            cs2.RegisterStartupScript(cstype2, s.ToString(), sb2.ToString(), true);
                        }



                        break;
                    #endregion
                    #region XML
                    case "XML":
                        this.DXML.Visible = true;

                        if (TFile.ID != RID)
                        {
                            RID = Titulo.ToString() + "_TFile2";
                            TFile2.ID = RID;
                            SubirFile2.ID = Titulo.ToString() + "_SubirFile2";
                            Columnas2.Text = SP.ToString();
                            TCadena2.Text = SP.ToString();

                            if (VFuncionJS != "" && VFuncionJS != null)
                            {
                                CadenaFuncionJS = VFuncionJS.Split(Convert.ToChar("§"));
                                string AtributoFuncionJS;
                                string ValorFuncionJS;
                                for (int i = 0; i < CadenaFuncionJS.Length; i++)
                                {
                                    string[] RegFuncionJS = CadenaFuncionJS[i].Split(Convert.ToChar(","));
                                    AtributoFuncionJS = RegFuncionJS[0].ToString();
                                    ValorFuncionJS = RegFuncionJS[1].ToString();
                                    this.TFile2.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);

                                }
                            }
                            if (VParsley != "" && VParsley != null)
                            {
                                CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                                string AtributoParsley;
                                string ValorParsley;
                                for (int i = 0; i < CadenaParsley.Length; i++)
                                {
                                    string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                                    AtributoParsley = RegParsley[0].ToString();
                                    ValorParsley = RegParsley[1].ToString();
                                    this.TFile2.Attributes.Add(AtributoParsley, ValorParsley);

                                }
                            }

                            Type cstypef = this.GetType();

                            string Separador;

                            /* Codigo Jerry */
                            if (VValorDefault == "" || VValorDefault == null)
                                Separador   = "','";
                            else
                                Separador   = VValorDefault;
                            /* Fin codigo Jerry */

                            string nombreScriptf = Titulo.ToString();
                            ClientScriptManager csf = Page.ClientScript;
                            StringBuilder sbf = new StringBuilder();
                            sbf.AppendLine("document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + "_SubirFile2').onclick =" + Titulo.ToString() + "_Subir2;");

                            sbf.AppendLine("function " + Titulo.ToString() + "_Subir2(){ ");

                            sbf.AppendLine("var str0 ='" + SP_VarCon.ToString() + "';");
                            sbf.AppendLine("var P = str0.split(',');");
                            sbf.AppendLine("var str = document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_TCadena2').value;");
                            sbf.AppendLine("var str2 = document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + "_TFile2').value;");
                            sbf.AppendLine("var Campos =str.split(',');");
                            sbf.AppendLine(@"str2 = str2 + '\n';");
                            sbf.AppendLine(@"var Datos = str2.split(/\n/);");
                            sbf.AppendLine("var Xml ='<'+P[0]+'>';var error = 0;");
                            sbf.AppendLine(@" Msg='Datos Listos';");
                            sbf.AppendLine(@" Tipoalert=3;");
                            sbf.AppendLine(@" if(Datos.length-1==0)
                                                {
                                                    Xml=''; error=0;  Msg='Datos Listos, pero los envio vacios';  Tipoalert=2;
                                                }
                                                else{
                                                        for (i = 1; i < Datos.length-1; i++) 
                                                            {
                                                                // Codigo Jerry
                                                                var Columna = Datos[i].split(" + Separador.ToString() + @");
                                            
                                                                if(Columna=== undefined || Columna=='')
                                                                {
                                                
                                                                }
                                                                else {
                                                                         Xml=Xml+'<'+P[1]+'>';
                                                                         if (Campos.length== Columna.length)
                                                                          {
                                                                            for (j = 0; j < Columna.length; j++)
                                                                            {
                                                                                Xml=Xml+'<'+Campos[j]+'>'+Columna[j]+'</'+Campos[j]+'>' ; 
                                                                             }
                                                                           }
                                                                           else 
                                                                           {
                                                                                Msg='Tipo de Datos y Campos no coinciden';
                                                                                Tipoalert=1;
                                                                                Xml=Xml+'</renglon>';
                                                                                error=1
                                                                                break; 
                                                                           }
                                                                          Xml=Xml+'</'+P[1]+'>';
                                                                       }
                                                             }
                                                        Xml=Xml+'</'+P[0]+'>';
                                                    }");
                            sbf.AppendLine("document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + "_TFile2').value=Xml;");
                            sbf.AppendLine(@"if (error==1){document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + @"_TFile2').value='Error al subir Info';}
                                            else{document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + @"_TFile2').style.display = 'none';

                                                
                                                }");
                            sbf.AppendLine(" alert(Msg,Tipoalert); ");
                            sbf.AppendLine("}");


                            sbf.AppendLine("function " + Titulo.ToString() + @"_readSingleFile(evt) {
                                            //Retrieve the first (and only!) File from the FileList object
                                                        var f = evt.target.files[0];

                                                        if (f) {
                                                            var r = new FileReader();
                                                            r.onload = function (e) {
                                                                var contents = e.target.result;
                                                                document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + @"_TFile2').value = e.target.result;
                                                                " + Titulo.ToString() + @"_Subir2()
                                                            }
                                                            r.readAsText(f);
                                                        } else {
                                                            alert('Error al cargar el archivo',1);
                                                        }
                                                    }


                                                    document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_TFileUpload').addEventListener('change'," + Titulo.ToString() + @"_readSingleFile, false); ");
                            csf.RegisterStartupScript(cstypef, nombreScriptf, sbf.ToString(), true);



                        }
                        if (ValorDefault != null)
                        {
                            TFile2.Text = ValorDefault.ToString();
                            Valor = ValorDefault.ToString();
                        }
                        if (VParsley != "" && VParsley != null)
                        {
                            CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                            string AtributoParsley;
                            string ValorParsley;
                            for (int i = 0; i < CadenaParsley.Length; i++)
                            {
                                string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                                AtributoParsley = RegParsley[0].ToString();
                                ValorParsley = RegParsley[1].ToString();
                                string campo = "ctl00_MainContent_" + Titulo.ToString() + "_TFileUpload";
                                TFileUpload.Attributes.Add(AtributoParsley, ValorParsley);
                            }
                        }


                        Session["FILS"] = 1;
                        break;
                    #endregion
                    #region FIL
                    case "FIL":
                        this.DFILE.Visible = true;
                        if (TFile.ID != RID)
                        {
                            RID = Titulo.ToString() + "_TFile";
                            TFile.ID = RID;
                            SubirFile.ID = Titulo.ToString() + "_SubirFile";
                            Columnas.Text = SP.ToString();
                            TCadena.Text = SP.ToString();

                            if (VFuncionJS != "" && VFuncionJS != null)
                            {
                                CadenaFuncionJS = VFuncionJS.Split(Convert.ToChar("§"));
                                string AtributoFuncionJS;
                                string ValorFuncionJS;
                                for (int i = 0; i < CadenaFuncionJS.Length; i++)
                                {
                                    string[] RegFuncionJS = CadenaFuncionJS[i].Split(Convert.ToChar(","));
                                    AtributoFuncionJS = RegFuncionJS[0].ToString();
                                    ValorFuncionJS = RegFuncionJS[1].ToString();
                                    this.TFile.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);

                                }
                            }
                            if (VParsley != "" && VParsley != null)
                            {
                                CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                                string AtributoParsley;
                                string ValorParsley;
                                for (int i = 0; i < CadenaParsley.Length; i++)
                                {
                                    string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                                    AtributoParsley = RegParsley[0].ToString();
                                    ValorParsley = RegParsley[1].ToString();
                                    this.TFile.Attributes.Add(AtributoParsley, ValorParsley);

                                }
                            }

                            Type cstypef = this.GetType();

                            string nombreScriptf = Titulo.ToString();
                            ClientScriptManager csf = Page.ClientScript;
                            StringBuilder sbf = new StringBuilder();
                            sbf.AppendLine("document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + "_SubirFile').onclick =" + Titulo.ToString() + "_Subir;");
                            sbf.AppendLine("function " + Titulo.ToString() + "_Subir(){ ");

                            sbf.AppendLine("var str0 ='" + SP_VarCon.ToString() + "';");
                            sbf.AppendLine("var P = str0.split(',');");
                            sbf.AppendLine("var str = document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_TCadena').value;");
                            sbf.AppendLine("var str2 = document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + "_TFile').value;");
                            sbf.AppendLine("var Campos =str.split(',');");
                            sbf.AppendLine(@"str2 = str2 + '\n';");
                            sbf.AppendLine(@"var Datos = str2.split(/\n/);");
                            sbf.AppendLine("var Xml ='<'+P[0]+'>';var error = 0;");
                            sbf.AppendLine(@" Msg='Datos Listos';");
                            sbf.AppendLine(@" Tipoalert=3;");
                            sbf.AppendLine(@" if(Datos.length-1==0){Xml=''; error=0;  Msg='Datos Listos, pero los envio vacios'; Tipoalert=2}
                                                else{
                                                     for (i = 0; i < Datos.length-1; i++) 
                                                        {
                                                             var Columna = Datos[i].split(',');
                                                            if(Columna=== undefined || Columna=='')
                                                                {
                                                
                                                                }
                                                                else 
                                                                {
                                                                    Xml=Xml+'<'+P[1]+'>';
                                                                    if (Campos.length== Columna.length)
                                                                    {
                                                                        for (j = 0; j < Columna.length; j++)
                                                                        {
                                                                            Xml=Xml+'<'+Campos[j]+'>'+Columna[j]+'</'+Campos[j]+'>' ; 
                                                                        }
                                                                      }
                                                                   else 
                                                                    {
                                                                       Msg='Tipo de Datos y Campos no coinciden';
                                                                       Tipoalert=1;
                                                                       Xml=Xml+'</renglon>';
                                                                        error=1
                                                                       break; 
                                                                    }
                                                                Xml=Xml+'</'+P[1]+'>';
                                                            } 
                                                            Xml=Xml+'</'+P[0]+'>';
                                                        }");
                            sbf.AppendLine("document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + "_TFile').value=Xml;");
                            sbf.AppendLine(@"if (error==1){document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + @"_TFile').value='Error al subir Info';}
                                            else{document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + @"_TFile').style.display = 'none';
                                                 document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + @"_SubirFile').style.display = 'none';
                                                document.getElementById('ctl00_MainContent_" + Titulo.ToString() + @"_Columnas').value='Listo';
                                                }");
                            sbf.AppendLine(" alert(Msg, Tipoalert); ");
                            sbf.AppendLine("}");
                            csf.RegisterStartupScript(cstypef, nombreScriptf, sbf.ToString(), true);



                        }
                        if (ValorDefault != null)
                        {
                            TFile.Text = ValorDefault.ToString() + "\r";
                            Valor = ValorDefault.ToString() + "\r";
                        }



                        Session["FIL"] = 1;
                        break;
                    #endregion
                    #region TXT
                    case "FTX":
                        this.DXML.Visible = true;

                        if (TFile.ID != RID)
                        {
                            RID = Titulo.ToString() + "_TFile2";
                            TFile2.ID = RID;
                            SubirFile2.ID = Titulo.ToString() + "_SubirFile2";
                            Columnas2.Text = SP.ToString();
                            TCadena2.Text = SP.ToString();

                            if (VFuncionJS != "" && VFuncionJS != null)
                            {
                                CadenaFuncionJS = VFuncionJS.Split(Convert.ToChar("§"));
                                string AtributoFuncionJS;
                                string ValorFuncionJS;
                                for (int i = 0; i < CadenaFuncionJS.Length; i++)
                                {
                                    string[] RegFuncionJS = CadenaFuncionJS[i].Split(Convert.ToChar(","));
                                    AtributoFuncionJS = RegFuncionJS[0].ToString();
                                    ValorFuncionJS = RegFuncionJS[1].ToString();
                                    this.TFile2.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);

                                }
                            }
                            if (VParsley != "" && VParsley != null)
                            {
                                CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                                string AtributoParsley;
                                string ValorParsley;
                                for (int i = 0; i < CadenaParsley.Length; i++)
                                {
                                    string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                                    AtributoParsley = RegParsley[0].ToString();
                                    ValorParsley = RegParsley[1].ToString();
                                    this.TFile2.Attributes.Add(AtributoParsley, ValorParsley);

                                }
                            }

                            Type cstypef = this.GetType();

                            
                            string nombreScriptf = Titulo.ToString();
                            ClientScriptManager csf = Page.ClientScript;
                            StringBuilder sbf = new StringBuilder();
                            sbf.AppendLine("document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + "_SubirFile2').onclick =" + Titulo.ToString() + "_Subir2;");

                            sbf.AppendLine("function " + Titulo.ToString() + "_Subir2(){ ");
                            sbf.AppendLine("var str2 = document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + "_TFile2').value;");
                            
                            sbf.AppendLine(@"var Datos = str2");
                            sbf.AppendLine("var Xml ='';var error = 0;");
                            sbf.AppendLine(@" Msg='Datos Listos';");
                            sbf.AppendLine(@" Tipoalert=3;");
                            sbf.AppendLine(@" if(Datos.length-1==0)
                                                {
                                                    Xml=''; error=0;  Msg='Datos Listos, pero los envio vacios';  Tipoalert=2;
                                                }
                                                else{
                                                        Xml=Datos;
                                                    }");
                            sbf.AppendLine("document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + "_TFile2').value=Xml;");
                            sbf.AppendLine(@"if (error==1){document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + @"_TFile2').value='Error al subir Info';}
                                            else{document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + @"_TFile2').style.display = 'none';

                                                
                                                }");
                            sbf.AppendLine(" alert(Msg,Tipoalert); ");
                            sbf.AppendLine("}");


                            sbf.AppendLine("function " + Titulo.ToString() + @"_readSingleFile(evt) {
                                            //Retrieve the first (and only!) File from the FileList object
                                                        var f = evt.target.files[0];

                                                        if (f) {
                                                            var r = new FileReader();
                                                            r.onload = function (e) {
                                                                var contents = e.target.result;
                                                                document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_" + Titulo.ToString() + @"_TFile2').value = e.target.result;
                                                                " + Titulo.ToString() + @"_Subir2()
                                                            }
                                                            r.readAsText(f);
                                                        } else {
                                                            alert('Error al cargar el archivo',1);
                                                        }
                                                    }


                                                    document.getElementById('ctl00_MainContent_" + Titulo.ToString() + "_TFileUpload').addEventListener('change'," + Titulo.ToString() + @"_readSingleFile, false); ");
                            csf.RegisterStartupScript(cstypef, nombreScriptf, sbf.ToString(), true);



                        }
                        if (ValorDefault != null)
                        {
                            TFile2.Text = ValorDefault.ToString();
                            Valor = ValorDefault.ToString();
                        }



                        Session["FTX"] = 1;
                        break;
                    #endregion

                    #region Grid
                    case "GRI":
                        this.DGrid.Visible = true;
                        RID = Titulo.ToString() + "_TGrid";
                        Tgrid.ID = RID;
                        if (SP != null)
                        {
                            string Text = SP + " " + SP_VarCon.ToString();
                            DS = Obj_Rep.Rep_Ejecuta(Text.ToString(), 0, Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(Session["SistemaId"].ToString()));

                            this.Tgrid.DataSource = DS.Tables[0];
                            this.Tgrid.Columns.Clear();
                            Tgrid.DataBind();


                        }
                        else
                        {
                            if (DS != null)
                            {

                                TCombo.DataSource = null;
                                TCombo.Items.Clear();
                                TCombo.DataBind();
                                TCombo.DataSource = this.DS.Tables[0];
                                TCombo.DataTextField = DS.Tables[0].Columns[1].ColumnName;
                                TCombo.DataValueField = DS.Tables[0].Columns[0].ColumnName;
                                TCombo.DataBind();


                                Valor = this.TCombo.SelectedValue.ToString();
                            }
                        }
                        break;
                    #endregion
                    #region ArchivosFisicos
                    case "ARF":

                        this.DXML.Visible = true;
                        //Label2.Visible = false;
                        this.Label2.Attributes.Remove("");
                        if (TFile.ID != RID)
                        {
                            RID = Titulo.ToString() + "_TFile2";
                            TFile2.ID = RID;
                            SubirFile2.ID = Titulo.ToString() + "_SubirFile2";
                            Columnas2.Text = SP.ToString();
                            TCadena2.Text = SP.ToString();

                            if (VFuncionJS != "" && VFuncionJS != null)
                            {
                                CadenaFuncionJS = VFuncionJS.Split(Convert.ToChar("§"));
                                string AtributoFuncionJS;
                                string ValorFuncionJS;
                                for (int i = 0; i < CadenaFuncionJS.Length; i++)
                                {
                                  
                                    string[] RegFuncionJS = CadenaFuncionJS[i].Split(Convert.ToChar(","));
                                    AtributoFuncionJS = RegFuncionJS[0].ToString();
                                    ValorFuncionJS = RegFuncionJS[1].ToString();
                                    this.TFile2.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);
                                    TFileUpload.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);
                                }
                            }
                        }
                        if (ValorDefault != null)
                        {
                            TFile2.Text = ValorDefault.ToString();
                            Valor = ValorDefault.ToString();
                        }
                        if (VParsley != "" && VParsley != null)
                        {
                            CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                            string AtributoParsley;
                            string ValorParsley;
                            for (int i = 0; i < CadenaParsley.Length; i++)
                            {
                                string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                                AtributoParsley = RegParsley[0].ToString();
                                ValorParsley = RegParsley[1].ToString();
                                string campo = "ctl00_MainContent_" + Titulo.ToString() + "_TFileUpload";
                                TFileUpload.Attributes.Add(AtributoParsley, ValorParsley);
                            }
                        }



                        break;
                    #endregion

                    #region Textarea
                    case "TXA":
                      this.DTextarea.Visible = true;
                      this.TTextarea.Attributes.Add("style", "text-transform: uppercase");
                      TTextarea.Text = TTextarea.Text.ToUpper();

                      if (VParsley != "" && VParsley != null)
                      {
                        CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                        string AtributoParsley;
                        string ValorParsley;
                        for (int i = 0; i < CadenaParsley.Length; i++)
                        {
                          string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                          AtributoParsley = RegParsley[0].ToString();
                          ValorParsley = RegParsley[1].ToString();
                          this.TTextarea.Attributes.Add(AtributoParsley, ValorParsley);
                        }
                      }

                      if (VFuncionJS != "" && VFuncionJS != null)
                      {
                        CadenaFuncionJS = VFuncionJS.Split(Convert.ToChar("§"));
                        string AtributoFuncionJS;
                        string ValorFuncionJS;
                        for (int i = 0; i < CadenaFuncionJS.Length; i++)
                        {
                          string[] RegFuncionJS = CadenaFuncionJS[i].Split(Convert.ToChar(","));
                          AtributoFuncionJS = RegFuncionJS[0].ToString();
                          ValorFuncionJS = RegFuncionJS[1].ToString();
                          this.TTextarea.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);
                        }
                      }

                      if (TTextarea.ID != RID)
                      {
                        RID = Titulo.ToString() + "_TTextArea";
                        TTextarea.ID = RID;
                        TTextarea.MaxLength = VLongitud;
                        if (VAncho > 0)
                        {
                          TTextarea.Width = VAncho;
                        }
                      }

                      if (ValorDefault != null && VValorDefault != "")
                      {
                        TTextarea.Text = ValorDefault.ToString();
                        Valor = ValorDefault.ToString();
                      }

                      break;
                    #endregion

                    #region Tabla
                    case "TBL":
                      this.DTabla.Visible = true;
                      RID = Titulo.ToString() + "_TTabla";
                      TTabla.ID = RID;
                      if (SP != null)
                      {
                        string Text = SP + " " + SP_VarCon.ToString();
                        DS = Obj_Rep.Rep_Ejecuta(Text.ToString(), 0, Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(Session["SistemaId"].ToString()));
                      }
                      if(DS != null)
                      {
                        DataTable DTAux = DS.Tables[0];                        
                        TableHeaderRow tblTHR = new TableHeaderRow();

                        int intX = 0; int intY = 0;
                        for (intX = 0; intX <= DTAux.Columns.Count - 1; intX++)
                        {
                          TableHeaderCell tblTHC = new TableHeaderCell();
                          tblTHC.Text = DTAux.Columns[intX].ColumnName;
                          tblTHR.Cells.Add(tblTHC);
                        }
                        TTabla.Rows.Add(tblTHR);

                        for (intX = 0; intX <= DTAux.Rows.Count - 1; intX++)
                        {
                          TableRow tblTR = new TableRow();                          
                          tblTR.HorizontalAlign = HorizontalAlign.Left;
                          for (intY = 0; intY <= DTAux.Columns.Count - 1; intY++)
                          {
                            TableCell tblC = new TableCell();
                            tblC.Text = DTAux.Rows[intX][intY].ToString();
                            tblTR.Cells.Add(tblC);
                          }
                          TTabla.Rows.Add(tblTR);
                        }

                      }
                      break;
                    #endregion

                    #region TextPassword
                      case "TXP":
                        this.DTextboxP.Visible = true;
                        this.TTextBoxP.Attributes.Add("style", "text-transform: uppercase");
                        TTextBoxP.Text = TTextBoxP.Text.ToUpper();

                        if (VParsley != "" && VParsley != null)
                        {
                          CadenaParsley = VParsley.Split(Convert.ToChar("§"));
                          string AtributoParsley;
                          string ValorParsley;
                          for (int i = 0; i < CadenaParsley.Length; i++)
                          {
                            string[] RegParsley = CadenaParsley[i].Split(Convert.ToChar(","));
                            AtributoParsley = RegParsley[0].ToString();
                            ValorParsley = RegParsley[1].ToString();
                            this.TTextBoxP.Attributes.Add(AtributoParsley, ValorParsley);
                          }
                        }

                        if (VFuncionJS != "" && VFuncionJS != null)
                        {
                          CadenaFuncionJS = VFuncionJS.Split(Convert.ToChar("§"));
                          string AtributoFuncionJS;
                          string ValorFuncionJS;
                          for (int i = 0; i < CadenaFuncionJS.Length; i++)
                          {
                            string[] RegFuncionJS = CadenaFuncionJS[i].Split(Convert.ToChar(","));
                            AtributoFuncionJS = RegFuncionJS[0].ToString();
                            ValorFuncionJS = RegFuncionJS[1].ToString();
                            this.TTextBoxP.Attributes.Add(AtributoFuncionJS, ValorFuncionJS);
                          }
                        }
                        if (TTextBoxP.ID != RID)
                        {
                          RID = Titulo.ToString() + "_TTextboxP";
                          TTextBoxP.ID = RID;
                          TTextBoxP.MaxLength = VLongitud;
                          if (VAncho > 0)
                          {
                            TTextBoxP.Width = VAncho;
                          }
                          string variJavaScript = "";
                        }
                        if (ValorDefault != null && VValorDefault != "")
                        {
                          TTextBoxP.Text = ValorDefault.ToString();
                          Valor = ValorDefault.ToString();
                        }

                        break;
          #endregion

          #region ArchivoDrive
          case "AGD":
            this.DArchivoDrive.Visible = true;
            if(TArchivoDrive.ID != RID)
            {
              RID = Titulo.ToString() + "_TArchivoDrive";
              TArchivoDrive.ID = RID;
            }
            break;
          #endregion
        }
        #endregion
      }

        }
    }
    #endregion




    #region  CambioTexto

    public void CambioTexto_Text(object sender, EventArgs e)
    {

        Valor = this.TTextBox.Text.ToString();

    }
    public void CambioTexto_Combo(object sender, EventArgs e)
    {

        Valor = this.TCombo.SelectedValue.ToString();
    }
    public void CambioTexto_Fecha(object sender, EventArgs e)
    {
        Valor = this.TFecha.Text.ToString();
    }

    public void CambioTexto_TFile2(object sender, EventArgs e)
    {

        Valor = this.TFile2.Text.ToString();
    }

    #endregion


    #region FuncionCadenaCombo
    public void Par_SP_VARCON()
    {
        string parametros = SP_VarCon;

        if (parametros != null && parametros != "" && Tipo.ToUpper() != "FIL")
        {
            string[] Cadena;

            string VarCon = "";
            Cadena = parametros.Split(Convert.ToChar(","));

            for (int i = 0; i < Cadena.Length; i++)
            {

                string[] CF = Cadena[i].Split(Convert.ToChar("§"));

                for (int j = 0; j < CF.Length; )
                {

                    if (CF[j].ToUpper() == "INT" || CF[j].ToUpper() == "STR")
                    {
                        if (i == 0)
                        {
                            VarCon = VarCon;
                        }
                        else
                        {
                            VarCon = VarCon + ",";
                        }

                        if (CF[j].ToUpper() == "STR")
                        {
                            VarCon = VarCon + "'";
                        }


                        VarCon = VarCon + CF[j + 1].ToString();

                        if (CF[j].ToUpper() == "STR")
                        {
                            VarCon = VarCon + "'";
                        }
                    }

                    else
                    {
                        if (i == 0)
                        {
                            VarCon = VarCon;
                        }
                        else
                        {
                            VarCon = VarCon + ",";
                        }

                        if (CF[j].ToUpper() == "SST")
                        {
                            VarCon = VarCon + "'";
                        }


                        VarCon = VarCon + Session[CF[j + 1].ToString()].ToString();

                        if (CF[j].ToUpper() == "SST")
                        {
                            VarCon = VarCon + "'";
                        }
                    }
                    j = j + 2;
                }


            }
            SP_VarCon = VarCon;
        }
    }
  #endregion
  #region Eventos
  #endregion

  #region f(x) Drive
  public void set_gd_IDDirectorio(string _IDDirectorio)
  {
    _IDDirectorio = _IDDirectorio.Trim();
    gdIDDirectorio.Value = _IDDirectorio;
  }

  public string get_gd_IDDirectorio()
  {
    return gdIDDirectorio.Value;
  }

  public void set_gd_IDArchivo(string _IDArchivo)
  {
    _IDArchivo = _IDArchivo.Trim();
    gdIDArchivo.Value = _IDArchivo;
  }

  public string get_gd_IDArchivo()
  {
    return gdIDArchivo.Value;
  }

  public void set_gd_URL(string _URL)
  {
    _URL = _URL.Trim();
    gdURL.Value = _URL;
  }

  public string get_gd_URL()
  {
    return gdURL.Value;
  }
  #endregion

}
