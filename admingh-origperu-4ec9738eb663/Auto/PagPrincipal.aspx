﻿<%@ Page Title="" Language="C#" MasterPageFile="../Site.master" AutoEventWireup="true" CodeFile="PagPrincipal.aspx.cs" Inherits="PagPrincipal" EnableEventValidation="true" ValidateRequest="false" %>

<%@ Register Src="Includes/ctlControles.ascx" TagName="Autocontrol" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">


    <script type="text/javascript" src="Scripts/jquery.maskMoney.min.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.symbol.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.orderbars.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.stack.js"></script>
    <script type="text/javaScript" src="Scripts/Jquery/jquery-ui.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.debug.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.highlighter.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.spider.js"></script>
    <script type="text/javascript" src="Scripts/framework.js"></script>
    <script type="text/javascript" src="/js/JScriptAuto.js"></script>

    <script type="text/javascript">

        $(function () {
            $('input').tooltip();
            $('select').tooltip();                        
        });

        function verreporte(sender, selectedIndex) {
            var boton = document.getElementById('ctl00_MainContent_Reportess');
            boton.click();
        }

        function abrir() {
            document.location.reload()
        }
        var Base64 = {

            // private property
            _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

            // public method for encoding
            encode: function (input) {
                var output = "";
                var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                var i = 0;

                input = Base64._utf8_encode(input);

                while (i < input.length) {

                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);

                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;

                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                    this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                    this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

                }

                return output;
            },

            // public method for decoding
            decode: function (input) {
                var output = "";
                var chr1, chr2, chr3;
                var enc1, enc2, enc3, enc4;
                var i = 0;

                input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

                while (i < input.length) {

                    enc1 = this._keyStr.indexOf(input.charAt(i++));
                    enc2 = this._keyStr.indexOf(input.charAt(i++));
                    enc3 = this._keyStr.indexOf(input.charAt(i++));
                    enc4 = this._keyStr.indexOf(input.charAt(i++));

                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;

                    output = output + String.fromCharCode(chr1);

                    if (enc3 != 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 != 64) {
                        output = output + String.fromCharCode(chr3);
                    }

                }

                output = Base64._utf8_decode(output);

                return output;

            },

            // private method for UTF-8 encoding
            _utf8_encode: function (string) {
                string = string.replace(/\r\n/g, "\n");
                var utftext = "";

                for (var n = 0; n < string.length; n++) {

                    var c = string.charCodeAt(n);

                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    }
                    else if ((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                    else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }

                }

                return utftext;
            },

            // private method for UTF-8 decoding
            _utf8_decode: function (utftext) {
                var string = "";
                var i = 0;
                var c = c1 = c2 = 0;

                while (i < utftext.length) {

                    c = utftext.charCodeAt(i);

                    if (c < 128) {
                        string += String.fromCharCode(c);
                        i++;
                    }
                    else if ((c > 191) && (c < 224)) {
                        c2 = utftext.charCodeAt(i + 1);
                        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                        i += 2;
                    }
                    else {
                        c2 = utftext.charCodeAt(i + 1);
                        c3 = utftext.charCodeAt(i + 2);
                        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                        i += 3;
                    }

                }

                return string;
            }

        }

        $(window).load(function () {

            var elementos = $('.MaskLS');
            var size = elementos.size();


            $.each(elementos, function (i, val) {
                var objeto = '#' + $(val).attr('id');

                $(objeto).maskMoney('mask');
            });
        });


        $(document).ready(function () {
            $('#Button1').click(function () {
                $('#aspnetForm').parsley();
                if ($('#aspnetForm').parsley().validate() != true) {
                    alert("Favor de Validar los campos requeridos.", 1);
                    return false;
                }
                Isfileupload = '<%=Session["Isfileupload"]%>';

                var elementos = $('.MaskLS');
                var size = elementos.size();
                var texto;

                $.each(elementos, function (i, val) {
                    var objeto = '#' + $(val).attr('id');


                    texto2 = $(objeto).val(); // document.getElementById(objeto).value;
                    texto2 = texto2.replace(",", "");
                    $(objeto).val(texto2);


                });
                leerValores();

                var sp_exec = document.getElementById('<%= QueryExec.ClientID %>').value + document.getElementById('<%= scrip.ClientID %>').value;
                var valor = document.getElementById('<%= txtValor.ClientID %>').value;
                var TypPage = document.getElementById('<%= TypPage.ClientID %>').value;
                var ButonVal = $('#Button1').val();
                var OnSuccessGuardar = "";
                var errorGuardar = "";

                //alert(nombre);
                var data_a = { "sp_exec": sp_exec, "valor": valor, "pagina": TypPage, "ButonVal": ButonVal};
                if (TypPage == 12 || TypPage == 10 || Isfileupload) {

                    var boton = document.getElementById('ctl00_MainContent_Exc');
                    boton.click();
                    boton.click();
                }
                else {


                    $.ajax({
                        type: "POST",
                        url: "PagPrincipal.aspx/BtnClick",
                        data: JSON.stringify(data_a),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (JSON.stringify(msg.d["Consulta"]) == 1) {
                                window.location.href = "PagPrincipal.aspx?8UOSDB5CZ2=" + Base64.encode(valor).replace(new RegExp("=", "g"), '%3d') + "&ZQY4UEOQHR=" + Base64.encode(document.getElementById('<%= LblTitulo.ClientID %>').textContent).replace(new RegExp("=", "g"), '%3d');
                            } else if (JSON.stringify(msg.d["Exito"]) == 1) {                                
                                if (JSON.stringify(msg.d["PagRedirect"]) == 'null' || JSON.stringify(msg.d["PagRedirect"]) == '') {
                                  <%                                    
                                    if(Session["Modal"] == null)                                    
                                      Response.Write("alert(JSON.stringify(msg.d['Mensaje']), 0);");
                                    else
                                      Response.Write("alert(JSON.stringify(msg.d['Mensaje']), 5);");      
                                  %>                                  
                                } else {
                                    var TipoOpePagina = JSON.stringify(msg.d["TipoOpenPag"]).replace(/"/g, "");
                                    if (TipoOpePagina == 1) {
                                      <%                                    
                                        if(Session["Modal"] == null)                                    
                                          Response.Write("alert(JSON.stringify(msg.d['Mensaje']), 0);");
                                        else
                                          Response.Write("alert(JSON.stringify(msg.d['Mensaje']), 5);");      
                                      %>    
                                        var urlRedirect = JSON.stringify(msg.d["PagRedirect"]).replace(/"/g, "");
                                      <%
                                        if (Session["Modal"] == null)
                                          Response.Write(" window.location.href = (urlRedirect);");
                                      %>
                                    } else {                                      
                                        var urlRedirect = JSON.stringify(msg.d["PagRedirect"]).replace(/"/g, "");
                                        window.open(urlRedirect);
                                    }
                                }
                            } else {
                                alert(JSON.stringify(msg.d["Mensaje"]), 1);
                            }
                        },
                        error: function (result) {
                            alert("ERROR " + result.status + ' ' + result.statusText);
                        }
                    });
                }
            });
        });

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
   
        <asp:DropDownList ID="Reporte" Visible="false" runat="server" class="td7" EmptyText="Seleccionar" Width="200px" Height="30px" MenuWidth="300" OnSelectedIndexChanged="Ver" onchange="verreporte();" FilterType="Contains">
        </asp:DropDownList>
        <!----Controles Ocultos ---->
        <div id="DReload" runat="server" style="display: none;">
        </div>
        <div id="BotonReporte" style="display: none;">
            <asp:Button ID="Reportess" runat="server" OnClick="Ver"></asp:Button>
        </div>
        <div id="Divgrid" runat="server" style="display: none;">
            <asp:CheckBox ID="VerGrid" runat="server" Text="Ver en Grid" Font-Size="Small" />
        </div>
        <div id="load" style="display: none;" runat="server"></div>
        <div id="server_oculto" style="display: none;">
            <asp:TextBox ID="Archiv" runat="server" Text="" Style="display: none;"></asp:TextBox>
            <asp:TextBox ID="QueryExec" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="scrip" runat="server" Text="" TextMode="MultiLine" type="textarea"></asp:TextBox>
            <asp:TextBox ID="Exec" runat="server" Text="" TextMode="MultiLine" type="textarea"></asp:TextBox>
            <asp:Button ID="Exc" OnClick="ExportarExcel" runat="server"></asp:Button>
            <asp:TextBox ID="txtValor" runat="server" Text="0"></asp:TextBox>
            <asp:TextBox ID="TypPage" runat="server" Text="0"></asp:TextBox>
        </div>
        <div id="MakeConrtrols">
          
                        <div class="row panel panel-default">
                            <div class="panel-heading panel-heading-custom text-center">
                                <h3 class="panel-title">
                                    <asp:Label runat="server" Text="" ID="LblTitulo" ></asp:Label>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <!---->
                                <div id="Div_Controles" runat="server" style="overflow: auto; display: none; top: 0%; height: 101%" class="table-responsive;form-group">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel runat="server" ID="CControl" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="Controles" runat="server" HorizontalAlign="Left">
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!---->
                                <div id="RGrid" runat="server" style="overflow:hidden; display: none;margin-top:10px;">
                                <br />
                                  <div class="panel panel-default">
                                       <div class="panel panel-default">
                                            <div class="panel-heading panel-heading-custom text-center"><span class="panel-title"  runat="server" id="TituloResultado" ></span>
                                      </div>
                                      <div class="panel-body">
                                          <div id="variosGrid" runat="server" style="height: 50px; display: none;">
                                                  <table border="0" width="100%">
                                                      <tr>
                                                          <td style="width: 90px;">
                                                              <label class="labelgrid">Resultados</label></td>
                                                          <td style="width: 100px">
                                                              <asp:DropDownList ID="Resultados" runat="server" OnTextChanged="DatosGrid" Width="200px" Height="30px" AutoPostBack="true" CssClass="form-control">
                                                              </asp:DropDownList></td>
                                                          <td>
                                                              <span style="float: left;">
                                                                  <asp:Label ID="lblInfo" runat="server" /></span>
                                                              <span style="float: right;">Total Registros:
                                                              <asp:Label ID="lblTotalClientes" Font-Size="Medium" ForeColor="#798e94" Font-Bold="true" runat="server" CssClass="label label-info" /></span>
                                                          </td>
                                                      </tr>
                                                  </table>
                                              </div>                                            
                                          <div class="table-responsive" style="overflow:auto;">
                                            <asp:Label ID="lblMensajeConsulta" runat="server"></asp:Label>
                                            <asp:Table runat="server" ID="tblGeneral" class="table table-bordered table-striped">                                                        
                                            </asp:Table>
                                          </div>
                                          <div id="variosPagina" runat="server" style="height: 50px; display: none;" class="form-group">
                                              <table border="0" width="100%">
                                                  <tr>
                                                      <td style="width: 90px">
                                                          <label class="labelgrid">Ir a la pág.</label></td>
                                                      <td style="width: 100px">
                                                          <asp:DropDownList ID="Paginas" runat="server" OnTextChanged="DatosPage" Width="100px" Height="30px" AutoPostBack="true" CssClass="form-control">
                                                          </asp:DropDownList>

                                                          <td align="right" valign="middle">
                                                              <asp:Label Font-Size="Medium" ID="CurrentPageLabel" runat="server" ForeColor="#798e94" Font-Bold="true" CssClass="label label-info" />
                                                          </td>

                                                      </td>
                                                  </tr>
                                              </table>
                                          </div>                                                
                                      </div>
                                  </div>
                              </div>
                                <!---->
                                <div class='row'>

                                <asp:Label ID="LblError" runat="server" Test=""></asp:Label>
                            </div>
                                <!---->
                                <div class='row'>
                                &nbsp<br />
                            </div>

                            </div>
                        </div>
                        </div>
              
        </div>

        <script type="text/javascript" src="../js/JScriptAuto.js"></script>
  

    <div id="divCubrir"></div>

</asp:Content>