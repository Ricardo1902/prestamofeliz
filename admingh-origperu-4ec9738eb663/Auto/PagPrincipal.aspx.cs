﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using ASPSnippets.GoogleAPI;
using System.Web.Script.Serialization;
using System.Xml;
using System.Runtime.Serialization.Json;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Collections.Generic;

using System.Security.Cryptography.X509Certificates;
//using Google.Apis.Drive.v2;
//using Google.Apis.Auth.OAuth2;
//using Google.Apis.Util.Store;
//using Google.Apis.Services;
//using Google.Apis.Drive.v2.Data;
using System.Threading;


public partial class PagPrincipal : System.Web.UI.Page
{

    cls_Rep Obj_Rep = new cls_Rep();
    static SqlConnection conBd;

    List<string> lstRedirect = new List<string>();
    string[] arrayCampos;

    protected override void OnPreInit(EventArgs e)
    {
      ViewState["No_Accion"] = "0";
      base.OnPreInit(e);
      string strModal = Request.QueryString["Modal"];
      
      if (strModal != null || Session["Modal"] != null)
      {
        if (strModal == null) strModal = Session["Modal"].ToString();
        if (strModal == "1")
        {
          this.MasterPageFile = "~/SiteVacio.master";
          Session["Modal"] = "1";

          if (Request.QueryString["CT"] != null)
          {
            ViewState["Cargar_CTL_Tabla"] = Request.QueryString["CT"].ToString();
          }
          if (Request.QueryString["NA"] != null)
          {
            ViewState["No_Accion"] = Request.QueryString["NA"].ToString();
          }         
          
          if (Session["lstRedirect"] != null)
          {
            lstRedirect = (List<string>)Session["lstRedirect"];
            int intMaxURL = lstRedirect.Count;
            if(lstRedirect[intMaxURL-1] != Request.Url.ToString())
            {
              if (Request.QueryString["AS"] != null)
              {
                if (Request.QueryString["AS"].ToString() == "0")
                {
                  lstRedirect.Add(Request.Url.ToString());
                }
              }              
            }
          }
          else
          {
            if (Request.QueryString["AS"] != null)
            {
              if (Request.QueryString["AS"].ToString() == "0")
              {              
                lstRedirect.Add(Request.Url.ToString());
                Session["lstRedirect"] = lstRedirect;
              }
            }                                      
          }

        }
      }
    }   

    #region Load
    protected void Page_Load(object sender, EventArgs e)
    {
        readQS();
        Page.LoadComplete += new EventHandler(Page_LoadComplete);
        TituloResultado.InnerText = ConfigurationManager.AppSettings["TituloResultado"];
        
        int strcode = 0;
        if (Session["Isfileupload"] != null)
        {
            DataSet DS_Datos1 = new DataSet();
            // GOOGLE DRIVE START
            /*
            GoogleConnect.ClientId = "1075414528348-ah6tcb5kll371k1aadel7mcg4s0fv1tl.apps.googleusercontent.com";
            GoogleConnect.ClientSecret = "N6klFsCqB5eUfq5oLTB4khjL";
            GoogleConnect.RedirectUri = Request.Url.AbsoluteUri.Split('?')[0];
            GoogleConnect.API = EnumAPI.Drive;
            */
            // GOOGLE DRIVE END
            //if (!string.IsNullOrEmpty(Request.QueryString["code"]))
            //{
                strcode = 1;
                string code = Request.QueryString["code"];
              
                // GOOGLE DRIVE START
                /*
                string json = GoogleConnect.PostFile(code, (HttpPostedFile)Session["File"], Session["Description"].ToString());
                GoogleDriveFile file = (new JavaScriptSerializer()).Deserialize<GoogleDriveFile>(json);
                */
                // GOOGLE DRIVE END

                /*Google.Apis.Drive.v2.Data.File file = archivoGoogleDrive((HttpPostedFile)Session["File"]);
                if (file == null)
                {
                  return;
                }*/

                //string exec_val = "@Id_Code" + "=" + "'" + file.Id + "',";
                //exec_val = exec_val + "@Titulo_Archivo" + "=" + "'" + file.Title
                //    + "',"; // v2 File.Title
                //exec_val = exec_val + "@Nombre_ArchivoOri" + "=" + "'" + file.OriginalFilename + "',";
                //exec_val = exec_val + "@Link_Archivo" + "=" + "'" + file.ThumbnailLink + "',";
                //exec_val = exec_val + "@Icono_Archivo" + "=" + "'" + file.IconLink + "',";
                //exec_val = exec_val + "@ContenidoWeb_Link" + "=" + "'" + file.WebContentLink + "',";                

                //DateTime auxDateTime = (DateTime)file.CreatedDate;
                //exec_val = exec_val + "@Fecha_Creacion" + "=" + "'" + auxDateTime.ToString("yyyy-MM-dd HH:mm") + "',";
                //auxDateTime = (DateTime)file.ModifiedDate;
                //exec_val = exec_val + "@Fecha_Modificacion" + "=" + "'" + auxDateTime.ToString("yyyy-MM-dd HH:mm") + "',";

                //exec_val = exec_val + "@Consecutivo" + "=" + "'" + Convert.ToInt32(Session["Consecutivo_File"]) + "'";
                
                //DataSet Val_Reporte = new DataSet();
                //Val_Reporte = Obj_Rep.Rep_Actualiza(Convert.ToInt32(Session["Val_Reporte"]));
                //DS_Datos1 = Obj_Rep.Rep_Ejecuta(exec_val.ToString(), Convert.ToInt32(Session["Val_Reporte"]), Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(Session["SistemaId"].ToString()));
                //ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "$(function(){alert('Información actualizada con Exito.',0);});", true);                

                //Session.Remove("Isfileupload");
                //Session.Remove("VaribleXML");
                //Session.Remove("Val_Reporte");
                //Session.Remove("Consecutivo_File");
            //}
            if (Request.QueryString["error"] == "access_denied")
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Access denied.')", true);
            }
        }

        if (!Page.IsPostBack && strcode != 1)
        {
            Response.Buffer = true;

            load.Visible = false;

            string txml = "0";
            if (Session["XML"] != null)
            { txml = "1"; }


            if (!Page.IsPostBack)
            {

                Session["Archivo"] = "Reporte";
                DataSet Das_Reporte = new DataSet();
                int x = 0;
                if (Session["UsuarioId"] != null)
                {
                    x = Convert.ToInt32(Session["UsuarioId"].ToString());
                }
                int Sistema = Convert.ToInt32(Session["SistemaId"].ToString());

                int valor = 0;
                if (Request.QueryString.Count > 0)
                {
                
                    var ValorDes = Convert.FromBase64String(Request.QueryString["8UOSDB5CZ2"].ToString());
                    var strValor = Encoding.UTF8.GetString(ValorDes);
                    var DescripDes = Convert.FromBase64String(Request.QueryString["ZQY4UEOQHR"].ToString());
                    var strDescripDes = Encoding.GetEncoding("iso-8859-1").GetString(DescripDes);
                    valor = Convert.ToInt32(strValor);
                    LblTitulo.Text = strDescripDes;
                    txtValor.Text = strValor;
                    
                }



                Das_Reporte = Obj_Rep.Rep_Reporte(x, Sistema, valor);
                Session["TipoRep"] = Das_Reporte.Tables[1].Rows[0][0].ToString();

                Session["DS_Reporte"] = Das_Reporte;






                Reporte.DataSource = null;
                Reporte.Items.Clear();
                Reporte.DataBind();

                if (Das_Reporte.Tables[0].Rows.Count != 0)
                {
                    Reporte.DataSource = Das_Reporte.Tables[0];
                    Reporte.DataTextField = Das_Reporte.Tables[0].Columns[1].ColumnName;
                    Reporte.DataValueField = Das_Reporte.Tables[0].Columns[0].ColumnName;
                    Reporte.DataBind();
                    Reporte.Items.Insert(0, "Seleccione Reporte");
                    Session["DS_Ctrl"] = null;
                }

                if (valor != 0)
                {
                    Ver3(valor);
                }
            }
        }
        else
        {
            if (Session["Grids"] != null && Session["Grids"].ToString() != "0")
            {
                if (Session["Grids"] != null && Session["Grids"].ToString() != "0")
                {
                    if (Session["Grids"].ToString() == "7" || Session["Grids"].ToString() == "8" || Session["Grids"].ToString() == "9")
                    {
                        Session["Boton"] = "Guardar";
                    }
                    else
                    {
                        Session["Boton"] = "Generar";
                    }
                }

                if (ViewState["No_Accion"] != null)
                {
                  if (ViewState["No_Accion"].ToString() == "0")
                  {
                    this.Controles.Controls.Add(new LiteralControl("<br /> <div class='col-xs-12'><br /><input id='Button1' type='button' class='btn btn-primary' value='" + Session["Boton"].ToString() + "'  /> </div>"));
                  }
                }


            }
        }
    }
    #endregion



    public void Ver3(int valor)
    {
        Reporte.SelectedIndex = valor;
        ClientScript.RegisterStartupScript(GetType(), "VerReportes", "verreporte(null," + valor + ");", true);
    }


    public void Ver(object sender, EventArgs e)
    {


        {

            Divgrid.Style.Add("display", "none");
            int valor = 0;
            if (Request.QueryString.Count > 0)
            {
                var ValorDes = Convert.FromBase64String(Request.QueryString["8UOSDB5CZ2"].ToString());
                var strValor = Encoding.UTF8.GetString(ValorDes);
                valor = Convert.ToInt32(strValor);
            }
            if (valor != 0)
            {
                Archiv.Text = "";
                Divgrid.Style.Add("display", "none");
                Div_Controles.Style.Add("display", "block");
                Session["Grids"] = "0";

                DataSet Ds_Controles = new DataSet();
                int reporte = Convert.ToInt32(valor);
                Ds_Controles = Obj_Rep.Rep_Campos(reporte);

                string CampoNombre = "";
                int i = 0;
                string s = "";
                s = Ds_Controles.Tables[0].Rows[0]["Pagina_Id"].ToString();

                Session["Grid"] = s.ToString();
                string Pagina = (string)Session["Grid"];
                TypPage.Text = Pagina;

                Session["RegGrid"] = Ds_Controles.Tables[0].Rows[0]["RegGrid"].ToString();
                if (s.ToString() == "2")
                {
                    Divgrid.Style.Add("display", "block");
                }

                string VarCon = "";
                string TipoVariable = "";
                if (Ds_Controles != null)
                {

                    if (Ds_Controles.Tables[0].Rows.Count > 0)
                    {
                        #region CadenaEjecutar
                        if (Ds_Controles.Tables[0].Rows[0]["Nombre_Archivo"].ToString() != null && Ds_Controles.Tables[0].Rows[0]["Nombre_Archivo"].ToString() != "")
                        {
                            Session["Archivo"] = Ds_Controles.Tables[0].Rows[0]["Nombre_Archivo"].ToString() + String.Format("{0:yyyyMMdd}", System.DateTime.Now);
                        }
                        else
                        {
                            Session["Archivo"] = (string)Session["Archivo"] + String.Format("{0:yyyyMMdd}", System.DateTime.Now);
                        }

                        Session["TipoEncoding"] = Ds_Controles.Tables[0].Rows[0]["Encoding"].ToString();
                        CampoNombre = Ds_Controles.Tables[0].Rows[0]["Nombre_Archivo2"].ToString();

                        foreach (DataRow row in Ds_Controles.Tables[2].Rows)
                        {
                            VarCon = "";
                            TipoVariable = Ds_Controles.Tables[2].Rows[i]["TipoVariable"].ToString();
                            TipoVariable = TipoVariable.ToUpper();
                            if (TipoVariable == "INT" || TipoVariable == "STR" || TipoVariable == "NUL" || TipoVariable == "XML" || TipoVariable == "FIL" || TipoVariable == "FTX" || TipoVariable == "ARF")
                            {
                                if (i != 0)
                                {

                                    VarCon = VarCon + ",";

                                }

                                VarCon = VarCon + Ds_Controles.Tables[2].Rows[i]["Parametro"].ToString() + "=";

                                if (TipoVariable == "STR" || TipoVariable == "XML"
                                        )
                                {
                                    VarCon = VarCon + "'";
                                }

                                if (TipoVariable == "NUL")
                                {
                                    VarCon = VarCon + "Null";
                                }
                                else
                                {
                                    if (Ds_Controles.Tables[2].Rows[i]["Parametro"].ToString() == "@TipoAccion" && Session["DtsModificar"] != null)
                                    {
                                        VarCon = VarCon + "3";
                                    }
                                    else
                                    {
                                        if (Session["Modal"] != null)
                                        {
                                            if(ViewState["ReadOnly"] != null)                                            
                                                if(ViewState["ReadOnly"].ToString() == "3")
                                                    if(arrayCampos != null)
                                                        if(arrayCampos.Length > 0)
                                                        {
                                                            int x;
                                                            for(x = 0; x <= arrayCampos.Length - 1; x++)
                                                            {
                                                                if (Ds_Controles.Tables[2].Rows[i]["Titulo_Pagina"].ToString().ToUpper() == arrayCampos[x].ToString().ToUpper().Split('§')[0])
                                                                {
                                                                    VarCon += arrayCampos[x].ToString().Split('§')[1];
                                                                    break;
                                                                }
                                                            }
                                                        }                                            
                                        }
                                        else
                                            VarCon = VarCon + Ds_Controles.Tables[2].Rows[i]["Valor_Variable"];

                                    }
                                }

                                if (TipoVariable == "STR")
                                {
                                    VarCon = VarCon + "'";
                                }
                            }
                            else
                            {
                                if (i != 0)
                                {
                                    VarCon = VarCon + ",";


                                }

                                VarCon = VarCon + Ds_Controles.Tables[2].Rows[i]["Parametro"].ToString() + "=";
                                if (TipoVariable == "SSR" || TipoVariable == "XML"
                                    )
                                {
                                    VarCon = VarCon + "'";
                                }


                                VarCon = VarCon + Session[Ds_Controles.Tables[2].Rows[i]["Valor_Variable"].ToString()].ToString();

                                if (TipoVariable == "SSR")
                                {
                                    VarCon = VarCon + "'";
                                }
                            }
                            this.QueryExec.Text = this.QueryExec.Text + VarCon;
                            i = i + 1;


                        }
                        if (i != 0)
                        {
                            if (Ds_Controles.Tables[1].Rows.Count != 0)
                            {
                                this.QueryExec.Text = this.QueryExec.Text + ",";
                            }
                        }

                        #endregion
                        i = 0;
                        #region Agregar controles
                        this.Controles.Controls.Clear();

                        DataTable DTControl = new DataTable();
                        DTControl.Columns.Add("ID", typeof(string));
                        DTControl.Columns.Add("ValorTipo", typeof(string));
                        DTControl.Columns.Add("Tipo", typeof(string));
                        DTControl.Columns.Add("Parametro", typeof(string));
                        DTControl.Columns.Add("NombreArc", typeof(string));
                        DTControl.Columns.Add("Titulo", typeof(string));
                        string Carch = "";
                        this.Controles.Controls.Clear();





                        if (Pagina == "9")
                        {

                            if ((Ds_Controles.Tables[1].Rows[i]["Visible"].ToString() == "1" || Ds_Controles.Tables[1].Rows[i]["Visible"].ToString() == "2") && (Session["DtsModificar"] == null))
                            {
                                int j = i;
                                int rows = 0;
                                int rowsAnt = 0;
                                foreach (DataRow row in Ds_Controles.Tables[1].Rows)
                                {
                                    Carch = "";
                                    if (Ds_Controles.Tables[1].Rows[j]["Visible"].ToString() == "2")
                                    {
                                        Includes_ctlControles Control2 = new Includes_ctlControles();
                                        Control2 = (Includes_ctlControles)this.Page.LoadControl("/Auto/Includes/ctlControles.ascx");


                                        Control2.ID = Ds_Controles.Tables[1].Rows[j]["Titulo_Pagina"].ToString();
                                        Control2.VTitulo = Ds_Controles.Tables[1].Rows[j]["Titulo_Pagina"].ToString();
                                        Control2.VNombre = Ds_Controles.Tables[1].Rows[j]["Nombre"].ToString();
                                        Control2.VTipo = Ds_Controles.Tables[1].Rows[j]["Control"].ToString();
                                        Control2.VSP = Ds_Controles.Tables[1].Rows[j]["DataSet"].ToString();
                                        Control2.VSP_Tipo = "";
                                        Control2.VSP_VarCon = Ds_Controles.Tables[1].Rows[j]["DS"].ToString();
                                        Control2.VRID = "";
                                        Control2.VValorTipo = Ds_Controles.Tables[1].Rows[j]["TipoVariable"].ToString();
                                        Control2.VValorDefault = Ds_Controles.Tables[1].Rows[j]["Valor_Variable"].ToString();
                                        Control2.VComboPadre = Ds_Controles.Tables[1].Rows[j]["ComboPadre"].ToString();
                                        Control2.VLongitud = Convert.ToInt32(Ds_Controles.Tables[1].Rows[j]["Longitud"].ToString());
                                        Control2.VAncho = Convert.ToInt32(Ds_Controles.Tables[1].Rows[j]["Ancho"].ToString());
                                        Control2.VParsley = Ds_Controles.Tables[1].Rows[j]["Parsley"].ToString();
                                        Control2.VFuncionJS = Ds_Controles.Tables[1].Rows[j]["FuncionJS"].ToString();
                                        if (Ds_Controles.Tables[1].Rows[i]["Control"].ToString() == "CMB")
                                        {
                                            Control2.VSoloLectura = true;
                                        }
                                        else
                                        {
                                            Control2.VSoloLectura = false;
                                        }
                                        if (rows != Convert.ToInt32(Ds_Controles.Tables[1].Rows[j]["Renglon"].ToString()))
                                        {
                                            this.Controles.Controls.Add(new LiteralControl("<div class='row'>"));
                                            if (rows != rowsAnt)
                                            {
                                                this.Controles.Controls.Add(new LiteralControl("</div>"));
                                            }
                                        }

                                        if (Control2.VAncho == -1)
                                        {
                                            this.Controles.Controls.Add(new LiteralControl(" <div class='" + Ds_Controles.Tables[1].Rows[j]["Class"].ToString() + "'>"));
                                        }

                                        this.Controles.Controls.Add(Control2);
                                        this.Controles.Controls.Add(new LiteralControl(" </div>"));
                                        if (rows == Convert.ToInt32(Ds_Controles.Tables[1].Rows[j]["Renglon"].ToString()))
                                        {
                                            this.Controles.Controls.Add(new LiteralControl("</div>"));
                                        }
                                        rowsAnt = rows;
                                        rows = Convert.ToInt32(Ds_Controles.Tables[1].Rows[j]["Renglon"].ToString());

                                        if (Control2.VRID.ToString() != null && Control2.VRID.ToString() != "")
                                        {
                                            if (Ds_Controles.Tables[1].Rows[j]["Parametro"].ToString() == CampoNombre && CampoNombre != null && CampoNombre != "")
                                            {
                                                Carch = "X";
                                            }

                                            if (Control2.VTipo.ToString() == "TXT" || Control2.VTipo.ToString() == "TXA" || Control2.VTipo.ToString() == "TXP")
                                            {

                                                DTControl.Rows.Add(Control2.ID + '_' + Control2.VRID.ToString(), Control2.VValorTipo, Control2.VTipo.ToString(), Ds_Controles.Tables[1].Rows[j]["Parametro"].ToString(), Carch, Control2.VTitulo);
                                            }
                                            else
                                            {
                                                DTControl.Rows.Add(Control2.VRID.ToString(), Control2.VValorTipo, Control2.VTipo.ToString(), Ds_Controles.Tables[1].Rows[j]["Parametro"].ToString(), Carch, Control2.VTitulo);
                                            }
                                        }
                                    }
                                    j = j + 1;
                                }
                            }
                            else
                            {
                                int rows2 = 0;
                                int rowsAnt2 = 0;
                                DataSet DS_LLenaDatos = new DataSet();
                                DS_LLenaDatos = (DataSet)Session["DtsModificar"];
                                foreach (DataRow row in Ds_Controles.Tables[1].Rows)
                                {
                                    string orden = Ds_Controles.Tables[1].Rows[i]["Orden"].ToString();
                                    Carch = "";

                                    Includes_ctlControles Control2 = new Includes_ctlControles();
                                    Control2 = (Includes_ctlControles)this.Page.LoadControl("/Auto/Includes/ctlControles.ascx");

                                    Control2.ID = Ds_Controles.Tables[1].Rows[i]["Titulo_Pagina"].ToString();
                                    Control2.VTitulo = Ds_Controles.Tables[1].Rows[i]["Titulo_Pagina"].ToString();
                                    Control2.VNombre = Ds_Controles.Tables[1].Rows[i]["Nombre"].ToString();
                                    Control2.VTipo = Ds_Controles.Tables[1].Rows[i]["Control"].ToString();
                                    Control2.VSP = Ds_Controles.Tables[1].Rows[i]["DataSet"].ToString();
                                    Control2.VSP_Tipo = "";
                                    Control2.VSP_VarCon = Ds_Controles.Tables[1].Rows[i]["DS"].ToString();
                                    Control2.VRID = "";
                                    Control2.VValorTipo = Ds_Controles.Tables[1].Rows[i]["TipoVariable"].ToString();

                                    if (DS_LLenaDatos != null) //MSA, validación porque marca este dato null la primera vez
                                    {
                                        Control2.VValorDefault = DS_LLenaDatos.Tables[0].Rows[0][Control2.ID].ToString();
                                    }

                                    Control2.VComboPadre = Ds_Controles.Tables[1].Rows[i]["ComboPadre"].ToString();
                                    Control2.VLongitud = Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Longitud"].ToString());
                                    Control2.VAncho = Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Ancho"].ToString());
                                    Control2.VParsley = Ds_Controles.Tables[1].Rows[i]["Parsley"].ToString();
                                    Control2.VFuncionJS = Ds_Controles.Tables[1].Rows[i]["FuncionJS"].ToString();
                                    if (Ds_Controles.Tables[1].Rows[i]["Visible"].ToString() == "2")
                                    {
                                        if (Ds_Controles.Tables[1].Rows[i]["Control"].ToString() == "CMB")
                                        {
                                            Control2.VSoloLectura = false;
                                        }
                                        else
                                        {
                                            Control2.VSoloLectura = true;
                                        }
                                    }
                                    else
                                    {
                                        if (Ds_Controles.Tables[1].Rows[i]["Control"].ToString() == "CMB")
                                        {
                                            Control2.VSoloLectura = true;
                                        }
                                        else
                                        {
                                            Control2.VSoloLectura = false;
                                        }

                                    }

                                    if (rows2 != Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Renglon"].ToString()))
                                    {
                                        this.Controles.Controls.Add(new LiteralControl("<div class='row'>"));
                                        if (rows2 != rowsAnt2)
                                        {
                                            this.Controles.Controls.Add(new LiteralControl("</div>"));
                                        }
                                    }
                                    if (Control2.VAncho == -1)
                                    {
                                        this.Controles.Controls.Add(new LiteralControl(" <div class='" + Ds_Controles.Tables[1].Rows[i]["Class"].ToString() + "'>"));
                                    }
                                    this.Controles.Controls.Add(Control2);
                                    this.Controles.Controls.Add(new LiteralControl(" </div>"));
                                    if (rows2 == Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Renglon"].ToString()))
                                    {
                                        this.Controles.Controls.Add(new LiteralControl("</div>"));
                                    }
                                    rowsAnt2 = rows2;
                                    rows2 = Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Renglon"].ToString());

                                    if (Control2.VRID.ToString() != null && Control2.VRID.ToString() != "")
                                    {
                                        if (Ds_Controles.Tables[1].Rows[i]["Parametro"].ToString() == CampoNombre && CampoNombre != null && CampoNombre != "")
                                        {
                                            Carch = "X";
                                        }

                                        if (Control2.VTipo.ToString() == "TXT" || Control2.VTipo.ToString() == "TXA" || Control2.VTipo.ToString() == "TXP")
                                        {

                                            DTControl.Rows.Add(Control2.ID + '_' + Control2.VRID.ToString(), Control2.VValorTipo, Control2.VTipo.ToString(), Ds_Controles.Tables[1].Rows[i]["Parametro"].ToString(), Carch, Control2.VTitulo);

                                        }
                                        else
                                        {


                                            DTControl.Rows.Add(Control2.VRID.ToString(), Control2.VValorTipo, Control2.VTipo.ToString(), Ds_Controles.Tables[1].Rows[i]["Parametro"].ToString(), Carch, Control2.VTitulo);

                                        }
                                    }

                                    #region Visible
                                    #endregion
                                    i = i + 1;

                                }

                            }
                        }
                        else
                        //Alta
                        {
                            int rows3 = 0;
                            int rowsAnt3 = 0;

                            foreach (DataRow row in Ds_Controles.Tables[1].Rows)
                            {
                                Carch = "";

                                Includes_ctlControles Control2 = new Includes_ctlControles();
                                Control2 = (Includes_ctlControles)this.Page.LoadControl("/Auto/Includes/ctlControles.ascx");

                                Control2.ID = Ds_Controles.Tables[1].Rows[i]["Titulo_Pagina"].ToString();
                                Control2.VTitulo = Ds_Controles.Tables[1].Rows[i]["Titulo_Pagina"].ToString();
                                Control2.VNombre = Ds_Controles.Tables[1].Rows[i]["Nombre"].ToString();
                                Control2.VTipo = Ds_Controles.Tables[1].Rows[i]["Control"].ToString();
                                Control2.VSP = Ds_Controles.Tables[1].Rows[i]["DataSet"].ToString();
                                Control2.VSP_Tipo = "";
                                Control2.VSP_VarCon = Ds_Controles.Tables[1].Rows[i]["DS"].ToString();
                                Control2.VRID = "";
                                Control2.VValorTipo = Ds_Controles.Tables[1].Rows[i]["TipoVariable"].ToString();
                                Control2.VValorDefault = Ds_Controles.Tables[1].Rows[i]["Valor_Variable"].ToString();
                                Control2.VComboPadre = Ds_Controles.Tables[1].Rows[i]["ComboPadre"].ToString();
                                Control2.VLongitud = Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Longitud"].ToString());
                                Control2.VAncho = Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Ancho"].ToString());
                                Control2.VParsley = Ds_Controles.Tables[1].Rows[i]["Parsley"].ToString();
                                Control2.VFuncionJS = Ds_Controles.Tables[1].Rows[i]["FuncionJS"].ToString();

                                /*Cargar Tabla con ID*/
                                if (ViewState["Cargar_CTL_Tabla"] != null && Control2.VTipo == "TBL")
                                {
                                  Control2.VSP_VarCon = ViewState["Cargar_CTL_Tabla"].ToString();
                                }

                                if (Ds_Controles.Tables[1].Rows[i]["Control"].ToString() == "ARF")
                                {
                                    Session["RutaDestino"] = Ds_Controles.Tables[1].Rows[i]["DataSet"].ToString();
                                    Session["Isfileupload"] = "1";
                                    Session["VaribleXML"] = Ds_Controles.Tables[1].Rows[i]["Parametro"].ToString();
                                }

                                if (Ds_Controles.Tables[1].Rows[i]["Control"].ToString() == "CMB")
                                {
                                    Control2.VSoloLectura = true;
                                }
                                else
                                {
                                    Control2.VSoloLectura = false;
                                }


                                if (rows3 != Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Renglon"].ToString()))
                                {
                                    this.Controles.Controls.Add(new LiteralControl("<div class='row'>"));
                                    if (rows3 != rowsAnt3)
                                    {
                                        this.Controles.Controls.Add(new LiteralControl("</div>"));
                                    }
                                }

                                if (Control2.VAncho == -1)
                                {
                                    this.Controles.Controls.Add(new LiteralControl(" <div class='" + Ds_Controles.Tables[1].Rows[i]["Class"].ToString() + "'>"));
                                }
                                this.Controles.Controls.Add(Control2);
                                this.Controles.Controls.Add(new LiteralControl(" </div>"));

                                if (rows3 == Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Renglon"].ToString()))
                                {
                                    this.Controles.Controls.Add(new LiteralControl("</div>"));
                                }
                                rowsAnt3 = rows3;
                                rows3 = Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Renglon"].ToString());
                                if (Control2.VRID.ToString() != null && Control2.VRID.ToString() != "")
                                {
                                    if (Ds_Controles.Tables[1].Rows[i]["Parametro"].ToString() == CampoNombre && CampoNombre != null && CampoNombre != "")
                                    {
                                        Carch = "X";
                                    }

                                    if (Control2.VTipo.ToString() == "TXT" || Control2.VTipo.ToString() == "TXA" || Control2.VTipo.ToString() == "TXP")
                                    {

                                        DTControl.Rows.Add(Control2.ID + '_' + Control2.VRID.ToString(), Control2.VValorTipo, Control2.VTipo.ToString(), Ds_Controles.Tables[1].Rows[i]["Parametro"].ToString(), Carch, Control2.VTitulo);
                                    }
                                    else
                                    {
                                        DTControl.Rows.Add(Control2.VRID.ToString(), Control2.VValorTipo, Control2.VTipo.ToString(), Ds_Controles.Tables[1].Rows[i]["Parametro"].ToString(), Carch, Control2.VTitulo);
                                    }
                                }
                                i = i + 1;
                            }
                        }
                        int la = i;
                        Session["DS_Ctrl"] = DTControl;

                        #endregion
                        i = 0;
                        #region Funcionjava

                        Type cstype = this.GetType();
                        string nombreScript = "alertar";
                        ClientScriptManager cs = Page.ClientScript;
                        StringBuilder sb = new StringBuilder();

                        string cadena;
                        string TC;
                        string TV;
                        string cadenaF = "";

                        if (DTControl.Rows.Count > 0 || la == 0)
                        {
                            sb.AppendLine("function leerValores(){ ");

                            sb.AppendLine(" document.getElementById('ctl00_MainContent_scrip').value=\"\";");

                            sb.AppendLine("var coma= \",\";");
                            foreach (DataRow row in DTControl.Rows)
                            {

                                #region Tipo de control
                                if (DTControl.Rows[i]["ID"].ToString() != null && DTControl.Rows[i]["ID"].ToString() != "")
                                {
                                    TC = DTControl.Rows[i]["Tipo"].ToString();
                                    if (TC.ToUpper() == "TXT" || TC.ToUpper() == "FCH" || TC.ToUpper() == "CMB" || TC.ToUpper() == "CMP" || TC.ToUpper() == "XML" || TC.ToUpper() == "FIL" || TC.ToUpper() == "FTX" || TC.ToUpper() == "ARF" || TC.ToUpper() == "TXA" || TC.ToUpper() == "TXP")
                                    {

                                        TV = DTControl.Rows[i]["ValorTipo"].ToString();
                                        if (TV.ToUpper() == "STR" || TV.ToUpper() == "SSR" || TV.ToUpper() == "XML"
                                            )
                                        {
                                            cadena = "var s" + i + "= \"'\";";
                                            sb.AppendLine(cadena);
                                        }
                                        else
                                        {
                                            cadena = "var s" + i + "= \"\";";
                                            sb.AppendLine(cadena);
                                        }

                                        if (TC.ToUpper() == "CMB" || TC.ToUpper() == "CMP" || TC.ToUpper() == "FIL" || TV.ToUpper() == "XML" || TC.ToUpper() == "FTX" || TC.ToUpper() == "ARF")
                                        {

                                            if (TC.ToUpper() == "CMB" || TC.ToUpper() == "CMP")
                                            {
                                                cadena = " var c" + i + "=ctl00_MainContent_" + DTControl.Rows[i]["Titulo"].ToString() + "_" + DTControl.Rows[i]["ID"].ToString() + ".options[ctl00_MainContent_" + DTControl.Rows[i]["Titulo"].ToString() + "_" + DTControl.Rows[i]["ID"].ToString() + ".selectedIndex].value;";

                                            }
                                            else if (TC.ToUpper() == "ARF")
                                            {
                                                cadena = "var c" + i + "=document.getElementById('ctl00_MainContent_" + DTControl.Rows[i]["Titulo"].ToString() + "_TFileUpload" + "').files[0].name;";

                                            }
                                            else
                                            {
                                                cadena = "var c" + i + "=document.getElementById('ctl00_MainContent_" + DTControl.Rows[i]["Titulo"].ToString() + "_" + DTControl.Rows[i]["ID"].ToString() + "').value;";
                                            }
                                        }
                                        else
                                        {

                                            cadena = "var c" + i + "= document.getElementById('ctl00_MainContent_" + DTControl.Rows[i]["ID"].ToString() + "').value;";

                                        }
                                        sb.AppendLine(cadena);
                                        if (i == 0)
                                        {
                                            cadenaF = "\"" + DTControl.Rows[i]["Parametro"].ToString() + "=\"+" + "s" + i + "+" + "c" + i + "+" + "s" + i + " ";
                                        }
                                        else
                                        {
                                            cadenaF = cadenaF + "+ coma +" + "\"" + DTControl.Rows[i]["Parametro"].ToString() + "=\"+" + "s" + i + "+" + "c" + i + "+" + "s" + i + " ";
                                        }
                                        if (DTControl.Rows[i]["NombreArc"].ToString() == "X")
                                        {
                                            sb.AppendLine(" document.getElementById('ctl00_MainContent_Archiv').value=c" + i + ";");
                                        }
                                    }

                                }
                                #endregion
                                i = i + 1;
                            }
                            if (i == 0 || cadenaF == "")
                            {
                                sb.AppendLine("var c= \"\";");
                            }
                            else
                            {
                                sb.AppendLine("var c= " + cadenaF + ";");
                            }
                            sb.AppendLine(" document.getElementById('ctl00_MainContent_scrip').value=c;");

                            sb.AppendLine("var boton = document.getElementById('ctl00_MainContent_Exc');");
                            sb.AppendLine("document.getElementById('ctl00_MainContent_Divgrid').style.display='none';");
                            sb.AppendLine("document.getElementById('ctl00_MainContent_DReload').style.display='block';");

                            sb.AppendLine("}");
                            if (!cs.IsStartupScriptRegistered(nombreScript))
                            {
                                cs.RegisterStartupScript(cstype, nombreScript, sb.ToString(), true);
                            }


                            if (Pagina.ToString() != null)
                            {

                                if (Pagina.ToString() != "0")
                                {
                                    if (Pagina.ToString() == "7" || Session["Grids"].ToString() == "8" || Session["Grids"].ToString() == "9")
                                    {
                                        Session["Boton"] = "Guardar";
                                    }
                                    else if (Pagina.ToString() == "9" && Session["DtsModificar"] == null)
                                    {
                                        Session["Boton"] = "Consultar";
                                    }
                                    else if (Pagina.ToString() == "9" && Session["DtsModificar"] != null)
                                    {
                                        Session["Boton"] = "Modificar";
                                        Session["DtsModificar"] = null;

                                    }
                                    else
                                    {
                                        Session["Boton"] = "Generar";
                                    }
                                }
                                if (Session["Boton"] != null)
                                {
                                  if (ViewState["No_Accion"] != null)
                                  {
                                    if (ViewState["No_Accion"].ToString() == "0")
                                    {
                                      this.Controles.Controls.Add(new LiteralControl("</br><div class='col-xs-12'><br /><input id='Button1' type='button' value='" + Session["Boton"].ToString() + "' class='btn btn-primary' /></div> "));
                                    }
                                  }
                                }
                            }
                        }




                        #endregion

                    }
                }
            }
        }
    }

    public void ExportarExcel(object sender, EventArgs e)
    {
        load.Visible = true;
        Divgrid.Style.Add("display", "none");
        Session["Pagina"] = "1";
        string Pagina = (string)Session["Grid"];
        string FTP = (string)Session["TipoRep"];
        int valor = 0;
        if (Request.QueryString.Count > 0)
        {
            var ValorDes = Convert.FromBase64String(Request.QueryString["8UOSDB5CZ2"].ToString());
            var strValor = Encoding.UTF8.GetString(ValorDes);
            valor = Convert.ToInt32(strValor);
        }

        int R = Convert.ToInt32(valor);

        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
        DataSet ReporteFinal = new DataSet();
        string s = QueryExec.Text.ToString() + scrip.Text.ToString();


        if (Pagina.ToString() == "5" || Pagina.ToString() == "4")
        {
            DataSet DS_Datos2 = new DataSet();

            DS_Datos2 = Obj_Rep.Rep_Ejecuta(s, R, Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(Session["SistemaId"].ToString()));
            Session["DatosGrafica"] = null;
            Session["DatosGrafica"] = DS_Datos2;

        }
        else
        {
            if ((Pagina.ToString() != "3" && Pagina.ToString() != "6") && (VerGrid.Checked != true) && (FTP == "FTP"))
            {
                Pagina = "3";
            }


            if (Pagina.ToString() != "3" && Pagina.ToString() != "6")
            {
                if (VerGrid.Checked != true)
                {
                    DataSet p = new DataSet();
                    p = Obj_Rep.Rep_Actualiza(R);

                    #region Archivo
                    Divgrid.Style.Add("display", "none");
                    string strKey = ConfigurationManager.AppSettings["CADENACONEXIONSQL"].ToString();
                    var conString = ConfigurationManager.ConnectionStrings[strKey];
                    string strConnString = conString.ConnectionString;
                    conBd = new SqlConnection(conString.ConnectionString);
                    conBd.Open();

                    try
                    {
                        //  SQL insert for product row 
                        s = s.Replace("'", "''");
                        s =  ConfigurationManager.AppSettings["Esquema"].ToString() + ".Ejecuta" + "'" + s + "'" + "," + R.ToString() + "," + Convert.ToInt32(Session["UsuarioId"].ToString()) + "," + Convert.ToInt32(Session["SistemaId"].ToString());
                        SqlCommand comm = new SqlCommand(s, conBd);
                        comm.CommandTimeout = 999999;
                        comm.CommandType = CommandType.Text;
                        if (Session["Isfileupload"] == null)
                        {
                            int rows = comm.ExecuteNonQuery();
                        }
                        if (Session["Isfileupload"] != null)
                        {
                            HttpFileCollection files = Request.Files;
                            for (int i = 0; i <= files.Count - 1; i++)
                            {
                                HttpPostedFile postedFile = files[i];
                                if (postedFile.ContentLength > 0)
                                {
                                    Session["File"] = postedFile;
                                    Session["Description"] = "AAA";
                                    using (SqlDataReader dr = comm.ExecuteReader())
                                    {
                                        if (dr.FieldCount > 0)
                                        {
                                            if (dr.GetName(0) == "Consecutivo")
                                            {
                                                while (dr.Read())
                                                {

                                                    Session["Consecutivo_File"] = dr.GetValue(0);
                                                }
                                            }
                                        }
                                    }

                                    Session["Val_Reporte"] = R.ToString();
                                    //GOOGLE DRIVE GoogleConnect.Authorize("https://www.googleapis.com/auth/drive.file");
                                }
                            }
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('Información actualizada con Exito.',0);", true);
                        }
                        else {

                            using (SqlDataReader dr = comm.ExecuteReader())
                            {

                                StringBuilder sb = new StringBuilder();
                                int x = 1;
                                int y = 0;


                                string arch = "";
                                if (Archiv.Text == "")
                                {
                                    arch = (string)Session["Archivo"] + "_F" + x.ToString();
                                }
                                else
                                {
                                    arch = Archiv.Text + "_" + String.Format("{0:yyyyMMdd}", System.DateTime.Now);
                                }

                                do
                                {



                                    if (x == 1)
                                    {



                                        if (Pagina.ToString() != "10")
                                        {
                                            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.csv", arch));
                                        }
                                        else { HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.txt", arch)); }
                                        Encoding TipoEnconding = Encoding.GetEncoding(int.Parse(Session["TipoEncoding"].ToString()));

                                        HttpContext.Current.Response.ContentEncoding = TipoEnconding; // System.Text.Encoding.Default;
                                        HttpContext.Current.Response.Charset = "";

                                        HttpContext.Current.Response.ContentType = "application/vnd.xls";


                                    }
                                    for (int count = 0; count < dr.FieldCount; count++)
                                    {
                                        if (dr.GetName(count) != null)
                                            sb.Append(dr.GetName(count));
                                        if (count < dr.FieldCount - 1)
                                        {
                                            sb.Append(",");
                                        }
                                    }

                                    HttpContext.Current.Response.Write(sb.ToString() + "\n");
                                    HttpContext.Current.Response.Flush();

                                    ////Append Data

                                    sb = new StringBuilder();
                                    if (dr.HasRows)
                                    {

                                        while (dr.Read())
                                        {

                                            for (int col = 0; col < dr.FieldCount - 1; col++)
                                            {
                                                if (!dr.IsDBNull(col))
                                                    sb.Append(dr.GetValue(col).ToString().Replace(",", " "));
                                                sb.Append(",");
                                            }

                                            if (!dr.IsDBNull(dr.FieldCount - 1))
                                                sb.Append(dr.GetValue(
                                                dr.FieldCount - 1).ToString().Replace(",", " "));

                                            HttpContext.Current.Response.Write(sb.ToString() + "\n");
                                            sb.Clear();
                                            HttpContext.Current.Response.Flush();

                                        }
                                    }
                                    else
                                    {
                                        sb.Append("Sin Informacion");


                                        HttpContext.Current.Response.Write(sb.ToString() + "\n");
                                        sb.Clear();
                                        HttpContext.Current.Response.Flush();
                                    }


                                    Response.Write("\n");
                                    Response.Flush();

                                    sb.Clear();


                                    x = x + 1;
                                } while (dr.NextResult());
                                dr.Dispose();
                                HttpContext.Current.Response.Close();

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert(' Exception thrown: " + ex.Message + ". Favor de Dar de Alta Nuevamente los Archivos',4);", true);
                        return;
                    }

                    finally
                    {

                        conBd.Close();
                    }
                    #endregion
                }
                else
                {
                  RGrid.Style.Add("display", "block");
                  DataSet DS_Datos = new DataSet();
                  DS_Datos = Obj_Rep.Rep_Ejecuta(s, R, Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(Session["SistemaId"].ToString()));
                  Session["DatosGrid"] = DS_Datos;
                  Session["Grids"] = "1";

                  //CambiarPorTabla
                  //##### ##### ##### ##### #####
                  if (DS_Datos.Tables.Count >= 1)
                  {
                    DataTable DTAux = DS_Datos.Tables[0];

                    int intRegistros = DTAux.Rows.Count;
                    int intPorPagina = Convert.ToInt32(Session["RegGrid"].ToString());

                    if (intRegistros != 0)
                    {

                      if (DTAux.Rows.Count < intPorPagina)
                        intPorPagina = DTAux.Rows.Count;

                      int intPaginas = intRegistros / intPorPagina + (intRegistros % intPorPagina > 0 ? 1 : 0);
                      lblTotalClientes.Text = intRegistros.ToString();

                      TableHeaderRow tblTHR = new TableHeaderRow();
                      tblTHR.CssClass = "info";

                      int intX = 0; int intY = 0;
                      for (intX = 0; intX <= DTAux.Columns.Count - 1; intX++)
                      {
                        TableHeaderCell tblTHC = new TableHeaderCell();
                        tblTHC.Text = DTAux.Columns[intX].ColumnName;
                        tblTHR.Cells.Add(tblTHC);
                      }
                      tblGeneral.Rows.Add(tblTHR);


                      for (intX = 0; intX <= intPorPagina - 1; intX++)
                      {
                        TableRow tblTR = new TableRow();
                        tblTR.CssClass = "headerGrid";
                        tblTR.HorizontalAlign = HorizontalAlign.Left;
                        for (intY = 0; intY <= DTAux.Columns.Count - 1; intY++)
                        {
                          TableCell tblC = new TableCell();
                          tblC.Text = DTAux.Rows[intX][intY].ToString();
                          tblTR.Cells.Add(tblC);
                        }
                        tblGeneral.Rows.Add(tblTR);
                      }

                      if (DS_Datos.Tables.Count > 1)
                      {

                        if (DS_Datos.Tables[DS_Datos.Tables.Count - 1].Columns.Contains("ResultadosDS"))
                        {
                          for (intX = 0; intX <= DS_Datos.Tables[DS_Datos.Tables.Count - 1].Rows.Count - 1; intX++)
                          {
                            Resultados.Items.Insert(intX, DS_Datos.Tables[DS_Datos.Tables.Count - 1].Rows[intX]["ResultadosDS"].ToString());
                          }
                        }
                        else
                        {
                          for (intX = 0; intX <= DS_Datos.Tables.Count - 1; intX++)
                          {
                            Resultados.Items.Insert(intX, "Resultado " + (intX + 1).ToString());
                          }
                        }


                        Resultados.Items[0].Selected = true;
                        variosGrid.Style.Add("display", "block");
                      }

                      for (intX = 0; intX <= intPaginas - 1; intX++)
                      {
                        Paginas.Items.Insert(intX, "Página " + (intX + 1).ToString());
                      }
                      variosPagina.Style.Add("display", "block");
                      Paginas.Items[0].Selected = true;

                      CurrentPageLabel.Text = "Página " + 1 + " de " + intPaginas;
                    }
                    else
                    {
                      lblMensajeConsulta.Text = "No hay resultados";
                    }
                  }
                  return;
                  //##### ##### ##### ##### ##### CambiarPorTabla
                }
              }
            else
            {
                #region Generar XML
                DataSet DS_Datos = new DataSet();


                DS_Datos = Obj_Rep.Rep_ArchivoXML(Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(valor), s.ToString());


                if (DS_Datos != null)
                {
                    string ruta = DS_Datos.Tables[0].Rows[0][2].ToString() + DS_Datos.Tables[0].Rows[0][1].ToString();
                    string XmlText = DS_Datos.Tables[0].Rows[0][0].ToString();


                    System.IO.StreamWriter csvFileWriter = new StreamWriter(DS_Datos.Tables[0].Rows[0][2].ToString() + DS_Datos.Tables[0].Rows[0][1].ToString(), false);
                    csvFileWriter.WriteLine(XmlText);
                    csvFileWriter.Flush();
                    csvFileWriter.Close();


                }
                #endregion

            }
        }
        this.Controles.Controls.Clear();
        Session.Remove("Isfileupload");

        if (Pagina.ToString() != "2" && Pagina.ToString() != "5" && Pagina.ToString() != "4")
        {
            load.Visible = false;
            Divgrid.Style.Add("display", "none");
        }
        else
        {
            Divgrid.Style.Add("display", "none");
        }

    }

    public void DatosPage(object sender, EventArgs e)
    {
        DataSet DS_Datos = new DataSet();
        DS_Datos = (DataSet)Session["DatosGrid"];
        int rowsPage = Convert.ToInt32(Session["RegGrid"].ToString());

    }

    public void DatosGrid(object sender, EventArgs e)
    {

        int rowsPage = Convert.ToInt32(Session["RegGrid"].ToString());
        int NPagina = 1;

        int Z = 0;
        ListItem item = new ListItem();


        int RegIni = (((NPagina - 1) * rowsPage) + 1) - 1;
        int RegFin = ((NPagina) * rowsPage) - 1;


    }
    public class Employee
    {
        public int Exito { get; set; }
        public string Mensaje { get; set; }
        public int Consulta { get; set; }
        public string PagRedirect { get; set; }
        public string TipoOpenPag { get; set; }
    }

    [System.Web.Services.WebMethod]
    public static Employee BtnClick(object sp_exec, object valor, object pagina, object ButonVal)
    {

        Employee objEmp = new Employee();
        DataTable TB_Datos1 = new DataTable();
        cls_Rep Obj_Rep = new cls_Rep();
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
        DataSet ReporteFinal = new DataSet();

        String Ruta = (String)HttpContext.Current.Session["RutaDestino"];
        DataSet p = new DataSet();
        p = Obj_Rep.Rep_Actualiza(Convert.ToInt32(valor));

        DataSet DS_Datos1 = new DataSet();
        objEmp.Consulta = 0;
        sp_exec = sp_exec.ToString().ToUpper();
        try
        {
            String exec = sp_exec.ToString();
            int val = Convert.ToInt32(valor);
            int idU = Convert.ToInt32(HttpContext.Current.Session["UsuarioId"].ToString());
            DS_Datos1 = Obj_Rep.Rep_Ejecuta(sp_exec.ToString(), Convert.ToInt32(valor), Convert.ToInt32(HttpContext.Current.Session["UsuarioId"].ToString()), Convert.ToInt32(HttpContext.Current.Session["SistemaId"].ToString()));

            if (pagina.ToString() == "9" && DS_Datos1.Tables.Count > 0)
            {
                if (ButonVal.ToString().Trim() == "Consultar")
                {
                    objEmp.Exito = 2;
                    objEmp.Mensaje = "";
                    objEmp.Consulta = 1;
                    HttpContext.Current.Session["DtsModificar"] = DS_Datos1;
                }
                else
                {
                    objEmp.Exito = 1;
                    objEmp.Mensaje = DS_Datos1.Tables[0].Rows[0][0].ToString();
                }
            }
            else
            {
                objEmp.Exito = 1;
                if (DS_Datos1.Tables.Count > 0)
                {
                    TB_Datos1 = DS_Datos1.Tables[0];                    

                    if (TB_Datos1.Columns.Contains("Mail"))
                    {
                      string txtTexto;
                      System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
                      correo.From = new System.Net.Mail.MailAddress(TB_Datos1.Rows[0]["De"].ToString());
                      correo.To.Add(TB_Datos1.Rows[0]["Para"].ToString());
                      correo.Subject = "@SDP@ " + TB_Datos1.Rows[0]["Titulo"].ToString();
                      txtTexto = TB_Datos1.Rows[0]["Mail"].ToString();
                      correo.Body = txtTexto.ToString();
                      correo.IsBodyHtml = true;
                      correo.Priority = System.Net.Mail.MailPriority.Normal;
                      //
                      System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                      //
                      //---------------------------------------------
                      // Estos datos debes rellanarlos correctamente
                      //---------------------------------------------
                      smtp.Host = "smtpout.secureserver.net";

                      smtp.Credentials = new System.Net.NetworkCredential(TB_Datos1.Rows[0]["EmailCuenta"].ToString(), TB_Datos1.Rows[0]["EmailPass"].ToString());
                      smtp.EnableSsl = false;
                      try
                      {
                        smtp.Send(correo);
                        txtTexto = "Mensaje enviado satisfactoriamente";
                      }
                      catch (Exception ex)
                      {
                        txtTexto = "ERROR: " + ex.Message;

                      }


                    }


                    if (TB_Datos1.Columns.Contains("Err_Message"))
                    {
                        objEmp.Mensaje = TB_Datos1.Rows[0]["Err_Message"].ToString();
                    }
                    else {
                        objEmp.Mensaje = "El Registro Se Realizó con Exito";
                    }
                    if (TB_Datos1.Columns.Contains("UrlRedirect"))
                    {
                        objEmp.PagRedirect = TB_Datos1.Rows[0]["UrlRedirect"].ToString();
                    }
                    if (TB_Datos1.Columns.Contains("TipoUrl"))
                    {
                        objEmp.TipoOpenPag = TB_Datos1.Rows[0]["TipoUrl"].ToString();
                    }
                    else
                    {
                        objEmp.PagRedirect = null;
                    }

                }
                else
                {

                    objEmp.Mensaje = "El Registro Se Realizó con Exito";
                    objEmp.PagRedirect = null;
                }
            }
        }
        catch (Exception ex)
        {
            objEmp.Exito = 0;
            objEmp.Mensaje = "Error: " + ex.Message;
        }

        return objEmp;
    }

    protected void Btn_Actualiza(object sender, EventArgs e)
    {
        Response.Redirect("../Principal.aspx");
    }

    public class GoogleDriveFile
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string OriginalFilename { get; set; }
        public string ThumbnailLink { get; set; }
        public string IconLink { get; set; }
        public string WebContentLink { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public string CreateXML(Object YourClassObject)
    {
        XmlDocument xmlDoc = new XmlDocument(); 
        XmlSerializer xmlSerializer = new XmlSerializer(YourClassObject.GetType());
        using (MemoryStream xmlStream = new MemoryStream())
        {
            xmlSerializer.Serialize(xmlStream, YourClassObject);
            xmlStream.Position = 0;
            xmlDoc.Load(xmlStream);
            return xmlDoc.InnerXml;
        }
    }
    
    public void readQS()
    {
        if (!IsPostBack) return;
        if (Session["Modal"] == null) return;

        try
        {
            int x;
            int y;          
            string AutoSubmit;
            string ReadOnly;

            for (x = 0; x <= Request.QueryString.Count - 1; x++)
            {
                if (Request.QueryString.GetKey(x).ToString() == "Modal")
                    break;
            }

            // LAS INDIVIDUALES START
            x++;
            if (Request.QueryString.GetKey(x).ToString() == "AS")
            {
                AutoSubmit = Request.QueryString["AS"].ToString();
                ViewState["AutoSubmit"] = AutoSubmit;
            }
            else
                ViewState["AutoSubmit"] = 0;

            x++;
            if (Request.QueryString.GetKey(x).ToString() == "RO")
            {
                ReadOnly = Request.QueryString["RO"].ToString();
                ViewState["ReadOnly"] = ReadOnly;
            }
            else
                ViewState["ReadOnly"] = 0;

            if (Request.QueryString["CT"] != null)
                x++;

            if (Request.QueryString["NA"] != null)
                x++;
            // LAS INDIVIDUALES END

            //QUERYSTRING
            if (x < Request.QueryString.Count - 1)
            {
                arrayCampos = new string[Request.QueryString.Count - x - 1];
                y = 0;
                for (++x; x <= Request.QueryString.Count - 1; x++)
                {
                    arrayCampos[y] = Request.QueryString.GetKey(x) + '§' + Request.QueryString[x];
                    y++;
                }
                cargarCampos(arrayCampos);
            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if(arrayCampos != null)
            if (arrayCampos.Length > 0)
                cargarCampos(arrayCampos);
    }

    bool cargarCampos(string[] arrayCampos)
    {
      bool resultado = false;
      try
      {
        Control _CTRL;
        ControlCollection _CTRL_CO;
        string strAux;
            strAux = "";
        bool ReadOnly = false;
        if (ViewState["ReadOnly"].ToString() == "1")
          ReadOnly = true;        

        for (int a = 0; a <= arrayCampos.Length - 1; a++) 
        {

          _CTRL = Page.Controls[0].FindControl("Form1").FindControl("MainContent").FindControl(arrayCampos[a].ToString().Split('§')[0]);
            if (_CTRL == null)
                continue;
          _CTRL_CO = _CTRL.Controls;

          for (int x = 2; x <= _CTRL_CO.Count - 1; x++)
          {
            if (_CTRL_CO[x].HasControls())
            {
              bool blnTodoVisible = true;
              for (int y = 0; y <= _CTRL_CO[x].Controls.Count - 1; y++)
              {
                if (!_CTRL_CO[x].Controls[y].Visible)
                {
                  blnTodoVisible = false;
                  break;
                }
              }
              if (blnTodoVisible)
              {
                //Aquí es donde!
                for (int z = 0; z <= _CTRL_CO[x].Controls.Count - 1; z++)
                {
                  if (_CTRL_CO[x].Controls[z] is TextBox)
                  {
                    TextBox txt = (TextBox)_CTRL_CO[x].Controls[z];
                    txt.Text = arrayCampos[a].ToString().Split('§')[1];
                    txt.ReadOnly = ReadOnly;
                                    //Label lbl = (Label)Page.Controls[0].FindControl("MainContent").FindControl("Form1").FindControl(arrayCampos[a].ToString().Split('§')[0] + "_TTitulo");

                    //                Label lbl = (Label)_CTRL_CO[1];
                    //if (ReadOnly)
                    //{
                    //    lbl.Style.Add("display","none");
                    //    txt.Style.Add("display", "none");
                    //}
                                    break;
                    
                  }
                  if (_CTRL_CO[x].Controls[z] is DropDownList)
                  {
                    DropDownList ddl = (DropDownList)_CTRL_CO[x].Controls[z];
                    ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(arrayCampos[a].ToString().Split('§')[1]));
                    ddl.Enabled = !ReadOnly;
                    break;
                  }
                }
                strAux = "!";
              } // Todos CTRLs Visibles
            } // Tiene CTRLs
          } // BuscarCTRL
        } // arrayCampos

        if (ViewState["AutoSubmit"].ToString() == "1")
        {
          ClientScript.RegisterStartupScript(GetType(), "AutoSubmit", "$(function(){document.getElementById('Button1').click();});", true);
        }
        resultado = true;
      }
      catch (Exception ex)
      {

      }
      return resultado;
    }

    protected void CerrarM(object sender, EventArgs e)
    {
      DatosGrid(sender, e);
      Session.Remove("Modal");

      if (Session["lstRedirect"] != null)
      {
        List<string> lstRedirect = new List<string>();
        lstRedirect = (List<string>)Session["lstRedirect"];
        int intMaxURL = lstRedirect.Count;
        lstRedirect.RemoveAt(intMaxURL - 1);

        if (lstRedirect.Count > 0)
        {
          ClientScript.RegisterStartupScript(GetType(), "iFrameRedirect", "$(function(){ iFrameRedirect('" + lstRedirect[intMaxURL - 2] + "'); });", true);
        }
        else
        {
          Session.Remove("lstRedirect");
          Session.Remove("AutoSubmit");
        }
      }

    }

  //private Google.Apis.Drive.v2.Data.File archivoGoogleDrive(HttpPostedFile pfArchivo)
  //  {
  //    try
  //    {        
  //      string strAppPath = HttpContext.Current.Server.MapPath("~/");

  //      string[] Scopes = new string[] { DriveService.Scope.Drive };

  //      UserCredential credential;

  //      string strAuthjson = "";
  //      strAuthjson = System.Configuration.ConfigurationManager.AppSettings["GoogleDrive_OAuth"];
  //      if (!System.IO.File.Exists(strAuthjson))
  //      {
  //        //Error = "No existe archivo json";
  //        return null;
  //      }

  //      using (var stream = new FileStream(strAuthjson, FileMode.Open, FileAccess.Read))
  //      {
  //        string credPath = System.Configuration.ConfigurationManager.AppSettings["GoogleDrive_Token"];

  //        credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
  //            GoogleClientSecrets.Load(stream).Secrets,
  //            Scopes,
  //            "infraGH",
  //            CancellationToken.None,
  //            new FileDataStore(credPath, true)).Result;
  //      }

  //      var service = new DriveService(new BaseClientService.Initializer()
  //      {
  //        HttpClientInitializer = credential,
  //        ApplicationName = "Sistemas",
  //      });

  //      //Directorios
  //      //FilesResource.ListRequest request = service.Files.List();
  //      //request.Q = "mimeType='application/vnd.google-apps.folder'";
  //      //FileList files = request.Execute();

  //      //El valor en Parent es el de un directorio creado en Google Drive
  //      string strParent = System.Configuration.ConfigurationManager.AppSettings["GoogleDrive_Directorio"];
  //      Google.Apis.Drive.v2.Data.File archivoGD = uploadFile(service, strParent, pfArchivo);

  //      //Permisos para habilitar descarga por link
  //      if (archivoGD != null)
  //      {
  //        Permission PermisosArchivo = new Permission();
  //        PermisosArchivo.Type = "anyone";
  //        PermisosArchivo.Role = "reader";
  //        PermisosArchivo.Value = "";
  //        PermisosArchivo.WithLink = true;
  //        service.Permissions.Insert(PermisosArchivo, archivoGD.Id).Execute();
  //      }

  //      return archivoGD;
  //    }
  //    catch (Exception ex)
  //    {
  //      //Response.Write(ex.Message.ToString());
  //      //return false;
  //      return null;
  //    }
  //  }

  //  public static Google.Apis.Drive.v2.Data.File uploadFile(DriveService _service, string _parent, HttpPostedFile _archivo)
  //  {      
  //    Google.Apis.Drive.v2.Data.File body = new Google.Apis.Drive.v2.Data.File();
  //    body.Title = _archivo.FileName;
  //    body.Description = "Archivo de usuario";
  //    body.MimeType = _archivo.ContentType;
      
  //    body.Parents = new List<ParentReference> { new ParentReference() { Id = _parent } };      
      
  //    byte[] byteArray = new byte[_archivo.ContentLength];
  //    _archivo.InputStream.Read(byteArray, 0, _archivo.ContentLength);
  //    System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);
  //    try
  //    {        
  //      //FilesResource.InsertMediaUpload request = _service.Files.Insert(body, stream, GetMimeType(_uploadFile));
  //      FilesResource.InsertMediaUpload request = _service.Files.Insert(body, stream, _archivo.ContentType);        
  //      request.Upload();
  //      return request.ResponseBody;

  //    }
  //    catch (Exception e)
  //    {
  //      Console.WriteLine("Error: " + e.Message);
  //      return null;
  //    }      
  //  }
}