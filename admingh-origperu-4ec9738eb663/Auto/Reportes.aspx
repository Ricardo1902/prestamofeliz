﻿<%@ Page Title="" Language="C#" MasterPageFile="../Site.master" AutoEventWireup="true" CodeFile="Reportes.aspx.cs" Inherits="Reportes" EnableEventValidation="true" ValidateRequest="false" %>

<%@ Register Src="Includes/ctlControles.ascx" TagName="Autocontrol" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%--  Scripts Requeridos de la pagina --%>
    <script type="text/javascript" src="Scripts/jquery.maskMoney.min.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.symbol.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.orderbars.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.stack.js"></script>
    <script type="text/javaScript" src="Scripts/Jquery/jquery-ui.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.debug.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.highlighter.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.flot.spider.js"></script>
    <script type="text/javascript" src="Scripts/framework.js"></script>
    <script type="text/javascript" src="/js/JScriptAuto.js"></script>    
    <%--  Funciones Especiales de la pagina --%>
    <script type="text/javascript" >
        var lastURL = new Array();
        $(function ()
                {
                    $('input').tooltip();
                    $('select').tooltip();
                    $(".linkModal").on("click", function ()
                                                        {
                                                            $(document).scrollLeft(0);
                                                            var strURL = location.href;
                                                            var arrayURL = strURL.split("/");
                                                            strURL = arrayURL[0] + "//" + arrayURL[2] + "/Auto/" + $(this).attr("pagURL") + ".aspx?";
                                                            strURL += $(this).attr("datosQS");
                                                            $("#divCubrir").addClass("divCubrir");
                                                            $("#divCubrir").css("height", $(document).height());
                                                            $("#divCubrir").css("width", $(document).width());
                                                            $(".pnlForm").show();
                                                            $("#iForm").attr('src', strURL);
                                                        });
                    $(".btnCerrarM").on("click", function ()
                                                        {
                                                            lastURL.pop();
                                                            if (lastURL.length > 0)
                                                                {
                                                                $("#iForm").attr('src', lastURL[lastURL.length - 1]);
                                                                
                                                                }
                                                            else
                                                                {
                                                                    lastURL.pop();
                                                                    $("#divCubrir").removeClass("divCubrir");
                                                                    $(".pnlForm").hide();
                                                                    location.reload();
                                                                   
                                                                }
                
                                                        });
                    $(".imgLoading").hide();

                });
        function arrayIFRAME_URL(iFrameURL)
                {
                    iFrameURL = iFrameURL.replace('#', '');
                    if (iFrameURL == "about:blank") {
                        return;
                    }
                    if (iFrameURL.indexOf("error.aspx") > 1 || iFrameURL.indexOf("frmLogin.aspx") > 1) {
                        window.location = '../frmLogin.aspx';
                    }
                    if (lastURL.length > 0)
                    {
                        if (iFrameURL != lastURL[lastURL.length - 1])
                            lastURL.push(iFrameURL);
                    }
                    else
                    {
                           lastURL.push(iFrameURL);
                    }
                    
                }
        function iFrameRedirect(strURL)
                {
                    $("#divCubrir").addClass("divCubrir");
                    $("#divCubrir").css("height", $(document).height());
                    $(".pnlForm").show();
                    $("#iForm").attr('src', strURL);
        }
        function verreporte(sender, selectedIndex) {
            var boton = document.getElementById('ctl00_MainContent_Reportess');
            boton.click();
            document.getElementById("ctl00_MainContent_Div_Controles").style.display = "none";

        }
        function abrir()
                {
                    document.location.reload()
                }
    </script>
    <%--  Estilos Especiales de la pagina --%>
    <style type="text/css">
      .divCubrir 
            {
                content: " ";
                z-index: 10;
                display: block;
                position: absolute;
                top: 0;left: 0;right: 0;
                background: rgba(0, 0, 0, 0.5);
            }
      .pnlForm 
            {
               z-index:11;
                border-radius:15px;background-color:#EEEEEE;display:none;
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                margin: auto;
            }
      .pnlFormExterno
            {
                width:90%;
                height:90%;
            }
      .pnlFormInterno 
            {
                width:100% !important;
                height:100% !important;
                
            }
      .btnCerrarM 
            {
                float:right;
                width:30px;
                height:30px;
                cursor:pointer;
            }
       #iForm    
            {
                float:left;
                width: 100%;
                height: 100%;margin:auto;
            }
      .imgLoading 
            {
                width:200px;height:200px;
                position: absolute;
                top:0;
                bottom: 0;
                left: 0;
                right: 0;
                margin: auto;
            }      
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    
         <div id="MakeConrtrols" class="" runat="server" style="display:none;">
      
            <div  class="panel panel-default" id="PageLabel" runat="server">
                <div id="Titulo_de_la_Pagina" class="panel-body">
                     <asp:Label ID="lblTitulo" runat="server" Text="Reportes" />
                </div>
            </div>
            <div class="panel panel-default">
                <div id="Titulo_de_la_Opcion" class="panel-heading panel-heading-custom text-center">
                    <h3 class="panel-title">
                        <asp:Label ID="lblOpcion" runat="server" Text="Reportes" />
                    </h3>
                </div>
                <div class="panel-body" style="overflow: auto;  top: 0%; height: 100%">

                        <%--  Div en el se encuentra toda la seleccion de los controles que se construyen en automatico --%>
                        <div id="ComboControles" runat="server" border="0">
                            <div id="DCombo" runat="server" style="display: block;">
                                <asp:DropDownList ID="Reporte" runat="server" class="td7 form-control" EmptyText="Seleccionar" Width="200px" Height="30px" MenuWidth="300" OnSelectedIndexChanged="Ver" onchange="verreporte();" FilterType="Contains">
                                </asp:DropDownList>
                             </div>
                             <div id="DReload" runat="server" style="display: none;">
                                <input type="button" id="btnRegresar"  class='btn btn-primary' style="display:none !important;" onclick ="window.location = 'Reportes.aspx?Modal=0';" value="Regresar" />
                             </div>
                             <div id="BotonReporte" style="visibility: hidden">
                                        <asp:Button ID="Reportess" runat="server" OnClick="Ver"></asp:Button>
                             </div>
                             <div id="Divgrid" runat="server" style="display: none;">
                                        <asp:CheckBox ID="VerGrid" runat="server" Text="Ver en Grid" Font-Size="Small" />
                            </div>
                            <div id="Div_Controles" runat="server" style="width: 90%; display: none;    min-width: 800px;">
                                <asp:UpdatePanel runat="server" ID="CControl" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel ID="Controles" runat="server" HorizontalAlign="Left">
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                        </div>
                        <%--  Div en el se encuentra la ejecucion a grid  despues de realizar el exec --%>
                        <div id="RGrid" runat="server"  class="panel panel-default" style="overflow:hidden; display: none;margin-top:10px;width: 100%;">
                          
                                <div class="panel-heading panel-heading-custom text-center"><span class="panel-title" runat="server" id="TituloResultado"></span>
                                </div>
                                <div class="panel-body">
                                    <div id="variosGrid" runat="server" style="height: 50px; display: none;">
                                                        <table border="0" width="100%">
                                                            <tr>
                                                                <td style="width: 90px;">
                                                                    <label class="labelgrid">Resultados</label></td>
                                                                <td style="width: 100px">
                                                                    <asp:DropDownList ID="Resultados" runat="server" OnTextChanged="DatosGrid" Width="200px" Height="30px" AutoPostBack="true" CssClass="form-control">
                                                                    </asp:DropDownList></td>
                                                                <td>
                                                                    <span style="float: left;">
                                                                        <asp:Label ID="lblInfo" runat="server" /></span>
                                                                    <span style="float: right;">Total Registros:
                                                                    <asp:Label ID="lblTotalClientes" Font-Size="Medium" ForeColor="#798e94" Font-Bold="true" runat="server" CssClass="label label-info" /></span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                    <div id="Tabla" class="table-responsive" >
                                        <asp:Label ID="lblMensajeConsulta" runat="server"></asp:Label>
                                        <asp:Table runat="server" ID="tblGeneral" class="table table-bordered table-striped">                                                        
                                        </asp:Table>
                                    </div>
                                    <div id="variosPagina" runat="server" style="height: 50px; display: none;" class="form-group">
                                                        <table border="0" width="100%" >
                                                            <tr>
                                                                <td style="width: 90px">
                                                                    <label class="labelgrid">Ir a la pág.</label>
                                                                </td>
                                                                <td style="width: 100px">
                                                                    <asp:DropDownList ID="Paginas" runat="server" OnTextChanged="DatosPage" Width="100px" Height="30px" AutoPostBack="true" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="right" valign="middle">
                                                                        <asp:Label Font-Size="Medium" ID="CurrentPageLabel" runat="server" ForeColor="#798e94" Font-Bold="true" CssClass="label label-info" />
                                                                </td>
                                                               
                                                            </tr>
                                                        </table>
                                                    </div>
                                                                                                 
                                        

                                                
                                </div>
                            
                        </div>
                        <%--  Div en el se encuentra la ejecucion a graficas Pay --%>
                        <div id="RGPay" runat="server" style="height: 800; overflow: auto; display: none; top: auto;    min-width: 800px;">
                                        <asp:Label ID="Titulo" runat="server" Test=""></asp:Label><br />
                                        <asp:Label ID="LblNombre" runat="server" Test=""></asp:Label>
                                        <table align="center">
                                            <tr>
                                                <td>
                                                    <div id="placeholder" class="grafica" style="width: 600px; height: 500px;"></div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LblError" runat="server" Test=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="tooltip" style="display: block; background-color: transparent; position: fixed; padding: 2px; width: 100px; height: 100px; top: 0; left: 0;"></div>


                                    </div>
                        <%--  Div en el se encuentra la ejecucion a graficas lineas --%>
                        <div id="RGLinea" runat="server" style="height: 800; overflow: auto; display: none;    min-width: 800px;">

                                        <h2>
                                            <asp:Label ID="Titulo2" runat="server" Test=""></asp:Label><br />
                                            <asp:Label ID="LblNombre2" runat="server" Test=""></asp:Label></h2>
                                        <table align="center">
                                            <tr>
                                                <td>
                                                    <div id="placeholder2" class="grafica" style="width: 600px; height: 500px;"></div>
                                                </td>
                                                <td>
                                                    <div style="width: 100px; height: 600px; overflow: auto;" id="labeler"></div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="ocultar">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <input id="Filtrar" type="checkbox" /><span>Filtrar Datos</span>

                                                                </td>
                                                                <td>
                                                                    <div style="width: 150px; height: 100px; overflow: auto; display: none; font-size: x-small;" id="choices"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                                <td>

                                                    <div id="ocultar2">
                                                        <input id="enableTooltip" type="checkbox" checked="checked" /><span>Ver Datos</span>
                                                        <br />
                                                        <asp:Label ID="LblError2" runat="server" Test=""></asp:Label>
                                                    </div>
                                                    <div id="tooltip2" style="display: block; background-color: transparent; position: fixed; padding: 2px; width: 100px; height: 100px; top: 0; left: 0;"></div>


                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                            
               
                </div>
            </div>
             <%--  Div en el se encuentra los parametros invisibles a enviar --%>
            <div id="server_oculto" style="visibility: hidden">
                <asp:TextBox ID="Archiv" runat="server" Text=""></asp:TextBox>
                <asp:TextBox ID="QueryExec" runat="server" Text=""></asp:TextBox>
                <asp:TextBox ID="scrip" runat="server" Text="" TextMode="MultiLine" type="textarea"></asp:TextBox>
                <asp:Button ID="Exc" OnClick="ExportarExcel" runat="server"></asp:Button>
            </div>


        </div>
      
       <%-- Pantalla Modal --%>
       <asp:Panel runat="server" ID="pnlForm" CssClass="pnlForm pnlFormExterno">        
        <div class="divpopup" >  
                <asp:label id="PopUpLabel" runat="server" style="height:30px;"> </asp:label>
                <img src="../Imagenes/Alertas/error.png" class="btnCerrarM" alt="X"/>

        </div>
                          
                    <iframe id="iForm" frameborder="0" style="margin-top:10px;" onLoad="arrayIFRAME_URL(this.contentWindow.location.href)"></iframe>
          
      </asp:Panel>


  <div id="divCubrir" ></div>
  
</asp:Content>

