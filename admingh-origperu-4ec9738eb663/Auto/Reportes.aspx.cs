﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows;

using System.IO;
using System.Collections.Generic;

using Google.Apis.Drive.v2;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using Google.Apis.Drive.v2.Data;
using System.Threading;
using System.Net;

public partial class Reportes : System.Web.UI.Page
{
    cls_Rep Obj_Rep = new cls_Rep();
    static SqlConnection conBd;

    List<string> lstRedirect = new List<string>();
    string[] arrayCampos;

    string strAppPath;

    static string[] Scopes = { DriveService.Scope.Drive };
    static UserCredential credential;

    protected override void OnPreInit(EventArgs e)
    {        
        if (Request.QueryString["ArchivoDrive"] != null)
        {
            ViewState["ArchivoDrive"] = "1";
            return;
        }

        ViewState["No_Accion"] = "0";
        base.OnPreInit(e);
        string strModal = Request.QueryString["Modal"];

        if (Request.QueryString["AS"] != null)
        {
            ViewState["AS"] = Request.QueryString["AS"].ToString();
        }

        if (strModal != null || Session["Modal"] != null)
        {
            if (strModal == null) strModal = Session["Modal"].ToString();
            if (strModal == "1" || strModal == "3")
            {
                if (strModal == "1")
                {
                    //this.MasterPageFile = "~/SiteVacio.master";
                    Session["Modal"] = "1";
                }
                else
                {
                    Session["Modal"] = "3";
                }


                if (Request.QueryString["CT"] != null)
                {
                    ViewState["Cargar_CTL_Tabla"] = Request.QueryString["CT"].ToString();
                }
                if (Request.QueryString["NA"] != null)
                {
                    ViewState["No_Accion"] = Request.QueryString["NA"].ToString();
                }

                if (Session["lstRedirect"] != null)
                {
                    lstRedirect = (List<string>)Session["lstRedirect"];
                    int intMaxURL = lstRedirect.Count;
                    if (lstRedirect[intMaxURL - 1] != Request.Url.ToString())
                    {
                        if (Request.QueryString["AS"] != null)
                        {
                            if (Request.QueryString["AS"].ToString() == "0")
                            {
                                lstRedirect.Add(Request.Url.ToString());
                            }
                        }
                    }
                }
                else
                {
                    if (Request.QueryString["AS"] != null)
                    {
                        if (Request.QueryString["AS"].ToString() == "0")
                        {
                            lstRedirect.Add(Request.Url.ToString());
                            Session["lstRedirect"] = lstRedirect;
                        }
                    }
                }

            }
            else { Session.Remove("Modal"); }
        }
    }

    #region Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ViewState["ArchivoDrive"] != null)
        {
            string IDArchivo = Request.QueryString["ArchivoDrive"];

            strAppPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            string PathTMPDocumentos = strAppPath + "\\Auto\\TMPDocumentos";
            DirectoryInfo TMPDocumentos = new DirectoryInfo(PathTMPDocumentos);
            FileInfo[] FI = TMPDocumentos.GetFiles(IDArchivo + "*");
            if (FI.Length != 0)
            {
                Response.Redirect("TMPDocumentos\\" + FI[0].Name, true); //Finaliza
            }

            string Error = "";
            string URLArchivo = "";
            if (descargarArchivo(IDArchivo, ref URLArchivo, ref Error))
            {
                Response.Redirect(URLArchivo, true); //Finaliza
            }
            else
            {
                return;
            }
        }


        readQS();

        Page.LoadComplete += new EventHandler(Page_LoadComplete);
        TituloResultado.InnerText = ConfigurationManager.AppSettings["TituloResultado"];





        if (!Page.IsPostBack)
        {

        
            Response.Buffer = true;
            ComboControles.Style.Add("display", "none");
            DReload.Style.Add("display", "none");
            MakeConrtrols.Style.Add("display", "none");
            DCombo.Style.Add("display", "Block");

            string txml = "0";
            if (Session["XML"] != null)
            { txml = "1"; }


            if (!Page.IsPostBack)
            {

                Session["Archivo"] = "Reporte";
                DataSet Das_Reporte = new DataSet();
                int x = 0;


                if (Session["UsuarioId"] != null)
                {
                    x = Convert.ToInt32(Session["UsuarioId"].ToString());
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Clear();
                    Session["identificado"] = false;
                    Response.Redirect("../frmLogin.aspx");
                }


                int Sistema = Convert.ToInt32(Session["SistemaId"].ToString());

                if (Session["Modal"] == null)
                {
                    Das_Reporte = Obj_Rep.Rep_Reporte(x, Sistema, 0);
                }
                else
                {
                    var ValorDes = Convert.FromBase64String(Request.QueryString["8UOSDB5CZ2"].ToString());
                    var strValor = Encoding.UTF8.GetString(ValorDes);
                    TituloResultado.InnerText = "";//Quita el dato de resultado del Grid
                    if (strValor != null)
                    {
                        Das_Reporte = Obj_Rep.Rep_Reporte(x, Sistema, Convert.ToInt32(strValor));
                    }

                }

                Session["TipoRep"] = Das_Reporte.Tables[1].Rows[0][0].ToString();

                Session["DS_Reporte"] = Das_Reporte;


                Reporte.DataSource = null;
                Reporte.Items.Clear();
                Reporte.DataBind();

                if (Das_Reporte.Tables[0].Rows.Count != 0)
                {

                    Reporte.DataSource = Das_Reporte.Tables[0];
                    Reporte.DataTextField = Das_Reporte.Tables[0].Columns[1].ColumnName;
                    Reporte.DataValueField = Das_Reporte.Tables[0].Columns[0].ColumnName;
                    Reporte.DataBind();
                    Reporte.Items.Insert(0, "Seleccione Reporte");

                    Session["DS_Ctrl"] = null;

                }


                if (Session["Modal"] != null)
                {
                    if (Session["Modal"].ToString() == "1" || Session["Modal"].ToString() == "3")
                    {
                        if (Session["Modal"].ToString() == "1")
                        {
                            pnlForm.CssClass = pnlForm.CssClass.Replace("pnlFormExterno", "pnlFormInterno");

                            //PageLabel.Style.Add("display", "none");

                        }
                        if (Reporte.Items.Count > 0)
                        {
                            var ValorDes = Convert.FromBase64String(Request.QueryString["8UOSDB5CZ2"].ToString());
                            var strValor = Encoding.UTF8.GetString(ValorDes);

                            var DescripDes = Convert.FromBase64String(Request.QueryString["ZQY4UEOQHR"].ToString());
                            var strDescripDes = Encoding.GetEncoding("iso-8859-1").GetString(DescripDes);
                            lblTitulo.Text = strDescripDes;
                            PopUpLabel.Text = strDescripDes;

                            if (strValor != null)
                            {
                                ViewState["strValor"] = strValor;
                            }
                        }
                        else
                        {
                            Response.Redirect("../frmLougout.aspx");
                        }
                    }
                }

            }
        }
        else
        {
            if (Session["Grids"] != null && Session["Grids"].ToString() != "0")
            //if (ViewState["Grids"] != null && ViewState["Grids"].ToString() != "0")

            {
                if (Session["Grids"] != null && Session["Grids"].ToString() != "0")
                // if (ViewState["Grids"] != null && ViewState["Grids"].ToString() != "0")
                {
                    //  if (ViewState["Grids"].ToString() == "7" || ViewState["Grids"].ToString() == "8" || ViewState["Grids"].ToString() == "9")
                    if (Session["Grids"].ToString() == "7" || Session["Grids"].ToString() == "8" || Session["Grids"].ToString() == "9")
                    { Session["Boton"] = "Guardar"; }
                    else { Session["Boton"] = "Generar"; }
                }

                if (Session["Modal"] == null)
                    Controles.Controls.Add(new LiteralControl("<br /><div class='col-xs-12'><br /><input id='Button1' type='button' value='" + Session["Boton"].ToString() + "' onclick='leerValores();' class='btn btn-primary' /></div>"));
                else
                    if (Session["Modal"].ToString() == "0")
                    Controles.Controls.Add(new LiteralControl("<br /><div class='col-xs-12'><br /><input id='Button1' type='button' value='" + Session["Boton"].ToString() + "' onclick='leerValores();' class='btn btn-primary' /></div>"));
                else
                        if (ViewState["AS"].ToString() == "1")
                    Controles.Controls.Add(new LiteralControl("<br /><div class='col-xs-12'><br /><input id='Button1' style='display:none;' type='button' value='" + Session["Boton"].ToString() + "' onclick='leerValores();' class='btn btn-primary' /></div>"));
                else
                    Controles.Controls.Add(new LiteralControl("<br /><div class='col-xs-12'><br /><input id='Button1' type='button' value='" + Session["Boton"].ToString() + "' onclick='leerValores();' class='btn btn-primary' /></div>"));


            }
        }

    }
    #endregion

    public void Ver(object sender, EventArgs e)
    {
        List<string> lstCamposAGD = new List<string>();


        DCombo.Style.Add("display", "none");
        lblOpcion.Text = Reporte.SelectedItem.Text.ToString();
        RGrid.Style.Add("display", "none");
        Divgrid.Style.Add("display", "none");
        variosGrid.Style.Add("display", "none");

        if (Reporte.SelectedValue != null && Reporte.SelectedValue.ToString() != "" && Reporte.SelectedIndex.ToString() != "0")
        {
            Archiv.Text = "";
            Divgrid.Style.Add("display", "none");
            Div_Controles.Style.Add("display", "block");

            ViewState["Grids"] = "0";
            Session["Grids"] = "0";


            DataSet Ds_Controles = new DataSet();
            int reporte = Convert.ToInt32(Reporte.SelectedValue);
            Ds_Controles = Obj_Rep.Rep_Campos(reporte);
            string CampoNombre = "";
            int i = 0;
            string s = "";
            s = Ds_Controles.Tables[0].Rows[0]["Pagina_Id"].ToString();

            ViewState["Grids"] = s.ToString();

            Session["Grids"] = s.ToString();
            string Pagina = (string)Session["Grids"];


            ViewState["RegGrid"] = Ds_Controles.Tables[0].Rows[0]["RegGrid"].ToString();
            ViewState["M_RegGrid"] = Ds_Controles.Tables[0].Rows[0]["RegGrid"].ToString();
            if (s.ToString() == "2")
            {
                Divgrid.Style.Add("display", "block");
            }

            string VarCon = "";
            string TipoVariable = "";
            this.QueryExec.Text = "";
            if (Ds_Controles != null)
            {

                if (Ds_Controles.Tables[0].Rows.Count > 0)
                {
                    #region CadenaEjecutar
                    if (Ds_Controles.Tables[0].Rows[0]["Nombre_Archivo"].ToString() != null && Ds_Controles.Tables[0].Rows[0]["Nombre_Archivo"].ToString() != "")
                    {
                        Session["Archivo"] = Ds_Controles.Tables[0].Rows[0]["Nombre_Archivo"].ToString() + String.Format("{0:yyyyMMdd}", System.DateTime.Now);
                    }
                    else
                    {
                        Session["Archivo"] = (string)Session["Archivo"] + String.Format("{0:yyyyMMdd}", System.DateTime.Now);
                    }

                    Session["TipoEncoding"] = Ds_Controles.Tables[0].Rows[0]["Encoding"].ToString();

                    //this.QueryExec.Text = Ds_Controles.Tables[0].Rows[0]["Sp"].ToString() + " ";
                    CampoNombre = Ds_Controles.Tables[0].Rows[0]["Nombre_Archivo2"].ToString();

                    foreach (DataRow row in Ds_Controles.Tables[2].Rows)
                    {
                        VarCon = "";
                        TipoVariable = Ds_Controles.Tables[2].Rows[i]["TipoVariable"].ToString();
                        TipoVariable = TipoVariable.ToUpper();
                        if (TipoVariable == "INT" || TipoVariable == "STR" || TipoVariable == "NUL" || TipoVariable == "XML" || TipoVariable == "FIL" || TipoVariable == "FTX")
                        {
                            if (i != 0)
                            {

                                VarCon = VarCon + ",";

                            }

                            VarCon = VarCon + Ds_Controles.Tables[2].Rows[i]["Parametro"].ToString() + "=";

                            if (TipoVariable == "STR" || TipoVariable == "XML"
                                    )
                            {
                                VarCon = VarCon + "'";
                            }

                            if (TipoVariable == "NUL")
                            {
                                VarCon = VarCon + "Null";
                            }
                            else
                            {
                                if (Session["Modal"] != null)
                                {
                                    if (ViewState["ReadOnly"] != null)
                                        if (ViewState["ReadOnly"].ToString() == "3")
                                            if (arrayCampos != null)
                                                if (arrayCampos.Length > 0)
                                                {
                                                    int x;
                                                    for (x = 0; x <= arrayCampos.Length - 1; x++)
                                                    {
                                                        if (Ds_Controles.Tables[2].Rows[i]["Titulo_Pagina"].ToString().ToUpper() == arrayCampos[x].ToString().ToUpper().Split('§')[0])
                                                        {
                                                            VarCon += arrayCampos[x].ToString().Split('§')[1];
                                                            break;
                                                        }
                                                    }
                                                }
                                }
                                else
                                    VarCon = VarCon + Ds_Controles.Tables[2].Rows[i]["Valor_Variable"];

                            }

                            if (TipoVariable == "STR")
                            {
                                VarCon = VarCon + "'";
                            }
                        }
                        else
                        {
                            if (i != 0)
                            {
                                VarCon = VarCon + ",";


                            }

                            VarCon = VarCon + Ds_Controles.Tables[2].Rows[i]["Parametro"].ToString() + "=";
                            if (TipoVariable == "SSR" || TipoVariable == "XML"
                                )
                            {
                                VarCon = VarCon + "'";
                            }


                            VarCon = VarCon + Session[Ds_Controles.Tables[2].Rows[i]["Valor_Variable"].ToString()].ToString();

                            if (TipoVariable == "SSR")
                            {
                                VarCon = VarCon + "'";
                            }
                        }
                        this.QueryExec.Text = this.QueryExec.Text + VarCon;
                        i = i + 1;


                    }
                    if (i != 0)
                    {
                        if (Ds_Controles.Tables[1].Rows.Count != 0)
                        {
                            this.QueryExec.Text = this.QueryExec.Text + ",";
                        }
                    }
                    #endregion
                    i = 0;
                    #region Agregar controles
                    this.Controles.Controls.Clear();

                    DataTable DTControl = new DataTable();
                    DTControl.Columns.Add("ID", typeof(string));
                    DTControl.Columns.Add("ValorTipo", typeof(string));
                    DTControl.Columns.Add("Tipo", typeof(string));
                    DTControl.Columns.Add("Parametro", typeof(string));
                    DTControl.Columns.Add("NombreArc", typeof(string));
                    DTControl.Columns.Add("Titulo", typeof(string));
                    string Carch = "";
                    this.Controles.Controls.Clear();

                    //this.Controles.Controls.Add(new LiteralControl("<table>"));
                    int rows3 = 0;
                    int rowsAnt3 = 0;
                    bool renglonAbierto = false;

                    foreach (DataRow row in Ds_Controles.Tables[1].Rows)
                    {
                        Carch = "";

                        Includes_ctlControles Control2 = new Includes_ctlControles();
                        Control2 = (Includes_ctlControles)this.Page.LoadControl("/Auto/Includes/ctlControles.ascx");

                        Control2.ID = Ds_Controles.Tables[1].Rows[i]["Titulo_Pagina"].ToString();
                        Control2.VTitulo = Ds_Controles.Tables[1].Rows[i]["Titulo_Pagina"].ToString();
                        Control2.VNombre = Ds_Controles.Tables[1].Rows[i]["Nombre"].ToString();
                        Control2.VTipo = Ds_Controles.Tables[1].Rows[i]["Control"].ToString();
                        Control2.VSP = Ds_Controles.Tables[1].Rows[i]["DataSet"].ToString();
                        Control2.VSP_Tipo = "";
                        Control2.VSP_VarCon = Ds_Controles.Tables[1].Rows[i]["DS"].ToString();
                        Control2.VRID = "";
                        Control2.VValorTipo = Ds_Controles.Tables[1].Rows[i]["TipoVariable"].ToString();
						//Control2.VValorDefault = Ds_Controles.Tables[1].Rows[i]["Valor_Variable"].ToString();
						Control2.VFuncionJS = Ds_Controles.Tables[1].Rows[i]["FuncionJS"].ToString();

                        if (Control2.VTipo.ToUpper() != "AGD")
                        {
                            Control2.VValorDefault = Ds_Controles.Tables[1].Rows[i]["Valor_Variable"].ToString();
                        }
                        else
                        {
                            string[] strValoresGD = Ds_Controles.Tables[1].Rows[i]["Valor_Variable"].ToString().Split('|');
                            Control2.set_gd_IDDirectorio(strValoresGD[0]);
                            Control2.set_gd_IDArchivo(strValoresGD[1]);
                            Control2.set_gd_URL(strValoresGD[2]);

                            lstCamposAGD.Add(Control2.ClientID);
                            ViewState["CamposAGD"] = lstCamposAGD.ToArray();
                        }

                        Control2.VComboPadre = Ds_Controles.Tables[1].Rows[i]["ComboPadre"].ToString();
                        Control2.VLongitud = Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Longitud"].ToString());
                        Control2.VAncho = Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Ancho"].ToString());
                        if (Ds_Controles.Tables[1].Rows[i]["Control"].ToString() == "CMB")
                        {
                            Control2.VSoloLectura = true;
                        }
                        else
                        {
                            Control2.VSoloLectura = false;
                        }

                        // Obtener el Renglon actual
                        rows3 = Convert.ToInt32(Ds_Controles.Tables[1].Rows[i]["Renglon"].ToString());

                        if (rows3 != rowsAnt3)
                        {
                            // Si hay un renglon abierto lo cerramos
                            if (renglonAbierto)
                                this.Controles.Controls.Add(new LiteralControl("</div>"));

                            this.Controles.Controls.Add(new LiteralControl("<div class='row'>"));
                            renglonAbierto = true;
                        }

                        this.Controles.Controls.Add(new LiteralControl(" <div class='" + Ds_Controles.Tables[1].Rows[i]["Class"].ToString() + "'>"));
                        this.Controles.Controls.Add(Control2);
                        this.Controles.Controls.Add(new LiteralControl(" </div>"));
                       
                        rowsAnt3 = rows3;                        

                        if (Control2.VRID.ToString() != null && Control2.VRID.ToString() != "")
                        {
                            if (Ds_Controles.Tables[1].Rows[i]["Parametro"].ToString() == CampoNombre && CampoNombre != null && CampoNombre != "")
                            {
                                Carch = "X";
                            }

                            if (Control2.VTipo.ToString() == "TXT" || Control2.VTipo.ToString() == "TXA" || Control2.VTipo.ToString() == "TXP")
                            {

                                DTControl.Rows.Add(Control2.ID + '_' + Control2.VRID.ToString(), Control2.VValorTipo, Control2.VTipo.ToString(), Ds_Controles.Tables[1].Rows[i]["Parametro"].ToString(), Carch, Control2.VTitulo);
                            }
                            else
                            {
                                DTControl.Rows.Add(Control2.VRID.ToString(), Control2.VValorTipo, Control2.VTipo.ToString(), Ds_Controles.Tables[1].Rows[i]["Parametro"].ToString(), Carch, Control2.VTitulo);
                            }
                        }
                        i = i + 1;
                    }

                    // Nos aseguramos de cerrar el ultimo Renglon si quedo abierto
                    if (renglonAbierto)
                        this.Controles.Controls.Add(new LiteralControl("</div>"));

                    int la = i;
                    Session["DS_Ctrl"] = DTControl;

                    #endregion
                    i = 0;
                    #region Funcionjava

                    Type cstype = this.GetType();
                    string nombreScript = "leer";
                    ClientScriptManager cs = Page.ClientScript;
                    StringBuilder sb = new StringBuilder();

                    string cadena;
                    string TC;
                    string TV;
                    string cadenaF = "";

                    if (DTControl.Rows.Count > 0 || la == 0)
                    {
                        sb.AppendLine("function leerValores(){ ");

                        sb.AppendLine(" document.getElementById('ctl00_MainContent_scrip').value=\"\";");

                        sb.AppendLine("var coma= \",\";");
                        //sb.AppendLine("alert( coma);");
                        foreach (DataRow row in DTControl.Rows)
                        {

                            #region Tipo de control
                            if (DTControl.Rows[i]["ID"].ToString() != null && DTControl.Rows[i]["ID"].ToString() != "")
                            {
                                TC = DTControl.Rows[i]["Tipo"].ToString();
                                if (TC.ToUpper() == "TXT" || TC.ToUpper() == "FCH" || TC.ToUpper() == "CMB" || TC.ToUpper() == "CMP" || TC.ToUpper() == "XML" || TC.ToUpper() == "FIL" || TC.ToUpper() == "FTX" || TC.ToUpper() == "TXA" || TC.ToUpper() == "TXP")
                                {

                                    TV = DTControl.Rows[i]["ValorTipo"].ToString();
                                    if (TV.ToUpper() == "STR" || TV.ToUpper() == "SSR" || TV.ToUpper() == "XML")
                                    {
                                        cadena = "var s" + i + "= \"'\";";
                                        sb.AppendLine(cadena);
                                    }
                                    else
                                    {
                                        cadena = "var s" + i + "= \"\";";
                                        sb.AppendLine(cadena);
                                    }

                                    if (TC.ToUpper() == "CMB" || TC.ToUpper() == "CMP" || TC.ToUpper() == "FIL" || TV.ToUpper() == "XML" || TC.ToUpper() == "FTX")
                                    {

                                        if (TC.ToUpper() == "CMB" || TC.ToUpper() == "CMP")
                                        {
                                            cadena = " var c" + i + "=ctl00_MainContent_" + DTControl.Rows[i]["Titulo"].ToString() + "_" + DTControl.Rows[i]["ID"].ToString() + ".options[ctl00_MainContent_" + DTControl.Rows[i]["Titulo"].ToString() + "_" + DTControl.Rows[i]["ID"].ToString() + ".selectedIndex].value;";

                                        }
                                        else
                                        {
                                            cadena = "var c" + i + "=document.getElementById('ctl00_MainContent_" + DTControl.Rows[i]["Titulo"].ToString() + "_" + DTControl.Rows[i]["ID"].ToString() + "').value;";
                                        }
                                    }
                                    else
                                    {

                                        cadena = "var c" + i + "= document.getElementById('ctl00_MainContent_" + DTControl.Rows[i]["ID"].ToString() + "').value;";

                                    }
                                    sb.AppendLine(cadena);
                                    if (i == 0)
                                    {
                                        cadenaF = "\"" + DTControl.Rows[i]["Parametro"].ToString() + "=\"+" + "s" + i + "+" + "c" + i + "+" + "s" + i + " ";
                                    }
                                    else
                                    {
                                        cadenaF = cadenaF + "+ coma +" + "\"" + DTControl.Rows[i]["Parametro"].ToString() + "=\"+" + "s" + i + "+" + "c" + i + "+" + "s" + i + " ";
                                    }
                                    if (DTControl.Rows[i]["NombreArc"].ToString() == "X")
                                    {
                                        sb.AppendLine(" document.getElementById('ctl00_MainContent_Archiv').value=c" + i + ";");
                                    }
                                }

                            }
                            #endregion
                            i = i + 1;
                        }
                        if (i == 0 || cadenaF == "")
                        {
                            sb.AppendLine("var c= \"\";");
                        }
                        else
                        {
                            sb.AppendLine("var c= " + cadenaF + ";");
                        }

                        sb.AppendLine(" document.getElementById('ctl00_MainContent_scrip').value=c;");

                        //if (ViewState["Modal"] == null)
                        sb.AppendLine("document.getElementById('Button1').style.visibility = 'hidden';");


                        sb.AppendLine("var boton = document.getElementById('ctl00_MainContent_Exc');");
                        sb.AppendLine("boton.click();");
                        //sb.AppendLine("boton.click(); ");

                        //sb.AppendLine("boton.click().style.display = 'block';");
                        sb.AppendLine("document.getElementById('ctl00_MainContent_Divgrid').style.display='none';");
                        sb.AppendLine("document.getElementById('ctl00_MainContent_Div_Controles').style.display='none';");
                        sb.AppendLine("document.getElementById('ctl00_MainContent_DCombo').style.display='none';");
                        sb.AppendLine("document.getElementById('ctl00_MainContent_DReload').style.display='block';");

                        sb.AppendLine("}");
                        //                        if (!cs.IsStartupScriptRegistered(nombreScript))
                        //                      {
                        // cs.RegisterStartupScript(cstype, nombreScript, sb.ToString(), true);

                        //                    }



                        if (ScriptManager.GetCurrent(this.Page) != null)
                        {
                            if (ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack)
                            {

                                ScriptManager.RegisterStartupScript(this.Page, cstype, nombreScript, sb.ToString(), true);
                            }

                            else
                            {

                                if (!cs.IsClientScriptBlockRegistered(cstype, nombreScript))
                                {

                                    cs.RegisterStartupScript(cstype, nombreScript, sb.ToString(), true);

                                }

                            }

                        }

                        else
                        {

                            if (!cs.IsClientScriptBlockRegistered(cstype, nombreScript))
                            {

                                cs.RegisterStartupScript(cstype, nombreScript, sb.ToString(), true);

                            }

                        }

                        ViewState["Leer"] = sb.ToString();



                        if (Pagina.ToString() != "9" && Pagina.ToString() != null)
                        {

                            if (Pagina.ToString() != "0")
                            {
                                //if (Pagina.ToString() == "7" || ViewState["Grids"].ToString() == "8" || Session["Grids"].ToString() == "9")
                                if (Pagina.ToString() == "7" || Session["Grids"].ToString() == "8" || Session["Grids"].ToString() == "9")
                                { Session["Boton"] = "Guardar"; }
                                else { Session["Boton"] = "Generar"; }
                            }
                            if (Session["Boton"] != null)
                            {

                                //this.Controles.Controls.Add(new LiteralControl("<br /><input id='Button1' type='button' value='" + Session["Boton"].ToString() + "' onclick='leerValores();' class='btn btn-primary' />"));
                                //this.Controles.Controls.Add(new LiteralControl("<br /><div class='col-xs-12'><br /><input id='Button1' type='button' value='" + Session["Boton"].ToString() + "' onclick='leerValores();' class='btn btn-primary' /></div>"));

                                if (Session["Modal"] == null)
                                    this.Controles.Controls.Add(new LiteralControl("<br /><div class='col-xs-12'><br /><input id='Button1' type='button' value='" + Session["Boton"].ToString() + "' onclick='leerValores();' class='btn btn-primary' /></div>"));
                                else
                                    if (Session["Modal"].ToString() == "0")
                                    this.Controles.Controls.Add(new LiteralControl("<br /><div class='col-xs-12'><br /><input id='Button1' type='button' value='" + Session["Boton"].ToString() + "' onclick='leerValores();' class='btn btn-primary' /></div>"));
                                else
                                    if (ViewState["AS"].ToString() == "1")
                                    this.Controles.Controls.Add(new LiteralControl("<br /><div class='col-xs-12'><br /><input id='Button1' style='display:none;' type='button' value='" + Session["Boton"].ToString() + "' onclick='leerValores();' class='btn btn-primary' /></div>"));
                                else
                                    this.Controles.Controls.Add(new LiteralControl("<br /><div class='col-xs-12'><br /><input id='Button1' type='button' value='" + Session["Boton"].ToString() + "' onclick='leerValores();' class='btn btn-primary' /></div>"));

                            }
                        }
                    }




                    #endregion

                }
            }
        }

    }

    public void ExportarExcel(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(GetType(), "hideButton1", "$(function(){$('#Button1').hide();});", true);

        if (ViewState["Grids"] != null)
        {
            Session["Grids"] = ViewState["Grids"].ToString();
        }        

        Divgrid.Style.Add("display", "none");
        RGrid.Style.Add("display", "none");
        DReload.Style.Add("display", "block");
        DCombo.Style.Add("display", "none");
        Session["Pagina"] = "1";
        //string Pagina = ViewState["Grid"].ToString();
        string Pagina = (string)Session["Grids"];
        string FTP = (string)Session["TipoRep"];
        int R = Convert.ToInt32(Reporte.SelectedValue.ToString());

        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
        DataSet ReporteFinal = new DataSet();
        string s = QueryExec.Text.ToString() + scrip.Text.ToString();

        if (ViewState["CamposAGD"] != null)
        {
          string[] arrayCamposAGD = ViewState["CamposAGD"] as string[];
          string[] arrayGDResultado;
          arrayGDResultado = subirCampoGD(arrayCamposAGD);
          for (int z = 0; z <= arrayGDResultado.Length - 1; z++)
          {
            s += "," + arrayGDResultado[z];
          }
        }

        if (Pagina.ToString() == "11")
        {
            VerGrid.Checked = true;
        }

        if (Pagina.ToString() == "5" || Pagina.ToString() == "4")
        {
            DataSet DS_Datos2 = new DataSet();

            DS_Datos2 = Obj_Rep.Rep_Ejecuta(s, R, Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(Session["SistemaId"].ToString()));

            if (Obj_Rep.result_number != -1)
                ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('" + Obj_Rep.result_msg + "'," + Obj_Rep.result_number + ");", true);

            Session["DatosGrafica"] = null;
            Session["DatosGrafica"] = DS_Datos2;

            if (Pagina.ToString() == "5")
            {
                RGPay.Style.Add("display", "block");
                Registrar();
            }
            if (Pagina.ToString() == "4")
            {
                RGLinea.Style.Add("display", "block");
                Registrar2();
            }


        }
        else
        {
            if ((Pagina.ToString() != "3" && Pagina.ToString() != "6") && (VerGrid.Checked != true) && (FTP == "FTP"))
            {
                Pagina = "3";
            }


            if (Pagina.ToString() != "3" && Pagina.ToString() != "6")
            {
                if (VerGrid.Checked != true)
                {
                    #region Archivo
                    Divgrid.Style.Add("display", "none");
                    s = s.Replace("'", "''");
                    s= "'" + s + "'" + "," + R.ToString() + "," + Convert.ToInt32(Session["UsuarioId"].ToString()) + "," + Convert.ToInt32(Session["SistemaId"].ToString());
                    try
                    {
                        Response.Buffer = true;
                        using (SqlDataReader dr = Obj_Rep.Rep_Ejecuta(s))
                        {

                            if (Obj_Rep.result_number != -1)
                            {
                                
                                ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('" + Obj_Rep.result_msg + "'," + Obj_Rep.result_number + ");", true);
                            }
                          

                            StringBuilder sb = new StringBuilder();
                            int x = 1;
                            int y = 0;


                            string arch = "";
                            if (Archiv.Text == "")
                            {
                                arch = (string)Session["Archivo"] + "_F" + x.ToString();
                            }
                            else
                            {
                                arch = Archiv.Text + "_" + String.Format("{0:yyyyMMdd}", System.DateTime.Now);
                            }

                            do
                            {



                                if (x == 1)
                                {



																		if (Pagina.ToString() != "10" && Pagina.ToString() != "12")
																		{
                                        HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.csv", arch));
                                    }
                                    else { HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.txt", arch)); }
                                    Encoding TipoEnconding = Encoding.GetEncoding(int.Parse(Session["TipoEncoding"].ToString()));

                                    HttpContext.Current.Response.ContentEncoding = TipoEnconding; // System.Text.Encoding.Default;
                                    HttpContext.Current.Response.Charset = "";

                                    HttpContext.Current.Response.ContentType = "application/vnd.xls";


                                }

																if (Pagina.ToString() != "12")
																{
																	for (int count = 0; count < dr.FieldCount; count++)
																	{
																		if (dr.GetName(count) != null)
																			sb.Append(dr.GetName(count));
																		if (count < dr.FieldCount - 1)
																		{
																			sb.Append(",");
																		}
																	}

																	HttpContext.Current.Response.Write(sb.ToString() + "\n");
																	HttpContext.Current.Response.Flush();
																}

                                ////Append Data

                                sb = new StringBuilder();
                                if (dr.HasRows)
                                {

                                    while (dr.Read())
                                    {

                                        for (int col = 0; col < dr.FieldCount - 1; col++)
                                        {
																					if (Pagina.ToString() != "12")
																					{
																						if (!dr.IsDBNull(col))
																							sb.Append(dr.GetValue(col).ToString().Replace(",", " "));
																						sb.Append(",");
																					}
																					else
																					{
																						sb.Append(dr.GetValue(col).ToString());
																					}
																				}

																					if (Pagina.ToString() != "12")
																					{
																						if (!dr.IsDBNull(dr.FieldCount - 1))
																							sb.Append(dr.GetValue(dr.FieldCount - 1).ToString().Replace(",", " "));

																						HttpContext.Current.Response.Write(sb.ToString() + "\n");
																					}
																					else
																					{
																						sb.Append(dr.GetValue(dr.FieldCount - 1).ToString());
																						HttpContext.Current.Response.Write(sb);
																					}

																					sb.Clear();
																					HttpContext.Current.Response.Flush();

																				}
                                }
                                else
                                {
                                    sb.Append("Sin Informacion");


                                    HttpContext.Current.Response.Write(sb.ToString() + "\n");
                                    sb.Clear();
                                    HttpContext.Current.Response.Flush();
                                }


																if (Pagina.ToString() != "12")
																{
																	Response.Write("\n");
																	Response.Flush();
																}

																sb.Clear();


                                x = x + 1;
                            } while (dr.NextResult());
                            dr.Dispose();
                            HttpContext.Current.Response.Close();

                        }
                        
                    }
                    catch (Exception ex)
                    {
                        HttpContext.Current.Response.Write(ex.Message);
                    }
                    finally
                    {
                        HttpContext.Current.Response.Close();
                        Obj_Rep.Rep_Close(); //---se quedo abierta la conc 
                        HttpContext.Current.Response.Clear();
                        //Response.Redirect(Request.UrlReferrer.ToString(),true);
                    }





                    #endregion                    
                }
                else
                {
                    #region  grid
                    RGrid.Style.Add("display", "block");
                    DataSet DS_Datos = new DataSet();
                    DS_Datos = Obj_Rep.Rep_Ejecuta(s, R, Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(Session["SistemaId"].ToString()));

                    if (Obj_Rep.result_number != -1)
                        ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('" + Obj_Rep.result_msg + "'," + Obj_Rep.result_number + ");", true);

                    Session["DatosGrid"] = DS_Datos;
                    ViewState["Grids"] = "1";
                    Session["Grids"] = "1";

                    if (ViewState["DSOriginal"] == null)
                    {
                        ViewState["DSOriginal"] = DS_Datos;
                    }
                    else
                    {
                        DS_Datos = (DataSet)ViewState["DSOriginal"];
                    }

                    if (DS_Datos.Tables.Count >= 1)
                    {
                        DataTable DTAux = DS_Datos.Tables[0];

                        int intRegistros = DTAux.Rows.Count;
                        int intPorPagina = Convert.ToInt32(ViewState["RegGrid"].ToString());

                        if (intRegistros != 0)
                        {

                            if (DTAux.Rows.Count < intPorPagina)
                                intPorPagina = DTAux.Rows.Count;

                            int intPaginas = intRegistros / intPorPagina + (intRegistros % intPorPagina > 0 ? 1 : 0);
                            lblTotalClientes.Text = intRegistros.ToString();

                            TableHeaderRow tblTHR = new TableHeaderRow();
                            tblTHR.CssClass = "info";

                            int intX = 0; int intY = 0;
                            for (intX = 0; intX <= DTAux.Columns.Count - 1; intX++)
                            {
                                TableHeaderCell tblTHC = new TableHeaderCell();
                                tblTHC.Text = DTAux.Columns[intX].ColumnName;
                                tblTHR.Cells.Add(tblTHC);
                            }
                            tblGeneral.Rows.Add(tblTHR);


                            for (intX = 0; intX <= intPorPagina - 1; intX++)
                            {
                                TableRow tblTR = new TableRow();
                                tblTR.CssClass = "headerGrid";
                                tblTR.HorizontalAlign = HorizontalAlign.Left;
                                for (intY = 0; intY <= DTAux.Columns.Count - 1; intY++)
                                {
                                    TableCell tblC = new TableCell();
                                    tblC.Text = DTAux.Rows[intX][intY].ToString();
                                    tblTR.Cells.Add(tblC);
                                }
                                tblGeneral.Rows.Add(tblTR);
                            }

                            if (DS_Datos.Tables.Count > 1)
                            {

                                if (DS_Datos.Tables[DS_Datos.Tables.Count - 1].Columns.Contains("ResultadosDS"))
                                {
                                    for (intX = 0; intX <= DS_Datos.Tables[DS_Datos.Tables.Count - 1].Rows.Count - 1; intX++)
                                    {
                                        Resultados.Items.Insert(intX, DS_Datos.Tables[DS_Datos.Tables.Count - 1].Rows[intX]["ResultadosDS"].ToString());
                                    }
                                }
                                else
                                {
                                    for (intX = 0; intX <= DS_Datos.Tables.Count - 1; intX++)
                                    {
                                        Resultados.Items.Insert(intX, "Resultado " + (intX + 1).ToString());
                                    }
                                }


                                Resultados.Items[0].Selected = true;
                                variosGrid.Style.Add("display", "block");
                            }

                            for (intX = 0; intX <= intPaginas - 1; intX++)
                            {
                                Paginas.Items.Insert(intX, "Página " + (intX + 1).ToString());
                            }
                            variosPagina.Style.Add("display", "block");
                            Paginas.Items[0].Selected = true;

                            CurrentPageLabel.Text = "Página " + 1 + " de " + intPaginas;
                        }
                        else
                        {
                            lblMensajeConsulta.Text = "No hay resultados";
                        }
                    }
                    return;


                    #endregion
                }
            }
            else
            {
                #region Generar XML
                DataSet DS_Datos = new DataSet();
                DS_Datos = Obj_Rep.Rep_ArchivoXML(Convert.ToInt32(Session["UsuarioId"].ToString()), Convert.ToInt32(Reporte.SelectedValue.ToString()), s.ToString());


                if (DS_Datos != null)
                {
                    string ruta = DS_Datos.Tables[0].Rows[0][2].ToString() + DS_Datos.Tables[0].Rows[0][1].ToString();

                    string XmlText = DS_Datos.Tables[0].Rows[0][0].ToString();


                    System.IO.StreamWriter csvFileWriter = new StreamWriter(DS_Datos.Tables[0].Rows[0][2].ToString() + DS_Datos.Tables[0].Rows[0][1].ToString(), false);
                    csvFileWriter.WriteLine(XmlText);
                    csvFileWriter.Flush();
                    csvFileWriter.Close();


                }
                #endregion

            }
        }
        this.Controles.Controls.Clear();


        if (Pagina.ToString() != "2" && Pagina.ToString() != "5" && Pagina.ToString() != "4")
        {

            Divgrid.Style.Add("display", "none");


        }
        else { Divgrid.Style.Add("display", "none"); }




    }

    public void ConstruirTabla(Table Tabla, int Inicial, int Final, DataSet DS, int Consulta)
    {
        if (DS.Tables.Count >= 1)
        {
            DataTable DTAux = DS.Tables[Consulta];


            TableHeaderRow tblTHR = new TableHeaderRow();
            tblTHR.CssClass = "info";

            int intX = 0; int intY = 0;
            for (intX = 0; intX <= DTAux.Columns.Count - 1; intX++)
            {
                TableHeaderCell tblTHC = new TableHeaderCell();
                tblTHC.Text = DTAux.Columns[intX].ColumnName;
                tblTHR.Cells.Add(tblTHC);
            }
            tblGeneral.Rows.Add(tblTHR);


            for (intX = Inicial; intX <= Final; intX++)
            {
                TableRow tblTR = new TableRow();
                tblTR.CssClass = "headerGrid";
                tblTR.HorizontalAlign = HorizontalAlign.Left;
                for (intY = 0; intY <= DTAux.Columns.Count - 1; intY++)
                {
                    TableCell tblC = new TableCell();
                    tblC.Text = DTAux.Rows[intX][intY].ToString();
                    tblTR.Cells.Add(tblC);
                }
                tblGeneral.Rows.Add(tblTR);
            }
        }
    }

    public void DatosPage(object sender, EventArgs e)
    {
        DataSet DS_Datos = new DataSet();
        DS_Datos = (DataSet)ViewState["DSOriginal"];
        int rowsPage = Convert.ToInt32(ViewState["M_RegGrid"].ToString());

        int Tabla = Convert.ToInt32(Resultados.SelectedIndex.ToString());
        int NPagina = Convert.ToInt32(Paginas.SelectedIndex.ToString()) + 1;

        if (Tabla == -1)
            Tabla = 0;

        DataTable DT = DS_Datos.Tables[Tabla].Clone();
        DataRow DR = DS_Datos.Tables[Tabla].NewRow();

        int rows = Convert.ToInt32(DS_Datos.Tables[Tabla].Rows.Count.ToString());


        int RegIni = (((NPagina - 1) * rowsPage) + 1) - 1;
        int RegFin = ((NPagina) * rowsPage) - 1;
        if (RegFin >= rows)
        {
            RegFin = rows - 1;
        }


        //##### ##### ##### ##### #####
        ConstruirTabla(tblGeneral, RegIni, RegFin, DS_Datos, Tabla);
        int pag = rows / rowsPage + (rows % rowsPage > 0 ? 1 : 0);
        CurrentPageLabel.Text = "Página " + NPagina + " de " + pag;
        Div_Controles.Style.Add("display", "none");
        return;
        //##### ##### ##### ##### ##### CambiarPorTabla

    }

    public void DatosGrid(object sender, EventArgs e)
    {
        Paginas.SelectedIndex = 0;
        DataSet DS_Datos = new DataSet();

        DS_Datos = (DataSet)ViewState["DSOriginal"];
        int NResultados = Convert.ToInt32(DS_Datos.Tables.Count.ToString());

        int rowsPage = Convert.ToInt32(ViewState["M_RegGrid"].ToString());

        int Tabla = Convert.ToInt32(Resultados.SelectedIndex.ToString());

        if (Tabla == -1)
            Tabla = 0;

        int NPagina = 1;

        int Z = 0;
        ListItem item = new ListItem();
        Paginas.Items.Clear();

        int rows = Convert.ToInt32(DS_Datos.Tables[Tabla].Rows.Count.ToString());

        int pag = rows / rowsPage + (rows % rowsPage > 0 ? 1 : 0);

        if (pag == 0)
        {
            Z = Z + 1;
            item.Text = "Pagina " + Z.ToString();
            item.Value = "0";
            item.Selected = false;



            Paginas.Items.Insert(0, item.Text);
        }
        else
        {
            for (int q = 0; q < pag; q++)
            {
                Z = Z + 1;
                item.Text = "Pagina " + Z.ToString();
                item.Value = q.ToString();
                item.Selected = false;



                Paginas.Items.Insert(q, item.Text);

                if (q == 0)
                {
                    Paginas.Items[q].Selected = true;
                }
            }
        }
        Paginas.DataBind();


        DataTable DT = DS_Datos.Tables[Tabla].Clone();
        DataRow DR = DS_Datos.Tables[Tabla].NewRow();




        int RegIni = (((NPagina - 1) * rowsPage) + 1) - 1;
        int RegFin = ((NPagina) * rowsPage) - 1;
        if (RegFin >= rows)
        {
            RegFin = rows - 1;
        }


        //##### ##### ##### ##### #####
        ConstruirTabla(tblGeneral, RegIni, RegFin, DS_Datos, Tabla);
        lblTotalClientes.Text = rows.ToString();
        CurrentPageLabel.Text = "Página " + NPagina + " de " + pag;
        Div_Controles.Style.Add("display", "none");
        return;
        //##### ##### ##### ##### ##### CambiarPorTabla



    }

    protected void Btn_Actualiza(object sender, EventArgs e)
    {
        Response.Redirect("../Site/Principal.aspx");
    }

    public void Registrar()
    {
        //string connetionString = null;
        //SqlConnection cnn;
        //connetionString = "Data Source=cnovasrv-sql;Initial Catalog=comernova;User ID=sa;Password=sa";
        //cnn = new SqlConnection(connetionString);
        try
        {
            //            cnn.Open();
            //            SqlDataAdapter DA = new SqlDataAdapter(@"select SuperSegmento=isnull(Tip_Cartera+' - '+sup.nombre+' - '+inter.Descripcion,'ND'),
            //		Capital=sum(Capital)
            //from comernova..cr_Saldos_Cartera sal
            //		 left join tb_CatSucursal suc on suc.cve_Sucurs=sal.cve_Sucurs
            //		left join tb_CatSegmento seg on  seg.idsegmento=suc.idSegmento
            //		left join tb_CatIntersegmento inter on inter.IntersegmentoId=seg.intersegmentoid
            //		left join tb_Catsupersegmento sup on sup.supersegmentoid=inter.supersegmentoid
            //where fecha='2012-12-31'
            //group by inter.Descripcion,sup.nombre,sal.Tip_Cartera
            //
            //
            //select Titulo='Grafica',Subtitulo='Subtitulo',Tipo='x',Columna='Dia',Cordenadas='' ", cnn);
            //            DA.SelectCommand.CommandTimeout = 120;
            DataSet dt = new DataSet();
            dt = (DataSet)Session["DatosGrafica"];

            StringBuilder MyStringBuilder = new StringBuilder();
            ClientScriptManager cs = Page.ClientScript;

            if (dt != null && dt.Tables[0].Rows.Count > 0)
            {
                Titulo.Text = dt.Tables[1].Rows[0]["Titulo"].ToString();
                LblNombre.Text = dt.Tables[1].Rows[0]["SubTitulo"].ToString();
                string Dato = "<script type=\"text/javascript\">";
                MyStringBuilder.AppendLine(Dato);
                Dato = "$(function ()     {";
                MyStringBuilder.AppendLine(Dato);

                #region  data /*Da de alta el data*/

                Dato = "var data = [";
                MyStringBuilder.AppendLine(Dato);

                int p = 0;
                for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                {

                    //Dato = "\""+dt.Rows[i][0].ToString()+"\":"+"{ data: d" + i + ", label: \"" + dt.Rows[i][0].ToString() + "\" , points: { symbol:";
                    Dato = "{ label: \"" + dt.Tables[0].Rows[i][0].ToString() + "\", data: " + dt.Tables[0].Rows[i][1].ToString() + "}";

                    if (i != dt.Tables[0].Rows.Count - 1)
                    {
                        Dato = Dato + ',';
                    }

                    MyStringBuilder.AppendLine(Dato);
                    Dato = "";

                    if (p < 3) { p++; } else { p = 1; }
                }
                Dato = "];";
                MyStringBuilder.AppendLine(Dato);
                #endregion
                #region Resto /*Resto de codigo jquery*/

                Dato = @"  
$.plot($(""#placeholder""), data, 
{
        series: {
            pie: { ";

                if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "DONA")
                { Dato = Dato + "innerRadius: 0.5,"; }
                Dato = Dato + @"show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 3/4,
                    formatter: function(label, series){
                        return '<div style=""font-size:8pt;text-align:center;padding:2px;color:white;"">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.5 }
                }
            }
        },
        legend: {
            show: false
        }
        ,
        grid: {
            hoverable: true,
            clickable: true
        }
});
$(""#placeholder"").bind(""plothover"", pieHover);
//$(""#placeholder"").bind(""plotclick"", pieClick);

                                    });





function pieHover(event, pos, obj)
{
        percent = parseFloat(obj.series.percent).toFixed(2);
        $(""#hover"").html('<span style=""font-weight: bold; color: '+obj.series.color+'"">'+obj.series.label+' ('+percent+'%)</span>');
        [tooltip].visible = true;
        //alert(pos.pageY);
         
        texto=''+obj.series.label+': '+percent+'%'+' Valor: '+obj.series.data[obj.dataIndex][1];
        
        var myElement = document.getElementById('tooltip');
        myElement.innerHTML = texto;

       myElement.style.top=''+pos.pageY+'px';
       myElement.style.left=''+pos.pageX+'px';
}

function pieClick(event, pos, obj)
{
        
        percent = parseFloat(obj.series.percent).toFixed(2);
        [tooltip].visible = true;
        //alert(pos.pageY);
         
        texto=''+obj.series.label+': '+percent+'%'+' " + dt.Tables[1].Rows[0]["Columna"].ToString().ToUpper() + @": '+obj.series.data;
        
        var myElement = document.getElementById('tooltip');
        myElement.innerHTML = texto;

       myElement.style.top=''+pos.pageY+'px';
       myElement.style.left=''+pos.pageX+'px';
       

}


</script>";
                MyStringBuilder.AppendLine(Dato);

                //Dato = "$.plot($(\"#placeholder\"), data,{series: { points: { show: true, radius: 4 } },grid: { hoverable: true },xaxis: {labelAngle: 45,ticks: TickGenerator},legend:{container: $(\"#labeler\")}} );";
                //MyStringBuilder.AppendLine(Dato);
                //Dato = "$.plot($(\"#placeholder2\"),data,{series: { points: { show: true, radius: 4 } },grid: { hoverable: true },xaxis: {labelAngle: 45,ticks: TickGenerator},legend:{container: $(\"#labeler2\")}});";
                //MyStringBuilder.AppendLine(Dato);

                Type c = this.GetType();
                cs.RegisterClientScriptBlock(c, "jquery", MyStringBuilder.ToString()); //MyStringBuilder.AppendLine(Dato);
                #endregion
            }


        }
        catch (Exception ex)
        {
            LblError.Text = "Error: " + ex.ToString();
        }
        finally
        {

        }
    }
    public void Registrar2()
    {

        try
        {

            //SqlDataAdapter DA = new SqlDataAdapter("exec rep_RepCurvasBrutasGrafica 200705,201112,'RECUP',1,'NUE'", cnn);


            DataSet dt = new DataSet();
            dt = (DataSet)Session["DatosGrafica"];


            StringBuilder MyStringBuilder = new StringBuilder();
            ClientScriptManager cs = Page.ClientScript;

            if (dt != null && dt.Tables[0].Rows.Count > 0)
            {
                Titulo2.Text = dt.Tables[1].Rows[0]["Titulo"].ToString();
                LblNombre2.Text = dt.Tables[1].Rows[0]["SubTitulo"].ToString();
                string Dato = "<script type=\"text/javascript\">";
                MyStringBuilder.AppendLine(Dato);
                Dato = "$(function ()     {";
                MyStringBuilder.AppendLine(Dato);
                #region variable

                string ticks = "";
                if (dt.Tables.Contains("Table2") && dt.Tables[2].Rows.Count > 0)
                {
                    ticks = "ticks: [";
                    for (int i = 0; i < dt.Tables[2].Rows.Count; i++)
                    {

                        ticks = ticks + "[" + i.ToString() + ",'" + Convert.ToString(dt.Tables[2].Rows[i][0].ToString()) + "']";
                        if (i + 1 != dt.Tables[2].Rows.Count)
                        {
                            ticks = ticks + ',';
                        }
                    }

                    ticks = ticks + @"]";
                    if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "LINEA") { ticks = ticks + "},"; }
                }
                else
                {
                    if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() != "LINEA") { ticks = ""; }
                    else { ticks = "ticks: TickGenerator },"; }

                }
                for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                {

                    Dato = "var d" + i + "=[";
                    for (int y = 1; y < dt.Tables[0].Columns.Count; y++)
                    {
                        int v = y - 1;
                        if (dt.Tables[0].Rows[i][y].ToString() != "")
                        {
                            if (dt.Tables[1].Rows[0]["Cordenadas"].ToString() == "")
                            {
                                Dato = Dato + '[' + v.ToString() + ',' + Convert.ToString(Convert.ToDouble(dt.Tables[0].Rows[i][y].ToString())) + ']';
                            }
                            else
                            {
                                if (dt.Tables[1].Rows[0]["Cordenadas"].ToString() == "XY")
                                {
                                    Dato = Dato + '[' + Convert.ToString(Convert.ToDouble(dt.Tables[0].Rows[i][y].ToString())) + ',' + Convert.ToString(Convert.ToDouble(dt.Tables[0].Rows[i][y + 1].ToString())) + ']';
                                    y++;
                                }
                                else
                                {
                                    Dato = Dato + Convert.ToString(Convert.ToDouble(dt.Tables[0].Rows[i][y].ToString()));
                                }
                            }

                            if (y != dt.Tables[0].Columns.Count - 1 && dt.Tables[0].Rows[i][y + 1].ToString() != "")
                            {
                                Dato = Dato + ',';
                            }
                        }
                    }

                    Dato = Dato + "];";

                    MyStringBuilder.AppendLine(Dato);
                    Dato = "";

                }
                #endregion
                #region  data /*Da de alta el data y le pone los simbolos*/

                if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "LINEA")
                {
                    Dato = "var data = {";
                }
                else
                {
                    if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "BARRA")
                    {
                        Dato = "var data = [";
                    }
                    {
                        Dato = "var data = [";
                    }
                }
                MyStringBuilder.AppendLine(Dato);

                int p = 0;
                for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                {

                    //Dato = "\""+dt.Rows[i][0].ToString()+"\":"+"{ data: d" + i + ", label: \"" + dt.Rows[i][0].ToString() + "\" , points: { symbol:";






                    if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "LINEA")
                    {
                        Dato = "\"" + dt.Tables[0].Rows[i][0].ToString() + "\":" + "{ data: d" + i + ", label: \"" + dt.Tables[0].Rows[i][0].ToString();
                        if (p == 0) { Dato = Dato + "\" , points: { symbol:" + "\"circle\""; }
                        if (p == 1) { Dato = Dato + "\" , points: { symbol:" + "\"square\""; }
                        if (p == 2) { Dato = Dato + "\" , points: { symbol:" + "\"diamond\""; }
                        if (p == 3) { Dato = Dato + "\" , points: { symbol:" + "\"triangle\""; }
                        if (p == 4) { Dato = Dato + "\" , points: { symbol:" + "\"cross\""; }

                        if (dt.Tables[0].Rows[i][0].ToString().ToUpper() == "PROMEDIO")
                        {
                            Dato = Dato + "} , lines: { show: true, fill: true }, color: '#FFFF00'}";
                        }
                        else
                        {
                            Dato = Dato + "} , lines: { show: true, fill: false }, color: '#92d5ea'}";
                        }
                    }

                    if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "BARRA")
                    {
                        Dato = " {data: d" + i + ", label: \"" + dt.Tables[0].Rows[i][0].ToString();
                        Dato = Dato + "\"," + @" bars: {
                                                show: true,
                                                fill: true,
                                                lineWidth: 0,
                                                order: 0,
                                                barWidth: 0.2";
                        ;

                        /*if (p == 0) { Dato = Dato + "#AA4643\"},color: \"#AA4643\"}"; }
                        if (p == 1) { Dato = Dato + "#89A54E\"},color: \"#89A54E\"}"; }
                        if (p == 2) { Dato = Dato + "#4572A7\"},color: \"#4572A7\"}"; }
                        if (p == 3) { Dato = Dato + "#80699B\"},color: \"#80699B\"}"; }
                        if (p == 4) { Dato = Dato + "#AA4643\"},color: \"#AA4643\"}"; }*/
                        if (p == 0) { Dato = Dato + "}}"; }
                        if (p == 1) { Dato = Dato + "}}"; }
                        if (p == 2) { Dato = Dato + "}}"; }
                        if (p == 3) { Dato = Dato + "}}"; }
                        if (p == 4) { Dato = Dato + "}}"; }



                    }

                    if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "SPIDER")
                    {
                        Dato = " { label: \"" + dt.Tables[0].Rows[i][0].ToString() + "\",data: d" + i + ", spider: {show: true}}";
                    }


                    if (i != dt.Tables[0].Rows.Count - 1)
                    {
                        Dato = Dato + ',';
                    }

                    MyStringBuilder.AppendLine(Dato);
                    Dato = "";

                    if (p < 3) { p++; } else { p = 1; }
                }


                if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "LINEA")
                {
                    Dato = "}";
                }
                else
                {
                    if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "BARRA")
                    {
                        Dato = "];";
                    }
                    else
                    {
                        Dato = "]";
                    }
                }
                MyStringBuilder.AppendLine(Dato);

                #region Aristas Spider
                if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "SPIDER")
                {

                    Dato = @" var p1,p2,data,options; 
                            options = { series:
                            { spider:
                                        { active: true
                     ,highlight: {mode: ""area""}
                     ,legs: { data: [";

                    for (int i = 0; i < dt.Tables[3].Rows.Count; i++)
                    {

                        Dato = Dato + "{label: \"" + Convert.ToString(dt.Tables[3].Rows[i][0].ToString()) + "\"}";
                        if (i + 1 != dt.Tables[3].Rows.Count)
                        {
                            Dato = Dato + ',';
                        }
                    }

                    Dato = Dato + @"]
                                                        , legScaleMax: 1, legScaleMin:0.8}
                     ,spiderSize: 0.9        }
                    }
                   ,grid:
                                    { hoverable: true
                                         ,clickable: true
                                         ,tickColor: ""rgba(0,0,0,0.2)""
                                         ,mode: ""Spider""
                                        }
                  };";

                    MyStringBuilder.AppendLine(Dato);
                }


                #endregion


                #endregion
                #region Resto /*Resto de codigo jquery Linea*/
                if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "LINEA")
                {
                    Dato = @"   $('<div class=""button"" style=""right:20px;top:20px"">zoom out</div>').appendTo(placeholder2).click(function (e) {e.preventDefault();plot.zoomOut();});
                                function showTooltip(x, y, contents) 
                                    {
                                        $('<div id=""tooltip"">' + contents + '</div>').css
                                            ( 
                                                {
                                                    position: 'absolute',
                                                    display: 'none',
                                                    top: y + 6,
                                                    left: x +6,
                                                    order: '1px solid #fdd',
                                                    padding: '2px',
                                                    'background-color': '#fee',
                                                    opacity: 0.80
                                                }
                                            ).appendTo(""body"").fadeIn(200);
                                    }

                                var previousPoint = null; 
                                        
                                var i = 0;
                                $.each
                                    (
                                        data, 
                                        function(key, val) 
                                            {
                                                val.color = i;
                                                ++i;}
                                     );
                             var choiceContainer = $(""#choices"");
                              $.each
                              (
                                data, 
                                function(key, val) 
                                    {
                                            
                                        choiceContainer.append('</br><input type=""checkbox"" name=""' + key +'"" checked=""checked"" id=""id' + key + '"">' 
                                        +'<label style=""background-color:'+ val.color + '"";"" id=""Lid' + key + '"">'+ val.label + '</label>');               
                                    }
                                
                               );

                        

                            choiceContainer.find(""input"").click(plotAccordingToChoices);
                            function plotAccordingToChoices() 
                                        {
                                            var data2 = [];
                                            choiceContainer.find(""input:checked"").each
                                                (
                                                    function () 
                                                        {
                                                            var key = $(this).attr(""name"");
                                                            if (key && data[key])
                                                                data2.push(data[key]);});
                                                            if (data2.length > 0)
                                                                $.plot
                                                                    (
                                                                      $(""#placeholder2""), 
                                                                      data2, 
                                                                      {
                                                                        series: { points: { show: true, radius: 4 }},
                                                                        grid: { hoverable: true },
                                                                        xaxis: {labelAngle: 45," + ticks.ToString() + @"
                                                                        legend:{container: $(""#labeler"")}
                                                                       }
                                                                    );
                                                        }
                                                    
                            plotAccordingToChoices();

                            $("".grafica"").bind
                                    (
                                        ""plothover"",
                                        function (event, pos, item)
                                            {
                                                $(""#x"").text(pos.x.toFixed(2));
                                                $(""#y"").text(pos.y.toFixed(2));
                                                if ($(""#enableTooltip:checked"").length > 0) 
                                                    {
                                                        if (item)
                                                            {
                                                                if (previousPoint != item.datapoint)
                                                                    {
                                                                        previousPoint = item.datapoint;
                                                                        $(""#tooltip"").remove();
                                                                        var x = item.datapoint[0].toFixed(0),
                                                                            y = item.datapoint[1].toFixed(2);
                                                                        showTooltip(item.pageX, item.pageY,item.series.label + "" ""+""" + dt.Tables[1].Rows[0]["Columna"].ToString().ToUpper() + @""" + x + "" = "" + y);
                                                                    }
                                                            }
                                                        else 
                                                          {
                                                            $(""#tooltip"").remove();
                                                            previousPoint = null
                                                           }
                                                    }
                                        }
                                    );
                                    function TickGenerator(axis)
                                        {
                                            var res = [],
                                            i = Math.floor(axis.min)-1;
                                            do {
                                                    res.push([i,  + i]);
                                                    ++i;
                                                } while (i < axis.max+1);
                                            return res;
                                        }

                                    $(document).ready
                                        (
                                            function()
                                              {   //nuevo
                                                $(""#Filtrar"").click
                                                    (
                                                        function (evento)
                                                            {
                                                                if ($(""#Filtrar"").attr(""checked""))
                                                                    {
                                                                        //si el checbox es seleccionado muesro el div
                                                                        $(""#choices"").css(""display"", ""inline-block"");
                                                                     }
                                                                else {
                                                                        $(""#choices"").css(""display"", ""none"");
                                                                     }
                                                              }
                                                     );
                                                  });
                                    });</script>";
                }
                #endregion
                else
                {
                    #region Resto /*Resto de codigo jquery BARRA*/
                    if (dt.Tables[1].Rows[0]["Tipo"].ToString().ToUpper() == "BARRA")
                    {
                        Dato = @"$('<div class=""button"" style=""right:20px;top:20px"">zoom out</div>').appendTo(placeholder2).click(function (e) {e.preventDefault();plot.zoomOut();});
                                function showTooltip(x, y, contents) 
                                    {
                                        $('<div id=""tooltip"">' + contents + '</div>').css
                                            ( 
                                                {
                                                    position: 'absolute',
                                                    display: 'none',
                                                    top: y + 6,
                                                    left: x +6,
                                                    order: '1px solid #fdd',
                                                    padding: '2px',
                                                    'background-color': '#fee',
                                                    opacity: 0.80
                                                }
                                            ).appendTo(""body"").fadeIn(200);
                                    }


                                     var previousPoint = null; 
                                        
                                var i = 0;
                                $.each
                                    (
                                        data, 
                                        function(key, val) 
                                            {
                                                val.color = i;
                                                ++i;}
                                     );
                             var choiceContainer = $(""#choices"");
                              $.each
                              (
                                data, 
                                function(key, val) 
                                    {
                                            
                                        choiceContainer.append('</br><input type=""checkbox"" name=""' + key +'"" checked=""checked"" id=""id' + key + '"">' 
                                        +'<label style=""background-color:'+ val.color + '"";"" id=""Lid' + key + '"">'+ val.label + '</label>');               
                                    }
                                
                               );
                            

                            choiceContainer.find(""input"").click(plotAccordingToChoices);
                            function plotAccordingToChoices() 
                                        {
                                            var data2 = [];
                                            choiceContainer.find(""input:checked"").each
                                                (
                                                    function () 
                                                        {
                                                            var key = $(this).attr(""name"");
                                                            if (key && data[key])
                                                                data2.push(data[key]);});
                                                            if (data2.length > 0)
                                                                $.plot
                                                                    (
                                                                      $(""#placeholder2""), data2,{stack: 0,
                 waterfall: true,
                 lines: {show: false, steps: false },
                 bars: {show: true, barWidth: 0.9, align: 'center'},
        xaxis: {" + ticks.ToString() + @"},
        yaxis: {
            axisLabel: 'Value',
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            axisLabelPadding: 5
        },
            grid: { hoverable: true },
                                                                        
                                                                        legend:{container: $(""#labeler"")}});

                                                        }
                                                    
                            plotAccordingToChoices();


            



                                     $("".grafica"").bind
                                    (
                                        ""plothover"",
                                        function (event, pos, item)
                                            {
                                                $(""#x"").text(pos.x.toFixed(2));
                                                $(""#y"").text(pos.y.toFixed(2));
                                                if ($(""#enableTooltip:checked"").length > 0) 
                                                    {
                                                        if (item)
                                                            {
                                                                if (previousPoint != item.datapoint)
                                                                    {
                                                                        previousPoint = item.datapoint;
                                                                        $(""#tooltip"").remove();
                                                                        var x = item.datapoint[0].toFixed(0),
                                                                            y = item.datapoint[1].toFixed(2);
                                                                        showTooltip(item.pageX, item.pageY,item.series.label +"" ""+ """ + dt.Tables[1].Rows[0]["Columna"].ToString().ToUpper() + @" "" + x + "" = "" + y);
                                                                    }
                                                            }
                                                        else 
                                                          {
                                                            $(""#tooltip"").remove();
                                                            previousPoint = null
                                                           }
                                                    }
                                        }
                                    );
                                
                                    $(document).ready
                                        (
                                            function()
                                              {   //nuevo
                                                $(""#Filtrar"").click
                                                    (
                                                        function (evento)
                                                            {
                                                                if ($(""#Filtrar"").attr(""checked""))
                                                                    {
                                                                        //si el checbox es seleccionado muesro el div
                                                                        $(""#choices"").css(""display"", ""inline-block"");
                                                                     }
                                                                else {
                                                                        $(""#choices"").css(""display"", ""none"");
                                                                     }
                                                              }
                                                     );
                                                  });

                                                     
});</script>";
                        #endregion
                    }
                    else
                    {
                        #region Resto /*Resto de codigo jquery SPIDER*/
                        Dato = @"
$(""#ocultar"").css(""display"", ""none"");
$(""#ocultar2"").css(""display"", ""none"");

    p1 = $.plot($(""#placeholder2""), data , options);
    p2 = $.plot($(""#placeholderhover""), data, options);
});

$(""#placeholder2"").bind(""plothover"", pieHover);

$(""#placeholder2"").bind(""plotclick""
                      , function (event, pos, item)
                                            { $(""#serie"").html(pos.pageX + "" : "" + pos.pageY + ""<br>""
                                          + item.series.spider.legs.data[item.dataIndex]
                                          + ""("" + item.series.label + "")="" + item.datapoint[1]);



    });
$(document).ready(function(){ $(""#tabs"").tabs(); });
$(""#sethovermode"").click(function () {setHoverMode() });
function setHoverMode()
{        var mode;
    mode = $(""#hovermode"").val();
        options.series.spider.highlight.mode = mode;
        mode = $(""#spidermode"").val();
        options.grid.mode = mode;
        mode = $(""#scalemode"").val();
        options.series.spider.scaleMode = mode;
        if(mode == 'static')
        { options.series.spider.legMin = parseFloat($(""#legMin"").val());
          options.series.spider.legMax = parseFloat($(""#legMax"").val());
        }
        options.series.spider.legs.legStartAngle = $(""#legStartAngle"").val();
        p2 = $.plot($(""#placeholderhover""), data, options);
}

function pieHover(event, pos, obj)
{
        alert(pos.pageY);
        percent = parseFloat(obj.series.percent).toFixed(2);
        $(""#hover"").html('<span style=""font-weight: bold; color: '+obj.series.color+'"">'+obj.series.label+' ('+percent+'%)</span>');
        [tooltip2].visible = true;
        
         
        texto=''+obj.series.label+': '+percent+'%'+' Valor: '+obj.series.data[obj.dataIndex][1];
        
        var myElement = document.getElementById('tooltip2');
        myElement.innerHTML = texto;

       myElement.style.top=''+pos.pageY+'px';
       myElement.style.left=''+pos.pageX+'px';
}
</script>
";
                        #endregion
                    }
                }




                MyStringBuilder.AppendLine(Dato);

                Type c = this.GetType();
                cs.RegisterClientScriptBlock(c, "jquery", MyStringBuilder.ToString()); //MyStringBuilder.AppendLine(Dato);

            }

            LblError2.Text = "Grafica Lista";
        }
        catch (Exception ex)
        {
            LblError2.Text = "Error: " + ex.ToString();
        }
        finally
        {

        }
    }

    void CerrarM(object sender, EventArgs e)
    {
        DatosGrid(sender, e);
        Session.Remove("Modal");
        Session.Remove("DatosGrid");

        if (Session["lstRedirect"] != null)
        {
            List<string> lstRedirect = new List<string>();
            lstRedirect = (List<string>)Session["lstRedirect"];
            int intMaxURL = lstRedirect.Count;
            lstRedirect.RemoveAt(intMaxURL - 1);

            if (lstRedirect.Count > 0)
            {
                ClientScript.RegisterStartupScript(GetType(), "iFrameRedirect", "$(function(){ iFrameRedirect('" + lstRedirect[intMaxURL - 2] + "'); });", true);
            }
            else
            {
                Session.Remove("lstRedirect");
                Session.Remove("AutoSubmit");
            }
        }

    }

    public void readQS()
    {
        if (!IsPostBack) return;
        if (Session["Modal"] == null) return;

        try
        {
            int x;
            int y;
            string AutoSubmit;
            string ReadOnly;

            for (x = 0; x <= Request.QueryString.Count - 1; x++)
            {
                if (Request.QueryString.GetKey(x).ToString() == "Modal")
                    break;
            }

            // LAS INDIVIDUALES START
            x++;
            if (Request.QueryString.GetKey(x).ToString() == "AS")
            {
                AutoSubmit = Request.QueryString["AS"].ToString();
                ViewState["AutoSubmit"] = AutoSubmit;
            }
            else
                ViewState["AutoSubmit"] = 0;

            x++;
            if (Request.QueryString.GetKey(x).ToString() == "RO")
            {
                ReadOnly = Request.QueryString["RO"].ToString();
                ViewState["ReadOnly"] = ReadOnly;
            }
            else
                ViewState["ReadOnly"] = 0;

            if (Request.QueryString["CT"] != null)
                x++;

            if (Request.QueryString["NA"] != null)
                x++;
            // LAS INDIVIDUALES END

            //QUERYSTRING
            if (x < Request.QueryString.Count - 1)
            {
                arrayCampos = new string[Request.QueryString.Count - x - 1];
                y = 0;
                for (++x; x <= Request.QueryString.Count - 1; x++)
                {
                    arrayCampos[y] = Request.QueryString.GetKey(x) + '§' + Request.QueryString[x];
                    y++;
                }
                cargarCampos(arrayCampos);
            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {                             
        if (Session["Modal"] != null)
            if (Session["Modal"].ToString() != "0")
            {
                ClientScript.RegisterStartupScript(GetType(), "OcultarModal", "$('#btnRegresar').hide();", true);
                Reporte.Style.Add("display", "none");
                if (ViewState["AS"].ToString() == "1")
                {
                    Div_Controles.Style.Add("display", "none");
                }

            }

        if (Session["Modal"] == null)
            ClientScript.RegisterStartupScript(GetType(), "showRegresar_2", "$(function(){$('#btnRegresar').show();});", true);
        else
            if (Session["Modal"].ToString() == "0")
            ClientScript.RegisterStartupScript(GetType(), "showRegresar_2", "$(function(){$('#btnRegresar').show();});", true);

        if (ViewState["Leer"] != null)
        {
            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "leer"))
            {
                if (ViewState["AutoSubmit"] != null)
                {
                    if (ViewState["AutoSubmit"].ToString() == "0")
                    {
                        ClientScript.RegisterStartupScript(GetType(), "leer", "<script type=\"text/javascript \" >" + ViewState["Leer"].ToString() + "</script>", false);
                        //ClientScript.RegisterStartupScript(GetType(), "leer", "<script type=\"text/javascript \" >" + ViewState["Leer"].ToString() + "</script>", true);
                        //ClientScript.RegisterStartupScript(GetType(), "leer", ViewState["Leer"].ToString() , false);
                        //ClientScript.RegisterStartupScript(GetType(), "leer", ViewState["Leer"].ToString(), false);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "leer", "<script type=\"text/javascript \" > function leerValores(){var x=5;} </script>", false);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "leer", "<script type=\"text/javascript \" >" + ViewState["Leer"].ToString() + "</script>", false);
                }
            }
        }

        if (!IsPostBack)
        {
            if (Session["Modal"] != null && ViewState["strValor"] != null && ViewState["SubmitModal"] == null)
            {
                ViewState["SubmitModal"] = "Listo";
                string strJS = "$('#ctl00_MainContent_Reporte').val(" + ViewState["strValor"] + ");";
                strJS += ClientScript.GetPostBackEventReference(Reporte, "");
                ClientScript.RegisterStartupScript(GetType(), "asignaValorReporte", strJS, true);
            }
            ComboControles.Style.Add("display", "Block");
            MakeConrtrols.Style.Add("display", "Block");
            return;//ver con jair
        }

        if (Session["Modal"] == null)
        {
            return;
        }



        try
        {
            if (arrayCampos != null)
                if (arrayCampos.Length > 0)
                    cargarCampos(arrayCampos);

        }
        catch (Exception ex)
        {

        }
        if (ViewState["AutoSubmit"] != null)
        {
            if (ViewState["AutoSubmit"].ToString() == "1")
            {
                ClientScript.RegisterStartupScript(GetType(), "AutoSubmit", "$(function(){if(document.getElementById('Button1'))document.getElementById('Button1').click();});", true);
            }
        }

    }

    bool cargarCampos(string[] arrayCampos)
    {
        bool resultado = false;
        try
        {
            Control _CTRL;
            ControlCollection _CTRL_CO;
            bool ReadOnly = false;
            if (ViewState["ReadOnly"].ToString() == "1")
                ReadOnly = true;

            for (int a = 0; a <= arrayCampos.Length - 1; a++)
            {

                _CTRL = Page.Controls[0].FindControl("Form1").FindControl("MainContent").FindControl(arrayCampos[a].ToString().Split('§')[0]);
                if (_CTRL == null)
                    return false;
                _CTRL_CO = _CTRL.Controls;

                for (int x = 2; x <= _CTRL_CO.Count - 1; x++)
                {
                    if (_CTRL_CO[x].HasControls())
                    {
                        bool blnTodoVisible = true;
                        for (int y = 0; y <= _CTRL_CO[x].Controls.Count - 1; y++)
                        {
                            if (!_CTRL_CO[x].Controls[y].Visible)
                            {
                                blnTodoVisible = false;
                                break;
                            }
                        }
                        if (blnTodoVisible)
                        {
                            for (int z = 0; z <= _CTRL_CO[x].Controls.Count - 1; z++)
                            {
                                if (_CTRL_CO[x].Controls[z] is TextBox)
                                {
                                    TextBox txt = (TextBox)_CTRL_CO[x].Controls[z];
                                    txt.Text = arrayCampos[a].ToString().Split('§')[1];
                                    txt.ReadOnly = ReadOnly;
                                    break;

                                }
                                if (_CTRL_CO[x].Controls[z] is DropDownList)
                                {
                                    DropDownList ddl = (DropDownList)_CTRL_CO[x].Controls[z];
                                    ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(arrayCampos[a].ToString().Split('§')[1]));
                                    ddl.Enabled = !ReadOnly;
                                    break;
                                }
                            }

                        } // Todos CTRLs Visibles
                    } // Tiene CTRLs
                } // BuscarCTRL
            } // arrayCampos


            resultado = true;
        }
        catch (Exception ex)
        {

        }
        return resultado;
    }

  #region GoogleDrive  

    string[] subirCampoGD(string[] arrayCamposAGD)
    {
      string strError = "";
      if (!cnxGoogle(ref strError))
      {
        return null;
      }

      List<string> lstArchivoGD = new List<string>();

      int intKeys = Request.Form.Keys.Count;
      int x, y;
      string strIDDirectorio, strIDArchivo, strURL;

      for (x = 0; x <= arrayCamposAGD.Length - 1; x++)
      {
        strIDDirectorio = strIDArchivo = strURL = string.Empty;

        for (y = 0; y <= intKeys - 1; y++)
        {
          if (Request.Form.Keys[y].Contains(arrayCamposAGD[x]))
          {
            if (Request.Form.Keys[y].Contains("gdIDDirectorio"))
              strIDDirectorio = Request.Form[y];
            if (Request.Form.Keys[y].Contains("gdIDArchivo"))
              strIDArchivo = Request.Form[y];
            if (Request.Form.Keys[y].Contains("gdURL"))
              strURL = Request.Form[y];
          }
          if (strIDDirectorio != "" && strIDArchivo != "" && strURL != "")
            break;
        }

        if (strIDDirectorio != "" && strIDArchivo != "" && strURL != "")
        {
          HttpPostedFile Archivo = (HttpPostedFile)Request.Files[x];
          if (Archivo.ContentLength != 0)
          {
            //Manda Auxiliarii
            string auxArchivo, auxURL;
            auxArchivo = auxURL = string.Empty;
            if (subirArchivo(strIDDirectorio, Archivo, ref auxArchivo, ref auxURL, ref strError))
            {
              lstArchivoGD.Add(strIDArchivo + "='" + auxArchivo + "'," + strURL + "='" + auxURL + "'");
            }
          }
          else
            lstArchivoGD.Add(strIDArchivo + "=''," + strURL + "=''");
        }
      }

      return lstArchivoGD.ToArray();
    }

    private bool cnxGoogle(ref string Error)
    {
        Boolean resultado = false;
        Error = "";
        try
        {

            strAppPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            string strAuthPath = strAppPath + "\\Auto\\auth" + "\\";

            if (!System.IO.File.Exists(strAuthPath + "SistemasOA.json"))
            {
                Error = "No existe archivo json";
                return false;
            }

            using (var stream = new FileStream(strAuthPath + "SistemasOA.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = strAuthPath + "user_json";

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "infraGH",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                resultado = true;
            }
        }
        catch (Exception ex)
        {
            Error = ex.Message;
        }
        return resultado;
    }

    private bool subirArchivo(string IDDirectorio, HttpPostedFile Archivo, ref string IDArchivo, ref string IDUrl, ref string Error)
    {
        bool blnRetorno = false;
        Error = ""; IDArchivo = "";
        try
        {
            //FileInfo fiArchivo = new FileInfo(Archivo);
            string[] scopes = new string[] { DriveService.Scope.Drive };

            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "appDrive",
            });

            Google.Apis.Drive.v2.Data.File body = new Google.Apis.Drive.v2.Data.File();
            //body.Title = fiArchivo.Name;
            body.Title = Archivo.FileName;
            body.Description = "Archivo de usuario";
            //body.MimeType = "application/pdf";
            body.MimeType = Archivo.ContentType;

            body.Parents = new List<ParentReference> { new ParentReference() { Id = IDDirectorio } };
            //byte[] byteArray = System.IO.File.ReadAllBytes(fiArchivo.FullName);
            byte[] byteArray = new byte[Archivo.ContentLength];
            Archivo.InputStream.Read(byteArray, 0, Archivo.ContentLength);
            System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);

            FilesResource.InsertMediaUpload request = service.Files.Insert(body, stream, Archivo.ContentType);
            request.Upload();
            Google.Apis.Drive.v2.Data.File archivoGD = request.ResponseBody;
            if (archivoGD != null)
            {
                IDArchivo = archivoGD.Id.ToString();
                IDUrl = archivoGD.WebContentLink;

                /*Las siguientes instrucciones permiten compartir un archivo por medio de LINK(público)*/
                /*
                Permission PermisosArchivo = new Permission();
                PermisosArchivo.Type = "anyone";
                PermisosArchivo.Role = "reader";
                PermisosArchivo.Value = "";
                PermisosArchivo.WithLink = true;
                service.Permissions.Insert(PermisosArchivo, archivoGD.Id).Execute();
                */

                blnRetorno = true;
            }
            else
            {
                Error = "No fue posible subir el archivo";
            }
        }
        catch (Exception ex)
        {
            Error = ex.Message;
        }
        return blnRetorno;
    }

    public bool descargarArchivo(string IDArchivo, ref string URLArchivo, ref string Error)
    {
        bool blnResultado = false;
        Error = "";
        if (cnxGoogle(ref Error))
        {
            try
            {
                var service = new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "appDrive",
                });

                Google.Apis.Drive.v2.Data.File fileGoogle = service.Files.Get(IDArchivo).Execute();
                var stream = DownloadFile(fileGoogle, ref Error);
                if (stream != null)
                {
                    string PathArchivo = strAppPath + "\\Auto\\TMPDocumentos\\";
                    URLArchivo = "TMPDocumentos\\" + fileGoogle.Id + "." + fileGoogle.FileExtension;
                    FileStream FS = new FileStream(PathArchivo + fileGoogle.Id + "." + fileGoogle.FileExtension, FileMode.Create);
                    stream.CopyTo(FS);
                    FS.Close();
                    blnResultado = true;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
            }
        }
        return blnResultado;
    }

    public static System.IO.Stream DownloadFile(Google.Apis.Drive.v2.Data.File file, ref string Error)
    {
        Error = "";
        if (!String.IsNullOrEmpty(file.DownloadUrl))
        {
            try
            {
                var service = new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "appDrive",
                });

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(file.DownloadUrl));
                //El Header Authorization lo agregamos de forma manual
                //authenticator.ApplyAuthenticationToRequest(request);           
                request.Headers.Add("Authorization: Bearer " + credential.Token.AccessToken);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return response.GetResponseStream();
                }
                else
                {
                    Error = response.StatusDescription;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
        else
        {
            Error = "No hay URL de descarga";
            return null;
        }
    }
    #endregion
}