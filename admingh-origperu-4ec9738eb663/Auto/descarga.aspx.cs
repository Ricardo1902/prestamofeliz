﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Security;
using System.Text;
using System.Data.SqlClient;

public partial class Auto_descarga : System.Web.UI.Page
{

  cls_Rep Obj_Rep = new cls_Rep();
  static SqlConnection conBd;

  protected override void OnInit(EventArgs e)
  {
    base.OnInit(e);
    if (Request.UrlReferrer == null)
    {
      if (Session["UsuarioId"] != null)
      {
        cls_Rep Obj_Rep = new cls_Rep();
        System.Data.DataSet DS_ValUser = new System.Data.DataSet();
        DS_ValUser = Obj_Rep.Val_Usuario("", "", Convert.ToInt32(Session["UsuarioId"]), 5, 0);

        FormsAuthentication.SignOut();
        Session.Clear();
        Session["identificado"] = false;
      }
      Response.Redirect("~/frmLogin.aspx");
    }
  }

  protected void Page_Load(object sender, EventArgs e)
  {
    try
    {      
      string s = Session["strSQL"].ToString();
      Session.Remove("strSQL");

      using (SqlDataReader dr = Obj_Rep.Rep_Ejecuta(s))
      {

        StringBuilder sb = new StringBuilder();
        int x = 1;
        int y = 0;


        string arch = Session["arch"].ToString();
        string Pagina = Session["Pagina"].ToString();
        Session.Remove("arch");
        Session.Remove("Pagina");

        do
        {
          if (x == 1)
          {

            if (Pagina.ToString() != "10")
            {
              HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.csv", arch));
            }
            else { HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.txt", arch)); }
            Encoding TipoEnconding = Encoding.GetEncoding(int.Parse(Session["TipoEncoding"].ToString()));

            HttpContext.Current.Response.ContentEncoding = TipoEnconding; // System.Text.Encoding.Default;
            HttpContext.Current.Response.Charset = "";

            HttpContext.Current.Response.ContentType = "application/vnd.xls";
          }
          for (int count = 0; count < dr.FieldCount; count++)
          {
            if (dr.GetName(count) != null)
              sb.Append(dr.GetName(count));
            if (count < dr.FieldCount - 1)
            {
              sb.Append(",");
            }
          }

          HttpContext.Current.Response.Write(sb.ToString() + "\n");
          HttpContext.Current.Response.Flush();

          ////Append Data

          sb = new StringBuilder();
          if (dr.HasRows)
          {

            while (dr.Read())
            {

              for (int col = 0; col < dr.FieldCount - 1; col++)
              {
                if (!dr.IsDBNull(col))
                  sb.Append(dr.GetValue(col).ToString().Replace(",", " "));
                sb.Append(",");
              }

              if (!dr.IsDBNull(dr.FieldCount - 1))
                sb.Append(dr.GetValue(
                dr.FieldCount - 1).ToString().Replace(",", " "));

              HttpContext.Current.Response.Write(sb.ToString() + "\n");
              sb.Clear();
              HttpContext.Current.Response.Flush();

            }
          }
          else
          {
            sb.Append("Sin Informacion");


            HttpContext.Current.Response.Write(sb.ToString() + "\n");
            sb.Clear();
            HttpContext.Current.Response.Flush();
          }


          Response.Write("\n");
          Response.Flush();

          sb.Clear();


          x = x + 1;
        } while (dr.NextResult());
        dr.Dispose();
        HttpContext.Current.Response.Close();

      }      
    }
    catch(Exception ex)
    {
      HttpContext.Current.Response.Write(ex.Message);
    }
    finally
    {
      HttpContext.Current.Response.Close();
      Obj_Rep.Rep_Close();
    }
  }
}