﻿<%@ WebHandler Language="C#" Class="DownloadFile" %>

using System;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using AMPEX;
using wsSOPF;

public class DownloadFile : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;

        // Validar si es Descarga de Drive o Directa de Archivo.
        string fileName = request.QueryString["fileName"];
        string IdGoogleDrive = request.QueryString["idGoogleDrive"];
        string IdPlantillaEnvioCobro = request.QueryString["idPlantillaEnvioCobro"];
        string IdSolicitudAmpliacion = request.QueryString["IdSolicitudAmpliacion"];

        if (!string.IsNullOrEmpty(IdGoogleDrive))
            DescargaGoogleDrive(context, IdGoogleDrive);
        else if (!string.IsNullOrEmpty(IdPlantillaEnvioCobro))
            DescargaLayoutCobro(context, IdPlantillaEnvioCobro);
        else if (!string.IsNullOrEmpty(IdSolicitudAmpliacion))
            DescargaHojaAmpliacion(context, IdSolicitudAmpliacion);
        else
            DescargaArchivo(context);
    }

    private void DescargaArchivo(HttpContext context)
    {
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;

        string fileName = request.QueryString["fileName"];

        response.ClearContent();
        response.Clear();
        response.ContentType = MimeMapping.GetMimeMapping(fileName);
        response.AddHeader("Content-Disposition", "attachment; filename=" + System.IO.Path.GetFileName(fileName) + ";");
        response.TransmitFile(fileName);
        response.Flush();
        response.End();
    }

    private void DescargaGoogleDrive(HttpContext context, string IdGoogleDrive)
    {
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;

        // Descargar Archivo desde Google Drive.
        wsSOPF.ArchivoGoogleDrive ArchivoGoogleDrive = new ArchivoGoogleDrive();
        using (wsSOPF.GestionClient ws = new GestionClient())
        {
            wsSOPF.ResultadoOfArchivoGoogleDrivex0d_SlH4I res = ws.DescargarArchivoGoogleDrive(IdGoogleDrive);

            // Codigo = 1, en esta funcion significa Exito(LOL).
            if (res.Codigo != 1)
                throw new Exception(res.Mensaje);
            else
                ArchivoGoogleDrive = res.ResultObject;
        }

        // Transmitir Archivo
        var tempBase64String = ArchivoGoogleDrive.Contenido != null ? Convert.ToBase64String(ArchivoGoogleDrive.Contenido) : null;

        byte[] arrayByte = Convert.FromBase64String(tempBase64String);
        if (arrayByte != null && arrayByte.Length > 0)
        {
            response.AddHeader("content-disposition", "attachment; filename=" + ArchivoGoogleDrive.NombreArchivo);
            response.ContentType = ArchivoGoogleDrive.Mime;
            response.BinaryWrite(arrayByte);
            response.Flush();
        }
        else
        {
            response.Write("No fue posible encontrar el Archivo.");
        }
    }

    private void DescargaLayoutCobro(HttpContext context, string IdPlantillaEnvioCobro)
    {
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;

        string UsuarioId = request.QueryString["UsuarioId"];
        DescargaArchivoDomiciliacionResponse res;

        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            wsSOPF.GenerarArchivoDomiciliacionRequest peticionDescarga = new GenerarArchivoDomiciliacionRequest();
            peticionDescarga.IdPlantillaEnvioCobro = int.Parse(IdPlantillaEnvioCobro);
            peticionDescarga.IdUsuario = int.Parse(UsuarioId);

            res = ws.Plantilla_DescargaArchivoDomiciliacion(peticionDescarga);

            if (res.Error)
                throw new Exception(res.MensajeOperacion);
        }

        // Transmitir Archivo
        var tempBase64String = res.Resultado.ContenidoArchivo != null ? Convert.ToBase64String(res.Resultado.ContenidoArchivo) : null;

        byte[] arrayByte = Convert.FromBase64String(tempBase64String);
        if (arrayByte != null && arrayByte.Length > 0)
        {
            response.AddHeader("content-disposition", "attachment; filename=" + string.Format("{0}{1}", res.Resultado.NombreArchivo, res.Resultado.Extension));
            response.ContentType = res.Resultado.TipoContenido;
            response.BinaryWrite(arrayByte);
            response.Flush();
        }
        else
        {
            response.Write("No fue posible encontrar el Archivo.");
        }
    }

    private void DescargaHojaAmpliacion(HttpContext context, string IdSolicitudAmpliacion)
    {
        DescargaHojaAmpliacion descargaHojaAmpliacion;

        string URL = "http://promotor.ws.prestamofeliz.com.mx:9052/api/OriginacionPeru/GetFormatoAmpliacion";
        string urlParameters = string.Format("?IDSolicitud={0}", IdSolicitudAmpliacion);

        HttpClient client = new HttpClient();
        client.BaseAddress = new Uri(URL);

        // Add an Accept header for JSON format.
        client.DefaultRequestHeaders.Accept.Add(
        new MediaTypeWithQualityHeaderValue("application/json"));

        // List data response.
        HttpResponseMessage res = client.GetAsync(urlParameters).Result;
        if (res.IsSuccessStatusCode)
        {
            // Parse the response body.
            descargaHojaAmpliacion = AMPEX.DescargaHojaAmpliacion.FromJson(res.Content.ReadAsStringAsync().Result);
        }
        else
            throw new Exception(res.ReasonPhrase);

        // Si regreso un error mandamos el mensaje
        if (descargaHojaAmpliacion.Data.Error)
            throw new Exception(descargaHojaAmpliacion.Data.MensajeOperacion.ToString());

        client.Dispose();

        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        byte[] pdfBytes = Convert.FromBase64String(descargaHojaAmpliacion.Data.Resultado.ContenidoArchivo);

        if (pdfBytes != null && pdfBytes.Length > 0)
        {
            response.AddHeader("content-disposition", "attachment; filename=" + descargaHojaAmpliacion.Data.Resultado.NombreArchivo + descargaHojaAmpliacion.Data.Resultado.Extension);
            response.ContentType = descargaHojaAmpliacion.Data.Resultado.TipoContenido;
            response.BinaryWrite(pdfBytes);
            response.Flush();
        }
        else
            throw new Exception("Archivo encontrado pero con contenido desconocido o vacio");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}