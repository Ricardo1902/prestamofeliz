﻿<%@ WebHandler Language="C#" Class="DownloadFile" %>

using System;
using System.Web;

public class DownloadFile : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;
        string fileName = request.QueryString["fileName"];

        response.ClearContent();
        response.Clear();
        response.ContentType = "text/CS";
        response.AddHeader("Content-Disposition",
                           "attachment; filename=" + System.IO.Path.GetFileName(fileName) + ";");
        response.TransmitFile(fileName);
        response.Flush();
        response.End();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}