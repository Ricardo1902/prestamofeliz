﻿<%@ WebHandler Language="C#" Class="DownloadFileZ" %>

using System;
using System.Web;


public class DownloadFileZ : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;
        string fileName = request.QueryString["fileName"];

        response.ClearContent();
        response.Clear();
        response.ContentType = "application/xls";
        response.AddHeader("Content-Disposition", "attachment; filename=" + System.IO.Path.GetFileName(fileName) + ";");
        //"content-disposition", "attachment; filename=Archivo"+ DateTime.Now.ToString("dd-MM-yyyy") + ".zip"
        response.TransmitFile(fileName);
        response.Flush();
        response.End();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}