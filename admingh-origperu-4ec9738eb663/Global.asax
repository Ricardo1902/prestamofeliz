﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {        
        WebControl.DisabledCssClass = "disabled";
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //if (Session["UsuarioId"] != null)
      {
        cls_Rep Obj_Rep = new cls_Rep();
        System.Data.DataSet DS_ValUser = new System.Data.DataSet();
        DS_ValUser = Obj_Rep.Val_Usuario("", "", Convert.ToInt32(Session["UsuarioId"]), 5, 0);

        FormsAuthentication.SignOut();
        Session.Clear();
        Session["identificado"] = false;
      }
    }
        
    void Application_Error(object sender, EventArgs e) 
    {
      try
      {
        HttpException lastErrorWrapper = Server.GetLastError() as HttpException;
        HttpContext con = HttpContext.Current;

        Exception lastError = lastErrorWrapper;
        if (lastErrorWrapper.InnerException != null)
          lastError = lastErrorWrapper.InnerException;

        string lastErrorTypeName = lastError.GetType().ToString();
        string lastErrorMessage = lastError.Message;
        string lastErrorStackTrace = lastError.StackTrace;

        Session["PaginaError"] = con.Request.Url.ToString();
        Session["lastErrorMessage"] = lastErrorMessage;
        Session["UsuarioError"] = Session["UsuarioId"];        
      }
      catch (Exception ex)
      {
      }      
    }

    void Session_Start(object sender, EventArgs e) 
    {
      
    }

    void Session_End(object sender, EventArgs e) 
    {      
      
    }
    
    void Application_PreRequestHandlerExecute(object sender, EventArgs e)
    {
        HttpApplication application = (HttpApplication)sender;
        HttpContext context = HttpContext.Current;

        if (context.Handler is Page)
        {
            Page page = (Page)context.Handler;
            page.PreInit += new EventHandler(cambiarMaster);
        }
    }

    void cambiarMaster(object sender, EventArgs e)
    {
        Page p = (Page)sender;
        if(p.MasterPageFile != null)
        {
			p.MasterPageFile = ConfigurationManager.AppSettings["Master"];
			string strModal = Request.QueryString["Modal"];
			if (strModal != null || Session["Modal"] != null)
			{
				if (strModal == null) strModal = Session["Modal"].ToString();
				if (strModal == "1")
					p.MasterPageFile = "~/SiteVacio.master";
			}
        }
    }
       
</script>