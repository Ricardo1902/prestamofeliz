﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Xml.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

public partial class SiteMaster : System.Web.UI.MasterPage
{
    cls_Rep Obj_Rep = new cls_Rep();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int Sistema = Convert.ToInt32(Session["SistemaId"].ToString());

        if (!Page.IsPostBack)
        {
            int UserId = 0;

            if (Session["UsuarioId"] != null)
            {
                UserId = Convert.ToInt32(Session["UsuarioId"].ToString());
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                Session["identificado"] = false;
                Response.Redirect("/frmLogin.aspx");
            }

            this.lblNombreUsuarioSession.Text = Session["NombreUsuario"].ToString();

            string sjs = "";
            int i, j;

            DataSet dsMenu_1 = new DataSet();
            DataSet dsMenu_2 = new DataSet();

            try
            {
                dsMenu_1 = Obj_Rep.Con_MenuOpcion(Convert.ToInt32(Session["UsuarioId"]), 0, Sistema);

                if (dsMenu_1.Tables[0].Rows.Count > 0)
                {
                    sjs = @"<div class='menu-list navbar-collapse'>
                            <ul id='menu-content' class=' menu-content collapse out '>";

                    for (i = 0; i < dsMenu_1.Tables[0].Rows.Count; i++)
                    {
                        sjs = sjs + @"<li class ='' data-toggle='collapse' class='collapsed' data-target='#" + dsMenu_1.Tables[0].Rows[i]["Descripcion"].ToString().Trim() + @"'  >
                                        <a href='";

                        if (dsMenu_1.Tables[0].Rows[i]["url"].ToString().Trim() == "#")
                        {
                            sjs = sjs + '#';//"/principal.aspx";
                        }
                        else
                        {
                            sjs = sjs + "/" + dsMenu_1.Tables[0].Rows[i]["url"].ToString().Trim();
                        }

                        string TipoRedirect = dsMenu_1.Tables[0].Rows[i]["TipoRedirect"].ToString().Trim();
                        if (TipoRedirect == "Redirect")
                        {
                            sjs = sjs + "'><i class='fa " + dsMenu_1.Tables[0].Rows[i]["class"].ToString().Trim() + "'></i>" + dsMenu_1.Tables[0].Rows[i]["Descripcion"].ToString().Trim()
                            + @" </a></li>";
                        }
                        else
                        {
                            sjs = sjs + "' target=\"_blank\"><i class='fa " + dsMenu_1.Tables[0].Rows[i]["class"].ToString().Trim() + "'></i>" + dsMenu_1.Tables[0].Rows[i]["Descripcion"].ToString().Trim()
                            + @" </a></li>";
                        }

                        dsMenu_2 = Obj_Rep.Con_MenuOpcion(Convert.ToInt32(Session["UsuarioId"]), Convert.ToInt32(dsMenu_1.Tables[0].Rows[i]["IdMenu"].ToString().Trim()), Sistema);

                        if (dsMenu_2.Tables[0].Rows.Count > 0)
                        {
                            sjs = sjs + "<ul class='sub-menu collapse collapsed' id='" + dsMenu_1.Tables[0].Rows[i]["Descripcion"].ToString().Trim() + "'>";

                            for (j = 0; j < dsMenu_2.Tables[0].Rows.Count; j++)
                            {
                                var Des_Byte = Encoding.UTF8.GetBytes(Convert.ToString(dsMenu_2.Tables[0].Rows[j]["Descripcion"].ToString().Trim()));
                                var Des_Encrip = Convert.ToBase64String(Des_Byte);
                                var val_Byte = Encoding.UTF8.GetBytes(Convert.ToString(dsMenu_2.Tables[0].Rows[j]["valor"].ToString().Trim()));
                                var val_Encrip = Convert.ToBase64String(val_Byte);

                                if (dsMenu_2.Tables[0].Rows[j]["URL"].ToString().Trim().EndsWith("?"))
                                {
                                    sjs = sjs + " <li><a href='/" + dsMenu_2.Tables[0].Rows[j]["URL"].ToString().Trim() + "8UOSDB5CZ2=" + val_Encrip + "&ZQY4UEOQHR=" + Des_Encrip + "' " + Convert.ToChar(34) + ">" + "<i class='fa "
                                        + dsMenu_2.Tables[0].Rows[j]["Class"].ToString().Trim() + "'></i>" + dsMenu_2.Tables[0].Rows[j]["Descripcion"].ToString().Trim() + "</a>" + Convert.ToChar(32);
                                    sjs = sjs + "</li>";
                                }
                                else
                                {
                                    sjs = sjs + " <li><a href='/" + dsMenu_2.Tables[0].Rows[j]["URL"].ToString().Trim() + "' " + Convert.ToChar(34) + ">" + "<i class='fa "
                                        + dsMenu_2.Tables[0].Rows[j]["Class"].ToString().Trim() + "'></i>" + dsMenu_2.Tables[0].Rows[j]["Descripcion"].ToString().Trim() + "</a>" + Convert.ToChar(32);
                                    sjs = sjs + "</li>";
                                }
                            }
                            sjs = sjs + "</ul>";
                        }
                    }

                    sjs = sjs + "</ul></div>";

                }
                lblMenu.Text = sjs;
            }
            catch (Exception exc)
            {
                Response.Redirect("/frmLogout.aspx");
            }
        }
    }

    protected void Menu(MenuItemCollection items, int idpadre, DataTable dt)
    {
        int id;
        string NombreMenu;

        try
        {
            // filtramos por el id que toma este, el primero es "0" que son los padres
            DataRow[] hijos = dt.Select("idPadre=" + idpadre.ToString());

            // validamos qu eencontremos resultados, si no volvemos    
            if (hijos.Length == 0)
                return;
            // recorremos la informacion filtrada
            foreach (DataRow hijo in hijos)
            {
                // asignamos las variables al menu
                id = Convert.ToInt32(hijo[0]);
                NombreMenu = Convert.ToString(hijo[1]);
                // creamos el item
                MenuItem nuevoItem = new MenuItem(NombreMenu, id.ToString());
                items.Add(nuevoItem);
                // llamamos a la funcion nuevamente para que revise si tiene mas informacion asociada
                Menu(nuevoItem.ChildItems, id, dt);
            }
        }
        catch (Exception) { }
    }

    /// <summary>
    /// Resize the image to the specified width and height.
    /// </summary>
    /// <param name="image">The image to resize.</param>
    /// <param name="width">The width to resize to.</param>
    /// <param name="height">The height to resize to.</param>
    /// <returns>The resized image.</returns>
    public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
    {
        var destRect = new Rectangle(0, 0, width, height);
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = CompositingMode.SourceCopy;
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

            using (var wrapMode = new ImageAttributes())
            {
                wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
            }
        }

        return destImage;
    }

    protected void btnImagenPerfil_Click(object sender, EventArgs e)
    {
        if (fuImagenPerfil.HasFile)
        {
            // Solo se procesan imagenes de extensiones validas
            string ext = Path.GetExtension(fuImagenPerfil.PostedFile.FileName);
            string[] extValidas = { ".jpg", ".png", ".bmp" };

            if (extValidas.Contains(ext))
            {
                // Ajustamos el tamaño de la imagen a 128x 128
                Bitmap img = ResizeImage(System.Drawing.Image.FromStream(fuImagenPerfil.PostedFile.InputStream), 128, 128);

                // Guardamos la imagen de perfil
                img.Save(Server.MapPath("~/Imagenes/ImagenPerfil/") + Session["UsuarioId"] + ext.ToLower());

                // Establecemos la nueva imagen de perfil en Session
                Session["ImagenPerfil"] = "/Imagenes/ImagenPerfil/" + Session["UsuarioId"] + ext.ToLower();

                // Si existia una imagen anterior con otra extension para el mismo IDUsuario se elimina
                foreach (string file in Directory.GetFiles(Server.MapPath("~/Imagenes/ImagenPerfil/")))
                {
                    if (Path.GetFileNameWithoutExtension(file) == Session["UsuarioId"].ToString())
                    {
                        if (Path.GetFileName(file).ToLower() != Session["UsuarioId"] + ext.ToLower())
                            File.Delete(file);
                    }
                }
            }
        }
    }
}
