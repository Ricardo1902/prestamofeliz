﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Xml.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows;
using System.IO;


public partial class SiteMaster : System.Web.UI.MasterPage
{
    cls_Rep Obj_Rep = new cls_Rep();

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      if (Request.UrlReferrer == null)
      {
        if (Session["UsuarioId"] != null)
        {
          cls_Rep Obj_Rep = new cls_Rep();
          System.Data.DataSet DS_ValUser = new System.Data.DataSet();
          DS_ValUser = Obj_Rep.Val_Usuario("", "", Convert.ToInt32(Session["UsuarioId"]), 5, 0);

          FormsAuthentication.SignOut();
          Session.Clear();
          Session["identificado"] = false;
        }
        Response.Redirect("~/frmLogin.aspx");      
      }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int Sistema=Convert.ToInt32(Session["SistemaId"].ToString());
        Titulo.InnerText = ConfigurationManager.AppSettings["Titulo"];
        Page.Title = ConfigurationManager.AppSettings["Titulo"];
        //titulo.HRef = Request.FilePath + "?#";
        if (!Page.IsPostBack)
        {

            int UserId = 0;


            if (Session["UsuarioId"] != null)
            {
                UserId = Convert.ToInt32(Session["UsuarioId"].ToString());
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                Session["identificado"] = false;
                Response.Redirect("~/frmLogin.aspx");
            }


            try
            {
                DataTable dt = this.GetData(0, UserId, Sistema);
                PopulateMenu(dt, 0, null,  UserId,  Sistema);
            }
            catch (Exception exc)
            {
                Response.Redirect("~/frmLogout.aspx");
            }

            
        }

    }


    #region Menu Nav
    private DataTable GetData(int parentMenuId, int UserId, int Sistema)
    {

        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        ds = Obj_Rep.Con_MenuOpcion(UserId, parentMenuId, Sistema);
        dt = ds.Tables[0];
        return dt;

    }

    private void PopulateMenu(DataTable dt, int parentMenuId, MenuItem parentMenuItem,  int UserId,  int Sistema)
    {
        string currentPage = Path.GetFileName(Request.Url.AbsolutePath);
        foreach (DataRow row in dt.Rows)
        {

            var url = "";
            if (parentMenuId == 0 || row["URL"].ToString().Trim() == "#")
            {
                if (row["URL"].ToString().Trim() == "#")
                {
                    //url =Request.FilePath+'?'+ row["URL"].ToString().Trim();
                    url = "/Site/Principal.aspx?#";
                }

                else
                {
                    url = row["URL"].ToString().Trim();
                }
            }
            else
            {
                var Des_Byte = Encoding.GetEncoding("iso-8859-1").GetBytes(Convert.ToString(row["Descripcion"].ToString().Trim()));
                var Des_Encrip = Convert.ToBase64String(Des_Byte);
                var val_Byte = Encoding.UTF8.GetBytes(Convert.ToString(row["valor"].ToString().Trim()));
                var val_Encrip = Convert.ToBase64String(val_Byte);
                if (row["IdMenu"].ToString().Trim() == "0")
                {
                    url = row["URL"].ToString().Trim() + "8UOSDB5CZ2=" + val_Encrip + "&ZQY4UEOQHR=" + Des_Encrip;
                }
                else { url = row["URL"].ToString().Trim(); }
            }

            MenuItem menuItem = new MenuItem
            {

                Value = row["IdMenu"].ToString(),
                Text = row["Descripcion"].ToString(),
                NavigateUrl = url,
                Selected = row["url"].ToString().EndsWith(currentPage, StringComparison.CurrentCultureIgnoreCase)
            };

            this.Page.ClientScript.RegisterStartupScript(GetType(), "menuIMG", "$(function(){ $('nav a[href^=\"" + url + "\"]').addClass('glyphicon glyphicon-tower'); });", true);

            if (parentMenuId == 0)
            {
                Menu1.Items.Add(menuItem);
                DataTable dtChild = this.GetData(int.Parse(menuItem.Value), UserId, Sistema);
                PopulateMenu(dtChild, int.Parse(menuItem.Value), menuItem, UserId, Sistema);
            }
            else
            {
                parentMenuItem.ChildItems.Add(menuItem);
            }
        }

        

       // MenuItemStyle mis = new MenuItemStyle();
        //mis.CssClass = "glyphicon glyphicon-tower";       
        //Menu1.LevelMenuItemStyles.Add(mis);
        //mis = new MenuItemStyle();
        //mis.CssClass = "glyphicon glyphicon-queen";
        //Menu1.LevelMenuItemStyles.Add(mis);
    }
    #endregion

   

}
