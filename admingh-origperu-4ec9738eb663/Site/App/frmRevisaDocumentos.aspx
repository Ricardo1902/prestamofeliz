﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmRevisaDocumentos.aspx.cs" Inherits="Site_App_frmRevisaDocumentos" MasterPageFile="~/Site.master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolKit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">     
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Ampliaciones Express APP - Revision Documentos</h3>
        </div>
        <div class="panel-body">           
            <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>                                               
                <asp:Panel ID="pnlBuscar" runat="server">
                    <div class="col-sm-12">
                        <h4>Buscar Solicitudes</h4>
                        <hr />
                    </div>
                    <div class="form-group col-sm-6 col-md-3 col-lg-3">
                        <label class="form-control-label small">Solicitud Ampliación</label>
                        <asp:textbox id="txtSolicitud_Buscar" runat="server" CssClass="form-control"></asp:textbox>
                    </div>         
                    
                    <div class="form-group col-md-6 col-lg-6">
                            <label class="small">Vendedor</label>
                            <asp:DropDownList ID="ddlPromotor" AppendDataBoundItems="true" runat="server" CssClass="form-control" AutoPostBack="false">
                                <asp:ListItem Text="Todos" Value="-1"></asp:ListItem>   
                            </asp:DropDownList>
                        </div>
                            
                    <div class="form-group col-sm-6 col-md-3 col-lg-3">           
                        <label class="form-control-label small">Estatus</label>
                        <asp:dropdownlist id="ddlEstatus_Buscar" AppendDataBoundItems="true" DataValueField="IdEstatus" DataTextField="EstatusDesc" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Todos" Value="-1"></asp:ListItem>                                        
                        </asp:dropdownlist>
                    </div>

                    <div class="form-group col-sm-6 col-md-4 col-lg-3">
                        <label class="form-control-label small">Nombre Cliente</label>
                        <asp:textbox id="txtNombreCliente" runat="server" CssClass="form-control"></asp:textbox>
                    </div> 

                    <div class="form-group col-md-4 col-lg-3">
                                <label class="form-control-label small">Fecha Solicitud (Desde)</label>                                
                                <div>     
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaSolicitudDesde" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                        <span class="input-group-addon">
                                            <asp:LinkButton ID="btnSelectFechaSolicitudDesde" runat="server">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </asp:LinkButton>
                                        </span>
                                    </div>                                                         
                                    <asp:RegularExpressionValidator runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtFechaSolicitudDesde"
                                            ForeColor="Red"
                                            ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />                                    
                                    <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaSolicitudDesde" PopupButtonID="btnSelectFechaSolicitudDesde" runat="server"></ajaxtoolKit:CalendarExtender>
                                </div>         
                            </div>            

                            <div class="form-group col-md-4 col-lg-3">
                                <label class="form-control-label small">Fecha Solicitud (Hasta)</label>                                
                                <div>                    
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaSolicitudHasta" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                        <span class="input-group-addon">
                                            <asp:LinkButton ID="btnSelectFechaSolicitudHasta" runat="server">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </asp:LinkButton>
                                        </span>
                                    </div>                    
                                    <asp:RegularExpressionValidator runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtFechaSolicitudHasta"
                                            ForeColor="Red"
                                            ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                    <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaSolicitudHasta" PopupButtonID="btnSelectFechaSolicitudHasta" runat="server"></ajaxtoolKit:CalendarExtender>
                                </div>        
                            </div>  

                    <div class="col-sm-12">            
                        <asp:LinkButton ID="btnBuscar_Buscar" runat="server" OnClick="btnBuscar_Click" CssClass="btn btn-primary btn-sm">
                            <i class="fas fa-search"></i> Buscar
                        </asp:LinkButton>                             
                    </div>    
                            
                    <asp:Panel ID="pnlError_Buscar" class="form-group col-sm-12" Visible="false" runat="server">                                
                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                            <asp:Label ID="lblError_Buscar" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>                                                                                                         

                    <div class="form-group col-sm-12">
                        <div style="height:auto; width: auto; overflow-x: auto;">
                            <hr />
                            <asp:GridView ID="gvBuscar" runat="server" Width="100%" AutoGenerateColumns="False"
                                HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                OnPageIndexChanging="gvBuscar_PageIndexChanging"
                                OnRowCommand="gvBuscar_RowCommand"                                         
                                PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                Style="overflow-x:auto;">                                        
                                <Columns>                                   
                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="Solicitud Ampliación" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnDetalle" CommandName="Detalle" CommandArgument='<%# Eval("IdSolicitudAmpliacionExpress") %>' data-toggle="tooltip" title="Detalle" runat="server">
                                                <i class="far fa-edit"></i>
                                                <%# Eval("IdSolicitudAmpliacion") %>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="Fecha" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# Eval("FechaAmpliacion.Value", "{0:d}") %>
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    <asp:TemplateField HeaderText="Cliente" ItemStyle-CssClass="small">
                                        <ItemTemplate>
                                            <%# Eval("Nombre") + " " + Eval("ApellidoPaterno") + " " + Eval("ApellidoMaterno")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Vendedor" DataField="NombrePromotor" ItemStyle-CssClass="small" /> 
                                    <asp:BoundField HeaderText="Monto otorgado" DataField="MontoOtorgado" DataFormatString="{0:c2}" />
                                    <asp:BoundField HeaderText="Estatus" DataField="EstatusDesc" ItemStyle-CssClass="small" />                                    
                                </Columns>                                        
                                <PagerStyle CssClass="pagination-ty warning" />                                                                                
                                <EmptyDataTemplate>
                                    No se encontraron solicitudes con los filtros seleccionados
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>  
                    </div>
                </asp:Panel>

                <asp:Panel ID="pnlDetalle" Visible="false" runat="server">
                    <div class="form-group col-xs-12"> 
                        <asp:Panel ID="pnlError_Detalle" class="form-group col-sm-12" Visible="false" runat="server">                                
                            <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                <asp:Label ID="lblError_Detalle" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                                    
                        <asp:Panel ID="pnlExito_Detalle" class="form-group col-sm-12 fadeSlideUp" Visible="false" runat="server">                                
                            <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                <asp:Label ID="lblExito_Detalle" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                    </div>

                    <div class="form-group col-xs-12">
                        <asp:Panel ID="pnlBtnsAccion" runat="server">
                            <asp:LinkButton ID="lbtnRegresar_Detalle" CssClass="btn btn-sm btn-default hidden-print" OnClick="lbtnRegresar_Detalle_Click" runat="server">
                                <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                            </asp:LinkButton>                                                              
                                    
                            <asp:LinkButton ID="lbtnDenegar_Detalle" CssClass="btn btn-sm btn-danger hidden-print" OnClick="lbtnDenegar_Detalle_Click" runat="server">
                                <i class="fas fa-times-circle"></i> Denegar Credito
                            </asp:LinkButton>
                        </asp:Panel>                     

                        <asp:Panel ID="pnlAdvertencia_DenegarCredito" CssClass="alert alert-warning col-md-12 col-sm-12" style="padding:4px 20px;" role="alert" Visible="false" runat="server">                            
                            <div class="form-group col-md-12">
                                <asp:Label ID="lblAdvertencia_DenegarCredito" runat="server" Text="¿Confirma que desea <b>Denegar el Credito</b>?"></asp:Label>
                                <asp:Button ID="btnConfirmar_DenegarCredito" Text="Si" CssClass="btn btn-sm btn-success" runat="server" OnClick="btnConfirmar_DenegarCredito_Click" />
                                <asp:Button ID="btnCancelar_DenegarCredito" Text="No" CssClass="btn btn-sm btn-danger" runat="server" OnClick="btnCancelar_DenegarCredito_Click" />
                            </div>
                            <div class="form-group col-md-10">                                
                                <asp:TextBox ID="txtComentario_pnlAdvertencia_DenegarCredito" placeholder="Comentario opcional" CssClass="form-control" MaxLength="500" runat="server"></asp:TextBox>
                            </div>
                        </asp:Panel>
                                                                        
                        <hr class="hidden-print" />

                        <div class="col-sm-12">
                            <h4 id="lblTituloDetalle" runat="server">Detalle Solicitud Ampliacion</h4>
                            <hr />
                        </div>                                                                      
                                                                                                            
                        <div class="form-group col-md-3">   
                            <label class="small">Solicitud Ampliación</label>
                            <asp:Label ID="lblIdSolicitudAmpliacion_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>

                        <div class="form-group col-md-3">   
                            <label class="small">Estatus</label>
                            <asp:Label ID="lblEstatus_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>

                        <div class="form-group col-md-6">   
                            <label class="small">Vendedor</label>
                            <asp:Label ID="lblVendedor_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>

                        <div class="clearfix"></div>

                         <div class="form-group col-md-3">   
                            <label class="small">DNI</label>
                            <asp:Label ID="lblDNI_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>
                                    
                        <div class="form-group col-md-6">                                                  
                            <label class="small">Nombre Cliente</label>
                            <asp:Label ID="lblNombreCompleto_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>

                        <div class="form-group col-md-3">                                                  
                            <label class="small">Plazo Otorgado</label>
                            <asp:Label ID="lblPlazoOtorgado_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>

                        <div class="form-group col-md-3">   
                            <label class="small">Fecha Ampliación</label>
                            <asp:Label ID="lblFechaLiquidacion_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>

                        <div class="form-group col-md-3">   
                            <label class="small">Monto Otorgado</label>
                            <asp:Label ID="lblMontoCredito_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>

                        <div class="form-group col-md-3">   
                            <label class="small">Monto Liquidacion</label>
                            <asp:Label ID="lblMontoLiquidacion_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>

                        <div class="form-group col-md-3">   
                            <label class="small">Monto Desembolsar</label>
                            <asp:Label ID="lblMontoDesembolsar_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>                                                                                       

                        <div class="form-group col-md-2">   
                            <label class="small">Telefono</label>
                            <asp:Label ID="lblTelefono_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>

                        <div class="form-group col-md-3">   
                            <label class="small">Celular</label>
                            <asp:Label ID="lblCelular_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>

                        <div class="form-group col-md-4">   
                            <label class="small">Correo</label>
                            <asp:Label ID="lblCorreo_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>   
                        
                        <div class="form-group col-md-3">   
                            <label class="small">Fecha Generado</label>
                            <asp:Label ID="lblFechaRegistro_Detalle" CssClass="form-control" runat="server"></asp:Label>
                        </div>   

                        <div class="form-group clearfix"></div>

                        <div class="col-xs-12">
                            <hr />
                        </div>

                        <ajaxtoolKit:TabContainer ID="tcDocumentacion" CssClass="actTab" runat="server">
                            <ajaxtoolKit:TabPanel HeaderText="<i class='fas fa-th-list'></i> Documentacion" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlDocumentos_Detalle" CssClass="col-sm-12 col-md-12" runat="server">                                       
                                        <asp:Panel ID="pnlAdvertencia_RechazarDocumento" CssClass="alert alert-warning col-md-12 col-sm-12" Style="padding: 4px 20px;" role="alert" Visible="false" runat="server">
                                            <asp:HiddenField ID="hdnIdDocumentoRechazar" runat="server"></asp:HiddenField>
                                            <div class="form-group col-md-12">
                                                <asp:Label ID="lblAdvertencia_RechazarDocumento" runat="server" Text=""></asp:Label>
                                            </div>
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txtComentario_pnlAdvertencia_RechazarDocumento" placeholder="Comentario opcional" CssClass="form-control" MaxLength="500" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <asp:Button ID="btnConfirmar_RechazarDocumento" Text="Si" CssClass="btn btn-sm btn-success" runat="server" OnClick="btnConfirmar_RechazarDocumento_Click" />
                                                <asp:Button ID="btnCancelar_RechazarDocumento" Text="No" CssClass="btn btn-sm btn-danger" runat="server" OnClick="btnCancelar_RechazarDocumento_Click" />
                                            </div>
                                        </asp:Panel>

                                        <asp:GridView ID="gvDocumentacion_Detalle" runat="server" Width="100%" AutoGenerateColumns="False"
                                            HeaderStyle-CssClass="info" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-hover-warning table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None"
                                            RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            OnRowCommand="gvDocumentacion_Detalle_RowCommand">
                                            <Columns>                                                
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderText="Estatus">
                                                    <ItemTemplate>
                                                        <%# Eval("Actual").ToString() == "1" ? "RECIBIDO" : "PENDIENTE" %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Documento" DataField="Documento" ItemStyle-CssClass="small" />
                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="Fecha Recibido" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <%# Eval("FechaRegistro.Value", "{0:dd/MM/yyyy HH:mm}") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="info small" />
                                            <EmptyDataTemplate>
                                                <i class="fas fa-info-circle"></i>ERROR: Revise la Configuracion de [Expediente Ampliacion]
                                       
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </asp:Panel>
                                </ContentTemplate>
                            </ajaxtoolKit:TabPanel>
                            <ajaxtoolKit:TabPanel HeaderText="<i class='fas fa-th-list'></i> Estatus Historico" runat="server">
                                <ContentTemplate>
                                    <div class="col-xs-12" style="overflow:auto">
                                        <asp:GridView ID="gvEstatusHistorico_Detalle" runat="server" Width="100%" AutoGenerateColumns="False"
                                            HeaderStyle-CssClass="info" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-hover-warning table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None"
                                            RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            OnRowCommand="gvDocumentacion_Detalle_RowCommand">
                                            <Columns>                                                                                               
                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="Fecha y Hora" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <%# Eval("FechaRegistro.Value", "{0:dd/MM/yyyy HH:mm}") %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Estatus" DataField="EstatusDesc" ItemStyle-CssClass="small" />
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderText="Usuario">
                                                    <ItemTemplate>
                                                        <%# Eval("NombreUsuario").ToString() %> <%# Eval("ApellidoPaternoUsuario").ToString() %> <%# Eval("ApellidoMaternoUsuario").ToString() %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Comentario" DataField="Comentario" ItemStyle-CssClass="small" />
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="info small" />
                                            <EmptyDataTemplate>
                                                <i class="fas fa-info-circle"></i>Sin estatus registrados.                                       
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </ajaxtoolKit:TabPanel>
                        </ajaxtoolKit:TabContainer>                                                                                                                                                                
                    </div>                                  
                </asp:Panel>                       
            </ContentTemplate>
                <Triggers>                    
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>       
    <asp:HiddenField ID="filtrarSucursal" runat="server" />
    <script type="text/javascript">     
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            // Activar Tooltips
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            $(".fadeSlideUp").fadeTo(2000, 500).slideUp(500, function () {
                $(".fadeSlideUp").slideUp(500);
            });
        });        
</script>
</asp:Content>
