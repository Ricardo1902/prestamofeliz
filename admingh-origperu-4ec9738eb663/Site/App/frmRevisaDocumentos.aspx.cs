﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Site_App_frmRevisaDocumentos : System.Web.UI.Page
{
    #region "Propiedades"

    private int IdUsuario
    {
        get
        {
            return Convert.ToInt32(Session["UsuarioId"].ToString());
        }
    }

    private int IdSolicitudAmpliacionExpressDetalle
    {
        set
        {
            ViewState[this.UniqueID + "_IdSolicitudAmpliacionExpressDetalle"] = value;
        }
        get
        {
            return (int)ViewState[this.UniqueID + "_IdSolicitudAmpliacionExpressDetalle"];
        }
    }

    private TB_SolicitudAmpliacionExpress SolicitudAmpliacionExpressDetalle
    {
        set
        {
            ViewState[this.UniqueID + "_SolicitudAmpliacionExpressDetalle"] = value;
        }
        get
        {
            return (TB_SolicitudAmpliacionExpress)ViewState[this.UniqueID + "_SolicitudAmpliacionExpressDetalle"];
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                CargarComboEstatus();
                CargarComboPromotor();                
            }
        }
        catch (Exception ex)
        {
            pnlError_Buscar.Visible = true;
            lblError_Buscar.Text = ex.Message;
        }
    }    

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        pnlError_Buscar.Visible = false;

        try
        {
            Solicitudes_Filtrar();
        }
        catch (Exception ex)
        {
            lblError_Buscar.Text = ex.Message;
            pnlError_Buscar.Visible = true;
        }
    }

    protected void gvBuscar_RowCommand(object sender, GridViewCommandEventArgs e)
    {        
        try
        {
            switch (e.CommandName)
            {
                case "Detalle":
                    using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                    {                        
                        Detalle_Cargar(int.Parse(e.CommandArgument.ToString()));
                    }
                    break;

                case "DescargarHojaAmpliacion":                    
                    Response.Redirect(string.Format("~/DownloadFile.ashx?IdSolicitudAmpliacion={0}", e.CommandArgument.ToString()));
                    break;
            }
        }
        catch (Exception ex)
        {
            lblError_Buscar.Text = ex.Message;
            pnlError_Buscar.Visible = true;
        }
    }

    protected void gvBuscar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBuscar.PageIndex = e.NewPageIndex;
        Solicitudes_Filtrar();
    }

    #region "Revision Documentos"       

    protected void lbtnRegresar_Detalle_Click(object sender, EventArgs e)
    {
        pnlBuscar.Visible = true;
        pnlDetalle.Visible = false;

        this.SolicitudAmpliacionExpressDetalle = null;
    }
    
    protected void lbtnDenegar_Detalle_Click(object sender, EventArgs e)
    {
        pnlBtnsAccion.Visible = false;
        pnlAdvertencia_DenegarCredito.Visible = true;
        txtComentario_pnlAdvertencia_DenegarCredito.Text = string.Empty;
    }    
    
    protected void btnConfirmar_DenegarCredito_Click(object sender, EventArgs e)
    {
        using (wsSOPF.SolicitudClient ws = new SolicitudClient())
        {
            ws.AmpliacionesApp_DenegarCredito(this.SolicitudAmpliacionExpressDetalle.IdSolicitudAmpliacion, this.IdUsuario, txtComentario_pnlAdvertencia_DenegarCredito.Text.Trim());
            Detalle_Cargar(this.IdSolicitudAmpliacionExpressDetalle);
        }

        pnlBtnsAccion.Visible = true;
        pnlAdvertencia_DenegarCredito.Visible = false;
    }

    protected void btnCancelar_DenegarCredito_Click(object sender, EventArgs e)
    {
        pnlBtnsAccion.Visible = true;
        pnlAdvertencia_DenegarCredito.Visible = false;
    }

    protected void gvDocumentacion_Detalle_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "Descargar":
                    TB_SolicitudAmpliacionExpress.ExpedienteDocumento Documento = new TB_SolicitudAmpliacionExpress.ExpedienteDocumento();

                    Documento = this.SolicitudAmpliacionExpressDetalle.ExpedienteDocumentos.Where(x => x.IdExpedienteAmpliacion == int.Parse(e.CommandArgument.ToString())).FirstOrDefault();

                    Response.Redirect(string.Format("~/DownloadFile.ashx?idGoogleDrive={0}", Documento.GDIdArchivo));                  
                    break;
                case "RechazarDocumento":
                    pnlAdvertencia_RechazarDocumento.Visible = true;
                    hdnIdDocumentoRechazar.Value = e.CommandArgument.ToString();

                    string DocumentoRechazar = this.SolicitudAmpliacionExpressDetalle.ExpedienteDocumentos.ToList().Where(x => x.IdExpedienteAmpliacion == int.Parse(e.CommandArgument.ToString())).FirstOrDefault().Documento;
                    lblAdvertencia_RechazarDocumento.Text = string.Format("¿Está seguro(a) de rechazar el documento <strong>{0}</strong>?", DocumentoRechazar);
                    txtComentario_pnlAdvertencia_RechazarDocumento.Text = string.Empty;
                    break;
            }
        }
        catch (Exception ex)
        {
            lblError_Detalle.Text = ex.Message;
            pnlError_Detalle.Visible = true;
        }
    }

    protected void btnConfirmar_RechazarDocumento_Click(object sender, EventArgs e)
    {
        using (wsSOPF.SolicitudClient ws = new SolicitudClient())
        {
            ws.AmpliacionesApp_EliminarDocumento(int.Parse(hdnIdDocumentoRechazar.Value), this.IdUsuario, txtComentario_pnlAdvertencia_RechazarDocumento.Text.Trim());
            Detalle_Cargar(this.IdSolicitudAmpliacionExpressDetalle);
        }

        pnlAdvertencia_RechazarDocumento.Visible = false;
        hdnIdDocumentoRechazar.Value = string.Empty;
    }

    protected void btnCancelar_RechazarDocumento_Click(object sender, EventArgs e)
    {
        pnlAdvertencia_RechazarDocumento.Visible = false;
        hdnIdDocumentoRechazar.Value = string.Empty;
    }    

    #endregion

    #region "Metodos Privados" 

    private void Solicitudes_Filtrar()
    {
        wsSOPF.TB_SolicitudAmpliacionExpress entity = new TB_SolicitudAmpliacionExpress();

        DateTime? fchSolicitudDesde = null;
        DateTime? fchSolicitudHasta = null;

        int IdSolicitudAmpliacion = 0;
        int.TryParse(txtSolicitud_Buscar.Text.Trim(), out IdSolicitudAmpliacion);

        if (!string.IsNullOrEmpty(txtFechaSolicitudDesde.Text.Trim()))
            fchSolicitudDesde = DateTime.ParseExact(txtFechaSolicitudDesde.Text.Trim(), "dd/MM/yyyy", null);

        if (!string.IsNullOrEmpty(txtFechaSolicitudHasta.Text.Trim()))
            fchSolicitudHasta = DateTime.ParseExact(txtFechaSolicitudHasta.Text.Trim(), "dd/MM/yyyy", null);

        entity.IdSolicitudAmpliacion = IdSolicitudAmpliacion;
        entity.IdEstatus = int.Parse(ddlEstatus_Buscar.SelectedValue);
        entity.Nombre = txtNombreCliente.Text.Trim();
        entity.IdPromotor = int.Parse(ddlPromotor.SelectedValue);

        entity.FechaAmpliacion = new UtilsDateTimeR();        
        entity.FechaAmpliacion.ValueIni = fchSolicitudDesde;
        entity.FechaAmpliacion.ValueEnd = fchSolicitudHasta;

        using (wsSOPF.SolicitudClient ws = new SolicitudClient())
        {
            wsSOPF.ResultadoOfArrayOfTB_SolicitudAmpliacionExpressXs2b9qnq res = ws.AmpliacionesApp_FiltrarSolicitudes(entity);

            if (res.Codigo > 0)
            {
                throw new Exception(res.Mensaje);
            }

            gvBuscar.DataSource = res.ResultObject.ToList();
            gvBuscar.DataBind();
        }
    }

    private void Detalle_Cargar(int Id)
    {
        pnlExito_Detalle.Visible = false;
        pnlError_Detalle.Visible = false;

        pnlBtnsAccion.Visible = true;
        pnlAdvertencia_RechazarDocumento.Visible = false;        
        pnlAdvertencia_DenegarCredito.Visible = false;

        using (wsSOPF.SolicitudClient ws = new SolicitudClient())
        {
            this.IdSolicitudAmpliacionExpressDetalle = Id;         

            wsSOPF.ResultadoOfTB_SolicitudAmpliacionExpressXs2b9qnq res = ws.AmpliacionesApp_ObtenerSolicitud(this.IdSolicitudAmpliacionExpressDetalle);

            if (res.Codigo > 0)
                throw new Exception(res.Mensaje);

            TB_SolicitudAmpliacionExpress ampliacionEx = res.ResultObject;
            this.SolicitudAmpliacionExpressDetalle = ampliacionEx;

            lblIdSolicitudAmpliacion_Detalle.Text = ampliacionEx.IdSolicitudAmpliacion.ToString();
            lblEstatus_Detalle.Text = ampliacionEx.EstatusDesc;
            lblMontoCredito_Detalle.Text = ampliacionEx.MontoOtorgado.ToString();
            lblMontoLiquidacion_Detalle.Text = ampliacionEx.MontoLiquidacion.ToString();
            lblMontoDesembolsar_Detalle.Text = ampliacionEx.MontoDesembolsar.ToString();
            lblDNI_Detalle.Text = ampliacionEx.DNI.Trim();
            lblNombreCompleto_Detalle.Text = string.Format("{0} {1} {2}", ampliacionEx.Nombre, ampliacionEx.ApellidoPaterno, ampliacionEx.ApellidoMaterno);
            lblTelefono_Detalle.Text = ampliacionEx.Telefono;
            lblCelular_Detalle.Text = ampliacionEx.Celular;
            lblCorreo_Detalle.Text = ampliacionEx.Correo;
            lblVendedor_Detalle.Text = ampliacionEx.NombrePromotor;
            lblPlazoOtorgado_Detalle.Text = ampliacionEx.PlazoOtorgado.ToString();
            lblFechaLiquidacion_Detalle.Text = ampliacionEx.FechaAmpliacion.Value.GetValueOrDefault().ToString("dd/MM/yyyy");
            lblFechaRegistro_Detalle.Text = ampliacionEx.FechaRegistro.Value.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm");

            // Habilitar Autorizar Credito, si tiene toda la documentacion.                        
            lbtnDenegar_Detalle.Visible = (ampliacionEx.Estatus == "P" || ampliacionEx.Estatus == "PD" || ampliacionEx.Estatus == "RD");                        

            // Ocultar Controles si el usuario es Promotor
            if (int.Parse(Session["Rol"].ToString()) == (int)TipoUsuario.Promotor)
            {                
                lbtnDenegar_Detalle.Visible = false;

                ((DataControlField)gvDocumentacion_Detalle.Columns
               .Cast<DataControlField>()
               .Where(fld => (fld.AccessibleHeaderText == "Eliminar"))
               .SingleOrDefault()).Visible = false;
            }

            gvDocumentacion_Detalle.DataSource = ampliacionEx.ExpedienteDocumentos;
            gvDocumentacion_Detalle.DataBind();

            gvEstatusHistorico_Detalle.DataSource = ampliacionEx.EstatusHistoricos;
            gvEstatusHistorico_Detalle.DataBind();

            pnlBuscar.Visible = false;
            pnlDetalle.Visible = true;
        }
    }

    private void CargarComboEstatus()
    {
        using (wsSOPF.SolicitudClient ws = new SolicitudClient())
        {
            ddlEstatus_Buscar.DataSource = ws.AmpliacionesApp_RevisionDocs_CboEstatus();
            ddlEstatus_Buscar.DataBind();
        }
    }

    private void CargarComboPromotor()
    {
        // Obtener datos de session
        int IdRol, IdSucursal, IdUsuario, Accion, ParamX;

        IdRol = Convert.ToInt32(Session["Rol"].ToString());
        IdUsuario = Convert.ToInt32(Session["UsuarioId"].ToString());
        IdSucursal = Convert.ToInt32(Session["compania"].ToString());

        // Si es vendedor (Promotor)
        if (IdRol == 2)
        {            
            Accion = 2;
            ParamX = IdUsuario;

            // Bloquear el ddlPromotor a los vendedores
            ddlPromotor.Enabled = false;
        }
        else
        {
            Accion = 1;
            ParamX = IdSucursal;
        }

        TBCATPromotor[] promotor = null;
        using (wsSOPF.CatalogoClient wsPromotor = new CatalogoClient())
        {
            promotor = wsPromotor.ObtenerPromotores(Accion, ParamX);
        }
        
        if (promotor.Count() > 0)
        {            
            ddlPromotor.DataSource = null;//Vaciar comboBox                                          
            ddlPromotor.DataTextField = "Promotor";//Indicar qué propiedad se verá en la lista
            ddlPromotor.DataValueField = "IdPromotor";//Indicar qué valor tendrá cada ítem
            ddlPromotor.DataSource = promotor;//Asignar la propiedad DataSource
            ddlPromotor.DataBind();

            // Si es vendedor, seleccionamos el promotor en ddlPromotor
            if (IdRol == 2)
                ddlPromotor.SelectedValue = promotor[0].IdPromotor.ToString();
        }
        else
        {
            ddlPromotor.Items.Clear();
            ddlPromotor.Items.Add(new ListItem("Sin Vendedores", "-1"));            
            ddlPromotor.SelectedIndex = -1;            
        }        
    }
  
    #endregion               
}