﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ExcluirCuentasDomiciliacion.aspx.cs" Inherits="Site_Cobranza_ExcluirCuentasDomiciliacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <link href="css/excluircuentasdomiciliacion.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />
    <script src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script src="../../js/datatables/1.10.19/jquery.dataTables.min.js"></script>
    <script src="../../js/datatables/1.10.19/dataTables.bootstrap.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script src="../../js/bootstrap/js/tooltip.js"></script>
    <script src="../../js/bootstrap/js/transition.js"></script>
    <script src="../../js/parsley.min.js"></script>
    <script src="js/excluircuentasdomiciliacion.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="page-header page-header-corp">
        <div class="row inline ">
            <h3 style="display: inline;">Excluir Cuentas en Domiciliación</h3>
        </div>
    </div>
    <div class="row row-clear row-filtro">
        <div class="col-md-2">
            <div class="form-group">
                <label for="txIdSolicitud">No. Cuenta:</label>
                <input type="text" id="txIdSolicitud" class="form-control" />
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group-command">
                <button type="button" id="btnBuscar" class="btn btn-primary" onclick="buscarCuentasExcluidas();">
                    <span class="glyphicon glyphicon-search"></span>&nbsp;Buscar
                </button>
                <button type="button" id="btnAgregar" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Agregar"
                    onclick="onAgregarCuentas();">
                    <span class="glyphicon glyphicon-plus-sign"></span>
                </button>
                <%--<button type="button" id="btnMasivo" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Agregar desde arcvhio"
                    onclick="">
                    <span class="glyphicon glyphicon-file"></span>
                </button>--%>
            </div>
        </div>
    </div>
    <div id="dAgregarCuentas" class="row row-clear collapse">
        <div class="col-md-12 acciones">
            <button type="button" id="btnExcluir" class="btn btn-danger" onclick="onExcluirCuentas();">
                <span class="glyphicon glyphicon-floppy-save"></span>&nbsp;Excluir
            </button>
            <button type="button" id="btnCancelar" class="btn btn-success" onclick="onCancelarExcluir();">
                <span class="glyphicon glyphicon-remove"></span>&nbsp;Cancelar
            </button>
        </div>
        <div class="dRegistros">
            <table id="tRegistros" class="display" style="width: 100%"></table>
        </div>
    </div>
    <div class="row row-clear">
        <table id="tCuentasExlcuidas" class="display" style="width: 100%"></table>
    </div>
    <div class="modal-corperp"></div>
</asp:Content>
