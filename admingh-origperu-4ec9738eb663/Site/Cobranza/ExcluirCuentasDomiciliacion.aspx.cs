﻿using DataTables;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Site_Cobranza_ExcluirCuentasDomiciliacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder strScript = new StringBuilder();
        if (!Page.IsPostBack)
        {
            strScript.AppendFormat("motivos={0};", JsonConvert.SerializeObject(ObtenerMotivos()));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
        }
    }

    #region WebMethods
    #region Get
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerMotivos()
    {
        int idUsuario = 0;
        string url = string.Empty;
        object motivos = null;
        List<wsSOPF.MotivoExclusionDomiciliacionVM> motivosExclusion = new List<wsSOPF.MotivoExclusionDomiciliacionVM>();

        try
        {
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogout.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            using (wsSOPF.CobranzaAdministrativaClient wsC = new wsSOPF.CobranzaAdministrativaClient())
            {
                var respMotivos = wsC.Domiciliacion_MotivosExclusion(new wsSOPF.MotivosExclusionRequest { IdUsuario = idUsuario, Activo = true });
                if (respMotivos != null && respMotivos.Resultado != null && respMotivos.Resultado.MotivosExclusion != null)
                {
                    motivosExclusion = respMotivos.Resultado.MotivosExclusion.ToList();
                }
            }
            motivosExclusion.Insert(0, new wsSOPF.MotivoExclusionDomiciliacionVM { IdMotivoExclusionDomiciliacion = 0, MotivoExclusion = "Seleccione" });
            motivos = motivosExclusion.Select(m => new
            {
                Id = m.IdMotivoExclusionDomiciliacion,
                m.MotivoExclusion
            });
        }
        catch
        {
        }
        return new { motivos, url };
    }

    #endregion
    #region Post
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerCuentasExcluidas(DataTableAjaxPostModel model, CuentasRequest peticion)
    {
        int idUsuario = 0;
        int totalRegistros = 0;
        int registros = 0;
        string url = string.Empty;
        object cuentasExcluidas = null;
        try
        {
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }

            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogout.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            using (wsSOPF.CobranzaAdministrativaClient wsC = new wsSOPF.CobranzaAdministrativaClient())
            {
                wsSOPF.CuentasExcluidasResponse respCuentas = wsC.Domiciliacion_CuentasExcluidas(new wsSOPF.CuentasExcluidasRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitud = peticion.IdSolicitud,
                    Pagina = (model.start / model.length) + 1,
                    RegistrosPagina = model.length
                });
                if (!respCuentas.Error && respCuentas.Resultado.CuentasExcluidas != null && respCuentas.Resultado.CuentasExcluidas.Length > 0)
                {
                    cuentasExcluidas = respCuentas.Resultado.CuentasExcluidas.Select(m => new
                    {
                        Id = m.IdCuentaExcluyeDomiciliacion,
                        m.IdSolicitud,
                        Motivo = m.MotivoExclusionDomiciliacion,
                        FechaRegistro = m.FechaRegistro == null ? "" : m.FechaRegistro.Value.ToString("yyyy-MM-dd hh:mm tt"),
                        m.Comentario
                    }).ToList();

                    registros = respCuentas.Resultado.CuentasExcluidas.Length;
                    totalRegistros = respCuentas.TotalRegistros;
                }
            }
        }
        catch (ValidacionExcepcion) { }
        catch (Exception)
        {
        }
        model.draw++;
        return new
        {
            drawn = model.draw,
            recordsTotal = registros,
            recordsFiltered = totalRegistros,
            data = cuentasExcluidas,
            url
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ExcluirCuentas(List<CuentaExclusionRequest> peticion)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        int idUsuario = 0;
        object errorCuentas = null;
        try
        {
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            if (peticion == null || peticion.Count == 0) throw new ValidacionExcepcion("Ingrese al menos una cuenta para excluir.");
            using (wsSOPF.CobranzaAdministrativaClient wsC = new wsSOPF.CobranzaAdministrativaClient())
            {
                var respExclusion = wsC.Domiciliacion_ExcluirCuentas(new wsSOPF.ExcluirCuentasRequest
                {
                    IdUsuario = idUsuario,
                    CuentasExcluir = peticion.Select(c => new wsSOPF.CuentaExcluyeDomiciliacionVM
                    {
                        IdSolicitud = c.IdSolicitud,
                        IdMotivoExclusionDomiciliacion = c.IdMotivo,
                        Comentario = c.Comentario
                    }).ToArray()
                });
                if (respExclusion != null)
                {
                    error = respExclusion.Error;
                    mensaje = respExclusion.MensajeOperacion;
                    errorCuentas = respExclusion.Resultado.ErrorCuentas;
                }
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = "Ocurrió un error al momento de excluir las cuentas";
        }
        return new { error, mensaje, url, errorCuentas };
    }
    #endregion
    #endregion

    public class CuentasRequest
    {
        public int? IdSolicitud { get; set; }
    }

    public class CuentaExclusionRequest
    {
        public int IdSolicitud { get; set; }
        public int IdMotivo { get; set; }
        public string Comentario { get; set; }
    }
}