﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="VerificacionRUC.aspx.cs" Inherits="Site_Cobranza_VerificacionRUC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" /> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.min.css" integrity="sha512-cyIcYOviYhF0bHIhzXWJQ/7xnaBuIIOecYoPZBgJHQKFPo+TOBA+BY1EnTpmM8yKDU4ZdI3UGccNGCEUdfbBqw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
 
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <script src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script src="../../js/datatables/1.10.19/jquery.dataTables.min.js"></script>
    <script src="../../js/datatables/1.10.19/dataTables.bootstrap.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script src="../../js/bootstrap/js/tooltip.js"></script>
    <script src="../../js/bootstrap/js/transition.js"></script>
    <script src="../../js/parsley.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.all.min.js" integrity="sha512-IZ95TbsPTDl3eT5GwqTJH/14xZ2feLEGJRbII6bRKtE/HC6x3N4cHye7yyikadgAsuiddCY2+6gMntpVHL1gHw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Verificación RUC</h3>
        </div>
        <div class="panel-body">
            <div class="row">    
                <div class="col-sm-12">
                <label class="small col-sm-12">Ingrese el número de RUC</label> 
                     </div>
                <div class="form-group col-md-6">   
                    <asp:TextBox ID="txtRUC" runat="server" CssClass="form-control" Text="" MaxLength="11"></asp:TextBox>
                </div>

                <div class="form-group col-md-6">
                    <asp:Button runat="server" ID="btnVerificar" Text="Verificar" CssClass="btn btn-sm btn-primary" OnClick="btnVerificarClick" />
                    <asp:Button runat="server" ID="btnGuardar" Text="Guardar" CssClass="btn btn-sm btn-primary"  Visible="false" OnClick="btnGuardarClick"/>
                </div>
            </div>
            <div class="col-sm-12">
                <hr />
            </div>
            <div class="row">   
                <asp:Label ID="lblNomEmpresa" runat="server" CssClass="col-sm-3 form-control-label small"  Font-Bold="true">
                                Nombre Empresa</asp:Label>   
                 <div class="form-group col-md-12">   
                    <asp:TextBox ID="txtNomEmpresa" runat="server" CssClass="form-control" Text="" Enabled="false"></asp:TextBox>
                </div>
            </div>
             <div class="row"> 
                 
                 <div class="form-group col-md-6">   
                     <label for="lblEstado" class="form-control-label small">Estado</label> 
                    <asp:TextBox ID="txtEstado" runat="server" CssClass="form-control" Text="" Enabled="false"></asp:TextBox>
                </div>
                 <div class="form-group col-md-6">
                     <label for="lblTipEmp" class="form-control-label small">Tipo Empresa</label> 
                    <asp:TextBox ID="txtTipEmp" runat="server" CssClass="form-control" Text="" Enabled="false"></asp:TextBox>
                </div>
            </div>
        </div>        
    </div>
    <div class="modal-pr"></div>
    <div id="divLoader" class="hidden">
        <div id="loader-background"></div>
        <div id="loader-content"></div>
    </div>

    <script type="text/javascript">
        function MensajeRUC() {
             Swal.fire("Mensaje", "Tienen que ser 11 números.", "warning");
        }
         function MensajeRUCDes(mensaje) {
              Swal.fire("Mensaje", mensaje, "warning");
        }
         function MensajeGuardarRUC() {
             Swal.fire("Mensaje", "Se guardo correctamente.", "success");
        }


    </script>
</asp:Content>
