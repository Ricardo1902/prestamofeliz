﻿using DataTables;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Site_Cobranza_VerificacionRUC : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }
    protected void btnGuardarClick(object sender, EventArgs e)
    {
        wsSOPF.VerificacionRUCResponse respuesta = new wsSOPF.VerificacionRUCResponse();
        using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
        {
            respuesta = ws.GuardarRUC(new wsSOPF.VerificacionRUCRequest
            {
                RUC = txtRUC.Text.Trim(),
                NombreEmp = txtNomEmpresa.Text.Trim(),
                Estado = txtEstado.Text.Trim(),
                TipoEmp=txtTipEmp.Text.Trim(),
                IdUsuario= Convert.ToInt32(Session["UsuarioId"])
            });
        }
        if (respuesta.Error != true)
        {
            txtNomEmpresa.Text = "";
            txtEstado.Text = "";
            txtTipEmp.Text = "";
            txtRUC.Text = "";
            btnGuardar.Visible = false;
            registrarScript("MensajeGuardarRUC();");
        }
        else
        {
            txtNomEmpresa.Text = "";
            txtEstado.Text = "";
            txtTipEmp.Text = "";
            txtRUC.Text = "";
            btnGuardar.Visible = false;
            registrarScript("MensajeRUCDes(" + respuesta.MensajeOperacion + ");");
        }
    }
    protected void btnVerificarClick(object sender, EventArgs e)
    {
        if (txtRUC.Text.Trim().Length !=11)
        {
            registrarScript("MensajeRUC();");
        }
        else
        {
            wsSOPF.VerificacionRUCResponse respuesta = new wsSOPF.VerificacionRUCResponse();
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                respuesta = ws.VerificarRUC(new wsSOPF.VerificacionRUCRequest
                {
                    RUC = txtRUC.Text.Trim()
                });
            }
            if (respuesta.Error != true)
            {
                txtNomEmpresa.Text = respuesta.data.nombre_o_razon_social;
                txtEstado.Text = respuesta.data.estado;
                txtTipEmp.Text = "PRIVADA";
                btnGuardar.Visible = true;
            }
            else
            {
                registrarScript("MensajeRUCDes("+respuesta.MensajeOperacion+");");
            }
        }
       
    }
    public void registrarScript(String pScript, String ScriptKey = "regscript")
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), pScript, true);
    }

    public void registrarScript(Page pPage, String pScript, String ScriptKey = "regscript")
    {
        ScriptManager.RegisterStartupScript(pPage, pPage.GetType(), Guid.NewGuid().ToString(), pScript, true);
    }
}