﻿var cuentas = [];
var motivos = [];
$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); $(".modal-pr").appendTo("body"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});
window.parsley.addValidator("seleccionCombo", {
    requirementType: "string",
    validateString: function (valor, control) {
        let esRequerido = $("#" + control).prop("required");
        if (esRequerido) {
            return valor != "" && valor != 0;
        } else return valor != "" && (valor > 0 || valor);
    },
    messages: {
        sp: "*"
    }
});
$(document).ready(function () {
    buscarCuentasExcluidas();
    $('[data-toggle="tooltip"]').tooltip();
    $("#dAgregarCuentas").on('shown.bs.collapse', function (e) {
        if ($.fn.dataTable.isDataTable("#tRegistros"))
            $("#tRegistros").DataTable().columns.adjust();
        mostrarCuentas();
    });
    $("#dAgregarCuentas").on('hide.bs.collapse', limpiarExcluirCuentas);
});
function buscarCuentasExcluidas() {
    var peticion = {
        IdSolicitud: $("#txIdSolicitud").val()
    };
    $('#tCuentasExlcuidas').DataTable({
        destroy: true,
        searching: false,
        ordering: false,
        serverSide: true,
        scrollX: true,
        scrollCollapse: true,
        fixedColumns: true,
        ajax: {
            url: "ExcluirCuentasDomiciliacion.aspx/ObtenerCuentasExcluidas",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d,
                    peticion: peticion
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d.url.length > 0) window.location = json.d.url;
                if (json.d.data == null) json.d.data = [];
                return JSON.stringify(json.d);
            }
        },
        columns: [
            { title: "Clave exclusión", data: "Id" },
            { title: "Cuenta", data: "IdSolicitud" },
            { title: "Motivo", data: "Motivo" },
            { title: "Fecha de Registro", data: "FechaRegistro" },
            { title: "Comentario", data: "Comentario" }
        ],
        columnDefs: [{ targets: 0, width: 150 }],
        language: español,
        order: [[0, "asc"]]
    });
}
function onAgregarCuentas() {
    cuentas = [{ IdSolicitud: null, IdMotivo: 0, Comentario: null }];
    $('#dAgregarCuentas').collapse('toggle');
}
function onCancelarExcluir() {
    $('#dAgregarCuentas').collapse('toggle');
    buscarCuentasExcluidas();
}
function mostrarCuentas() {
    if (cuentas == undefined || cuentas == null) cuentas = [];
    let motivosE = [];
    if (motivos != undefined && motivos != null && motivos.motivos != null != undefined && motivos.motivos != null) motivosE = motivos.motivos;

    $("#tRegistros").dataTable({
        destroy: true,
        searching: false,
        ordering: false,
        paging: false,
        scrollY: 200,
        scrollX: true,
        language: español,
        order: [[0, "asc"]],
        data: cuentas,
        columns: [
            {
                title: "No. Cuenta", data: "IdSolicitud", render: function (data, type, row, meta) {
                    if (type === "display") {
                        let cCuenta = $('<input>', {
                            id: "cCuenta_" + meta.row, type: 'text', class: 'form-control', value: data, 'required': true, 'data-parsley-required-message': '*',
                            'data-parsley-type': 'digits'
                        });
                        return cCuenta[0].outerHTML;
                    } else { return ""; }
                }
            },
            {
                title: "Motivo", data: "IdMotivo", render: function (data, type, row, meta) {
                    if (type === "display") {
                        let cMotivo = $('<select>', {
                            id: 'cMotivo_' + meta.row, class: 'form-control', 'required': true, 'data-parsley-seleccion-combo': "cMotivo_" + meta.row
                        });
                        motivosE.forEach(function (val, pos) {
                            let cMotivoOp = $('<option>', { value: val.Id, text: val.MotivoExclusion });
                            if (val.Id.toString() === row.IdMotivo) $(cMotivoOp).attr('selected', true);
                            cMotivo.append(cMotivoOp);
                        });
                        return cMotivo[0].outerHTML;
                    } else { return ""; }
                }
            },
            {
                title: "Comentario", data: "Comentario", render: function (data, type, row, meta) {
                    if (type === "display") {
                        let cComenario = $('<textarea>', {
                            id: "cComentario_" + meta.row, class: 'form-control', rows: 1, cols: 0, text: data, 'required': true,
                            'data-parsley-required-message': '*'
                        });
                        return cComenario[0].outerHTML;
                    } else { return ""; }
                }
            },
            {
                title: " ", render: function (data, type, row, meta) {
                    if (type === "display") {
                        let cAcciones = $('<div>', { class: 'row-cuentas' });
                        if (cuentas.length - 1 == meta.row) {
                            $(cAcciones).append(
                                $('<button>', {
                                    type: 'button', class: 'btn btn-success', title: 'Agregar', 'data-toggle': 'tooltip', 'data-placement': 'top', onclick: 'onRegAgregar();'
                                })
                                    .append($('<span>', { class: 'glyphicon glyphicon-plus-sign' })))
                        }
                        $(cAcciones).append($('<button>', {
                            type: 'button', class: 'btn btn-danger', title: 'Eliminar', 'data-toggle': 'tooltip', 'data-placement': 'top', onclick: 'onRegQuitar(' + meta.row + ');'
                        })
                            .append($('<span>', { class: 'glyphicon glyphicon-trash' })))
                        return cAcciones[0].outerHTML;
                    } else { return ""; }
                }
            }
        ],
        columnDefs: [
            { targets: 0, width: 150 },
            { targets: 1, width: 200 },
            { targets: 2, width: 300 },
        ],
        createdRow: function (data, type, row, meta) {
            data.setAttribute('data-id', row);
            $(data).bind("change", function (e) {
                let pos = this.getAttribute('data-id');
                if (cuentas != null) {
                    cuentas[pos].IdSolicitud = $(this).find("#cCuenta_" + pos).val();
                    cuentas[pos].IdMotivo = $(this).find("#cMotivo_" + pos).val();
                    cuentas[pos].Comentario = $(this).find("#cComentario_" + pos).val();
                }
            });
        },
    });
}
function onExcluirCuentas() {
    if (!$("#aspnetForm").parsley().validate()) { alert("Verifique todos los campos estén completos."); return; }
    if (!validarExcuirCuentas()) { alert("Ingrese al menos una cuenta para excluir."); return; }
    let parametros = {
        url: "ExcluirCuentasDomiciliacion.aspx/ExcluirCuentas",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ peticion: cuentas })
    };
    $.ajax(parametros)
        .done(function (data) {
            let resultado = data.d;
            let operacionCorrecta = null;
            let mensaje = '';
            let errorCuentas = [];
            if (resultado != null) {
                if (resultado.url.length > 0) window.location = resultado.url;
                operacionCorrecta = !resultado.error;
                if (operacionCorrecta === true) {
                    mensaje = (resultado != null && resultado.mensaje.length > 0) ? resultado.mensaje : "Las cuentas fueron excluidas correctamente!";
                } else {
                    mensaje = (resultado != null && resultado.mensaje.length > 0) ? resultado.mensaje : "Ocurrió un error al momento de excluir las cuentas.";
                }
                if (resultado.errorCuentas != null) errorCuentas = resultado.errorCuentas;
            }
            if (operacionCorrecta == null || operacionCorrecta == undefined) mensaje = "Favor de revisar los resultado obtenidos.";
            alert(mensaje);
            if (errorCuentas != null && errorCuentas.length > 0) {
                mostrarErrorCuentas(errorCuentas);
            } else onCancelarExcluir();
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de tratar de ejecutar la acción");
        });
}
function limpiarExcluirCuentas() {
    if ($.fn.dataTable.isDataTable("#tRegistros")) {
        $("#tRegistros").DataTable().destroy();
    }
    $("#tRegistros").empty();
    cuentas = [];
}
function mostrarErrorCuentas(cuentas) {
    if (cuentas == undefined || cuentas == null) cuentas = [];
    limpiarExcluirCuentas();
    $("#tRegistros").dataTable({
        destroy: true,
        searching: false,
        ordering: false,
        paging: false,
        scrollY: 200,
        scrollX: true,
        language: español,
        order: [[0, "asc"]],
        data: cuentas,
        columns: [
            { title: "No. Cuenta", data: "IdSolicitud" },
            { title: "Mensaje", data: "Mensaje" }
        ],
        columnDefs: [
            { targets: 0, width: 150 },
            { targets: 1, width: 300 },
        ]
    });
}
function validarExcuirCuentas() {
    if (cuentas != null && cuentas.length > 0) {
        for (let i = 0; i < cuentas.length; i++) {
            if (cuentas[i].IdSolicitud == null || cuentas[i].IdSolicitud == "" || cuentas[i].IdSolicitud.length == 0) return false;
            if (cuentas[i].IdMotivo == null || cuentas[i].IdMotivo == "0") return false;
            if (cuentas[i].Comentario == null || cuentas[i].Comentario.length == 0) return false;
        }
        return true;
    } else return false;
}
function onRegAgregar() {
    if (cuentas != null && cuentas.length > 0) {
        cuentas.push({ IdSolicitud: null, IdMotivo: 0, Comentario: null });
        mostrarCuentas();
    }
}
function onRegQuitar(pos) {
    if (cuentas != null && cuentas.length > 0 && pos >= 0 && cuentas.length - 1 > 0) {
        cuentas.splice(pos, 1);
        mostrarCuentas();
    }
}