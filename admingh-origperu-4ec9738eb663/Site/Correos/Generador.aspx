﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Generador.aspx.cs" Inherits="Site_Correos_Generador" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link href="Generador.css" rel="stylesheet" />
    <link href="../../Css/daterangepicker.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:HiddenField ID="hdnIdUsuario" runat="server" />
    <asp:HiddenField ID="hdnAlgoliaApplicationId" runat="server" />
    <asp:HiddenField ID="hdnAlgoliaApiKey" runat="server" />
    <asp:HiddenField ID="hdnAlgoliaMetadataIndex" runat="server" />
    <asp:HiddenField ID="hdnAlgoliaSmartTypeIndex" runat="server" />
    <asp:HiddenField ID="hdnAlgoliaClientEmailIndex" runat="server" />
    <asp:HiddenField ID="hdnImagesUrl" runat="server" />

    <div class="container-fluid">
        <div class="p-10 h-full w-full">
            <div class="flex flex-row items-stretch gap-4 w-full">
                <div class="flex flex-col w-3/12 py-5 px-8 space-y-8 border border-gray-200 rounded-md shadow border-l-8 border-l-blue-500">
                    <div class="w-full">
                        <label for="sltTemplate" class="block mb-1 text-md font-medium text-blue-500">Template</label>
                        <small class="block mb-2 text-base font-medium text-gray-400">Seleccionar el template deseado.</small>
                        <div class="relative w-full inline-block text-gray-700">
                            <select id="sltTemplate" class="w-full h-full m-0 py-2 pl-3 pr-8 text-xl placeholder-gray-600 text-gray-700 border border-solid border-gray-300 rounded transition ease-in-out appearance-none focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                            </select>
                            <div class="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
                                <svg class="w-4 h-4 fill-current" viewBox="0 0 20 20"><path d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" fill-rule="evenodd"></path></svg>
                            </div>
                        </div>
                    </div>
                    <div class="w-full">
                        <label for="inpName" class="block mb-1 text-md font-medium text-blue-500">Nombre</label>
                        <small class="block mb-2 text-base font-medium text-gray-400">Nombre descriptivo para identificar el uso del correo.</small>
                        <input type="text" id="inpName" class="bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Ej. Correo de aniversario" />
                    </div>
                    <div class="w-full">
                          <label for="inpDescription" class="block mb-1 text-md font-medium text-blue-500">Descripcion</label>
                          <small class="block mb-2 text-base font-medium text-gray-400">Breve descripcion del motivo de la generacion del correo.</small>
                          <textarea type="text" id="inpDescription" class="bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" rows="3" cols="3" placeholder="Ej. Fidelizacion de clientes por su aniversario"></textarea>
                    </div>
                    <div class="w-full">
                        <label for="eFrom" class="block mb-1 text-md font-medium text-blue-500">Cuenta remitente</label>
                        <small class="block mb-2 text-base font-medium text-gray-400">Cuenta con la que se enviara el correo.</small>
                        <div class="relative w-full inline-block text-gray-700">
                            <select id="sltAccount" class="w-full h-full m-0 py-2 pl-3 pr-8 text-xl placeholder-gray-600 text-gray-700 border border-solid border-gray-300 rounded transition ease-in-out appearance-none focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                            </select>
                            <div class="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
                                <svg class="w-4 h-4 fill-current" viewBox="0 0 20 20"><path d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" fill-rule="evenodd"></path></svg>
                            </div>
                        </div>
                    </div>
                    <div class="w-full">
                        <label for="dpSendDate" class="block mb-1 text-md font-medium text-blue-500">Fecha de envio</label>
                        <small class="flex justify-between items-center mb-2 text-base font-medium text-gray-400">
                            Indicar la fecha y hora en que se enviara el correo.
                        </small>
                        <input type="text" id="dpSendDate" class="bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" />
                    </div>
                    <div class="w-after:rounded-full">
                        <div id="repetitiveWrapper" class="w-full">
                            <label class="relative flex justify-start items-baseline text-base">
                                <input id="chkRepetitive" type="checkbox" class="absolute left-1/2 focus:outline-none -translate-x-1/2 w-full h-full peer appearance-none rounded-md" />
                                <span class="w-11 h-6 flex items-center mr-3 flex-shrink-0 p-1 bg-gray-300 rounded-full duration-300 peer-checked:bg-blue-500 after:w-4 after:h-4 after:bg-white after:rounded-full after:shadow-md after:duration-300 peer-checked:after:translate-x-4"></span>
                                Repetitivo
                            </label>
                        </div>
                        <div class="w-full">
                            <label class="relative flex justify-start items-baseline text-base">
                                <input id="chkUniqueEmail" type="checkbox" class="absolute left-1/2 focus:outline-none -translate-x-1/2 w-full h-full peer appearance-none rounded-md" />
                                <span class="w-11 h-6 flex items-center mr-3 flex-shrink-0 p-1 bg-gray-300 rounded-full duration-300 peer-checked:bg-blue-500 after:w-4 after:h-4 after:bg-white after:rounded-full after:shadow-md after:duration-300 peer-checked:after:translate-x-4"></span>
                                Correo Masivo
                            </label>
                        </div>
                        <div class="w-full">
                            <label class="relative flex justify-start items-baseline text-base">
                                <input id="chkFilesInZip" type="checkbox" class="absolute left-1/2 focus:outline-none -translate-x-1/2 w-full h-full peer appearance-none rounded-md" />
                                <span class="w-11 h-6 flex items-center mr-3 flex-shrink-0 p-1 bg-gray-300 rounded-full duration-300 peer-checked:bg-blue-500 after:w-4 after:h-4 after:bg-white after:rounded-full after:shadow-md after:duration-300 peer-checked:after:translate-x-4"></span>
                                Archivos en zip
                            </label>
                        </div>
                    </div>
                </div>

                <div class="flex flex-col w-9/12 space-y-5">
                    <div id="addressesWrapper" class="flex flex-col w-full relative">
                        <div class="relative z-0 w-full group">
                            <i class="fas fa-cloud-upload-alt hover:cursor-pointer absolute" style="right: 10px; top: 12px; font-size: 1.5rem;" onclick="HandleUploadAddresses('To')"></i>
                            <input type="text" id="inpTo" class="block py-3 pl-16 pr-16 w-full text-xl text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " autocomplete="off" data-offset="35"/>
                            <label for="inpTo" class="absolute text-xl text-gray-500 top-3 peer-focus:text-blue-600">Para:</label>
                        </div>
                        <div class="relative z-0 w-full group">
                            <i class="fas fa-cloud-upload-alt hover:cursor-pointer absolute" style="right: 10px; top: 12px; font-size: 1.5rem;" onclick="HandleUploadAddresses('CC')"></i>
                            <input type="text" id="inpCC" class="block py-3 pl-12 pr-16 w-full text-xl text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " autocomplete="off" data-offset="70"/>
                            <label for="inpCC" class="absolute text-xl text-gray-500 top-3 peer-focus:text-blue-600">CC:</label>
                        </div>
                        <div class="relative z-0 w-full group">
                            <i class="fas fa-cloud-upload-alt hover:cursor-pointer absolute" style="right: 10px; top: 12px; font-size: 1.5rem;" onclick="HandleUploadAddresses('BCC')"></i>
                            <input type="text" id="inpBCC" class="block py-3 pl-16 pr-16 w-full text-xl text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " autocomplete="off" data-offset="105" />
                            <label for="inpBCC" class="absolute text-xl text-gray-500 top-3 peer-focus:text-blue-600">BCC:</label>
                        </div>
                        <%--<div class="relative z-0 w-full group">
                            <input type="text" id="inpSubject" class="block py-3 pl-24 w-full text-xl text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />
                            <label for="inpSubject" class="absolute text-xl text-gray-500 top-3 peer-focus:text-blue-600">Asunto:</label>
                        </div>--%>

                        <div id="autocompletePanel" class="hidden flex-col shadow-md border border-gray-200 rounded-md bg-white autocomplete-panel"></div>
                        <input type="file" id="uploadAddressesCsv" class="hidden" data-upload-type="To" />
                    </div>

                    <div id="dpGenerationHourWrapper" class="hidden flex-col w-full">
                        <div class="w-full">
                            <label for="sltSmartType" class="block mb-1 text-md font-medium text-blue-500">Destinatarios</label>
                            <small class="block mb-2 text-base font-medium text-gray-400">Grupo de clientes a los cuales se les enviara este correo.</small>
                            <div class="relative w-full inline-block text-gray-700">
                                <select id="sltSmartTypes" class="w-full h-full m-0 py-2 pl-3 pr-8 text-xl placeholder-gray-600 text-gray-700 border border-solid border-gray-300 rounded transition ease-in-out appearance-none focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                                </select>
                                <div class="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
                                    <svg class="w-4 h-4 fill-current" viewBox="0 0 20 20"><path d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" fill-rule="evenodd"></path></svg>
                                </div>
                            </div>
                        </div>
                        <div class="w-full">
                            <label for="dpGenerationHour" class="block mb-1 text-md font-medium text-blue-500">Hora de generación</label>
                            <small class="flex justify-between items-center mb-2 text-base font-medium text-gray-400">
                                Indicar la hora en que se generara el correo diariamente.
                            </small>
                            <input type="time" id="dpGenerationHour" class="bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="HH:MM" />
                        </div>
                        <div class="w-full">
                            <label for="dpSendHour" class="block mb-1 text-md font-medium text-blue-500">Hora de envio</label>
                            <small class="flex justify-between items-center mb-2 text-base font-medium text-gray-400">
                                Indicar la hora en que se enviara el correo generado diariamente.
                            </small>
                            <input type="time" id="dpSendHour" class="bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="HH:MM" />
                        </div>
                        <div class="hidden w-full">
                            <label for="dpGenerateLastDateTime" class="block mb-1 text-md font-medium text-blue-500">Fecha ultima generacion</label>
                            <small class="flex justify-between items-center mb-2 text-base font-medium text-gray-400">
                                Indicar la fecha y hora en que se enviara el ultimo correo repetitivo.
                            </small>
                            <input type="text" id="dpGenerateLastDateTime" class="bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" />
                        </div>
                    </div>

                    <div class="flex flex-col w-full relative">
                        <div class="relative z-0 w-full group">
                            <input type="text" id="inpSubject" class="block py-3 pl-24 w-full text-xl text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />
                            <label for="inpSubject" class="absolute text-xl text-gray-500 top-3 peer-focus:text-blue-600">Asunto:</label>
                        </div>
                    </div>

                    <div class="flex flex-row w-full h-full">
                        <div class="relative">
                            <div id="emailSettingsWrapper" class="hidden flex-col h-full px-3 space-y-3" style="max-height: 500px; overflow: auto" data-status="close"></div>
                            <div id="autocompletePanelMetadata" class="flex flex-col shadow-md border border-gray-200 rounded-md bg-white autocomplete-metadata-panel"></div>
                        </div>
                        <div class="flex flex-col relative w-full h-full border border-gray-200" style="max-height: 500px; overflow: auto">
                            <a id="btnEmailSettings" class="absolute text-text-gray-900 hover:underline top-3 left-4 hover:cursor-not-allowed" style="font-size: 2rem;">
                                <i class="fas fa-cog"></i>
                            </a>
                            <div id="templateHtml" class="flex flex-col w-full h-full justify-center items-center">
                                Selecciona un template
                            </div>
                        </div>
                    </div>
                    <div class="flex items-center w-full">
                        <button id="btnGenerar" type="button" class="inline-flex items-center px-5 py-2.5 text-lg font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 hover:cursor-pointer">
                            Generar
                        </button>
                        <a id="btnAddAttachment" class="text-text-gray-900 hover:underline ml-4 hover:cursor-not-allowed" style="font-size: 2rem;">
                            <i class="fas fa-paperclip"></i>
                        </a>
                        <input id="uploadAttachment" class="hidden" type="file" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="../../Scripts/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../../Scripts/daterangepicker.min.js"></script>
    <script type="text/javascript" src="../../Scripts/algoliasearch.umd.js"></script>
    <script type="text/javascript" src="../../Scripts/autocomplete.js"></script>
    <script type="text/javascript" src="../../Scripts/textarea-caret.js"></script>
    <script type="text/javascript" src="Generador.js"></script>
</asp:Content>

