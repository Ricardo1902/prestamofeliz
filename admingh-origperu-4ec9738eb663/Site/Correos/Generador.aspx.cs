﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;
using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;

public partial class Site_Correos_Generador : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int tempIdUsuario = 0;
            if (Session["UsuarioId"] != null) int.TryParse(Session["UsuarioId"].ToString(), out tempIdUsuario);

            hdnIdUsuario.Value = tempIdUsuario.ToString();
            hdnAlgoliaApplicationId.Value = "WPBSP39EMG";
            hdnAlgoliaApiKey.Value = "8d6f6707676918706c86a936bf6ebdae";
            hdnAlgoliaMetadataIndex.Value = "PF.Email.Metadata";
            hdnAlgoliaClientEmailIndex.Value = "PF.Client.Email";
            hdnAlgoliaSmartTypeIndex.Value = "PF.Smart.Type";
            LoadInitialData();
        }
    }

    private void LoadInitialData()
    {
        HubbleClient hubbleClient = new HubbleClient();
        List<Template> templates = new List<Template>();
        List<TB_TemplateParamInputTypes> types = new List<TB_TemplateParamInputTypes>();
        List<TB_EmailAccounts> emailAccounts = new List<TB_EmailAccounts>();
        List<TB_SmartTypes> smartTypes = new List<TB_SmartTypes>();

        try
        {
            templates = hubbleClient.GetAllTemplates().ToList();
            types = hubbleClient.GetAllTemplateParamInputTypes().ToList();
            emailAccounts = hubbleClient.GetAllAccounts().ToList();
            smartTypes = hubbleClient.GetAllSmartTypes().ToList();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "InitializeEmailSettings(" + JsonConvert.SerializeObject(templates) + ", " + JsonConvert.SerializeObject(types.Select(t => new { Id = t.IdEmailTemplateParamType, t.Tipo, t.HtmlFragment })) + ", " + JsonConvert.SerializeObject(emailAccounts.Select(e => new { Id = e.IdEmailAccount, Account = e.Cuenta })) + ", " + JsonConvert.SerializeObject(smartTypes.Select(st => new { Id = st.IdSmartType, Type = st.Tipo })) + ");", true);
        }
        catch (Exception ex)
        {

        }

        hubbleClient.Close();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GetTemplateHtml(string template)
    {
        string templatePath = "C:/tmp/Hubble/Templates/";
        string html = "";
        bool success = false;

        try
        {
            html = File.ReadAllText(Path.Combine(templatePath, template + ".html"));
            success = true;
        }
        catch
        {

        }

        return new { success, html };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object SaveEmailSchedule(string jsonEmailSetting)
    {
        EmailSetting emailSetting;
        bool created = false;
        string error = "";

        try
        {
            string attachmentsPath = System.Configuration.ConfigurationManager.AppSettings["AttachmentsTemplate"];
            emailSetting = JsonConvert.DeserializeObject<EmailSetting>(jsonEmailSetting);
            HubbleClient hubbleClient = new HubbleClient();

            if (!emailSetting.IsRepetitive)
            {
                string[] To = !String.IsNullOrEmpty(emailSetting.Addresses.To) ? emailSetting.Addresses.To.Trim().Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries) : Array.Empty<string>();
                string[] CC = !String.IsNullOrEmpty(emailSetting.Addresses.CC) ? emailSetting.Addresses.CC.Trim().Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries) : Array.Empty<string>();
                string[] BCC = !String.IsNullOrEmpty(emailSetting.Addresses.BCC) ? emailSetting.Addresses.BCC.Trim().Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries) : Array.Empty<string>();

                created = hubbleClient.CreateEmailSchedule(
                    emailSetting.Name,
                    emailSetting.Description,
                    emailSetting.Subject,
                    emailSetting.SendDateTime.Value,
                    emailSetting.IsUniqueEmail,
                    emailSetting.ZipFiles,
                    emailSetting.TemplateId,
                    emailSetting.AccountId,
                    emailSetting.UserId,
                    To,
                    CC,
                    BCC,
                    emailSetting.TemplateParams.Select(tp => new ReqTemplateParam { Id = tp.Id, Value = tp.value }).ToArray(),
                    emailSetting.Attachments.Select(a => Path.Combine(attachmentsPath, a)).ToArray()
                );
            }
            else
            {
                created = hubbleClient.CreateSmartEmailSchedule(
                    emailSetting.Name,
                    emailSetting.Description,
                    emailSetting.Subject,
                    emailSetting.GenerateTime.Value,
                    emailSetting.SendTime.Value,
                    DateTime.MinValue,
                    emailSetting.ZipFiles,
                    emailSetting.TemplateId,
                    emailSetting.AccountId,
                    emailSetting.SmartType.Value,
                    emailSetting.UserId,
                    emailSetting.TemplateParams.Select(tp => new ReqTemplateParam { Id = tp.Id, Value = tp.value }).ToArray(),
                    emailSetting.Attachments.Select(a => Path.Combine(attachmentsPath, a)).ToArray()
                );
            }

            hubbleClient.Close();
        }
        catch(Exception ex)
        {
            created = false;
            error = ex.Message;
        }

        return new { created, error };
    }

    public class EmailSetting
    {
        public int AccountId { get; set; }
        public Address Addresses { get; set; }
        public List<string> Attachments { get; set; }
        public string Description { get; set; }
        public DateTime? GenerateTime { get; set; }
        public DateTime? SendTime { get; set; }
        public int? SmartType { get; set; }
        public bool IsRepetitive { get; set; }
        public bool IsUniqueEmail { get; set; }
        public string Name { get; set; }
        public DateTime? SendDateTime { get; set; }
        public string Subject { get; set; }
        public int TemplateId { get; set; }
        public List<TemplateParam> TemplateParams { get; set; }
        public bool ZipFiles { get; set; }
        public int UserId { get; set; }
    }

    public class TemplateParam
    {
        public int Id { get; set; }
        public string value { get; set; }
    }

    public class Address
    {
        public string To { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
    }
}