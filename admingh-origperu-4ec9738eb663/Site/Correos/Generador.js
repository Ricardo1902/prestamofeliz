﻿// types

class EmailSetting {
    #Templates = [];
    #Accounts = [];
    #TemplateParamTypes = [];
    #SmartTypes = [];
    Name;
    Description;
    SendDateTime;
    GenerateTime;
    SendTime;
    SmartTypeId;
    TemplateId;
    AccountId;
    TemplateParams = [];
    Attachments = [];
    ZipFiles = false;
    IsRepetitive = false;
    IsUniqueEmail = true;
    Addresses = {
        To: "",
        CC: "",
        BCC: ""
    };
    Subject;

    constructor(templates, types, accounts, smartTypes) {
        this.#Templates = templates;
        this.#TemplateParamTypes = types;
        this.#Accounts = accounts;
        this.#SmartTypes = smartTypes;
    }

    get TemplatesForSelectOption() {
        return this.#Templates.map(t => ({ id: t.Id, name: t.Name }));
    }

    get TemplateInputTypesForSelectOption() {
        return this.#TemplateParamTypes.map(t => ({ id: t.Id, type: t.Tipo }));
    }

    get SmartTypesForSelectOption() {
        return this.#SmartTypes.map(st => ({ id: st.Id, type: st.Type }));
    }

    get AccountsForSelectOption() {
        return this.#Accounts.map(t => ({ id: t.Id, account: t.Account }));
    }

    get TemplateParameters() {
        return this.#Templates.find(t => t.Id == this.TemplateId)?.Parameters;
    }

    get TemplateNameSelected() {
        return this.#Templates.find(t => t.Id == this.TemplateId)?.Name;
    }

    get TemplateTypeSelected() {
        return this.#Templates.find(t => t.Id == this.TemplateId)?.Tipo;
    }

    get IsValid() {
        if (!this.IsRepetitive) {
            return this.TemplateId > 0 &&
                this.Name != undefined &&
                this.Name != "" &&
                this.Description != undefined &&
                this.Description != "" &&
                this.AccountId > 0 &&
                this.SendDateTime != undefined &&
                this.SendDateTime != "" &&
                this.Addresses.To != "" &&
                this.Subject != undefined &&
                this.Subject != "";
        }
        else {
            return this.TemplateId > 0 &&
                this.Name != undefined &&
                this.Name != "" &&
                this.Description != undefined &&
                this.Description != "" &&
                this.AccountId > 0 &&
                this.Subject != undefined &&
                this.Subject != "" &&
                this.SmartTypeId > 0 &&
                this.GenerateTime != undefined &&
                this.GenerateTime != "" &&
                this.SendTime != undefined &&
                this.SendTime != "";
        }
    }

    GetTemplateParamValueById(id) {
        return this.TemplateParams.find(tp => tp.Id == id)?.value;
    }

    GetTemplateParamInputTypeById(id) {
        return this.#TemplateParamTypes.find(pt => pt.Id == id);
    }

    CreateOrUpdateTemplateParam(id, value) {
        let param = this.TemplateParams.find(tp => tp.Id == id) || null;

        if (param) { param.value = value; }
        else { this.TemplateParams.push({ Id: id, value: value }); }
    }

    GenerateFreeEditParam() {
        let finalValue = "";
        let paramId = this.#Templates.find(t => t.Id == this.TemplateId)?.Parameters[0].Id;

        this.TemplateParams.forEach(tp => {
            finalValue += tp.value;
        });
        this.TemplateParams = [];
        this.TemplateParams.push({ Id: paramId, value: finalValue });
    }

    DeleteTemplateParam(id) {
        this.TemplateParams = this.TemplateParams.filter(tp => tp.Id != id);
    }

    Clear() {
        this.Attachments = [];
        this.TemplateParams = [];
    }
}

datepickerLocale = {
    separator: " - ",
    applyLabel: "Aplicar",
    cancelLabel: "Cancelar",
    fromLabel: "DE",
    toLabel: "HASTA",
    customRangeLabel: "Custom",
    daysOfWeek: [
        "Dom",
        "Lun",
        "Mar",
        "Mie",
        "Jue",
        "Vie",
        "Sáb"
    ],
    monthNames: [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    ],
    firstDay: 1
}

// global variables

const { autocomplete, getAlgoliaResults } = window['@algolia/autocomplete-js'];
let emailSetting;
let algoliaClient;
let metadataIndex;
let smartTypeIndex;
let clientEmailIndex;

function InitializeEmailSettings(templates, types, accounts, smartTypes) {
    try {
        emailSetting = new EmailSetting(templates, types, accounts, smartTypes);
        CheckRepetitiveWrapperPermissions();
    } catch (e) {
        console.error(e);
    }
}

function domReady(fn) {
    // If we're early to the party
    document.addEventListener("DOMContentLoaded", fn);
    // If late; I mean on time.
    if (document.readyState === "interactive" || document.readyState === "complete") {
        fn();
    }
}

domReady(() => {
    moment.locale('es');

    // Initialize inputs
    configureAlgolia();
    configureTemplateSelect();
    configureAccountSelect();
    configureNameInput();
    configureDescriptionInput();
    configureSendDateDP();
    configureGenerationHourDP();
    configureSendHourDP();
    configureSmartTypesSelect();
    configureRepetitiveCheck();
    configureUniqueEmailCheck();
    configureFilesInZipCheck();
    configureToInput();
    configureCCInput();
    configureBCCInput();
    configureSubjectInput();
    configureEmailSettings();
    configureAttachments();
    configureUploadAttachment();
    configureUploadAddresses();
    configureGenerate();

    // fix
    $('textarea').keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            this.value = this.value + "\n";
        }
    });
});

function configureAlgolia() {
    const hdnIdUsuario = document.getElementById("ctl00_MainContent_hdnIdUsuario");
    const hdnAlgoliaApplicationId = document.getElementById("ctl00_MainContent_hdnAlgoliaApplicationId");
    const hdnAlgoliaApiKey = document.getElementById("ctl00_MainContent_hdnAlgoliaApiKey");
    const hdnAlgoliaMetadataIndex = document.getElementById("ctl00_MainContent_hdnAlgoliaMetadataIndex");
    const hdnAlgoliaClientEmailIndex = document.getElementById("ctl00_MainContent_hdnAlgoliaClientEmailIndex");
    const hdnAlgoliaSmartTypeIndex = document.getElementById("ctl00_MainContent_hdnAlgoliaSmartTypeIndex");

    algoliaClient = algoliasearch(hdnAlgoliaApplicationId.value, hdnAlgoliaApiKey.value, {
        headers: {
            'X-Algolia-UserToken': hdnIdUsuario.value
        }
    });
    metadataIndex = algoliaClient.initIndex(hdnAlgoliaMetadataIndex.value);
    clientEmailIndex = algoliaClient.initIndex(hdnAlgoliaClientEmailIndex.value);
    smartTypeIndex = algoliaClient.initIndex(hdnAlgoliaSmartTypeIndex.value);
}

function configureNameInput() {
    const inpName = document.getElementById("inpName");

    // Add event
    inpName.addEventListener("input", (e) => {
        const value = e.target.value;

        emailSetting.Name = value;
    });
}

function configureDescriptionInput() {
    const inpDescription = document.getElementById("inpDescription");

    // Add event
    inpDescription.addEventListener("input", (e) => {
        const value = e.target.value;

        emailSetting.Description = value;
    });
}

function configureTemplateSelect() {
    const sltTemplate = document.getElementById("sltTemplate");
    const emailSettingsWrapper = document.getElementById("emailSettingsWrapper");
    const hdnIdUsuario = document.getElementById("ctl00_MainContent_hdnIdUsuario");
    const templatesUser = ["PF_Solo_Imagen", "PF_Edicion_Libre"];

    // Initialize
    let options = "<option value='-1'>Seleccionar</option>";

    emailSetting.TemplatesForSelectOption.forEach(te => {
        if (parseInt(hdnIdUsuario.value) != 21 && templatesUser.includes(te.name)) {
            options += `<option value="${te.id}">${te.name}</option>`;
        }

        if (parseInt(hdnIdUsuario.value) == 21) {
            options += `<option value="${te.id}">${te.name}</option>`;
        }
    });

    sltTemplate.innerHTML = options;

    // Add event
    sltTemplate.addEventListener("change", async (e) => {
        const templateHtml = document.getElementById("templateHtml");
        const btnAddAttachment = document.getElementById("btnAddAttachment");
        const btnEmailSettings = document.getElementById("btnEmailSettings");
        const value = parseInt(e.target.value);

        emailSetting.TemplateId = value == -1 ? 0 : value;

        if (emailSetting.TemplateId > 0) {
            const resp = await fetch(`Generador.aspx/GetTemplateHtml`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ template: emailSetting.TemplateNameSelected })
            });
            const data = await resp.json();

            if (data.d.success) {
                const parser = new DOMParser();
                const html = parser.parseFromString(data.d.html, "text/html");

                templateHtml.innerHTML = html.getElementsByTagName("body")[0].innerHTML;
                templateHtml.classList.remove("justify-center");

                btnEmailSettings.classList.remove("hover:cursor-not-allowed");
                btnEmailSettings.classList.add("hover:cursor-pointer");
                btnAddAttachment.classList.remove("hover:cursor-not-allowed");
                btnAddAttachment.classList.add("hover:cursor-pointer");

                emailSettingsWrapperStatus = emailSettingsWrapper.dataset.status;
                if (emailSettingsWrapperStatus == "open") {
                    emailSettingsWrapper.classList.toggle("hidden");
                    emailSettingsWrapper.dataset.status = "close";
                }

                emailSetting.Clear();
            }
        }
        else {
            templateHtml.innerHTML = "Selecciona un template";
            templateHtml.classList.add("justify-center");

            btnEmailSettings.classList.remove("hover:cursor-pointer");
            btnEmailSettings.classList.add("hover:cursor-not-allowed");
            btnAddAttachment.classList.remove("hover:cursor-pointer");
            btnAddAttachment.classList.add("hover:cursor-not-allowed");
        }
    });
}

function configureAccountSelect() {
    const sltAccount = document.getElementById("sltAccount");

    // Initialize
    let options = "<option value='-1'>Seleccionar</option>";

    emailSetting.AccountsForSelectOption.forEach(acc => {
        options += `<option value="${acc.id}">${acc.account}</option>`;
    });

    sltAccount.innerHTML = options;

    // Add event
    sltAccount.addEventListener("change", (e) => {
        const value = parseInt(e.target.value);

        emailSetting.AccountId = value == -1 ? 0 : value;
    });
}

function configureSendDateDP() {
    // Initialize
    $('#dpSendDate').daterangepicker({
        opens: 'center',
        drops: "auto",
        singleDatePicker: true,
        timePicker: true,
        minDate: moment(),
        format: "DD/MM/YYYY HH:mm",
        locale: datepickerLocale,
        autoUpdateInput: false
    });

    // Add event
    $('#dpSendDate').on('apply.daterangepicker', function (ev, picker) {
        emailSetting.SendDateTime = picker.startDate.format('YYYY-MM-DD HH:mm');
        $('#dpSendDate').val(picker.startDate.format('YYYY-MM-DD HH:mm'));
    });
}

function configureGenerationHourDP() {
    const dpGenerationHour = document.getElementById("dpGenerationHour");

    // Add event
    dpGenerationHour.addEventListener("change", (e) => {
        emailSetting.GenerateTime = e.target.value;
    });
}

function configureSendHourDP() {
    const dpSendHour = document.getElementById("dpSendHour");

    // Add event
    dpSendHour.addEventListener("change", (e) => {
        emailSetting.SendTime = e.target.value;
    });
}

function configureSmartTypesSelect() {
    const sltSmartTypes = document.getElementById("sltSmartTypes");

    // Initialize
    let options = "<option value='-1'>Seleccionar</option>";

    emailSetting.SmartTypesForSelectOption.forEach(st => {
        options += `<option value="${st.id}">${st.type}</option>`;
    });

    sltSmartTypes.innerHTML = options;

    // Add event
    sltSmartTypes.addEventListener("change", (e) => {
        const value = parseInt(e.target.value);

        emailSetting.SmartTypeId = value == -1 ? 0 : value;
    });
}

function configureRepetitiveCheck() {
    const hdnIdUsuario = document.getElementById("ctl00_MainContent_hdnIdUsuario");

    if (parseInt(hdnIdUsuario.value) == 21) {
        const chkRepetitive = document.getElementById("chkRepetitive");

        // Add event
        chkRepetitive.addEventListener("change", (e) => {
            const isChecked = e.target.checked;
            const dpGenerationHourWrapper = document.getElementById("dpGenerationHourWrapper");
            const addressesWrapper = document.getElementById("addressesWrapper");
            const dpSendDate = document.getElementById("dpSendDate");

            emailSetting.IsRepetitive = isChecked;
            if (isChecked) {
                dpGenerationHourWrapper.classList.remove("hidden");
                dpGenerationHourWrapper.classList.add("flex");
                addressesWrapper.classList.add("hidden");
                addressesWrapper.classList.remove("flex");
                dpSendDate.setAttribute("disabled", "disabled");
                dpSendDate.classList.add("hover:cursor-not-allowed");
                dpSendDate.value = "";
                emailSetting.SendDateTime = "";
            }
            else {
                dpGenerationHourWrapper.classList.add("hidden");
                dpGenerationHourWrapper.classList.remove("flex");
                addressesWrapper.classList.remove("hidden");
                addressesWrapper.classList.add("flex");
                dpSendDate.removeAttribute("disabled", "disabled");
                dpSendDate.classList.remove("hover:cursor-not-allowed");
                emailSetting.GenerateTime = "";
                emailSetting.SendTime = "";
                emailSetting.SmartTypeId = 0;
            }
        });
    }
}

function configureUniqueEmailCheck() {
    const chkUniqueEmail = document.getElementById("chkUniqueEmail");

    // Add event
    chkUniqueEmail.addEventListener("change", (e) => {
        const isChecked = e.target.checked;

        emailSetting.IsUniqueEmail = !isChecked;
    });
}

function configureFilesInZipCheck() {
    const chkFilesInZip = document.getElementById("chkFilesInZip");

    // Add event
    chkFilesInZip.addEventListener("change", (e) => {
        const isChecked = e.target.checked;

        emailSetting.ZipFiles = isChecked;
    });
}

function configureToInput() {
    const inpTo = document.getElementById("inpTo");

    // Add event
    inpTo.addEventListener("input", (e) => {
        const value = e.target.value;

        emailSetting.Addresses.To = value;
        handleAutocompleteInput(e.target);
    });
}

function configureCCInput() {
    const inpCC = document.getElementById("inpCC");

    // Add event
    inpCC.addEventListener("input", (e) => {
        const value = e.target.value;

        emailSetting.Addresses.CC = value;
        handleAutocompleteInput(e.target);
    });
}

function configureBCCInput() {
    const inpBCC = document.getElementById("inpBCC");

    // Add event
    inpBCC.addEventListener("input", (e) => {
        const value = e.target.value;

        emailSetting.Addresses.BCC = value;
        handleAutocompleteInput(e.target);
    });
}

function configureSubjectInput() {
    const inpSubject = document.getElementById("inpSubject");

    // Add event
    inpSubject.addEventListener("input", (e) => {
        const value = e.target.value;

        emailSetting.Subject = value;
    });
}

function configureEmailSettings() {
    const btnEmailSettings = document.getElementById("btnEmailSettings");

    // Add event
    btnEmailSettings.addEventListener("click", () => {
        const emailSettingsWrapper = document.getElementById("emailSettingsWrapper");
        const currentStatus = emailSettingsWrapper.dataset.status;

        if (currentStatus == "close" && emailSetting.TemplateId > 0) {
            const templateParameters = emailSetting.TemplateParameters;
            const templateType = emailSetting.TemplateTypeSelected;

            if (templateParameters && templateParameters.length > 0 && templateType != "Edicion libre") {
                let dynamicImputs = "";

                templateParameters.forEach(tp => {
                    const currentValue = emailSetting.GetTemplateParamValueById(tp.Id);

                    switch (tp.InputType) {
                        case "String":
                            dynamicImputs += `
                                <div class="w-full">
                                    <label for="parameter-${tp.Parameter}" class="block mb-1 text-md font-medium text-blue-500">${tp.FriendlyName}</label>
                                    <small class="block mb-2 text-base font-medium text-gray-400">${tp.Help}</small>
                                    <input type="text" id="parameter-${tp.Parameter}" class="bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="${tp.Placeholder}" oninput="registerEmailParameter(this, ${tp.Id}, '${tp.InputType}', '${tp.Parameter}')" ${currentValue ? `value="${currentValue}"` : ""} />
                                </div>
                            `;
                            break;
                        case "LongString":
                            dynamicImputs += `
                                <div class="w-full">
                                    <label for="parameter-${tp.Parameter}" class="block mb-1 text-md font-medium text-blue-500">${tp.FriendlyName}</label>
                                    <small class="block mb-2 text-base font-medium text-gray-400">${tp.Help}</small>
                                    <textarea id="parameter-${tp.Parameter}" class="bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 resize-none" rows="4" cols="50" placeholder="${tp.Placeholder}" oninput="registerEmailParameter(this, ${tp.Id}, '${tp.InputType}', '${tp.Parameter}')">${currentValue ? currentValue : ""}</textarea>
                                </div>
                            `;
                            break;
                        case "Image":
                            dynamicImputs += `
                                <div class="w-full">
                                    <label for="parameter-${tp.Parameter}" class="block mb-1 text-md font-medium text-blue-500">${tp.FriendlyName}</label>
                                    <small class="block mb-2 text-base font-medium text-gray-400">${tp.Help}</small>
                                    <input type="file" id="parameter-${tp.Parameter}" class="bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 resize-none" placeholder="${tp.Placeholder}" onchange="registerEmailParameter(this, ${tp.Id}, '${tp.InputType}', '${tp.Parameter}')" />
                                </div>
                            `;
                            break;
                        default:
                            console.log("input unknow");
                            break;
                    }
                });

                emailSettingsWrapper.innerHTML = dynamicImputs;

                $('textarea').keypress(function (event) {
                    if (event.which == 13) {
                        event.preventDefault();
                        this.value = this.value + "\n";
                    }
                });
            }
            else {
                const inputTypes = emailSetting.TemplateInputTypesForSelectOption;
                let options = "";

                inputTypes.filter(it => ["String", "LongString"].includes(it.type)).forEach(it => {
                    switch (it.type) {
                        case "String":
                            options += `
                                <div class="w-full">
                                    <label for="input-${it.type}" class="block mb-1 text-md font-medium text-blue-500">Titulo</label>
                                    <input type="text" id="input-${it.type}" class="mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" oninput="handleMetadataAutocompleteInput(this)" />
                                    <button type="button" onclick="AddDynamicSeccion(${it.id}, '${it.type}')" class="w-full inline-flex items-center justify-center px-5 py-2.5 text-lg font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                        Agregar
                                    </button>
                                </div>
                            `;
                            break;
                        case "LongString":
                            options += `
                                <div class="w-full">
                                    <label for="input-${it.type}" class="block mb-1 text-md font-medium text-blue-500">Parrafo</label>
                                    <textarea id="input-${it.type}" class="mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 resize-none" rows="4" cols="50" oninput="handleMetadataAutocompleteInput(this)"></textarea>
                                    <button type="button" onclick="AddDynamicSeccion(${it.id}, '${it.type}')" class="w-full inline-flex items-center justify-center px-5 py-2.5 text-lg font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                        Agregar
                                    </button>
                                </div>
                            `;
                            break;
                        default:
                    }
                });

                let sltOptions = "";
                inputTypes.filter(it => ["Image", "ImageLink", "ImageWhatsApp", "ImageEmail"].includes(it.type)).forEach(it => {
                    const optionsFriendlyName = {
                        "Image": "Imagen",
                        "ImageLink": "Imagen con url",
                        "ImageWhatsApp": "Imagen con whatsapp",
                        "ImageEmail": "Imagen con email"
                    }

                    sltOptions += `
                        <option value="${it.id}">${optionsFriendlyName[it.type]}</option>
                    `;
                });

                options += `
                    <div class="w-full">
                        <label for="input-image-type" class="block mb-1 text-md font-medium text-blue-500">Tipo imagen</label>
                        <div class="relative w-full inline-block text-gray-700 mb-2">
                            <select id="input-image-type" onchange="HandleImageTypeChange()" class="w-full h-full m-0 py-2 pl-3 pr-8 text-xl placeholder-gray-600 text-gray-700 border border-solid border-gray-300 rounded transition ease-in-out appearance-none focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" data-current-type="Image">
                                ${sltOptions}
                            </select>
                            <div class="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
                                <svg class="w-4 h-4 fill-current" viewBox="0 0 20 20"><path d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" fill-rule="evenodd"></path></svg>
                            </div>
                        </div>
                        <input type="file" id="input-Image" class="mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 resize-none" />
                        <input type="text" id="input-image-extra-data" class="hidden mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg placeholder:text-gray-400 outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 resize-none" placeholder="" />
                        <button type="button" onclick="HandleDynamicSltSection()" class="w-full inline-flex items-center justify-center px-5 py-2.5 text-lg font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                            Agregar
                        </button>
                    </div>
                `;

                emailSettingsWrapper.innerHTML = options;
                HandleDynamicSltSection();

                $('textarea').keypress(function (event) {
                    if (event.which == 13) {
                        event.preventDefault();
                        this.value = this.value + "\n";
                    }
                });
            }
        }
        else {
            emailSettingsWrapper.innerHTML = "";
        }

        if (emailSetting.TemplateId > 0) {
            emailSettingsWrapper.dataset.status = currentStatus == "close" ? "open" : "close";
            emailSettingsWrapper.classList.toggle("hidden");
        }
    });
}

function configureAttachments() {
    const btnAddAttachment = document.getElementById("btnAddAttachment");
    const uploadAttachment = document.getElementById("uploadAttachment");

    // Add event
    btnAddAttachment.addEventListener("click", () => {
        if (emailSetting.TemplateId > 0) {
            uploadAttachment.click();
        }
    });
}

function configureUploadAttachment() {
    const uploadAttachment = document.getElementById("uploadAttachment");

    // Add event
    uploadAttachment.addEventListener("change", async (e) => {
        const templateHtml = document.getElementById("templateHtml");
        const formData = new FormData();
        const file = e.target.files[0];
        const lastDot = file.name.lastIndexOf('.');
        const fileName = GetUniqueId();
        const ext = file.name.substring(lastDot + 1);

        formData.append(`${fileName}.${ext}`, file);

        const resp = await fetch("TemplateAttachmentHandler.ashx", {
            method: "POST",
            body: formData
        });

        if (resp.ok) {
            emailSetting.Attachments.push(`${fileName}.${ext}`);

            templateHtml.innerHTML += `
                <div id="${fileName}-${ext}" class="flex flex-col w-full py-2 px-5 underline">
                    <p><i class="fas fa-file"></i> ${file.name} <i onclick="removeAttachment('${fileName}-${ext}')" class="text-lg fas fa-times text-red-500"></i></p>
                </div>
            `;
        }
    });
}

function configureUploadAddresses() {
    const uploadAddressesCsv = document.getElementById("uploadAddressesCsv");

    // Add event
    uploadAddressesCsv.addEventListener("change", (e) => {
        const reader = new FileReader();
        const type = e.target.dataset.uploadType;
        const file = e.target.files[0];

        reader.onload = function (event) {
            switch (type) {
                case "To":
                    const inpTo = document.getElementById("inpTo");
                    inpTo.value += event.target.result.split("\n").map(r => `${r}; `).toString().replaceAll(",", "");
                    emailSetting.Addresses.To = inpTo.value;
                    break;
                case "CC":
                    const inpCC = document.getElementById("inpCC");
                    inpCC.value += event.target.result.split("\n").map(r => `${r}; `).toString().replaceAll(",", "");
                    emailSetting.Addresses.CC = inpCC.value;
                    break;
                case "BCC":
                    const inpBCC = document.getElementById("inpBCC");
                    inpBCC.value += event.target.result.split("\n").map(r => `${r}; `).toString().replaceAll(",", "");
                    emailSetting.Addresses.BCC = inpBCC.value;
                    break;
                default:
            }
        };

        reader.readAsText(file);
        e.target.value = "";
    });
}

function configureGenerate() {
    const btnGenerar = document.getElementById("btnGenerar");

    // Add event
    btnGenerar.addEventListener("click", async () => {
        const hdnIdUsuario = document.getElementById("ctl00_MainContent_hdnIdUsuario");
        btnGenerar.innerText = "Guardando...";
        btnGenerar.setAttribute("disabled", "disabled");
        btnGenerar.classList.remove("hover:cursor-pointer");
        btnGenerar.classList.add("hover:cursor-not-allowed");

        if (emailSetting.IsValid) {
            if (emailSetting.TemplateNameSelected == "PF_Edicion_Libre") { emailSetting.GenerateFreeEditParam(); }

            const resp = await fetch(`Generador.aspx/SaveEmailSchedule`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    jsonEmailSetting: JSON.stringify({
                        AccountId: emailSetting.AccountId,
                        Addresses: emailSetting.Addresses,
                        Attachments: emailSetting.Attachments,
                        Description: emailSetting.Description,
                        GenerateTime: emailSetting.GenerateTime ? `0001-01-01 ${emailSetting.GenerateTime}` : null,
                        SendTime: emailSetting.SendTime ? `0001-01-01 ${emailSetting.SendTime}` : null,
                        SmartType: emailSetting.SmartTypeId,
                        IsRepetitive: emailSetting.IsRepetitive,
                        IsUniqueEmail: emailSetting.IsUniqueEmail,
                        Name: emailSetting.Name,
                        SendDateTime: emailSetting.SendDateTime,
                        Subject: emailSetting.Subject,
                        TemplateId: emailSetting.TemplateId,
                        TemplateParams: emailSetting.TemplateParams,
                        ZipFiles: emailSetting.ZipFiles,
                        UserId: hdnIdUsuario.value
                    })
                })
            });

            const result = await resp.json();

            if (result.d.created) {
                alert("Se genero el email correctamente.");
                window.location.reload();
            }
            else {
                alert(result.d.error);
            }
        }
        else {
            alert("Faltan datos requeridos por llenar, favor de validar.")
        }

        btnGenerar.innerText = "Generar";
        btnGenerar.removeAttribute("disabled", "disabled");
        btnGenerar.classList.add("hover:cursor-pointer");
        btnGenerar.classList.remove("hover:cursor-not-allowed");
    });
}

async function handleMetadataAutocompleteInput(el) {
    const autocompletePanelMetadata = document.getElementById("autocompletePanelMetadata");
    const emailSettingsWrapper = document.getElementById("emailSettingsWrapper");

    const { value, selectionEnd = 0 } = el;
    const { word } = getActiveTokenOld(value, selectionEnd);
    const shouldOpenAutocomplete = /@[A-Za-z0-9_ ]{1,15}$/.test(word);

    autocompletePanelMetadata.innerHTML = "";

    if (shouldOpenAutocomplete) {
        const results = await getMetadataIndexResults(word.trim());

        if (results.length > 0) {
            let items = `
                <div class="flex flex-col p-3 absolute" style="right: 0;" onclick="closeAutocomplete('${autocompletePanelMetadata.id}')">
                    <div class="font-semibold text-xl text-red-500 cursor-pointer"><i class="fas fa-times"></i></div>
                </div>
            `;

            results.forEach(item => {
                items += `
                    <div class="flex flex-col py-5 px-8 cursor-pointer hover:bg-blue-100" onclick="handleMetadataAutocompleteSelection('${el.id}', '${item.dynamicField}')">
                        <div class="font-semibold text-xl text-blue-500">${item.dynamicField}</div>
                        <div class="text-xl text-gray-400">${item.description}</div>
                    </div>
                `;
            });

            const elClientRect = el.getBoundingClientRect();
            const emailSettingsRect = emailSettingsWrapper.getBoundingClientRect();

            autocompletePanelMetadata.style.top = `${elClientRect.top - emailSettingsRect.top + elClientRect.height}px`;
            autocompletePanelMetadata.innerHTML = items;
            autocompletePanelMetadata.classList.add("flex");
            autocompletePanelMetadata.classList.remove("hidden");
        }
        else {
            autocompletePanelMetadata.classList.add("hidden");
            autocompletePanelMetadata.classList.remove("flex");
        }
    }
}

async function handleAutocompleteInput(el) {
    const autocompletePanel = document.getElementById("autocompletePanel");
    const offset = el.dataset.offset;

    const { value, selectionEnd = 0 } = el;
    const { word } = getActiveToken(value, selectionEnd);
    const shouldOpenAutocomplete = /^[^@]*(?:\w)+[^;]/.test(word); // old /(?:\w)+$/
    const shouldOpenSmartTypeAutocomplete = /^[ ]?@[A-Za-z0-9_ ]{1,15}$/.test(word);

    autocompletePanel.innerHTML = "";

    if (shouldOpenAutocomplete || shouldOpenSmartTypeAutocomplete) {
        let results;

        if (shouldOpenAutocomplete) {
            results = await getClientEmailIndexResults(word.trim());

            if (results.length > 0) {
                let items = `
                    <div class="flex flex-col p-3 absolute" style="right: 0;" onclick="closeAutocomplete('${autocompletePanel.id}')">
                        <div class="font-semibold text-xl text-red-500 cursor-pointer"><i class="fas fa-times"></i></div>
                    </div>
                `;

                results.forEach(item => {
                        items += `
                        <div class="flex flex-col py-5 px-8 cursor-pointer hover:bg-blue-100" onclick="handleAutocompleteSelection('${el.id}', '${item.clientEmail}')">
                            <div class="font-semibold text-xl text-blue-500">${item.clientName}</div>
                            <div class="text-xl text-gray-400">${item.clientEmail}</div>
                        </div>
                    `;
                });

                autocompletePanel.style.top = `${offset}px`;
                autocompletePanel.innerHTML = items;
                autocompletePanel.classList.add("flex");
                autocompletePanel.classList.remove("hidden");
            }
        }
        if (shouldOpenSmartTypeAutocomplete) {
            results = await getSmartTypeIndexResults(word.trim());

            if (results.length > 0) {
                let items = `
                    <div class="flex flex-col p-3 absolute" style="right: 0;" onclick="closeAutocomplete('${autocompletePanel.id}')">
                        <div class="font-semibold text-xl text-red-500 cursor-pointer"><i class="fas fa-times"></i></div>
                    </div>
                `;

                results.forEach(item => {
                    items += `
                        <div class="flex flex-col py-5 px-8 cursor-pointer hover:bg-blue-100" onclick="handleAutocompleteSelection('${el.id}', '${item.dynamicType}')">
                            <div class="font-semibold text-xl text-blue-500">${item.dynamicType}</div>
                            <div class="text-xl text-gray-400">${item.description}</div>
                        </div>
                    `;
                });

                autocompletePanel.style.top = `${offset}px`;
                autocompletePanel.innerHTML = items;
                autocompletePanel.classList.add("flex");
                autocompletePanel.classList.remove("hidden");
            }
        }
    }
    else {
        autocompletePanel.classList.add("hidden");
        autocompletePanel.classList.remove("flex");
    }
}

function getActiveToken(input, cursorPosition) {
    // recuperamos la posición actual del cursor
    if (cursorPosition === undefined) return undefined;
    // creamos un array temporal para guardar las palabras
    const words = [];
    // recorremos el texto y lo separamos por espacios y saltos de línea
    input.split(/[;\n]/).forEach((word, index) => {
        // recuperamos la palabra anterior
        const previous = words[index - 1];
        // calculamos el rango de la palabra
        // recuperamos el índice inicial de la palabra
        const start = index === 0 ? index : previous.range[1] + 1;
        // recuperamos donde termina la palabra
        const end = start + word.length;
        // guardamos la palabra y su rango en el texto
        words.push({ word, range: [start, end] });
    });

    // buscamos en qué palabra estamos dependiendo de la posición del cursor
    return words.find(
        ({ range }) => range[0] <= cursorPosition && range[1] >= cursorPosition
    );
}

function getActiveTokenOld(input, cursorPosition) {
    // recuperamos la posición actual del cursor
    if (cursorPosition === undefined) return undefined;
    // creamos un array temporal para guardar las palabras
    const words = [];
    // recorremos el texto y lo separamos por espacios y saltos de línea
    input.split(/[\s\n]/).forEach((word, index) => {
        // recuperamos la palabra anterior
        const previous = words[index - 1];
        // calculamos el rango de la palabra
        // recuperamos el índice inicial de la palabra
        const start = index === 0 ? index : previous.range[1] + 1;
        // recuperamos donde termina la palabra
        const end = start + word.length;
        // guardamos la palabra y su rango en el texto
        words.push({ word, range: [start, end] });
    });

    // buscamos en qué palabra estamos dependiendo de la posición del cursor
    return words.find(
        ({ range }) => range[0] <= cursorPosition && range[1] >= cursorPosition
    );
}

async function getClientEmailIndexResults(query) {
    const { hits } = await clientEmailIndex.search(query);
    return hits;
}

async function getMetadataIndexResults(query) {
    const { hits } = await metadataIndex.search(query);
    return hits;
}

async function getSmartTypeIndexResults(query) {
    const { hits } = await smartTypeIndex.search(query);
    return hits;
}

function handleMetadataAutocompleteSelection(inpId, metadata) {
    const autocompletePanelMetadata = document.getElementById("autocompletePanelMetadata");
    const inp = document.getElementById(inpId);
    const evt = document.createEvent("HTMLEvents");
    const { value, selectionEnd = 0 } = inp;
    const { word, range } = getActiveTokenOld(value, selectionEnd);
    const [index] = range;

    const prefix = value.substring(0, index);
    const suffix = value.substring(index + word.length);
    const newText = prefix + `[${metadata}]` + suffix;

    evt.initEvent("input", false, true);
    inp.value = newText;
    inp.dispatchEvent(evt);
    inp.focus();
    autocompletePanelMetadata.classList.add("hidden");
    autocompletePanelMetadata.classList.remove("flex");
}

function handleAutocompleteSelection(inpId, email) {
    const autocompletePanel = document.getElementById("autocompletePanel");
    const inp = document.getElementById(inpId);
    const evt = document.createEvent("HTMLEvents");
    const { value, selectionEnd = 0 } = inp;
    const { word, range } = getActiveToken(value, selectionEnd);
    const [index] = range;

    const prefix = value.substring(0, index);
    const suffix = value.substring(index + word.length);
    const newText = prefix + email + "; " + suffix;

    evt.initEvent("input", false, true);
    inp.value = newText;
    inp.dispatchEvent(evt);
    inp.focus();
    autocompletePanel.classList.add("hidden");
    autocompletePanel.classList.remove("flex");
}

function closeAutocomplete(autocompleteId) {
    const autocompletePanel = document.getElementById(autocompleteId);

    autocompletePanel.classList.add("hidden");
    autocompletePanel.classList.remove("flex");
}

async function registerEmailParameter(target, id, inputType, parameter) {
    const templateHtml = document.getElementById("templateHtml");
    const el = templateHtml.querySelector(`#${parameter}`);
    const hdnImagesUrl = document.getElementById("ctl00_MainContent_hdnImagesUrl");

    switch (inputType) {
        case "String":
        case "LongString":
            emailSetting.CreateOrUpdateTemplateParam(id, target.value);

            const custom = el.dataset.custom || false;
            if (custom) {
                const prefix = el.dataset.prefix || "";
                const attr = el.dataset.attr;

                el.setAttribute(attr, `${prefix}${target.value}`);
            }
            else {
                el.innerText = target.value;
            }

            handleMetadataAutocompleteInput(target);
            break;
        case "Image":
            const formData = new FormData();
            const file = target.files[0];
            const lastDot = file.name.lastIndexOf('.');
            const fileName = GetUniqueId();
            const ext = file.name.substring(lastDot + 1);

            formData.append(`${fileName}.${ext}`, file);

            const resp = await fetch("TemplateImagesHandler.ashx", {
                method: "POST",
                body: formData
            });

            if (resp.ok) {
                emailSetting.CreateOrUpdateTemplateParam(id, `cid:${fileName}.${ext}`);

                const fr = new FileReader();
                fr.onload = function () {
                    el.src = fr.result;
                }
                fr.readAsDataURL(file);
            }
            break;
        default:
    }
}

function removeAttachment(attachment) {
    const templateHtml = document.getElementById("templateHtml");
    const elAttachment = templateHtml.querySelector(`#${attachment}`);

    emailSetting.Attachments = emailSetting.Attachments.filter(at => at != attachment.replace("-", "."));
    elAttachment.remove();
}

async function HandleDynamicSltSection() {
    const emailSettingsWrapper = document.getElementById("emailSettingsWrapper");
    const dynamicSelect = emailSettingsWrapper.querySelector("#input-image-type");
    const typeId = parseInt(dynamicSelect.value);
    const inputType = emailSetting.GetTemplateParamInputTypeById(typeId);

    AddDynamicSeccion(typeId, inputType.Tipo);
}

function HandleImageTypeChange() {
    const emailSettingsWrapper = document.getElementById("emailSettingsWrapper");
    const dynamicSelect = emailSettingsWrapper.querySelector("#input-image-type");
    const inputImageExtraData = emailSettingsWrapper.querySelector("#input-image-extra-data");
    const currentType = dynamicSelect.dataset.currentType;
    const dynamicInput = emailSettingsWrapper.querySelector(`#input-${currentType}`);
    const typeId = parseInt(dynamicSelect.value);
    const inputType = emailSetting.GetTemplateParamInputTypeById(typeId);

    dynamicInput.setAttribute("id", `input-${inputType.Tipo}`);
    dynamicSelect.dataset.currentType = inputType.Tipo;
    if (inputType.Tipo == "ImageLink") { inputImageExtraData.setAttribute("placeholder", "Ej. https://www.prestamofeliz.pe"); }
    if (inputType.Tipo == "ImageWhatsApp") { inputImageExtraData.setAttribute("placeholder", "Ej. 974 680370"); }
    if (inputType.Tipo == "ImageEmail") { inputImageExtraData.setAttribute("placeholder", "Ej. atencionaclientes@prestamofeliz.pe"); }

    if (inputType.Tipo == "Image") { inputImageExtraData.classList.add("hidden"); }
    else { inputImageExtraData.classList.remove("hidden"); }
}

async function AddDynamicSeccion(typeId, type) {
    const templateHtml = document.getElementById("templateHtml");
    const emailSettingsWrapper = document.getElementById("emailSettingsWrapper");
    const dynamicInput = emailSettingsWrapper.querySelector(`#input-${type}`);

    if (dynamicInput.value) {
        const inputType = emailSetting.GetTemplateParamInputTypeById(typeId);
        const el = templateHtml.querySelector("#Contenido");
        const hdnImagesUrl = document.getElementById("ctl00_MainContent_hdnImagesUrl");
        const parser = new DOMParser();
        let fragment;
        let previewFragment;
        let fragmentParsed;
        let wrapperFragment;
        let finalFragment;

        const removeNode = document.createElement("i");
        const removeNodeId = GetUniqueId();
        removeNode.classList.add("fas", "fa-times", "hover:cursor-pointer", "remove-section");
        removeNode.setAttribute("id", removeNodeId);
        removeNode.setAttribute("onclick", `removeEmailSection('${removeNodeId}')`);

        if (el.innerText.includes("{{{Contenido}}}")) { el.innerHTML = ""; }

        switch (type) {
            case "String":
            case "LongString":
                fragment = inputType.HtmlFragment.replace(/(?:\{){2,3}Texto(?:\}){2,3}/, dynamicInput.value.replace(/\n\r?/g, '<br />'));
                fragmentParsed = parser.parseFromString(fragment, "text/html");
                finalFragment = fragmentParsed.querySelector(`#${type}-wrapper`).parentElement;
                wrapperFragment = finalFragment.querySelector(`#${type}-wrapper`);
                wrapperFragment.insertBefore(removeNode, wrapperFragment.children[0]);

                el.innerHTML += finalFragment.innerHTML;

                emailSetting.CreateOrUpdateTemplateParam(removeNodeId, fragment);
                break;
            case "Image":
            case "ImageLink":
            case "ImageWhatsApp":
            case "ImageEmail":
                const formData = new FormData();
                const file = dynamicInput.files[0];
                const lastDot = file.name.lastIndexOf('.');
                const fileName = GetUniqueId();
                const ext = file.name.substring(lastDot + 1);

                formData.append(`${fileName}.${ext}`, file);

                const resp = await fetch("TemplateImagesHandler.ashx", {
                    method: "POST",
                    body: formData
                });

                if (resp.ok) {
                    const fr = new FileReader();
                    fr.onload = function () {
                        fragment = inputType.HtmlFragment.replace(/(?:\{){2,3}Image(?:\}){2,3}/, `cid:${fileName}.${ext}`);
                        previewFragment = inputType.HtmlFragment.replace(/(?:\{){2,3}Image(?:\}){2,3}/, fr.result);

                        if (type == "ImageLink") {
                            const imageUrl = emailSettingsWrapper.querySelector("#input-image-extra-data");

                            fragment = fragment.replace(/(?:\{){2,3}Link(?:\}){2,3}/, imageUrl.value);
                            previewFragment = previewFragment.replace(/(?:\{){2,3}Link(?:\}){2,3}/, imageUrl.value);
                            imageUrl.value = "";
                        }

                        if (type == "ImageWhatsApp") {
                            const imagePhoneNumber = emailSettingsWrapper.querySelector("#input-image-extra-data");

                            fragment = fragment.replace(/(?:\{){2,3}PhoneNumber(?:\}){2,3}/, imagePhoneNumber.value);
                            previewFragment = previewFragment.replace(/(?:\{){2,3}PhoneNumber(?:\}){2,3}/, imagePhoneNumber.value);
                            imagePhoneNumber.value = "";
                        }

                        if (type == "ImageEmail") {
                            const imageEmail = emailSettingsWrapper.querySelector("#input-image-extra-data");

                            fragment = fragment.replace(/(?:\{){2,3}Email(?:\}){2,3}/, imageEmail.value);
                            previewFragment = previewFragment.replace(/(?:\{){2,3}Email(?:\}){2,3}/, imageEmail.value);
                            imageEmail.value = "";
                        }

                        fragmentParsed = parser.parseFromString(previewFragment, "text/html");
                        finalFragment = fragmentParsed.querySelector(`#${type}-wrapper`).parentElement;
                        wrapperFragment = finalFragment.querySelector(`#${type}-wrapper`);
                        wrapperFragment.insertBefore(removeNode, wrapperFragment.children[0]);

                        el.innerHTML += finalFragment.innerHTML;
                        emailSetting.CreateOrUpdateTemplateParam(removeNodeId, fragment);
                    }
                    fr.readAsDataURL(file);
                }
                break;
            default:
        }

        dynamicInput.value = "";
    }
}

function HandleUploadAddresses(uploadType) {
    const uploadAddressesCsv = document.getElementById("uploadAddressesCsv");

    uploadAddressesCsv.dataset.uploadType = uploadType;
    uploadAddressesCsv.click();
}

function GetUniqueId () {
    return Date.now().toString(36) + Math.random().toString(36).substr(2);
}

function removeEmailSection(removeId) {
    const templateHtml = document.getElementById("templateHtml");
    const elToRemove = templateHtml.querySelector(`#${removeId}`);
    elToRemove.parentElement.remove();
    emailSetting.DeleteTemplateParam(removeId);
}

function CheckRepetitiveWrapperPermissions() {
    const repetitiveWrapper = document.getElementById("repetitiveWrapper");
    const hdnIdUsuario = document.getElementById("ctl00_MainContent_hdnIdUsuario");

    if (parseInt(hdnIdUsuario.value) != 21) {
        repetitiveWrapper.remove();
    }
}