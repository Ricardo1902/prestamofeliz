﻿<%@ WebHandler Language="C#" Class="TemplateAttachmentHandler" %>

using System;
using System.Web;
using System.IO;

public class TemplateAttachmentHandler : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        if (context.Request.Files.Count > 0)
        {
            string attachmentsPath = System.Configuration.ConfigurationManager.AppSettings["AttachmentsTemplate"];
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string name = files.AllKeys[0];
                file.SaveAs(Path.Combine(attachmentsPath, name));
            }
            context.Response.ContentType = "text/plain";
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}