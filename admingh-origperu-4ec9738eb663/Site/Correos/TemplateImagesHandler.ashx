﻿<%@ WebHandler Language="C#" Class="TemplateImagesHandler" %>

using System;
using System.Web;

public class TemplateImagesHandler : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string name = files.AllKeys[0];
                //string fname = context.Server.MapPath("~/Imagenes/EmailTemplate/" + name); 
                file.SaveAs("C:/tmp/OrigPeru/ImagesEmailTemplate/" + name);
            }
            context.Response.ContentType = "text/plain";
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}