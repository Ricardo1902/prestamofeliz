﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Visanet.Model.Request;
using Visanet.Model.Response;
using OfficeOpenXml;
using System.Text.RegularExpressions;
using Kushki.Model.Response;
using System.Collections.Generic;
using System.Data;
using System.ComponentModel;
using System.Configuration;

public partial class Site_Creditos_PagosKushki : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {        
    }

    private static string fomatoMoneda(decimal? valor)
    {
        decimal valorD = 0M;
        if (valor != null) valorD = valor.Value;
        return valorD.ToString("C", CultureInfo.CurrentCulture);
    }

    #region Metodos Privados
    public static DateTime? ValidarFecha(string fecha)
    {
        if (!string.IsNullOrEmpty(fecha))
        {
            //string[] formats = { "dd/MM/yyyy" };
            DateTime FechaFinal;
            if (DateTime.TryParseExact(fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaFinal))
                return FechaFinal;
            else
                return null;
        }
        else
            return null;
    }

    public static string VerificarIdSuscripcionKushki(string tarjeta)
    {
        string resultado = "";

        if (tarjeta != null)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago = ws.RecuperarIdSuscripcionKushki(tarjeta);
                    resultado = respPago;
                }
            }
            catch (Exception ex)
            {

            }
        }
        return resultado;
    }
    #endregion

    #region WebMethods


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerPagoMasivoKushki(ObtenerPagoMasivoKushkiRequest peticion)
    {
        DateTime? DatoFechaDesde = null;
        DateTime? DatoFechaHasta = null;
        DatoFechaDesde = ValidarFecha(peticion.FechaDesde);
        DatoFechaHasta = ValidarFecha(peticion.FechaHasta);

        object resultado = null;
        if (!DatoFechaDesde.HasValue || !DatoFechaHasta.HasValue || DatoFechaDesde.Value > DatoFechaHasta.Value)
            return resultado;

        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                var respPagos = ws.PagosMasivosKushki(new wsSOPF.PagosMasivosKushkiRequest()
                {
                    FechaDesde = DatoFechaDesde.Value,
                    FechaHasta = DatoFechaHasta.Value
                });
                if (respPagos != null)
                {
                    return (from p in respPagos.Resultado.PagosMasivosKushki
                             select new
                             {
                                 p.IdPagoMasivoKushki,
                                 p.NombreArchivo,
                                 FechaRegistro = p.FechaRegistro.ToString("dd-MM-yyyy HH:mm"),
                                 TotalRegistros = p.TotalRegistros == null ? 0 : p.TotalRegistros.Value,
                                 TotalProcesado = p.TotalProcesado == null ? 0 : p.TotalProcesado.Value,
                                 TotalAprobados = p.TotalAprobados == null ? 0 : p.TotalAprobados.Value,
                                 TotalNoProcesado = p.TotalNoProcesado == null ? 0 : p.TotalNoProcesado.Value,
                                 MontoTotal = fomatoMoneda(p.MontoTotal),
                                 MontoCobrado = fomatoMoneda(p.MontoCobrado),
                                 PorcentajeCobrado = p.PorcentajeCobrado,
                                 Estatus = p.EstatusProceso,
                                 UltimaActualizacion = p.UltimaActualizacion == null ? "" : p.UltimaActualizacion.Value.ToString("dd-MM-yy HH:mm"),
                                 p.Mensaje
                             })
                            .OrderByDescending(p => p.FechaRegistro)
                            .ToList();
                }
            }
        }
        catch (Exception er)
        {
        }
        return resultado;
    }
    

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object Validar(string[][] row, int numDatos,string nombreDoc)
    {
        PagoResponseKushki respuesta = new PagoResponseKushki();
        List<PagoKushkiRequest> LisPago = new List<PagoKushkiRequest>();
        PagoKushkiRequest pago = new PagoKushkiRequest();


        int linea = 1;
            int idUsuario = 0;
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }
            for (int i = 0; i < numDatos; i++)
            {
                pago = new PagoKushkiRequest();
                pago.IdProducto = row[i][0];
                pago.Monto = Convert.ToDouble(row[i][1]);
                pago.NumeroTarjeta = row[i][2];
                pago.NombreTarjetahabiente = row[i][3];
                pago.Telefono = row[i][4];
                pago.IdUsuario = idUsuario;
                LisPago.Add(pago);
            }
            if (numDatos == 0)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "WVD01";
                respuesta.MensajeOperacion = "Por favor ingrese información del pago a realizar.";
                return respuesta;
            }
            foreach (var peticion in LisPago)
            {
                
                if (peticion.IdUsuario <= 0)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD02";
                    respuesta.MensajeOperacion = "El usuario no es válido para registrar el pago.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }

                if (string.IsNullOrEmpty(peticion.IdProducto) || peticion.IdProducto.Length >20)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD03";
                    respuesta.MensajeOperacion = "Favor de proporcionar el codigo del producto";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }

                if (peticion.Monto <= 0)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD04";
                    respuesta.MensajeOperacion = "El valor del monto no es válido, favor de capturar un monto mayor a cero.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }

                if (string.IsNullOrWhiteSpace(peticion.NumeroTarjeta) || peticion.NumeroTarjeta.Length != 16)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD05";
                    respuesta.MensajeOperacion = "Favor de ingresar un número de tarjeta a 16 dígitos";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                else
                {
                    var idsus = VerificarIdSuscripcionKushki(peticion.NumeroTarjeta);
                    if (idsus == "")
                    {
                        respuesta.Error = true;
                        respuesta.CodigoError = "WVD15";
                        respuesta.MensajeOperacion = "La tarjeta no se encuentra suscrita";
                        respuesta.Linea = Convert.ToString(linea);
                        return respuesta;
                    }
                }
                if (string.IsNullOrEmpty(peticion.NombreTarjetahabiente) || peticion.NombreTarjetahabiente.Length > 80)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD08";
                    respuesta.MensajeOperacion = "Favor ingresar el Tarjeta Habiente.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                if (string.IsNullOrWhiteSpace(peticion.Telefono) || peticion.Telefono.Substring(0, 1) != "+" || peticion.Telefono.Length !=12)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD14";
                    respuesta.MensajeOperacion = "Telefono no es válido.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }

            linea++;
            }

            if (numDatos>0)
            {
                try
                {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                        var respPago = ws.GenerarPagoKushki(new wsSOPF.GenerarPagoKushkiRequest
                        {
                            NumeroDatos= numDatos,
                            NombreDocumento=nombreDoc,
                            pagos= row,
                            IdUsuario=idUsuario
                        });
                        if (respPago != null)
                        {
                            respuesta.Error = respPago.Error;
                            respuesta.CodigoError = respPago.CodigoError;
                            respuesta.MensajeOperacion = respPago.MensajeOperacion;
                            respuesta.Resultado.IdPeticion = respPago.Resultado.IdPeticion;
                          
                        }
                        else
                        {
                            respuesta.Error = true;
                            respuesta.MensajeOperacion = "Ocurrió un error al momento de solicitar la autorización de pago.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = ex.Message;
                }
            }
       
        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object PagarKushki( string subscriptionId, PagoKushkiRequest pago)
    {
        object resultado = "";
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }

        if (pago != null)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago = ws.GenerarPagoMasivoKushki(new wsSOPF.GeneraPagoMavisokushkiRequest
                    {
                        IdUsuario = idUsuario,
                        IdProducto = pago.IdProducto,
                        Monto = pago.Monto,
                        TipoMoneda = pago.TipoMoneda,
                        Moneda=pago.Moneda,
                        NumeroTarjeta = pago.NumeroTarjeta,
                        NombreTarjetahabiente=pago.NombreTarjetahabiente,
                        Telefono=pago.Telefono,
                        fecha = pago.fecha,
                        subscriptionId = subscriptionId,
                        IdPagoMasivo=pago.IdPagoMasivo
                    });
                    resultado = respPago;
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object TerminoProceso(int IdPagoMasivo)
    {
        object resultado = "";        

        if (IdPagoMasivo != 0)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago = ws.TerminoProcesoPago(IdPagoMasivo);
                    resultado = respPago;
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object DescargarExcelFallidos(string[][] row, int numDatos, string nombreDoc)
    {
        object resultado = null;
        List<PagoKushkiRequest> LisPago = new List<PagoKushkiRequest>();
        PagoKushkiRequest pago = new PagoKushkiRequest();
        List<FallidosPagoKushkiRequest> LisErr = new List<FallidosPagoKushkiRequest>();
        FallidosPagoKushkiRequest err = new FallidosPagoKushkiRequest();
        int linea = 1;
        for (int i = 0; i < numDatos; i++)
        {
            pago = new PagoKushkiRequest();
            pago.IdProducto = row[i][0];
            pago.Monto = Convert.ToDouble(row[i][1]);
            pago.NumeroTarjeta = row[i][2];
            pago.NombreTarjetahabiente = row[i][3];
            pago.Telefono = row[i][4];
            LisPago.Add(pago);
        }
        foreach (var peticion in LisPago)
        {
            int error = 0;
            string Mensaje = "";
            if (string.IsNullOrEmpty(peticion.IdProducto) || peticion.IdProducto.Length >20)
            {
                error = 1;
                Mensaje = "Revise Solicitud";
            }

            if (peticion.Monto <= 0)
            {
                error = 1;
                if (Mensaje == "")
                {
                   Mensaje = "Revise Monto";
                }
                else
                {
                    Mensaje = Mensaje + ", "+ "Monto";
                }
              
            }

            if (string.IsNullOrWhiteSpace(peticion.NumeroTarjeta) || peticion.NumeroTarjeta.Length != 16)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NoTarjeta";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NoTarjeta";
                }

            }
            else
            {
                var idsus = VerificarIdSuscripcionKushki(peticion.NumeroTarjeta);
                if (idsus == "")
                {
                    error = 1;
                    if (Mensaje == "")
                    {
                        Mensaje = "Revise la tarjeta no esta suscrita";
                    }
                    else
                    {
                        Mensaje = Mensaje + ", " + "la tarjeta no esta suscrita";
                    }
                }
            }
            if (string.IsNullOrEmpty(peticion.NombreTarjetahabiente) || peticion.NombreTarjetahabiente.Length > 80)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NombreTarjetahabiente";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NombreTarjetahabiente";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Telefono) || peticion.Telefono.Substring(0, 1) != "+" || peticion.Telefono.Length !=12)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Telefono";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Telefono";
                }
            }

            if (error == 1)
            {
                err = new FallidosPagoKushkiRequest();
                err.Linea = linea;
                err.Solicitud = peticion.IdProducto;
                err.Monto = peticion.Monto;
                err.NoTarjeta = peticion.NumeroTarjeta;
                err.NombreTarjetahabiente = peticion.NombreTarjetahabiente;
                err.Telefono = peticion.Telefono;
                err.Mensaje = Mensaje;
                LisErr.Add(err);

            }
            linea++;
        }

        if (LisErr != null)
        {
           
                string NombreArchivoNuevo = "ErroresPago_" + nombreDoc ;
                string contenido = string.Empty;
            DataSet ds = new DataSet();
            ds.Tables.Add(ConvertToDatatable.ConvertToDatatab(LisErr));
           
            using (ExcelPackage pck = new ExcelPackage())
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Resultado");
                    ws.Cells["A1"].LoadFromDataTable(ds.Tables[0], true);
                    var ms = new System.IO.MemoryStream();
                    pck.SaveAs(ms);
                    byte[] bytes = ms.ToArray();
                    contenido = Convert.ToBase64String(bytes);
             }
                resultado = new
                {
                    Error = false,
                    Mensaje = "El reporte se ha generado exitosamente.",
                    Tipo = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    Longitud = contenido.Length.ToString(),
                    NombreArchivoNuevo,
                    Contenido = contenido
                };
     
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ProcesarSoloCorrectos(string[][] row, int numDatos, string nombreDoc, string[][] row2)
    {
        PagoResponseKushki respuesta = new PagoResponseKushki();
        List<PagoKushkiRequest> LisPago = new List<PagoKushkiRequest>();
        PagoKushkiRequest pago = new PagoKushkiRequest();
        List<PagoKushkiRequest> LisErr = new List<PagoKushkiRequest>();
        PagoKushkiRequest err = new PagoKushkiRequest();
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }
        for (int i = 0; i < numDatos; i++)
        {
            pago = new PagoKushkiRequest();
            pago.IdProducto = row[i][0];
            pago.Monto = Convert.ToDouble(row[i][1]);
            pago.NumeroTarjeta = row[i][2];
            pago.NombreTarjetahabiente = row[i][3];
            pago.Telefono = row[i][4];
            pago.IdUsuario = idUsuario;
            LisPago.Add(pago);
        }

        int numDatos1 = 0;
        int numDatos2 = 0;
        foreach (var peticion in LisPago)
        {
            int error = 0;
            if (peticion.IdUsuario <= 0)
            {
                error = 1;
            }
            if (string.IsNullOrEmpty(peticion.IdProducto) || peticion.IdProducto.Length > 20)
            {
                error = 1;
            }

            if (peticion.Monto <= 0)
            {
                error = 1;

            }

            if (string.IsNullOrWhiteSpace(peticion.NumeroTarjeta) || peticion.NumeroTarjeta.Length != 16)
            {
                error = 1;

            }
            else
            {
                var idsus = VerificarIdSuscripcionKushki(peticion.NumeroTarjeta);
                if (idsus == "")
                {
                    error = 1;
                }
            }

            if (string.IsNullOrEmpty(peticion.NombreTarjetahabiente) || peticion.NombreTarjetahabiente.Length > 80)
            {
                error = 1;
            }
            if (string.IsNullOrWhiteSpace(peticion.Telefono) || peticion.Telefono.Substring(0, 1) != "+" || peticion.Telefono.Length !=12)
            {
                error = 1;
            }


            if (error == 0)
            {
                row[numDatos1][0]= peticion.IdProducto;
                row[numDatos1][1] = Convert.ToString(peticion.Monto);
                 row[numDatos1][2]= peticion.NumeroTarjeta ;
                row[numDatos1][3] = peticion.NombreTarjetahabiente;
                row[numDatos1][4] = peticion.Telefono;
                numDatos1 ++;

            }
        }

        foreach (var peticion in LisPago)
        {
            int error = 0;
            string Mensaje = "";
            if (string.IsNullOrEmpty(peticion.IdProducto) || peticion.IdProducto.Length >20)
            {
                error = 1;
                Mensaje = "Revise Solicitud";
            }

            if (peticion.Monto <= 0)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Monto";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Monto";
                }

            }

            if (string.IsNullOrWhiteSpace(peticion.NumeroTarjeta) || peticion.NumeroTarjeta.Length != 16)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NoTarjeta";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NoTarjeta";
                }

            }
            else
            {
                var idsus = VerificarIdSuscripcionKushki(peticion.NumeroTarjeta);
                if (idsus == "")
                {
                    error = 1;
                    if (Mensaje == "")
                    {
                        Mensaje = "Revise la tarjeta no esta suscrita";
                    }
                    else
                    {
                        Mensaje = Mensaje + ", " + "la tarjeta no esta suscrita";
                    }
                }
            }

            if (string.IsNullOrEmpty(peticion.NombreTarjetahabiente) || peticion.NombreTarjetahabiente.Length > 80)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NombreTarjetahabiente";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NombreTarjetahabiente";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Telefono) || peticion.Telefono.Substring(0, 1) != "+" || peticion.Telefono.Length !=12)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Telefono";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Telefono";
                }
            }


            if (error == 1)
            {
                row2[numDatos2][0] = peticion.IdProducto.Length > 20 ? peticion.IdProducto.Substring(0, 20) : peticion.IdProducto;
                row2[numDatos2][1] = Convert.ToString(peticion.Monto);
                row2[numDatos2][2] = peticion.NumeroTarjeta.Length > 16 ? peticion.NumeroTarjeta.Substring(0, 16) : peticion.NumeroTarjeta;
                row2[numDatos2][3] = peticion.NombreTarjetahabiente.Length > 80 ? peticion.NombreTarjetahabiente.Substring(0, 80) : peticion.NombreTarjetahabiente;
                row2[numDatos2][4] = peticion.Telefono.Length > 12 ? peticion.Telefono.Substring(0, 12) : peticion.Telefono;
                numDatos2 ++;

            }
        }
        if (numDatos > 0)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago = ws.GenerarPagoConErrKushki(new wsSOPF.GenerarPagoKushkiRequest
                    {
                        NumeroDatos = numDatos,
                        NumeroDatosSE = numDatos1,
                        NumeroDatosCE = numDatos2,
                        NombreDocumento = nombreDoc,
                        pagos = row,
                        pagosErr = row2,
                        IdUsuario = idUsuario
                    });
                    if (respPago != null)
                    {
                        respuesta.Error = respPago.Error;
                        respuesta.CodigoError = respPago.CodigoError;
                        respuesta.MensajeOperacion = respPago.MensajeOperacion;
                        respuesta.Resultado.IdPeticion = respPago.Resultado.IdPeticion;
                        respuesta.Resultado.rowdev = row;
                        respuesta.Resultado.ndatos = numDatos1;
                    }
                    else
                    {
                        respuesta.Error = true;
                        respuesta.MensajeOperacion = "Ocurrió un error al momento de solicitar la autorización de pago.";
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = ex.Message;
            }
        }
        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object MostrarFallidos(string[][] row, int numDatos, string nombreDoc)
    {
        object resultado = null;
        List<PagoKushkiRequest> LisPago = new List<PagoKushkiRequest>();
        PagoKushkiRequest pago = new PagoKushkiRequest();
        List<FallidosPagoKushkiRequest> LisErr = new List<FallidosPagoKushkiRequest>();
        FallidosPagoKushkiRequest err = new FallidosPagoKushkiRequest();
        int linea = 1;
        for (int i = 0; i < numDatos; i++)
        {
            pago = new PagoKushkiRequest();
            pago.IdProducto = row[i][0];
            pago.Monto = Convert.ToDouble(row[i][1]);
            pago.NumeroTarjeta = row[i][2];
            pago.NombreTarjetahabiente = row[i][3];
            pago.Telefono = row[i][4];
            LisPago.Add(pago);
        }
        foreach (var peticion in LisPago)
        {
            int error = 0;
            string Mensaje = "";
            if (string.IsNullOrEmpty(peticion.IdProducto) || peticion.IdProducto.Length >20)
            {
                error = 1;
                Mensaje = "Revise Solicitud";
            }

            if (peticion.Monto <= 0)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Monto";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Monto";
                }

            }

            if (string.IsNullOrWhiteSpace(peticion.NumeroTarjeta) || peticion.NumeroTarjeta.Length != 16)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NoTarjeta";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NoTarjeta";
                }

            }
            else
            {
                var idsus = VerificarIdSuscripcionKushki(peticion.NumeroTarjeta);
                if (idsus == "")
                {
                    error = 1;
                    if (Mensaje == "")
                    {
                        Mensaje = "Revise la tarjeta no esta suscrita";
                    }
                    else
                    {
                        Mensaje = Mensaje + ", " + "la tarjeta no esta suscrita";
                    }
                }
            }

            if (string.IsNullOrEmpty(peticion.NombreTarjetahabiente) || peticion.NombreTarjetahabiente.Length > 80)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NombreTarjetahabiente";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NombreTarjetahabiente";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Telefono) || peticion.Telefono.Substring(0, 1) != "+" || peticion.Telefono.Length !=12)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Telefono";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Telefono";
                }
            }
            if (error == 1)
            {
                err = new FallidosPagoKushkiRequest();
                err.Linea = linea;
                err.Solicitud = peticion.IdProducto;
                err.Monto = peticion.Monto;
                err.NoTarjeta = peticion.NumeroTarjeta;
                err.NombreTarjetahabiente = peticion.NombreTarjetahabiente;
                err.Telefono = peticion.Telefono;
                err.Mensaje = Mensaje;
                LisErr.Add(err);

            }
            linea++;
        }

        if (LisErr != null)
        {

            return LisErr;

        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object MostrarArchivos(string[][] row, int numDatos, string nombreDoc)
    {
        object resultado = null;
        List<PagoKushkiRequest> LisPago = new List<PagoKushkiRequest>();
        PagoKushkiRequest pago = new PagoKushkiRequest();

        for (int i = 0; i < numDatos; i++)
        {
            pago = new PagoKushkiRequest();
            pago.IdProducto = row[i][0];
            pago.Monto = Convert.ToDouble(row[i][1]);
            pago.NumeroTarjeta = row[i][2];
            pago.NombreTarjetahabiente = row[i][3];
            pago.Telefono = row[i][4];
            LisPago.Add(pago);
        }
        
        if (LisPago != null)
        {

            return LisPago;

        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object RecuperarIdSuscripcionKushki(string tarjeta)
    {
        object resultado = "";

        if (tarjeta != null)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago = ws.RecuperarIdSuscripcionKushki(tarjeta);
                    resultado = respPago;
                }
            }
            catch (Exception ex)
            {

            }
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GuardarPagoMasivoPagar(string Mensaje, PagoKushkiRequest pago)
    {
        object resultado = "";
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }

        if (pago != null)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago = ws.GuardarPagoMasivoPagar(new wsSOPF.GeneraPagoMavisokushkiRequest
                    {
                        IdUsuario = idUsuario,
                        cvv = pago.cvv,
                        IdProducto = pago.IdProducto,
                        Monto = pago.Monto,
                        TipoMoneda = pago.TipoMoneda,
                        Moneda = pago.Moneda,
                        NumeroTarjeta = pago.NumeroTarjeta,
                        MesExpiracion = pago.MesExpiracion,
                        AnioExpiracion = pago.AnioExpiracion,
                        NombreTarjetahabiente = pago.NombreTarjetahabiente,
                        fecha = pago.fecha,
                        Telefono = pago.Telefono,
                        Apellido = pago.Apellido,
                        email = pago.email,
                        TipoDocumento = pago.TipoDocumento,
                        Documento = pago.Documento,
                        IdPagoMasivo = pago.IdPagoMasivo,
                        mensaje = Mensaje
                    });
                    resultado = respPago;
                }
            }
            catch (Exception ex)
            {

            }
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object RecuperarIdComercioMoneda()
    {
        object resultado = null;
        IdComercioResponsekushki id = new IdComercioResponsekushki();
        id.IdComercio = ConfigurationManager.AppSettings.Get("MerchandId");
        id.Moneda = ConfigurationManager.AppSettings.Get("Moneda");
        if (id.IdComercio == null || id.Moneda == null)
        {
            return resultado;
        }
        else
        {
            resultado = id;
        }
        return resultado;
    }
    #endregion

}
