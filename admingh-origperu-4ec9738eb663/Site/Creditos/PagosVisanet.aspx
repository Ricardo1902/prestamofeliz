﻿<%@ Page Title="PagosVisanet" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="PagosVisanet.aspx.cs" Inherits="Site_Creditos_PagosVisanet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.css"/>
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../Content/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <style>
        /* Start by setting display:none to make this hidden.
        Then we position it in relation to the viewport window
        with position:fixed. Width, height, top and left speak
        for themselves. Background we set to 80% white with
        our animation centered, and no-repeating */
        .modal-pr {
            display: none;
            position: fixed;
            z-index: 9000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 ) url('../../Imagenes/ajax-loader.gif') 50% 50% no-repeat;
        }

        /* When the body has the loading class, we turn
        the scrollbar off with overflow:hidden */
        body.loading .modal-pr {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
        modal element will be visible */
        body.loading .modal-pr {
            display: block;
        }

        /*Quitar formato al archivo excel*/
        /*.GridAExcel {
            padding:0;
            margin:0;
            text-decoration:none;
        }

        .GridAExcel th a {
            text-decoration:none;
        }*/
    </style>
    <script src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script src="../../js/datatables/js/jquery.dataTables.min.js"></script>    
    <script src="../../js/datatables/js/dataTables.bootstrap.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>     
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
    <script src="../../js/creditos/PagosVisanet.js?v=<%=AppSettings.VersionPub %>"></script>
    <script type="text/javascript" src="../../Scripts/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../../Scripts/bootstrap-datetimepicker.js"></script>
        
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Pagos Masivos VisaNET</h3>
        </div>
        <div class="panel-body">
            <asp:Panel ID="pnlError" class="form-group col-sm-12" Visible="false" runat="server">                                
                <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </div>
            </asp:Panel>
            <div class="row">                
                <label class="small col-sm-12">Seleccione el archivo(.xls - .xlsx)</label> 
                <div class="form-group col-md-5">                                      
                    <asp:FileUpload runat="server" CssClass="form-control" ID="fupArchivo" accept=".xls,.xlsx" />
                </div>
                <div class="form-group col-md-7">
                    <asp:Button runat="server" ID="btnCargaArchivo" CssClass="btn btn-success" Text="Procesar Pagos" OnClick="btnCargaArchivo_Click" />                   
             
                    <a href="#" id="btnRecargar" class="btn btn-primary">
                        <i class="fas fa-retweet"></i> Actualizar
                    </a>
                </div>
            </div>
            <div class="col-sm-12">
                <hr />
            </div>
            <div class="row">               
                <div class="col-sm-6">
                    <label for="txFechaDesde">Desde</label>
                    <div class="input-group">
                        <span class="input-group-addon"><span
                            class="glyphicon glyphicon-calendar"></span></span>
                        <input type="text" id="txFechaDesde" class="form-control" />
                    </div>
                </div>
                <div class="col-sm-6">
                    <label for="txFechaHasta">Hasta</label>
                    <div class="input-group">
                        <span class="input-group-addon"><span
                            class="glyphicon glyphicon-calendar"></span></span>
                        <input type="text" id="txFechaHasta" class="form-control" />
                    </div>
                </div>
            </div>
            <br /><table id="tbArchivosMasivos" class="table display compact nowrap" style="width:100%"></table>
        </div>        
    </div>
    <div class="modal-pr"></div>
    <div id="divLoader" class="hidden">
        <div id="loader-background"></div>
        <div id="loader-content"></div>
    </div>
</asp:Content>
