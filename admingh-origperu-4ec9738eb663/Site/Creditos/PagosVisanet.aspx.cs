﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Visanet.Model.Request;
using Visanet.Model.Response;
using OfficeOpenXml;
using System.Text.RegularExpressions;

public partial class Site_Creditos_PagosVisanet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {        
    }

    protected void btnCargaArchivo_Click(object sender, EventArgs e)
    {
        pnlError.Visible = false;
        int idUsuario = 0;

        try
        {
            byte[] contenido = null;
            string nombreArchivo = string.Empty;

            if (fupArchivo.FileBytes.Length > 0)
            {
                contenido = fupArchivo.FileBytes;
                nombreArchivo = fupArchivo.FileName;
            }
            if (Session["UsuarioId"] != null)
            {
                int.TryParse(Session["UsuarioId"].ToString(), out idUsuario);
            }

            wsSOPF.GeneraPagoMavisoResponse respPagoMasivo = new wsSOPF.GeneraPagoMavisoResponse();
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                respPagoMasivo = ws.GenerarPagoMasivo(new wsSOPF.GeneraPagoMavisoRequest
                {
                    IdUsuario = idUsuario,
                    NombreArchivo = nombreArchivo,
                    ContenidoArchivo = contenido
                });
            }

            if (respPagoMasivo != null)
            {
                if (!respPagoMasivo.Error)
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                    throw new Exception(respPagoMasivo.MensajeOperacion);
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            pnlError.Visible = true;
        }
    }

    private static string fomatoMoneda(decimal? valor)
    {
        decimal valorD = 0M;
        if (valor != null) valorD = valor.Value;
        return valorD.ToString("C", CultureInfo.CurrentCulture);
    }

    #region Metodos Privados
    public static DateTime? ValidarFecha(string fecha)
    {
        if (!string.IsNullOrEmpty(fecha))
        {
            //string[] formats = { "dd/MM/yyyy" };
            DateTime FechaFinal;
            if (DateTime.TryParseExact(fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaFinal))
                return FechaFinal;
            else
                return null;
        }
        else
            return null;
    }
    #endregion

    #region WebMethods
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GenerarPago(GenerarPagoRequest pago)
    {
        PagoResponse respuesta = new PagoResponse();
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }
        if (string.IsNullOrEmpty(pago.Cuenta))
        {
            respuesta.Error = true;
            respuesta.MensajeOperacion = "Capture el número de cuenta";
            return respuesta;
        }

        if (pago.Monto <= 0)
        {
            respuesta.Error = true;
            respuesta.MensajeOperacion = "El valor de monto del pago no es correcto.";
            return respuesta;
        }

        if (string.IsNullOrEmpty(pago.Tarjeta.Trim()) || pago.Tarjeta.Trim().Length != 16)
        {
            respuesta.Error = true;
            respuesta.MensajeOperacion = "El número de tarjeta no es válido.";
            return respuesta;
        }

        if (pago.MesExp <= 0 || pago.MesExp > 12)
        {
            respuesta.Error = true;
            respuesta.MensajeOperacion = "El mes de expiración de la tarjeta no es válida.";
            return respuesta;
        }
        if (pago.AnioExp <= 0 || pago.AnioExp < DateTime.Now.Year - 1)
        {
            respuesta.Error = true;
            respuesta.MensajeOperacion = "El año de expiración de la tarjeta no es válida.";
            return respuesta;
        }

        if (pago != null)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago = ws.GenerarPago(new wsSOPF.GeneraPagoRequest
                    {
                        IdUsuario = idUsuario,
                        Cuenta = pago.Cuenta,
                        Monto = pago.Monto,
                        NumeroTarjeta = pago.Tarjeta,
                        MesExpiracion = pago.MesExp,
                        AnioExpiracion = pago.AnioExp
                    });
                    if (respPago != null)
                    {
                        respuesta.Error = respPago.Error;
                        respuesta.CodigoError = respPago.CodigoError;
                        respuesta.MensajeOperacion = respPago.MensajeOperacion;
                        if (respPago.Resultado != null)
                        {
                            respuesta.Resultado = new PagoResultado
                            {
                                IdTransaccion = respPago.Resultado.IdTransaccion,
                                FechaTransaccion = respPago.Resultado.FechaTransaccion,
                                Estatus = respPago.Resultado.Estatus,
                                DescripcionAcccion = respPago.Resultado.DescripcionAcccion,
                                CodigoAutorizacion = respPago.Resultado.CodigoAutorizacion,
                                NumeroRastreo = respPago.Resultado.NumeroRastreo,
                                Terminal = respPago.Resultado.Terminal
                            };
                        }
                    }
                    else
                    {
                        respuesta.Error = true;
                        respuesta.MensajeOperacion = "Ocurrió un error al momento de solicitar la autorización de pago.";
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = ex.Message;
            }
        }
        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerPagoMasivo(ObtenerPagoMasivoRequest peticion)
    {
        DateTime? DatoFechaDesde = null;
        DateTime? DatoFechaHasta = null;
        DatoFechaDesde = ValidarFecha(peticion.FechaDesde);
        DatoFechaHasta = ValidarFecha(peticion.FechaHasta);

        object resultado = null;
        if (!DatoFechaDesde.HasValue || !DatoFechaHasta.HasValue || DatoFechaDesde.Value > DatoFechaHasta.Value)
            return resultado;

        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                var respPagos = ws.PagosMasivos(new wsSOPF.PagosMasivosRequest()
                {
                    FechaDesde = DatoFechaDesde.Value,
                    FechaHasta = DatoFechaHasta.Value
                });
                if (respPagos != null)
                {
                    return (from p in respPagos.Resultado.PagosMasivos
                             select new
                             {
                                 p.IdPagoMasivo,
                                 p.NombreArchivo,
                                 FechaRegistro = p.FechaRegistro.ToString("dd-MM-yyyy HH:mm"),
                                 TotalRegistros = p.TotalRegistros == null ? 0 : p.TotalRegistros.Value,
                                 TotalProcesado = p.TotalProcesado == null ? 0 : p.TotalProcesado.Value,
                                 TotalAprobados = p.TotalAprobados == null ? 0 : p.TotalAprobados.Value,
                                 MontoTotal = fomatoMoneda(p.MontoTotal),
                                 Estatus = p.EstatusProceso,
                                 UltimaActualizacion = p.UltimaActualizacion == null ? "" : p.UltimaActualizacion.Value.ToString("dd-MM-yy HH:mm"),
                                 p.Mensaje
                             })
                            .OrderByDescending(p => p.FechaRegistro)
                            .ToList();
                }
            }
        }
        catch (Exception er)
        {
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object DescargarExcelFallidos(DescargarExcelFallidosRequest datos)
    {
        object resultado = null;
        try
        {
            wsSOPF.DescargarPagosFallidosVisanetResponse respDescargarPagosFallidosVisanet = new wsSOPF.DescargarPagosFallidosVisanetResponse();
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                respDescargarPagosFallidosVisanet = ws.DescargarPagosFallidosVisanet(datos.IdPagoMasivo);
            }
            if (respDescargarPagosFallidosVisanet != null)
            {
                if (!respDescargarPagosFallidosVisanet.Error && respDescargarPagosFallidosVisanet.Resultado.PagosFallidosVisanetVM.Tables.Count > 0)
                {
                    string NombreArchivoNuevo = "FondosInsuficientes_" + datos.NombreArchivo;                   
                    string contenido = string.Empty;

                    using (ExcelPackage pck = new ExcelPackage())
                    {
                        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Resultado");
                        ws.Cells["A1"].LoadFromDataTable(respDescargarPagosFallidosVisanet.Resultado.PagosFallidosVisanetVM.Tables[0], true);
                        var ms = new System.IO.MemoryStream();
                        pck.SaveAs(ms);
                        byte[] bytes = ms.ToArray();
                        contenido = Convert.ToBase64String(bytes);
                    }
                    resultado = new
                    {
                        Error = false,
                        Mensaje = "El reporte se ha generado exitosamente.",
                        Tipo = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        Longitud = contenido.Length.ToString(),
                        NombreArchivoNuevo,
                        Contenido = contenido
                    };
                }
                else
                    throw new Exception("Ha ocurrido un error. " + respDescargarPagosFallidosVisanet.MensajeOperacion ?? "");
            }
            return resultado;
        }
        catch (Exception ex)
        {
            return new { Error = true, Mensaje = "Ha ocurrido un error en la conexión para obtener los pagos fallidos. " + ex.Message ?? "" };
        }
    }
    #endregion
}