﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Visanet.Model.Request;
using Visanet.Model.Response;
using OfficeOpenXml;
using System.Text.RegularExpressions;
using Kushki.Model.Response;
using System.Collections.Generic;
using System.Data;
using System.ComponentModel;
using System.Configuration;

public partial class Site_Creditos_SuscripcionKushki : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {        
    }

    private static string fomatoMoneda(decimal? valor)
    {
        decimal valorD = 0M;
        if (valor != null) valorD = valor.Value;
        return valorD.ToString("C", CultureInfo.CurrentCulture);
    }

    #region Metodos Privados
    public static DateTime? ValidarFecha(string fecha)
    {
        if (!string.IsNullOrEmpty(fecha))
        {;
            DateTime FechaFinal;
            if (DateTime.TryParseExact(fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaFinal))
                return FechaFinal;
            else
                return null;
        }
        else
            return null;
    }

    public static string RecuperarIdSuscripcionKushki(string tarjeta)
    {
        string resultado = "";

        if (tarjeta != null)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago = ws.RecuperarIdSuscripcionKushki(tarjeta);
                    resultado = respPago;
                }
            }
            catch (Exception ex)
            {

            }
        }
        return resultado;
    }
    #endregion

    #region WebMethods


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerPagoMasivoKushki(ObtenerPagoMasivoKushkiRequest peticion)
    {
        DateTime? DatoFechaDesde = null;
        DateTime? DatoFechaHasta = null;
        DatoFechaDesde = ValidarFecha(peticion.FechaDesde);
        DatoFechaHasta = ValidarFecha(peticion.FechaHasta);

        object resultado = null;
        if (!DatoFechaDesde.HasValue || !DatoFechaHasta.HasValue || DatoFechaDesde.Value > DatoFechaHasta.Value)
            return resultado;

        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                var respPagos = ws.SuscripcionMasivosKushki(new wsSOPF.PagosMasivosKushkiRequest()
                {
                    FechaDesde = DatoFechaDesde.Value,
                    FechaHasta = DatoFechaHasta.Value
                });
                if (respPagos != null)
                {
                    return (from p in respPagos.Resultado.PagosMasivosKushki
                             select new
                             {
                                 p.IdPagoMasivoKushki,
                                 p.NombreArchivo,
                                 FechaRegistro = p.FechaRegistro.ToString("dd-MM-yyyy HH:mm"),
                                 TotalRegistros = p.TotalRegistros == null ? 0 : p.TotalRegistros.Value,
                                 TotalProcesado = p.TotalProcesado == null ? 0 : p.TotalProcesado.Value,
                                 TotalAprobados = p.TotalAprobados == null ? 0 : p.TotalAprobados.Value,
                                 TotalNoProcesado = p.TotalNoProcesado == null ? 0 : p.TotalNoProcesado.Value,
                                 Estatus = p.EstatusProceso,
                                 UltimaActualizacion = p.UltimaActualizacion == null ? "" : p.UltimaActualizacion.Value.ToString("dd-MM-yy HH:mm"),
                                 p.Mensaje
                             })
                            .OrderByDescending(p => p.FechaRegistro)
                            .ToList();
                }
            }
        }
        catch (Exception er)
        {
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object Validar(string[][] row, int numDatos,string nombreDoc)
    {
        PagoResponseKushki respuesta = new PagoResponseKushki();
        List<PagoKushkiRequest> LisPago = new List<PagoKushkiRequest>();
        PagoKushkiRequest pago = new PagoKushkiRequest();


        int linea = 1;
            int idUsuario = 0;
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }
            for (int i = 0; i < numDatos; i++)
            {
                pago = new PagoKushkiRequest();
                pago.NumeroTarjeta = row[i][0];
                pago.MesExpiracion = row[i][1];
                pago.AnioExpiracion = row[i][2];
                pago.NombreTarjetahabiente = row[i][3];
                pago.cvv = row[i][4];
                pago.TipoDocumento = row[i][5];
                pago.Documento = row[i][6];
                pago.email = row[i][7];
                pago.Apellido = row[i][8];
                pago.Telefono = row[i][9];
                pago.IdUsuario = idUsuario;
                LisPago.Add(pago);
            }
            if (numDatos == 0)
            {
                respuesta.Error = true;
                respuesta.CodigoError = "WVD01";
                respuesta.MensajeOperacion = "Por favor ingrese información del pago a realizar.";
                return respuesta;
            }
            foreach (var peticion in LisPago)
            {
           
                if (peticion.IdUsuario <= 0)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD02";
                    respuesta.MensajeOperacion = "El usuario no es válido para registrar el pago.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }

                if (string.IsNullOrWhiteSpace(peticion.NumeroTarjeta) || peticion.NumeroTarjeta.Length != 16)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD05";
                    respuesta.MensajeOperacion = "Favor de ingresar un número de tarjeta a 16 dígitos";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                else
                {
                    var idsus = RecuperarIdSuscripcionKushki(peticion.NumeroTarjeta);
                    if (idsus != "")
                    {
                        respuesta.Error = true;
                        respuesta.CodigoError = "WVD15";
                        respuesta.MensajeOperacion = "La tarjeta ya se encuentra suscrita";
                        respuesta.Linea = Convert.ToString(linea);
                        return respuesta;
                    }
                }

                if (string.IsNullOrWhiteSpace(peticion.MesExpiracion) || peticion.MesExpiracion.Length != 2 || Convert.ToInt32(peticion.MesExpiracion) <= 0 || Convert.ToInt32(peticion.MesExpiracion) > 12)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD06";
                    respuesta.MensajeOperacion = "El mes de expiración no es válido.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                if (string.IsNullOrWhiteSpace(peticion.AnioExpiracion) || peticion.AnioExpiracion.Length != 2)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD07";
                    respuesta.MensajeOperacion = "El año de expiración no es válido.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                if (string.IsNullOrEmpty(peticion.NombreTarjetahabiente) || peticion.NombreTarjetahabiente.Length > 80)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD08";
                    respuesta.MensajeOperacion = "Favor ingresar el Tarjeta Habiente.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                if (string.IsNullOrWhiteSpace(peticion.cvv) || peticion.cvv.Length < 3 || peticion.cvv.Length > 4)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD09";
                    respuesta.MensajeOperacion = "CVV no es válido.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                if (string.IsNullOrWhiteSpace(peticion.TipoDocumento) || peticion.TipoDocumento.Length < 3 || peticion.TipoDocumento.Length > 4)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD10";
                    respuesta.MensajeOperacion = "Tipo de Documento no es válido.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                if (string.IsNullOrWhiteSpace(peticion.Documento) ||peticion.Documento.Length > 11)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD11";
                    respuesta.MensajeOperacion = "Documento no es válido.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                if (string.IsNullOrWhiteSpace(peticion.email) || peticion.email.Length > 60)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD12";
                    respuesta.MensajeOperacion = "Email no es válido.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                if (string.IsNullOrWhiteSpace(peticion.Apellido) || peticion.Apellido.Length > 80)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD13";
                    respuesta.MensajeOperacion = "Apellido no es válido.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }
                if (string.IsNullOrWhiteSpace(peticion.Telefono) || peticion.Telefono.Substring(0, 1)!="+" || peticion.Telefono.Length !=12)
                {
                    respuesta.Error = true;
                    respuesta.CodigoError = "WVD14";
                    respuesta.MensajeOperacion = "Telefono no es válido.";
                    respuesta.Linea = Convert.ToString(linea);
                    return respuesta;
                }

                
            linea++;
            }

            if (numDatos>0)
            {
                try
                {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                        var respPago = ws.GenerarSuscripcionKushki(new wsSOPF.GenerarPagoKushkiRequest
                        {
                            NumeroDatos= numDatos,
                            NombreDocumento=nombreDoc,
                            pagos= row,
                            IdUsuario=idUsuario
                        });
                        if (respPago != null)
                        {
                            respuesta.Error = respPago.Error;
                            respuesta.CodigoError = respPago.CodigoError;
                            respuesta.MensajeOperacion = respPago.MensajeOperacion;
                            respuesta.Resultado.IdPeticion = respPago.Resultado.IdPeticion;
                          
                        }
                        else
                        {
                            respuesta.Error = true;
                            respuesta.MensajeOperacion = "Ocurrió un error al momento de solicitar la autorización de pago.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    respuesta.Error = true;
                    respuesta.MensajeOperacion = ex.Message;
                }
            }
       
        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object SuscribirseKushki(string Token, PagoKushkiRequest pago)
    {
        object resultado = "";
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }

        if (pago != null)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago =  ws.GenerarSuscripcionMasivoKushki(new wsSOPF.GeneraPagoMavisokushkiRequest
                    {
                        IdUsuario = idUsuario,
                        cvv = pago.cvv.Length > 4 ? pago.cvv.Substring(0,4) : pago.cvv,
                        TipoMoneda = pago.TipoMoneda,
                        Moneda = pago.Moneda,
                        NumeroTarjeta = pago.NumeroTarjeta.Length > 16 ? pago.NumeroTarjeta.Substring(0,16) : pago.NumeroTarjeta,
                        MesExpiracion = pago.MesExpiracion.Length > 2 ? pago.MesExpiracion.Substring(0,2) : pago.MesExpiracion,
                        AnioExpiracion =  pago.AnioExpiracion.Length > 2 ? pago.AnioExpiracion.Substring(0,2) : pago.AnioExpiracion,
                        NombreTarjetahabiente = pago.NombreTarjetahabiente.Length > 80 ? pago.NombreTarjetahabiente.Substring(0,80) : pago.NombreTarjetahabiente,
                        fecha = pago.fecha,
                        Telefono = pago.Telefono.Length > 12 ? pago.Telefono.Substring(0,12) : pago.Telefono,
                        Apellido = pago.Apellido.Length > 80 ? pago.Apellido.Substring(0,80) : pago.Apellido,
                        email = pago.email.Length > 60 ? pago.email.Substring(0,60) : pago.email,
                        TipoDocumento = pago.TipoDocumento.Length > 4 ? pago.TipoDocumento.Substring(0,4) : pago.TipoDocumento,
                        Documento = pago.Documento.Length > 11 ? pago.Documento.Substring(0,11) : pago.Documento,
                        Token=Token,
                        IdPagoMasivo=pago.IdPagoMasivo
                    });
                    resultado = respPago;
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GuardarPagoMasivoSuscripcion(string Mensaje, PagoKushkiRequest pago)
    {
        object resultado = "";
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }

        if (pago != null)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago =   ws.GuardarPagoMasivoSuscripcion(new wsSOPF.GeneraPagoMavisokushkiRequest
                    {
                        IdUsuario = idUsuario,
                        cvv = pago.cvv,
                        TipoMoneda = pago.TipoMoneda,
                        Moneda = pago.Moneda,
                        NumeroTarjeta = pago.NumeroTarjeta,
                        MesExpiracion = pago.MesExpiracion,
                        AnioExpiracion = pago.AnioExpiracion,
                        NombreTarjetahabiente = pago.NombreTarjetahabiente,
                        fecha = pago.fecha,
                        Telefono = pago.Telefono,
                        Apellido = pago.Apellido,
                        email = pago.email,
                        TipoDocumento = pago.TipoDocumento,
                        Documento = pago.Documento,
                        IdPagoMasivo = pago.IdPagoMasivo,
                        mensaje=Mensaje
                    });
                    resultado = respPago;
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object TerminoProceso(int IdPagoMasivo)
    {
        object resultado = "";        

        if (IdPagoMasivo != 0)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago = ws.TerminoProcesoSuscripcion(IdPagoMasivo);
                    resultado = respPago;
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object DescargarExcelFallidos(string[][] row, int numDatos, string nombreDoc)
    {
        object resultado = null;
        List<PagoKushkiRequest> LisPago = new List<PagoKushkiRequest>();
        PagoKushkiRequest pago = new PagoKushkiRequest();
        List<FallidosSuscripcionKushkiRequest> LisErr = new List<FallidosSuscripcionKushkiRequest>();
        FallidosSuscripcionKushkiRequest err = new FallidosSuscripcionKushkiRequest();
        int linea = 1;
        for (int i = 0; i < numDatos; i++)
        {
            pago = new PagoKushkiRequest();
            pago.NumeroTarjeta = row[i][0];
            pago.MesExpiracion = row[i][1];
            pago.AnioExpiracion = row[i][2];
            pago.NombreTarjetahabiente = row[i][3];
            pago.cvv = row[i][4];
            pago.TipoDocumento = row[i][5];
            pago.Documento = row[i][6];
            pago.email = row[i][7];
            pago.Apellido = row[i][8];
            pago.Telefono = row[i][9];
            LisPago.Add(pago);
        }
        foreach (var peticion in LisPago)
        {
            int error = 0;
            string Mensaje = "";

            if (string.IsNullOrWhiteSpace(peticion.NumeroTarjeta) || peticion.NumeroTarjeta.Length != 16)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NoTarjeta";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NoTarjeta";
                }

            }
            else
            {
                var idsus = RecuperarIdSuscripcionKushki(peticion.NumeroTarjeta);
                if (idsus != "")
                {
                    error = 1;
                    if (Mensaje == "")
                    {
                        Mensaje = "Revise la tarjeta ya esta suscrita";
                    }
                    else
                    {
                        Mensaje = Mensaje + ", " + "la tarjeta ya esta suscrita";
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(peticion.MesExpiracion) || peticion.MesExpiracion.Length != 2 || Convert.ToInt32(peticion.MesExpiracion) <= 0 || Convert.ToInt32(peticion.MesExpiracion) > 12)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise MesExpiracion";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "MesExpiracion";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.AnioExpiracion) || peticion.AnioExpiracion.Length != 2)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise AnioExpiracion";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "AnioExpiracion";
                }
            }
            if (string.IsNullOrEmpty(peticion.NombreTarjetahabiente) || peticion.NombreTarjetahabiente.Length >80)
            {
                error = 1;
                if (Mensaje == "")
                {
                   Mensaje = "Revise NombreTarjetahabiente";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NombreTarjetahabiente";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.cvv) || peticion.cvv.Length < 3 || peticion.cvv.Length > 4)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Cvv";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Cvv";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.TipoDocumento) || peticion.TipoDocumento.Length < 3 || peticion.TipoDocumento.Length > 4)
            {
                error = 1;
                if (Mensaje == "")
                {
                     Mensaje = "Revise TipoDocumento";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "TipoDocumento";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Documento) || peticion.Documento.Length > 11)
            {
                error = 1;
                if ( Mensaje == "")
                {
                    Mensaje = "Revise Documento";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Documento";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.email) || peticion.email.Length > 60)
            {
                error = 1;
                if ( Mensaje == "")
                {
                     Mensaje = "Revise email";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "email";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Apellido) || peticion.Apellido.Length > 80)
            {
                error = 1;
                if ( Mensaje == "")
                {
                     Mensaje = "Revise Apellido";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Apellido";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Telefono) || peticion.Telefono.Substring(0, 1) != "+" || peticion.Telefono.Length !=12)
            {
                error = 1;
                if ( Mensaje == "")
                {
                    Mensaje = "Revise Telefono";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Telefono";
                }
            }

            if (error == 1)
            {
                err = new FallidosSuscripcionKushkiRequest();
                err.Linea = linea;
                err.NoTarjeta = peticion.NumeroTarjeta;
                err.MesExpiracion = peticion.MesExpiracion;
                err.AnioExpiracion = peticion.AnioExpiracion;
                err.NombreTarjetahabiente = peticion.NombreTarjetahabiente;
                err.Cvv = peticion.cvv;
                err.TipoDocumento = peticion.TipoDocumento;
                err.Documento = peticion.Documento;
                err.email = peticion.email;
                err.Apellido = peticion.Apellido;
                err.Telefono = peticion.Telefono;
                err.Mensaje = Mensaje;
                LisErr.Add(err);

            }
            linea++;
        }

        if (LisErr != null)
        {
           
                string NombreArchivoNuevo = "ErroresSuscripcion_" + nombreDoc ;
                string contenido = string.Empty;
            DataSet ds = new DataSet();
            ds.Tables.Add(ConvertToDatatable.ConvertToDatatab(LisErr));
           
            using (ExcelPackage pck = new ExcelPackage())
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Resultado");
                    ws.Cells["A1"].LoadFromDataTable(ds.Tables[0], true);
                    var ms = new System.IO.MemoryStream();
                    pck.SaveAs(ms);
                    byte[] bytes = ms.ToArray();
                    contenido = Convert.ToBase64String(bytes);
             }
                resultado = new
                {
                    Error = false,
                    Mensaje = "El reporte se ha generado exitosamente.",
                    Tipo = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    Longitud = contenido.Length.ToString(),
                    NombreArchivoNuevo,
                    Contenido = contenido
                };
     
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ProcesarSoloCorrectos(string[][] row, int numDatos, string nombreDoc, string[][] row2)
    {
        PagoResponseKushki respuesta = new PagoResponseKushki();
        List<PagoKushkiRequest> LisPago = new List<PagoKushkiRequest>();
        PagoKushkiRequest pago = new PagoKushkiRequest();
        List<PagoKushkiRequest> LisErr = new List<PagoKushkiRequest>();
        PagoKushkiRequest err = new PagoKushkiRequest();
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }
        for (int i = 0; i < numDatos; i++)
        {
            pago = new PagoKushkiRequest();
            pago.NumeroTarjeta = row[i][0];
            pago.MesExpiracion = row[i][1];
            pago.AnioExpiracion = row[i][2];
            pago.NombreTarjetahabiente = row[i][3];
            pago.cvv = row[i][4];
            pago.TipoDocumento = row[i][5];
            pago.Documento = row[i][6];
            pago.email = row[i][7];
            pago.Apellido = row[i][8];
            pago.Telefono = row[i][9];
            pago.IdUsuario = idUsuario;
            LisPago.Add(pago);
        }
       
        int numDatos1 = 0;
        int numDatos2 = 0;
        foreach (var peticion in LisPago)
        {
            int error = 0;
            if (peticion.IdUsuario <= 0)
            {
                error = 1;
            }

            if (string.IsNullOrWhiteSpace(peticion.NumeroTarjeta) || peticion.NumeroTarjeta.Length != 16)
            {
                error = 1;

            }
            else
            {
                var idsus = RecuperarIdSuscripcionKushki(peticion.NumeroTarjeta);
                if (idsus != "")
                {
                    error = 1;
                }
            }

            if (string.IsNullOrWhiteSpace(peticion.MesExpiracion) || peticion.MesExpiracion.Length != 2 || Convert.ToInt32(peticion.MesExpiracion) <= 0 || Convert.ToInt32(peticion.MesExpiracion) > 12)
            {
                error = 1;
            }
            if (string.IsNullOrWhiteSpace(peticion.AnioExpiracion) || peticion.AnioExpiracion.Length != 2)
            {
                error = 1;
            }
            if (string.IsNullOrEmpty(peticion.NombreTarjetahabiente) || peticion.NombreTarjetahabiente.Length >80)
            {
                error = 1;
            }
            if (string.IsNullOrWhiteSpace(peticion.cvv) || peticion.cvv.Length < 3 || peticion.cvv.Length > 4)
            {
                error = 1;
            }
            if (string.IsNullOrWhiteSpace(peticion.TipoDocumento) || peticion.TipoDocumento.Length < 3 || peticion.TipoDocumento.Length > 4)
            {
                error = 1;
            }
            if (string.IsNullOrWhiteSpace(peticion.Documento) || peticion.Documento.Length > 11)
            {
                error = 1;
            }
            if (string.IsNullOrWhiteSpace(peticion.email) || peticion.email.Length > 60)
            {
                error = 1;
            }
            if (string.IsNullOrWhiteSpace(peticion.Apellido) || peticion.Apellido.Length > 80)
            {
                error = 1;
            }
            if (string.IsNullOrWhiteSpace(peticion.Telefono) || peticion.Telefono.Substring(0, 1) != "+" || peticion.Telefono.Length != 12)
            {
                error = 1;
            }

            if (error == 0)
            {
                 row[numDatos1][0]= peticion.NumeroTarjeta ;
                 row[numDatos1][1]= peticion.MesExpiracion ;
                 row[numDatos1][2]= peticion.AnioExpiracion ;
                 row[numDatos1][3]= peticion.NombreTarjetahabiente ;
                 row[numDatos1][4]= peticion.cvv ;
                 row[numDatos1][5]= peticion.TipoDocumento ;
                 row[numDatos1][6]= peticion.Documento ;
                row[numDatos1][7]= peticion.email ;
                row[numDatos1][8]= peticion.Apellido ;
                 row[numDatos1][9]= peticion.Telefono ;
                numDatos1 ++;

            }
        }

        foreach (var peticion in LisPago)
        {
            int error = 0;
            string Mensaje = "";


            if (string.IsNullOrWhiteSpace(peticion.NumeroTarjeta) || peticion.NumeroTarjeta.Length != 16)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NoTarjeta";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NoTarjeta";
                }

            }
            else
            {
                var idsus = RecuperarIdSuscripcionKushki(peticion.NumeroTarjeta);
                if (idsus != "")
                {
                    error = 1;
                    if (Mensaje == "")
                    {
                        Mensaje = "Revise la tarjeta ya esta suscrita";
                    }
                    else
                    {
                        Mensaje = Mensaje + ", " + "la tarjeta ya esta suscrita";
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(peticion.MesExpiracion) || peticion.MesExpiracion.Length != 2 || Convert.ToInt32(peticion.MesExpiracion) <= 0 || Convert.ToInt32(peticion.MesExpiracion) > 12)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise MesExpiracion";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "MesExpiracion";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.AnioExpiracion) || peticion.AnioExpiracion.Length != 2)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise AnioExpiracion";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "AnioExpiracion";
                }
            }
            if (string.IsNullOrEmpty(peticion.NombreTarjetahabiente) || peticion.NombreTarjetahabiente.Length > 80)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NombreTarjetahabiente";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NombreTarjetahabiente";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.cvv) || peticion.cvv.Length < 3 || peticion.cvv.Length > 4)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Cvv";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Cvv";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.TipoDocumento) || peticion.TipoDocumento.Length < 3 || peticion.TipoDocumento.Length > 4)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise TipoDocumento";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "TipoDocumento";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Documento) || peticion.Documento.Length > 11)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Documento";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Documento";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.email) || peticion.email.Length > 60)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise email";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "email";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Apellido) || peticion.Apellido.Length > 80)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Apellido";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Apellido";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Telefono) || peticion.Telefono.Substring(0, 1) != "+" || peticion.Telefono.Length !=12)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Telefono";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Telefono";
                }
            }
            if (error == 1)
            {
                row2[numDatos2][0] = peticion.NumeroTarjeta.Length > 16 ? peticion.NumeroTarjeta.Substring(0,16) : peticion.NumeroTarjeta;
                row2[numDatos2][1] = peticion.MesExpiracion.Length > 2 ? peticion.MesExpiracion.Substring(0,2) : peticion.MesExpiracion;
                row2[numDatos2][2] = peticion.AnioExpiracion.Length > 2 ? peticion.AnioExpiracion.Substring(0,2) : peticion.AnioExpiracion;
                row2[numDatos2][3] = peticion.NombreTarjetahabiente.Length > 80 ? peticion.NombreTarjetahabiente.Substring(0,80) : peticion.NombreTarjetahabiente;
                row2[numDatos2][4] = peticion.cvv.Length > 4 ? peticion.cvv.Substring(0,4) : peticion.cvv;
                row2[numDatos2][5] = peticion.TipoDocumento.Length > 4 ? peticion.TipoDocumento.Substring(0,4) : peticion.TipoDocumento;
                row2[numDatos2][6] = peticion.Documento.Length > 11 ? peticion.Documento.Substring(0,11) : peticion.Documento;
                row2[numDatos2][7] = peticion.email.Length > 60 ? peticion.email.Substring(0,60) : peticion.email;
                row2[numDatos2][8] = peticion.Apellido.Length > 80 ? peticion.Apellido.Substring(0,80) : peticion.Apellido;
                row2[numDatos2][9] = peticion.Telefono.Length > 12 ? peticion.Telefono.Substring(0,12) : peticion.Telefono;
                numDatos2 ++;

            }
        }
        if (numDatos > 0)
        {
            try
            {
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    var respPago = ws.GenerarSuscripcionConErrKushki(new wsSOPF.GenerarPagoKushkiRequest
                    {
                        NumeroDatos = numDatos,
                        NumeroDatosSE = numDatos1,
                        NumeroDatosCE = numDatos2,
                        NombreDocumento = nombreDoc,
                        pagos = row,
                        pagosErr = row2,
                        IdUsuario = idUsuario
                    });
                    if (respPago != null)
                    {
                        respuesta.Error = respPago.Error;
                        respuesta.CodigoError = respPago.CodigoError;
                        respuesta.MensajeOperacion = respPago.MensajeOperacion;
                        respuesta.Resultado.IdPeticion = respPago.Resultado.IdPeticion;
                        respuesta.Resultado.rowdev = row;
                        respuesta.Resultado.ndatos = numDatos1;
                    }
                    else
                    {
                        respuesta.Error = true;
                        respuesta.MensajeOperacion = "Ocurrió un error al momento de solicitar la autorización de pago.";
                    }
                }
            }
            catch (Exception ex)
            {
                respuesta.Error = true;
                respuesta.MensajeOperacion = ex.Message;
            }
        }
        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object MostrarFallidos(string[][] row, int numDatos, string nombreDoc)
    {
        object resultado = null;
        List<PagoKushkiRequest> LisPago = new List<PagoKushkiRequest>();
        PagoKushkiRequest pago = new PagoKushkiRequest();
        List<FallidosSuscripcionKushkiRequest> LisErr = new List<FallidosSuscripcionKushkiRequest>();
        FallidosSuscripcionKushkiRequest err = new FallidosSuscripcionKushkiRequest();
        int linea = 1;
        for (int i = 0; i < numDatos; i++)
        {
            pago = new PagoKushkiRequest();
            pago.NumeroTarjeta = row[i][0];
            pago.MesExpiracion = row[i][1];
            pago.AnioExpiracion = row[i][2];
            pago.NombreTarjetahabiente = row[i][3];
            pago.cvv = row[i][4];
            pago.TipoDocumento = row[i][5];
            pago.Documento = row[i][6];
            pago.email = row[i][7];
            pago.Apellido = row[i][8];
            pago.Telefono = row[i][9];
            LisPago.Add(pago);
        }
        foreach (var peticion in LisPago)
        {
            int error = 0;
            string Mensaje = "";

            if (string.IsNullOrWhiteSpace(peticion.NumeroTarjeta) || peticion.NumeroTarjeta.Length != 16)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NoTarjeta";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NoTarjeta";
                }

            }
            else
            {
                var idsus = RecuperarIdSuscripcionKushki(peticion.NumeroTarjeta);
                if (idsus != "")
                {
                    error = 1;
                    if (Mensaje == "")
                    {
                        Mensaje = "Revise la tarjeta ya esta suscrita";
                    }
                    else
                    {
                        Mensaje = Mensaje + ", " + "la tarjeta ya esta suscrita";
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(peticion.MesExpiracion) || peticion.MesExpiracion.Length != 2 || Convert.ToInt32(peticion.MesExpiracion) <= 0 || Convert.ToInt32(peticion.MesExpiracion) > 12)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise MesExpiracion";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "MesExpiracion";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.AnioExpiracion) || peticion.AnioExpiracion.Length != 2)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise AnioExpiracion";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "AnioExpiracion";
                }
            }
            if (string.IsNullOrEmpty(peticion.NombreTarjetahabiente) || peticion.NombreTarjetahabiente.Length > 80)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise NombreTarjetahabiente";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "NombreTarjetahabiente";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.cvv) || peticion.cvv.Length < 3 || peticion.cvv.Length > 4)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Cvv";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Cvv";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.TipoDocumento) || peticion.TipoDocumento.Length < 3 || peticion.TipoDocumento.Length > 4)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise TipoDocumento";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "TipoDocumento";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Documento) || peticion.Documento.Length > 11)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Documento";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Documento";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.email) || peticion.email.Length > 60)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise email";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "email";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Apellido) || peticion.Apellido.Length > 80)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Apellido";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Apellido";
                }
            }
            if (string.IsNullOrWhiteSpace(peticion.Telefono) || peticion.Telefono.Substring(0, 1) != "+" || peticion.Telefono.Length != 12)
            {
                error = 1;
                if (Mensaje == "")
                {
                    Mensaje = "Revise Telefono";
                }
                else
                {
                    Mensaje = Mensaje + ", " + "Telefono";
                }
            }

            if (error == 1)
            {
                err = new FallidosSuscripcionKushkiRequest();
                err.Linea = linea;
                err.NoTarjeta = peticion.NumeroTarjeta;
                err.MesExpiracion = peticion.MesExpiracion;
                err.AnioExpiracion = peticion.AnioExpiracion;
                err.NombreTarjetahabiente = peticion.NombreTarjetahabiente;
                err.Cvv = peticion.cvv;
                err.TipoDocumento = peticion.TipoDocumento;
                err.Documento = peticion.Documento;
                err.email = peticion.email;
                err.Apellido = peticion.Apellido;
                err.Telefono = peticion.Telefono;
                err.Mensaje = Mensaje;
                LisErr.Add(err);

            }
            linea++;
        }
       
        if (LisErr != null)
        {

            return LisErr;

        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object MostrarArchivos(string[][] row, int numDatos, string nombreDoc)
    {
        object resultado = null;
        List<PagoKushkiRequest> LisPago = new List<PagoKushkiRequest>();
        PagoKushkiRequest pago = new PagoKushkiRequest();

        for (int i = 0; i < numDatos; i++)
        {
            pago = new PagoKushkiRequest();
            pago.NumeroTarjeta = row[i][0];
            pago.MesExpiracion = row[i][1];
            pago.AnioExpiracion = row[i][2];
            pago.NombreTarjetahabiente = row[i][3];
            pago.cvv = row[i][4];
            pago.TipoDocumento = row[i][5];
            pago.Documento = row[i][6];
            pago.email = row[i][7];
            pago.Apellido = row[i][8];
            pago.Telefono = row[i][9];
            LisPago.Add(pago);
        }
        
        if (LisPago != null)
        {

            return LisPago;

        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object RecuperarIdComercioMoneda()
    {
        object resultado = null;
        IdComercioResponsekushki id = new IdComercioResponsekushki();
        id.IdComercio = ConfigurationManager.AppSettings.Get("MerchandId");
        id.Moneda = ConfigurationManager.AppSettings.Get("Moneda");
        if (id.IdComercio == null || id.Moneda == null)
        {
            return resultado;
        }
        else
        {
            resultado = id;
        }
        return resultado;
    }

    #endregion

}
