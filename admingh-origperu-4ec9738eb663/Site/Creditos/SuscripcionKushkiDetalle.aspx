﻿<%@ Page Title="SuscripcionMasivosKushkiDetalle" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SuscripcionKushkiDetalle.aspx.cs" Inherits="Site_Creditos_SuscripcionKushkiDetalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.css"/>
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <style>
        /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
        .modal-pr {
            display: none;
            position: fixed;
            z-index: 9000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 ) url('../../Imagenes/ajax-loader.gif') 50% 50% no-repeat;
        }

        /* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
        body.loading .modal-pr {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
   modal element will be visible */
        body.loading .modal-pr {
            display: block;
        }        
    </style>

    <script src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script src="../../js/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../../js/datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/datatables.min.js"></script>        
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/creditos/SuscripcionKushkiDetalle.js?v=<%=AppSettings.VersionPub %>"></script>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Suscripcion Masiva Kushki - Detalle</h3>
        </div>
        <div class="panel-body">
            <div class="row">                
                <div class="col-sm-12">                    
                   <input type="number" id="txIdArchivo" class="form-control" placeholder="Id de archivo" style="visibility:hidden;" />
                   <asp:LinkButton ID="lbtnRegresar" CssClass="btn btn-sm btn-default hidden-print" OnClick="lbtnRegresar_Click" runat="server">
                        <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                    </asp:LinkButton>
                    <hr />
                    <table id="tbPagoMasivoKushkiDetalle" class="table display compact nowrap" style="width:100%"></table>
                </div>               
            </div>
        </div>        
    </div>
    <div class="modal-pr"></div>
    <div id="divLoader" class="hidden">
        <div id="loader-background"></div>
        <div id="loader-content"></div>
    </div>
</asp:Content>
