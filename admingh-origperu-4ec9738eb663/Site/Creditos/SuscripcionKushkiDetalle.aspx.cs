﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;

public partial class Site_Creditos_SuscripcionKushkiDetalle : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void lbtnRegresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("SuscripcionKushki.aspx");
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerPagoMasivoKushkiDetalle(int pagoMasivo)
    {
        object respuesta = null;

        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                var respPagos = ws.SuscripcionMasivosDetalleKushki(new wsSOPF.PagosMasivosDetalleKushkiRequest()
                {
                    IdPagoMasivo = pagoMasivo                    
                });
                if (respPagos != null)
                {
                    var pagoMasivoDet = (from p in respPagos.Resultado.PagoMasivoDetalleKushki
                                         select new
                                         {
                                             p.IdPagoMasivoDetalleKushki,
                                             p.NoTarjeta,
                                             p.Estatus,
                                             p.DescripcionAccion,
                                             p.NumeroRastreo,
                                             p.Telefono,
                                             p.NombreTarjetahabiente
                                         })
                                         .OrderBy(p => p.IdPagoMasivoDetalleKushki)
                                         .ToList();
                    return pagoMasivoDet;
                }
            }
        }
        catch (Exception ex)
        {
            return ex;
        }
        return respuesta;
    }

    private static string fomatoMoneda(decimal? valor)
    {
        decimal valorD = 0M;
        if (valor != null) valorD = valor.Value;
        return valorD.ToString("C", CultureInfo.CurrentCulture);
    }
}