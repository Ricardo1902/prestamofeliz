﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmActualizarCliente.aspx.cs"
    Inherits="Creditos_frmActualizarCliente" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Actualizar Datos</title>
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>

    <link href="/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/bootstrap.min.css" /> 
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <blockquote>
            <p>
                Actualizar Datos
            </p>
        </blockquote>
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Lada Fijo:" CssClass="Etiqueta"></asp:Label>
            <asp:TextBox ID="txtLada" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Telefono Fijo:" CssClass="Etiqueta"></asp:Label>
            <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Celular:" CssClass="Etiqueta"></asp:Label>
            <asp:Label ID="txtCelular" runat="server" Text="" CssClass="form-control"></asp:Label>
        </div>

        <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="Calle:" CssClass="Etiqueta"></asp:Label>
            <asp:TextBox ID="txtCalle" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:Label ID="Label8" runat="server" Text="Número:" CssClass="Etiqueta"></asp:Label>
            <asp:TextBox ID="txtNumero" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Estado:" CssClass="Etiqueta"></asp:Label>
            <asp:DropDownList ID="cboEstado" runat="server" onchange="SelectedIndexChange(this);" CssClass="form-control">
            </asp:DropDownList>
        </div>

        <div class="form-group">
            <asp:Label ID="Label5" runat="server" Text="Ciudad:" CssClass="Etiqueta"></asp:Label>
            <asp:DropDownList ID="cboCiudad" runat="server" onchange="SelectedIndexChange(this);" CssClass="form-control">
            </asp:DropDownList>
        </div>

        <div class="form-group">
            <asp:Label ID="Label6" runat="server" Text="Colonia:" CssClass="Etiqueta"></asp:Label>
            <asp:DropDownList ID="cboColonia" runat="server" onchange="SelectedIndexChange(this);" CssClass="form-control">
            </asp:DropDownList>
        </div>

        <div class="form-group">
            <asp:Label ID="Label9" runat="server" Text="Clabe:" CssClass="Etiqueta"></asp:Label>
            <asp:TextBox ID="txtClabe" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        
        <div class="form-group">
            <asp:Label ID="Label10" runat="server" Text="Banco:" CssClass="Etiqueta"></asp:Label>
            <asp:DropDownList ID="cboBanco" runat="server" onchange="SelectedIndexChange(this);" CssClass="form-control">
            </asp:DropDownList>
        </div>
        
         <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" OnClick="btnActualizar_Click" CssClass="btn btn-default" />
    </div>
    </form>
</body>
</html>
