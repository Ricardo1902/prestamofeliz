﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmAdministrarPlantillaCobranza.aspx.cs" Inherits="Site_Creditos_frmAdministrarPlantillaCobranza" MasterPageFile="~/Site.master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolKit"%>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../jQuery/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../jQuery/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="../../jQuery/jquery-ui.theme.min.css" rel="stylesheet" />
    <link href="../../Css/select2.min.css" rel="stylesheet" />
    <link href="../../Css/select2-bootstrap.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../js/select2.min.js"></script>
    <script type="text/javascript" src="../../js/es.js"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="150" runat="server">
        <ProgressTemplate>
            <div id="loader-background"></div>
            <div id="loader-content"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>    
    <div class="panel panel-primary">
        <div class="panel-heading">
            <asp:UpdatePanel ID="upTitulo" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <h3 class="panel-title"><asp:Label ID="lblTitulo" Text="Administrar Plantillas Cobranza" runat="server"></asp:Label></h3>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="panel-body">            
            <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>  
                <!-- PANEL BUSCAR -->                                             
                <asp:Panel ID="pnlBuscar" runat="server">
                    <asp:Panel ID="pnlExito_Buscar" class="form-group col-sm-12" Visible="false" runat="server">                                
                        <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                            <asp:Label ID="lblExito_Buscar" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlError_Buscar" class="form-group col-sm-12" Visible="false" runat="server">                                
                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                            <asp:Label ID="lblError_Buscar" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>

                    <div class="col-md-4 col-lg-3">
                        <asp:LinkButton ID="lbtnAgregarPlantilla" OnClick="lbtnAgregarPlantilla_Click" CssClass="btn btn-sm btn-success" runat="server">
                           <i class="fas fa-file-medical" style="font-size:1.4em"></i> Crear Nueva Plantilla
                        </asp:LinkButton>
                    </div>
                    
                    <div class="col-sm-12">
                        <hr />
                    </div>

                    <div class="form-group col-md-4 col-lg-4">
                        <label class="small">Nombre</label>
                        <asp:TextBox ID="txtNombrePlantilla_Buscar" MaxLength="200" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                    <div class="form-group col-md-4 col-lg-4">
                        <label class="small">Ver Plantillas Activas</label>
                        <asp:DropDownList ID="ddlActivo_Buscar" CssClass="form-control" runat="server">
                            <asp:ListItem Value="-1" Text="Todos"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <div class="form-group col-md-4 col-lg-3">
                        <label class="small">Tipo Layout</label>
                        <asp:DropDownList ID="ddlTipoLayout_Buscar" DataValueField="IdTipoLayoutCobro" DataTextField="TipoLayout" AppendDataBoundItems="true" CssClass="form-control" runat="server">
                            <asp:ListItem Value="-1" Text="Todos"></asp:ListItem>                            
                        </asp:DropDownList>
                    </div>
                    
                    <div class="form-group col-md-4 col-lg-3">
                        <asp:LinkButton ID="lbtnBuscar" OnClick="lbtnBuscar_Click" CssClass="btn btn-sm btn-default" runat="server">
                           <i class="fas fa-search" style="font-size:1.4em"></i> Buscar
                        </asp:LinkButton>
                    </div>
                            
                    <div class="form-group col-sm-12">
                        <asp:GridView ID="gvResultado_Buscar" runat="server" Width="100%" AutoGenerateColumns="False"
                            Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"                                                                
                            OnPageIndexChanging="gvResultado_Buscar_PageIndexChanging"
                            OnRowCommand="gvResultado_Buscar_RowCommand"
                            PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"                            
                            Style="overflow-x:auto;">                                        
                            <Columns>
                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnEditar" CommandName="Editar" CommandArgument='<%# Eval("IdPlantilla") %>' data-toggle="tooltip" title="Editar" runat="server">
                                            <i class="far fa-edit"></i>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Nombre" DataField="Nombre" ItemStyle-Width="60%"/>
                                <asp:BoundField HeaderText="Layout" DataField="TipoLayout.TipoLayout"/>                                                            
                                <asp:CheckBoxField HeaderText="Activo" DataField="Activo" ItemStyle-HorizontalAlign="Center"/> 
                            </Columns>                                        
                            <PagerStyle CssClass="pagination-ty warning" />                                                                                
                            <EmptyDataTemplate>
                                No se encontraron Plantillas
                            </EmptyDataTemplate>
                        </asp:GridView>                                                
                    </div>                          
                </asp:Panel>
                <!-- TERMINA PANEL BUSCAR -->

                <!-- PANEL GUARDAR -->
                <asp:Panel ID="pnlGuardar" Visible="false" runat="server">                                      
                    <div class="form-group col-md-8 col-lg-8">
                        <label class="small">Nombre Plantilla</label>
                        <asp:TextBox ID="txtNombrePlantilla_Guardar" MaxLength="200" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>                 

                    <div class="col-md-4 col-lg-4">
                        <label class="small">Layout Cobro</label>
                        <asp:DropDownList ID="ddlTipoLayout_Guardar" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoLayout_Guardar_SelectedIndexChanged" DataValueField="IdTipoLayoutCobro" DataTextField="TipoLayout" CssClass="form-control" runat="server">
                        </asp:DropDownList>
                    </div>
                    
                    <div class="clearfix"></div>                                        

                     <div class="col-md-2 col-lg-2">
                        <!-- Rounded switch -->
                        <div>
                            <label class="small">Activa</label>
                        </div>
                        <label class="switch">
                            <asp:CheckBox ID="chkActivo" Checked="true" runat="server" />
                            <span class="slider round"></span>
                        </label>                        
                    </div>

                    <div class="form-group col-md-2 col-lg-3">
                        <!-- Rounded switch -->
                        <div>
                            <label class="small">Usar Cuenta Emergente</label>
                        </div>
                        <label class="switch">
                            <asp:CheckBox ID="chkUsarEmergente" runat="server" />
                            <span class="slider round"></span>
                        </label>                        
                    </div>                   

                    <asp:Panel ID="pnlOtrosBancos" class="form-group col-md-2 col-lg-3" runat="server">
                        <!-- Rounded switch -->
                        <div>
                            <label class="small">Otros Bancos</label>
                        </div>
                        <label class="switch">
                            <asp:CheckBox ID="chkOtrosBancos" runat="server" />
                            <span class="slider round"></span>
                        </label>                        
                    </asp:Panel>

                    <div class="clearfix"></div>

                    <div class="form-group col-sm-12">                        
                        <ajaxtoolKit:TabContainer ID="tcConfiguracion" CssClass="actTab"  runat="server">
                            <ajaxtoolKit:TabPanel ID="tpSituacionLaboral" HeaderText="Situacion Laboral" runat="server">
                                <ContentTemplate>
                                    <asp:Repeater ID="rptSituacionLaboral_Guardar" runat="server">
                                        <ItemTemplate>
                                            <div class="col-md-4" style="padding-left:0px; margin-bottom:4px;">
                                                <asp:HiddenField ID="hdnIdSituacionLaboral" Value='<%# Eval("Valor") %>' runat="server" />
                                                <asp:CheckBox ID="chkSituacionLaboral" CssClass="form-control" style="font-size:85%;" Text='<%# Eval("Descripcion") %>' Checked="true" runat="server" />
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ContentTemplate>
                            </ajaxtoolKit:TabPanel>
                        </ajaxtoolKit:TabContainer>                        
                    </div>

                    <div class="col-sm-12">
                        <h4>Empresas de la Plantilla</h4>
                        <hr style="margin-top:0;" />
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group col-sm-12 input-group">
                             <span class="input-group-addon">
                                RUC - Empresa
                            </span>
                            <asp:DropDownList ID="ddlEmpresa" DataTextField="Descripcion" DataValueField="Valor" CssClass="form-control" runat="server" />                                                    
                        </div>
                    </div>

                    <div class="form-group col-sm-12 col-md-9 col-lg-8 text-right">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <asp:CheckBox ID="chkAgregarCurrent" Checked="true" Text="&nbsp;&nbsp;Agregar Bucket Current" CssClass="small" OnCheckedChanged="chkAgregarCurrent_CheckedChanged" AutoPostBack="true" runat="server" />
                            </span>
                            <asp:TextBox ID="txtPorcentajeCurrent" Text="100" CssClass="form-control" onKeyPress="return EvaluateText('%d', this);" MaxLength="3" OnTextChanged="txtPorcentajeCurrent_TextChanged" AutoPostBack="true" runat="server"></asp:TextBox>
                            <span class="input-group-addon">
                                %
                            </span>
                            <span class="input-group-addon">
                                <asp:CheckBox ID="chkAgregarMoroso" Checked="true" Text="&nbsp;&nbsp;Agregar Bucket Moroso" CssClass="small" OnCheckedChanged="chkAgregarMoroso_CheckedChanged" AutoPostBack="true" runat="server" />
                            </span>
                            <asp:TextBox ID="txtPorcentajeMoroso" Text="100" CssClass="form-control" onKeyPress="return EvaluateText('%d', this);" MaxLength="3" OnTextChanged="txtPorcentajeMoroso_TextChanged" AutoPostBack="true" runat="server"></asp:TextBox>
                            <span class="input-group-addon">
                                %
                            </span>
                        </div>
                    </div>

                    <div class="form-group col-sm-12 col-md-3 col-lg-4 text-right">                        
                        <asp:LinkButton ID="lbntAgregarEmpresa_Guardar" CssClass="btn btn-sm btn-default" data-toggle="tooltip" title="Agregar Empresa"  OnClick="lbntAgregarEmpresa_Guardar_Click" runat="server">
                            <i class="fas fa-plus"></i> Agregar Empresa
                        </asp:LinkButton>
                    </div>

                    <div class="col-sm-12" style="background-color:#F8F8F8; border-radius: 8px; min-height:400px;">
                        <div class="col-sm-12 col-md-3">
                            <asp:GridView ID="gvEmpresa" CssClass="plantilla_GV" Width="100%"
                                OnRowCommand="gvEmpresa_RowCommand"
                                OnRowDataBound="gvEmpresa_RowDataBound"
                                AutoGenerateColumns="false" 
                                ShowFooter="true" 
                                AllowPaging="false" 
                                runat="server">
                                <Columns>
                                    <asp:TemplateField HeaderText="EMPRESAS">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEmpresa" CssClass="lbtnTipoConvenio" CommandName="EditarEmpresa" CommandArgument='<%# Eval("Empresa.IdEmpresa") %>' runat="server">                                                
                                                <div class="TipoConvenioDesc"><%# Eval("Empresa.Empresa") %></div>
                                                <div class="ConvenioDesc"><%# Eval("Empresa.RUC") %></div>                                                
                                            </asp:LinkButton>                                       
                                            <asp:LinkButton ID="lbtnEliminarConvenio" data-toggle="tooltip" title="Eliminar" CssClass="lbtnEliminarConvenio" CommandName="EliminarEmpresa" CommandArgument='<%# Eval("Empresa.IdEmpresa") %>' runat="server">
                                                <i class="far fa-trash-alt hvr-icon-bounce"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <asp:Panel ID="pnlDetalle_EditarEmpresa" class="col-sm-12 col-md-9" style="padding-top:27px;" runat="server">
                            <div class="col-sm-12">
                                 <h4>Configurar Buckets</h4>
                                <hr style="margin-top:0;" />
                                <div class="col-md-6">
                                    <h6><strong>EMPRESA:</strong> <asp:Label ID="lblEmpresa_EditarEmpresa" runat="server"></asp:Label></h6>
                                </div>
                                <div class="col-md-6">
                                    <h6><strong>RUC:</strong> <asp:Label ID="lblRUC_EditarEmpresa" runat="server"></asp:Label></h6>
                                </div>
                            </div>
                            <div class="form-group col-sm-12 input-group">                                                                                        
                                <span class="input-group-addon">
                                    Agregar Bucket
                                </span>
                                <asp:DropDownList ID="ddlBucket_EditarEmpresa" DataValueField="IdBucket" DataTextField="Descripcion" CssClass="form-control" runat="server">
                                </asp:DropDownList>
                                <span class="input-group-addon">
                                    <asp:LinkButton ID="lbtnAgregarBucket_EditarEmpresa" OnClick="lbtnAgregarBucket_EditarEmpresa_Click" data-toggle="tooltip" title="Agregar Bucket" runat="server"><i class="fas fa-plus-square"></i></asp:LinkButton>
                                </span>                                
                            </div>
                            <div class="col-sm-12">
                                <asp:Repeater ID="rptBuckets" OnItemCommand="rptBuckets_ItemCommand" OnItemDataBound="rptBuckets_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnIdBucket" Value='<%# Eval("Bucket.IdBucket") %>' runat="server" />
                                        <div class="col-md-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">                                                    
                                                    PARTIDAS - Bucket <%# Eval("Bucket.Descripcion") %>
                                                    <div class="btn-group pull-right">
                                                        <asp:LinkButton ID="lbtnLimpiarPartidas_EditarEmpresa" data-toggle="tooltip" title="Limpiar Partidas" CommandName="LimpiarPartidas" CommandArgument='<%# Eval("Bucket.IdBucket") %>' runat="server">
                                                            <i class="fas fa-broom" style="color:#428cc3;"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="lbtnAgregarPartida_EditarEmpresa" data-toggle="tooltip" title="Agregar Partida" CommandName="AgregarPartida" CommandArgument='<%# Eval("Bucket.IdBucket") %>' runat="server">
                                                            <i class="fas fa-plus" style="color:#428cc3;"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="lbtnEliminarBucket_EditarEmpresa" data-toggle="tooltip" title="Eliminar Bucket" CommandName="EliminarBucket" CommandArgument='<%# Eval("Bucket.IdBucket") %>' runat="server">
                                                            <i class="fas fa-times" style="color:tomato;"></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="panel-body" style="min-height:85px;">
                                                    <asp:Repeater ID="rptPartidas" runat="server">
                                                    <ItemTemplate>
                                                        <div class="row col-sm-12 col-md-6">                                                            
                                                            <span class="row col-md-2" style="padding:0;">
                                                                <strong><%# Eval("NoPartida") %>.</strong>
                                                            </span>
                                                            <span class="row col-md-8" style="padding:0;">
                                                                <asp:HiddenField ID="hdnNoPartida" Value='<%# Eval("NoPartida") %>' runat="server" />
                                                                <asp:TextBox ID="txtPorcentajePartida" Text='<%# Eval("PorcentajePartida") %>' OnTextChanged="txtPorcentajePartida_TextChanged" AutoPostBack="true" onKeyPress="return EvaluateText('%d', this);" MaxLength="3" CssClass="txt-droid" runat="server"></asp:TextBox>
                                                            </span>
                                                            <span class="row col-md-2" style="padding:0;">
                                                                <strong>%</strong>
                                                            </span>                                                            
                                                        </div>
                                                    </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="form-group col-sm-12 input-group">                                                                                        
                                                        <span class="input-group-addon">
                                                            No. Maximo Cuotas
                                                        </span>
                                                        <asp:DropDownList ID="ddlNoErogacionesMax_EditarEmpresa" AutoPostBack="true" OnSelectedIndexChanged="ddlNoErogacionesMax_EditarEmpresa_SelectedIndexChanged" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="1.00" Text="1"></asp:ListItem>
                                                            <asp:ListItem Value="1.50" Text="1.5"></asp:ListItem>                                                            
                                                            <asp:ListItem Value="2.00" Text="2"></asp:ListItem>
                                                            <asp:ListItem Value="2.50" Text="2.5"></asp:ListItem>                                                            
                                                            <asp:ListItem Value="3.00" Text="3"></asp:ListItem>                                                           
                                                        </asp:DropDownList>                                                                                     
                                                    </div>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            S/
                                                        </span>
                                                        <asp:TextBox ID="txtMontoIndivisible_EditarEmpresa" Text='<%# Eval("MontoIndivisible") %>' AutoPostBack="true" OnTextChanged="txtMontoIndivisible_EditarEmpresa_TextChanged" CssClass="form-control" placeholder="Monto Minimo Partidas" onKeyPress="return EvaluateText('%f', this);" MaxLength="10" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                    </div>

                    <div class="col-sm-12">
                        <hr />
                        <asp:Panel ID="pnlError_Guardar" class="form-group col-sm-12" Visible="false" runat="server">                                
                            <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                <asp:Label ID="lblError_Guardar" runat="server"></asp:Label>
                            </div>
                            <hr />
                        </asp:Panel> 
                        <asp:LinkButton ID="lbtnGuardar" OnClick="lbtnGuardar_Click" CssClass="btn btn-sm btn-success" runat="server">
                           <i class="fas fa-save"></i> Guardar
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbtnCancelar" OnClick="lbtnCancelar_Click" CssClass="btn btn-sm btn-danger" runat="server">
                           <i class="fas fa-arrow-alt-circle-left"></i> Cancelar
                        </asp:LinkButton>
                    </div>                                      
                </asp:Panel>
                <!-- TERMINA PANEL GUARDAR -->
            </ContentTemplate>              
            </asp:UpdatePanel>
        </div>
    </div>
    
    <script type="text/javascript" src="/js/siteCommon.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Activar Tooltips
            activarTooltips();

            $("#<%=ddlEmpresa.ClientID%>").select2({
                language: "es", theme: "bootstrap", dropdownAutoWidth: false,
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            // Activar Tooltips
            activarTooltips();

            $("#<%=ddlEmpresa.ClientID%>").select2({
                language: "es", theme: "bootstrap", dropdownAutoWidth: false,
            });
        });        
    </script> 
</asp:Content>
