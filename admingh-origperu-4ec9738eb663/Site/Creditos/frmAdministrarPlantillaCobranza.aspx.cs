﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using wsSOPF;
using System.Reflection;
using System.IO;
using System.Text;
using SpreadsheetLight;
using Ionic.Zip;

public partial class Site_Creditos_frmAdministrarPlantillaCobranza : System.Web.UI.Page
{
    private enum Vista
    {
        Busqueda,
        Registrar,
        Actualizar
    }

    private List<Bucket> CATBuckets
    {
        get
        {
            if (ViewState[this.UniqueID + "_CATbuckets"] != null)
            {
                return (List<Bucket>)ViewState[this.UniqueID + "_CATbuckets"];
            }
            else
            {                
                return new List<Bucket>();
            }
        }
        set
        {
            ViewState[this.UniqueID + "_CATbuckets"] = value;
        }
    }

    private Plantilla Plantilla
    {
        get
        {
            if (ViewState[this.UniqueID + "_plantilla"] != null)
            {
                return (Plantilla)ViewState[this.UniqueID + "_plantilla"];
            }
            else
            {
                Plantilla p = new Plantilla();
                p.TipoLayout = new TipoLayoutCobro();
                p.Empresas = new List<Plantilla.EmpresaConfiguracion>().ToArray();
                p.IdUsuarioRegistro = int.Parse(Session["UsuarioId"].ToString());

                return p;
            }
        }
        set
        {
            ViewState[this.UniqueID + "_plantilla"] = value;
        }
    }

    private Plantilla.EmpresaConfiguracion EmpresaEditar
    {
        get
        {
            if (ViewState[this.UniqueID + "_empresaEditar"] != null)
            {
                return (Plantilla.EmpresaConfiguracion)ViewState[this.UniqueID + "_empresaEditar"];
            }
            else
            {             
                return new Plantilla.EmpresaConfiguracion();
            }
        }
        set
        {
            ViewState[this.UniqueID + "_empresaEditar"] = value;
        }
    }

    private Vista VistaPagina
    {
        get { return (ViewState[this.UniqueID + "_vistaPagina"] != null) ? (Vista)ViewState[this.UniqueID + "_vistaPagina"] : Vista.Busqueda; }
        set
        {
            ViewState[this.UniqueID + "_vistaPagina"] = value;

            this.lblTitulo.Text = "Administrar Plantillas Cobranza";
            switch (this.VistaPagina)
            {
                case Vista.Busqueda:
                    this.lblTitulo.Text += " - Buscar";
                    pnlBuscar.Visible = true;
                    pnlGuardar.Visible = false;
                    break;
                case Vista.Registrar:
                    this.lblTitulo.Text += " - Registrar";
                    pnlBuscar.Visible = false;
                    pnlGuardar.Visible = true;
                    break;
                case Vista.Actualizar:
                    this.lblTitulo.Text += " - Actualizar";
                    pnlBuscar.Visible = false;
                    pnlGuardar.Visible = true;
                    break;
            }

            upTitulo.Update();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.VistaPagina = Vista.Busqueda;
            CargarCombos_Buscar();
        }
    }


    #region "Buscar"

    protected void lbtnAgregarPlantilla_Click(object sender, EventArgs e)
    {
        pnlExito_Buscar.Visible = false;
        pnlError_Buscar.Visible = false;
        pnlError_Guardar.Visible = false;

        this.VistaPagina = Vista.Registrar;
        this.Plantilla = null;
        this.EmpresaEditar = null;

        pnlDetalle_EditarEmpresa.Visible = false;
        txtNombrePlantilla_Guardar.Text = string.Empty;
        chkActivo.Checked = true;
        chkUsarEmergente.Checked = false;

        chkAgregarCurrent.Checked = true;
        chkAgregarMoroso.Checked = true;
        txtPorcentajeCurrent.Text = "100";
        txtPorcentajeMoroso.Text = "100";
        txtPorcentajeCurrent.Enabled = true;
        txtPorcentajeMoroso.Enabled = true;

        CargarCombos_Guardar();
        CargarBuckets_Guardar();

        gvEmpresa.DataSource = null;
        gvEmpresa.DataBind();
    }

    protected void lbtnBuscar_Click(object sender, EventArgs e)
    {
        pnlExito_Buscar.Visible = false;
        pnlError_Buscar.Visible = false;

        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
            {
                wsSOPF.Plantilla p = new Plantilla();
                p.TipoLayout = new TipoLayoutCobro();

                p.Nombre = txtNombrePlantilla_Buscar.Text.Trim();
                if (int.Parse(ddlActivo_Buscar.SelectedValue.ToString()) >= 0)
                    p.Activo = int.Parse(ddlActivo_Buscar.SelectedValue.ToString()) == 1;
                if (int.Parse(ddlTipoLayout_Buscar.SelectedValue.ToString()) > 0)
                    p.TipoLayout.IdTipoLayoutCobro = int.Parse(ddlTipoLayout_Buscar.SelectedValue.ToString());

                ResultadoOfArrayOfPlantilla1rkJJdIU res = ws.Plantilla_Buscar(p);

                if (res.Codigo > 0)
                    throw new Exception(res.Mensaje);

                gvResultado_Buscar.DataSource = res.ResultObject;
                gvResultado_Buscar.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblError_Buscar.Text = ex.Message;
            pnlError_Buscar.Visible = true;
        }
    }

    protected void gvResultado_Buscar_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        pnlExito_Buscar.Visible = false;
        pnlError_Buscar.Visible = false;
        pnlError_Guardar.Visible = false;

        try
        {
            switch (e.CommandName)
            {
                case "Editar":
                    chkAgregarCurrent.Checked = true;
                    chkAgregarMoroso.Checked = true;
                    txtPorcentajeCurrent.Text = "100";
                    txtPorcentajeMoroso.Text = "100";
                    txtPorcentajeCurrent.Enabled = true;
                    txtPorcentajeMoroso.Enabled = true;

                    Detalle_Plantilla_Cargar(int.Parse(e.CommandArgument.ToString()));
                    break;
            }
        }
        catch (Exception ex)
        {
            lblError_Buscar.Text = ex.Message;
            pnlError_Buscar.Visible = true;
        }
    }

    protected void gvResultado_Buscar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvResultado_Buscar.PageIndex = e.NewPageIndex;
        lbtnBuscar_Click(sender, e);
    }

    private void Detalle_Plantilla_Cargar(int IdPlantilla)
    {
        pnlDetalle_EditarEmpresa.Visible = false;
        CargarCombos_Guardar();
        CargarBuckets_Guardar();

        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {            
            wsSOPF.ResultadoOfPlantilla1rkJJdIU res = ws.Plantilla_Obtener(IdPlantilla);

            if (res.Codigo > 0)
                throw new Exception(res.Mensaje);

            this.Plantilla = res.ResultObject;

            // Cargar datos principales
            txtNombrePlantilla_Guardar.Text = this.Plantilla.Nombre.Trim();
            ddlTipoLayout_Guardar.SelectedValue = this.Plantilla.TipoLayout.IdTipoLayoutCobro.ToString();
            ddlTipoLayout_Guardar_SelectedIndexChanged(null, null);
            chkActivo.Checked = this.Plantilla.Activo.GetValueOrDefault();
            chkOtrosBancos.Checked = this.Plantilla.OtrosBancos.GetValueOrDefault();
            chkUsarEmergente.Checked = this.Plantilla.UsarCuentaEmergente.GetValueOrDefault();

            // Cargar Situaciones Laborales
            foreach (RepeaterItem r in rptSituacionLaboral_Guardar.Items)
            {
                ((CheckBox)r.FindControl("chkSituacionLaboral")).Checked = this.Plantilla.SituacionLaboral.ToList().Any(x => x.IdSituacion == int.Parse(((HiddenField)r.FindControl("hdnIdSituacionLaboral")).Value));
            }

            // Cargar las Empresas
            gvEmpresa.DataSource = this.Plantilla.Empresas;
            gvEmpresa.DataBind();

            this.VistaPagina = Vista.Actualizar;
        }
    }

    #endregion


    #region "Guardar"

    protected void lbtnGuardar_Click(object sender, EventArgs e)
    {
        pnlError_Guardar.Visible = false;

        try
        {
            // Recuperar datos principales de la Plantilla
            Plantilla plantilla = this.Plantilla;

            plantilla.Nombre = txtNombrePlantilla_Guardar.Text.Trim();
            plantilla.TipoLayout = new TipoLayoutCobro() { IdTipoLayoutCobro = int.Parse(ddlTipoLayout_Guardar.SelectedValue) };
            plantilla.Activo = chkActivo.Checked;
            plantilla.UsarCuentaEmergente = chkUsarEmergente.Checked;
            plantilla.OtrosBancos = chkOtrosBancos.Checked;

            // Recuperar Situacion Laboral
            List<wsSOPF.TBCATSituacionLaboral_PE> situacionLaboral = new List<TBCATSituacionLaboral_PE>();
            foreach (RepeaterItem item in rptSituacionLaboral_Guardar.Items)
            {
                if (((CheckBox)item.FindControl("chkSituacionLaboral")).Checked)
                {
                    situacionLaboral.Add(new TBCATSituacionLaboral_PE() { IdSituacion = int.Parse(((HiddenField)item.FindControl("hdnIdSituacionLaboral")).Value) });
                }
            }

            plantilla.SituacionLaboral = situacionLaboral.ToArray();
            this.Plantilla = plantilla;

            // Validar la informacion antes de proceder a Guardar
            ValidarGuardar();

            // Guardar la Plantilla
            using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
            {
                ResultadoOfPlantilla1rkJJdIU res = ws.Plantilla_Guardar(this.Plantilla);

                if (res.Codigo > 0)
                    throw new Exception(res.Mensaje);
            }

            this.VistaPagina = Vista.Busqueda;
            this.EmpresaEditar = null;
            pnlExito_Buscar.Visible = true;
            lblExito_Buscar.Text = string.Format("Se guardo la Plantilla {0} correctamente", this.Plantilla.Nombre);
        }
        catch (Exception ex)
        {
            lblError_Guardar.Text = ex.Message;
            pnlError_Guardar.Visible = true;
        }
    }

    protected void lbtnCancelar_Click(object sender, EventArgs e)
    {        
        this.VistaPagina = Vista.Busqueda;
        this.EmpresaEditar = null;
    }

    protected void ddlTipoLayout_Guardar_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnlOtrosBancos.Visible = !(ddlTipoLayout_Guardar.SelectedItem.Text.ToUpper().Contains("VISANET"));
    }

    protected void chkAgregarCurrent_CheckedChanged(object sender, EventArgs e)
    {
        txtPorcentajeCurrent.Enabled = chkAgregarCurrent.Checked;

        if (!chkAgregarCurrent.Checked)
            txtPorcentajeCurrent.Text = "100";
    }

    protected void chkAgregarMoroso_CheckedChanged(object sender, EventArgs e)
    {
        txtPorcentajeMoroso.Enabled = chkAgregarMoroso.Checked;

        if (!chkAgregarMoroso.Checked)
            txtPorcentajeMoroso.Text = "100";
    }

    protected void txtPorcentajeCurrent_TextChanged(object sender, EventArgs e)
    {
        int porcentaje = 0;

        if (int.TryParse(txtPorcentajeCurrent.Text, out porcentaje))
        {
            if (porcentaje > 100)
                porcentaje = 100;
        }
        else
            porcentaje = 100;

        txtPorcentajeCurrent.Text = porcentaje.ToString();
    }

    protected void txtPorcentajeMoroso_TextChanged(object sender, EventArgs e)
    {
        int porcentaje = 0;

        if (int.TryParse(txtPorcentajeMoroso.Text, out porcentaje))
        {
            if (porcentaje > 100)
                porcentaje = 100;
        }
        else
            porcentaje = 100;

        txtPorcentajeMoroso.Text = porcentaje.ToString();
    }

    protected void lbntAgregarEmpresa_Guardar_Click(object sender, EventArgs e)
    {
        pnlDetalle_EditarEmpresa.Visible = false;

        Plantilla ePlantilla = this.Plantilla;
        List<wsSOPF.Plantilla.EmpresaConfiguracion> configuracionEmpresas = ePlantilla.Empresas.ToList();
        List<int> lstEmpresas = new List<int>();

        lstEmpresas.Add(int.Parse(ddlEmpresa.SelectedValue));

        // Agregar las Empresas Seleccionadas
        foreach (int em in lstEmpresas)
        {
            // Validar que no se hayan agregado previamente
            if (!configuracionEmpresas.Exists(x => x.Empresa.IdEmpresa == em))
            {
                wsSOPF.Plantilla.EmpresaConfiguracion cc = new Plantilla.EmpresaConfiguracion();
                List<Plantilla.EmpresaConfiguracion.BucketConfiguracion> bucketsEmpresa = new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion>();                

                // Agregar Bucket Default
                if (chkAgregarCurrent.Checked)
                {
                    Plantilla.EmpresaConfiguracion.BucketConfiguracion bckCurrentConfig = new Plantilla.EmpresaConfiguracion.BucketConfiguracion();
                    Bucket bckCurrent = this.CATBuckets.Where(b => b.Descripcion.ToLower() == "current").FirstOrDefault();
                    List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion> partidas = new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>();

                    // Agregar primera Partida                    
                    Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion p1 = new Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion();
                    p1.NoPartida = 1;
                    p1.PorcentajePartida = int.Parse(txtPorcentajeCurrent.Text.Trim());
                    partidas.Add(p1);                    

                    // Si la primera Partida es de menos del 100% agregamos una segunda con el porcentaje restante
                    if (p1.PorcentajePartida < 100)
                    {
                        Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion p2 = new Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion();
                        p2.NoPartida = 2;
                        p2.PorcentajePartida = 100 - int.Parse(txtPorcentajeCurrent.Text.Trim());
                        partidas.Add(p2);
                    }

                    bckCurrentConfig.Partidas = partidas.ToArray();
                    bckCurrentConfig.Bucket = bckCurrent;
                    bckCurrentConfig.NoErogacionesMax = 1;
                    bucketsEmpresa.Add(bckCurrentConfig);
                }

                // Agregar Bucket Moroso
                if (chkAgregarMoroso.Checked)
                {
                    Plantilla.EmpresaConfiguracion.BucketConfiguracion bckMorosoConfig = new Plantilla.EmpresaConfiguracion.BucketConfiguracion();
                    Bucket bckMoroso = this.CATBuckets.Where(b => b.Descripcion.ToLower() == "moroso").FirstOrDefault();
                    List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion> partidas = new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>();

                    // Agregar primera Partida
                    Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion p1 = new Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion();
                    p1.NoPartida = 1;
                    p1.PorcentajePartida = int.Parse(txtPorcentajeMoroso.Text.Trim());
                    partidas.Add(p1);

                    // Si la primera Partida es de menos del 100% agregamos una segunda con el porcentaje restante
                    if (p1.PorcentajePartida < 100)
                    {
                        Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion p2 = new Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion();
                        p2.NoPartida = 2;
                        p2.PorcentajePartida = 100 - int.Parse(txtPorcentajeMoroso.Text.Trim());
                        partidas.Add(p2);
                    }

                    bckMorosoConfig.Partidas = partidas.ToArray();
                    bckMorosoConfig.Bucket = bckMoroso;
                    bckMorosoConfig.NoErogacionesMax = 1;
                    bucketsEmpresa.Add(bckMorosoConfig);
                }

                cc.Empresa = new TB_CATEmpresa();
                cc.Empresa.IdEmpresa = em;

                cc.Buckets = bucketsEmpresa.ToArray();

                CargarInfoEmpresa(ref cc);
                configuracionEmpresas.Add(cc);
            }
        }

        // Deseleccionar la empresa a editar
        this.EmpresaEditar = null;

        ePlantilla.Empresas = configuracionEmpresas.ToArray();
        this.Plantilla = ePlantilla;
        gvEmpresa.DataSource = configuracionEmpresas;
        gvEmpresa.DataBind();
    }
    
    protected void gvEmpresa_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Plantilla ePlantilla = this.Plantilla;
        List<wsSOPF.Plantilla.EmpresaConfiguracion> configuracionEmpresas = ePlantilla.Empresas.ToList();

        switch (e.CommandName)
        {
            case "EditarEmpresa":
                wsSOPF.Plantilla.EmpresaConfiguracion empresaEditar = configuracionEmpresas.FirstOrDefault(x => x.Empresa.IdEmpresa == int.Parse(e.CommandArgument.ToString()));

                lblEmpresa_EditarEmpresa.Text = empresaEditar.Empresa.Empresa.Trim();
                lblRUC_EditarEmpresa.Text = empresaEditar.Empresa.RUC.Trim();

                this.EmpresaEditar = empresaEditar;
                EnlazarBuckets();

                gvEmpresa.DataSource = configuracionEmpresas;
                gvEmpresa.DataBind();                

                pnlDetalle_EditarEmpresa.Visible = true;
                ddlBucket_EditarEmpresa.SelectedIndex = -1;
                break;
            case "EliminarEmpresa":
                configuracionEmpresas.RemoveAll(x => x.Empresa.IdEmpresa == int.Parse(e.CommandArgument.ToString()));

                // Si la Empresa que se elimina es el que esta en Edicion, se ocultan los buckets del DOM
                if (this.EmpresaEditar.Empresa != null && this.EmpresaEditar.Empresa.IdEmpresa == int.Parse(e.CommandArgument.ToString()))                
                    pnlDetalle_EditarEmpresa.Visible = false;                    
                

                ePlantilla.Empresas = configuracionEmpresas.ToArray();
                this.Plantilla = ePlantilla;

                gvEmpresa.DataSource = configuracionEmpresas;
                gvEmpresa.DataBind();
                break;
        }
    }

    protected void gvEmpresa_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (this.EmpresaEditar.Empresa != null)
            {
                if (((wsSOPF.Plantilla.EmpresaConfiguracion)e.Row.DataItem).Empresa.IdEmpresa == this.EmpresaEditar.Empresa.IdEmpresa)
                {
                    ((LinkButton)e.Row.FindControl("lbtnEmpresa")).CssClass += " lbtnTipoConvenio_Selected";
                }
            }
        }
    }

    protected void lbtnAgregarBucket_EditarEmpresa_Click(object sender, EventArgs e)
    {
        Plantilla ePlantilla = this.Plantilla;
        List<wsSOPF.Plantilla.EmpresaConfiguracion> configuracionConvenios = ePlantilla.Empresas.ToList();
        wsSOPF.Plantilla.EmpresaConfiguracion convenioEditar = this.EmpresaEditar;
        wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion nuevoBucket = null; 
        
        List<wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion> Buckets = (convenioEditar.Buckets != null) ? convenioEditar.Buckets.ToList() : new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion>();

        // Inicializar si no hay buckets
        if (convenioEditar.Buckets == null)
            convenioEditar.Buckets = new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion>().ToArray();
        else
            nuevoBucket = convenioEditar.Buckets.ToList().FirstOrDefault(x => x.Bucket.IdBucket == int.Parse(ddlBucket_EditarEmpresa.SelectedValue));

        // Si es un bucket nuevo
        if (nuevoBucket == null)
        {
            nuevoBucket = new Plantilla.EmpresaConfiguracion.BucketConfiguracion();
            nuevoBucket.Bucket = new Bucket();
            nuevoBucket.Partidas = new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>().ToArray();
            nuevoBucket.Bucket.IdBucket = int.Parse(ddlBucket_EditarEmpresa.SelectedValue);
            nuevoBucket.Bucket.Descripcion = ddlBucket_EditarEmpresa.SelectedItem.Text;
            nuevoBucket.NoErogacionesMax = 1;

            Buckets.Add(nuevoBucket);
            convenioEditar.Buckets = Buckets.ToArray();
        }
        
        this.EmpresaEditar = convenioEditar;
        this.Plantilla.Empresas[this.Plantilla.Empresas.ToList().FindIndex(x => x.Empresa.IdEmpresa == this.EmpresaEditar.Empresa.IdEmpresa)] = this.EmpresaEditar;

        EnlazarBuckets();
    }

    protected void ddlNoErogacionesMax_EditarEmpresa_SelectedIndexChanged(object sender, EventArgs e)
    {         
        int IdBucket = int.Parse(((HiddenField)((DropDownList)sender).Parent.FindControl("hdnIdBucket")).Value);

        wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion Bucket = this.EmpresaEditar.Buckets.ToList().FirstOrDefault(x => x.Bucket.IdBucket == IdBucket);

        // Asignar el Valor capturado por el Usuario
        Bucket.NoErogacionesMax = decimal.Parse(((DropDownList)sender).SelectedValue.Trim());


        this.EmpresaEditar.Buckets[this.EmpresaEditar.Buckets.ToList().FindIndex(x => x.Bucket.IdBucket == Bucket.Bucket.IdBucket)] = Bucket;

        this.Plantilla.Empresas[this.Plantilla.Empresas.ToList().FindIndex(x => x.Empresa.IdEmpresa == this.EmpresaEditar.Empresa.IdEmpresa)] = this.EmpresaEditar;

        EnlazarBuckets();
    }

    protected void txtMontoIndivisible_EditarEmpresa_TextChanged(object sender, EventArgs e)
    {
        decimal montoIndivisible = 0;
        int IdBucket = int.Parse(((HiddenField)((TextBox)sender).Parent.FindControl("hdnIdBucket")).Value);

        wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion Bucket = this.EmpresaEditar.Buckets.ToList().FirstOrDefault(x => x.Bucket.IdBucket == IdBucket);
        decimal.TryParse(((TextBox)sender).Text.Trim(), out montoIndivisible);

        if (montoIndivisible > 0)
            Bucket.MontoIndivisible = montoIndivisible;
        else
            Bucket.MontoIndivisible = null;

        this.EmpresaEditar.Buckets[this.EmpresaEditar.Buckets.ToList().FindIndex(x => x.Bucket.IdBucket == Bucket.Bucket.IdBucket)] = Bucket;

        this.Plantilla.Empresas[this.Plantilla.Empresas.ToList().FindIndex(x => x.Empresa.IdEmpresa == this.EmpresaEditar.Empresa.IdEmpresa)] = this.EmpresaEditar;

        EnlazarBuckets();
    }

    protected void rptBuckets_ItemCommand(object source, RepeaterCommandEventArgs e)
    {        
        wsSOPF.Plantilla.EmpresaConfiguracion empresaEditar = this.EmpresaEditar;
        List<wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion> buckets = this.Plantilla.Empresas[this.Plantilla.Empresas.ToList().FindIndex(x => x.Empresa.IdEmpresa == empresaEditar.Empresa.IdEmpresa)].Buckets.ToList();
        wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion bucket = buckets[buckets.ToList().FindIndex(x => x.Bucket.IdBucket == int.Parse(e.CommandArgument.ToString()))];

        switch (e.CommandName)
        {
            case "LimpiarPartidas":
                // Se reinicia el Monto Indivisible y las Partidas
                bucket.MontoIndivisible = null;
                bucket.Partidas = new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>().ToArray();

                this.EmpresaEditar.Buckets[this.EmpresaEditar.Buckets.ToList().FindIndex(x => x.Bucket.IdBucket == bucket.Bucket.IdBucket)] = bucket;
                this.Plantilla.Empresas[this.Plantilla.Empresas.ToList().FindIndex(x => x.Empresa.IdEmpresa == this.EmpresaEditar.Empresa.IdEmpresa)] = this.EmpresaEditar;

                EnlazarBuckets();
                break;
            case "AgregarPartida":
                // Recuperar las partidas actuales
                bucket.Partidas = (bucket.Partidas == null) ? new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>().ToArray() : bucket.Partidas;

                // Crear la nueva partida con el porcentaje faltante para llegar al 100%
                Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion partida = new Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion();
                partida.NoPartida = bucket.Partidas.Count() + 1;
                partida.PorcentajePartida = 100 - bucket.Partidas.Select(x => x.PorcentajePartida).Sum();

                // Se agrega la partida, siempre y cuando no tenga porcentaje de 0%
                List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion> partidas = bucket.Partidas.ToList();
                if (partida.PorcentajePartida > 0)
                    partidas.Add(partida);

                bucket.Partidas = partidas.ToArray();

                this.EmpresaEditar.Buckets[this.EmpresaEditar.Buckets.ToList().FindIndex(x => x.Bucket.IdBucket == bucket.Bucket.IdBucket)] = bucket;
                this.Plantilla.Empresas[this.Plantilla.Empresas.ToList().FindIndex(x => x.Empresa.IdEmpresa == this.EmpresaEditar.Empresa.IdEmpresa)] = this.EmpresaEditar;

                EnlazarBuckets();
                break;
            case "EliminarBucket":                
                buckets = buckets.Where(x => x.Bucket.IdBucket != bucket.Bucket.IdBucket).ToList();
                empresaEditar.Buckets = buckets.ToArray();

                this.EmpresaEditar = empresaEditar;
                this.Plantilla.Empresas[this.Plantilla.Empresas.ToList().FindIndex(x => x.Empresa.IdEmpresa == this.EmpresaEditar.Empresa.IdEmpresa)] = this.EmpresaEditar;

                EnlazarBuckets();
                break;
        }
    }

    protected void rptBuckets_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion[] bckPartidas = ((wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion)e.Item.DataItem).Partidas;

        bckPartidas = (bckPartidas) ?? new List<Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion>().ToArray();

        // Seleccionar el item de ddlNoErogacionesMax_EditarEmpresa
        ((DropDownList)e.Item.FindControl("ddlNoErogacionesMax_EditarEmpresa")).SelectedValue = ((wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion)e.Item.DataItem).NoErogacionesMax.ToString();

        // Habilitar/Deshabilitar txtMontoIndivisible, se habilita cuando haya al menos 2 partidas
        ((TextBox)e.Item.FindControl("txtMontoIndivisible_EditarEmpresa")).Enabled = bckPartidas.Count() > 1;        


        // Ocultar el boton para agregar partidas cuando se llegue al maximo de 4        
        ((LinkButton)e.Item.FindControl("lbtnAgregarPartida_EditarEmpresa")).Visible = bckPartidas.Count() < 4;

        ((Repeater)e.Item.FindControl("rptPartidas")).DataSource = bckPartidas;
        ((Repeater)e.Item.FindControl("rptPartidas")).DataBind();
    }

    protected void txtPorcentajePartida_TextChanged(object sender, EventArgs e)
    {
        // Recuperar los Hidden locales
        int porcentajePartida = 0;
        int IdBucket = int.Parse(((HiddenField)((TextBox)sender).Parent.Parent.Parent.FindControl("hdnIdBucket")).Value);
        int NoPartida = int.Parse(((HiddenField)((TextBox)sender).Parent.FindControl("hdnNoPartida")).Value);

        wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion Bucket = this.EmpresaEditar.Buckets.ToList().FirstOrDefault(x => x.Bucket.IdBucket == IdBucket);
        wsSOPF.Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion Partida = Bucket.Partidas[Bucket.Partidas.ToList().FindIndex(x => x.NoPartida == NoPartida)];

        // Calcular el porcentaje de la Partida para que la SUMA de las partidas no exceda el 100%
        int.TryParse(((TextBox)sender).Text.Trim(), out porcentajePartida);

        int TotalPorcentaje = Bucket.Partidas.Where(x => x.NoPartida != NoPartida).Select(x => x.PorcentajePartida).Sum() + porcentajePartida;

        Partida.PorcentajePartida = (TotalPorcentaje > 100) ? porcentajePartida - (TotalPorcentaje - 100) : porcentajePartida;

        // Si el porcentaje es mayor a 0 se agrega, sino se elimina la partida
        if (Partida.PorcentajePartida > 0)
            Bucket.Partidas[Bucket.Partidas.ToList().FindIndex(x => x.NoPartida == NoPartida)] = Partida;
        else
            Bucket.Partidas = Bucket.Partidas.Where(x => x.NoPartida != NoPartida).ToArray();

        // Reenumerar las partidas segun su orden
        int i = 1;        
        foreach (Plantilla.EmpresaConfiguracion.BucketConfiguracion.PartidaConfiguracion p in Bucket.Partidas.OrderBy(x => x.NoPartida))
        {
            p.NoPartida = i;            
            i++;
        }        

        // Se elimina el MontoIndivisible si son menos de 2 Partidas
        Bucket.MontoIndivisible = (Bucket.Partidas.Length < 2) ? null : Bucket.MontoIndivisible;

        this.EmpresaEditar.Buckets[this.EmpresaEditar.Buckets.ToList().FindIndex(x => x.Bucket.IdBucket == Bucket.Bucket.IdBucket)] = Bucket;

        this.Plantilla.Empresas[this.Plantilla.Empresas.ToList().FindIndex(x => x.Empresa.IdEmpresa== this.EmpresaEditar.Empresa.IdEmpresa)] = this.EmpresaEditar;

        EnlazarBuckets();
    }

    private void CargarInfoEmpresa(ref wsSOPF.Plantilla.EmpresaConfiguracion cc)
    {
        wsSOPF.TB_CATEmpresa Empresa;        

        // Obtener Datos Empresa
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            Empresa = wsCatalogo.ObtenerEmpresa(cc.Empresa.IdEmpresa);

            cc.Empresa.IdEmpresa = Empresa.IdEmpresa;
            cc.Empresa.Empresa = Empresa.Empresa;
            cc.Empresa.RUC = Empresa.RUC;
        }
    }

    private void EnlazarBuckets()
    {
        if (this.EmpresaEditar.Buckets != null)        
            rptBuckets.DataSource = this.EmpresaEditar.Buckets.OrderBy(x => x.Bucket.IdBucket);                    
        else
            rptBuckets.DataSource = null;

        rptBuckets.DataBind();
    }

    #endregion


    #region "Metodos"

    private void ValidarGuardar()
    {
        if (this.Plantilla.Nombre.Trim() == string.Empty)
            throw new Exception("Capture el nombre de la Plantilla");

        if (this.Plantilla.SituacionLaboral.Length == 0)
            throw new Exception("Seleccione al menos una Situacion Laboral");

        if (this.Plantilla.Empresas.Length == 0)
            throw new Exception("Agregue al menos 1 Empresa a la Plantilla");
        else
        {
            // Validar las Empresas de la Plantilla
            foreach (Plantilla.EmpresaConfiguracion c in this.Plantilla.Empresas)
            {
                if (c.Buckets.Length == 0)
                    throw new Exception(string.Format("Agregue al menos 1 Bucket a la Empresa {0}", c.Empresa.Empresa));
                else
                {
                    // Validar los Buckets de la Empresa
                    foreach (Plantilla.EmpresaConfiguracion.BucketConfiguracion b in c.Buckets)
                    {                                                
                        if (b.Partidas.Length == 0)
                            throw new Exception(string.Format("Agregue al menos 1 Partida al Bucket {1} de la Empresa {0}", c.Empresa.Empresa, b.Bucket.Descripcion));
                        else if(b.Partidas.Select(x => x.PorcentajePartida).Sum() == 0)
                            throw new Exception(string.Format("Las Partida del Bucket {1} de la Empresa {0}, no pueden ser del 0%", c.Empresa.Empresa, b.Bucket.Descripcion));
                    }
                }
            }
        }
    }

    private void CargarCombos_Buscar()
    {       
        // Cargar Combo Tipo Layout
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            List<wsSOPF.TipoLayoutCobro> tipoLayoutActivos = ws.TipoLayoutCobro_ObtenerActivos().ToList();

            ddlTipoLayout_Buscar.DataSource = tipoLayoutActivos;
            ddlTipoLayout_Buscar.DataBind();
        }        
    }

    private void CargarCombos_Guardar()
    {
        Combo[] Empresa_PE;

        // Cargar Combo Empresa
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {            
            Empresa_PE = wsCatalogo.ObtenerCombo_Empresa_PE();
        }
        
        if (Empresa_PE.Length > 0)
        {
            ddlEmpresa.DataSource = Empresa_PE;                        
            ddlEmpresa.DataBind();            
        }

        // Cargar Combo Tipo Layout    
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            List<wsSOPF.TipoLayoutCobro> tipoLayoutActivos = ws.TipoLayoutCobro_ObtenerActivos().ToList();

            ddlTipoLayout_Guardar.DataSource = tipoLayoutActivos;
            ddlTipoLayout_Guardar.DataBind();

            ddlTipoLayout_Guardar_SelectedIndexChanged(null, null);
        }

        // Cargar Checks Situacion Laboral
        using (wsSOPF.CatalogoClient ws = new CatalogoClient())
        {
            rptSituacionLaboral_Guardar.DataSource = ws.ObtenerCombo_SituacionLaboral_PE();
            rptSituacionLaboral_Guardar.DataBind();
        }
    }

    private void CargarBuckets_Guardar()
    {
        List<Bucket> Buckets;

        // Cargar Buckets
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            Buckets = ws.Plantilla_ListarBucketsLayout().ToList();
        }

        this.CATBuckets = Buckets;

        ddlBucket_EditarEmpresa.DataSource = this.CATBuckets;
        ddlBucket_EditarEmpresa.DataBind();
    }

    #endregion            
}