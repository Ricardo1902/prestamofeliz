﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmAplicacionMasivaPagos.aspx.cs" Inherits="Site_Creditos_frmAplicacionMasivaPagos" MasterPageFile="~/Site.master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolKit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">     
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Aplicación de pagos</h3>
        </div>
        <div class="panel-body">
            <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
                <ProgressTemplate>                 
                    <div id="loader-background"></div>
                    <div id="loader-content"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>                               
                <ajaxtoolKit:TabContainer ID="tcMain" CssClass="actTab" runat="server">                    
                    <ajaxtoolKit:TabPanel HeaderText="Consulta Pagos" runat="server">
                        <ContentTemplate>
                            <div class="row">                                
                                <div class="form-group col-md-3">
                                    <label for="ddlTipoConvenio" class="form-control-label small">Empresa</label>
                                    <div>
                                        <asp:DropDownList ID="ddlTipoConvenio" DataValueField="IdTipoConvenio" DataTextField="TipoConvenio" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlTipoConvenio_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="ddlProducto" class="form-control-label small">Producto</label>
                                    <div>
                                        <asp:DropDownList ID="ddlConvenio" DataValueField="Valor" DataTextField="Descripcion" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlConvenio_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="txtPlazo" class="form-control-label small">Plazo</label>
                                    <div>
                                        <asp:DropDownList ID="ddlProducto" AutoPostBack="true" runat="server" CssClass="form-control" DataTextField="Producto" DataValueField="IdProducto">
                                        </asp:DropDownList>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="form-group col-md-3">
                                    <label class="form-control-label small">Id Solicitud</label>
                                    <asp:textbox id="txtIdSolicitud" onKeyPress="return EvaluateText('%f', this);" runat="server" CssClass="form-control"></asp:textbox>
                                </div>            

                                <div class="form-group col-md-3">           
                                    <label class="form-control-label small">Fecha Cobro (Desde)</label>                                
                                    <div class="form-group">                    
                                        <div class="input-group">
                                            <asp:TextBox ID="txtFechaPagoDesde_Pagos" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                            <span class="input-group-addon">
                                                <asp:LinkButton ID="btnSelectFechaPagoDesde_Pagos" runat="server">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </asp:LinkButton>
                                            </span>
                                        </div>                    
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                                ErrorMessage="Formato Incorrecto"
                                                ControlToValidate="txtFechaPagoDesde_Pagos"
                                                ForeColor="Red"
                                                ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                        <ajaxtoolKit:CalendarExtender ID="ceFechaPagoDesde" Format="dd/MM/yyyy" TargetControlID="txtFechaPagoDesde_Pagos" PopupButtonID="btnSelectFechaPagoDesde_Pagos" runat="server"></ajaxtoolKit:CalendarExtender>
                                    </div> 
                                </div>

                                <div class="form-group col-md-3">
                                    <label class="form-control-label small">Fecha Cobro (Hasta)</label>                                
                                    <div class="form-group">                    
                                        <div class="input-group">
                                            <asp:TextBox ID="txtFechaPagoHasta_Pagos" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                            <span class="input-group-addon">
                                                <asp:LinkButton ID="btnSelectFechaPagoHasta_Pagos" runat="server">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </asp:LinkButton>
                                            </span>
                                        </div>                    
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ErrorMessage="Formato Incorrecto"
                                                ControlToValidate="txtFechaPagoHasta_Pagos"
                                                ForeColor="Red"
                                                ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                        <ajaxtoolKit:CalendarExtender ID="ceFechaPagoHasta" Format="dd/MM/yyyy" TargetControlID="txtFechaPagoHasta_Pagos" PopupButtonID="btnSelectFechaPagoHasta_Pagos" runat="server"></ajaxtoolKit:CalendarExtender>
                                    </div>       
                                </div>            
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="form-control-label small">Canal Pago</label>
                                    <asp:DropDownList ID="ddlCanalPago" DataValueField="IdCanalPago" DataTextField="Nombre" CssClass="form-control" AppendDataBoundItems="true" runat="server">
                                        <asp:ListItem Selected="True" Value="0" Text="Todos"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>  

                                <div class="form-group col-md-9">  
                                    <label class="form-control-label small">Estatus</label>
                                    <div class="form-group">
                                        <asp:Panel ID="pnlEstatus" runat="server">
                                            <asp:CheckBox ID="chkEstRegistrado" runat="server" Text="Registrado"/>                                
                                            <asp:CheckBox ID="chkEstParcialAplicado" runat="server" Text="Parcial Aplicado"/>
                                            <asp:CheckBox ID="chkEstAplicado" runat="server" Text="Aplicado"/>
                                            <asp:CheckBox ID="chkEstCancelado" runat="server" Text="Cancelado"/>
                                            <asp:CheckBox ID="chkEstDevuelto" runat="server" Text="Devuelto"/>
                                            <asp:CheckBox ID="chkEstLiquidado" runat="server" Text="Liquidación"/>
                                        </asp:Panel> 
                                        <asp:Label ID="lblEstatus" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">     
                                <div class="col-md-3">                
                                    <label class="form-control-label small">Fecha Registro (Desde)</label>                                
                                    <div class="form-group">                    
                                        <div class="input-group">
                                            <asp:TextBox ID="txtFechaRegistroDesde_Pagos" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                            <span class="input-group-addon">
                                                <asp:LinkButton ID="btnSelectFechaRegistroDesde_Pagos" runat="server">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </asp:LinkButton>
                                            </span>
                                        </div>                    
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                ErrorMessage="Formato Incorrecto"
                                                ControlToValidate="txtFechaRegistroDesde_Pagos"
                                                ForeColor="Red"
                                                ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                        <ajaxtoolKit:CalendarExtender ID="CalendarExtender1" Format="dd/MM/yyyy" TargetControlID="txtFechaRegistroDesde_Pagos" PopupButtonID="btnSelectFechaRegistroDesde_Pagos" runat="server"></ajaxtoolKit:CalendarExtender>
                                    </div> 
                                </div>

                                <div class="col-md-3"> 
                                    <label class="form-control-label small">Fecha Registro (Hasta)</label>                                
                                    <div class="form-group">                    
                                        <div class="input-group">
                                            <asp:TextBox ID="txtFechaRegistroHasta_Pagos" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                            <span class="input-group-addon">
                                                <asp:LinkButton ID="btnSelectFechaRegistroHasta_Pagos" runat="server">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </asp:LinkButton>
                                            </span>
                                        </div>                    
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ErrorMessage="Formato Incorrecto"
                                                ControlToValidate="txtFechaRegistroHasta_Pagos"
                                                ForeColor="Red"
                                                ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                        <ajaxtoolKit:CalendarExtender ID="CalendarExtender2" Format="dd/MM/yyyy" TargetControlID="txtFechaRegistroHasta_Pagos" PopupButtonID="btnSelectFechaRegistroHasta_Pagos" runat="server"></ajaxtoolKit:CalendarExtender>
                                    </div>       
                                </div>

                                <div class="col-md-3">                
                                    <label class="form-control-label small">Fecha Aplicación (Desde)</label>                                
                                    <div class="form-group">                    
                                        <div class="input-group">
                                            <asp:TextBox ID="txtFechaAplicacionDesde_Pagos" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                            <span class="input-group-addon">
                                                <asp:LinkButton ID="btnSelectFechaAplicacionDesde_Pagos" runat="server">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </asp:LinkButton>
                                            </span>
                                        </div>                    
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                ErrorMessage="Formato Incorrecto"
                                                ControlToValidate="txtFechaRegistroDesde_Pagos"
                                                ForeColor="Red"
                                                ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                        <ajaxtoolKit:CalendarExtender ID="CalendarExtender3" Format="dd/MM/yyyy" TargetControlID="txtFechaAplicacionDesde_Pagos" PopupButtonID="btnSelectFechaAplicacionDesde_Pagos" runat="server"></ajaxtoolKit:CalendarExtender>
                                    </div> 
                                </div>

                                <div class="col-md-3"> 
                                    <label class="form-control-label small">Fecha Aplicación (Hasta)</label>                                
                                    <div class="form-group">                    
                                        <div class="input-group">
                                            <asp:TextBox ID="txtFechaAplicacionHasta_Pagos" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                            <span class="input-group-addon">
                                                <asp:LinkButton ID="btnSelectFechaAplicacionHasta_Pagos" runat="server">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </asp:LinkButton>
                                            </span>
                                        </div>                    
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                ErrorMessage="Formato Incorrecto"
                                                ControlToValidate="txtFechaAplicacionHasta_Pagos"
                                                ForeColor="Red"
                                                ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                        <ajaxtoolKit:CalendarExtender ID="CalendarExtender4" Format="dd/MM/yyyy" TargetControlID="txtFechaAplicacionHasta_Pagos" PopupButtonID="btnSelectFechaAplicacionHasta_Pagos" runat="server"></ajaxtoolKit:CalendarExtender>
                                    </div>       
                                </div>
                            </div>

                            <div class="row"> 
                                <div class="col-sm-12">            
                                    <asp:Button ID="btnBuscar" Text="Buscar" CssClass="btn btn-primary btn-sm" runat="server" OnClick="btnBuscar_Click"/>
                                </div>            
                            </div>

                            <asp:Panel ID="pnlError_FltPagosDom" class="form-group col-sm-12" Visible="false" runat="server">                                
                                <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                    <asp:Label ID="lblError_FltPagosDom" runat="server"></asp:Label>
                                </div>
                            </asp:Panel>
                            <br /> 
                            <div class="row">
                                <div id="divErrorPago" runat="server" class="alert alert-danger" role="alert" visible="false">
                                    <asp:Label ID="lblErrorPago" runat="server" Text="Favor de subir correctamente los documentos necesarios"></asp:Label>
                                </div>                                
                            </div>
                            <ajaxtoolKit:TabContainer ID="tcResultado" CssClass="actTab" runat="server" Visible="false">
                                <ajaxtoolKit:TabPanel ID="tabResultado" HeaderText="Pagos" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group col-sm-12">   
                                            <asp:Panel ID="pnlPagos" runat="server" Visible="false">                                                                                                            
                                                <div class="row"> 
                                                    <div class="col-md-6">                                        
                                                        <asp:CheckBox ID="chkTodos" runat="server" Text="Seleccionar todos" OnCheckedChanged="chkTodos_CheckedChanged" AutoPostBack="True"/>
                                                        <asp:label ID="lblResultado" runat="server"></asp:label>
                                                    </div>                                        
                                                    <div class="col-md-6 text-right">
                                                        <asp:Panel ID="pnlActionButtons" runat="server">
                                                            <asp:label ID="lblAction" runat="server">Iniciar:</asp:label>
                                                            <asp:Button ID="btnAplicar" Text="Aplicación" CssClass="btn btn-success btn-sm" runat="server" OnClick="Procesar"/>
                                                            <asp:Button ID="btnDesaplicar" Text="Desaplicación" CssClass="btn btn-danger btn-sm" runat="server" OnClick="Procesar"/>
                                                        </asp:Panel>                                        
                                                    </div>                                        
                                                </div>                                                                                                
                                                <hr />
                                                <div class="col-sm-12">
                                                    <div style="height:auto; width: auto; overflow-x: auto;">                                            
                                                        <asp:GridView ID="gvPagos" runat="server" Width="100%" AutoGenerateColumns="False"
                                                            HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                                            PageSize="1000" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                            DataKeyNames="idPago, idSolicitud, montoCobrado, montoAplicado, montoPorAplicar, canalPagoDesc, estatusDesc, fechaPago, fechaRegistro, fechaAplicacion, estatusClave" Style="overflow-x:auto;" OnRowDataBound="gvPagos_RowDataBound" OnRowCommand="gvPagos_RowCommand">                                        
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="Sel.">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hdnMontoAplicado" runat="server" Value='<%# Eval("montoAplicado") %>' />
                                                                        <asp:HiddenField ID="hdnMontoPorAplicar" runat="server" Value='<%# Eval("montoPorAplicar") %>' />
                                                                        <asp:CheckBox ID="chkId" runat="server" AutoPostBack="true" OnCheckedChanged="chkId_CheckedChanged"/>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>">
                                                                    <ItemTemplate>
                                                                        <div class="dropdown">                                                        
                                                                            <button class="btn btn-xs btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">                                                                
                                                                                <i class="fas fa-bars" style="color: #888888;"></i>
                                                                            <span class="caret"></span>
                                                                            </button>
                                                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                                                <li>                                                                                                                                    
                                                                                    <asp:LinkButton ID="lbtnAplicarPago" CommandName="AplicarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                        Aplicar
                                                                                    </asp:LinkButton>                                                                
                                                                                </li>
                                                                                <li>                                                                                                                                    
                                                                                    <asp:LinkButton ID="lbtnAplicarPagoUnico" CommandName="AplicarPagoUnico" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                        Aplicar Unico
                                                                                    </asp:LinkButton>                                                                
                                                                                </li>                                                                
                                                                                <li>
                                                                                    <asp:LinkButton ID="lbtnDesaplicarPago" CommandName="DesaplicarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                        Desaplicar
                                                                                    </asp:LinkButton>                                                                
                                                                                </li>
                                                                                <li>
                                                                                    <asp:LinkButton ID="lbtnCancelarPago" CommandName="CancelarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                        Cancelar
                                                                                    </asp:LinkButton>                                                                
                                                                                </li>
                                                                                <li>
                                                                                    <asp:LinkButton ID="lbtnDevolverPago" CommandName="DevolverPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                        Devolver
                                                                                    </asp:LinkButton>                                                                
                                                                                </li>
                                                                                <li>
                                                                                    <asp:LinkButton ID="lbtnLiquidarPago" CommandName="LiquidarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                        Liquidación
                                                                                    </asp:LinkButton>                                                                
                                                                                </li>
                                                                                <li>
                                                                                    <asp:LinkButton ID="lbtnRegistrarPago" CommandName="RegistrarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                        Registrar
                                                                                    </asp:LinkButton>                                                                
                                                                                </li>
                                                                            </ul>
                                                                        </div>                                                        
                                                                    </ItemTemplate>
                                                                </asp:TemplateField> 
                                                                <asp:BoundField HeaderText="Id" DataField="idPago" />                            
                                                                <asp:BoundField HeaderText="Solicitud" DataField="idSolicitud" />                            
                                                                <asp:BoundField HeaderText="Monto Cobrado" DataField="montoCobrado" DataFormatString="{0:c2}" />
                                                                <asp:BoundField HeaderText="Monto Aplicado" DataField="montoAplicado" DataFormatString="{0:c2}" />                            
                                                                <asp:BoundField HeaderText="Monto Por Aplicar" DataField="montoPorAplicar" DataFormatString="{0:c2}" />                            
                                                                <asp:BoundField HeaderText="Canal de Pago" DataField="canalPagoDesc" />                            
                                                                <asp:BoundField HeaderText="Estatus" DataField="estatusDesc" />
                                                                <asp:BoundField HeaderText="Fecha Cobro" DataField="fechaPago" />
                                                                <asp:BoundField HeaderText="Fecha Registro" DataField="fechaRegistro" />
                                                                <asp:BoundField HeaderText="Fecha Aplicacion" DataField="fechaAplicacion" />
                                                            </Columns>
                                                            <PagerStyle CssClass="pagination-ty warning" /> 
                                                            <EmptyDataTemplate>
                                                                No se encontraron Pagos con los filtros seleccionados
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </div>                                                                     
                                            </asp:Panel>                                
                                        </div>
                                    </ContentTemplate>                                                                            
                                </ajaxtoolKit:TabPanel>
                                <ajaxtoolKit:TabPanel ID="tabProceso" HeaderText="Proceso" runat="server" Visible="false">
                                    <ContentTemplate>
                                        <div class="form-group col-sm-12">   
                                            <asp:Panel ID="pnlPagosProceso" runat="server">                                    
                                                <br />
                                                <div class="row"> 
                                                    <div class="col-md-4">                                        
                                                        <asp:Label ID="lblProcesar" runat="server"></asp:Label>
                                                    </div>                                      
                                                    <div class="col-md-8 text-right">                                            
                                                        <asp:CheckBox ID="chkAplicarUnico" runat="server" Text="Aplicar único" AutoPostBack="false" Visible="false"/>
                                                        <asp:Button ID="btnCancelarProceso" Text="Cancelar" CssClass="btn btn-danger btn-sm" runat="server" OnClick="btnCancelarProceso_Click"/>                                            
                                                        <asp:Button ID="btnProcesar" Text="Aplicar" CssClass="btn btn-success btn-sm" runat="server" OnClick="btnProcesar_Click"/>                                                                                        
                                                    </div>
                                                </div> 
                                                <br />
                                                <div class="row">
                                                    <div id="divError" runat="server" class="alert alert-danger" role="alert" visible="false">
                                                        <asp:Label ID="lblError" runat="server" Text="Favor de subir correctamente los documentos necesarios"></asp:Label>
                                                    </div>                            
                                                    <div id="divCorrecto" runat="server" class="alert alert-success" role="alert" visible="false">
                                                        <asp:Button ID="btnCancelarConfirmacion" Text="Cancelar" CssClass="btn btn-warning btn-sm" runat="server" OnClick="btnCancelarConfirmacion_Click"/>
                                                        <asp:Button ID="btnConfirmar" Text="Continuar" CssClass="btn btn-success btn-sm" runat="server" OnClick="btnConfirmar_Click"/>
                                                        <asp:Label ID="lblCorrecto" runat="server" Text="Favor de subir correctamente los documentos necesarios"></asp:Label>                                             
                                                    </div>
                                                    <asp:HiddenField ID="hdnAplicar" runat="server" />                                
                                                </div>                                               
                                                <div class="col-sm-12">
                                                    <div style="height:auto; width: auto; overflow-x: auto;">                                            
                                                        <asp:GridView ID="gvPagosProceso" runat="server" Width="100%" AutoGenerateColumns="False"
                                                            HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                                            PageSize="1000" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                            DataKeyNames="idPago" Style="overflow-x:auto;">                                        
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="Sel.">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hdnMontoAplicado" runat="server" Value='<%# Eval("montoAplicado") %>' />
                                                                        <asp:HiddenField ID="hdnMontoPorAplicar" runat="server" Value='<%# Eval("montoPorAplicar") %>' />
                                                                        <asp:CheckBox ID="chkIdPagoProceso" runat="server" Checked="true"  AutoPostBack="true" OnCheckedChanged="chkIdPagoProceso_CheckedChanged"/>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Id" DataField="idPago" />                            
                                                                <asp:BoundField HeaderText="Solicitud" DataField="idSolicitud" />                            
                                                                <asp:BoundField HeaderText="Monto Cobrado" DataField="montoCobrado" DataFormatString="{0:c2}" />
                                                                <asp:BoundField HeaderText="Monto Aplicado" DataField="montoAplicado" DataFormatString="{0:c2}" />                            
                                                                <asp:BoundField HeaderText="Monto Por Aplicar" DataField="montoPorAplicar" DataFormatString="{0:c2}" />                            
                                                                <asp:BoundField HeaderText="Canal de Pago" DataField="canalPagoDesc" />                            
                                                                <asp:BoundField HeaderText="Estatus" DataField="estatusDesc" />                            
                                                                <asp:BoundField HeaderText="Fecha Cobro" DataField="fechaPago"/>
                                                                <asp:BoundField HeaderText="Fecha Registro" DataField="fechaRegistro" />
                                                                <asp:BoundField HeaderText="Fecha Aplicación" DataField="fechaAplicacion" />
                                                            </Columns>
                                                            <PagerStyle CssClass="pagination-ty warning" /> 
                                                            <EmptyDataTemplate>
                                                                Aún no se seleccionan pagos
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </div>                                                                     
                                            </asp:Panel>                                
                                        </div>
                                    </ContentTemplate>                                                                            
                                </ajaxtoolKit:TabPanel>
                            </ajaxtoolKit:TabContainer>
                        </ContentTemplate>
                    </ajaxtoolKit:TabPanel>
                    <ajaxtoolKit:TabPanel HeaderText="Procesos Masivos" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlProceso" runat="server">  
                                <div class="col-sm-12 text-right">
                                    <asp:LinkButton ID="lnkRefreshProcesos" CssClass="btn btn-sm btn-default hidden-print" OnClick="lnkRefreshProcesos_Click" runat="server">
                                        <i class="fas fa-retweet"></i> Actualizar
                                    </asp:LinkButton>
                                </div>                                
                                <div class="form-group col-sm-12">
                                    <div style="height:auto; width: auto; overflow-x: auto;">
                                        <hr />
                                        <asp:GridView ID="gvProcesos" runat="server" Width="100%" AutoGenerateColumns="False"
                                            Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                            PageSize="1000" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            DataKeyNames="idProceso" Style="overflow-x:auto;" OnRowCommand="gvProcesos_RowCommand" OnRowDataBound="gvProcesos_RowDataBound">                                        
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkBtnSel" runat="server" CommandName="ConsultarDetalle" CommandArgument='<%# Eval("idProceso") %>' CssClass="btn btn-sm btn-secondary" data-toggle="tooltip" title="Ver Detalle">
                                                            <i class="fas fa-list fa-lg" style="color: #888888;"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="lnkReversa" runat="server" CommandName="ConsultarReversa" CommandArgument='<%# Eval("reversaIdProceso") %>' CssClass="btn btn-sm btn-secondary" data-toggle="tooltip" title="Ver Origen">
                                                            <i class="fas fa-history fa-lg" style="color: red;"></i>
                                                        </asp:LinkButton>
                                                        <asp:HiddenField runat="server" ID="hdnReversaIdProceso" Value='<%# Eval("reversaIdProceso") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                
                                                <asp:BoundField HeaderText="Tipo" DataField="tipo" />
                                                <asp:BoundField HeaderText="Estatus" DataField="estatus" />
                                                <asp:BoundField HeaderText="Total Pagos" DataField="totalPagos" />                            
                                                <asp:BoundField HeaderText="Pagos procesados" DataField="totalProcesados" />                            
                                                <asp:BoundField HeaderText="Fecha creación" DataField="fechaCreacion" />                          
                                                <asp:BoundField HeaderText="Usuario" DataField="usuarioNombre" />                            
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ty warning" /> 
                                            <EmptyDataTemplate>
                                                No se encontraron procesos
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>  
                            </asp:Panel>
                            <asp:Panel ID="pnlProcesoDetalle" runat="server" Visible="false">
                                <asp:HiddenField ID="hdnIdProcesoDetalle" runat="server" />
                                <asp:LinkButton ID="lbtnRegresar_Balance" CssClass="btn btn-sm btn-default hidden-print" OnClick="lbtnRegresar_Balance_Click" runat="server">
                                    <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbtnReversa" CssClass="btn btn-sm btn-danger hidden-print" OnClick="lbtnReversa_Click" runat="server">
                                    <i class="fas fa-history fa-lg"></i> Generar Reversa
                                </asp:LinkButton>

                                <div class="form-group col-sm-12">
                                    <br />
                                    <div id="divConfirmarReversa" runat="server" class="alert alert-success" role="alert" visible="false">
                                        <asp:Button ID="btnCancelarReversa" Text="Cancelar" CssClass="btn btn-warning btn-sm" runat="server" OnClick="btnCancelarReversa_Click"/>
                                        <asp:Button ID="btnConfirmarReversa" Text="Continuar" CssClass="btn btn-success btn-sm" runat="server" OnClick="btnConfirmarReversa_Click"/>
                                        <asp:Label ID="Label3" runat="server" Text="¿Deseas continuar con la reversa del proceso?"></asp:Label>         
                                    </div>
                                    <h2><asp:Label ID="lblTituloDetalleProceso" runat="server"></asp:Label></h2>                                                                        
                                    <div style="height:auto; width: auto; overflow-x: auto;">
                                        <hr />
                                        <asp:GridView ID="gvProcesoDetalle" runat="server" Width="100%" AutoGenerateColumns="False"
                                            Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                            PageSize="1000" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            DataKeyNames="idDetalle" Style="overflow-x:auto;">
                                            <Columns>                                                                                                                                                
                                                <asp:BoundField HeaderText="Solicitud" DataField="idSolicitud" />
                                                <asp:BoundField HeaderText="Resultado" DataField="mensajeEstatus" />                            
                                                <asp:BoundField HeaderText="Fecha aplicación" DataField="fechaAplicacion" />                            
                                                <asp:BoundField HeaderText="Monto por aplicar" DataField="montoPorAplicar" DataFormatString="{0:c2}"/>                            
                                                <asp:BoundField HeaderText="Monto aplicado" DataField="montoAplicado" DataFormatString="{0:c2}"/>                            
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ty warning" /> 
                                            <EmptyDataTemplate>
                                                No se encontro detalle
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxtoolKit:TabPanel>
                </ajaxtoolKit:TabContainer>
                <!-- RESULTADO -->
                <asp:Panel ID="pnlTabProceso" runat="server" Visible="false">
                    
                </asp:Panel>
            </ContentTemplate> 
            </asp:UpdatePanel>
        </div>
    </div>    
    <style type="text/css">
        hr {
            margin-top: 20px !important;
        }
    </style>
</asp:Content>