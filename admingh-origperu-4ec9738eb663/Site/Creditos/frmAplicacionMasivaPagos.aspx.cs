﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using wsSOPF;
using System.Threading.Tasks;

[Serializable]
public class PagoProceso
{
    public int idPago { get; set; }
    public int idSolicitud { get; set; }
    public double montoCobrado { get; set; }
    public double montoAplicado { get; set; }
    public double montoPorAplicar { get; set; }
    public string canalPagoDesc { get; set; }
    public string estatusDesc { get; set; }
    public string fechaPago { get; set; }
    public string fechaRegistro { get; set; }
    public string fechaAplicacion { get; set; }
}

public partial class Site_Creditos_frmAplicacionMasivaPagos : System.Web.UI.Page
{
    List<PagoProceso> lstPagosProceso;    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ViewState["procesos"] != null)
        {
            lstPagosProceso = (List<PagoProceso>)ViewState["procesos"];
        }
        else
        {
            lstPagosProceso = new List<PagoProceso>();
        }

        if (!Page.IsPostBack)
        {
            ddlTipoConvenio.Items.Insert(0, "Seleccionar");
            ddlConvenio.Items.Insert(0, "Seleccionar");
            ddlProducto.Items.Insert(0, "Seleccionar");

            this.CargarProcesos();
            this.CargarConvenios();
            this.CargaComboCanalPago();
        }      
    }

    protected void CargarProcesos()
    {
        ProcesoPagosAplicacionCondiciones condiciones = new ProcesoPagosAplicacionCondiciones();

        if (Convert.ToInt32(Session["UsuarioId"].ToString()) != 21)
        {
            condiciones.idUsuario = Convert.ToInt32(Session["UsuarioId"].ToString());
        }

        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            List<ProcesoPagosAplicacion> procesos = ws.ConsultarProcesosPagosAplicacion(condiciones).ToList();
            gvProcesos.DataSource = procesos;
            gvProcesos.DataBind();
        }
    }

    protected void ConsultarProcesoDetalle(int idProceso, bool reversa)
    {
        divConfirmarReversa.Visible = false;
        hdnIdProcesoDetalle.Value = idProceso.ToString();
        this.lblTituloDetalleProceso.Text = "Detalle";
        if (reversa)
        {
            this.lblTituloDetalleProceso.Text = this.lblTituloDetalleProceso.Text + " origen";
        }

        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            ProcesoPagosAplicacionCondiciones condiciones = new ProcesoPagosAplicacionCondiciones();
            condiciones.idProceso = Convert.ToInt32(idProceso);
            List<ProcesoPagosAplicacion> procesos = ws.ConsultarProcesosPagosAplicacion(condiciones).ToList();
            ProcesoPagosAplicacion proceso = procesos[0];

            lbtnReversa.Visible = false;
            if (proceso.idEstatus == 2 && proceso.reversaIdProceso == 0)
            {
                lbtnReversa.Visible = true;
            }

            ProcesoPagosAplicacionDetalleCondiciones dCondiciones = new ProcesoPagosAplicacionDetalleCondiciones();
            dCondiciones.idProceso = proceso.idProceso;
            List<ProcesoPagosAplicacionDetalle> detalles = ws.ConsultarProcesoPagosAplicacionDetalle(dCondiciones).ToList();
            gvProcesoDetalle.DataSource = detalles;
            gvProcesoDetalle.DataBind();
        }
    }

    protected void CargarConvenios()
    {
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            TBCATTipoConvenio[] convenios = wsCatalogo.ObtenerTipoConvenio(2, int.Parse(Session["cve_sucurs"].ToString()), string.Empty, 0, 0);
            ddlTipoConvenio.DataSource = convenios;
            ddlTipoConvenio.DataBind();
            ddlTipoConvenio.Items.Insert(0, new ListItem("Seleccionar", "0"));

            this.CargarProductos();
        }        
    }

    protected void CargarProductos()
    {
        using (wsSOPF.CatalogoClient ws = new CatalogoClient())
        {
            List<Combo> convenios = ws.ObtenerCombo_Convenio(int.Parse(ddlTipoConvenio.SelectedValue)).ToList();
            ddlConvenio.DataSource = convenios;
            ddlConvenio.DataBind();            
            ddlConvenio.Items.Insert(0, new ListItem("Seleccionar", "0"));

            this.CargarPlazos();
        }
    }

    protected void CargarPlazos()
    {
        using (wsSOPF.CatalogoClient ws = new CatalogoClient())
        {
            if (ddlConvenio.SelectedValue != string.Empty)
            {
                List<TBCATProducto> plazos = ws.ObtenerTBCATProducto(5, int.Parse(ddlConvenio.SelectedValue), string.Empty, 1, 0, -1).ToList();
                ddlProducto.DataSource = plazos;
                ddlProducto.DataBind();                
                ddlProducto.Items.Insert(0, new ListItem("Seleccionar", "0"));
            }
        }
    }

    private void MostrarMensaje(bool error, string mensaje)
    {
        divCorrecto.Visible = !error;
        divError.Visible = error;

        if (error)
        {
            lblError.Text = mensaje;
        }
        else
        {
            lblCorrecto.Text = mensaje;
        }
    }

    protected void PedirConfirmacion()
    {
        if (lstPagosProceso.Count == 0)
        {
            this.MostrarMensaje(true, "Favor de seleccionar al menos un pago");
            return;
        }

        this.btnAplicar.Visible = false;
        this.btnDesaplicar.Visible = false;

        string msj = "";
        if (hdnAplicar.Value == "1")
        {
            msj = "¿Desea continuar con la aplicación?";
        }
        else
        {
            msj = "¿Desea continuar con la desaplicación?";
        }

        this.MostrarMensaje(false, msj);
        btnCancelarProceso.Visible = false;
        btnProcesar.Visible = false;
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (!tabProceso.Visible)
        {
            this.tcResultado.Visible = true;
            this.pnlPagos.Visible = true;
            this.pnlTabProceso.Visible = true;

            this.chkTodos.Visible = false;            

            this.btnAplicar.Visible = false;
            this.btnDesaplicar.Visible = false;
        }

        this.chkTodos.Checked = false;
        this.divCorrecto.Visible = false;
        this.divError.Visible = false;
        this.divErrorPago.Visible = false;

        this.BuscarPagos();
    }

    protected void BuscarPagos()
    {
        DetallePagoMasivoCondiciones condiciones = new DetallePagoMasivoCondiciones();
        if (txtIdSolicitud.Text.Length > 0)
        {
            condiciones.idSolicitud = Convert.ToInt32(txtIdSolicitud.Text);
        }
        if (txtFechaPagoDesde_Pagos.Text.Length > 0)
        {
            condiciones.fechaPagoDesde = DateTime.ParseExact(txtFechaPagoDesde_Pagos.Text.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None);
        }
        if (txtFechaPagoHasta_Pagos.Text.Length > 0)
        {
            condiciones.fechaPagoHasta = DateTime.ParseExact(txtFechaPagoHasta_Pagos.Text.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None);
        }

        var idEstatus = new List<int>();
        if (chkEstRegistrado.Checked)
        {
            idEstatus.Add(1);
        }
        if (chkEstParcialAplicado.Checked)
        {
            idEstatus.Add(2);
        }
        if (chkEstAplicado.Checked)
        {
            idEstatus.Add(3);
        }
        if (chkEstCancelado.Checked)
        {
            idEstatus.Add(4);
        }
        if (chkEstDevuelto.Checked)
        {
            idEstatus.Add(5);
        }
        if (chkEstLiquidado.Checked)
        {
            idEstatus.Add(6);
        }
        condiciones.idEstatus = idEstatus.ToArray();

        if (ddlCanalPago.SelectedIndex > 0)
        {
            condiciones.idCanalPago = Convert.ToInt32(ddlCanalPago.SelectedValue);
        }
        if (txtFechaRegistroDesde_Pagos.Text.Length > 0)
        {
            condiciones.fechaRegistroDesde = DateTime.ParseExact(txtFechaRegistroDesde_Pagos.Text.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None);
        }
        if (txtFechaRegistroHasta_Pagos.Text.Length > 0)
        {
            condiciones.fechaRegistroHasta = DateTime.ParseExact(txtFechaRegistroHasta_Pagos.Text.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None);
        }
        if (txtFechaAplicacionDesde_Pagos.Text.Length > 0)
        {
            condiciones.fechaAplicacionDesde = DateTime.ParseExact(txtFechaAplicacionDesde_Pagos.Text.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None);
        }
        if (txtFechaAplicacionHasta_Pagos.Text.Length > 0)
        {
            condiciones.fechaAplicacionHasta = DateTime.ParseExact(txtFechaAplicacionHasta_Pagos.Text.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None);
        }
        if (ddlTipoConvenio.SelectedIndex > 0)
        {
            condiciones.idTipoConvenio = Convert.ToInt32(ddlTipoConvenio.SelectedValue);
        }
        if (ddlConvenio.SelectedIndex > 0)
        {
            condiciones.idConvenio = Convert.ToInt32(ddlConvenio.SelectedValue);
        }
        if (ddlProducto.SelectedIndex > 0)
        {
            condiciones.idProducto = Convert.ToInt32(ddlProducto.SelectedValue);
        }
        if (idEstatus.Contains(1) && idEstatus.Count == 1)
        {
            this.btnAplicar.Visible = true;
            this.btnDesaplicar.Visible = false;
        }
        if (idEstatus.Contains(3) && idEstatus.Count == 1)
        {
            this.btnAplicar.Visible = false;
            this.btnDesaplicar.Visible = true;
        }
        if (idEstatus.Contains(2) && idEstatus.Count == 1)
        {
            this.btnAplicar.Visible = true;
            this.btnDesaplicar.Visible = true;
        }
        if (idEstatus.Contains(1) && idEstatus.Contains(2) && idEstatus.Count == 2)
        {
            this.btnAplicar.Visible = true;
            this.btnDesaplicar.Visible = false;
        }
        if (idEstatus.Contains(3) && idEstatus.Contains(2) && idEstatus.Count == 2)
        {
            this.btnAplicar.Visible = false;
            this.btnDesaplicar.Visible = true;
        }

        if (btnAplicar.Visible || btnDesaplicar.Visible)
        {
            lblAction.Visible = true;
        }
        else
        {
            lblAction.Visible = false;
        }

        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            ResultadoDetallePagoMasivo res = ws.ConsultarPagosMasivos(condiciones);
            gvPagos.DataSource = res.pagos;
            gvPagos.DataBind();

            lblResultado.Text = "Mostrando " + res.pagos.Count() + " de " + res.total;
        }
    }

    protected void Procesar(object sender, EventArgs e)
    {
        divError.Visible = false;
        divErrorPago.Visible = false;
        pnlPagosProceso.Visible = true;
        tabProceso.Visible = true;        
        pnlEstatus.Visible = false;
        lblEstatus.Visible = true;

        chkEstRegistrado.Checked = false;
        chkEstParcialAplicado.Checked = false;
        chkEstAplicado.Checked = false;        
        chkEstCancelado.Checked = false;
        chkEstDevuelto.Checked = false;

        tabProceso.HeaderText = "Proceso";
        string textStatus = "Mostrando pagos";
        if (sender == btnAplicar)
        {
            this.hdnAplicar.Value = "1";
            this.chkAplicarUnico.Visible = true;
            chkEstRegistrado.Checked = true;
            chkEstParcialAplicado.Checked = true;

            lblEstatus.Text = textStatus + " por aplicar y parcialmente aplicados";
        }
        else
        {
            this.hdnAplicar.Value = "0";
            this.chkAplicarUnico.Visible = false;
            chkEstAplicado .Checked = true;
            chkEstParcialAplicado.Checked = true;

            lblEstatus.Text = textStatus + " aplicados y parcialmente aplicados";
        }
                
        lstPagosProceso = new List<PagoProceso>();
        ViewState["procesos"] = lstPagosProceso;        
        gvPagosProceso.DataSource = lstPagosProceso;        
        gvPagosProceso.DataBind();
        
        BuscarPagos();
        pnlActionButtons.Visible = false;
        chkTodos.Visible = true;        
    }

    protected void ActualizarPagosProceso(int accion, PagoProceso pago, Boolean actualizarGrid = true)
    {
        if (accion == 1)
        {
            int index = -1;
            for (int i = 0; i < lstPagosProceso.Count; i++)
            {
                PagoProceso p = lstPagosProceso[i];
                if (p.idPago == pago.idPago)
                {
                    index = i;
                    break;
                }
            }

            if (index == -1)
            {
                lstPagosProceso.Add(pago);
            }            
        }
        else
        {
            int index = -1;
            for (int i = 0; i < lstPagosProceso.Count; i++)
            {
                PagoProceso p = lstPagosProceso[i];
                if (p.idPago == pago.idPago)
                {
                    index = i;
                    break;
                }
            }

            if (index >= 0)
            {
                lstPagosProceso.RemoveAt(index);
            }            
                        
            foreach (GridViewRow row in gvPagos.Rows)
            {
                int iP = Convert.ToInt32(gvPagos.DataKeys[row.RowIndex][0].ToString());
                if (iP == pago.idPago)
                {
                    ((CheckBox)row.FindControl("chkId")).Checked = false;
                }
            }
        }

        ViewState["procesos"] = lstPagosProceso;

        if (actualizarGrid)
        {
            ActualizarGridPagosProceso();
        }        
    }

    private void ActualizarGridPagosProceso()
    {
        gvPagosProceso.DataSource = lstPagosProceso;
        gvPagosProceso.DataBind();        

        int cant = 0;
        decimal monto = 0;
        foreach (GridViewRow row in gvPagosProceso.Rows)
        {
            bool check = ((CheckBox)row.FindControl("chkIdPagoProceso")).Checked;

            if (check)
            {
                HiddenField hdn;

                if (hdnAplicar.Value == "1")
                {
                    hdn = (HiddenField)row.FindControl("hdnMontoPorAplicar");
                }
                else
                {
                    hdn = (HiddenField)row.FindControl("hdnMontoAplicado");
                }

                cant += 1;
                monto += Convert.ToDecimal(hdn.Value);
            }
        }

        lblProcesar.Text = cant + " pagos (S/" + Decimal.Round(monto, 2) + ")";
        tabProceso.HeaderText = "Proceso (" + cant + " pagos)";
    }

    protected void lbtnRegresar_Balance_Click(object sender, EventArgs e)
    {
        pnlProcesoDetalle.Visible = false;
        pnlProceso.Visible = true;
        CargarProcesos();
    }

    protected void gvProcesos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ConsultarDetalle") {
            pnlProcesoDetalle.Visible = true;
            pnlProceso.Visible = false;

            int idProceso = int.Parse(e.CommandArgument.ToString());
            this.ConsultarProcesoDetalle(idProceso, false);
        }

        if (e.CommandName == "ConsultarReversa")
        {
            pnlProcesoDetalle.Visible = true;
            pnlProceso.Visible = false;

            int reversaIdProceso = int.Parse(e.CommandArgument.ToString());
            this.ConsultarProcesoDetalle(reversaIdProceso, true);
        }
    }

    protected void chkTodos_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in gvPagos.Rows)
        {
            bool c = ((CheckBox)sender).Checked;
            ((CheckBox)row.FindControl("chkId")).Checked = c;

            if (c)
            {
                ActualizarPago(1, row, false);
            }            
        }

        if (((CheckBox)sender).Checked == false)
        {
            lstPagosProceso.Clear();
        }

        ActualizarGridPagosProceso();
    }

    protected void ddlTipoConvenio_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.CargarProductos();
    }

    protected void ddlConvenio_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.CargarPlazos();
    }

    protected void btnCancelarConfirmacion_Click(object sender, EventArgs e)
    {
        divCorrecto.Visible = false;

        btnAplicar.Enabled = true;
        btnDesaplicar.Enabled = true;
        btnCancelarProceso.Visible = true;
        btnProcesar.Visible = true;
    }

    protected void btnConfirmar_Click(object sender, EventArgs e)
    {
        var idPagos = new List<int>();
        foreach (PagoProceso p in lstPagosProceso)
        {
            idPagos.Add(p.idPago);
        }

        wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient();
        ProcesoPagosAplicacion proceso = new ProcesoPagosAplicacion();
        proceso.idUsuario = Convert.ToInt32(Session["UsuarioId"].ToString());
        proceso.idEstatus = 1;
        if (hdnAplicar.Value == "1")
        {
            proceso.aplicar = true;
        }
        else
        {
            proceso.aplicar = false;
        }   
        proceso.idPagos = idPagos.ToArray();

        int idProceso = ws.ProcesoPagosAplicacionAlta(proceso);
        proceso.idProceso = idProceso;        

        ProcesoPagosAplicacionDetalleCondiciones dCondiciones = new ProcesoPagosAplicacionDetalleCondiciones();
        dCondiciones.idProceso = proceso.idProceso;
        ProcesoPagosAplicacionDetalle[] pagos = ws.ConsultarProcesoPagosAplicacionDetalle(dCondiciones);

        idPagos.Clear();
        foreach (ProcesoPagosAplicacionDetalle p in pagos)
        {
            idPagos.Add(p.idPago);
        }
        proceso.idPagos = idPagos.ToArray();
        ProcesarPagos(proceso);

        this.lnkRefreshProcesos_Click(sender, e);
        ReiniciarPantalla();
    }

    protected void ProcesarPagos(ProcesoPagosAplicacion proceso, bool reversa = false)
    {
        wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient();
        var task = Task.Factory.StartNew(() =>
        {
            foreach (int id in proceso.idPagos)
            {
                try
                {
                    if (proceso.aplicar)
                    {
                        AplicarPagoParametros parametros = new AplicarPagoParametros();
                        parametros.idPago = id;
                        parametros.idProceso = proceso.idProceso;

                        if (reversa)
                        {
                            parametros.aplicarUnico = false;
                        }
                        else
                        {
                            parametros.aplicarUnico = chkAplicarUnico.Checked;
                        }                        
                        ws.Proceso_AplicarPago(parametros);
                    }
                    else
                    {
                        DesaplicarPagoParametros parametros = new DesaplicarPagoParametros();
                        parametros.idPago = id;
                        parametros.idProceso = proceso.idProceso;
                        ws.Proceso_DesaplicarPago(parametros);
                    }
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.Message);
                }
            }
        });
    }

    protected void gvPagos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (tabProceso.Visible)
        {
            e.Row.Cells[0].Visible = true;
            if (e.Row.Cells.Count > 2)
            {
                e.Row.Cells[1].Visible = false;
            }
        }
        else
        {
            e.Row.Cells[0].Visible = false;
            if (e.Row.Cells.Count > 2)
            {
                e.Row.Cells[1].Visible = true;
            }
        }

        if (e.Row.RowIndex < 0)
        {
            return;
        }

        int iP = Convert.ToInt32(gvPagos.DataKeys[e.Row.RowIndex][0].ToString());
        foreach (PagoProceso p in lstPagosProceso)
        {
            if (iP == p.idPago)
            {
                ((CheckBox)e.Row.FindControl("chkId")).Checked = true;
            }
        }

        string estatusClave = gvPagos.DataKeys[e.Row.RowIndex]["estatusClave"].ToString();
        if (estatusClave == "PR" || estatusClave == "PP")
        {
            ((LinkButton)e.Row.FindControl("lbtnAplicarPago")).Visible = true;
            ((LinkButton)e.Row.FindControl("lbtnAplicarPagoUnico")).Visible = true;
        }

        if (estatusClave == "PA" || estatusClave == "PP")
        {
            ((LinkButton)e.Row.FindControl("lbtnDesaplicarPago")).Visible = true;
        }

        if (estatusClave == "PR")
        {
            ((LinkButton)e.Row.FindControl("lbtnCancelarPago")).Visible = true;
            ((LinkButton)e.Row.FindControl("lbtnDevolverPago")).Visible = false;
            ((LinkButton)e.Row.FindControl("lbtnLiquidarPago")).Visible = true;
        }

        if (estatusClave == "PC")
        {
            ((LinkButton)e.Row.FindControl("lbtnRegistrarPago")).Visible = true;
        }

        if (Convert.ToInt32(Session["UsuarioId"].ToString()) == 21)
        {
            if (estatusClave == "PD" || estatusClave == "PL")
            {
                ((LinkButton)e.Row.FindControl("lbtnRegistrarPago")).Visible = true;
            }
        }
    }

    protected void gvPagos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        this.divCorrecto.Visible = false;
        this.divError.Visible = false;
        this.divErrorPago.Visible = false;

        if (e.CommandName == "select")
        {
            CheckBox chk = e.CommandSource as CheckBox;
            GridViewRow row = chk.Parent.Parent as GridViewRow;

            if (chk.Checked)
            {
                ActualizarPago(1, row);
            }
            else
            {
                ActualizarPago(2, row);
            }
        }

        switch (e.CommandName)
        {
            case "select":
                CheckBox chk = e.CommandSource as CheckBox;
                GridViewRow row = chk.Parent.Parent as GridViewRow;
                if (chk.Checked)
                {
                    ActualizarPago(1, row);
                }
                else
                {
                    ActualizarPago(2, row);
                }
                break;
            case "AplicarPago":
            case "AplicarPagoUnico":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    bool bAplicarUnico = (e.CommandName == "AplicarPagoUnico") ? true : false;
                    ResultadoOfboolean resultado = ws.Pagos_AplicarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) }, bAplicarUnico);

                    if (resultado.Codigo > 0)
                    {
                        divErrorPago.Visible = true;
                        lblErrorPago.Text = resultado.Mensaje;
                    }
                    else
                    {
                        btnBuscar_Click(sender, e);
                    }
                }
                break;
            case "DesaplicarPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean resultado = ws.Pagos_DesaplicarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) });
                    if (resultado.Codigo > 0)
                    {
                        divErrorPago.Visible = true;
                        lblErrorPago.Text = resultado.Mensaje;
                    }
                    else
                    {
                        btnBuscar_Click(sender, e);
                    }
                }
                break;
            case "CancelarPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean resultado = ws.Pagos_CancelarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) });
                    if (resultado.Codigo > 0)
                    {
                        divErrorPago.Visible = true;
                        lblErrorPago.Text = resultado.Mensaje;
                    }
                    else
                    {
                        btnBuscar_Click(sender, e);
                    }
                }
                break;
            //case "DevolverPago":
            //    using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            //    {
            //        ResultadoOfboolean resultado = ws.Pagos_DevolverPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) });
            //        if (resultado.Codigo > 0)
            //        {
            //            divErrorPago.Visible = true;
            //            lblErrorPago.Text = resultado.Mensaje;
            //        }
            //        else
            //        {
            //            btnBuscar_Click(sender, e);
            //        }
            //    }
            //    break;
            case "LiquidarPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean resultado = ws.Pagos_LiquidarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) }, Convert.ToInt32(Session["UsuarioId"].ToString()));
                    if (resultado.Codigo > 0)
                    {
                        divErrorPago.Visible = true;
                        lblErrorPago.Text = resultado.Mensaje;
                    }
                    else
                    {
                        btnBuscar_Click(sender, e);
                    }
                }
                break;
            case "RegistrarPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean resultado = ws.Pagos_RegistrarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) }, Convert.ToInt32(Session["UsuarioId"].ToString()));
                    if (resultado.Codigo > 0)
                    {
                        divErrorPago.Visible = true;
                        lblErrorPago.Text = resultado.Mensaje;
                    }
                    else
                    {
                        btnBuscar_Click(sender, e);
                    }
                }
                break;
        }
    }

    protected void ActualizarPago(int accion, GridViewRow row, Boolean actualizarGrid = true)
    {
        PagoProceso p = new PagoProceso();
        p.idPago = Convert.ToInt32(gvPagos.DataKeys[row.RowIndex][0].ToString());
        p.idSolicitud = Convert.ToInt32(gvPagos.DataKeys[row.RowIndex][1].ToString());
        p.montoCobrado = Convert.ToDouble(gvPagos.DataKeys[row.RowIndex][2].ToString());
        p.montoAplicado = Convert.ToDouble(gvPagos.DataKeys[row.RowIndex][3].ToString());
        p.montoPorAplicar = Convert.ToDouble(gvPagos.DataKeys[row.RowIndex][4].ToString());
        p.canalPagoDesc = gvPagos.DataKeys[row.RowIndex][5].ToString();
        p.estatusDesc = gvPagos.DataKeys[row.RowIndex][6].ToString();
        p.fechaPago = gvPagos.DataKeys[row.RowIndex][7].ToString();
        p.fechaRegistro = gvPagos.DataKeys[row.RowIndex][8].ToString();
        p.fechaAplicacion = gvPagos.DataKeys[row.RowIndex][9].ToString();         
        ActualizarPagosProceso(accion, p, actualizarGrid);
    }

    protected void chkId_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (CheckBox)sender;
        GridViewRow row = (GridViewRow)((DataControlFieldCell)(chk).Parent).Parent;

        if (chk.Checked)
        {
            ActualizarPago(1, row);
        }
        else
        {
            ActualizarPago(2, row);
        }        
    }

    protected void chkIdPagoProceso_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow row = (GridViewRow)((DataControlFieldCell)((CheckBox)sender).Parent).Parent;
        PagoProceso p = new PagoProceso();
        p.idPago = Convert.ToInt32(gvPagosProceso.DataKeys[row.RowIndex][0].ToString());
        ActualizarPagosProceso(2, p, true);
    }

    protected void btnProcesar_Click(object sender, EventArgs e)
    {
        PedirConfirmacion();
    }

    private void ReiniciarPantalla()
    {
        lstPagosProceso = new List<PagoProceso>();
        ViewState["procesos"] = lstPagosProceso;

        tcResultado.Visible = false;
        divCorrecto.Visible = false;
        divError.Visible = false;
        divErrorPago.Visible = false;

        chkTodos.Visible = false;
        chkTodos.Checked = false;

        btnAplicar.Visible = true;
        btnDesaplicar.Visible = true;

        pnlEstatus.Visible = true;
        chkEstRegistrado.Checked = false;
        chkEstParcialAplicado.Checked = false;
        chkEstAplicado.Checked = false;
        chkEstCancelado.Checked = false;
        chkEstDevuelto.Checked = false;
        lblEstatus.Visible = false;

        tabProceso.Visible = false;        
        pnlActionButtons.Visible = true;
        btnCancelarProceso.Visible = true;
        btnProcesar.Visible = true;
        lblProcesar.Text = "";
    }

    protected void btnCancelarProceso_Click(object sender, EventArgs e)
    {
        ReiniciarPantalla();
    }

    protected void gvProcesos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex < 0)
        {
            return;
        }

        int reversaIdProceso = Convert.ToInt32(((HiddenField)e.Row.FindControl("hdnReversaIdProceso")).Value);

        ((LinkButton)e.Row.FindControl("lnkReversa")).Visible = false;
        if (reversaIdProceso > 0)
        {
            ((LinkButton)e.Row.FindControl("lnkReversa")).Visible = true;
        }
    }

    protected void lbtnReversa_Click(object sender, EventArgs e)
    {
        divConfirmarReversa.Visible = true;
    }

    protected void btnCancelarReversa_Click(object sender, EventArgs e)
    {
        divConfirmarReversa.Visible = false;
    }

    protected void btnConfirmarReversa_Click(object sender, EventArgs e)
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            ProcesoPagosAplicacionCondiciones condiciones = new ProcesoPagosAplicacionCondiciones();
            condiciones.idProceso = Convert.ToInt32(hdnIdProcesoDetalle.Value);

            List<ProcesoPagosAplicacion> procesos = ws.ConsultarProcesosPagosAplicacion(condiciones).ToList();
            ProcesoPagosAplicacion procesoReversa = procesos[0];

            ProcesoPagosAplicacionDetalleCondiciones dCondiciones = new ProcesoPagosAplicacionDetalleCondiciones();
            dCondiciones.idProceso = procesoReversa.idProceso;
            ProcesoPagosAplicacionDetalle[] pagos = ws.ConsultarProcesoPagosAplicacionDetalle(dCondiciones);

            var idPagos = new List<int>();
            foreach (ProcesoPagosAplicacionDetalle p in pagos)
            {
                idPagos.Add(p.idPago);
            }
            
            ProcesoPagosAplicacion proceso = new ProcesoPagosAplicacion();
            proceso.idUsuario = Convert.ToInt32(Session["UsuarioId"].ToString());
            proceso.idEstatus = 1;
            proceso.aplicar = !procesoReversa.aplicar;
            proceso.idPagos = idPagos.ToArray();
            proceso.reversaIdProceso = procesoReversa.idProceso;

            int idProceso = ws.ProcesoPagosAplicacionAlta(proceso);
            proceso.idProceso = idProceso;
            ProcesarPagos(proceso, true);

            lbtnRegresar_Balance_Click(sender, e);
        }
    }

    protected void lnkRefreshProcesos_Click(object sender, EventArgs e)
    {
        CargarProcesos();
    }

    private void CargaComboCanalPago()
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            List<wsSOPF.CanalPago> canalesPago = ws.CanalPago_ListarActivos().ToList();

            ddlCanalPago.DataSource = canalesPago;
            ddlCanalPago.DataBind();
        }
    }
}
