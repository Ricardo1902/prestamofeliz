﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmBusquedaFiniquito.aspx.cs" Inherits="Creditos_frmBusquedaFiniquito" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">


    <script type="text/javascript">
        function SelectedIndexChange(ddl) {


            if (ddl.selectedIndex == 1) {
                document.getElementById('PorCuenta').style.display = "block";
                document.getElementById('PorCliente').style.display = "none";
                document.getElementById('PorGestor').style.display = "none";
            }
            else if (ddl.selectedIndex == 2) {
                document.getElementById('PorCuenta').style.display = "none";
                document.getElementById('PorCliente').style.display = "block";
                document.getElementById('PorGestor').style.display = "none";

            } else if (ddl.selectedIndex == 3) {

                document.getElementById('PorCuenta').style.display = "none";
                document.getElementById('PorCliente').style.display = "none";
                document.getElementById('PorGestor').style.display = "block";
            }
            else {
                document.getElementById('PorCuenta').style.display = "none";
                document.getElementById('PorCliente').style.display = "none";
                document.getElementById('PorGestor').style.display = "none";
            }


        }
    </script>
    





        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">CARTA DE FINIQUITO</h3>
            </div>
            <div class="panel-body col-xs-12 col-md-12">
                <div class="form-group">
                    <div class="form-group row">
                        <label for="txtCuenta" class="col-xs-12 col-md-12 form-control-label ">
                            Buscar Cuenta</label>

                        <div class="col-md-2">
                            <asp:TextBox ID="txtCuenta" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="txtCuenta" ErrorMessage="Favor de introducir Cuenta" ForeColor="Red" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                ErrorMessage="Cuenta Invalida."
                                ControlToValidate="txtCuenta"
                                ForeColor="Red"
                                ValidationExpression="^[0-9]{8}$" />
                        </div>




                    </div>


                </div>
            </div>
            <div class="panel-footer">

                <asp:Button ID="btnBuscarCuenta" runat="server" Text="Buscar" OnClick="btnBuscarCuenta_Click" CssClass="btn-primary btn-lg " />

            </div>

        </div>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Listado de Creditos</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="form-group row">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBuscarCuenta" EventName="Click" />
                                <asp:PostBackTrigger ControlID="gvCuentas" />
                            </Triggers>
                            <ContentTemplate>
                                 <div class="table-responsive">
                                <asp:GridView runat="server" ID="gvCuentas" DataKeyNames="idsolicitud,IdEstatus"
                                    Height="25%" AutoGenerateColumns="False" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow"
                                    AlternatingRowStyle-CssClass="GridRowAlternate" 
                                      CssClass="table table-bordered table-striped table table-responsive"  EmptyDataRowStyle-CssClass="GridEmptyData"
                                    GridLines="None" AllowPaging="true" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false"
                                    HeaderStyle-Wrap="false" OnPageIndexChanging="gvCuentas_PageIndexChanging" OnRowCommand="gvCuentas_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Cuenta">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="btnDetalle" CommandName="editar" OnClientClick="aspnetForm.target ='_blank';"><%#Eval("idsolicitud")%></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Cliente" DataField="idcliente" />
                                        <asp:BoundField HeaderText="Credito" DataField="idcuenta" />
                                        <asp:BoundField HeaderText="Nombre Completo" DataField="NombreCompleto" />
                                        <asp:BoundField HeaderText="Monto Último Pago" DataField="MontoUltPago" />
                                        <asp:BoundField HeaderText="Fecha Último Pago" DataField="FecUltPago" />
                                        <asp:BoundField HeaderText="Tipo Liquidación" DataField="TipoLiqui" />
                                        <asp:BoundField HeaderText="Id Liquidación" DataField="IdEstatus" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No se encontraron datos.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                     </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <div class="progress">
                                    Cargando...
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>
            </div>
        </div>


</asp:Content>

