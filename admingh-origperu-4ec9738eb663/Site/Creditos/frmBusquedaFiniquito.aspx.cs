﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;
using System.Data;

public partial class Creditos_frmBusquedaFiniquito : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnBuscarCuenta_Click(object sender, EventArgs e)
    {
        int IdCuenta = 0;
        int idUsu = 0;

        IdCuenta = Convert.ToInt32(txtCuenta.Text);
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        DataSet ListFiniquito = new DataSet(); //para datasets asi es
        //TBCreditos.Finiquitos[] ListFiniquito = null; Para listas asi es

        using (wsSOPF.CreditoClient wsCredito = new CreditoClient())
        {

            ListFiniquito = wsCredito.ObtenerTBCreditosFiniquitos(2, IdCuenta, idUsu);
            gvCuentas.DataSource = ListFiniquito;
            gvCuentas.DataBind();


        }
    }

    protected void gvCuentas_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        GridView gv = (GridView)sender;
        gv.PageIndex = e.NewPageIndex;

        using (wsSOPF.CreditoClient wsCredito = new CreditoClient())
        {

            DataSet ListFiniquito = new DataSet();

            int IdCuenta = 0;
            int idUsu = 0;

            IdCuenta = Convert.ToInt32(txtCuenta.Text);
            idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

            ListFiniquito = wsCredito.ObtenerTBCreditosFiniquitos(2, IdCuenta, idUsu);

            gvCuentas.DataSource = ListFiniquito;
            gvCuentas.DataBind();

        }
    }

    protected void gvCuentas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            Session["idCuenta"] = null;
            Session["idEstatus"] = null;

            if (e.CommandName == "editar")
            {
                int idCuenta = 0;
                int idEstatus = 0;

                LinkButton comando = e.CommandSource as LinkButton;
                GridViewRow registro = comando.Parent.Parent as GridViewRow;

                int.TryParse(gvCuentas.DataKeys[registro.RowIndex]["idsolicitud"].ToString(), out idCuenta);
                int.TryParse(gvCuentas.DataKeys[registro.RowIndex]["IdEstatus"].ToString(), out idEstatus);

                Session["idCuenta"] = idCuenta;
                Session["idEstatus"] = idEstatus;

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                switch (idEstatus)
                {
                    case 8:
                    case 13:
                    case 19:
                        Response.Redirect("frmRepCartaFinNoAdeudo.aspx?idsolicitud=" + idCuenta, false);
                        break;

                    case 16: Response.Redirect("frmRepCartaFinRenovacion.aspx?idsolicitud=" + idCuenta, false);
                        break;

                    case 17: Response.Redirect("frmRepCartaFinDefuncion.aspx?idsolicitud=" + idCuenta, false);
                        break;

                    default: Response.Redirect("frmRepCartaFinNoAdeudo.aspx?idsolicitud=" + idCuenta, false);
                        break;

                }

            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('Error Inesperado!!',1)</script>");
        }
    }

   
}