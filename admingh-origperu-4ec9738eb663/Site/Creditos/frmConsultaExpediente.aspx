﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmConsultaExpediente.aspx.cs" Inherits="Site_Creditos_frmConsultaExpediente" MasterPageFile="~/Site.master" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        html, body {
            height: 100%;
            margin: 0;         /* Reset default margin on the body element */
        }
    </style>
    <script src="js/frmConsultaExpediente.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">    
    <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
        <ProgressTemplate>
            <div id="loader-background"></div>
            <div id="loader-content"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlBuscar" Visible="true" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Consulta Expediente</h3>
                    </div>
                    <div class="panel-body">
                        <asp:Panel ID="pnlExito_Buscar" class="col-sm-12" Visible="false" runat="server">
                            <div class="alert alert-success" style="padding: 5px 15px; margin: 5px 0px 0px 0px;">
                                <i class="far fa-check-circle"></i>
                                <asp:Label ID="lblExito_Buscar" runat="server"></asp:Label>
                            </div>
                            <hr />
                        </asp:Panel>

                        <asp:Panel ID="pnlError_Buscar" class="col-sm-12" Visible="false" runat="server">
                            <div class="alert alert-danger" style="padding: 5px 15px; margin: 5px 0px 0px 0px;">
                                <i class="fas fa-exclamation-circle"></i>
                                <asp:Label ID="lblError_Buscar" runat="server"></asp:Label>
                            </div>
                            <hr />
                        </asp:Panel>
                       
                        <div class="form-group col-sm-8 col-md-6 col-lg-4">
                            <label class="small">IdSolicitud, DNI o Nombre de Cliente</label>
                            <asp:TextBox ID="txtBuscar" MaxLength="25" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="form-group col-sm-12">
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-sm btn-primary" OnClick="btnBuscar_Click" />
                        </div>

                        <div class="form-group col-sm-12">
                            <div style="height: auto; width: auto; overflow-x: auto;">
                                <asp:GridView ID="gvBuscar" runat="server" Width="100%" AutoGenerateColumns="False"
                                    Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                    CssClass="table table-bordered table-striped  table-hover table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                    OnPageIndexChanging="gvBuscar_PageIndexChanging" OnRowDataBound="gvBuscar_RowDataBound" PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                    DataKeyNames="IdSolicitud" Style="overflow-x: auto;">
                                    <PagerStyle CssClass="pagination-ty" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnVerExpediente" CssClass="btn btn-xs" data-toggle="tooltip" Visible="true" runat="server"
                                                    OnClientClick='<%# "verExpediente(`" + Eval("GDriveID") + "`," + Eval("IdSolicitud")  + ");return false;" %>'>                                                
                                                    <i class="fas fa-file-download fa-lg" style="color: #f77272;"></i>&nbsp;&nbsp;Descargar
                                                </asp:LinkButton>                                             
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Solicitud" DataField="IdSolicitud" ItemStyle-Width="100px" />
                                        <asp:BoundField HeaderText="Fecha Solicitud" DataField="FechaSolicitud.Value" DataFormatString="{0:d}" ItemStyle-Width="100px" />
                                        <asp:BoundField HeaderText="DNI" DataField="DNI" ItemStyle-Width="100px" />                                        
                                        <asp:TemplateField HeaderText="Cliente">
                                            <ItemTemplate>
                                                <%# Eval("NombreCliente") %> <%# Eval("ApellidoPaterno") %> <%# Eval("ApellidoMaterno") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No se encontraron Expedientes con los filtros seleccionados
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>                       
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(document).ready(function () {
            // Activar Tooltips primer carga de la pagina
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            // Activar Tooltips
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });
    </script>
</asp:Content>
