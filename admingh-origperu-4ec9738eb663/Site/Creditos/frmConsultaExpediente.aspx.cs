﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Site_Creditos_frmConsultaExpediente : System.Web.UI.Page
{   
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!Page.IsPostBack)
        {
            
        }
    }

    protected void gvBuscar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Mostramos la "accion" Ver Expediente si el GDriveID existe.
            ConsultaExpediente dataActual = (ConsultaExpediente)e.Row.DataItem;

            LinkButton lbtnVerExpediente = (LinkButton)e.Row.FindControl("lbtnVerExpediente");
            lbtnVerExpediente.Visible = !string.IsNullOrEmpty(dataActual.GDriveID);            
        }
    }   

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        pnlError_Buscar.Visible = false;
        pnlExito_Buscar.Visible = false;

        try
        {
            if (txtBuscar.Text.Trim().Length < 5)
                throw new Exception("Capture al menos 5 caracteres para realizar la busqueda");

            using (wsSOPF.CreditoClient ws = new CreditoClient())
            {
                ResultadoOfArrayOfConsultaExpedientehDx61Z0Z res = ws.ConsultaExpediente(txtBuscar.Text.Trim());

                if (res.Codigo > 0)
                    throw new Exception(res.Mensaje);

                gvBuscar.DataSource = res.ResultObject;
                gvBuscar.DataBind();
            }
        }
        catch (Exception ex)
        {
            pnlError_Buscar.Visible = true;
            lblError_Buscar.Text = ex.Message;
        }
    }   

    protected void gvBuscar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBuscar.PageIndex = e.NewPageIndex;
        btnBuscar_Click(sender, e);
    }      
}
