﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmDevoluciones.aspx.cs" Inherits="Site_Creditos_frmDevoluciones" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Devoluciones</h3>
        </div>
        <div class="panel-body">
            <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
                <ProgressTemplate>                 
                    <div id="loader-background"></div>
                    <div id="loader-content"></div>
                </ProgressTemplate> 
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upMain" runat="server">
                <ContentTemplate>
                    <ajaxtoolKit:TabContainer ID="tcDevolucionesNoIndentificadas" CssClass="actTab" OnActiveTabChanged="tcDevolucionesNoIndentificadas_ActiveTabChanged" AutoPostBack="true" runat="server">
                        <ajaxtoolKit:TabPanel HeaderText="Buscar" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnlGuardaSolicitud" class="col-sm-12" Visible="false" runat="server">
                                    <asp:HiddenField ID="lblIdDevolucionNoIdentificada" runat="server" /> 
                                    <div class="form-group col-md-4 col-lg-3">
                                        <label class="form-control-label small">Solicitud</label>                                
                                        <div> 
                                            <asp:TextBox ID="txtIdSolicitud" CssClass="form-control" placeholder="Solicitud" MaxLength="10" onKeyPress="return EvaluateText('%d', this);" runat="server"/>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Solicitud es obligatorio" ControlToValidate="txtIdSolicitud" ForeColor="Red" ValidationGroup="GuardaSolicitudVal" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>                                  
                                    <div class="form-group col-md-4 col-lg-3">
                                        <label class="form-control-label small">Canal de pago</label>                                                                        
                                        <asp:TextBox ID="lblCanalPago" CssClass="form-control"  runat="server" Enabled="false"/>                                                                                     
                                    </div>
                                    <div class="form-group col-md-4 col-lg-3">
                                        <label class="form-control-label small">Monto</label>                                                                        
                                        <asp:TextBox ID="lblMonto" CssClass="form-control"  runat="server" Enabled="false"/>                                                                                     
                                    </div>
                                    <div class="form-group col-md-4 col-lg-3">
                                        <label class="form-control-label small">Fecha de Devolución</label>                                                                        
                                        <asp:TextBox ID="lblFechaDevolucion" CssClass="form-control"  runat="server" Enabled="false"/>                                                  
                                    </div>                                                                       
                                    <div class="form-group col-md-4 col-lg-12">
                                        <input type="button" onclick="javascript: document.getElementById('<%=btnGuardaIdSolicitud.ClientID%>').click();" class="btn btn-sm btn-success" value="Guardar" />
                                        <input type="button" onclick="javascript: document.getElementById('<%=btnCancelaGuardaIdSolicitud.ClientID%>').click();" class="btn btn-sm btn-danger" value="Cancelar" />
                                    </div>

                                </asp:Panel>
                                <asp:Panel ID="pnlErrorBusqueda" class="col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <asp:Label ID="lblErrorBusqueda" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlGuardaSoliExito" class="col-sm-12" Visible="false" runat="server">                                
                                            <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                <i class="far fa-check-circle"></i>
                                                <asp:Label ID="lblGuardaSoliExito" runat="server"></asp:Label>
                                            </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlCarga_BuscarDevoluciones" runat="server">
                                    <div class="form-group col-md-2 col-lg-2">
                                        <label class="form-control-label small">Id Solicitud</label>                                
                                        <div>     
                                            <div class="input-group">
                                                <asp:TextBox ID="txtBuscarSolicitud" CssClass="form-control" placeholder="Id Solicitud" MaxLength="10"  runat="server"/>                                                                    
                                            </div>
                                        </div>         
                                    </div> 
                                    <div class="form-group col-md-5 col-lg-5">
                                        <label class="form-control-label small">Fecha Inicio</label>                                
                                        <div>     
                                            <div class="input-group">
                                                <asp:TextBox ID="txtFechaReciboDesde" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                                <span class="input-group-addon">
                                                    <asp:LinkButton ID="btnSelectFechaCreditoDesde_Balance" runat="server">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </asp:LinkButton>
                                                </span>
                                            </div>                                                         
                                            <asp:RegularExpressionValidator runat="server"
                                                    ErrorMessage="Formato Incorrecto"
                                                    ControlToValidate="txtFechaReciboDesde"
                                                    ForeColor="Red"
                                                    ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />                                    
                                            <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaReciboDesde" PopupButtonID="btnSelectFechaCreditoDesde_Balance" runat="server"></ajaxtoolKit:CalendarExtender>
                                        </div>         
                                    </div>
                                    <div class="form-group col-md-5 col-lg-5">
                                        <label class="form-control-label small">Fecha Fin</label>                                
                                        <div>                    
                                            <div class="input-group">
                                                <asp:TextBox ID="txtFechaReciboHasta" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                                <span class="input-group-addon">
                                                    <asp:LinkButton ID="btnSelectFechaCreditoHasta_Balance" runat="server">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </asp:LinkButton>
                                                </span>
                                            </div>                    
                                            <asp:RegularExpressionValidator runat="server"
                                                    ErrorMessage="Formato Incorrecto"
                                                    ControlToValidate="txtFechaReciboHasta"
                                                    ForeColor="Red"
                                                    ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                            <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaReciboHasta" PopupButtonID="btnSelectFechaCreditoHasta_Balance" runat="server"></ajaxtoolKit:CalendarExtender>
                                        </div>        
                                    </div>
                                    
                                    <div class="form-group col-md-4 col-lg-12">                                 
                                        <input type="button" onclick="javascript: document.getElementById('<%=btnBusqueda.ClientID%>').click();" class="btn btn-sm btn-primary" value="Buscar devoluciones" />
                                    </div>

                                    <asp:Panel ID="pnlError_BuscarDevoluciones" class="col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <asp:Label ID="lblError_BuscarDevolucion" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                    
                                    <div style ="height:auto; width:100%; overflow-x:auto;">
                                    <asp:GridView ID="gvDevoluciones"                                                 
                                        OnPageIndexChanging="gvDevoluciones_PageIndexChanging"
                                        runat="server" Width="100%" AutoGenerateColumns="false"
                                        Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-hover-warning table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                        PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        Style="overflow-x:auto; overflow-y:scroll;"                                         
                                        selectedindex="1"
                                        onselectedindexchanged="CustomersGridView_SelectedIndexChanged"
                                        autogenerateselectbutton="false">
                                        <PagerStyle CssClass="pagination-ty" />
                                        <Columns>                                            
                                            <asp:BoundField HeaderText="#" DataField="idDevolucion" />                                             
                                            <asp:BoundField HeaderText="Id Solicitud" DataField="idsolicitud"/>
                                            <asp:BoundField HeaderText="Monto" DataField="montoPago" DataFormatString="{0:C2}"/>                                            
                                            <asp:BoundField HeaderText="Fecha de Devolución" DataField="fechaPago" DataFormatString="{0:dd/MM/yyyy}"/>
                                            <asp:BoundField HeaderText="Comentarios" DataField="comentarios" />
                                            <asp:BoundField HeaderText="Resultado" DataField="resultado" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No se encontraron registros de devoluciones
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    </div>
                                </asp:Panel>                                
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                        <ajaxtoolKit:TabPanel ID="tpCargaMasiva" HeaderText="Carga Masiva" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnlCarga_CargarArchivoDevoluciones" runat="server">  
                                <div class="form-group col-sm-12">           
                                        <label>Formato e Instrucciones</label>
                                        <p>
                                            <ul>
                                                <li>
                                                    Seleccione el archivo para realizar la carga de devoluciones, el nombre del archivo servira para identificar la carga que se
                                                    realiza, debe ser un nombre único, de lo contrario el sistema lo marcara como duplicado.
                                                </li>
                                                <li>
                                                    La información de las Devoluciones Directas se debe presentar en el siguiente orden (Columnas):<br />
                                                    <span class="small"><strong>ID SOLICITUD, MONTO, FECHA </strong>(DD/MM/AAAA)<strong>, COMENTARIOS</strong></span>
                                                </li>
                                                <li>
                                                    La primer linea del archivo se ignorara ya que deben ser los encabezados en el orden indicado
                                                </li>
                                                <li>
                                                    Descarga el Layout para comenzar
                                                    <a href="/archivos/devoluciones.xlsx" class="btn btn-sm btn-default" download="devoluciones.xlsx"><i class="fas fa-download fa-lg"></i> Descargar</a>
                                                </li>
                                            </ul>
                                        </p>
                                </div>
                                <div class="form-group col-md-4 col-lg-12">   
                                    <asp:FileUpload runat="server" CssClass="form-control" ID="archDevoluciones" accept=".xls,.xlsx" />
                                    <asp:label id="lblErrorSubirArchivodevoluciones" runat="server" Text="Elija un archivo xls para subir" forecolor="Red" Visible="false"/>
                                </div>
                                <div class="form-group col-md-4 col-lg-12">
                                    <asp:RegularExpressionValidator
                                      id="RegularExpressionValidator1"                                    
                                      ErrorMessage="El archivo debe ser formato xlsx"
                                      ValidationExpression ="^.+(.xlsx|.XLS|.xlsx|.XLSX)$"
                                      ControlToValidate="archDevoluciones" ValidationGroup="CargaArchivoVal" CssClass="small" ForeColor="Red" Display="Dynamic"
                                      runat="server"></asp:RegularExpressionValidator>
                                </div>
                                <div class="form-group col-md-4 col-lg-12">
                                    <input type="button" onclick="javascript: document.getElementById('<%=btnCargaArchivoDevoluciones.ClientID%>').click();" class="btn btn-sm btn-primary" value="Subir Devoluciones" />
                                </div>
                                <asp:Panel ID="pnlExito_ArchivosCargados" class="col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <i class="far fa-check-circle"></i>
                                        <asp:Label ID="lblExito_ArchivosCargados" Text="La informacion ha sido registrada" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>
                                    <asp:Panel ID="pnlErrorGuardarArchivo" class="col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <asp:Label ID="lblErrorGuardarArchivo" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                                <asp:Panel ID="pnlResumen_CargarArchivo" Visible="false" runat="server">                                  
                                    <div class="col-sm-12 form-group">
                                        <strong>Nombre del Archivo: </strong> <%= this.NombreArchivo %>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-12" style="width:auto; height:auto; overflow-x: auto;">                                        
                                        <asp:GridView ID="gvResumen_CargarArchivo"                                                                                 
                                            runat="server" Width="100%" AutoGenerateColumns="False"
                                            HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None"
                                            RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            >                                        
                                            <Columns>    
                                                <asp:BoundField HeaderText="#" DataField="noLinea" />
                                                <asp:BoundField HeaderText="Id Solicitud" DataField="idSolicitud" />                                                
                                                <asp:BoundField HeaderText="Monto" DataField="monto" DataFormatString="{0:C2}" />                                                               
                                                <asp:BoundField HeaderText="Fecha" DataField="fecha" DataFormatString="{0:d}" />                                            
                                                <asp:BoundField HeaderText="Comentarios" DataField="comentarios" />                                            
                                            </Columns>
                                            <EmptyDataTemplate>
                                                No se encontraron Devoluciones para Registrar
                                            </EmptyDataTemplate>
                                        </asp:GridView>                                        
                                    </div>

                                    <asp:Panel ID="pnlAdvertencia_CargarArchivo" class="col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-warning" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <i class="fas fa-exclamation-circle"></i>
                                            <asp:Label ID="lblAdvertencia_CargarArchivo" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>

                                    <div class="col-sm-12">
                                        <asp:LinkButton ID="lbtnGuardarArchivoDevolucionesDirectos_CargarArchivo" OnClick="lbtnGuardarArchivoDevolucionesDirectos_CargarArchivo_Click" CssClass="btn btn-sm btn-success" runat="server">
                                            <i class="far fa-check-circle"></i> Guardar Devoluciones
                                        </asp:LinkButton>

                                        <a href="frmDevoluciones.aspx" class="btn btn-sm btn-danger">
                                            <i class="far fa-times-circle"></i>
                                            Cancelar
                                        </a>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel> 
                         
                        <ajaxtoolKit:TabPanel HeaderText="Carga Individual" runat="server">
                            <ContentTemplate>
                                <div class="form-group col-sm-12">
                                    <div style="height:auto; width: auto; overflow-x: auto;">                                        
                                        <div class="form-group col-md-4 col-lg-3">
                                            <label class="form-control-label small">Id Solicitud</label>                                
                                            <div>                                                                    
                                                <asp:TextBox ID="txtSolicitud" CssClass="form-control" placeholder="IdSolicitud" MaxLength="10" onKeyPress="return EvaluateText('%i', this);" runat="server"/>                                                                                            
                                            </div>     
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Solicitud es obligatoria" ControlToValidate="txtSolicitud" ForeColor="Red" ValidationGroup="GuardaDevolucionVal" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>   
                                        </div>
                                        <div class="form-group col-md-4 col-lg-3">
                                            <label class="form-control-label small">Fecha de Devolución</label>                                
                                            <div>                    
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtFechaDevolucion" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                                    <span class="input-group-addon">
                                                        <asp:LinkButton ID="btnSelectFechaDevolucion_Balance" runat="server">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </asp:LinkButton>
                                                    </span>
                                                </div>                    
                                                <asp:RegularExpressionValidator runat="server"
                                                        ErrorMessage="Formato Incorrecto"
                                                        ControlToValidate="txtFechaDevolucion"
                                                        ForeColor="Red"
                                                        ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                                <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaDevolucion" PopupButtonID="btnSelectFechaDevolucion_Balance" runat="server"></ajaxtoolKit:CalendarExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Fecha Devolucion es obligatorio" ControlToValidate="txtFechaDevolucion" ForeColor="Red" ValidationGroup="GuardaDevolucionVal" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                            </div>        
                                        </div>
                                        <div class="form-group col-md-4 col-lg-3">
                                            <label class="form-control-label small">Monto</label>                                
                                            <div>                                                                    
                                                <asp:TextBox ID="txtMonto" CssClass="form-control" placeholder="Monto" MaxLength="10" onKeyPress="return EvaluateText('%f', this);" runat="server"/>                                                                                            
                                            </div>     
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Monto es obligatorio" ControlToValidate="txtMonto" ForeColor="Red" ValidationGroup="GuardaDevolucionVal" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>   
                                        </div>
                                        <div class="form-group col-md-12 col-lg-9">
                                            <label class="form-control-label small">Comentarios</label>                                
                                            <div>                                                                    
                                                <asp:TextBox  ID="txtObserva" TextMode="multiline" CssClass="form-control" placeholder="Comentarios" Columns="50" Rows="5" MaxLength="250"  runat="server"/>                                                 
                                            </div>        
                                        </div>
                                        <div class="visible-md-block clearfix"></div>
                                        <div class="form-group col-md-4 col-lg-12">  
                                           <input type="button" onclick="javascript: document.getElementById('<%=btnGuardaDevolucion.ClientID%>').click();" class="btn btn-sm btn-primary" value="Guardar Devolución" />
                                        </div>
                                        <asp:Panel ID="pnlDevolucionExito" class="col-sm-12" Visible="false" runat="server">                                
                                            <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                <i class="far fa-check-circle"></i>
                                                <asp:Label ID="lblDevolucionMens" runat="server"></asp:Label>
                                            </div>
                                        </asp:Panel>                                        
                                    </div>  
                                </div> 
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>                                                                  
                    </ajaxtoolKit:TabContainer>
                    <div style="visibility:hidden">
                        <asp:Button ID="btnBusqueda" ValidationGroup="BusquedaVal" OnClick="btnBusqueda_Click" runat="server" />
                        <asp:Button ID="btnGuardaDevolucion" ValidationGroup="GuardaDevolucionVal" OnClick="btnGuardaDevolucion_Click" runat="server" />
                        <asp:Button ID="btnCargaArchivoDevoluciones" ValidationGroup="CargaArchivoVal" OnClick="btnCargaArchivoDevoluciones_Click" runat="server" />
                        <asp:Button ID="btnGuardaIdSolicitud" ValidationGroup="GuardaSolicitudVal" runat="server" />
                        <asp:Button ID="btnCancelaGuardaIdSolicitud"  OnClick="btnCancelaGuardaIdSolicitud_Click" runat="server" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnBusqueda" />
                    <asp:PostBackTrigger ControlID="btnGuardaDevolucion" />
                    <asp:PostBackTrigger ControlID="btnCargaArchivoDevoluciones" />
                    <asp:PostBackTrigger ControlID="btnGuardaIdSolicitud" />
                    <asp:PostBackTrigger ControlID="btnCancelaGuardaIdSolicitud" />
                </Triggers>           
            </asp:UpdatePanel>                                               
        </div>
    </div>
</asp:Content>
