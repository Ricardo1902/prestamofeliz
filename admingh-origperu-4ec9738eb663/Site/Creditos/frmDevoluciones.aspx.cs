﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpreadsheetLight;

public partial class Site_Creditos_frmDevoluciones : System.Web.UI.Page
{
    byte[] contenido = null;
    string nombreArchivo = string.Empty;

    protected string NombreArchivo
    {
        get
        {
            return (ViewState[this.UniqueID + "_NombreArchivo"] != null) ? ViewState[this.UniqueID + "_NombreArchivo"].ToString() : string.Empty;
        }
        set
        {
            ViewState[this.UniqueID + "_NombreArchivo"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }

    #region "Domiciliacion BBVA"

    protected void btnBusqueda_Click(object sender, EventArgs e)
    {
        pnlError_BuscarDevoluciones.Visible = false;

        try
        {
            buscarDevoluciones();
        }
        catch (Exception ex)
        {
            pnlCarga_BuscarDevoluciones.Visible = true;
            lblError_BuscarDevolucion.Text = ex.Message;
            pnlError_BuscarDevoluciones.Visible = true;
        }
    }

    private void buscarDevoluciones()
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
        {
            DateTime? fchCreditoDesde = null;
            DateTime? fchCreditoHasta = null;

            wsSOPF.DevolucionCondiciones condiciones = new wsSOPF.DevolucionCondiciones();
            if (!string.IsNullOrEmpty(txtFechaReciboDesde.Text.Trim()))
            {
                fchCreditoDesde = DateTime.ParseExact(txtFechaReciboDesde.Text.Trim(), "dd/MM/yyyy", null);
                condiciones.fechaPagoInicio = fchCreditoDesde;
            }

            if (!string.IsNullOrEmpty(txtFechaReciboHasta.Text.Trim()))
            {
                fchCreditoHasta = DateTime.ParseExact(txtFechaReciboHasta.Text.Trim(), "dd/MM/yyyy", null);
                condiciones.fechaPagoFin = fchCreditoHasta;
            }

            if (!string.IsNullOrEmpty(txtBuscarSolicitud.Text.Trim()))
            {
                condiciones.idSolicitud = int.Parse(txtBuscarSolicitud.Text);
            }

            List<wsSOPF.Devolucion> devoluciones = ws.ObtenerDevoluciones(condiciones).ToList();
            gvDevoluciones.DataSource = devoluciones;
            gvDevoluciones.DataBind();

            pnlCarga_BuscarDevoluciones.Visible = true;
            pnlGuardaSolicitud.Visible = false;
        }
    }

    protected void btnCargaArchivoDevoluciones_Click(object sender, EventArgs e)
    {
        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                // VALIDAR ANTES DE CARGAR EL ARCHIVO
                if (!archDevoluciones.HasFile)
                {
                    lblErrorSubirArchivodevoluciones.Visible = true;
                }
                else
                {
                    string folderPath = Server.MapPath("~/Archivos/Devoluciones/");
                    archDevoluciones.SaveAs(folderPath + "TMP_" + Path.GetFileName(archDevoluciones.FileName));
                    this.NombreArchivo = archDevoluciones.FileName.Trim();

                    ProcesarExcel();

                    pnlCarga_CargarArchivoDevoluciones.Visible = false;
                    pnlResumen_CargarArchivo.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            pnlCarga_CargarArchivoDevoluciones.Visible = false;
            pnlResumen_CargarArchivo.Visible = true;

            pnlAdvertencia_CargarArchivo.Visible = true;
            lblAdvertencia_CargarArchivo.Text = ex.Message;
        }
    }

    protected void lbtnGuardarArchivoDevolucionesDirectos_CargarArchivo_Click(object sender, EventArgs e)
    {
        lblErrorSubirArchivodevoluciones.Visible = false;
        int idUsuario = 0;
        try
        {
            string filePath = "";

            string folderPath = Server.MapPath("~/Archivos/Devoluciones/");
            filePath = folderPath + "TMP_" + Path.GetFileName(this.NombreArchivo);

            byte[] bytes = File.ReadAllBytes(filePath);

            if (bytes.Length > 0)
            {
                contenido = bytes;
                nombreArchivo = this.NombreArchivo;
            }
            if (Session["UsuarioId"] != null)
            {
                int.TryParse(Session["UsuarioId"].ToString(), out idUsuario);
            }

            wsSOPF.GeneraDevolucionResponse respDevolucionMasivo = new wsSOPF.GeneraDevolucionResponse();
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                respDevolucionMasivo = ws.GenDevolucionesMasivo(new wsSOPF.GeneraDevolucionRequest
                {
                    idUsuario = idUsuario,
                    nombreArchivo = nombreArchivo,
                    contenidoArchivo = contenido
                });
            }
            if (respDevolucionMasivo != null)
            {
                if (!respDevolucionMasivo.Error)
                {
                    pnlExito_ArchivosCargados.Visible = true;
                    pnlResumen_CargarArchivo.Visible = false;
                    pnlCarga_CargarArchivoDevoluciones.Visible = true;
                }
            }
        }
        catch (Exception er)
        {
            pnlErrorGuardarArchivo.Visible = true;
            lblErrorGuardarArchivo.Text = er.Message;
        }
    }

    private void ProcesarExcel()
    {
        lbtnGuardarArchivoDevolucionesDirectos_CargarArchivo.Enabled = true;

        string folderPath = Server.MapPath("~/Archivos/Devoluciones/");
        List<DevolucionResumen> devolucionesX = new List<DevolucionResumen>();
        DevolucionResumen devolucionX = null;

        string path = folderPath + "TMP_" + this.NombreArchivo;
        SLDocument sl = new SLDocument(path);

        string errLines = string.Empty;

        int iRow = 2;
        while (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 1)))
        {
            try
            {
                devolucionX = new DevolucionResumen();
                string fec = sl.GetCellValueAsString(iRow, 3);

                devolucionX.noLinea = iRow;
                devolucionX.idSolicitud = int.Parse(sl.GetCellValueAsString(iRow, 1));
                devolucionX.monto = decimal.Parse(sl.GetCellValueAsString(iRow, 2));
                devolucionX.fecha = DateTime.ParseExact(sl.GetCellValueAsString(iRow, 3), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                devolucionX.comentarios = sl.GetCellValueAsString(iRow, 4);
                devolucionesX.Add(devolucionX);
            }
            catch (Exception ex)
            {
                errLines += ex.Message + ", ";
            }

            iRow++;
        }

        // MOSTRAR EL RESUMEN DEL ARCHIVO CSV PROCESADO
        gvResumen_CargarArchivo.DataSource = devolucionesX;
        gvResumen_CargarArchivo.DataBind();

        if (!string.IsNullOrEmpty(errLines))
        {
            lbtnGuardarArchivoDevolucionesDirectos_CargarArchivo.Enabled = false;

            pnlAdvertencia_CargarArchivo.Visible = true;
            lblAdvertencia_CargarArchivo.Text = string.Format("Las siguientes devoluciones presentan formato incorrecto en su información:<br/>{0}", errLines.Remove(errLines.LastIndexOf(","), 1).Trim());
        }
    }

    protected void btnGuardaDevolucion_Click(object sender, EventArgs e)
    {
        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                DateTime? fchDevolucion = null;
                decimal Monto = 0;
                int idSolicitud = 0;
                int IdUsuario = 0;

                if (!string.IsNullOrEmpty(txtFechaDevolucion.Text.Trim()))
                    fchDevolucion = DateTime.ParseExact(txtFechaDevolucion.Text.Trim(), "dd/MM/yyyy", null);

                if (!string.IsNullOrEmpty(txtSolicitud.Text.Trim()))
                {
                    idSolicitud = Convert.ToInt32(txtSolicitud.Text.Trim());
                }

                if (!string.IsNullOrEmpty(txtMonto.Text.Trim()))
                {
                    Monto = Convert.ToDecimal(txtMonto.Text.Trim());
                }

                if (Session["UsuarioId"] != null)
                {
                    int.TryParse(Session["UsuarioId"].ToString(), out IdUsuario);
                }

                TimeSpan ts = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                fchDevolucion = fchDevolucion.Value.Date + ts;

                wsSOPF.Devolucion devolucion = new wsSOPF.Devolucion();
                devolucion.idSolicitud = idSolicitud;
                devolucion.fechaPago = fchDevolucion;
                devolucion.montoPago = Monto;
                devolucion.idUsuario = IdUsuario;
                devolucion.comentarios = txtObserva.Text;

                wsSOPF.ResultadoOfboolean res = ws.GuardaDevolucion(devolucion);
                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);
                else
                {
                    ws.AplicarDevolucionesPendientes();

                    pnlDevolucionExito.Visible = true;
                    lblDevolucionMens.Text = res.Mensaje;
                }
            }
            txtSolicitud.Text = "";
            txtFechaDevolucion.Text = "";
            txtMonto.Text = "";
            txtObserva.Text = "";
        }
        catch (Exception er)
        {
        }
    }

    protected void btnCancelaGuardaIdSolicitud_Click(object sender, EventArgs e)
    {
        lblIdDevolucionNoIdentificada.Value = "";
        lblMonto.Text = "";
        lblFechaDevolucion.Text = "";
        txtIdSolicitud.Text = "";
        lblCanalPago.Text = "";
        pnlGuardaSolicitud.Visible = false;
        pnlCarga_BuscarDevoluciones.Visible = true;
        pnlErrorBusqueda.Visible = false;
    }

    protected void CustomersGridView_SelectedIndexChanged(Object sender, EventArgs e)
    {
        GridViewRow row = gvDevoluciones.SelectedRow;

        lblIdDevolucionNoIdentificada.Value = row.Cells[1].Text;
        lblMonto.Text = row.Cells[3].Text;
        lblFechaDevolucion.Text = row.Cells[4].Text;
        lblCanalPago.Text = row.Cells[2].Text;

        pnlGuardaSolicitud.Visible = true;
        pnlCarga_BuscarDevoluciones.Visible = false;
    }

    protected void tcDevolucionesNoIndentificadas_ActiveTabChanged(object sender, EventArgs e)
    {
        switch (tcDevolucionesNoIndentificadas.ActiveTab.HeaderText.Trim())
        {
            case "Buscar":
                pnlError_BuscarDevoluciones.Visible = false;
                pnlDevolucionExito.Visible = false;
                lblErrorSubirArchivodevoluciones.Visible = false;
                pnlExito_ArchivosCargados.Visible = false;
                break;
            case "Carga Masiva":
                pnlGuardaSoliExito.Visible = false;
                pnlDevolucionExito.Visible = false;
                pnlErrorBusqueda.Visible = false;

                break;
            case "Carga Individual":
                pnlGuardaSoliExito.Visible = false;
                lblErrorSubirArchivodevoluciones.Visible = false;
                pnlExito_ArchivosCargados.Visible = false;
                pnlErrorBusqueda.Visible = false;
                break;
        }
    }

    protected void gvDevoluciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDevoluciones.PageIndex = e.NewPageIndex;
        buscarDevoluciones();
    }

    #endregion

    private class DevolucionResumen
    {
        public int noLinea { get; set; }
        public int idSolicitud { get; set; }
        public decimal monto { get; set; }
        public DateTime fecha { get; set; }
        public string comentarios { get; set; }
    }
}
