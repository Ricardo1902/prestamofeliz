﻿<%@ Page Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="frmDomiciliacionArchivos.aspx.cs" Inherits="Site_Creditos_frmDomiciliacionArchivos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolKit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">           
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Archivos Respuesta Domiciliacion</h3>
        </div>
        <div class="panel-body">
            <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
                <ProgressTemplate>                 
                    <div id="loader-background"></div>
                    <div id="loader-content"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upMain" runat="server">
                <ContentTemplate>
                    <ajaxtoolKit:TabContainer ID="tcDomiciliacionArchivos" CssClass="actTab" OnActiveTabChanged="tcDomiciliacionArchivos_ActiveTabChanged" AutoPostBack="true" runat="server">
                        <ajaxtoolKit:TabPanel HeaderText="Cargar TXT" runat="server">
                            <ContentTemplate>
                                <div class="form-group col-sm-12 col-md-7">           
                                    <label>Tipo de Archivo</label>
                                    <asp:dropdownlist id="ddlTipoArchivo" DataValueField="IdTipoArchivo" DataTextField="CustomDisplayField" CssClass="form-control" runat="server">                   				                                                                
                                    </asp:dropdownlist>
                                </div>

                                <div class="form-group col-sm-12 col-md-5"> 
                                    <label>Archivo a Cargar</label>                                          
                                    <asp:FileUpload ID="fuArchivo" CssClass="form-control" runat="server" />
                                    <asp:RegularExpressionValidator
                                        id="RegularExpressionValidator1"                                    
                                        ErrorMessage="El archivo debe ser un TXT"
                                        ValidationExpression ="^.+(.txt|.TXT)$"
                                        ControlToValidate="fuArchivo" ValidationGroup="CargarArchivo" CssClass="small" ForeColor="Red" Display="Dynamic"
                                        runat="server"></asp:RegularExpressionValidator>
                                </div>
           
                                <div class="form-group col-sm-3 col-md-3">                                
                                    <input type="button" onclick="javascript: document.getElementById('<%=btnCargarArchivo.ClientID%>').click();" class="btn btn-sm btn-primary" value="Cargar Archivo" />
                                </div>

                                <asp:Panel ID="pnlError_CargarTXT" class="col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <asp:Label ID="lblError_CargarTXT" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel> 
                                <asp:Panel ID="pnlExito_CargarTXT" class="col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-info" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <asp:Label ID="lblExito_CargarTXT" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel> 
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                        <ajaxtoolKit:TabPanel HeaderText="TXT Aprobados" runat="server">
                            <ContentTemplate>                                                       
                                <div class="form-group col-sm-12">
                                    <div style="height:auto; width: auto; overflow-x: auto;">
                                        <asp:Panel ID="pnlAprobadosGV" runat="server">
                                            <asp:GridView ID="gvArchivosAprobados" 
                                                OnRowCommand="gvArchivosAprobados_RowCommand"
                                                OnPageIndexChanging="gvArchivosAprobados_PageIndexChanging"
                                                runat="server" Width="100%" AutoGenerateColumns="False"
                                                Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                CssClass="table table-bordered table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                                PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                Style="overflow-x:auto;">
                                                <PagerStyle CssClass="pagination-ty" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnDetalleArchivo" CommandName="VerDetalle" CommandArgument='<%# Eval("IdArchivo") %>' CssClass="btn btn-sm btn-secondary" ToolTip="Ver Archivo" runat="server">
                                                                <i class="fas fa-file-alt fa-lg"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Entidad Financiera" DataField="TipoArchivo.EntidadFinanciera" />
                                                    <asp:BoundField HeaderText="Nombre" DataField="Nombre" />
                                                    <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaRegistro" />                                            
                                                    <asp:BoundField HeaderText="Cobros" DataField="TotalRegistros" />
                                                    <asp:BoundField HeaderText="Total Cobrado" DataField="TotalMonto" DataFormatString="{0:C2}" />                                                               
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No se encontraron Archivos Aprobados
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlAprobadosDetalle" runat="server">
                                            <div class="form-group col-xs-12">
                                                <asp:LinkButton ID="lbtnRegresar_Aprobados" CssClass="btn btn-sm btn-default" OnClick="lbtnRegresar_Aprobados_Click" runat="server">
                                                    <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                                                </asp:LinkButton>
                                                 <asp:LinkButton ID="lbtnDescargar_Aprobados" CssClass="btn btn-sm btn-default" OnClick="lbtnDescargar_Aprobados_Click" runat="server">
                                                    <i class="fas fa-download fa-lg"></i> Descargar
                                                </asp:LinkButton>
                                                <asp:Panel ID="pnlDescargaError_Aprobados" class="col-sm-12" Visible="false" runat="server">                                
                                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                        No se puede descargar el archivo, ha sido movido o eliminado
                                                    </div>
                                                </asp:Panel> 
                                                 <hr />
                                            </div>                                                                              

                                            <div class="form-group col-sm-6">
                                                <label>Nombre Archivo</label>           
                                                <pre><asp:Label ID="lblNombreArchivo_Aprobados" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6">   
                                                <label>Tipo Archivo</label>        
                                                <pre><asp:Label ID="lblTipoArchivo_Aprobados" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6"> 
                                                <label>Entidad Financiera</label>          
                                                <pre><asp:Label ID="lblEntidadFinanciera_Aprobados" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6"> 
                                                <label>Fecha Carga</label>          
                                                <pre><asp:Label ID="lblFechaRegistro_Aprobados" runat="server"></asp:Label></pre>
                                            </div>

                                            <br />
                                            <div class="col-sm-12">
                                                <blockquote style="background-color:#F8F8F8;">
                                                    <p>Contenido del Archivo</p>
                                                </blockquote>
                                            </div>
                                            <div class="form-group col-xs-12" style="overflow:auto; background-color:#fcf8e3;">
                                            
                                                <asp:Label ID="lblDetalleArchivo_Aprobados" CssClass="small" runat="server"></asp:Label>
                                            </div>
                                        </asp:Panel>
                                    </div>  
                                </div>                            
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                        <ajaxtoolKit:TabPanel HeaderText="TXT Rechazados" runat="server">
                            <ContentTemplate>
                                <div class="form-group col-sm-12">
                                    <div style="height:auto; width: auto; overflow-x: auto;">
                                        <asp:Panel ID="pnlRechazadosGV" runat="server">
                                            <asp:GridView ID="gvArchivosRechazados" 
                                                OnRowCommand="gvArchivosRechazados_RowCommand"
                                                OnPageIndexChanging="gvArchivosRechazados_PageIndexChanging"
                                                runat="server" Width="100%" AutoGenerateColumns="False"
                                                Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                CssClass="table table-bordered table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                                PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                Style="overflow-x:auto;">
                                                <PagerStyle CssClass="pagination" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnDetalleArchivo" CommandName="VerDetalle" CommandArgument='<%# Eval("IdArchivo") %>' CssClass="btn btn-sm btn-secondary" ToolTip="Ver Archivo" runat="server">
                                                                <i class="fas fa-file-alt fa-lg"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Entidad Financiera" DataField="TipoArchivo.EntidadFinanciera" />
                                                    <asp:BoundField HeaderText="Nombre" DataField="Nombre" />
                                                    <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaRegistro" />                                            
                                                    <asp:BoundField HeaderText="Cobros" DataField="TotalRegistros" />
                                                    <asp:BoundField HeaderText="Total Rechazado" DataField="TotalMonto" DataFormatString="{0:C2}" />                                                               
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No se encontraron Archivos Rechazados
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlRechazadosDetalle" runat="server">
                                            <div class="form-group col-xs-12">
                                                <asp:LinkButton ID="lbtnRegresar_Rechazados" CssClass="btn btn-sm btn-default" OnClick="lbtnRegresar_Rechazados_Click" runat="server">
                                                    <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                                                </asp:LinkButton>
                                                 <asp:LinkButton ID="lbtnDescargar_Rechazados" CssClass="btn btn-sm btn-default" OnClick="lbtnDescargar_Rechazados_Click" runat="server">
                                                    <i class="fas fa-download fa-lg"></i> Descargar
                                                </asp:LinkButton>
                                                <asp:Panel ID="pnlDescargaError_Rechazados" class="col-sm-12" Visible="false" runat="server">                                
                                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                        No se puede descargar el archivo, ha sido movido o eliminado
                                                    </div>
                                                </asp:Panel> 
                                                 <hr />
                                            </div>                                                                              

                                            <div class="form-group col-sm-6">
                                                <label>Nombre Archivo</label>           
                                                <pre><asp:Label ID="lblNombreArchivo_Rechazados" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6">   
                                                <label>Tipo Archivo</label>        
                                                <pre><asp:Label ID="lblTipoArchivo_Rechazados" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6"> 
                                                <label>Entidad Financiera</label>          
                                                <pre><asp:Label ID="lblEntidadFinanciera_Rechazados" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6"> 
                                                <label>Fecha Carga</label>          
                                                <pre><asp:Label ID="lblFechaRegistro_Rechazados" runat="server"></asp:Label></pre>
                                            </div>

                                            <br />
                                            <div class="col-sm-12">
                                                <blockquote style="background-color:#F8F8F8;">
                                                    <p>Contenido del Archivo</p>
                                                </blockquote>
                                            </div>
                                            <div class="form-group col-xs-12" style="overflow:auto; background-color:#fcf8e3;">                                            
                                                <asp:Label ID="lblDetalleArchivo_Rechazados" CssClass="small" runat="server"></asp:Label>
                                            </div>
                                        </asp:Panel>
                                    </div>  
                                </div>    
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>                    
                        <ajaxtoolKit:TabPanel HeaderText="Archivos con Error" runat="server">
                            <ContentTemplate>
                                <div class="form-group col-sm-12">
                                    <div style="height:auto; width: auto; overflow-x: auto;">
                                        <asp:Panel ID="pnlErrorGV" runat="server">
                                            <asp:GridView ID="gvArchivosError" 
                                                OnRowCommand="gvArchivosError_RowCommand"
                                                OnPageIndexChanging="gvArchivosError_PageIndexChanging"
                                                runat="server" Width="100%" AutoGenerateColumns="False"
                                                Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                CssClass="table table-bordered table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                                PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                Style="overflow-x:auto;">
                                                <PagerStyle CssClass="pagination" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnDetalleArchivo" CommandName="VerDetalle" CommandArgument='<%# Eval("IdArchivo") %>' CssClass="btn btn-sm btn-secondary" ToolTip="Ver Archivo" runat="server">
                                                                <i class="fas fa-file-alt fa-lg"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                          
                                                    <asp:BoundField HeaderText="Entidad Financiera" DataField="TipoArchivo.EntidadFinanciera" />
                                                    <asp:BoundField HeaderText="Nombre" DataField="Nombre" />
                                                    <asp:BoundField HeaderText="Tipo" DataField="TipoArchivo.Descripcion" />
                                                    <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaRegistro" />                                                                                        
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No se encontraron Archivos con Error
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlErrorDetalle" runat="server">
                                            <div class="form-group col-xs-12">
                                                <asp:LinkButton ID="lbtnRegresar_Error" CssClass="btn btn-sm btn-default" OnClick="lbtnRegresar_Error_Click" runat="server">
                                                    <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                                                </asp:LinkButton>
                                                 <asp:LinkButton ID="lbtnDescargar_Error" CssClass="btn btn-sm btn-default" OnClick="lbtnDescargar_Error_Click" runat="server">
                                                    <i class="fas fa-download fa-lg"></i> Descargar
                                                </asp:LinkButton>
                                                <asp:Panel ID="pnlDescargaError_Error" class="col-sm-12" Visible="false" runat="server">                                
                                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                        No se puede descargar el archivo, ha sido movido o eliminado
                                                    </div>
                                                </asp:Panel> 
                                                 <hr />
                                            </div>                                                                              

                                            <div class="form-group col-sm-6">
                                                <label>Nombre Archivo</label>           
                                                <pre><asp:Label ID="lblNombreArchivo_Error" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6">   
                                                <label>Tipo Archivo</label>        
                                                <pre><asp:Label ID="lblTipoArchivo_Error" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6"> 
                                                <label>Entidad Financiera</label>          
                                                <pre><asp:Label ID="lblEntidadFinanciera_Error" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6"> 
                                                <label>Fecha Carga</label>          
                                                <pre><asp:Label ID="lblFechaRegistro_Error" runat="server"></asp:Label></pre>
                                            </div>

                                            <br />
                                            <div class="col-sm-12">
                                                <blockquote style="background-color:#F8F8F8;">
                                                    <p>Contenido del Archivo</p>
                                                </blockquote>
                                            </div>
                                            <div class="form-group col-xs-12" style="overflow:auto; background-color:#fcf8e3;">                                            
                                                <asp:Label ID="lblDetalleArchivo_Error" CssClass="small" runat="server"></asp:Label>
                                            </div>
                                        </asp:Panel>
                                    </div>  
                                </div> 
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                    </ajaxtoolKit:TabContainer>
                    <div style="visibility:hidden">                        
                        <asp:Button ID="btnCargarArchivo" ValidationGroup="CargarArchivo" OnClick="btnCargarArchivo_Click" runat="server" />
                    </div>
                </ContentTemplate>
                <Triggers>                    
                    <asp:PostBackTrigger ControlID="btnCargarArchivo" />
                </Triggers>           
            </asp:UpdatePanel>                                               
        </div>
    </div>           
</asp:Content>
