﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Site_Creditos_frmDomiciliacionArchivos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Botones de descarga Full Postback para evitar que el Update Panel Falle
        ScriptManager.GetCurrent(this).RegisterPostBackControl(lbtnDescargar_Aprobados);
        ScriptManager.GetCurrent(this).RegisterPostBackControl(lbtnDescargar_Rechazados);
        ScriptManager.GetCurrent(this).RegisterPostBackControl(lbtnDescargar_Error);

        if (!Page.IsPostBack)
        {
            // Ocultamos los paneles de detalle de inicio
            pnlAprobadosDetalle.Visible = false;
            pnlRechazadosDetalle.Visible = false;
            pnlErrorDetalle.Visible = false;

            CargarCombos();            
        }
    }

    #region "Domiciliacion"

    protected void btnCargarArchivo_Click(object sender, EventArgs e)
    {
        pnlExito_CargarTXT.Visible = false;
        pnlError_CargarTXT.Visible = false;

        try
        {
            using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
            {
                // Validar antes de cargar el archivo
                if (!fuArchivo.HasFile)
                    throw new Exception("Seleccione el Archivo a cargar");
                else if (ws.DomiciliacionArchivo_Filtrar(new wsSOPF.ArchivoMasivo() { Nombre = fuArchivo.FileName.Trim() }).Count() > 0)
                    throw new Exception("Ya existe un Archivo con el mismo nombre");


                // Guardar el archivo a la carpeta local
                string folderPath = Server.MapPath("~/Archivos/DomiciliacionCargaMasiva/");
                fuArchivo.SaveAs(folderPath + Path.GetFileName(fuArchivo.FileName));

                // Copiamos el archivo al servidor de BD
                string localURL = folderPath + Path.GetFileName(fuArchivo.FileName);
                string remoteURL = System.Configuration.ConfigurationManager.AppSettings["URLRedArchivoMasivo"];
                string remoteUser = System.Configuration.ConfigurationManager.AppSettings["UsuarioArchivoMasivo"];
                string remotePass = System.Configuration.ConfigurationManager.AppSettings["ClaveArchivoMasivo"];

                NetworkShare.DisconnectFromShare(remoteURL, true); // Nos desconectamos en caso de que estemos actualmente conectados

                NetworkShare.ConnectToShare(remoteURL, remoteUser, remotePass); // Nos conectamos con las nuevas credenciales

                File.Copy(localURL, remoteURL + fuArchivo.FileName, true);

                NetworkShare.DisconnectFromShare(remoteURL, false); // Nos desconectamos del server

                // Procesar el archivo
                wsSOPF.ArchivoMasivo gEntity = new wsSOPF.ArchivoMasivo();

                gEntity.TipoArchivo = new wsSOPF.TipoArchivo() { IdTipoArchivo = int.Parse(ddlTipoArchivo.SelectedValue) };
                gEntity.Nombre = fuArchivo.FileName.Trim();

                wsSOPF.ResultadoOfboolean res = ws.DomiciliacionArchivo_Registrar(gEntity);

                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);

                // Enviar Mensaje de archivo procesado correctamente
                lblExito_CargarTXT.Text = "El Archivo " + Path.GetFileName(fuArchivo.FileName) + " ha sido cargado";
                pnlExito_CargarTXT.Visible = true;
            } 
        }
        catch (Exception ex)
        {
            lblError_CargarTXT.Text = ex.Message;
            pnlError_CargarTXT.Visible = true;
        }
    }

    protected void tcDomiciliacionArchivos_ActiveTabChanged(object sender, EventArgs e)
    {
        switch (tcDomiciliacionArchivos.ActiveTab.HeaderText.Trim())
        {
            case "Cargar TXT":
                pnlError_CargarTXT.Visible = false;
                pnlExito_CargarTXT.Visible = false;
                break;
            case "TXT Aprobados":
                CargarArchivosAprobados();

                pnlAprobadosGV.Visible = true;
                pnlAprobadosDetalle.Visible = false;
                pnlDescargaError_Aprobados.Visible = false;
                break;
            case "TXT Rechazados":
                CargarArchivosRechazados();

                pnlRechazadosGV.Visible = true;
                pnlRechazadosDetalle.Visible = false;
                pnlDescargaError_Rechazados.Visible = false;
                break;
            case "Archivos con Error":
                CargarArchivosError();

                pnlErrorGV.Visible = true;
                pnlErrorDetalle.Visible = false;
                pnlDescargaError_Error.Visible = false;
                break;
        }
    }

    #region "Archivos Aprobados"

    protected void lbtnRegresar_Aprobados_Click(object sender, EventArgs e)
    {
        pnlAprobadosGV.Visible = true;
        pnlAprobadosDetalle.Visible = false;
        pnlDescargaError_Aprobados.Visible = false;
    }

    protected void lbtnDescargar_Aprobados_Click(object sender, EventArgs e)
    {
        if (File.Exists(Server.MapPath("~/Archivos/DomiciliacionCargaMasiva/") + lblNombreArchivo_Aprobados.Text.Trim()))
            Response.Redirect(string.Format("~/DownloadFile.ashx?fileName={0}", Server.MapPath("~/Archivos/DomiciliacionCargaMasiva/") + lblNombreArchivo_Aprobados.Text.Trim()));
        else
            pnlDescargaError_Aprobados.Visible = true;
    }        
    
    protected void gvArchivosAprobados_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "VerDetalle":
                using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
                {
                    wsSOPF.ArchivoMasivo archivo = ws.DomiciliacionArchivo_ObtenerDetalle(int.Parse(e.CommandArgument.ToString()));

                    lblNombreArchivo_Aprobados.Text = archivo.Nombre;
                    lblTipoArchivo_Aprobados.Text = archivo.TipoArchivo.Descripcion;
                    lblEntidadFinanciera_Aprobados.Text = archivo.TipoArchivo.EntidadFinanciera;
                    lblFechaRegistro_Aprobados.Text = archivo.FechaRegistro.GetValueOrDefault().ToString();

                    lblDetalleArchivo_Aprobados.Text = string.Empty;
                    foreach (wsSOPF.ArchivoMasivoDetalle d in archivo.Detalle)
                    {
                        lblDetalleArchivo_Aprobados.Text += d.Descripcion.Replace(" ", "&nbsp;") + "<br/>";
                    }

                    pnlAprobadosGV.Visible = false;
                    pnlAprobadosDetalle.Visible = true;                                       
                }
                break;
        }
    }

    protected void gvArchivosAprobados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvArchivosAprobados.PageIndex = e.NewPageIndex;
        CargarArchivosAprobados();
    }

    #endregion

    #region "Archivos Rechazados"

    protected void lbtnRegresar_Rechazados_Click(object sender, EventArgs e)
    {
        pnlRechazadosGV.Visible = true;
        pnlRechazadosDetalle.Visible = false;
        pnlDescargaError_Rechazados.Visible = false;
    }

    protected void lbtnDescargar_Rechazados_Click(object sender, EventArgs e)
    {
        if (File.Exists(Server.MapPath("~/Archivos/DomiciliacionCargaMasiva/") + lblNombreArchivo_Rechazados.Text.Trim()))
            Response.Redirect(string.Format("~/DownloadFile.ashx?fileName={0}", Server.MapPath("~/Archivos/DomiciliacionCargaMasiva/") + lblNombreArchivo_Rechazados.Text.Trim()));
        else
            pnlDescargaError_Rechazados.Visible = true;
    }

    protected void gvArchivosRechazados_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "VerDetalle":
                using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
                {
                    wsSOPF.ArchivoMasivo archivo = ws.DomiciliacionArchivo_ObtenerDetalle(int.Parse(e.CommandArgument.ToString()));

                    lblNombreArchivo_Rechazados.Text = archivo.Nombre;
                    lblTipoArchivo_Rechazados.Text = archivo.TipoArchivo.Descripcion;
                    lblEntidadFinanciera_Rechazados.Text = archivo.TipoArchivo.EntidadFinanciera;
                    lblFechaRegistro_Rechazados.Text = archivo.FechaRegistro.GetValueOrDefault().ToString();

                    lblDetalleArchivo_Rechazados.Text = string.Empty;
                    foreach (wsSOPF.ArchivoMasivoDetalle d in archivo.Detalle)
                    {
                        lblDetalleArchivo_Rechazados.Text += d.Descripcion.Replace(" ", "&nbsp;") + "<br/>";
                    }

                    pnlRechazadosGV.Visible = false;
                    pnlRechazadosDetalle.Visible = true;
                }
                break;
        }
    }

    protected void gvArchivosRechazados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvArchivosRechazados.PageIndex = e.NewPageIndex;
        CargarArchivosRechazados();
    }

    #endregion

    #region "Archivos con Error"

    protected void lbtnRegresar_Error_Click(object sender, EventArgs e)
    {
        pnlErrorGV.Visible = true;
        pnlErrorDetalle.Visible = false;
        pnlDescargaError_Error.Visible = false;
    }

    protected void lbtnDescargar_Error_Click(object sender, EventArgs e)
    {
        if (File.Exists(Server.MapPath("~/Archivos/DomiciliacionCargaMasiva/") + lblNombreArchivo_Error.Text.Trim()))
            Response.Redirect(string.Format("~/DownloadFile.ashx?fileName={0}", Server.MapPath("~/Archivos/DomiciliacionCargaMasiva/") + lblNombreArchivo_Error.Text.Trim()));
        else
            pnlDescargaError_Error.Visible = true;
    }

    protected void gvArchivosError_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "VerDetalle":
                using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
                {
                    wsSOPF.ArchivoMasivo archivo = ws.DomiciliacionArchivo_ObtenerDetalle(int.Parse(e.CommandArgument.ToString()));

                    lblNombreArchivo_Error.Text = archivo.Nombre;
                    lblTipoArchivo_Error.Text = archivo.TipoArchivo.Descripcion;
                    lblEntidadFinanciera_Error.Text = archivo.TipoArchivo.EntidadFinanciera;
                    lblFechaRegistro_Error.Text = archivo.FechaRegistro.GetValueOrDefault().ToString();

                    lblDetalleArchivo_Error.Text = string.Empty;
                    foreach (wsSOPF.ArchivoMasivoDetalle d in archivo.Detalle)
                    {
                        lblDetalleArchivo_Error.Text += d.Descripcion.Replace(" ", "&nbsp;") + "<br/>";
                    }

                    pnlErrorGV.Visible = false;
                    pnlErrorDetalle.Visible = true;
                }
                break;
        }
    }

    protected void gvArchivosError_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvArchivosError.PageIndex = e.NewPageIndex;
        CargarArchivosError();
    }

    #endregion

    #endregion 

    #region "Metodos"

    private void CargarCombos()
    {
        // Cargar ddl tipo archivo domiciliacion
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.TipoArchivo> tipoRespuestaDomiciliacion = ws.TipoArchivo_Filtrar(new wsSOPF.TipoArchivo() { Descripcion = "RESPUESTA DOMICILIACION" }).ToList();

            var ds = from x in tipoRespuestaDomiciliacion
                     select new
                     {
                         x.IdTipoArchivo,
                         CustomDisplayField = string.Format("{0} ({1})", x.Descripcion, x.EntidadFinanciera)
                     };


            ddlTipoArchivo.DataSource = ds;
            ddlTipoArchivo.DataBind();
        }
    }

    private void CargarArchivosAprobados()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.ArchivoMasivo> archivosProcesados = ws.DomiciliacionArchivo_ObtenerProcesados().ToList();

            gvArchivosAprobados.DataSource = archivosProcesados;
            gvArchivosAprobados.DataBind();
        }
    }

    private void CargarArchivosRechazados()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.ArchivoMasivo> archivosRechazados = ws.DomiciliacionArchivo_ObtenerRechazados().ToList();

            gvArchivosRechazados.DataSource = archivosRechazados;
            gvArchivosRechazados.DataBind();
        }
    }

    private void CargarArchivosError()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.ArchivoMasivo> archivosError = ws.DomiciliacionArchivo_ObtenerConError().ToList();

            gvArchivosError.DataSource = archivosError;
            gvArchivosError.DataBind();
        }
    }

    #endregion               
}