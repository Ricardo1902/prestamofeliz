﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmGenerarLayout.aspx.cs" Inherits="Site_Creditos_frmGenerarLayout" MasterPageFile="~/Site.master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolKit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../Content/bootstrap-datetimepicker.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="../../Scripts/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Scripts/bootstrap-datetimepicker.js"></script>
    <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="150" runat="server">
        <ProgressTemplate>
            <!-- Lo deshabilito ya que hay funcionalidad de Descarga
            <div id="loader-background"></div>
            <div id="loader-content"></div>
            -->
        </ProgressTemplate>
    </asp:UpdateProgress> 
    <div class="panel panel-primary">
        <div class="panel-heading">
             <asp:UpdatePanel ID="upTitulo" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <h3 class="panel-title"><asp:Label ID="lblTitulo" Text="Layouts Cobranza" runat="server"></asp:Label></h3>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="panel-body">            
            <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>                               
                <ajaxtoolKit:TabContainer ID="tcMain" CssClass="actTab" runat="server">
                    <ajaxtoolKit:TabPanel ID="tpLayoutsGenerados" Visible="true" HeaderText="Layouts Generados" runat="server">
                        <ContentTemplate>
                            <!-- PANEL LAYOUTS GENERADOS -->
                            <asp:Panel ID="pnlLayoutsGenerados_Buscar" Visible="True" runat="server">                                
                                <asp:Panel ID="pnlExito_LayoutsGenerados_Buscar" class="col-xs-12 alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;" Visible="false" runat="server">
                                    <asp:Label ID="lblExito_LayoutsGenerados_Buscar" runat="server"></asp:Label>
                                </asp:Panel>

                                <asp:Panel ID="pnlError_LayoutsGenerados_Buscar" class="col-xs-12 alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;" Visible="false" runat="server">
                                    <asp:Label ID="lblError_LayoutsGenerados_Buscar" runat="server"></asp:Label>
                                </asp:Panel>

                                <div class="form-group col-sm-12">
                                    <div class="form-group col-md-4 col-lg-6">
                                        <label class="form-control-label small">Fecha Proceso Inicio</label>                                
                                        <div>     
                                            <div class="input-group">
                                                <asp:TextBox ID="txtFechaRegistroDesde" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10" Enabled="false" runat="server"/>                    
                                                <span class="input-group-addon">
                                                    <asp:LinkButton ID="btnFechaRegistroDesde" runat="server">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </asp:LinkButton>
                                                </span>
                                            </div>                                                         
                                            <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaRegistroDesde" PopupButtonID="btnFechaRegistroDesde" runat="server"></ajaxtoolKit:CalendarExtender>                                            
                                        </div>         
                                    </div>

                                    <div class="form-group col-md-4 col-lg-6">
                                        <label class="form-control-label small">Fecha Proceso Fin</label>                                
                                        <div>     
                                            <div class="input-group">
                                                <asp:TextBox ID="txtFechaRegistroHasta" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10" Enabled="false" runat="server"/>                    
                                                <span class="input-group-addon">
                                                    <asp:LinkButton ID="btnFechaRegistroHasta" runat="server">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </asp:LinkButton>
                                                </span>
                                            </div>                                                         
                                            <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaRegistroHasta" PopupButtonID="btnFechaRegistroHasta" runat="server"></ajaxtoolKit:CalendarExtender>                                            
                                        </div>         
                                    </div>

                                    <div class="col-xs-12 col-xs-8 col-md-6 col-lg-3">
                                        <asp:LinkButton ID="btnBuscar_LayoutsGenerados_Buscar" OnClick="btnBuscar_LayoutsGenerados_Buscar_Click" CssClass="btn btn-sm btn-default" runat="server">
                                            <i class="fas fa-search"></i> Buscar
                                        </asp:LinkButton>
                                    </div>
                                </div>

                                <div class="form-group col-sm-12">
                                    <asp:GridView ID="gvLayoutsGenerados" runat="server" Width="100%" AutoGenerateColumns="False"
                                        Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"                                                                
                                        OnPageIndexChanging="gvLayoutsGenerados_Buscar_PageIndexChanging"
                                        OnRowCommand="gvLayoutsGenerados_Buscar_RowCommand"
                                        PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"                            
                                        Style="overflow-x:auto;">                                        
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEditar" CommandName="Detalle" CommandArgument='<%# Eval("IdLayoutGenerado") %>' data-toggle="tooltip" title="Ver Detalle" runat="server">
                                                        <i class="fas fa-external-link-alt"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Grupo" DataField="GrupoLayout.Nombre" ItemStyle-Width="60%"/>
                                            <asp:BoundField HeaderText="Fecha Proceso" DataField="FechaRegistro.Value" DataFormatString="{0:dd/MM/yyyy HH:mm}"/>  
                                            <asp:BoundField HeaderText="Estatus" DataField="Estatus.EstatusDesc"/>                                                            
                                            <asp:CheckBoxField HeaderText="Auto." DataField="EsAutomatico" ItemStyle-HorizontalAlign="Center"/> 
                                        </Columns>                                        
                                        <PagerStyle CssClass="pagination-ty warning" />                                                                                
                                        <EmptyDataTemplate>
                                            No se encontraron Layouts Generados
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </asp:Panel>
                            <!-- PANEL LAYOUT GENERADO DETALLE -->
                            <asp:Panel ID="pnlLayoutsGenerados_Detalle" Visible="false" runat="server">                                
                                <div class="col-sm-12">                                    
                                    <asp:Panel ID="pnlError_LayoutsGenerados_Detalle" class="form-group col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <asp:Label ID="lblError_LayoutsGenerados_Detalle" runat="server"></asp:Label>
                                        </div>
                                        <hr />
                                    </asp:Panel> 
                                   
                                    <asp:LinkButton ID="lbtnRegresar_LayoutsGenerados_Detalle" OnClick="lbtnRegresar_LayoutsGenerados_Detalle_Click" CssClass="btn btn-sm btn-default" runat="server">
                                       <i class="fas fa-arrow-alt-circle-left"></i> Regresar
                                    </asp:LinkButton>

                                    <hr />
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-group col-md-12 col-lg-6">
                                        <label class="small">Nombre Grupo</label>
                                        <pre><asp:Label ID="lblNombreGrupo_LayoutsGenerados_Detalle" runat="server"></asp:Label></pre>
                                    </div> 
                                    <div class="form-group col-md-4 col-lg-3">
                                        <label class="small">Total Registros</label>
                                        <pre><asp:Label ID="lblTotalRegistros_LayoutsGenerados_Detalle" runat="server"></asp:Label></pre>
                                    </div> 
                                    <div class="form-group col-md-4 col-lg-3">
                                        <label class="small">Total a Cobrar</label>
                                        <pre><asp:Label ID="lblTotalCobrar_LayoutsGenerados_Detalle" runat="server"></asp:Label></pre>
                                    </div> 
                                </div>

                                <div class="col-xs-12">
                                    <h4>Plantillas Procesadas</h4>
                                    <hr />
                                </div>

                                <div class="col-xs-12">
                                    <asp:GridView ID="gvPlantillas_LayoutsGenerados_Detalle" runat="server" Width="100%" AutoGenerateColumns="False"
                                        Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="false"
                                        OnRowCommand="gvPlantillas_LayoutsGenerados_Detalle_RowCommand"
                                        OnRowDataBound="gvPlantillas_LayoutsGenerados_Detalle_RowDataBound"
                                        RowStyle-Wrap="false" HeaderStyle-Wrap="false"                            
                                        Style="overflow-x:auto;">                                        
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnDescargarLayout" CommandName="DescargarLayout" CommandArgument='<%# Eval("IdPlantillaEnvioCobro") %>' data-toggle="tooltip" title="Descargar Layout" runat="server">
                                                        <i class="fas fa-download"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Plantilla" DataField="NombrePlantilla" ItemStyle-Width="60%"/>
                                            <asp:BoundField HeaderText="Total Registros" DataField="TotalRegistros" />  
                                            <asp:BoundField HeaderText="MontoTotal" DataField="MontoTotal" DataFormatString="{0:c}"/>                                                            
                                            <asp:BoundField HeaderText="Num. Descargas" DataField="VecesGenerado" /> 
                                        </Columns>                                        
                                        <PagerStyle CssClass="pagination-ty warning" />                                                                                
                                        <EmptyDataTemplate>
                                            No hay Plantillas Procesadas
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </asp:Panel>                            
                        </ContentTemplate>
                    </ajaxtoolKit:TabPanel>
                    <ajaxtoolKit:TabPanel ID="tpConfigurarGrupos" HeaderText ="Configurar Grupos" runat="server">
                        <ContentTemplate>
                            <!-- PANEL BUSCAR -->
                            <asp:Panel ID="pnlBuscar_Administrar" runat="server">
                                <asp:Panel ID="pnlExito_Buscar" class="form-group col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <asp:Label ID="lblExito_Buscar" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlError_Buscar" class="form-group col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <asp:Label ID="lblError_Buscar" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>

                                <div class="col-md-4 col-lg-3">
                                    <asp:LinkButton ID="lbtnAgregarGrupo" OnClick="lbtnAgregarGrupo_Click" CssClass="btn btn-sm btn-success" runat="server">
                                       <i class="fas fa-file-medical" style="font-size:1.4em"></i> Crear Nuevo Grupo
                                    </asp:LinkButton>
                                </div>
                    
                                <div class="col-sm-12">
                                    <hr />
                                </div>

                                <div class="form-group col-md-4 col-lg-4">
                                    <label class="small">Nombre</label>
                                    <asp:TextBox ID="txtNombreGrupo_Buscar" MaxLength="200" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <div class="form-group col-md-4 col-lg-4">
                                    <label class="small">Ver Grupos Activos</label>
                                    <asp:DropDownList ID="ddlActivo_Buscar" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="-1" Text="Todos"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="clearfix"></div>
                    
                                <div class="form-group col-md-4 col-lg-3">
                                    <asp:LinkButton ID="lbtnBuscar" OnClick="lbtnBuscar_Click" CssClass="btn btn-sm btn-default" runat="server">
                                       <i class="fas fa-search" style="font-size:1.4em"></i> Buscar
                                    </asp:LinkButton>
                                </div>
                            
                                <div class="form-group col-sm-12">
                                    <asp:Panel ID="pnlAdvertencia_ProcesarGrupo" CssClass="alert alert-warning col-md-12 col-sm-12" style="padding:4px 20px;" role="alert" Visible="false" runat="server">
                                        <asp:HiddenField ID="hdnIdProcesarGrupo" runat="server"></asp:HiddenField>
                                        <asp:Label ID="lblAdvertencia_ProcesarGrupo" runat="server" Text=""></asp:Label>
                                        <asp:Button ID="btnConfirmar_ProcesarGrupo" Text="Si" CssClass="btn btn-sm btn-success" runat="server" OnClick="btnConfirmar_ProcesarGrupo_Click" />
                                        <asp:Button ID="btnCancelar_ProcesarGrupo" Text="No" CssClass="btn btn-sm btn-danger" runat="server" OnClick="btnCancelar_ProcesarGrupo_Click" />
                                    </asp:Panel>

                                    <asp:GridView ID="gvResultado_Buscar" runat="server" Width="100%" AutoGenerateColumns="False"
                                        Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"                                                                
                                        OnPageIndexChanging="gvResultado_Buscar_PageIndexChanging"
                                        OnRowCommand="gvResultado_Buscar_RowCommand"
                                        PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"                            
                                        Style="overflow-x:auto;">                                        
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEditar" CommandName="Editar" CommandArgument='<%# Eval("IdGrupoLayout") %>' data-toggle="tooltip" title="Editar" runat="server">
                                                        <i class="far fa-edit"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lbtnProcesarGrupo" CommandName="ProcesarGrupo" CommandArgument='<%# Eval("IdGrupoLayout") %>' data-toggle="tooltip" title="ProcesarGrupo" runat="server">
                                                        <i class="fas fa-cog"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Nombre" DataField="Nombre" ItemStyle-Width="60%"/>                                            
                                            <asp:CheckBoxField HeaderText="Activo" DataField="Activo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"/> 
                                        </Columns>                                        
                                        <PagerStyle CssClass="pagination-ty warning" />                                                                                
                                        <EmptyDataTemplate>
                                            No se encontraron Grupos
                                        </EmptyDataTemplate>
                                    </asp:GridView>                                                
                                </div>                          
                            </asp:Panel>
                            <!-- TERMINA PANEL BUSCAR -->
                            
                            <!-- PANEL GUARDAR -->
                            <asp:Panel ID="pnlGuardar_Administrar" Visible="false" runat="server">                                      
                                <div class="form-group col-md-8 col-lg-8">
                                    <label class="small">Nombre Grupo</label>
                                    <asp:TextBox ID="txtNombreGrupo_Guardar" MaxLength="200" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>                                                                                                  

                                <div class="col-md-2 col-lg-2">
                                    <!-- Rounded switch -->
                                    <div>
                                        <label class="small">Activo</label>
                                    </div>
                                    <label class="switch">
                                        <asp:CheckBox ID="chkActivo" Checked="true" runat="server" />
                                        <span class="slider round"></span>
                                    </label>                        
                                </div> 
                                
                                <div class="col-md-2 col-lg-2 hidden">
                                    <!-- Rounded switch -->
                                    <div>
                                        <label class="small">Usar Cuenta Emergente</label>
                                    </div>
                                    <label class="switch">
                                        <asp:CheckBox ID="chkUsarCuentaEmergente" Checked="false" Visible="false" runat="server" />
                                        <span class="slider round"></span>
                                    </label>                        
                                </div> 

                                <div class="clearfix"></div>                                

                                <div class="form-group col-sm-12">                        
                                    <ajaxtoolKit:TabContainer ID="tcConfiguracion" CssClass="actTab"  runat="server">
                                        <ajaxtoolKit:TabPanel ID="tpPlantillas" HeaderText="Plantillas" runat="server">
                                            <ContentTemplate>
                                                <div class="col-sm-12">
                                                    <h4>Agregar Plantillas al Grupo</h4>
                                                    <hr style="margin-top:0;" />
                                                </div>
                                                
                                                 <div class="form-group col-md-3 col-lg-3">
                                                    <label class="small">Nombre</label>
                                                     <div class="col-sm-12 input-group">
                                                        <asp:TextBox ID="txtNombrePlantilla_Guardar" MaxLength="200" CssClass="form-control" runat="server"></asp:TextBox>
                                                         <span class="input-group-addon">
                                                            <asp:LinkButton ID="lbtnBuscarPlantilla_Guardar" OnClick="lbtnBuscarPlantilla_Guardar_Click" data-toggle="tooltip" title="Buscar" runat="server"><i class="fas fa-search"></i></asp:LinkButton>                                
                                                        </span>                            
                                                    </div>
                                                </div>

                                                <div class="col-md-3 col-lg-3">
                                                    <label class="small">Layout Cobro</label>
                                                    <asp:DropDownList ID="ddlTipoLayout_Guardar" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoLayout_Guardar_SelectedIndexChanged" DataValueField="IdTipoLayoutCobro" DataTextField="TipoLayout" AppendDataBoundItems="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Text="Todos" Value="-1"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="form-group col-md-6 col-lg-6">
                                                    <label class="small">Plantilla</label>
                                                    <div class="input-group">
                                                        <asp:DropDownList ID="ddlPlantilla_Guardar" DataValueField="IdPlantilla" DataTextField="Nombre" CssClass="form-control" runat="server">                                        
                                                        </asp:DropDownList>
                                                        <span class="input-group-addon">
                                                            <asp:LinkButton ID="lbtnAgregarPlantilla_Guardar" OnClick="lbtnAgregarPlantilla_Guardar_Click" data-toggle="tooltip" title="Agregar Plantilla" runat="server"><i class="fas fa-plus-square"></i></asp:LinkButton>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>  

                                                <div class="col-sm-12">
                                                    <asp:Repeater ID="rptPlantillas_Guardar" OnItemCommand="rptPlantillas_Guardar_ItemCommand" runat="server">
                                                        <ItemTemplate>  
                                                            <asp:HiddenField ID="hdnIdPlantilla" Value='<%# Eval("Plantilla.IdPlantilla") %>' runat="server" />
                                                            <div class="col-md-6 col-lg-4">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">                                                    
                                                                        PLANTILLA
                                                                        <div class="btn-group pull-right">                                                                           
                                                                            <asp:LinkButton ID="lbtnEliminarPlantilla_Guardar" data-toggle="tooltip" title="Eliminar Plantilla" CommandName="EliminarPlantilla" CommandArgument='<%# Eval("Plantilla.IdPlantilla") %>' runat="server">
                                                                                <i class="fas fa-times" style="color:tomato;"></i>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body" style="min-height:75px;">                                                                                                                            
                                                                        <div class="col-sm-12">
                                                                            <label><%# Eval("Plantilla.Nombre") %></label>
                                                                        </div>
                                                                        <div class="input-group col-sm-12">
                                                                             <span class="input-group-addon">
                                                                                Dias Vencimiento
                                                                            </span>
                                                                            <asp:TextBox ID="txtDiasVencimiento" Text='<%# Eval("DiasVencimiento") %>' CssClass="form-control" OnTextChanged="txtDiasVencimiento_TextChanged" AutoPostBack="true" onKeyPress="return EvaluateText('%d', this);" MaxLength="2" runat="server"></asp:TextBox>
                                                                        </div>
                                                                    </div>                                                
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxtoolKit:TabPanel>
                                        <ajaxtoolKit:TabPanel ID="tpDiasEjecucion" HeaderText="Dias Ejecucion" runat="server">
                                            <ContentTemplate>
                                                 <div class="col-sm-12">
                                                    <h4>Configurar Dias de Ejecucion</h4>
                                                    <hr style="margin-top:0;" />
                                                </div>
                                               
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_1" CssClass="small" Text="1" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_2" CssClass="small" Text="2" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_3" CssClass="small" Text="3" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_4" CssClass="small" Text="4" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_5" CssClass="small" Text="5" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_6" CssClass="small" Text="6" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_7" CssClass="small" Text="7" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_8" CssClass="small" Text="8" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_9" CssClass="small" Text="9" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_10" CssClass="small" Text="10" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_11" CssClass="small" Text="11" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_12" CssClass="small" Text="12" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_13" CssClass="small" Text="13" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_14" CssClass="small" Text="14" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_15" CssClass="small" Text="15" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_16" CssClass="small" Text="16" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_17" CssClass="small" Text="17" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_18" CssClass="small" Text="18" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_19" CssClass="small" Text="19" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_20" CssClass="small" Text="20" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_21" CssClass="small" Text="21" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_22" CssClass="small" Text="22" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_23" CssClass="small" Text="23" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_24" CssClass="small" Text="24" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_25" CssClass="small" Text="25" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_26" CssClass="small" Text="26" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_27" CssClass="small" Text="27" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_28" CssClass="small" Text="28" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_29" CssClass="small" Text="29" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_30" CssClass="small" Text="30" runat="server" /></div>
                                                <div class="col-sm-1"><asp:CheckBox ID="chkDiaEjecucion_31" CssClass="small" Text="31" runat="server" /></div>
                                                <div class="col-sm-12"><asp:CheckBox ID="chkDiaEjecucion_ULTIMO_DIA_MES" CssClass="small" Text="ULTIMO DIA MES" runat="server" /></div>                                                      
                                            </ContentTemplate>
                                        </ajaxtoolKit:TabPanel>
                                        <ajaxtoolKit:TabPanel ID="tpProgramaciones" HeaderText="Programaciones" Visible="true" runat="server">
                                            <ContentTemplate>
                                                 <div class="col-sm-12">
                                                    <h4>Programaciones</h4>
                                                    <hr style="margin-top:0;" />
                                                </div>

                                                <asp:Panel ID="pnlProgramacion_Administrar" runat="server">
                                                    <asp:Panel ID="pnlExito_Programacion" class="form-group col-sm-12" Visible="false" runat="server">
                                                        <div class="alert alert-success" style="padding: 5px 15px 5px 15px; margin: 10px 0px;">
                                                            <asp:Label ID="lblExito_Programacion" runat="server"></asp:Label>
                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="pnlError_Programacion" class="form-group col-sm-12" Visible="false" runat="server">
                                                        <div class="alert alert-danger" style="padding: 5px 15px 5px 15px; margin: 10px 0px;">
                                                            <asp:Label ID="lblError_Programacion" runat="server"></asp:Label>
                                                        </div>
                                                    </asp:Panel>

                                                    <div class="col-md-4 col-lg-3">
                                                        <asp:LinkButton ID="lbtnAgregarProgramacion" OnClick="lbtnAgregarProgramacion_Click" CssClass="btn btn-sm btn-success" runat="server">
                                                           <i class="fas fa-file-medical" style="font-size:1.4em"></i> Crear Nueva Programacion
                                                        </asp:LinkButton>
                                                    </div>

                                                    <div class="col-md-2 col-md-offset-6">
                                                    <asp:LinkButton ID="lbActualizarProgramacion" OnClick="lbActualizarProgramacion_Click" CssClass="btn btn-sm btn-info" runat="server">
                                                        <i class="fas fa-retweet" style="font-size:1.4em"></i> Actualizar
                                                    </asp:LinkButton>
                                                </div>

                                                    <div class="col-sm-12">
                                                        <hr />
                                                    </div>

                                                    <div class="col-xs-12">
                                                        <asp:GridView ID="gvProgramaciones" runat="server" Width="100%" AutoGenerateColumns="False"
                                                            Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                                            OnPageIndexChanging="gvProgramaciones_PageIndexChanging"
                                                            OnRowCommand="gvProgramaciones_RowCommand"
                                                            PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                            Style="overflow-x: auto;">
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lbtnEditar" CommandName="Editar" CommandArgument='<%# Eval("IdProgramacion") %>' data-toggle="tooltip" title="Editar" runat="server">
                                                                            <i class="far fa-edit"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Tipo" DataField="TipoProgramacion.TipoProgramacionDescripcion" />
                                                                <asp:BoundField HeaderText="Registro" DataField="FechaRegistro" DataFormatString="{0:dd/MM/yyyy}" />
                                                                <asp:BoundField HeaderText="FechaInicio" DataField="FechaInicio" DataFormatString="{0:dd/MM/yyyy}" />
                                                                <asp:BoundField HeaderText="FechaFin" DataField="FechaTermino" DataFormatString="{0:dd/MM/yyyy}" />
                                                                <asp:BoundField HeaderText="Inicio" DataField="HoraInicio" DataFormatString="{0:HH:mm}" />
                                                                <asp:BoundField HeaderText="Fin" DataField="HoraTermino" DataFormatString="{0:HH:mm}" />
                                                                <asp:TemplateField HeaderText="Intervalo">
                                                                    <ItemTemplate><%# Math.Truncate((decimal)Eval("ValorIntervalo"))%> <%# Eval("Intervalo.Intervalo").ToString().ToLower().Substring(0, 3)%></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Intentos" DataField="Intentos" />
                                                                <asp:CheckBoxField HeaderText="Moroso" DataField="EsGrupoMoroso" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                                <asp:CheckBoxField HeaderText="Activo" DataField="Activo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                            </Columns>
                                                            <PagerStyle CssClass="pagination-ty warning" />
                                                            <EmptyDataTemplate>
                                                                No se encontraron programaciones
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="pnlProgramacion_Detalle" runat="server">
                                                    <div class="form-group col-md-4 col-lg-4">
                                                        <label class="small">Tipo Programación</label>
                                                        <asp:DropDownList ID="ddlTipoProgramacion" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoProgramacion_SelectedIndexChanged" DataValueField="IdTipoProgramacion" DataTextField="TipoProgramacionDescripcion" AppendDataBoundItems="true" CssClass="form-control" runat="server" />
                                                    </div>

                                                    <div class="col-md-1 col-lg-1">
                                                        <div>
                                                            <label class="small">Activo</label>
                                                        </div>
                                                        <label class="switch">
                                                            <asp:CheckBox ID="chkActivoProgramacion" Checked="true" runat="server" />
                                                            <span class="slider round"></span>
                                                        </label>                        
                                                    </div> 
                                
                                                    <div class="col-md-1 col-lg-1">
                                                        <div>
                                                            <label class="small">Moroso</label>
                                                        </div>
                                                        <label class="switch">
                                                            <asp:CheckBox ID="chkMorosoProgramacion" runat="server" />
                                                            <span class="slider round"></span>
                                                        </label>                        
                                                    </div> 

                                                    <div class="form-group col-md-2 col-lg-2">
                                                        <label class="small">Valor Intervalo</label>
                                                        <asp:TextBox ID="txtValorIntervalo" CssClass="form-control" runat="server" OnTextChanged="txtValorIntervalo_TextChanged" AutoPostBack="true" onKeyPress="return EvaluateText('%f', this);"></asp:TextBox>
                                                    </div>

                                                    <div class="form-group col-md-2 col-lg-2">
                                                        <label class="small">Tipo Intervalo</label>
                                                        <asp:DropDownList ID="ddlTipoIntervalo" AutoPostBack="true" DataValueField="IdIntervalo" DataTextField="Intervalo" AppendDataBoundItems="true" CssClass="form-control" runat="server" />
                                                    </div>

                                                    <div class="form-group col-md-2 col-lg-2">
                                                        <label class="small">Intentos</label>
                                                        <asp:TextBox ID="txtIntentos" CssClass="form-control" runat="server" OnTextChanged="RevisaIntentosMaximo" AutoPostBack="true" onKeyPress="return EvaluateText('%d', this);"></asp:TextBox>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="form-group col-md-2 col-lg-3">
                                                        <label class="form-control-label small">Fecha Inicio</label>                                
                                                        <div>     
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtFechaInicioProgramacion" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10" Enabled="true" runat="server" OnTextChanged="ValidarHorario" AutoPostBack="true" />
                                                                <span class="input-group-addon">
                                                                    <asp:LinkButton ID="lbFechaInicioProgramacion" runat="server">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </div>                                                         
                                                            <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaInicioProgramacion" PopupButtonID="lbFechaInicioProgramacion" runat="server"></ajaxtoolKit:CalendarExtender>                                            
                                                        </div>         
                                                    </div>

                                                    <div class="form-group col-md-2 col-lg-3">
                                                        <label class="form-control-label small">Fecha Fin</label>                                
                                                        <div>     
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtFechaFinProgramacion" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10" Enabled="true" runat="server" OnTextChanged="ValidarHorario" AutoPostBack="true" />
                                                                <span class="input-group-addon">
                                                                    <asp:LinkButton ID="lbFechaFinProgramacion" runat="server" Enabled="true">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </div>                                                         
                                                            <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaFinProgramacion" PopupButtonID="lbFechaFinProgramacion" runat="server"></ajaxtoolKit:CalendarExtender>                                            
                                                        </div>         
                                                    </div>

                                                    <div class="form-group col-md-2 col-lg-3">
                                                        <label for="txtInicio">Hora Inicio (23:59)</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span
                                                                class="glyphicon glyphicon-time"></span></span>
                                                            <asp:TextBox type="text" ID="txtInicio" CssClass="form-control" runat="server" OnTextChanged="ValidarHorario" AutoPostBack="true" MaxLength="5"  />
                                                        </div>        
                                                    </div>

                                                    <div class="form-group col-md-2 col-lg-3">
                                                        <label for="txtFin">Hora Fin (23:59)</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span
                                                                class="glyphicon glyphicon-time"></span></span>
                                                            <asp:TextBox type="text" ID="txtFin" CssClass="form-control" runat="server" OnTextChanged="ValidarHorario" AutoPostBack="true" MaxLength="5"  />
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="pnlDiasProgramacion" runat="server">
                                                            <div class="col-sm-12">
                                                                <h6>Configurar Dias de Ejecucion</h6>
                                                                <hr style="margin-top:0;" />
                                                            </div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_1" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="1" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_2" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="2" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_3" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="3" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_4" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="4" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_5" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="5" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_6" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="6" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_7" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="7" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_8" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="8" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_9" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="9" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_10" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="10" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_11" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="11" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_12" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="12" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_13" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="13" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_14" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="14" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_15" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="15" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_16" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="16" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_17" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="17" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_18" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="18" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_19" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="19" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_20" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="20" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_21" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="21" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_22" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="22" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_23" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="23" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_24" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="24" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_25" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="25" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_26" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="26" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_27" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="27" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_28" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="28" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_29" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="29" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_30" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="30" runat="server" /></div>
                                                            <div class="col-sm-1"><asp:CheckBox ID="chkDiaProg_31" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="31" runat="server" /></div>
                                                            <div class="col-sm-12"><asp:CheckBox ID="chkDiaProg_32" OnCheckedChanged="RevisaIntentosMaximo" AutoPostBack="true" CssClass="small" Text="ULTIMO DIA MES" runat="server" /></div>
                                                        </asp:Panel>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </ajaxtoolKit:TabPanel>
                                    </ajaxtoolKit:TabContainer>
                                </div>                                

                                <div class="col-sm-12">
                                    <hr />
                                    <asp:Panel ID="pnlError_Guardar" class="form-group col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <asp:Label ID="lblError_Guardar" runat="server"></asp:Label>
                                        </div>
                                        <hr />
                                    </asp:Panel> 
                                    <asp:LinkButton ID="lbtnGuardar" OnClick="lbtnGuardar_Click" CssClass="btn btn-sm btn-success" runat="server">
                                       <i class="fas fa-save"></i> Guardar
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lbtnCancelar" OnClick="lbtnCancelar_Click" CssClass="btn btn-sm btn-danger" runat="server">
                                       <i class="fas fa-arrow-alt-circle-left"></i> Cancelar
                                    </asp:LinkButton>
                                </div>
                            </asp:Panel>
                            <!-- TERMINA PANEL GUARDAR -->
                        </ContentTemplate>
                    </ajaxtoolKit:TabPanel>                   
                </ajaxtoolKit:TabContainer>
            </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            // Activar Tooltips
            activarTooltips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            // Activar Tooltips
            activarTooltips();
        });
    </script>
    <script type="text/javascript">
       function pageLoad(sender, args) {
           $("#<%= txtInicio.ClientID %>").datetimepicker({ locale: 'es', format: 'HH:mm' });
           $("#<%= txtInicio.ClientID %>").on("dp.hide", function () { $("#<%= txtInicio.ClientID %>").trigger('change'); });
           $("#<%= txtFin.ClientID %>").datetimepicker({ locale: 'es', format: 'HH:mm' });
           $("#<%= txtFin.ClientID %>").on("dp.hide", function () { $("#<%= txtFin.ClientID %>").trigger('change'); });
       }  
    </script>
</asp:Content>
