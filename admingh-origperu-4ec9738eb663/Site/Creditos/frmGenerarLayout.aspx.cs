﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using wsSOPF;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

public partial class Site_Creditos_frmGenerarLayout : System.Web.UI.Page
{
    private string[] TIPOPROGRAMACION_RECURRENTES = { "Recurrente Día Mensual" };

    private enum Vista
    {
        GrupoLayout_Busqueda,
        GrupoLayout_Registrar,
        GrupoLayout_Actualizar,
        LayoutGenerado_Busqueda,
        LayoutGenerado_Detalle,
        Programacion_Administrar,
        Programacion_Registrar,
        Programacion_Actualizar
    }

    private Vista VistaPagina
    {
        get { return (ViewState[UniqueID + "_vistaPagina"] != null) ? (Vista)ViewState[this.UniqueID + "_vistaPagina"] : Vista.GrupoLayout_Busqueda; }
        set
        {
            ViewState[this.UniqueID + "_vistaPagina"] = value;

            this.lblTitulo.Text = "Grupos Layouts Cobranza";
            switch (this.VistaPagina)
            {
                case Vista.GrupoLayout_Busqueda:
                    this.lblTitulo.Text += " - Buscar";
                    pnlBuscar_Administrar.Visible = true;
                    pnlGuardar_Administrar.Visible = false;
                    pnlProgramacion_Administrar.Visible = false;
                    pnlProgramacion_Detalle.Visible = false;

                    tpLayoutsGenerados.Visible = true;
                    tcMain.ActiveTab = tpConfigurarGrupos;
                    break;
                case Vista.GrupoLayout_Registrar:
                    this.lblTitulo.Text += " - Registrar";
                    pnlBuscar_Administrar.Visible = false;
                    pnlGuardar_Administrar.Visible = true;
                    pnlProgramacion_Administrar.Visible = true;
                    pnlProgramacion_Detalle.Visible = false;

                    tpLayoutsGenerados.Visible = false;
                    break;
                case Vista.GrupoLayout_Actualizar:
                    this.lblTitulo.Text += " - Actualizar";
                    pnlBuscar_Administrar.Visible = false;
                    pnlGuardar_Administrar.Visible = true;
                    pnlProgramacion_Administrar.Visible = true;
                    pnlProgramacion_Detalle.Visible = false;

                    tpLayoutsGenerados.Visible = false;
                    break;
                case Vista.LayoutGenerado_Busqueda:
                    this.lblTitulo.Text += " - Buscar";
                    pnlLayoutsGenerados_Detalle.Visible = false;
                    pnlLayoutsGenerados_Buscar.Visible = true;

                    tcMain.ActiveTab = tpLayoutsGenerados;
                    break;
                case Vista.LayoutGenerado_Detalle:
                    this.lblTitulo.Text += " - Detalle";
                    pnlLayoutsGenerados_Detalle.Visible = true;
                    pnlLayoutsGenerados_Buscar.Visible = false;
                    break;
                case Vista.Programacion_Administrar:
                    this.lblTitulo.Text += " - Administrar";
                    pnlBuscar_Administrar.Visible = false;
                    pnlGuardar_Administrar.Visible = true;
                    pnlProgramacion_Administrar.Visible = true;
                    pnlProgramacion_Detalle.Visible = false;

                    tpLayoutsGenerados.Visible = false;
                    tcConfiguracion.ActiveTab = tpProgramaciones;
                    break;
                case Vista.Programacion_Registrar:
                    lblTitulo.Text += " - Registrar";
                    pnlBuscar_Administrar.Visible = false;
                    pnlGuardar_Administrar.Visible = true;
                    pnlProgramacion_Administrar.Visible = false;
                    pnlProgramacion_Detalle.Visible = true;

                    tpLayoutsGenerados.Visible = false;
                    break;
                case Vista.Programacion_Actualizar:
                    lblTitulo.Text += " - Actualizar";
                    pnlBuscar_Administrar.Visible = false;
                    pnlGuardar_Administrar.Visible = true;
                    pnlProgramacion_Administrar.Visible = false;
                    pnlProgramacion_Detalle.Visible = true;

                    tpLayoutsGenerados.Visible = false;
                    break;
            }
        }
    }

    private GrupoLayout GrupoLayout
    {
        get
        {
            if (ViewState[this.UniqueID + "_GrupoLayout"] != null)
            {
                return (GrupoLayout)ViewState[this.UniqueID + "_GrupoLayout"];
            }
            else
            {
                GrupoLayout g = new GrupoLayout
                {
                    Plantillas = new List<GrupoLayout.PlantillaGrupoLayout>().ToArray(),
                    DiasEjecucion = new List<GrupoLayout.ConfiguracionEjecucion>().ToArray(),
                    Programaciones = new List<GrupoLayout.ProgramacionGrupoLayout>().ToArray(),
                    IdUsuarioRegistro = int.Parse(Session["UsuarioId"].ToString())
                };

                return g;
            }
        }
        set
        {
            ViewState[this.UniqueID + "_GrupoLayout"] = value;
        }
    }

    private GrupoLayout.ProgramacionGrupoLayout Programacion
    {
        get
        {
            if (ViewState[UniqueID + "_Programacion"] != null)
            {
                return (GrupoLayout.ProgramacionGrupoLayout)ViewState[UniqueID + "_Programacion"];
            }
            else
            {
                GrupoLayout.ProgramacionGrupoLayout p = new GrupoLayout.ProgramacionGrupoLayout
                {
                    TipoProgramacion = new TipoProgramacion(),
                    Intervalo = new ProgramacionIntervalo(),
                    DiasEjecucion = new List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>().ToArray(),
                    IdUsuarioRegistro = int.Parse(Session["UsuarioId"].ToString())
                };
                return p;
            }
        }
        set
        {
            ViewState[UniqueID + "_Programacion"] = value;
        }
    }

    private int MaximoCobros
    {
        get { return ViewState[UniqueID + "_MaximoCobros"] != null ? (int)ViewState[UniqueID + "_MaximoCobros"] : 30; }
        set { ViewState[UniqueID + "_MaximoCobros"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtFechaRegistroDesde.Text = DateTime.Today.ToString("dd/MM/yyyy");
            txtFechaRegistroHasta.Text = DateTime.Today.ToString("dd/MM/yyyy");
            txtFechaInicioProgramacion.Text = DateTime.Today.ToString("dd/MM/yyyy");
            txtFechaFinProgramacion.Text = DateTime.Today.ToString("dd/MM/yyyy");
            txtInicio.Text = DateTime.Now.ToString("HH:mm");
            txtFin.Text = DateTime.Now.AddMinutes(1).ToString("HH:mm");

            this.VistaPagina = Vista.LayoutGenerado_Busqueda;

            using (CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
            {
                TB_CATParametro resMaximoCobros = ws.ObtenerMaximoCobrosProgramaciones("MCP");
                int val = 0;
                if (resMaximoCobros != null && resMaximoCobros.IdParametro > 0 && (int.TryParse(resMaximoCobros.valor, out val)))
                    MaximoCobros = val;
                else
                    MaximoCobros = 30;//default 30
            }
        }
    }


    #region "Layouts Generados"

    protected void btnBuscar_LayoutsGenerados_Buscar_Click(object sender, EventArgs e)
    {
        pnlError_LayoutsGenerados_Buscar.Visible = false;
        pnlExito_LayoutsGenerados_Buscar.Visible = false;

        try
        {
            CargarLayoutGenerados();
        }
        catch (Exception ex)
        {
            pnlError_LayoutsGenerados_Buscar.Visible = true;
            lblError_LayoutsGenerados_Buscar.Text = ex.Message;
        }
    }

    protected void gvLayoutsGenerados_Buscar_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        pnlError_LayoutsGenerados_Buscar.Visible = false;
        pnlExito_LayoutsGenerados_Buscar.Visible = false;

        try
        {
            switch (e.CommandName)
            {
                case "Detalle":
                    Detalle_Cargar_LayoutsGenerados(int.Parse(e.CommandArgument.ToString()));
                    break;
            }
        }
        catch (Exception ex)
        {
            lblError_LayoutsGenerados_Buscar.Text = ex.Message;
            pnlError_LayoutsGenerados_Buscar.Visible = true;
        }
    }

    protected void gvLayoutsGenerados_Buscar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvLayoutsGenerados.PageIndex = e.NewPageIndex;
        btnBuscar_LayoutsGenerados_Buscar_Click(sender, e);
    }

    protected void lbtnRegresar_LayoutsGenerados_Detalle_Click(object sender, EventArgs e)
    {
        pnlError_LayoutsGenerados_Buscar.Visible = false;
        pnlError_LayoutsGenerados_Detalle.Visible = false;
        this.VistaPagina = Vista.LayoutGenerado_Busqueda;
    }

    protected void gvPlantillas_LayoutsGenerados_Detalle_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ((LinkButton)e.Row.FindControl("lbtnDescargarLayout")).Visible = ((LayoutCobroLayoutGenerado.Plantilla)e.Row.DataItem).TotalRegistros > 0;
        }
    }

    protected void gvPlantillas_LayoutsGenerados_Detalle_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        pnlError_LayoutsGenerados_Buscar.Visible = false;
        pnlExito_LayoutsGenerados_Buscar.Visible = false;

        try
        {
            switch (e.CommandName)
            {
                case "DescargarLayout":
                    Response.Redirect(string.Format("~/DownloadFile.ashx?idPlantillaEnvioCobro={0}&UsuarioId={1}", int.Parse(e.CommandArgument.ToString()), Session["UsuarioId"].ToString()));
                    break;
            }
        }
        catch (Exception ex)
        {
            lblError_Buscar.Text = ex.Message;
            pnlError_Buscar.Visible = true;
        }
    }

    private void CargarLayoutGenerados()
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            DateTime dtInicio;
            DateTime dtFin;

            if (!DateTime.TryParseExact(txtFechaRegistroDesde.Text.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None, out dtInicio))
                throw new Exception("Seleccione Fecha de Inicio");

            if (!DateTime.TryParseExact(txtFechaRegistroHasta.Text.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None, out dtFin))
                throw new Exception("Seleccione Fecha de Fin");

            wsSOPF.LayoutCobroLayoutGenerado e = new LayoutCobroLayoutGenerado();
            e.IdUsuarioRegistro = int.Parse(Session["UsuarioId"].ToString());
            e.FechaRegistro = new UtilsDateTimeR();
            e.FechaRegistro.ValueIni = dtInicio;
            e.FechaRegistro.ValueEnd = dtFin;

            List<LayoutCobroLayoutGenerado> layouts = ws.LayoutGenerado_Filtrar(e).ToList().OrderByDescending(x => x.FechaRegistro.Value).ToList();

            gvLayoutsGenerados.DataSource = layouts;
            gvLayoutsGenerados.DataBind();
        }
    }

    private void Detalle_Cargar_LayoutsGenerados(int IdLayoutGenerado)
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            wsSOPF.LayoutCobroLayoutGenerado layoutGenerado = ws.LayoutGenerado_ObtenerDetalle(IdLayoutGenerado);

            lblNombreGrupo_LayoutsGenerados_Detalle.Text = layoutGenerado.GrupoLayout.Nombre;
            lblTotalRegistros_LayoutsGenerados_Detalle.Text = layoutGenerado.Plantillas.Sum(x => x.TotalRegistros).ToString();
            lblTotalCobrar_LayoutsGenerados_Detalle.Text = layoutGenerado.Plantillas.Sum(x => x.MontoTotal).ToString("C", CultureInfo.CurrentCulture);

            gvPlantillas_LayoutsGenerados_Detalle.DataSource = layoutGenerado.Plantillas;
            gvPlantillas_LayoutsGenerados_Detalle.DataBind();

            this.VistaPagina = Vista.LayoutGenerado_Detalle;
        }
    }

    #endregion


    #region "Buscar"

    protected void lbtnAgregarGrupo_Click(object sender, EventArgs e)
    {
        pnlExito_Buscar.Visible = false;
        pnlError_Buscar.Visible = false;
        pnlError_Guardar.Visible = false;
        pnlExito_Programacion.Visible = false;
        pnlError_Programacion.Visible = false;

        CargarCombos_Guardar();

        this.GrupoLayout = null;

        if (GrupoLayout.IdGrupoLayout == null || GrupoLayout.IdGrupoLayout <= 0)
            tpProgramaciones.Visible = false;

        txtNombreGrupo_Guardar.Text = string.Empty;
        chkActivo.Checked = true;
        txtNombrePlantilla_Guardar.Text = string.Empty;
        tcConfiguracion.ActiveTabIndex = 0;

        ddlPlantilla_Guardar.Items.Clear();

        rptPlantillas_Guardar.DataSource = null;
        rptPlantillas_Guardar.DataBind();

        // Deschecar todos los Checkbox de Dias Ejecucion
        foreach (Control c in tpDiasEjecucion.Controls[0].Controls)
        {
            if (c is CheckBox)
            {
                ((CheckBox)c).Checked = false;
            }
        }

        //Programacion = null;
        //gvProgramaciones.DataSource = null;
        //gvProgramaciones.DataBind();
        //CargarCombosProgramaciones();
        //ddlTipoProgramacion.SelectedIndex = 0;
        //ddlTipoProgramacion_SelectedIndexChanged(sender, e);
        //chkActivoProgramacion.Checked = true;
        //chkMorosoProgramacion.Checked = false;
        //txtValorIntervalo.Text = "1";
        //ddlTipoIntervalo.SelectedIndex = 0;
        //txtIntentos.Text = "1";
        //string hoy = DateTime.Now.ToString("dd/MM/yyyy");
        //string ahora = DateTime.Now.ToString("HH:mm");
        //txtFechaInicioProgramacion.Text = hoy;
        //txtFechaFinProgramacion.Text = hoy;
        //txtInicio.Text = ahora;
        //txtFin.Text = ahora;
        //AlternarDiasProgramacion(false, false);

        this.VistaPagina = Vista.GrupoLayout_Registrar;
    }

    protected void lbtnBuscar_Click(object sender, EventArgs e)
    {
        pnlExito_Buscar.Visible = false;
        pnlError_Buscar.Visible = false;
        pnlAdvertencia_ProcesarGrupo.Visible = false;

        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
            {
                wsSOPF.GrupoLayout p = new GrupoLayout();

                p.Nombre = txtNombreGrupo_Buscar.Text.Trim();
                if (int.Parse(ddlActivo_Buscar.SelectedValue.ToString()) >= 0)
                    p.Activo = int.Parse(ddlActivo_Buscar.SelectedValue.ToString()) == 1;

                ResultadoOfArrayOfGrupoLayout1rkJJdIU res = ws.GrupoLayout_Buscar(p);

                if (res.Codigo > 0)
                    throw new Exception(res.Mensaje);

                gvResultado_Buscar.DataSource = res.ResultObject;
                gvResultado_Buscar.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblError_Buscar.Text = ex.Message;
            pnlError_Buscar.Visible = true;
        }
    }

    protected void btnConfirmar_ProcesarGrupo_Click(object sender, EventArgs e)
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            GrupoLayout.PeticionProcesar p = new GrupoLayout.PeticionProcesar();
            p.IdGrupoLayout = int.Parse(hdnIdProcesarGrupo.Value);
            p.IdUsuarioProcesa = int.Parse(Session["UsuarioId"].ToString());
            p.EsAutomatico = false;

            ws.GrupoLayout_Procesar(p);
        }

        pnlAdvertencia_ProcesarGrupo.Visible = false;
        hdnIdProcesarGrupo.Value = string.Empty;

        this.VistaPagina = Vista.LayoutGenerado_Busqueda;
        btnBuscar_LayoutsGenerados_Buscar_Click(sender, e);
        pnlExito_LayoutsGenerados_Buscar.Visible = true;
        lblExito_LayoutsGenerados_Buscar.Text = "Generado archivos de Cobranza!";
    }

    protected void btnCancelar_ProcesarGrupo_Click(object sender, EventArgs e)
    {
        pnlAdvertencia_ProcesarGrupo.Visible = false;
        hdnIdProcesarGrupo.Value = string.Empty;
    }

    protected void gvResultado_Buscar_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        pnlExito_Buscar.Visible = false;
        pnlError_Buscar.Visible = false;
        pnlError_Guardar.Visible = false;

        try
        {
            switch (e.CommandName)
            {
                case "Editar":
                    Detalle_Cargar(int.Parse(e.CommandArgument.ToString()));
                    break;
                case "ProcesarGrupo":
                    using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
                    {
                        wsSOPF.ResultadoOfGrupoLayout1rkJJdIU grupoLayout = ws.GrupoLayout_Obtener(int.Parse(e.CommandArgument.ToString()));

                        hdnIdProcesarGrupo.Value = e.CommandArgument.ToString();

                        pnlAdvertencia_ProcesarGrupo.Visible = true;
                        lblAdvertencia_ProcesarGrupo.Text = string.Format("¿Generar archivos del Grupo: {0}?", grupoLayout.ResultObject.Nombre);
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            lblError_Buscar.Text = ex.Message;
            pnlError_Buscar.Visible = true;
        }
    }

    protected void gvResultado_Buscar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvResultado_Buscar.PageIndex = e.NewPageIndex;
        lbtnBuscar_Click(sender, e);
    }

    private void Detalle_Cargar(int IdGrupoLayout)
    {
        pnlExito_Buscar.Visible = false;
        pnlError_Buscar.Visible = false;
        pnlError_Guardar.Visible = false;

        txtNombrePlantilla_Guardar.Text = string.Empty;
        tcConfiguracion.ActiveTabIndex = 0;
        ddlPlantilla_Guardar.Items.Clear();

        CargarCombos_Guardar();

        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            wsSOPF.ResultadoOfGrupoLayout1rkJJdIU res = ws.GrupoLayout_Obtener(IdGrupoLayout);

            if (res.Codigo > 0)
                throw new Exception(res.Mensaje);

            this.GrupoLayout = res.ResultObject;

            AlternarPestañaProgramaciones();

            // Cargar datos principales
            txtNombreGrupo_Guardar.Text = this.GrupoLayout.Nombre.Trim();
            chkActivo.Checked = this.GrupoLayout.Activo.GetValueOrDefault();
            chkUsarCuentaEmergente.Checked = this.GrupoLayout.UsarCuentaEmergente.GetValueOrDefault();

            // Cargar Dias Ejecucion
            foreach (Control chk in tpDiasEjecucion.Controls[0].Controls)
            {
                if (chk is CheckBox)
                    ((CheckBox)chk).Checked = this.GrupoLayout.DiasEjecucion.ToList().Any(x => x.Dia == chk.ID.Substring(chk.ID.IndexOf("_") + 1));
            }

            // Cargar las Plantillas
            rptPlantillas_Guardar.DataSource = this.GrupoLayout.Plantillas;
            rptPlantillas_Guardar.DataBind();

            // Cargar Programaciones
            CargarProgramaciones();

            switch(VistaPagina)
            {
                case Vista.Programacion_Registrar:
                case Vista.Programacion_Actualizar:
                case Vista.Programacion_Administrar:
                    this.VistaPagina = Vista.Programacion_Administrar;
                    break;
                default:
                    this.VistaPagina = Vista.GrupoLayout_Actualizar;
                    break;
            }

            ValidarProgramacionSoloVisanet();
        }
    }

    private void CancelarGrupoLayoutPantalla()
    {
        pnlAdvertencia_ProcesarGrupo.Visible = false;
        this.VistaPagina = Vista.GrupoLayout_Busqueda;
    }

    private void ValidarGuardar()
    {
        if (this.GrupoLayout.Nombre.Trim() == string.Empty)
            throw new Exception("Capture el nombre del Grupo");

        if (this.GrupoLayout.Plantillas.Length == 0)
            throw new Exception("Agregue al menos 1 Plantilla al Grupo");

        if (this.GrupoLayout.Programaciones.Length == 0 && this.GrupoLayout.DiasEjecucion.Length == 0)
            throw new Exception("Agregue al menos 1 Dia de Ejecucion al Grupo");
    }

    private void CargarCombos_Guardar()
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            // Cargar Combo Tipo Layout    
            List<wsSOPF.TipoLayoutCobro> tipoLayoutActivos = ws.TipoLayoutCobro_ObtenerActivos().ToList();

            // Eliminar los elementos menos el primero
            var toDelete = ddlTipoLayout_Guardar.Items
               .Cast<ListItem>()
               .Where(i => int.Parse(i.Value) > 0);

            toDelete.ToList().ForEach(i => ddlTipoLayout_Guardar.Items.Remove(i));

            // Recargar los Tipos Layout Activos
            ddlTipoLayout_Guardar.DataSource = tipoLayoutActivos;
            ddlTipoLayout_Guardar.DataBind();
        }
    }

    #endregion

    #region "Programaciones"
    protected void gvProgramaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProgramaciones.PageIndex = e.NewPageIndex;
    }

    protected void gvProgramaciones_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        pnlExito_Programacion.Visible = false;
        pnlError_Programacion.Visible = false;
        pnlError_Guardar.Visible = false;

        try
        {
            switch (e.CommandName)
            {
                case "Editar":
                    Detalle_CargarProgramacion(int.Parse(e.CommandArgument.ToString()));
                    this.VistaPagina = Vista.Programacion_Actualizar;
                    break;
                default:
                    break;
            }
        }
        catch (Exception ex)
        {
            lblError_Buscar.Text = ex.Message;
            pnlError_Buscar.Visible = true;
        }
    }

    private void Detalle_CargarProgramacion(int IdProgramacion)
    {
        pnlExito_Programacion.Visible = false;
        pnlError_Programacion.Visible = false;
        pnlError_Guardar.Visible = false;
        CargarCombosProgramaciones();
        ValidarProgramacionSoloVisanet();

        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            ResultadoOfGrupoLayoutProgramacionGrupoLayout1rkJJdIU res = ws.GrupoLayoutProgramacion_Obtener(IdProgramacion);

            if (res.Codigo > 0)
                throw new Exception(res.Mensaje);

            this.Programacion = res.ResultObject;

            ddlTipoProgramacion.SelectedValue = Programacion.TipoProgramacion.IdTipoProgramacion.ToString();
            ddlTipoProgramacion_SelectedIndexChanged(null, null);
            chkActivoProgramacion.Checked = Programacion.Activo;
            chkMorosoProgramacion.Checked = Programacion.EsGrupoMoroso;
            string valor = Programacion.ValorIntervalo.ToString();
            string valorMostrar = valor.Substring(valor.Length - 3).IndexOf(".00") == -1 ? valor : valor.Substring(0, valor.Length - 3);
            txtValorIntervalo.Text = valorMostrar;
            ddlTipoIntervalo.SelectedValue = Programacion.Intervalo.IdIntervalo.ToString();
            txtIntentos.Text = Programacion.Intentos.ToString();
            AlternarDiasProgramacion(true, false);
            SuperaIntentosMaximo();
            txtFechaInicioProgramacion.Text = Programacion.FechaInicio.ToString("dd/MM/yyyy");
            txtFechaFinProgramacion.Text = Programacion.FechaTermino.ToString("dd/MM/yyyy");
            DateTime di = Programacion.HoraInicio;
            txtInicio.Text = string.Concat(di.Hour.ToString("00"), ":", di.Minute.ToString("00"));
            DateTime dt = Programacion.HoraTermino;
            txtFin.Text = string.Concat(dt.Hour.ToString("00"), ":", dt.Minute.ToString("00"));
            ValidaFechaHorasInvalida();
        }
    }

    protected void ddlTipoProgramacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(!TIPOPROGRAMACION_RECURRENTES.Contains(ddlTipoProgramacion.SelectedItem.Text))
        {
            txtFechaFinProgramacion.Text = txtFechaInicioProgramacion.Text.Trim();
            txtFechaFinProgramacion.Enabled = false;
            lbFechaFinProgramacion.Enabled = false;
            pnlDiasProgramacion.Visible = false;
            AlternarDiasProgramacion(false, false);
        }
        else
        {
            txtFechaFinProgramacion.Enabled = true;
            lbFechaFinProgramacion.Enabled = true;
            txtFechaFinProgramacion.Text = Programacion.IdProgramacion > 0 ? Programacion.FechaTermino.ToString("dd/MM/yyyy") : DateTime.Now.AddDays(1).ToString("dd/MM/yyy");
            pnlDiasProgramacion.Visible = true;
            AlternarDiasProgramacion(true, false);
        }
    }

    protected void lbtnAgregarProgramacion_Click(object sender, EventArgs e)
    {
        if(!ValidarProgramacionSoloVisanet())
            return;
        else
            AlternarError(false, "");
        
        CargarCombosProgramaciones();
        Programacion = null;
        GrupoLayout.ProgramacionGrupoLayout p = new GrupoLayout.ProgramacionGrupoLayout();
        Programacion = p;
        ddlTipoProgramacion.SelectedIndex = 0;
        ddlTipoProgramacion_SelectedIndexChanged(sender, e);
        chkActivoProgramacion.Checked = true;
        chkMorosoProgramacion.Checked = false;
        txtValorIntervalo.Text = "1";
        ddlTipoIntervalo.SelectedIndex = 0;
        txtIntentos.Text = "1";
        string hoy = DateTime.Now.ToString("dd/MM/yyyy");
        string ahora = DateTime.Now.ToString("HH:mm");
        string ahora1 = DateTime.Now.AddMinutes(1).ToString("HH:mm");
        txtFechaInicioProgramacion.Text = hoy;
        txtFechaFinProgramacion.Text = hoy;
        txtInicio.Text = ahora;
        txtFin.Text = ahora1;
        AlternarDiasProgramacion(false, false);

        this.VistaPagina = Vista.Programacion_Registrar;
    }
    
    protected void ValidarHorario(object sender, EventArgs e)
    {
        ValidaFechaHorasInvalida();
    }

    private bool ValidaFechaHorasInvalida()
    {
        if (string.IsNullOrEmpty(txtFechaInicioProgramacion.Text)
            || string.IsNullOrEmpty(txtFechaFinProgramacion.Text)
            || string.IsNullOrEmpty(txtInicio.Text)
            || string.IsNullOrEmpty(txtFin.Text))
        {
            AlternarError(true, "Fechas y horarios no pueden ir vacíos.");
            return true;
        }
        DateTime? FechaInicio = null;
        DateTime? FechaFin = null;
        DateTime? HoraInicio = null;
        DateTime? HoraFin = null;
        double dias = 0;
        FechaInicio = ValidarFechaHora(txtFechaInicioProgramacion.Text.Trim(), string.Empty);
        if (!FechaInicio.HasValue)
        {
            AlternarError(true, "FechaInicio inválida.");
            return true;
        }
        FechaFin = ValidarFechaHora(txtFechaFinProgramacion.Text.Trim(), string.Empty);
        if (!FechaFin.HasValue)
        {
            AlternarError(true, "FechaFin inválida.");
            return true;
        }
        HoraInicio = ValidarFechaHora(string.Empty, txtInicio.Text.Trim());
        if (!HoraInicio.HasValue)
        {
            AlternarError(true, "HoraInicio inválida.");
            return true;
        }
        HoraFin = ValidarFechaHora(string.Empty, txtFin.Text.Trim());
        if (!HoraFin.HasValue)
        {
            AlternarError(true, "HoraFin inválida.");
            return true;
        }
        if (!TIPOPROGRAMACION_RECURRENTES.Contains(ddlTipoProgramacion.SelectedItem.Text))
        {
            txtFechaFinProgramacion.Text = txtFechaInicioProgramacion.Text.Trim();
            txtFechaFinProgramacion.Enabled = false;
            lbFechaFinProgramacion.Enabled = false;
        }
        else
        {
            txtFechaFinProgramacion.Enabled = true;
            lbFechaFinProgramacion.Enabled = true;
            TimeSpan difFechas = FechaFin.Value - FechaInicio.Value;
            dias = difFechas.TotalDays;
            if (dias < 1)
            {
                AlternarError(true, "La fecha fin debe de ser mínimo 1 día más que la fecha inicio.");
                return true;
            }
        }
        if (HoraInicio == HoraFin)
        {
            AlternarError(true, "La HoraInicio y HoraFin no pueden ser iguales.");
            return true;
        }
        if (HoraInicio.Value > HoraFin.Value)
        {
            AlternarError(true, "HoraInicio no puede ser mayor al HoraFin.");
            return true;
        }
        AlternarError(false, "");
        return false;
    }

    private void CancelarProgramacionPantalla()
    {
        //CANCELA CAMBIOS REALIZADOS Y RECARGA LO QUE ESTA GUARDADO EN CASO DE GUARDAR CAMBIOS DE GRUPO
        if (VistaPagina == Vista.Programacion_Actualizar)
            Detalle_CargarProgramacion(Programacion.IdProgramacion);
        this.VistaPagina = Vista.Programacion_Administrar;
    }

    private void GuardaProgramacion()
    {
        pnlError_Guardar.Visible = false;
        try
        {
            ValidarGuardarProgramacion();

            GrupoLayout.ProgramacionGrupoLayout programacion = this.Programacion;
            TipoProgramacion tipoProgramacion = this.Programacion.TipoProgramacion ?? new TipoProgramacion();
            ProgramacionIntervalo intervalo = this.Programacion.Intervalo ?? new ProgramacionIntervalo();
            programacion.IdGrupoLayout = this.GrupoLayout.IdGrupoLayout.Value;
            programacion.IdUsuarioRegistro = int.Parse(Session["UsuarioId"].ToString());
            programacion.Activo = chkActivoProgramacion.Checked;
            tipoProgramacion.IdTipoProgramacion = Convert.ToInt32(ddlTipoProgramacion.SelectedValue);
            programacion.TipoProgramacion = tipoProgramacion;
            programacion.Activo = chkActivoProgramacion.Checked;
            programacion.EsGrupoMoroso = chkMorosoProgramacion.Checked;
            decimal valInt = -1;
            if (!decimal.TryParse(txtValorIntervalo.Text.Trim(), out valInt) || valInt == -1)
                throw new Exception("Valor intervalo inválido.");
            programacion.ValorIntervalo = valInt;
            intervalo.IdIntervalo = Convert.ToInt32(ddlTipoIntervalo.SelectedValue);
            programacion.Intervalo = intervalo;
            int ints = 0;
            if (!int.TryParse(txtIntentos.Text.Trim(), out ints) || ints <= 0)
                throw new Exception("Valor de intentos inválido.");
            programacion.Intentos = ints;
            programacion.FechaInicio = DateTime.ParseExact(txtFechaInicioProgramacion.Text.Trim(), "dd/MM/yyyy", null);
            programacion.FechaTermino = DateTime.ParseExact(txtFechaFinProgramacion.Text.Trim(), "dd/MM/yyyy", null);
            programacion.HoraInicio = DateTime.ParseExact(string.Concat(DateTime.Now.ToString("dd/MM/yyyy"), " ", txtInicio.Text.Trim()), "dd/MM/yyyy HH:mm", null);
            programacion.HoraTermino = DateTime.ParseExact(string.Concat(DateTime.Now.ToString("dd/MM/yyyy"), " ", txtFin.Text.Trim()), "dd/MM/yyyy HH:mm", null);
            // Recuperar Dias Ejecucion
            List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion> diasEjecucion =
                new List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>();
            foreach (Control chk in pnlDiasProgramacion.Controls)
            {
                if (chk is CheckBox)
                {
                    if (((CheckBox)chk).Checked)
                    {
                        diasEjecucion.Add(new GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion() { Dia = Convert.ToInt32(chk.ID.Substring(chk.ID.IndexOf("_") + 1)) });
                    }
                }
            }
            programacion.DiasEjecucion = diasEjecucion.ToArray();
            this.Programacion = programacion;
            using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
            {
                ResultadoOfGrupoLayoutProgramacionGrupoLayout1rkJJdIU res = ws.Programacion_Guardar(this.Programacion);
                if (res.Codigo > 0)
                    throw new Exception(res.Mensaje);
            }
            VistaPagina = Vista.Programacion_Administrar;
            pnlExito_Programacion.Visible = true;
            lblExito_Programacion.Text = string.Format("Se guardo la programacion {0} correctamente", Programacion.TipoProgramacion.TipoProgramacionDescripcion);
            Detalle_Cargar(Convert.ToInt32(GrupoLayout.IdGrupoLayout));
            CargarProgramaciones();
        }
        catch (Exception ex)
        {
            lblError_Guardar.Text = string.IsNullOrEmpty(ex.Message) ? lblError_Guardar.Text : ex.Message;
            pnlError_Guardar.Visible = true;
        }
    }

    private void ValidarGuardarProgramacion()
    {
        if (GrupoLayout.IdGrupoLayout == null || GrupoLayout.IdGrupoLayout <= 0)
            throw new Exception("No existe grupo para dicha programación.");
        if(!ValidarProgramacionSoloVisanet())
            throw new Exception("");
        if (SuperaIntentosMaximo())
            throw new Exception("");
        if (ValidaFechaHorasInvalida())
            throw new Exception("");
        DateTime? FechaFin = null;
        FechaFin = ValidarFechaHora(txtFechaFinProgramacion.Text.Trim(), string.Empty);
        DateTime? HoraFin = null;
        HoraFin = ValidarFechaHora(string.Empty, txtFin.Text.Trim());
        DateTime FechaMaximaVencida = new DateTime(FechaFin.Value.Year, FechaFin.Value.Month, FechaFin.Value.Day, HoraFin.Value.Hour, HoraFin.Value.Minute, HoraFin.Value.Second);
        if (chkActivoProgramacion.Checked && (FechaMaximaVencida < DateTime.Now))
            throw new Exception("No se puede guardar una programación activa estando vencida, es decir, con FechaHoraFin menor a la fecha actual.");
        if (TIPOPROGRAMACION_RECURRENTES.Contains(ddlTipoProgramacion.SelectedItem.Text) && Programacion.DiasEjecucion.Length == 0)
            throw new Exception("Debe seleccionar al menos 1 día válido en el rango de ejecución.");
        bool sucedeAlMenosUnaVez = false;
        if (chkActivoProgramacion.Checked && TIPOPROGRAMACION_RECURRENTES.Contains(ddlTipoProgramacion.SelectedItem.Text) && Programacion.DiasEjecucion.Length > 0)
        {
            double diff = 0;
            int diaMin = Programacion.DiasEjecucion.Min(d => d.Dia);
            int diaMax = Programacion.DiasEjecucion.Max(d => d.Dia);
            DateTime? FechaInicio = null;
            FechaInicio = ValidarFechaHora(txtFechaInicioProgramacion.Text.Trim(), string.Empty);
            TimeSpan difFechas = FechaFin.Value - FechaInicio.Value;
            diff = difFechas.TotalDays;
            if (diff < 32)
            {
                int DayInterval = 1;
                int[] DiasSeleccionados = Programacion.DiasEjecucion.Select(r => r.Dia).ToArray();
                if(DiasSeleccionados.Contains(32))
                    DiasSeleccionados[Array.IndexOf(DiasSeleccionados, 32)] = DateTime.DaysInMonth(FechaFin.Value.Year, FechaFin.Value.Month);
                DateTime FinalDiaFinal = new DateTime(FechaFin.Value.Year, FechaFin.Value.Month, FechaFin.Value.Day, 23, 59, 59);
                while (FechaInicio.Value <= FinalDiaFinal)
                {
                    if(DiasSeleccionados.Contains(FechaInicio.Value.Day))
                    {
                        DateTime? HoraInicio = null;
                        HoraInicio = ValidarFechaHora(string.Empty, txtInicio.Text.Trim());
                        DateTime exacta = new DateTime(FechaInicio.Value.Year, FechaInicio.Value.Month, FechaInicio.Value.Day, HoraInicio.Value.Hour, HoraInicio.Value.Minute, HoraInicio.Value.Second);
                        sucedeAlMenosUnaVez = exacta > DateTime.Now;
                        if(sucedeAlMenosUnaVez)
                            break;
                    }
                    FechaInicio = FechaInicio.Value.AddDays(DayInterval);
                }
            }
            else
            {
                sucedeAlMenosUnaVez = true;
            }
            if(!sucedeAlMenosUnaVez)
                throw new Exception("No se puede activar una programación con una configuración que no permita su ejecución al menos una vez.");
        }
    }

    private bool ValidarProgramacionSoloVisanet()
    {
        //TODO: validar con peru si desactivamos en automatico este escenario
        if (GrupoLayout.Plantillas.Length > 0 && GrupoLayout.Plantillas.Any(p => p.TipoLayoutStr != "VISANET ONLINE"))
        {
            AlternarError(true, "No se pueden crear programaciones por que el grupo tiene plantillas que no son exclusivamente VISANET.");
            return false;
        }
        else
        {
            return true;
        }
    }

    protected void lbActualizarProgramacion_Click(object sender, EventArgs e)
    {
        Detalle_Cargar(Convert.ToInt32(GrupoLayout.IdGrupoLayout));
        CargarProgramaciones();
    }

    private void CargarProgramaciones()
    {
        gvProgramaciones.DataSource = this.GrupoLayout.Programaciones.ToList().OrderByDescending(x => x.FechaRegistro).ToList();
        gvProgramaciones.DataBind();
    }

    protected void txtValorIntervalo_TextChanged(object sender, EventArgs e)
    {
        SuperaIntentosMaximo();
    }

    private void CargarCombosProgramaciones()
    {
        ddlTipoProgramacion.Items.Clear();
        ddlTipoIntervalo.Items.Clear();
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            List<TipoProgramacion> tipoProgramaciones = ws.TipoProgramacion_ObtenerActivos().ToList();
            ddlTipoProgramacion.DataSource = tipoProgramaciones;
            ddlTipoProgramacion.DataBind();
            List<ProgramacionIntervalo> tipoIntervalos = ws.Intervalos_ObtenerActivos().ToList();
            ddlTipoIntervalo.DataSource = tipoIntervalos;
            ddlTipoIntervalo.DataBind();
        }
    }

    private void AlternarDiasProgramacion(bool carga, bool cargarVS)
    {
        if(cargarVS)
        {
            List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion> diasEjecucion = new List<GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion>();
            foreach (Control chk in pnlDiasProgramacion.Controls)
            {
                if (chk is CheckBox)
                {
                    if (((CheckBox)chk).Checked)
                    {
                        diasEjecucion.Add(new GrupoLayout.ProgramacionGrupoLayout.ProgramacionDiasEjecucion() { Dia = Convert.ToInt32(chk.ID.Substring(chk.ID.IndexOf("_") + 1)) });
                    }
                }
            }
            Programacion.DiasEjecucion = diasEjecucion.ToArray();
        }
        else
        {
            foreach (Control chk in pnlDiasProgramacion.Controls)
            {
                if (chk is CheckBox)
                    if (Programacion.DiasEjecucion != null)
                        ((CheckBox)chk).Checked = carga ? Programacion.DiasEjecucion.ToList().Any(x => x.Dia.ToString() == chk.ID.Substring(chk.ID.IndexOf("_") + 1)) : carga;
                    else if (Programacion.IdProgramacion > 0)
                        ((CheckBox)chk).Checked = carga;
            }
        }
    }

    protected void RevisaIntentosMaximo(object sender, EventArgs e)
    {
        SuperaIntentosMaximo();
    }

    private bool SuperaIntentosMaximo()
    {
        int ints = 0;
        bool esMoroso = chkMorosoProgramacion.Checked;
        if (!int.TryParse(txtIntentos.Text.Trim(), out ints) || ints <= 0 || (esMoroso ? ints + 1 : ints) > MaximoCobros)
        {
            AlternarError(true, "Valor de intentos inválido.");
            return true;
        }
        AlternarDiasProgramacion(false, true);
        if ((TIPOPROGRAMACION_RECURRENTES.Contains(ddlTipoProgramacion.SelectedItem.Text) && (Programacion.DiasEjecucion.Length * (esMoroso ? ints + 1 : ints)) > MaximoCobros)
            || (!TIPOPROGRAMACION_RECURRENTES.Contains(ddlTipoProgramacion.SelectedItem.Text) && ((esMoroso ? ints + 1 : ints) > MaximoCobros)))
        {
            AlternarError(true, "La suma de intentos de cobro total supera el máximo permitido.");
            return true;
        }
        AlternarError(false, "");
        return false;
    }

    private void AlternarPestañaProgramaciones()
    {
        if (GrupoLayout != null && GrupoLayout.IdGrupoLayout > 0 && ValidarProgramacionSoloVisanet())
            tpProgramaciones.Visible = true;
        else
            tpProgramaciones.Visible = false;
    }

    #endregion

    #region "Guardar"

    protected void lbtnGuardar_Click(object sender, EventArgs e)
    {
        switch (VistaPagina)
        {
            case Vista.GrupoLayout_Registrar:
            case Vista.GrupoLayout_Actualizar:
            case Vista.Programacion_Administrar:
                GuardaGrupo();
                break;
            case Vista.Programacion_Registrar:
            case Vista.Programacion_Actualizar:
                GuardaProgramacion();
                break;
            case Vista.LayoutGenerado_Busqueda:
            case Vista.LayoutGenerado_Detalle:
                break;
            default:
                break;
        }
    }

    protected void lbtnCancelar_Click(object sender, EventArgs e)
    {
        switch (this.VistaPagina)
        {
            case Vista.GrupoLayout_Registrar:
            case Vista.GrupoLayout_Actualizar:
            case Vista.Programacion_Administrar:
                CancelarGrupoLayoutPantalla();
                break;
            case Vista.Programacion_Registrar:
            case Vista.Programacion_Actualizar:
                CancelarProgramacionPantalla();
                break;
            case Vista.LayoutGenerado_Busqueda:
            case Vista.LayoutGenerado_Detalle:
                break;
            default:
                break;
        }
    }

    protected void lbtnBuscarPlantilla_Guardar_Click(object sender, EventArgs e)
    {
        BuscarPlantilla_Guardar();
    }

    protected void ddlTipoLayout_Guardar_SelectedIndexChanged(object sender, EventArgs e)
    {
        BuscarPlantilla_Guardar();
    }

    protected void lbtnAgregarPlantilla_Guardar_Click(object sender, EventArgs e)
    {
        // Verificar que se haya seleccionado un Item valido
        if (ddlPlantilla_Guardar.SelectedValue != "")
        {
            // Validar que no se hayan agregado previamente
            if (!this.GrupoLayout.Plantillas.ToList().Exists(x => x.Plantilla.IdPlantilla == int.Parse(ddlPlantilla_Guardar.SelectedValue)))
            {
                GrupoLayout.PlantillaGrupoLayout plantilla = new GrupoLayout.PlantillaGrupoLayout();
                plantilla.Plantilla = new Plantilla();
                plantilla.Plantilla.IdPlantilla = int.Parse(ddlPlantilla_Guardar.SelectedValue);
                plantilla.Plantilla.Nombre = ddlPlantilla_Guardar.SelectedItem.Text.Trim();
                plantilla.DiasVencimiento = 0;

                List<GrupoLayout.PlantillaGrupoLayout> plantillas = this.GrupoLayout.Plantillas.ToList();
                plantillas.Add(plantilla);

                GrupoLayout grupo = this.GrupoLayout;
                grupo.Plantillas = plantillas.ToArray();
                this.GrupoLayout = grupo;

                rptPlantillas_Guardar.DataSource = this.GrupoLayout.Plantillas;
                rptPlantillas_Guardar.DataBind();

                // Seleccionar dias de ejecucion
                using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
                {
                    wsSOPF.ResultadoOfArrayOfintuHEDJ7Dj res = ws.Plantilla_ObtenerDiasEjecucion(plantilla.Plantilla.IdPlantilla.GetValueOrDefault());

                    if (res.Codigo == 0)
                    {
                        foreach (int i in res.ResultObject)
                        {
                            ((CheckBox)tpDiasEjecucion.FindControl("chkDiaEjecucion_" + i.ToString())).Checked = true;
                        }
                    }
                }
                AlternarPestañaProgramaciones();
                if (ValidarProgramacionSoloVisanet())
                    AlternarError(false, "");
            }
        }
    }

    protected void txtDiasVencimiento_TextChanged(object sender, EventArgs e)
    {
        // Recuperar los Hidden locales
        int diasVencimiento = 0;
        int IdPlantilla = int.Parse(((HiddenField)((TextBox)sender).Parent.FindControl("hdnIdPlantilla")).Value);

        wsSOPF.GrupoLayout.PlantillaGrupoLayout Plantilla = this.GrupoLayout.Plantillas.ToList().FirstOrDefault(x => x.Plantilla.IdPlantilla == IdPlantilla);

        // Obtener los Dias Vencimiento capturados
        int.TryParse(((TextBox)sender).Text.Trim(), out diasVencimiento);

        Plantilla.DiasVencimiento = diasVencimiento;

        this.GrupoLayout.Plantillas[this.GrupoLayout.Plantillas.ToList().FindIndex(x => x.Plantilla.IdPlantilla == Plantilla.Plantilla.IdPlantilla)] = Plantilla;

        EnlazarPlantillas_Guardar();
    }

    protected void rptPlantillas_Guardar_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        GrupoLayout grupo = this.GrupoLayout;

        if (e.CommandName == "EliminarPlantilla")
        {
            List<GrupoLayout.PlantillaGrupoLayout> plantillas = grupo.Plantillas.ToList();
            plantillas.RemoveAll(x => x.Plantilla.IdPlantilla == int.Parse(e.CommandArgument.ToString()));
            grupo.Plantillas = plantillas.ToArray();
            this.GrupoLayout = grupo;

            // Deseleccionar dias de ejecucion
            using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
            {
                wsSOPF.ResultadoOfArrayOfintuHEDJ7Dj res = ws.Plantilla_ObtenerDiasEjecucion(int.Parse(e.CommandArgument.ToString()));

                if (res.Codigo == 0)
                {
                    foreach (int i in res.ResultObject)
                    {
                        ((CheckBox)tpDiasEjecucion.FindControl("chkDiaEjecucion_" + i.ToString())).Checked = false;
                    }
                }
            }
            EnlazarPlantillas_Guardar();
            AlternarPestañaProgramaciones();
            if (ValidarProgramacionSoloVisanet())
                AlternarError(false, "");
        }
    }

    private void BuscarPlantilla_Guardar()
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            Plantilla eBuscar = new Plantilla();
            eBuscar.TipoLayout = new TipoLayoutCobro();

            eBuscar.Nombre = txtNombrePlantilla_Guardar.Text.Trim();
            eBuscar.Activo = true;
            if (int.Parse(ddlTipoLayout_Guardar.SelectedValue) > 0)
                eBuscar.TipoLayout.IdTipoLayoutCobro = int.Parse(ddlTipoLayout_Guardar.SelectedValue);

            ResultadoOfArrayOfPlantilla1rkJJdIU res = ws.Plantilla_Buscar(eBuscar);

            if (res.Codigo > 0)
                throw new Exception(res.Mensaje);

            ddlPlantilla_Guardar.DataSource = res.ResultObject;
            ddlPlantilla_Guardar.DataBind();
        }
    }

    private void EnlazarPlantillas_Guardar()
    {
        if (this.GrupoLayout.Plantillas != null)
            rptPlantillas_Guardar.DataSource = this.GrupoLayout.Plantillas;
        else
            rptPlantillas_Guardar.DataSource = null;

        rptPlantillas_Guardar.DataBind();
    }

    private void GuardaGrupo()
    {
        pnlError_Guardar.Visible = false;

        try
        {
            // Recuperar datos principales
            GrupoLayout grupo = this.GrupoLayout;

            grupo.Nombre = txtNombreGrupo_Guardar.Text.Trim();
            grupo.Activo = chkActivo.Checked;
            grupo.UsarCuentaEmergente = chkUsarCuentaEmergente.Checked;

            // Recuperar Dias Ejecucion
            List<GrupoLayout.ConfiguracionEjecucion> diasEjecucion = new List<GrupoLayout.ConfiguracionEjecucion>();
            foreach (Control chk in tpDiasEjecucion.Controls[0].Controls)
            {
                if (chk is CheckBox)
                {
                    if (((CheckBox)chk).Checked)
                    {
                        diasEjecucion.Add(new GrupoLayout.ConfiguracionEjecucion() { Dia = chk.ID.Substring(chk.ID.IndexOf("_") + 1) });
                    }
                }
            }

            grupo.DiasEjecucion = diasEjecucion.ToArray();

            this.GrupoLayout = grupo;

            // Validar la informacion antes de proceder a Guardar
            ValidarGuardar();

            // Guardar la Plantilla
            using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
            {
                ResultadoOfGrupoLayout1rkJJdIU res = ws.GrupoLayout_Guardar(this.GrupoLayout);

                if (res.Codigo > 0)
                    throw new Exception(res.Mensaje);
            }

            this.VistaPagina = Vista.GrupoLayout_Busqueda;
            pnlExito_Buscar.Visible = true;
            lblExito_Buscar.Text = string.Format("Se guardo el Grupo {0} correctamente", this.GrupoLayout.Nombre);
        }
        catch (Exception ex)
        {
            lblError_Guardar.Text = ex.Message;
            pnlError_Guardar.Visible = true;
        }
    }

    private void AlternarError(bool activo, string mensaje)
    {
        lblError_Guardar.Text = activo ? mensaje : "";
        pnlError_Guardar.Visible = activo;
    }

    #endregion


    #region "Balance"   

    protected void btnDescargaLayout_Click(object sender, EventArgs e)
    {
        //int canal = 0;

        //if (canal == 4) //Interbank
        //{
        //    GenerarLayoutInterbank();
        //}
        //else
        //if (canal == 5) //netcash
        //{
        //    GeneraLayoutNetCash();
        //}
        //else
        //if (canal == 11) //Visanet
        //{
        //    GeneraLayoutVisaNet();            
        //}
    }

    #endregion


    #region "Metodos Privados"

    public static void ExportCsv<T>(List<T> genericList, string fileName)
    {
        var sb = new StringBuilder();
        var basePath = AppDomain.CurrentDomain.BaseDirectory;
        var finalPath = fileName;
        var header = "";
        var info = typeof(T).GetProperties();

        if (!File.Exists(finalPath))
        {
            var file = File.Create(finalPath);
            file.Close();
            foreach (var prop in typeof(T).GetProperties())
            {
                if (prop.Name != "ExtensionData")
                {
                    header += prop.Name + ", ";
                }
            }
            header = header.Substring(0, header.Length - 2);
            string[] encabe = header.Split(',');//dividimos la cadena del encabezado en un arreglo
            header = encabe[4] + "," + encabe[5] + "," + encabe[3] + "," + encabe[2] + "," + encabe[1] + "," + encabe[0]; //reordenamos el encabezado
            sb.AppendLine(header);
            TextWriter sw = new StreamWriter(finalPath, true);
            sw.Write(sb.ToString());
            sw.Close();
        }

        foreach (var obj in genericList)
        {
            sb = new StringBuilder();
            var line = "";
            foreach (var prop in info)
            {
                if (prop.Name != "ExtensionData")
                {
                    line += prop.GetValue(obj, null) + ", ";
                }
            }
            line = line.Substring(0, line.Length - 2);

            string[] line2 = line.Split(',');//dividimos la cadena en un arreglo
            line = line2[4] + "," + line2[5] + "," + line2[3] + "," + line2[2] + "," + line2[1] + "," + line2[0];//reordenamos la cadena.

            sb.AppendLine(line);
            TextWriter sw = new StreamWriter(finalPath, true);
            sw.Write(sb.ToString());
            sw.Close();
        }
    }

    private string CadenaAleatoria()
    {
        int maxLenght = 5;
        var obj = new Random();
        const string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        var longitud = allowedChars.Length;
        var res = "";

        for (int i = 0; i < maxLenght; i++)
        {
            res += allowedChars[obj.Next(longitud)];
        }
        return res;
    }

    public static DateTime? ValidarFechaHora(string fecha, string hora)
    {
        fecha = string.IsNullOrEmpty(fecha) ? DateTime.Now.ToString("dd/MM/yyyy") : fecha;
        hora = string.IsNullOrEmpty(hora) ? "00:00" : hora;
        Regex rg = new Regex(@"^(?:[01][0-9]|2[0-3]):[0-5][0-9]$");
        hora = hora.Substring(0, 5);
        if (rg.IsMatch(hora))
        {
            string[] formats = { "dd/MM/yyyy HH:mm" };
            DateTime FechaHoraFinal;
            string fechaHora = string.Format("{0} {1}", fecha, hora);
            if (DateTime.TryParseExact(fechaHora, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaHoraFinal))
            {
                return FechaHoraFinal;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
    
    #endregion
}
