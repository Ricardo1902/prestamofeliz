﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmGestionCobranza.aspx.cs" Inherits="Site_Creditos_frmGestionCobranza" MasterPageFile="~/Site.master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolKit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">     
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Gestión de la Cobranza</h3>
        </div>
        <div class="panel-body">
            <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
                <ProgressTemplate>                 
                    <div id="loader-background"></div>
                    <div id="loader-content"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>                               
                <ajaxtoolKit:TabContainer ID="tcMain" CssClass="actTab" runat="server">
                    <ajaxtoolKit:TabPanel HeaderText="Balance Cuentas" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlBuscar_Balance" runat="server">
                            <div class="form-group col-md-4 col-lg-3">
                                <label class="form-control-label small">Solicitud</label>
                                <asp:textbox id="txtSolicitud_Balance" onKeyPress="return EvaluateText('%f', this);" runat="server" CssClass="form-control"></asp:textbox>
                            </div>            

                            <div class="form-group col-md-4 col-lg-3">
                                <label class="form-control-label small">Fecha Credito (Desde)</label>                                
                                <div>     
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaCreditoDesde_Balance" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                        <span class="input-group-addon">
                                            <asp:LinkButton ID="btnSelectFechaCreditoDesde_Balance" runat="server">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </asp:LinkButton>
                                        </span>
                                    </div>                                                         
                                    <asp:RegularExpressionValidator runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtFechaCreditoDesde_Balance"
                                            ForeColor="Red"
                                            ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />                                    
                                    <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaCreditoDesde_Balance" PopupButtonID="btnSelectFechaCreditoDesde_Balance" runat="server"></ajaxtoolKit:CalendarExtender>
                                </div>         
                            </div>            

                            <div class="form-group col-md-4 col-lg-3">
                                <label class="form-control-label small">Fecha Credito (Hasta)</label>                                
                                <div>                    
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaCreditoHasta_Balance" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                        <span class="input-group-addon">
                                            <asp:LinkButton ID="btnSelectFechaCreditoHasta_Balance" runat="server">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </asp:LinkButton>
                                        </span>
                                    </div>                    
                                    <asp:RegularExpressionValidator runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtFechaCreditoHasta_Balance"
                                            ForeColor="Red"
                                            ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                    <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaCreditoHasta_Balance" PopupButtonID="btnSelectFechaCreditoHasta_Balance" runat="server"></ajaxtoolKit:CalendarExtender>
                                </div>        
                            </div>                            

                            <div class="visible-md-block clearfix"></div> 
                            <div class="form-group col-md-4 col-lg-3">           
                                <label class="form-control-label small">Estatus Credito</label>
                                <asp:dropdownlist id="ddlEstatusCredito_Balance" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="Todos"								        Value="0"	></asp:ListItem>
                                    <asp:ListItem Text="Autorizado"                                 Value="10"  ></asp:ListItem>
                                    <asp:ListItem Text="Dispersado"                                 Value="11"  ></asp:ListItem>
                                    <asp:ListItem Text="Saldado por Liquidación"                    Value="8"   ></asp:ListItem>
                                    <asp:ListItem Text="Liquidacion por Ampliacion"                 Value="16"  ></asp:ListItem>
                                    <asp:ListItem Text="Liquidacion por Defuncion"                  Value="17"  ></asp:ListItem>                  
                                </asp:dropdownlist>
                            </div>
                            
                            <div class="visible-lg-block clearfix"></div><!-- Fix format validation FechaDesde -->
                            <div class="form-group col-md-8 col-lg-6">
                                <label class="form-control-label small">Cliente</label>
                                <asp:textbox id="txtCliente_Balance" runat="server" CssClass="form-control text-uppercase"></asp:textbox>                                
                            </div>

                            <div class="col-sm-12">            
                                <asp:Button ID="btnBuscar_Balance" runat="server" Text="Buscar" OnClick="btnBuscar_Balance_Click" CssClass="btn btn-primary btn-sm"   />
                            </div>    
                            
                            <asp:Panel ID="pnlError_FltBalance" class="form-group col-sm-12" Visible="false" runat="server">                                
                                <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                    <asp:Label ID="lblError_FltBalance" runat="server"></asp:Label>
                                </div>
                            </asp:Panel>                                                                                                         

                            <div class="form-group col-sm-12">
                                <div style="height:auto; width: auto; overflow-x: auto;">
                                    <hr />
                                    <asp:GridView ID="gvBalanceCuentas" runat="server" Width="100%" AutoGenerateColumns="False"
                                        Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                        OnPageIndexChanging="gvBalanceCuentas_PageIndexChanging"
                                        OnRowCommand="gvBalanceCuentas_RowCommand"                                         
                                        PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        DataKeyNames="IdSolicitud" Style="overflow-x:auto;">                                        
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnDetalleBalance" CommandName="VerBalance" CommandArgument='<%# Eval("IdSolicitud") %>' CssClass="btn btn-sm btn-secondary" data-toggle="tooltip" title="Ver Balance" runat="server">
                                                        <i class="fas fa-money-check-alt fa-lg" style="color: #888888;"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Solicitud" DataField="IdSolicitud" />
                                            <asp:BoundField HeaderText="Fecha Credito" DataField="FechaCredito.Value" DataFormatString="{0:d}" />                            
                                            <asp:BoundField HeaderText="Cliente" DataField="ClienteNombre" /> 
                                            <asp:BoundField HeaderText="Producto" DataField="ProductoDesc" /> 
                                            <asp:BoundField HeaderText="Capital" DataField="Capital" DataFormatString="{0:c2}" />
                                            <asp:BoundField HeaderText="Saldo Capital" DataField="SaldoCapital" DataFormatString="{0:c2}" />
                                            <asp:BoundField HeaderText="Saldo Vencido" DataField="SaldoVencido" DataFormatString="{0:c2}" ItemStyle-CssClass="jsSaldoVencido" />
                                        </Columns>                                        
                                        <PagerStyle CssClass="pagination-ty warning" />
                                        <EmptyDataTemplate>
                                            No se encontraron Cuentas con los filtros seleccionados
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>  
                            </div>
                            </asp:Panel>

                            <asp:Panel ID="pnlVerBalance_Balance" Visible="false" runat="server">
                                <div class="form-group col-xs-12">
                                    <asp:LinkButton ID="lbtnRegresar_Balance" CssClass="btn btn-sm btn-default hidden-print" OnClick="lbtnRegresar_Balance_Click" runat="server">
                                        <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                                    </asp:LinkButton>
                                    <button class="btn btn-default btn-sm hidden-print" onclick="printPF();"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Imprimir</button>
                                    <hr class="hidden-print" />

                                    <div class="col-sm-12">
                                        <blockquote style="background-color:#F8F8F8;">
                                            <p>Estado Cuenta</p>
                                        </blockquote>
                                    </div>

                                    <div class="col-sm-12 hidden-print"> 
                                        <p class="small"><strong>ESTATUS DEL CREDITO:</strong> <asp:Label ID="lblEstatusCredito_VerBalance" runat="server"></asp:Label></p>                                        
                                        <hr />
                                    </div> 
                                    <div class="clearfix"></div>
                                    <div class="col-sm-3 col-print-3">
                                        <label class="small">Solicitud</label>           
                                        <pre><asp:Label ID="lblIdSolicitud_VerBalance" runat="server"></asp:Label></pre>
                                    </div>

                                    <div class="col-sm-6 col-print-6">   
                                        <label class="small">Cliente</label>        
                                        <pre><asp:Label ID="lblCliente_VerBalance" runat="server"></asp:Label></pre>
                                    </div>

                                    <div class="col-sm-3 col-print-3"> 
                                        <label class="small">Fecha Credito</label>          
                                        <pre><asp:Label ID="lblFechaCredito_VerBalance" runat="server"></asp:Label></pre>
                                    </div>
                                    
                                    <div class="col-sm-3 col-print-3"> 
                                        <label class="small">Monto Uso Canal</label>          
                                        <pre><asp:Label ID="lblMontoUsoCanal_VerBalance" runat="server"></asp:Label></pre>
                                    </div>

                                    <div class="col-sm-3 col-print-3"> 
                                        <label class="small">Monto GAT</label>          
                                        <pre><asp:Label ID="lblMontoGAT_VerBalance" runat="server"></asp:Label></pre>
                                    </div>

                                    <div class="col-sm-3 col-print-3"> 
                                        <label class="small">Cuota</label>          
                                        <pre><asp:Label ID="lblCuota_VerBalance" runat="server"></asp:Label></pre>
                                    </div>

                                    <div class="col-sm-3 col-print-3"> 
                                        <label class="small">Capital</label>          
                                        <pre><asp:Label ID="lblCapital_VerBalance" runat="server"></asp:Label></pre>
                                    </div>                                    

                                    <div class="col-sm-6 col-print-6"> 
                                        <label class="small">Producto</label>          
                                        <pre><asp:Label ID="lblProducto_VerBalance" runat="server"></asp:Label></pre>
                                    </div>                                    

                                    <div class="col-sm-3 col-print-3"> 
                                        <label class="small">Saldo Vencido</label>          
                                        <pre><asp:Label ID="lblSaldoVencido_VerBalance" runat="server"></asp:Label></pre>
                                    </div>  

                                    <div class="col-sm-3 col-print-3"> 
                                        <label class="small">Costo Total Credito</label>          
                                        <pre><asp:Label ID="lblCostoTotalCredito_VerBalance" runat="server"></asp:Label></pre>
                                    </div>  
                                    
                                    <div class="col-sm-3 col-print-3"> 
                                        <label class="small">Tipo Credito</label>          
                                        <pre><asp:Label ID="lblTipoCredito_VerBalance" runat="server"></asp:Label></pre>
                                    </div> 
                                    
                                    <div class="col-sm-3 col-print-3"> 
                                        <label class="small">Monto Comisión uso de canal</label>          
                                        <pre><asp:Label ID="lblMontoComision_VerBalance" runat="server"></asp:Label></pre>
                                    </div>  
                                    
                                    <div class="clearfix"></div>

                                    <ajaxtoolKit:TabContainer ID="tcDetalle_VerBalance" CssClass="actTab" runat="server">
                                        <ajaxtoolKit:TabPanel HeaderText="Recibos" runat="server">
                                            <ContentTemplate>
                                            <div class="col-sm-12 col-print-12">
                                                <div style="height:auto; width: auto; overflow-x: auto;">                                                    
                                                    <asp:GridView ID="gvRecibos_VerBalance" runat="server" Width="100%" AutoGenerateColumns="False"
                                                        HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                        CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData"
                                                        RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                        OnRowDataBound="gvRecibos_VerBalance_RowDataBound">                                        
                                                        <Columns>
                                                            <asp:BoundField HeaderText="#" DataField="Recibo" ItemStyle-CssClass="small" />                                            
                                                            <asp:BoundField HeaderText="Fecha" DataField="FechaRecibo.Value" DataFormatString="{0:d}" ItemStyle-CssClass="small" />    
                                                            <asp:BoundField HeaderText="Cap. Ini." DataField="CapitalInicial" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />                  
                                                            <asp:BoundField HeaderText="S. Capital" DataField="SaldoCapital" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                                            <asp:BoundField HeaderText="S. Interes" DataField="SaldoInteres" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                                            <asp:BoundField HeaderText="S. IGV" DataField="SaldoIGV" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                                            <asp:BoundField HeaderText="S. Seguro" DataField="SaldoSeguro" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                                            <asp:BoundField HeaderText="S. GAT" DataField="SaldoGAT" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />                                                            
                                                            <asp:BoundField HeaderText="S. Comisión" DataField="SaldoOtrosCargos" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />                                                            
                                                            <asp:BoundField HeaderText="S. IGV Comisión" DataField="SaldoIGVOtrosCargos" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />                                                            
                                                            <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="small">
                                                                <ItemTemplate>
                                                                    <span class="d-inline-block tooltipIdeaLeft" data-toggle="tooltip" title="<%# Eval("EstatusDesc") %>">
                                                                        <%# Eval("EstatusClave") %>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                                                
                                                            <asp:BoundField HeaderText="Saldo" DataField="SaldoRecibo" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />                                                            
                                                            <asp:BoundField HeaderText="Cap. Ins." DataField="CapitalInsoluto" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                                        </Columns>                                                 
                                                        <EmptyDataTemplate>
                                                            Hay un problema con la Cuenta, no tiene Recibos registrados
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>  
                                            </div>
                                            </ContentTemplate>
                                        </ajaxtoolKit:TabPanel>
                                        <ajaxtoolKit:TabPanel HeaderText="Pagos" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="pnlError_Balance_VerBalance_Pagos" class="form-group col-sm-12" Visible="false" runat="server">                                
                                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                        <asp:Label ID="lblError_Balance_VerBalance_Pagos" runat="server"></asp:Label>
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="pnlAplicarPagos" CssClass="form-group col-sm-12" Visible="false" runat="server">
                                                    <label>Aplicar Unico</label>
                                                    <asp:CheckBox ID="chkAplicarPagosUnico" runat="server" />
                                                    <asp:LinkButton ID="lbtnAplicarPagos" OnClick="lbtnAplicarPagos_Click" CssClass="btn btn-sm btn-default" runat="server">
                                                         Aplicar Todos los Pagos
                                                     </asp:LinkButton>
                                                </asp:Panel>

                                                <div class="col-sm-12">
                                                    <div style="height:auto; width: auto; overflow-x: auto;">                                                    
                                                        <asp:GridView ID="gvPagos_VerBalance" runat="server" Width="100%" AutoGenerateColumns="False"
                                                            HeaderStyle-CssClass="info" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                            CssClass="table table-bordered table-responsive table-condensed table-hover" EmptyDataRowStyle-CssClass="GridEmptyData"
                                                            RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                            OnRowDataBound="gvPagos_VerBalance_RowDataBound"
                                                            OnRowCommand="gvPagos_VerBalance_RowCommand">                                        
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-CssClass="text-center" AccessibleHeaderText="OpcionesPago" HeaderText="<i class='fab fa-superpowers fa-lg'></i>" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Panel runat="server" ID="pnlOpcionesPago">
                                                                            <div class="dropdown">                                                        
                                                                                <button class="btn btn-xs btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">                                                                
                                                                                    <i class="fas fa-bars" style="color: #888888;"></i>
                                                                                <span class="caret"></span>
                                                                                </button>
                                                                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                                                    <li>                                                                                                                                    
                                                                                        <asp:LinkButton ID="lbtnAplicarPago" CommandName="AplicarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                            Aplicar
                                                                                        </asp:LinkButton>                                                                
                                                                                    </li>
                                                                                    <li>                                                                                                                                    
                                                                                        <asp:LinkButton ID="lbtnAplicarPagoUnico" CommandName="AplicarPagoUnico" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                            Aplicar Unico
                                                                                        </asp:LinkButton>                                                                
                                                                                    </li>                                                                
                                                                                    <li>
                                                                                        <asp:LinkButton ID="lbtnDesaplicarPago" CommandName="DesaplicarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                            Desaplicar
                                                                                        </asp:LinkButton>                                                                
                                                                                    </li>
                                                                                    <li>
                                                                                        <asp:LinkButton ID="lbtnCancelarPago" CommandName="CancelarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                                            Cancelar
                                                                                        </asp:LinkButton>                                                                
                                                                                    </li>
                                                                                </ul>
                                                                            </div>  
                                                                        </asp:Panel>                                                                                                                              
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>                                                                                                                                                           
                                                                <asp:BoundField HeaderText="Canal de Pago" DataField="CanalPagoDesc" />
                                                                <asp:BoundField HeaderText="Fecha Pago" DataField="FechaPago.Value" DataFormatString="{0:d}" />
                                                                <asp:BoundField HeaderText="Monto del Pago" DataField="MontoCobrado" DataFormatString="{0:c2}" />
                                                                <asp:BoundField HeaderText="Aplicado" DataField="MontoAplicado" DataFormatString="{0:c2}" />                                                                                                                 
                                                                <asp:BoundField HeaderText="Por Aplicar" DataField="MontoPorAplicar" DataFormatString="{0:c2}" />
                                                                <asp:BoundField HeaderText="Estatus" DataField="Estatus.EstatusDesc" />                                                                
                                                            </Columns>
                                                            <EmptyDataRowStyle CssClass="warning small" />                                               
                                                            <EmptyDataTemplate>
                                                                <i class="fas fa-exclamation-circle" style="color: #888888;"></i> La Cuenta no tiene Pagos registrados
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>  
                                                </div>
                                            </ContentTemplate>
                                        </ajaxtoolKit:TabPanel>
                                        <ajaxtoolKit:TabPanel ID="tpLiquidacion_VerBalance" HeaderText="Liquidacion" Visible="false" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="pnlError_VerBalance_Liquidacion" class="form-group col-sm-12" Visible="false" runat="server">                                
                                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                        <asp:Label ID="lblError_VerBalance_Liquidacion" runat="server"></asp:Label>
                                                    </div>
                                                </asp:Panel>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Numero Liquidacion</label>          
                                                    <pre><asp:Label ID="lblIdLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Numero Transaccion (Autorizacion)</label>          
                                                    <pre><asp:Label ID="lblNumeroTransaccionLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Fecha Liquidacion</label>          
                                                    <pre><asp:Label ID="lblFechaLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                                </div>

                                                 <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Capital</label>          
                                                    <pre><asp:Label ID="lblCapitalLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Interes</label>          
                                                    <pre><asp:Label ID="lblInteresLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">IGV</label>          
                                                    <pre><asp:Label ID="lblIGVLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Seguro</label>          
                                                    <pre><asp:Label ID="lblSeguroLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">GAT</label>          
                                                    <pre><asp:Label ID="lblGATLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Total Liquidacion</label>          
                                                    <pre><asp:Label ID="lblTotalLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                                </div>

                                                <div class="col-sm-12 col-print-12">
                                                    <hr />
                                                    <div style="height:auto; width: auto; overflow-x: auto;">                                                    
                                                        <asp:GridView ID="gvRecibos_VerBalance_Liquidacion" runat="server" Width="100%" AutoGenerateColumns="False"
                                                            HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData"
                                                            RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                            OnRowDataBound="gvRecibos_VerBalance_Liquidacion_RowDataBound">                                        
                                                            <Columns>
                                                                <asp:BoundField HeaderText="#" DataField="Recibo" />      
                                                                <asp:BoundField HeaderText="Fecha" DataField="FechaRecibo" DataFormatString="{0:d}" />                      
                                                                <asp:BoundField HeaderText="Tipo Recibo" DataField="TipoReciboCalculo" />  
                                                                <asp:BoundField HeaderText="Liquida Capital" DataField="LiquidaCapital" DataFormatString="{0:c2}" />
                                                                <asp:BoundField HeaderText="Liquida Interes" DataField="LiquidaInteres" DataFormatString="{0:c2}" />
                                                                <asp:BoundField HeaderText="Liquida IGV" DataField="LiquidaIGV" DataFormatString="{0:c2}" />
                                                                <asp:BoundField HeaderText="Liquida Seguro" DataField="LiquidaSeguro" DataFormatString="{0:c2}"/>
                                                                <asp:BoundField HeaderText="Liquida GAT" DataField="LiquidaGAT" DataFormatString="{0:c2}" />                                                                
                                                            </Columns>                                                
                                                            <EmptyDataTemplate>
                                                                Hay un problema con la Liquidacion, no tiene Información de Detalle
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>  
                                                </div>
                                            </ContentTemplate>
                                        </ajaxtoolKit:TabPanel>
                                        <ajaxtoolKit:TabPanel ID="tpAmpliacionAnterior_VerBalance" HeaderText="Solicitud Anterior" Visible="false" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="pnlError_AmpliacionAnterior_VerBalance" class="form-group col-sm-12" Visible="false" runat="server">                                
                                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                        <asp:Label ID="lblError_AmpliacionAnterior_VerBalance" runat="server"></asp:Label>
                                                    </div>
                                                </asp:Panel>                                               
                                
                                                <div class="col-sm-4 col-print-4">
                                                    <label class="small">Solicitud</label>          
                                                    <asp:TextBox ID="txtIdSolicitudAnterior_AmpliacionAnterior" MaxLength="8" CssClass="form-control" onKeyPress="return EvaluateText('%d', this);" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                <div class="col-sm-4 col-print-4">
                                                    <label class="small">Fecha Ampliacion</label>          
                                                    <asp:TextBox ID="txtFechaAmpliacion_AmpliacionAnterior" MaxLength="10" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>                                
                                
                                                <div class="clearfix"></div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Capital Liquida</label>          
                                                    <asp:TextBox ID="txtCapitalLiquida_AmpliacionAnterior" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Interes Liquida</label>          
                                                    <asp:TextBox ID="txtInteresLiquida_AmpliacionAnterior" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                 <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">IGV Liquida</label>          
                                                    <asp:TextBox ID="txtIGVLiquida_AmpliacionAnterior" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Seguro Liquida</label>          
                                                    <asp:TextBox ID="txtSeguroLiquida_AmpliacionAnterior" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">GAT Liquida</label>          
                                                    <asp:TextBox ID="txtGATLiquida_AmpliacionAnterior" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Total Liquida</label>          
                                                    <asp:TextBox ID="txtTotalLiquida_AmpliacionAnterior" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>                                                
                                            </ContentTemplate>
                                        </ajaxtoolKit:TabPanel>
                                        <ajaxtoolKit:TabPanel ID="tpAmpliacionPosterior_VerBalance" HeaderText="Solicitud Posterior" Visible="false" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="pnlError_AmpliacionPosterior_VerBalance" class="form-group col-sm-12" Visible="false" runat="server">                                
                                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                        <asp:Label ID="lblError_AmpliacionPosterior_VerBalance" runat="server"></asp:Label>
                                                    </div>
                                                </asp:Panel>                                               
                                
                                                <div class="col-sm-4 col-print-4">
                                                    <label class="small">Solicitud</label>          
                                                    <asp:TextBox ID="txtIdSolicitudAmplia_AmpliacionPosterior" MaxLength="8" CssClass="form-control" onKeyPress="return EvaluateText('%d', this);" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                <div class="col-sm-4 col-print-4">
                                                    <label class="small">Fecha Ampliacion</label>          
                                                    <asp:TextBox ID="txtFechaAmpliacion_AmpliacionPosterior" MaxLength="10" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>                                
                                
                                                <div class="clearfix"></div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Capital Liquida</label>          
                                                    <asp:TextBox ID="txtCapitalLiquida_AmpliacionPosterior" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Interes Liquida</label>          
                                                    <asp:TextBox ID="txtInteresLiquida_AmpliacionPosterior" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                 <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">IGV Liquida</label>          
                                                    <asp:TextBox ID="txtIGVLiquida_AmpliacionPosterior" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Seguro Liquida</label>          
                                                    <asp:TextBox ID="txtSeguroLiquida_AmpliacionPosterior" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">GAT Liquida</label>          
                                                    <asp:TextBox ID="txtGATLiquida_AmpliacionPosterior" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                                </div>

                                                <div class="col-sm-4 col-print-4"> 
                                                    <label class="small">Total Liquida</label>          
                                                    <asp:TextBox ID="txtTotalLiquida_AmpliacionPosterior" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>                                                
                                            </ContentTemplate>
                                        </ajaxtoolKit:TabPanel>
                                    </ajaxtoolKit:TabContainer>                                    
                                </div>                                  
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxtoolKit:TabPanel>
                    <ajaxtoolKit:TabPanel HeaderText="Consulta Cobros" runat="server">
                        <ContentTemplate>
                            <div class="row">
                            <div class="col-md-3">
                                <label class="form-control-label small">Solicitud</label>
                                <asp:textbox id="txtSolicitud_Pagos" onKeyPress="return EvaluateText('%f', this);" runat="server" CssClass="form-control"></asp:textbox>
                            </div>            

                            <div class="col-md-3">                
                                <label class="form-control-label small">Fecha Cobro (Desde)</label>                                
                                <div class="form-group">                    
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaPagoDesde_Pagos" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                        <span class="input-group-addon">
                                            <asp:LinkButton ID="btnSelectFechaPagoDesde_Pagos" runat="server">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </asp:LinkButton>
                                        </span>
                                    </div>                    
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtFechaPagoDesde_Pagos"
                                            ForeColor="Red"
                                            ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                    <ajaxtoolKit:CalendarExtender ID="ceFechaPagoDesde" Format="dd/MM/yyyy" TargetControlID="txtFechaPagoDesde_Pagos" PopupButtonID="btnSelectFechaPagoDesde_Pagos" runat="server"></ajaxtoolKit:CalendarExtender>
                                </div> 
                            </div>

                            <div class="col-md-3"> 
                                <label class="form-control-label small">Fecha Cobro (Hasta)</label>                                
                                <div class="form-group">                    
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaPagoHasta_Pagos" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                        <span class="input-group-addon">
                                            <asp:LinkButton ID="btnSelectFechaPagoHasta_Pagos" runat="server">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </asp:LinkButton>
                                        </span>
                                    </div>                    
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtFechaPagoHasta_Pagos"
                                            ForeColor="Red"
                                            ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                    <ajaxtoolKit:CalendarExtender ID="ceFechaPagoHasta" Format="dd/MM/yyyy" TargetControlID="txtFechaPagoHasta_Pagos" PopupButtonID="btnSelectFechaPagoHasta_Pagos" runat="server"></ajaxtoolKit:CalendarExtender>
                                </div>       
                            </div>            

                            <div class="col-md-3">           
                                <label class="form-control-label small">Estatus</label>
                                <asp:dropdownlist id="ddlEstatusPago_Pagos" runat="server" CssClass="form-control" AutoPostBack="true">
                                    <asp:ListItem Text="Todos" Value="0"	></asp:ListItem>
                                    <asp:ListItem Text="Registrado" Value="1"	></asp:ListItem>
                                    <asp:ListItem Text="Parcial Aplicado" Value="2"   ></asp:ListItem>                    
                                    <asp:ListItem Text="Aplicado" Value="3"   ></asp:ListItem>
                                    <asp:ListItem Text="Cancelado" Value="4"   ></asp:ListItem>
                                    <asp:ListItem Text="Devuelto" Value="5"   ></asp:ListItem>
                                </asp:dropdownlist>
                            </div>
                            </div>
                            
                            <div class="col-md-3">
                                <label class="form-control-label small">Canal Pago</label>
                                <asp:DropDownList ID="ddlCanalPago" DataValueField="IdCanalPago" DataTextField="Nombre" CssClass="form-control" AppendDataBoundItems="true" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Todos"></asp:ListItem>
                                </asp:DropDownList>
                            </div>            

                            <div class="col-md-3">                
                                <label class="form-control-label small">Fecha Registro (Desde)</label>                                
                                <div class="form-group">                    
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaRegistroDesde_Pagos" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                        <span class="input-group-addon">
                                            <asp:LinkButton ID="btnSelectFechaRegistroDesde_Pagos" runat="server">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </asp:LinkButton>
                                        </span>
                                    </div>                    
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtFechaRegistroDesde_Pagos"
                                            ForeColor="Red"
                                            ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                    <ajaxtoolKit:CalendarExtender ID="CalendarExtender1" Format="dd/MM/yyyy" TargetControlID="txtFechaRegistroDesde_Pagos" PopupButtonID="btnSelectFechaRegistroDesde_Pagos" runat="server"></ajaxtoolKit:CalendarExtender>
                                </div> 
                            </div>

                            <div class="col-md-3"> 
                                <label class="form-control-label small">Fecha Registro (Hasta)</label>                                
                                <div class="form-group">                    
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaRegistroHasta_Pagos" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                        <span class="input-group-addon">
                                            <asp:LinkButton ID="btnSelectFechaRegistroHasta_Pagos" runat="server">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </asp:LinkButton>
                                        </span>
                                    </div>                    
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtFechaRegistroHasta_Pagos"
                                            ForeColor="Red"
                                            ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                    <ajaxtoolKit:CalendarExtender ID="CalendarExtender2" Format="dd/MM/yyyy" TargetControlID="txtFechaRegistroHasta_Pagos" PopupButtonID="btnSelectFechaRegistroHasta_Pagos" runat="server"></ajaxtoolKit:CalendarExtender>
                                </div>       
                            </div>

                            <div class="col-sm-12">            
                                <asp:Button ID="btnBuscar" Text="Buscar" CssClass="btn btn-primary btn-sm" OnClick="btnBuscar_Pagos_Click"  runat="server"/>
                            </div>

                            <asp:Panel ID="pnlError_FltPagosDom" class="form-group col-sm-12" Visible="false" runat="server">                                
                                <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                    <asp:Label ID="lblError_FltPagosDom" runat="server"></asp:Label>
                                </div>
                            </asp:Panel> 

                            <div class="form-group col-sm-12">
                                <div style="height:auto; width: auto; overflow-x: auto;">
                                    <hr />
                                    <asp:GridView ID="gvPagosDomiciliacion" runat="server" Width="100%" AutoGenerateColumns="False"
                                        Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                        OnPageIndexChanging="gvPagosDomiciliacion_PageIndexChanging" PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        DataKeyNames="IdSolicitud" Style="overflow-x:auto;">                                        
                                        <Columns>
                                            <asp:BoundField HeaderText="Solicitud" DataField="IdSolicitud" />                            
                                            <asp:BoundField HeaderText="Monto Cobrado" DataField="MontoCobrado" DataFormatString="{0:c2}" />
                                            <asp:BoundField HeaderText="Monto Aplicado" DataField="MontoAplicado" DataFormatString="{0:c2}" />                            
                                            <asp:BoundField HeaderText="Canal de Pago" DataField="CanalPagoDesc" />                            
                                            <asp:BoundField HeaderText="Estatus" DataField="Estatus.EstatusDesc" />                            
                                            <asp:BoundField HeaderText="Fecha Cobro" DataField="FechaPago.Value" DataFormatString="{0:d}" />
                                            <asp:BoundField HeaderText="Fecha Registro" DataField="FechaRegistro.Value" />
                                            <asp:BoundField HeaderText="Fecha Aplicacion" DataField="FechaAplicacion.Value" />
                                        </Columns>
                                        <PagerStyle CssClass="pagination-ty warning" /> 
                                        <EmptyDataTemplate>
                                            No se encontraron Cobros con los filtros seleccionados
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>  
                            </div>
                        </ContentTemplate>
                    </ajaxtoolKit:TabPanel>
                    <ajaxtoolKit:TabPanel HeaderText="Cartera" runat="server">
                        <ContentTemplate>
                            <div class="col-md-12">
                                <div class="form-group col-md-3">
                                    <label for="" class="form-control-label small">Año</label>                                                                                                 
                                    <div>                                    
                                        <asp:DropDownList ID="ddlAnioChartCartera" AppendDataBoundItems="true" CssClass="form-control" runat="server">
                                            <asp:ListItem Text="Seleccione" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                     </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="" class="form-control-label small">Mes Desde</label>                                                                                                 
                                    <div>                                    
                                        <asp:DropDownList ID="ddlMesDesdeCartera" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1">Enero</asp:ListItem>
                                            <asp:ListItem Value="2">Febrero</asp:ListItem>
                                            <asp:ListItem Value="3">Marzo</asp:ListItem>
                                            <asp:ListItem Value="4">Abril</asp:ListItem>
                                            <asp:ListItem Value="5">Mayo</asp:ListItem>
                                            <asp:ListItem Value="6">Junio</asp:ListItem>
                                            <asp:ListItem Value="7">Julio</asp:ListItem>
                                            <asp:ListItem Value="8">Agosto</asp:ListItem>
                                            <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                            <asp:ListItem Value="10">Octubre</asp:ListItem>
                                            <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                            <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                    <div class="form-group col-md-3">
                                    <label for="" class="form-control-label small">Mes Hasta</label>                                                                                                 
                                    <div>                                    
                                        <asp:DropDownList ID="ddlMesHastaCartera" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1">Enero</asp:ListItem>
                                            <asp:ListItem Value="2">Febrero</asp:ListItem>
                                            <asp:ListItem Value="3">Marzo</asp:ListItem>
                                            <asp:ListItem Value="4">Abril</asp:ListItem>
                                            <asp:ListItem Value="5">Mayo</asp:ListItem>
                                            <asp:ListItem Value="6">Junio</asp:ListItem>
                                            <asp:ListItem Value="7">Julio</asp:ListItem>
                                            <asp:ListItem Value="8">Agosto</asp:ListItem>
                                            <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                            <asp:ListItem Value="10">Octubre</asp:ListItem>
                                            <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                            <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>&nbsp;</label>                                                                                   
                                    <button class="btn btn-primary form-control" onclick="actualizarChartCartera(); return false">
                                        <i class="fas fa-retweet"></i>
                                    </button>
                                </div>
                                <canvas id="cvsChartCartera"></canvas>                                
                            </div>
                        </ContentTemplate>
                    </ajaxtoolKit:TabPanel>
                </ajaxtoolKit:TabContainer>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>       
    <asp:HiddenField ID="filtrarSucursal" runat="server" />
    <script type="text/javascript">
        var chartCartera;
        var Meses = new Array(), CarteraVencida = new Array(), Cobros = new Array(), PorAplicar = new Array();

        $(document).ready(function ()
        {
            actualizarChartCartera();            
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            // Activar Tooltips
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            chartCartera = null;
            cargaChartCartera();
            formatearSaldoVencido_Balance();            
        });

        function formatearSaldoVencido_Balance()
        {
            $('#<%= gvBalanceCuentas.ClientID %> tbody  > tr').each(function () {
                var tdSaldoVencido = $(this).find('td.jsSaldoVencido');
                var dSaldoVencido = tdSaldoVencido.text().replace(/[^0-9\.-]+/g, "");

                if (dSaldoVencido > 0)
                    tdSaldoVencido.addClass('danger');
            });
        }       

        function actualizarChartCartera()
        {            
            // Obtener la informacion para cargar la grafica.
            var data_a = { "Anio": $('#<%= ddlAnioChartCartera.ClientID %>').val(), "MesInicio": $('#<%= ddlMesDesdeCartera.ClientID %>').val(), "MesFin": $('#<%= ddlMesHastaCartera.ClientID %>').val() };            
                        
            $.ajax({
                type: "POST",
                url: "frmGestionCobranza.aspx/ObtenerDatosChartCartera",
                data: JSON.stringify(data_a),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    Meses = []; CarteraVencida = []; Cobros = []; PorAplicar = [];

                    $.each(msg.d["Meses"], function (i, v) {                    
                        Meses.push(v);
                    });                    
                    $.each(msg.d["CarteraVencida"], function (i, v) {
                        CarteraVencida.push(v);
                    });
                    $.each(msg.d["Cobros"], function (i, v) {
                        Cobros.push(v);
                    });
                    $.each(msg.d["PorAplicar"], function (i, v) {
                        PorAplicar.push(v);
                    });

                    // Desplegar la grafica con la Informacion Actualizada
                    cargaChartCartera();
                },
                error: function (result) {
                    alert("Error al cargar la Grafica de la Cartera " + result.status + ' ' + result.statusText);
                }
            });            
        }

        function cargaChartCartera() {            
            var ctx = document.getElementById("cvsChartCartera").getContext("2d");
            var data = {
                labels: Meses,            
                datasets: [
                    {
                        label: "Cartera Vencida",
                        backgroundColor: "#ffe6e6",
                        borderColor: "#ff9999",
                        borderWidth: 1,
                        data: CarteraVencida
                    },
                    {
                        label: "Cobros",
                        backgroundColor: "#ccffcc",
                        borderColor: "#99ff99",
                        borderWidth: 1,
                        data: Cobros
                    },
                    {
                        label: "Por Aplicar",
                        backgroundColor: "#e5e5e5",
                        borderColor: "#b3b3b3",
                        borderWidth: 1,
                        data: PorAplicar
                    }
                ]
            };

            // Carga la grafica por primera vez
            if (chartCartera == null) {
                chartCartera = new Chart(ctx, {
                    type: 'horizontalBar',
                    data: data,
                    options: {
                        barValueSpacing: 20,
                        tooltips: {
                            callbacks: {
                                label: function (tooltipItem, data) {                                   
                                    return formatCurrency(tooltipItem.xLabel);
                                },
                            }
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                            xAxes: [{
                                ticks: {                                    
                                    callback: function (value, index, values) {                                        
                                        return formatCurrency(value);
                                    }
                                }
                            }]
                        }
                    }
                });                
            }
            else {
                // Actualiza la informacion de la grafica
                chartCartera.data = data;
                chartCartera.update();
            }
        }
</script>
</asp:Content>
