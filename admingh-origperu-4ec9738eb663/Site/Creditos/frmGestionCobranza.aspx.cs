﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using wsSOPF;

public partial class Site_Creditos_frmGestionCobranza : System.Web.UI.Page
{   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // Cargar el ddl filtro del chart cartera
            CargaComboAnioChartCartera();

            // Cargar el ddl filtro Canal Pago
            CargaComboCanalPago();

            // Cargar seleccionado por default el Mes actual y Año actual para la carga inicial de la Cartera
            ddlAnioChartCartera.SelectedValue = DateTime.Now.Year.ToString();
            ddlMesDesdeCartera.SelectedValue = DateTime.Now.Month.ToString();
            ddlMesHastaCartera.SelectedValue = DateTime.Now.Month.ToString();
        }
    }

    #region "Balance"

    protected void btnBuscar_Balance_Click(object sender, EventArgs e)
    {
        pnlError_FltBalance.Visible = false;

        try
        {
            Balance_Filtrar();
        }
        catch (Exception ex)
        {
            lblError_FltBalance.Text = ex.Message;
            pnlError_FltBalance.Visible = true;
        }
    }

    protected void lbtnRegresar_Balance_Click(object sender, EventArgs e)
    {
        pnlBuscar_Balance.Visible = true;
        pnlVerBalance_Balance.Visible = false;        
    }  

    protected void gvBalanceCuentas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "VerBalance":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {                    
                    Balance_VerBalance_Cargar(int.Parse(e.CommandArgument.ToString()));
                }
                break;
        }
    }

    protected void gvBalanceCuentas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBalanceCuentas.PageIndex = e.NewPageIndex;
        Balance_Filtrar();
    }

    protected void gvRecibos_VerBalance_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            wsSOPF.BalanceCuenta.Recibos recibo = (wsSOPF.BalanceCuenta.Recibos)e.Row.DataItem;

            // Obtenemos los Index de cada Columna para accesarla posteriormente
            int indexSaldoCapital = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoCapital");
            int indexSaldoInteres = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoInteres");
            int indexSaldoIGV = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoIGV");
            int indexSaldoSeguro = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoSeguro");
            int indexSaldoGAT = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoGAT");
            int indexSaldoOtrosCargos = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoOtrosCargos");
            int indexSaldoIGVOtrosCargos = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoIGVOtrosCargos");

            // Damos formato a las celdas de los Saldos Vencidos (recibos que esten Activos y que sean de fechas pasadas)
            if (recibo.FechaRecibo.Value < DateTime.Today.AddDays(1) && recibo.EstatusClave == "A")
            {
                if (recibo.SaldoCapital > 0)
                    e.Row.Cells[indexSaldoCapital].CssClass += "small danger";

                if (recibo.SaldoInteres > 0)
                    e.Row.Cells[indexSaldoInteres].CssClass += "small danger";

                if (recibo.SaldoIGV > 0)
                    e.Row.Cells[indexSaldoIGV].CssClass += "small danger";

                if (recibo.SaldoSeguro > 0)
                    e.Row.Cells[indexSaldoSeguro].CssClass += "small danger";

                if (recibo.SaldoGAT > 0)
                    e.Row.Cells[indexSaldoGAT].CssClass += "small danger";

                if (recibo.SaldoOtrosCargos > 0)
                    e.Row.Cells[indexSaldoOtrosCargos].CssClass += "small danger";

                if (recibo.SaldoIGVOtrosCargos > 0)
                    e.Row.Cells[indexSaldoIGVOtrosCargos].CssClass += "small danger";
            }
            else
            {
                // Damos formato a las celdas de los Recibos Saldados 
                // Damos formato a las celdas de los Recibos Cancelados
                // Damos formato a las celdas de los Recibos Condonados
                string cssEstatusRecibo = "small";
                switch (recibo.EstatusClave)
                {
                    case "S":
                        cssEstatusRecibo += " success";
                        break;
                    case "C":
                        cssEstatusRecibo += " warning";
                        break;
                    case "RC":
                        cssEstatusRecibo += " active";
                        break;
                }

                // Aplicamos el css correspondiente.
                e.Row.Cells[indexSaldoCapital].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoInteres].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoIGV].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoSeguro].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoGAT].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoOtrosCargos].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoIGVOtrosCargos].CssClass += string.Format(" {0}", cssEstatusRecibo);
            }
        }
    }

    protected void gvPagos_VerBalance_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            wsSOPF.Pago dataItem = (wsSOPF.Pago)e.Row.DataItem;

            // Obtenemos los Index de cada Columna para accesarla posteriormente
            int indexEstatusPago = ControlsHelper.GetColumnIndexByName(e.Row, "Estatus.EstatusDesc");

            switch (dataItem.Estatus.EstatusClave)
            {
                case "PR":
                    e.Row.Cells[indexEstatusPago].CssClass = "active";
                    break;
                case "PP":
                    e.Row.Cells[indexEstatusPago].CssClass = "active";
                    break;
                case "PA":
                    e.Row.Cells[indexEstatusPago].CssClass = "success";
                    break;
                case "PC":
                    e.Row.Cells[indexEstatusPago].CssClass = "warning";
                    break;
                case "PD":
                    e.Row.Cells[indexEstatusPago].CssClass = "";
                    break;
                case "DR":
                    {
                        e.Row.CssClass = "info";
                        ((Panel)e.Row.FindControl("pnlOpcionesPago")).Visible = false;
                        break;
                    }
                case "DA":
                    {
                        e.Row.CssClass = "danger";
                        ((Panel)e.Row.FindControl("pnlOpcionesPago")).Visible = false;
                        break;
                    }
                case "DE":
                    {
                        e.Row.CssClass = "warning";
                        ((Panel)e.Row.FindControl("pnlOpcionesPago")).Visible = false;
                        break;
                    }
            }

            if (dataItem.Estatus.EstatusClave == "PR" || dataItem.Estatus.EstatusClave == "PP")
            {
                // Si hay al menos un pago (PR o PP), mostramos la seccion de aplicar todos los pagos
                pnlAplicarPagos.Visible = true;

                ((LinkButton)e.Row.FindControl("lbtnAplicarPago")).Visible = true;
                ((LinkButton)e.Row.FindControl("lbtnAplicarPagoUnico")).Visible = true;
            }

            if (dataItem.Estatus.EstatusClave == "PA" || dataItem.Estatus.EstatusClave == "PP")
                ((LinkButton)e.Row.FindControl("lbtnDesaplicarPago")).Visible = true;

            if (dataItem.Estatus.EstatusClave == "PR")
                ((LinkButton)e.Row.FindControl("lbtnCancelarPago")).Visible = true;
        }
    }

    protected void gvPagos_VerBalance_RowCommand(object sender, GridViewCommandEventArgs e)
    {       
        switch (e.CommandName)
        {
            case "AplicarPago":
            case "AplicarPagoUnico":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    bool bAplicarUnico = (e.CommandName == "AplicarPagoUnico") ? true : false;
                    ResultadoOfboolean res = ws.Pagos_AplicarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) }, bAplicarUnico);                    

                    // Si hay algun error al Aplicar el Pago lo Mostramos, Sino Actualizamos los Recibos y Pagos.
                    if (res.Codigo > 0)
                    {
                        lblError_Balance_VerBalance_Pagos.Text = res.Mensaje;
                        pnlError_Balance_VerBalance_Pagos.Visible = true;
                    }
                    else
                    {                      
                        Balance_VerBalance_Cargar(int.Parse(lblIdSolicitud_VerBalance.Text.Trim()));                        
                    }
                }
                break;
            case "DesaplicarPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean res = ws.Pagos_DesaplicarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) });

                    // Si hay algun error al Desaplicar el Pago lo Mostramos, Sino Actualizamos los Recibos y Pagos.
                    if (res.Codigo > 0)
                    {
                        lblError_Balance_VerBalance_Pagos.Text = res.Mensaje;
                        pnlError_Balance_VerBalance_Pagos.Visible = true;
                    }
                    else
                    {
                        Balance_VerBalance_Cargar(int.Parse(lblIdSolicitud_VerBalance.Text.Trim()));
                    }
                }
                break;
            case "CancelarPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean res = ws.Pagos_CancelarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) });

                    // Si hay algun error al Desaplicar el Pago lo Mostramos, Sino Actualizamos los Recibos y Pagos.
                    if (res.Codigo > 0)
                    {
                        lblError_Balance_VerBalance_Pagos.Text = res.Mensaje;
                        pnlError_Balance_VerBalance_Pagos.Visible = true;
                    }
                    else
                    {
                        Balance_VerBalance_Cargar(int.Parse(lblIdSolicitud_VerBalance.Text.Trim()));
                    }
                }
                break;
        }
    }

    protected void lbtnAplicarPagos_Click(object sender, EventArgs e)
    {
        try
        {
            List<wsSOPF.Pago> pagos = (List<wsSOPF.Pago>)gvPagos_VerBalance.DataSource;
            using (wsSOPF.CobranzaAdministrativaClient wsCobranza = new CobranzaAdministrativaClient())
            {
                pagos = wsCobranza.Pagos_Filtrar(new Pago() { IdSolicitud = int.Parse(lblIdSolicitud_VerBalance.Text.Trim()) }).ToList();
            }

            foreach (wsSOPF.Pago p in pagos)
            {
                if (p.Estatus.EstatusClave == "PR" || p.Estatus.EstatusClave == "PP")
                {
                    using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                    {
                        ResultadoOfboolean res = ws.Pagos_AplicarPago(new Pago() { IdPago = p.IdPago }, chkAplicarPagosUnico.Checked);

                        // Si hay algun error al Aplicar el Pago lo Mostramos, Sino Actualizamos los Recibos y Pagos.
                        if (res.Codigo > 0)
                        {
                            lblError_Balance_VerBalance_Pagos.Text = res.Mensaje;
                            pnlError_Balance_VerBalance_Pagos.Visible = true;
                        }
                    }
                }
            }

            Balance_VerBalance_Cargar(int.Parse(lblIdSolicitud_VerBalance.Text.Trim()));
        }
        catch (Exception ex)
        {
            pnlError_Balance_VerBalance_Pagos.Visible = true;
            lblError_Balance_VerBalance_Pagos.Text = ex.Message;
        }
    }

    protected void gvRecibos_VerBalance_Liquidacion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            wsSOPF.TBCreditos.Liquidacion.LiquidacionDetalle objData = (wsSOPF.TBCreditos.Liquidacion.LiquidacionDetalle)e.Row.DataItem;

            // Obtenemos los Index de cada Columna para accesarla posteriormente            
            int idxInteres = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaInteres");
            int idxIGV = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaIGV");
            int idxSeguro = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaSeguro");
            int idxGAT = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaGAT");

            e.Row.Cells[idxInteres].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaInteres.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxIGV].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaIGV.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxSeguro].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaSeguro.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxGAT].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaGAT.ToString("C", CultureInfo.CurrentCulture) : "-";
        }
    }

    private void Balance_VerBalance_Cargar(int IdSolicitud)
    {
        pnlError_Balance_VerBalance_Pagos.Visible = false;        
        tpLiquidacion_VerBalance.Visible = false;
        tpAmpliacionAnterior_VerBalance.Visible = false;
        tpAmpliacionPosterior_VerBalance.Visible = false;        
        pnlAplicarPagos.Visible = false;

        using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
        {
            wsSOPF.BalanceCuenta eCuenta = new wsSOPF.BalanceCuenta() { IdSolicitud = IdSolicitud };
            eCuenta = ws.Balance_Obtener(eCuenta);

            // Verificar si el Credito esta Liquidado, cargamos los datos de la Liquidacion
            if (eCuenta.EstatusCredito.EstatusClave == "L")
                Balance_VerBalance_CargarLiquidacion(IdSolicitud);

            if (eCuenta.EstatusCredito.EstatusClave == "LA")
                Balance_VerBalance_CargarDatosAmpliacionPosterior(IdSolicitud);

            List<wsSOPF.BalanceCuenta.Recibos> eRecibos = ws.Balance_Obtener_Recibos(eCuenta).ToList();
            List<wsSOPF.Pago> ePagos = ws.Pagos_Filtrar(new Pago() { IdSolicitud = eCuenta.IdSolicitud }).ToList();

            lblIdSolicitud_VerBalance.Text = eCuenta.IdSolicitud.ToString();
            lblCliente_VerBalance.Text = string.Format("{0} {1} {2}", eCuenta.ClienteNombre, eCuenta.ClienteApPaterno, eCuenta.ClienteApMaterno);
            lblFechaCredito_VerBalance.Text = eCuenta.FechaCredito.Value.GetValueOrDefault().ToString("dd/MM/yyyy");
            lblProducto_VerBalance.Text = eCuenta.ProductoDesc;
            
            lblMontoUsoCanal_VerBalance.Text = eCuenta.MontoUsoCanal.ToString("C", CultureInfo.CurrentCulture);
            lblMontoGAT_VerBalance.Text = eCuenta.MontoGAT.ToString("C", CultureInfo.CurrentCulture);
            lblCuota_VerBalance.Text = eCuenta.MontoCuota.ToString("C", CultureInfo.CurrentCulture);
            lblCapital_VerBalance.Text = eCuenta.Capital.ToString("C", CultureInfo.CurrentCulture);
            lblSaldoVencido_VerBalance.Text = eCuenta.SaldoVencido.ToString("C", CultureInfo.CurrentCulture);
            lblCostoTotalCredito_VerBalance.Text = eCuenta.CostoTotalCredito.ToString("C", CultureInfo.CurrentCulture);
            lblEstatusCredito_VerBalance.Text = eCuenta.EstatusCredito.EstatusDesc;
            lblTipoCredito_VerBalance.Text = eCuenta.TipoCreditoDesc.ToUpper();
            lblMontoComision_VerBalance.Text = eCuenta.ComisionOrigen.ToString("C", CultureInfo.CurrentCulture);

            if (eCuenta.TipoCreditoDesc.ToUpper() == "AMPLIACIÓN")
                Balance_VerBalance_CargarDatosAmpliacionAnterior(IdSolicitud);

            gvRecibos_VerBalance.DataSource = eRecibos;
            gvRecibos_VerBalance.DataBind();

            // AGREGAMOS LAS DEVOLUCIONES AL GRID DE PAGOS

            #region Devoluciones

            wsSOPF.DevolucionCondiciones condiciones = new wsSOPF.DevolucionCondiciones();

            condiciones.idSolicitud = eCuenta.IdSolicitud;

            List<wsSOPF.Devolucion> devoluciones = ws.ObtenerDevoluciones(condiciones).ToList();

            if (devoluciones.Count > 0)
            {
                foreach (wsSOPF.Devolucion dev in devoluciones)
                {
                    string desc = "DEVOLUCION";
                    string clave = string.Empty;
                    switch (dev.idEstatus) { case 0: clave = "DR"; break; case 1: clave = "DA"; break; case 2: clave = "DE"; break; default: break; }
                    Pago nPago = new Pago()
                    {
                        CanalPagoDesc = desc,
                        FechaPago = new UtilsDateTimeR() { Value = dev.fechaPago.GetValueOrDefault().Date + new TimeSpan(23, 59, 59) },
                        MontoCobrado = dev.montoPago,
                        MontoAplicado = dev.idEstatus == 1 ? dev.montoPago : 0,
                        MontoPorAplicar = dev.idEstatus == 1 ? 0 : dev.montoPago,
                        Estatus = new TBCATEstatus() {
                            EstatusDesc = dev.resultado,
                            EstatusClave = clave
                        }
                    };
                    ePagos.Add(nPago);
                }
            }

            #endregion

            ePagos.Sort(new Comparison<Pago>((x, y) => DateTime.Compare(x.FechaPago.Value.GetValueOrDefault(), y.FechaPago.Value.GetValueOrDefault())));

            gvPagos_VerBalance.DataSource = ePagos;
            gvPagos_VerBalance.DataBind();

            pnlBuscar_Balance.Visible = false;
            pnlVerBalance_Balance.Visible = true;
            AsignarPermisosGridPagos();
        }
    }

    private void Balance_VerBalance_CargarLiquidacion(int IdSolicitud)
    {
        try
        {
            // Mostrar el Tab Panel Liquidacion que esta oculto por Default
            tpLiquidacion_VerBalance.Visible = true;

            using (wsSOPF.CreditoClient ws = new CreditoClient())
            {
                wsSOPF.TBCreditos.Liquidacion LiquidacionCredito = new TBCreditos.Liquidacion();
                LiquidacionCredito.IdSolicitud = IdSolicitud;

                // Obtener los datos de la Liquidacion del Credito
                LiquidacionCredito = ws.ObtenerLiquidacionCredito(LiquidacionCredito);

                if (LiquidacionCredito == null)
                    throw new Exception("Error al cargar la Liquidacion del Credito");

                lblIdLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.IdLiquidacion.ToString();
                lblNumeroTransaccionLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.NumeroTransaccion;
                lblFechaLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.FechaLiquidar.Value.GetValueOrDefault().ToString("dd/MM/yyyy");
                lblCapitalLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.CapitalLiquidar.ToString("C", CultureInfo.CurrentCulture);
                lblInteresLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.InteresLiquidar.ToString("C", CultureInfo.CurrentCulture);
                lblIGVLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.IGVLiquidar.ToString("C", CultureInfo.CurrentCulture);
                lblSeguroLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.SeguroLiquidar.ToString("C", CultureInfo.CurrentCulture);
                lblGATLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.GATLiquidar.ToString("C", CultureInfo.CurrentCulture);
                lblTotalLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.TotalLiquidar.ToString("C", CultureInfo.CurrentCulture);

                gvRecibos_VerBalance_Liquidacion.DataSource = LiquidacionCredito.Detalle;
                gvRecibos_VerBalance_Liquidacion.DataBind();                
            }
        }        
        catch (Exception ex)
        {          
            lblError_VerBalance_Liquidacion.Text = ex.Message;
            pnlError_VerBalance_Liquidacion.Visible = true;
        }
    }

    private void Balance_VerBalance_CargarDatosAmpliacionAnterior(int IdSolicitud)
    {
        try
        {
            // Mostrar el Tab Panel AmpliacionAnterior que esta oculto por Default
            tpAmpliacionAnterior_VerBalance.Visible = true;

            using (wsSOPF.CreditoClient ws = new CreditoClient())
            {
                wsSOPF.TBCreditos.AmpliacionEntity AmpliacionCredito = new TBCreditos.AmpliacionEntity();                

                // Obtener los datos de la Liquidacion del Credito
                wsSOPF.ResultadoOfTBCreditosAmpliacionEntityhDx61Z0Z res = ws.ObtenerInfoAmpliacionAnterior(IdSolicitud);

                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);

                AmpliacionCredito = res.ResultObject;                

                txtIdSolicitudAnterior_AmpliacionAnterior.Text = AmpliacionCredito.IdSolicitudAmplia.ToString();
                txtFechaAmpliacion_AmpliacionAnterior.Text = AmpliacionCredito.FechaLiquidar.GetValueOrDefault().ToString("dd/MM/yyyy");
                txtCapitalLiquida_AmpliacionAnterior.Text = AmpliacionCredito.CapitalLiquidar.ToString("C", CultureInfo.CurrentCulture);
                txtInteresLiquida_AmpliacionAnterior.Text = AmpliacionCredito.InteresLiquidar.ToString("C", CultureInfo.CurrentCulture);
                txtIGVLiquida_AmpliacionAnterior.Text = AmpliacionCredito.IGVLiquidar.ToString("C", CultureInfo.CurrentCulture);
                txtSeguroLiquida_AmpliacionAnterior.Text = AmpliacionCredito.SeguroLiquidar.ToString("C", CultureInfo.CurrentCulture);
                txtGATLiquida_AmpliacionAnterior.Text = AmpliacionCredito.GATLiquidar.ToString("C", CultureInfo.CurrentCulture);
                txtTotalLiquida_AmpliacionAnterior.Text = AmpliacionCredito.TotalLiquidar.ToString("C", CultureInfo.CurrentCulture);                
            }
        }
        catch (Exception ex)
        {
            lblError_AmpliacionAnterior_VerBalance.Text = ex.Message;
            pnlError_AmpliacionAnterior_VerBalance.Visible = true;
        }
    }

    private void Balance_VerBalance_CargarDatosAmpliacionPosterior(int IdSolicitud)
    {
        try
        {
            // Mostrar el Tab Panel AmpliacionPosterior que esta oculto por Default
            tpAmpliacionPosterior_VerBalance.Visible = true;

            using (wsSOPF.CreditoClient ws = new CreditoClient())
            {
                wsSOPF.TBCreditos.AmpliacionEntity AmpliacionCredito = new TBCreditos.AmpliacionEntity();

                // Obtener los datos de la Liquidacion del Credito
                wsSOPF.ResultadoOfTBCreditosAmpliacionEntityhDx61Z0Z res = ws.ObtenerInfoAmpliacionPosterior(IdSolicitud);

                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);

                AmpliacionCredito = res.ResultObject;

                txtIdSolicitudAmplia_AmpliacionPosterior.Text = AmpliacionCredito.IdSolicitud.ToString();
                txtFechaAmpliacion_AmpliacionPosterior.Text = AmpliacionCredito.FechaLiquidar.GetValueOrDefault().ToString("dd/MM/yyyy");
                txtCapitalLiquida_AmpliacionPosterior.Text = AmpliacionCredito.CapitalLiquidar.ToString("C", CultureInfo.CurrentCulture);
                txtInteresLiquida_AmpliacionPosterior.Text = AmpliacionCredito.InteresLiquidar.ToString("C", CultureInfo.CurrentCulture);
                txtIGVLiquida_AmpliacionPosterior.Text = AmpliacionCredito.IGVLiquidar.ToString("C", CultureInfo.CurrentCulture);
                txtSeguroLiquida_AmpliacionPosterior.Text = AmpliacionCredito.SeguroLiquidar.ToString("C", CultureInfo.CurrentCulture);
                txtGATLiquida_AmpliacionPosterior.Text = AmpliacionCredito.GATLiquidar.ToString("C", CultureInfo.CurrentCulture);
                txtTotalLiquida_AmpliacionPosterior.Text = AmpliacionCredito.TotalLiquidar.ToString("C", CultureInfo.CurrentCulture);
            }
        }
        catch (Exception ex)
        {
            lblError_AmpliacionPosterior_VerBalance.Text = ex.Message;
            pnlError_AmpliacionPosterior_VerBalance.Visible = true;
        }
    }

    #endregion

    #region "Pagos"

    protected void btnBuscar_Pagos_Click(object sender, EventArgs e)
    {
        pnlError_FltPagosDom.Visible = false;

        try
        {
            Pagos_Filtrar();
        }
        catch (Exception ex)
        {
            lblError_FltPagosDom.Text = ex.Message;
            pnlError_FltPagosDom.Visible = true;
        }
    }       

    protected void gvPagosDomiciliacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPagosDomiciliacion.PageIndex = e.NewPageIndex;
        Pagos_Filtrar();   
    }

    #endregion
    
    #region "Metodos Privados" 

    private void Balance_Filtrar()
    {
        wsSOPF.BalanceCuenta entity = new BalanceCuenta();
        int IDSolicitud = 0;
        DateTime? fchCreditoDesde = null;
        DateTime? fchCreditoHasta = null;

        if (int.TryParse(txtSolicitud_Balance.Text.Trim(), out IDSolicitud))
            entity.IdSolicitud = IDSolicitud;

        if (!string.IsNullOrEmpty(txtFechaCreditoDesde_Balance.Text.Trim()))
            fchCreditoDesde = DateTime.ParseExact(txtFechaCreditoDesde_Balance.Text.Trim(), "dd/MM/yyyy", null);

        if (!string.IsNullOrEmpty(txtFechaCreditoHasta_Balance.Text.Trim()))
            fchCreditoHasta = DateTime.ParseExact(txtFechaCreditoHasta_Balance.Text.Trim(), "dd/MM/yyyy", null);

        entity.EstatusCredito = new TBCATEstatus();
        entity.EstatusCredito.IdEstatus = int.Parse(ddlEstatusCredito_Balance.SelectedValue);
        entity.ClienteNombre = txtCliente_Balance.Text.Trim();
        entity.FechaCredito = new UtilsDateTimeR();
        entity.FechaCredito.ValueIni = fchCreditoDesde;
        entity.FechaCredito.ValueEnd = fchCreditoHasta;

        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            List<wsSOPF.BalanceCuenta> cuentas = ws.Balance_Filtrar(entity).ToList();

            gvBalanceCuentas.DataSource = cuentas;
            gvBalanceCuentas.DataBind();
        }
    }

    private void Pagos_Filtrar()
    {
        wsSOPF.Pago entity = new Pago();
        entity.FechaPago = new UtilsDateTimeR();
        entity.FechaRegistro = new UtilsDateTimeR();

        int IDSolicitud = 0;      

        if (int.TryParse(txtSolicitud_Pagos.Text.Trim(), out IDSolicitud))
            entity.IdSolicitud = IDSolicitud;

        if (!string.IsNullOrEmpty(txtFechaPagoDesde_Pagos.Text.Trim()))
            entity.FechaPago.ValueIni = DateTime.ParseExact(txtFechaPagoDesde_Pagos.Text.Trim(), "dd/MM/yyyy", null);

        if (!string.IsNullOrEmpty(txtFechaPagoHasta_Pagos.Text.Trim()))
            entity.FechaPago.ValueEnd = DateTime.ParseExact(txtFechaPagoHasta_Pagos.Text.Trim(), "dd/MM/yyyy", null);

        if (!string.IsNullOrEmpty(txtFechaRegistroDesde_Pagos.Text.Trim()))
            entity.FechaRegistro.ValueIni = DateTime.ParseExact(txtFechaRegistroDesde_Pagos.Text.Trim(), "dd/MM/yyyy", null);

        if (!string.IsNullOrEmpty(txtFechaRegistroHasta_Pagos.Text.Trim()))
            entity.FechaRegistro.ValueEnd = DateTime.ParseExact(txtFechaRegistroHasta_Pagos.Text.Trim(), "dd/MM/yyyy", null);

        entity.Estatus = new TBCATEstatus();
        entity.Estatus.IdEstatus = int.Parse(ddlEstatusPago_Pagos.SelectedValue);

        entity.CanalPago = new CanalPago();
        entity.CanalPago.IdCanalPago = int.Parse(ddlCanalPago.SelectedValue);

        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            List<wsSOPF.Pago> pagos = ws.Pagos_Filtrar(entity).ToList();

            gvPagosDomiciliacion.DataSource = pagos;
            gvPagosDomiciliacion.DataBind();
        }
    }

    private void CargaComboAnioChartCartera()
    {
        int AnioInicioOperaciones = 2017;

        for (int AnioActual = DateTime.Now.Year; AnioActual >= AnioInicioOperaciones; AnioActual--)
        {
            ddlAnioChartCartera.Items.Add(new ListItem(AnioActual.ToString(), AnioActual.ToString()));
        }
    }

    private void CargaComboCanalPago()
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            List<wsSOPF.CanalPago> canalesPago = ws.CanalPago_ListarActivos().ToList();

            ddlCanalPago.DataSource = canalesPago;
            ddlCanalPago.DataBind();
        }
    }

    [System.Web.Services.WebMethod]
    public static ResultDatosChartCartera ObtenerDatosChartCartera(int Anio, int MesInicio, int MesFin)
    {
        ResultDatosChartCartera datosChart = new ResultDatosChartCartera();
        string mesC = string.Empty;

        using (wsSOPF.CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
        {
            List<PagosChartCartera> chartMeses = ws.Pagos_ChartCartera(Anio, MesInicio, MesFin).ToList();

            foreach (PagosChartCartera pc in chartMeses)
            {
                datosChart.Meses.Add(pc.AnioMes);
                datosChart.CarteraVencida.Add(pc.CarteraVencida);
                datosChart.Cobros.Add(pc.Cobros);
                datosChart.PorAplicar.Add(pc.PorAplicar);
            }
        }

        return datosChart;
    }

    private void AsignarPermisosGridPagos()
    {
        ValidaRecursoAccesoResponse validaRec;
        int idUsuario = HttpContext.Current.ValidarUsuario();
        if (idUsuario <= 0) throw new Exception("La clave del usuario no es válida.");

        using (SistemaClient wsS = new SistemaClient())
        {
            string recurso = Request.Url.AbsolutePath;
            recurso = recurso.Substring(recurso.IndexOf("/") + 1, recurso.Length - 1);
            validaRec = wsS.ValidaRecursoAcceso(new ValidaRecursoAccesoRequest
            {
                IdUsuario = idUsuario,
                Recurso = recurso
            });
        }

        if (validaRec != null && validaRec.RecursoAcciones != null && validaRec.RecursoAcciones.Length > 0)
        {
            if (validaRec.RecursoAcciones.Any(ac => ac.Accion == "MenuPago"))
            {
                ((DataControlField)gvPagos_VerBalance.Columns
              .Cast<DataControlField>()
              .Where(fld => (fld.AccessibleHeaderText == "OpcionesPago"))
              .SingleOrDefault()).Visible = true;
            }
        }
    }

    public class ResultDatosChartCartera
    {
        public List<string> Meses { get; set; }
        public List<decimal> CarteraVencida { get; set; }
        public List<decimal> Cobros { get; set; }
        public List<decimal> PorAplicar { get; set; }

        public ResultDatosChartCartera()
        {
            this.Meses = new List<string>();
            this.CarteraVencida = new List<decimal>();
            this.Cobros = new List<decimal>();
            this.PorAplicar = new List<decimal>();
        }
    }

    #endregion     
}