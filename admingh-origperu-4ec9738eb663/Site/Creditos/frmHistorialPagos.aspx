﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmHistorialPagos.aspx.cs" Inherits="Creditos_frmHistorialPagos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Historial de Pagos</title>
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>

    <link href="/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/bootstrap.min.css" /> 
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <div id="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <blockquote>
                    <p>Historial de Pagos</p>
                </blockquote>
                <div class="GridContenedor">
                    <asp:GridView runat="server" ID="gvPagos" DataKeyNames="IdCuenta" Width="100%"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow"
                        AlternatingRowStyle-CssClass="GridRowAlternate" CssClass="Grid table table-hover" EmptyDataRowStyle-CssClass="GridEmptyData"
                        GridLines="None" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <Columns>
                            <asp:BoundField HeaderText="Canal de Pago" DataField="CanalPago" />
                            <asp:BoundField HeaderText="Fecha Pago" DataField="FechaPago" DataFormatString="{0:d}" />
                            <asp:BoundField HeaderText="Monto del Pago" DataField="MontoCobrado" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="Aplicado" DataField="MontoAplicado" DataFormatString="{0:c2}" />                                                                                                                 
                            <asp:BoundField HeaderText="Por Aplicar" DataField="MontoPorAplicar" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="Estatus" DataField="Estatus" />                                 
                        </Columns>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
