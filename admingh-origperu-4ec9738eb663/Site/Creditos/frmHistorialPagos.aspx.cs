﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Creditos_frmHistorialPagos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataResultCreditoUsp_HistorialPagos[] Pagos = null;
            
            using (wsSOPF.CreditoClient wsCredito = new CreditoClient())
            {
                Pagos = wsCredito.ObtenerHistorialPagos(Convert.ToInt32(Request.QueryString["idcuenta"]));

                if (Pagos.Length > 0)
                {
                    gvPagos.DataSource = Pagos;
                    gvPagos.DataBind();
                }
            }
        }
    }
}