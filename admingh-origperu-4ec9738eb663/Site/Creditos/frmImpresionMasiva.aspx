﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmImpresionMasiva.aspx.cs" Inherits="Creditos_frmImpresionMasiva" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <script language="JavaScript" type="text/javascript">
        function Pregunta() {
            __doPostBack('Actualizar_PostBack', "");
        }
    </script>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Impresion de Formularios</h3>
        </div>
        <div class="panel-body">
            <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>
            <div class="izq">                
                <div class="row">
                    <div class="form-group col-sm-6 col-md-4 col-lg-4">
                        <label class="form-control-label small">Tipo de Crédito</label>                        
                        <asp:DropDownList ID="cmbFormato" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cmbFormato_OnSelectedIndexChanged "></asp:DropDownList>
                    </div>                        
                      
                    <div class="form-group col-sm-6 col-md-4 col-lg-4">
                        <label class="form-control-label small">Número de impresiones</label>                             
                        <asp:TextBox ID="txtCantidad" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                                        
                <div class="form-group row col-sm-12">                    
                    <asp:Button ID="btnImprimir" CssClass="btn btn-sm btn-info" runat="server" Text="Imprimir" OnClick="btnImprimir_Click" />                        
                    <asp:Button ID="btnActualizar" CssClass="btn btn-sm btn-default" runat="server" Text="Revisar Impresiones Pendientes" OnClick="btnActualizar_Click"/>                      
                </div>

                <div class="col-sm-12 col-md-6">
                    <asp:Panel ID="pnlError" CssClass="alert alert-danger small" style="padding:0px 15px;" runat="server" Visible="false">
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                        <asp:LinkButton ID="btnOk" runat="server" Text="OK" OnClick="btnOK_Click" CssClass="btn btn-sm">
                            <i class="fas fa-times-circle fa-lg" style="color: #cc0000;"></i>
                        </asp:LinkButton>
                    </asp:Panel>
                </div>                    

                <div class="form-group row col-sm-12">
                    <div class="form-group" style="overflow:auto">
                        <asp:GridView runat="server" ID="grdLotes" DataKeyNames="IdLote" AutoGenerateColumns="False"
                            CssClass="table table-bordered table-striped  table-hover table-responsive">
                            <Columns>
                                <asp:BoundField HeaderText="Lote" DataField="IdLote" />                                
                                <asp:BoundField HeaderText="Formato" DataField="Formato" />
                                <asp:BoundField HeaderText="Fecha Alta" DataField="Fecha" />
                                <asp:BoundField HeaderText="Folio Inicial" DataField="FolioInicial" />
                                <asp:BoundField HeaderText="Folio Final" DataField="FolioFinal" />

                                <%-- <asp:TemplateField HeaderText="Confirmar Estatus">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="btnEstatus" CommandName="editar"><%#Eval("EstatusLote")%></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>

                                <%-- %><asp:TemplateField HeaderText="Estatus">
                            <ItemTemplate>
                                <asp:DropDownList ID="cmbEstatusLote" runat="server" CommandName="seleccionar" Text ='<%#Bind("IdEstatusLote") %>'></asp:DropDownList>
                                <asp:HiddenField id="hdnEstatusLote" runat="server" Value='<%# Eval("IdEstatusLote") %>'/> 
                            </ItemTemplate>
                        </asp:TemplateField>--%>

                                <asp:TemplateField>
                                    <ItemStyle Width="10px" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnLote" runat="server" Value='<%# Eval("IdLote") %>' />
                                        <asp:Button ID="btnConfirmar" runat="server" CommandName="guardar"
                                            CommandArgument="<%# ((GridViewRow)Container).RowIndex %>"
                                            Text="Confirmar" CssClass="btn btn-sm btn-success" OnClick="btnConfirmar_Click"></asp:Button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemStyle Width="10px" />
                                    <ItemTemplate>
                                        <asp:Button ID="btnCancelar" runat="server" CommandName="cancelar"
                                            CommandArgument="<%# ((GridViewRow)Container).RowIndex %>"
                                            Text="Cancelar" CssClass="btn btn-sm btn-danger" OnClick="btnCancelar_Click"></asp:Button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron impresiones pendientes.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>                
            </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnImprimir" />
            </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
