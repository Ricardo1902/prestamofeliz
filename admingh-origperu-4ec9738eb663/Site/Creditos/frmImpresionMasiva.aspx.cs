﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Linq;
using wsSOPF;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text.RegularExpressions;
using System.Web.Hosting;

public partial class Creditos_frmImpresionMasiva : System.Web.UI.Page
{
    private const int posXMax = 612;
    private const int posYMax = 1008;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = "";

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        if (!IsPostBack)
        {
            List<TBCATFormatos> ds = new List<TBCATFormatos>();

            TBCATFormatos[] sArray = ds.ToArray();

            using (wsSOPF.CatalogoClient wsFormato = new CatalogoClient())
            {
                sArray = wsFormato.ConsultaFormatos(0, 99, "", 0, idSucursal);

                //ACTUALIZAMOS SOLO PARA USO DE ESTE ASPX
                var fContrato = sArray.SingleOrDefault(f => f.Formato == "CONTRATO PERÚ");
                if (fContrato != null)
                    fContrato.Formato = "Crédito Nuevo";

                cmbFormato.Items.Clear();
                cmbFormato.DataTextField = "Formato";
                cmbFormato.DataValueField = "IdFormato";
                cmbFormato.DataSource = sArray.OrderByDescending(f => f.Formato);

                cmbFormato.DataBind();
            }

            //Sacamos la consulta de los pendientes (detalle)
            ObtenerLotesImpresionPendientes(idSucursal, idUsu, false);
        }

        /*else
        {
            //Sacamos la consulta de los registros que por ahora estan pendientes porque se acaban de dar de alta
            LotesImpresion[] DetalleIni = null;
            using (wsSOPF.CreditoClient wsDetalleIni = new CreditoClient())
            {
                DetalleIni = wsDetalleIni.ConsultarLotesImpresion(2, 0, DateTime.Now, idSucursal, idUsu, 0, 0, 1, Convert.ToInt32(cmbFormato.SelectedValue));
                grdLotes.DataSource = DetalleIni;
                grdLotes.DataBind();
            }
        }*/
    }

    protected void cmbFormato_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        lblError.Text = "";

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        //Sacamos la consulta de los pendientes (detalle)
        ObtenerLotesImpresionPendientes(idSucursal, idUsu, true);

        txtCantidad.Text = "";
    }

    protected void btnConfirmar_Click(object sender, EventArgs e)
    {
        pnlError.Visible = false;

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        foreach (GridViewRow registro in grdLotes.Rows)
        {
            int _hdnLote;
            _hdnLote = 0;

            HiddenField hdnIdLote = registro.FindControl("hdnLote") as HiddenField;
            System.Web.UI.WebControls.Button btnConfirmar = registro.FindControl("btnConfirmar") as System.Web.UI.WebControls.Button;

            _hdnLote = Convert.ToInt32(hdnIdLote.Value);

            //Actualizamos el estatus de la impresión
            wsSOPF.CreditoClient wsEstatusLote = new CreditoClient();
            wsEstatusLote.ActualizarLote(1, DateTime.Now, 0, Convert.ToInt32(cmbFormato.SelectedValue), 0, 0, 0, 2, _hdnLote);

            //Sacamos la consulta de los registros que por ahora estan pendientes porque se acaban de dar de alta
            ObtenerLotesImpresionPendientes(idSucursal, idUsu, true);
        }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        pnlError.Visible = false;

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        foreach (GridViewRow registro in grdLotes.Rows)
        {
            int _hdnLote;
            _hdnLote = 0;

            HiddenField hdnIdLote = registro.FindControl("hdnLote") as HiddenField;
            System.Web.UI.WebControls.Button btnCancelar = registro.FindControl("btnCancelar") as System.Web.UI.WebControls.Button;

            _hdnLote = Convert.ToInt32(hdnIdLote.Value);

            //Actualizamos el estatus de la impresión
            wsSOPF.CreditoClient wsEstatusLote = new CreditoClient();
            wsEstatusLote.ActualizarLote(1, DateTime.Now, 0, Convert.ToInt32(cmbFormato.SelectedValue), 0, 0, 0, 3, _hdnLote);

            //Sacamos la consulta de los registros que por ahora estan pendientes porque se acaban de dar de alta
            ObtenerLotesImpresionPendientes(idSucursal, idUsu, true);
        }
    }

    protected void btnImprimir_Click(object sender, EventArgs e)
    {
        // Sacamos la clave de la sucursal        
        pnlError.Visible = false;

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        // Validacion que no dejen el textbox vacio
        if (string.IsNullOrEmpty(txtCantidad.Text.Trim()))
        {
            lblError.Text = "ERROR: Capture la Cantidad";
            pnlError.Visible = true;

            return;
        }

        // Validacion de texto que solo sean numeros 
        String expValEntero;
        expValEntero = "[1234567890]";

        if (Regex.Replace(txtCantidad.Text, expValEntero, String.Empty).Length != 0)
        {
            lblError.Text = "ERROR: Solo puede capturar numeros";
            pnlError.Visible = true;
            return;
        }

        // Validacion de texto que solo sean maximo 10 contratos
        if (Convert.ToInt32(txtCantidad.Text) > 10)
        {
            lblError.Text = "ERROR: Solo puede imprimir como máximo 10 contratos por lote";
            pnlError.Visible = true;
            return;
        }

        if (txtCantidad.Text.Trim() == "")
        {
            lblError.Text = "ERROR: Debe de capturar un numero";
            pnlError.Visible = true;
            return;
        }

        if (txtCantidad.Text.Trim() == "0")
        {
            lblError.Text = "ERROR: La cantidad no puede ser cero";
            pnlError.Visible = true;
            return;
        }

        int veces;
        veces = Convert.ToInt32(txtCantidad.Text);
        txtCantidad.Text = "";

        //Sacamos la consulta de los pendientes (detalle)
        ObtenerLotesImpresionPendientes(idSucursal, idUsu, true);

        // Sacamos el maximo folio de los confirmados
        LotesImpresion[] TotPen = null;
        using (wsSOPF.CreditoClient wsTotPen = new CreditoClient())
        {
            TotPen = wsTotPen.ConsultarLotesImpresion(1, 0, DateTime.Now, idSucursal, idUsu, 0, 0, 2, Convert.ToInt32(cmbFormato.SelectedValue)); //Estatus 2 = Confirmado
        }

        int TotalPendientes;
        TotalPendientes = Convert.ToInt32(TotPen[0].Cantidad);

        //TotalPendientes = grdLotes.Rows.Count;

        if (TotalPendientes == 0)       // Quiere decir que no hay pendientes de confirmar y puede seguir imprimiendo
        {
            // Sacamos el maximo folio de los confirmados
            LotesImpresion[] Folio = null;
            using (wsSOPF.CreditoClient wsFolio = new CreditoClient())
            {
                Folio = wsFolio.ConsultarLotesImpresion(1, 0, DateTime.Now, idSucursal, idUsu, 0, 0, 2, Convert.ToInt32(cmbFormato.SelectedValue)); //Estatus 2 = Confirmado
            }

            int FolMaxConfirmados;
            FolMaxConfirmados = Convert.ToInt32(Folio[0].FolioFinal);

            // Insertamos el lote a imprimir con estatus 1 - Alta de Impresion
            int FolioInicial;
            int FolioFinal;

            FolioInicial = FolMaxConfirmados + 1;
            FolioFinal = FolMaxConfirmados + veces;

            wsSOPF.CreditoClient wsRegistro = new CreditoClient();
            wsRegistro.InsertarLote(0, DateTime.Now, idUsu, Convert.ToInt32(cmbFormato.SelectedValue), idSucursal, FolioInicial, FolioFinal, 1, 0);

            //Sacamos la consulta de los pendientes (detalle)
            LotesImpresion[] DetalleP = null;
            DetalleP = ObtenerLotesImpresionPendientes(idSucursal, idUsu, true);

            TBCATSucursal Suc = null;
            TBCATSucursal[] Sucursal = null;

            using (wsSOPF.UsuarioClient wsSuc = new UsuarioClient())
            {
                Suc = new TBCATSucursal();
                Suc.Accion = 1;
                Suc.IdSucursal = idSucursal;
                Suc.Sucursal = "";
                Suc.IdEstatus = 0;
                Suc.IdAfiliado = 0;
                Sucursal = wsSuc.ObtenerSucursal(Suc);
            }

            string cveSucurs;
            cveSucurs = Sucursal[0].cve_sucursal;

            // Consultamos los datos del formato para sacar plantilla
            TBCATFormatos[] Datos = null;
            using (wsSOPF.CatalogoClient wsDatosFormato = new CatalogoClient())
            {
                Datos = wsDatosFormato.ConsultaFormatos(1, Convert.ToInt32(cmbFormato.SelectedValue), "", 0, idSucursal);
            }

            List<FormatoPaginaVM> formatos = new List<FormatoPaginaVM>();

            //Obtener páginas a imprimir folio
            using (SolicitudClient ws = new SolicitudClient())
            {
                ObtenerPaginasFormatosResponse res = ws.ObtenerPaginasFormatos();
                if (!res.Error && res.formatos.Length > 0)
                {
                    formatos = new List<FormatoPaginaVM>(res.formatos);
                }
            }

            List<int> paginasValidas = new List<int>();
            List<FormatoPaginaVM> fs = formatos.Where(f => f.IdFormato == Convert.ToInt32(cmbFormato.SelectedValue)).ToList();
            if (fs != null && fs.Count > 0)
            {
                foreach (FormatoPaginaVM p in fs)
                {
                    for(int i = p.PaginaInicial; i <= p.PaginaFinal; i++)
                    {
                        paginasValidas.Add(i);
                    }
                    if(p.CantidadCopias > 1)
                    {
                        for(int j = 1; j < p.CantidadCopias; j++) //copia 2 en delante empieza en 1 pues la primera ya se agregó
                        {
                            //Se suma 2 porque: 1 por obtener cantidad de paginas del formato al sumar 1 a la resta, y otro para pagina en blanco
                            //TODO: agregar totalpaginas a bd para evitar calcular y campo AgregaHojaBlanco
                            int diff = j * (p.PaginaFinal - p.PaginaInicial + 2);
                            for (int k = p.PaginaInicial + diff; k <= p.PaginaFinal + diff; k++)
                            {
                                paginasValidas.Add(k);
                            }
                        }
                    }
                }
            }

            string Plantilla;
            string Descarga;
            string DescargaIni;
            string DescargaFin;

            Plantilla = ("PlantillasPDF/" + Datos[0].NombrePlantilla).ToString() + ".pdf";
            DescargaIni = DetalleP[0].FolioInicial.ToString().Trim();
            DescargaFin = DetalleP[0].FolioFinal.ToString().Trim();

            Descarga = Datos[0].NombreDescarga.ToString().Trim() + " Folios " + DescargaIni + "al " + DescargaFin;

            // Mandamos imprimir
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename= " + Descarga + ".pdf");
            byte[] outputs = null;

            Document document = new Document();
            PdfCopy copy = new PdfCopy(document, Response.OutputStream);
            document.Open();
            for (int j = 1; j <= veces; j++)
            {
                //Generar con AcroFields
                //outputs = GeneratePDF(Server.MapPath(Plantilla), cveSucurs + " N°" + Convert.ToString(j + FolMaxConfirmados));
                //Generar con iText
                outputs = GenerarPDF(Server.MapPath(Plantilla), cveSucurs + " N°" + Convert.ToString(j + FolMaxConfirmados), paginasValidas.ToArray());
                PdfReader reader = new PdfReader(outputs);

                for (int pageCounter = 1; pageCounter < reader.NumberOfPages + 1; pageCounter++)
                {
                    copy.AddPage(copy.GetImportedPage(reader, pageCounter));
                }
                reader.Close();
            }

            document.Close();
            btnActualizar_Click(sender, e);
        }
        else
        {
            //Sacamos la consulta de los pendientes (detalle)
            ObtenerLotesImpresionPendientes(idSucursal, idUsu, false);

            txtCantidad.Text = "";

            lblError.Text = "ERROR: Existen impresiones pendientes por confirmar";
            pnlError.Visible = true;
        }
    }

    private static byte[] GenerarPDF(string pdfPath, string folio, int[] paginasValidas)
    {
        BaseFont arial = BaseFont.CreateFont(HostingEnvironment.ApplicationPhysicalPath + "fonts\\arial.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);

        using (var reader = new PdfReader(pdfPath))
        {
            Rectangle tamanioOrientacion = reader.GetPageSizeWithRotation(1);
            int posMaxY = (int)tamanioOrientacion.Height;
            using (MemoryStream ms = new MemoryStream())
            {
                var document = new Document(reader.GetPageSizeWithRotation(1));
                var writer = PdfWriter.GetInstance(document, ms);

                document.Open();

                for (var i = 1; i <= reader.NumberOfPages; i++)
                {
                    document.NewPage();

                    var importedPage = writer.GetImportedPage(reader, i);

                    var contentByte = writer.DirectContent;
                    contentByte.BeginText();
                    if (paginasValidas.Contains(i))
                        EscribirTexto(contentByte, folio, 575, posMaxY - 15, 8, arial, alineacion: PdfContentByte.ALIGN_RIGHT);

                    /* CODIGO PARA DIBUJAR LA CUADRICULA, NO BORRAR */
                    //for (int ai = 0; ai < posYMax; ai += 5)
                    //{
                    //    EscribirTexto(contentByte, PosY(ai).ToString(), 2, ai, 4, arial, 0);
                    //    for (int j = 10; j < posXMax; j += 10)
                    //    {
                    //        EscribirTexto2(contentByte, j.ToString(), j, PosY(ai), 5, arial, 0);
                    //    }
                    //}

                    contentByte.EndText();
                    contentByte.AddTemplate(importedPage, 0, 0);
                }

                document.Close();
                writer.Close();
                return ms.ToArray();
            }
        }
    }

    public static byte[] GeneratePDF(string pdfPath, string sucursalFolio)
    {
        var output = new MemoryStream();
        var reader = new PdfReader(pdfPath);
        var stamper = new PdfStamper(reader, output);
        var formFields = stamper.AcroFields;

        stamper.AcroFields.SetField("Folio", sucursalFolio.ToString());
        stamper.FormFlattening = true;
        stamper.Close();
        reader.Close();

        return output.ToArray();
    }

    private static int PosY(int posY)
    {
        return posYMax - posY;
    }

    private static void EscribirTexto2(PdfContentByte contenido, string texto, int x, int y, int tamanioLetra, BaseFont fuente, int alineacion = PdfContentByte.ALIGN_LEFT)
    {
        if (string.IsNullOrEmpty(texto)) texto = "";
        contenido.SetFontAndSize(fuente, tamanioLetra);
        contenido.SetRGBColorFill(200, 100, 200);
        contenido.ShowTextAligned(alineacion, texto, x, y, 0);
    }

    private static void EscribirTexto(PdfContentByte contenido, string texto, int x, int y, int tamanioLetra, BaseFont fuente, int alineacion = PdfContentByte.ALIGN_LEFT)
    {
        if (string.IsNullOrEmpty(texto)) texto = "";
        contenido.SetFontAndSize(fuente, tamanioLetra);
        contenido.SetRGBColorFill(255, 0, 0); //rojo
        contenido.ShowTextAligned(alineacion, texto, x, y, 0);
        contenido.SetRGBColorFill(0, 0, 0); //negro se regresa a negro en caso de existir objetos fill en pdf base
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        pnlError.Visible = false;

        // Sacamos la clave de la sucursal
        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        //Sacamos la consulta de los pendientes (detalle)
        ObtenerLotesImpresionPendientes(idSucursal, idUsu, false);

        txtCantidad.Text = "";
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Sacamos la clave de la sucursal        
        lblError.Text = "";

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        //Sacamos la consulta de los pendientes (detalle)
        ObtenerLotesImpresionPendientes(idSucursal, idUsu, false);

        txtCantidad.Text = "";
        pnlError.Visible = false;
    }

    private LotesImpresion[] ObtenerLotesImpresionPendientes(int idSucursal, int idUsuario, bool soloSeleccionado)
    {
        LotesImpresion[] DetallePen = null;
        using (wsSOPF.CreditoClient wsDetallePen = new CreditoClient())
        {
            DetallePen = wsDetallePen.ConsultarLotesImpresion(2, 0, DateTime.Now, idSucursal, idUsuario, 0, 0, 1, Convert.ToInt32(cmbFormato.SelectedValue));
            if (DetallePen.Length <= 0 && !soloSeleccionado)
            {
                foreach (System.Web.UI.WebControls.ListItem item in cmbFormato.Items)
                {
                    if (item.Value == cmbFormato.SelectedValue)
                        continue;
                    DetallePen = wsDetallePen.ConsultarLotesImpresion(2, 0, DateTime.Now, idSucursal, idUsuario, 0, 0, 1, Convert.ToInt32(item.Value));
                    if (DetallePen.Length > 0)
                        break;
                }
            }
            
            grdLotes.DataSource = DetallePen;
            grdLotes.DataBind();
        }

        return DetallePen;
    }
}
