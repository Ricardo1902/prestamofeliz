﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Site_Creditos_frmPagosDirectos : System.Web.UI.Page
{
    #region "Propiedades"

    protected string NombreArchivo
    {
        get
        {
            return (ViewState[this.UniqueID + "_NombreArchivo"] != null) ? ViewState[this.UniqueID + "_NombreArchivo"].ToString() : string.Empty;
        }
        set
        {
            ViewState[this.UniqueID + "_NombreArchivo"] = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        // Botones de descarga Full Postback para evitar que el Update Panel Falle
        ScriptManager.GetCurrent(this).RegisterPostBackControl(lbtnDescargar_Aprobados);

        if (!Page.IsPostBack)
        {
            // Ocultamos los paneles de detalle de inicio
            pnlAprobadosDetalle.Visible = false;
            pnlErrorDetalle.Visible = false;          
        }
    }

    #region "Pagos Directos"

    protected void btnCargarArchivo_Click(object sender, EventArgs e)
    {        
        pnlError_CargarArchivo.Visible = false;

        try
        {
            using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
            {                
                // Validar antes de cargar el archivo
                if (!fuArchivo.HasFile)
                    throw new Exception("Seleccione el Archivo a cargar");
                else if (ws.PagosDirectosArchivo_Filtrar(new wsSOPF.ArchivoMasivo() { Nombre = Path.GetFileNameWithoutExtension(fuArchivo.FileName.Trim()) + ".txt" }).Count() > 0)
                    throw new Exception("Ya existe un Archivo con el mismo nombre");


                // Guardar el archivo a la carpeta local de manera TEMPORAL (manejo en memoria)
                string folderPath = Server.MapPath("~/Archivos/PagosDirectos/");
                fuArchivo.SaveAs(folderPath + "TMP_" + Path.GetFileName(fuArchivo.FileName));

                this.NombreArchivo = fuArchivo.FileName.Trim();

                ProcesarCSV();

                pnlCarga_CargarArchivo.Visible = false;
                pnlResumen_CargarArchivo.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblError_CargarArchivo.Text = ex.Message;
            pnlError_CargarArchivo.Visible = true;
        }
    }

    protected void tcCargaArchivos_ActiveTabChanged(object sender, EventArgs e)
    {
        switch (tcCargaArchivos.ActiveTab.HeaderText.Trim())
        {
            case "Cargar Archivo":
                pnlError_CargarArchivo.Visible = false;                
                break;
            case "Archivos Cargados":
                CargarArchivosAprobados();

                pnlExito_ArchivosCargados.Visible = false;
                pnlAprobadosGV.Visible = true;
                pnlAprobadosDetalle.Visible = false;
                pnlDescargaError_Aprobados.Visible = false;
                break;
            case "Archivos con Error":
                CargarArchivosError();

                pnlErrorGV.Visible = true;
                pnlErrorDetalle.Visible = false;
                pnlDescargaError_Error.Visible = false;
                break;
        }
    }

    protected void lbtnGuardarArchivoPagosDirectos_CargarArchivo_Click(object sender, EventArgs e)
    {
        try
        {
            ProcesarCSV();
            ConvertirCSVToTxt();
            GuardarArchivo();

            pnlCarga_CargarArchivo.Visible = true;
            pnlResumen_CargarArchivo.Visible = false;

            pnlExito_ArchivosCargados.Visible = true;
            tcCargaArchivos.ActiveTab = tpArchivosCargados;

            CargarArchivosAprobados();
        }
        catch (Exception ex)
        {
            pnlCarga_CargarArchivo.Visible = true;
            pnlResumen_CargarArchivo.Visible = false;

            pnlError_CargarArchivo.Visible = true;
            lblError_CargarArchivo.Text = ex.Message;
        }
    }

    #region "Archivos Aprobados"

    protected void lbtnRegresar_Aprobados_Click(object sender, EventArgs e)
    {
        pnlAprobadosGV.Visible = true;
        pnlAprobadosDetalle.Visible = false;
        pnlDescargaError_Aprobados.Visible = false;
    }

    protected void lbtnDescargar_Aprobados_Click(object sender, EventArgs e)
    {
        if (File.Exists(Server.MapPath("~/Archivos/PagosDirectos/") + lblNombreArchivo_Aprobados.Text.Trim()))
            Response.Redirect(string.Format("~/DownloadFile.ashx?fileName={0}", Server.MapPath("~/Archivos/PagosDirectos/") + lblNombreArchivo_Aprobados.Text.Trim()));
        else
            pnlDescargaError_Aprobados.Visible = true;
    }

    protected void gvArchivosAprobados_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "VerDetalle":
                using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
                {
                    wsSOPF.ArchivoMasivo archivo = ws.DomiciliacionArchivo_ObtenerDetalle(int.Parse(e.CommandArgument.ToString()));

                    lblNombreArchivo_Aprobados.Text = archivo.Nombre;
                    lblTipoArchivo_Aprobados.Text = archivo.TipoArchivo.Descripcion;                    
                    lblFechaRegistro_Aprobados.Text = archivo.FechaRegistro.GetValueOrDefault().ToString();

                    lblDetalleArchivo_Aprobados.Text = string.Empty;
                    foreach (wsSOPF.ArchivoMasivoDetalle d in archivo.Detalle)
                    {
                        lblDetalleArchivo_Aprobados.Text += d.Descripcion.Replace(" ", "&nbsp;") + "<br/>";
                    }

                    pnlAprobadosGV.Visible = false;
                    pnlAprobadosDetalle.Visible = true;
                }
                break;
        }
    }

    protected void gvArchivosAprobados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvArchivosAprobados.PageIndex = e.NewPageIndex;
        CargarArchivosAprobados();
    }

    #endregion

    #region "Archivos con Error"

    protected void lbtnRegresar_Error_Click(object sender, EventArgs e)
    {
        pnlErrorGV.Visible = true;
        pnlErrorDetalle.Visible = false;
        pnlDescargaError_Error.Visible = false;
    }

    protected void lbtnDescargar_Error_Click(object sender, EventArgs e)
    {
        if (File.Exists(Server.MapPath("~/Archivos/PagosDirectos/") + lblNombreArchivo_Error.Text.Trim()))
            Response.Redirect(string.Format("~/DownloadFile.ashx?fileName={0}", Server.MapPath("~/Archivos/DomiciliacionCargaMasiva/") + lblNombreArchivo_Error.Text.Trim()));
        else
            pnlDescargaError_Error.Visible = true;
    }

    protected void gvArchivosError_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "VerDetalle":
                using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
                {
                    wsSOPF.ArchivoMasivo archivo = ws.PagosDirectosArchivo_ObtenerDetalle(int.Parse(e.CommandArgument.ToString()));

                    lblNombreArchivo_Error.Text = archivo.Nombre;
                    lblTipoArchivo_Error.Text = archivo.TipoArchivo.Descripcion;                    
                    lblFechaRegistro_Error.Text = archivo.FechaRegistro.GetValueOrDefault().ToString();

                    lblDetalleArchivo_Error.Text = string.Empty;
                    foreach (wsSOPF.ArchivoMasivoDetalle d in archivo.Detalle)
                    {
                        lblDetalleArchivo_Error.Text += d.Descripcion.Replace(" ", "&nbsp;") + "<br/>";
                    }

                    pnlErrorGV.Visible = false;
                    pnlErrorDetalle.Visible = true;
                }
                break;
        }
    }

    protected void gvArchivosError_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvArchivosError.PageIndex = e.NewPageIndex;
        CargarArchivosError();
    }

    #endregion

    #endregion

    #region "Metodos"

    private void ProcesarCSV()
    {
        lbtnGuardarArchivoPagosDirectos_CargarArchivo.Enabled = true;

        string folderPath = Server.MapPath("~/Archivos/PagosDirectos/");
        List<PagosDirectosExcelResumen> PagosCSV = new List<PagosDirectosExcelResumen>();
        PagosDirectosExcelResumen pagoCSV = null;

        using (var reader = new StreamReader(string.Format("{0}{1}", folderPath, "TMP_" + this.NombreArchivo)))
        {            
            string errLines = string.Empty;
            int lineNum = 0;
            int IdSolicitud = 0;
            decimal Monto = 0;
            DateTime FechaPago;

            // Obtener los canales de Pago Activos para validar los pagos Directos antes de guardarlos.
            List<wsSOPF.CanalPago> canalesPago = new List<wsSOPF.CanalPago>();
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                canalesPago = ws.CanalPago_ListarActivos().ToList();
            }

            while (!reader.EndOfStream)
            {
                try
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    // Ignorar el encabezado
                    if (lineNum > 0)
                    {
                        pagoCSV = new PagosDirectosExcelResumen();

                        // Validar los tipos de dato antes de agregarlos como renglon Valido
                        if (!int.TryParse(values[1].Trim(), out IdSolicitud) ||
                            !decimal.TryParse(values[2].Trim(), out Monto) ||
                            !DateTime.TryParseExact(values[3].Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out FechaPago) ||
                            !canalesPago.Any(s => s.Nombre.ToUpper() == values[0].ToString().ToUpper()))
                            throw new Exception(lineNum.ToString());

                        pagoCSV.NoPago = lineNum;
                        pagoCSV.TipoPago = values[0].ToUpper();
                        pagoCSV.IdSolicitud = int.Parse(values[1]);
                        pagoCSV.Monto = decimal.Parse(values[2]);
                        pagoCSV.FechaPago = DateTime.ParseExact(values[3], "dd/MM/yyyy", null);
                        pagoCSV.Observaciones = (values[4] != null) ? values[4] : string.Empty;

                        PagosCSV.Add(pagoCSV);
                    }
                }
                catch (Exception ex)
                {
                    errLines += ex.Message + ", ";
                }

                lineNum++;
            }

            // Mostrar el Resumen del archivo CSV Procesado
            gvResumen_CargarArchivo.DataSource = PagosCSV;
            gvResumen_CargarArchivo.DataBind();

            if (!string.IsNullOrEmpty(errLines))
            {
                lbtnGuardarArchivoPagosDirectos_CargarArchivo.Enabled = false;

                pnlAdvertencia_CargarArchivo.Visible = true;
                lblAdvertencia_CargarArchivo.Text = string.Format("Las siguientes pagos presentan formato incorrecto en su información o el canal de pago no existe:<br/>{0}", errLines.Remove(errLines.LastIndexOf(","), 1).Trim());
            }
        }
    }

    private void ConvertirCSVToTxt()
    {
        string folderPath = Server.MapPath("~/Archivos/PagosDirectos/");
        List<string> txtLines = new List<string>();

        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.TipoArchivo.LayoutArchivo> LayoutPagosDirectos = ws.PagosDirectosLayout_Obtener().ToList();
            IEnumerable<wsSOPF.TipoArchivo.LayoutArchivo> lpdOrder = LayoutPagosDirectos.OrderBy(x => x.IniciaPosicion);

            // Colocamos el encabezado del Archivo TXT
            string txtLine = string.Empty;
            foreach (wsSOPF.TipoArchivo.LayoutArchivo Layout in lpdOrder)
            {
                switch (Layout.Parametro)
                {
                    case "TipoPago":
                        txtLine = txtLine.PadRight(Layout.IniciaPosicion - 1, ' ') + ("TIPO PAGO").PadRight(Layout.Longuitud, ' ');
                        break;
                    case "IdSolicitud":
                        txtLine = txtLine.PadRight(Layout.IniciaPosicion - 1, ' ') + ("SOLICITUD").PadRight(Layout.Longuitud, ' ');
                        break;
                    case "Monto":
                        txtLine = txtLine.PadRight(Layout.IniciaPosicion -1 , ' ') + ("MONTO").PadRight(Layout.Longuitud, ' ');
                        break;
                    case "Fecha":
                        txtLine = txtLine.PadRight(Layout.IniciaPosicion - 1, ' ') + ("FECHA PAGO").PadRight(Layout.Longuitud, ' ');
                        break;
                    case "Observaciones":
                        txtLine = txtLine.PadRight(Layout.IniciaPosicion - 1, ' ') + ("OBSERVACIONES").PadRight(Layout.Longuitud, ' ');
                        break;
                }
            }
            txtLines.Add(txtLine);

            foreach (PagosDirectosExcelResumen Pago in (List<PagosDirectosExcelResumen>)gvResumen_CargarArchivo.DataSource)
            {
                txtLine = string.Empty;
                foreach (wsSOPF.TipoArchivo.LayoutArchivo Layout in lpdOrder)
                {
                    switch (Layout.Parametro)
                    {
                        case "TipoPago":
                            txtLine = txtLine.PadRight(Layout.IniciaPosicion - 1, ' ') + (Pago.TipoPago.Trim()).PadRight(Layout.Longuitud, ' ');
                            break;
                        case "IdSolicitud":
                            txtLine = txtLine.PadRight(Layout.IniciaPosicion - 1, ' ') + (Pago.IdSolicitud.ToString()).PadRight(Layout.Longuitud, ' ');
                            break;
                        case "Monto":
                            txtLine = txtLine.PadRight(Layout.IniciaPosicion - 1, ' ') + (Pago.Monto.ToString()).PadRight(Layout.Longuitud, ' ');
                            break;
                        case "Fecha":
                            txtLine = txtLine.PadRight(Layout.IniciaPosicion - 1, ' ') + (Pago.FechaPago.ToString("dd/MM/yyyy")).PadRight(Layout.Longuitud, ' ');
                            break;
                        case "Observaciones":
                            txtLine = txtLine.PadRight(Layout.IniciaPosicion - 1, ' ') + (Pago.Observaciones.Trim()).PadRight(Layout.Longuitud, ' ');
                            break;
                    }
                }
                txtLines.Add(txtLine);
            }
        }

        // Guardar el TXT y eliminar el CSV temporal
        File.WriteAllLines(folderPath + Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".txt", txtLines);
        File.Delete(folderPath + "TMP_" + this.NombreArchivo);
    }

    private void GuardarArchivo()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {

            string folderPath = Server.MapPath("~/Archivos/PagosDirectos/");

            // Copiamos el archivo al servidor de BD
            string localURL = folderPath + Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".txt";
            string remoteURL = System.Configuration.ConfigurationManager.AppSettings["URLRedArchivoMasivo"];
            string remoteUser = System.Configuration.ConfigurationManager.AppSettings["UsuarioArchivoMasivo"];
            string remotePass = System.Configuration.ConfigurationManager.AppSettings["ClaveArchivoMasivo"];

            NetworkShare.DisconnectFromShare(remoteURL, true); // Nos desconectamos en caso de que estemos actualmente conectados

            NetworkShare.ConnectToShare(remoteURL, remoteUser, remotePass); // Nos conectamos con las nuevas credenciales

            File.Copy(localURL, remoteURL + Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".txt", true);

            NetworkShare.DisconnectFromShare(remoteURL, false); // Nos desconectamos del server

            // Procesar el archivo
            wsSOPF.ArchivoMasivo gEntity = new wsSOPF.ArchivoMasivo();
            gEntity.Nombre = Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".txt";

            wsSOPF.ResultadoOfboolean res = ws.PagosDirectosArchivo_Registrar(gEntity);

            if (res.Codigo != 0)
                throw new Exception(res.Mensaje);
        }        
    }

    private void CargarArchivosAprobados()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.ArchivoMasivo> archivosProcesados = ws.PagosDirectosArchivo_ObtenerProcesados().ToList();

            gvArchivosAprobados.DataSource = archivosProcesados;
            gvArchivosAprobados.DataBind();
        }
    }

    private void CargarArchivosError()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.ArchivoMasivo> archivosError = ws.PagosDirectosArchivo_ObtenerConError().ToList();

            gvArchivosError.DataSource = archivosError;
            gvArchivosError.DataBind();
        }
    }

    #endregion

    #region "Clases Privadas"            

    private class PagosDirectosExcelResumen
    {
        public int NoPago { get; set; }
        public string TipoPago { get; set; }
        public int IdSolicitud { get; set; }
        public decimal Monto { get; set; }
        public DateTime FechaPago { get; set; }
        public string Observaciones { get; set; }
    }

    #endregion    
}