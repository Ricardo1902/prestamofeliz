﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmPagosNoIdentificados.aspx.cs" Inherits="Site_Creditos_frmPagosNoIdentificados" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Pagos No Identificados</h3>
        </div>
        <div class="panel-body">
            <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
                <ProgressTemplate>                 
                    <div id="loader-background"></div>
                    <div id="loader-content"></div>
                </ProgressTemplate> 
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upMain" runat="server">
                <ContentTemplate>
                    <ajaxtoolKit:TabContainer ID="tcPagosNoIndentificados" CssClass="actTab" OnActiveTabChanged="tcPagosNoIndentificados_ActiveTabChanged" AutoPostBack="true" runat="server">
                        <ajaxtoolKit:TabPanel HeaderText="Buscar" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnlGuardaSolicitud" class="col-sm-12" Visible="false" runat="server">
                                    <asp:HiddenField ID="lblIdPagoNoIdentificado" runat="server" /> 
                                    <div class="form-group col-md-4 col-lg-3">
                                        <label class="form-control-label small">Solicitud</label>                                
                                        <div> 
                                            <asp:TextBox ID="txtIdSolicitud" CssClass="form-control" placeholder="Solicitud" MaxLength="10" onKeyPress="return EvaluateText('%d', this);" runat="server"/>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Solicitud es obligatorio" ControlToValidate="txtIdSolicitud" ForeColor="Red" ValidationGroup="GuardaSolicitudVal" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>                                  
                                    <div class="form-group col-md-4 col-lg-3">
                                        <label class="form-control-label small">Canal de pago</label>                                                                        
                                        <asp:TextBox ID="lblCanalPago" CssClass="form-control"  runat="server" Enabled="false"/>                                                                                     
                                    </div>
                                    <div class="form-group col-md-4 col-lg-3">
                                        <label class="form-control-label small">Monto</label>                                                                        
                                        <asp:TextBox ID="lblMonto" CssClass="form-control"  runat="server" Enabled="false"/>                                                                                     
                                    </div>
                                    <div class="form-group col-md-4 col-lg-3">
                                        <label class="form-control-label small">Fecha de Pago</label>                                                                        
                                        <asp:TextBox ID="lblFechaPago" CssClass="form-control"  runat="server" Enabled="false"/>                                                  
                                    </div>                                                                       
                                    <div class="form-group col-md-4 col-lg-12">
                                        <input type="button" onclick="javascript: document.getElementById('<%=btnGuardaIdSolicitud.ClientID%>').click();" class="btn btn-sm btn-success" value="Guardar" />
                                        <input type="button" onclick="javascript: document.getElementById('<%=btnCancelaGuardaIdSolicitud.ClientID%>').click();" class="btn btn-sm btn-danger" value="Cancelar" />
                                    </div>

                                </asp:Panel>
                                <asp:Panel ID="pnlErrorBusqueda" class="col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <asp:Label ID="lblErrorBusqueda" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlGuardaSoliExito" class="col-sm-12" Visible="false" runat="server">                                
                                            <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                <i class="far fa-check-circle"></i>
                                                <asp:Label ID="lblGuardaSoliExito" runat="server"></asp:Label>
                                            </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlCarga_BuscarPagos" runat="server">
                                    <div class="form-group col-md-4 col-lg-6">
                                        <label class="form-control-label small">Fecha Pago Inicio</label>                                
                                        <div>     
                                            <div class="input-group">
                                                <asp:TextBox ID="txtFechaReciboDesde" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                                <span class="input-group-addon">
                                                    <asp:LinkButton ID="btnSelectFechaCreditoDesde_Balance" runat="server">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </asp:LinkButton>
                                                </span>
                                            </div>                                                         
                                            <asp:RegularExpressionValidator runat="server"
                                                    ErrorMessage="Formato Incorrecto"
                                                    ControlToValidate="txtFechaReciboDesde"
                                                    ForeColor="Red"
                                                    ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />                                    
                                            <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaReciboDesde" PopupButtonID="btnSelectFechaCreditoDesde_Balance" runat="server"></ajaxtoolKit:CalendarExtender>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="El campo Fecha Inicio es obligatorio" ControlToValidate="txtFechaReciboDesde" ForeColor="Red" ValidationGroup="BusquedaVal"></asp:RequiredFieldValidator>--%>
                                        </div>         
                                    </div> 

                                    <div class="form-group col-md-4 col-lg-6">
                                        <label class="form-control-label small">Fecha Pago Fin</label>                                
                                        <div>                    
                                            <div class="input-group">
                                                <asp:TextBox ID="txtFechaReciboHasta" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                                <span class="input-group-addon">
                                                    <asp:LinkButton ID="btnSelectFechaCreditoHasta_Balance" runat="server">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </asp:LinkButton>
                                                </span>
                                            </div>                    
                                            <asp:RegularExpressionValidator runat="server"
                                                    ErrorMessage="Formato Incorrecto"
                                                    ControlToValidate="txtFechaReciboHasta"
                                                    ForeColor="Red"
                                                    ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                            <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaReciboHasta" PopupButtonID="btnSelectFechaCreditoHasta_Balance" runat="server"></ajaxtoolKit:CalendarExtender>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El campo Fecha Fin es obligatorio" ControlToValidate="txtFechaReciboHasta" ForeColor="Red" ValidationGroup="BusquedaVal"></asp:RequiredFieldValidator>--%>
                                        </div>        
                                    </div>
                                    
                                    <div class="form-group col-md-4 col-lg-12">                                 
                                        <input type="button" onclick="javascript: document.getElementById('<%=btnBusqueda.ClientID%>').click();" class="btn btn-sm btn-primary" value="Buscar pagos" />
                                    </div>

                                    <asp:Panel ID="pnlError_BuscarPago" class="col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <asp:Label ID="lblError_BuscarPago" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                    
                                    <div style ="height:auto; width:100%; overflow-x:auto;">
                                    <asp:GridView ID="gvPagosNoIdentificados"                                                 
                                        OnPageIndexChanging="gvPagosNoIdentificados_PageIndexChanging"
                                        runat="server" Width="100%" AutoGenerateColumns="False"
                                        Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-hover-warning table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                        PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        Style="overflow-x:auto; overflow-y:scroll;"                                         
                                        selectedindex="1"
                                        onselectedindexchanged="CustomersGridView_SelectedIndexChanged"
                                        autogenerateselectbutton="true">
                                        <PagerStyle CssClass="pagination-ty" />
                                        <Columns>                                            
                                            <asp:BoundField HeaderText="#" DataField="IdPagoNoIdentificado" /> 
                                            <asp:BoundField HeaderText="Canal de pago" DataField="NombreCanal"/>                                      
                                            <asp:BoundField HeaderText="Monto" DataField="Monto" DataFormatString="{0:C2}"/>                                            
                                            <asp:BoundField HeaderText="Fecha de Pago" DataField="Fch_pago" DataFormatString="{0:dd/MM/yyyy}"/>
                                            <asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No se encontraron registros de pagos no identificados
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    </div>
                                </asp:Panel>                                
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                        <ajaxtoolKit:TabPanel ID="tpCargaMasiva" HeaderText="Carga Masiva" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnlCarga_CargarArchivoPagos" runat="server">  
                                <div class="form-group col-sm-12">           
                                        <label>Formato e Instrucciones</label>
                                        <p>
                                            <ul>
                                                <li>
                                                    Seleccione el archivo xlsx para realizar la carga de Pagos No Identificados, el nombre del archivo servira para identificar la carga que se
                                                    realiza, debe ser un nombre único de lo contrario el sistema lo marcara como duplicado.
                                                </li>
                                                <li>
                                                    La información de los Pagos Directos se debe presentar en el siguiente orden (Columnas):<br />
                                                    <span class="small"><strong>CANAL DE PAGO, MONTO, FECHA PAGO </strong>(DD/MM/AAAA)<strong>, OBSERVACIONES</strong></span>
                                                </li>
                                                <li>
                                                    La primer linea del archivo se ignorara ya que deben ser los encabezados en el orden indicado
                                                </li>
                                                <li>
                                                    Descarga el Layout para comenzar
                                                    <a href="/archivos/LayoutPagosNoIdentificados.xlsx" class="btn btn-sm btn-default" download="Layout Pagos No Identificados.xlsx"><i class="fas fa-download fa-lg"></i> Descargar</a>
                                                </li>
                                            </ul>
                                        </p>
                                </div>
                                <div class="form-group col-md-4 col-lg-12">   
                                    <asp:FileUpload runat="server" CssClass="form-control" ID="archPagos" accept=".xls,.xlsx" />
                                    <asp:label id="lblErrorSubirArchivopagos" runat="server" Text="Elija un archivo xls para subir" forecolor="Red" Visible="false"/>
                                </div>
                                <div class="form-group col-md-4 col-lg-12">
                                    <asp:RegularExpressionValidator
                                      id="RegularExpressionValidator1"                                    
                                      ErrorMessage="El archivo debe ser formato xls o xlsx"
                                      ValidationExpression ="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                      ControlToValidate="archPagos" ValidationGroup="CargaArchivoVal" CssClass="small" ForeColor="Red" Display="Dynamic"
                                      runat="server"></asp:RegularExpressionValidator>
                                </div>
                                <div class="form-group col-md-4 col-lg-12">
                                    <input type="button" onclick="javascript: document.getElementById('<%=btnCargaArchivoPagos.ClientID%>').click();" class="btn btn-sm btn-primary" value="Subir Pagos" />
                                </div>
                                <asp:Panel ID="pnlExito_ArchivosCargados" class="col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <i class="far fa-check-circle"></i>
                                        <asp:Label ID="lblExito_ArchivosCargados" Text="La informacion ha sido registrada" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>
                                    <asp:Panel ID="pnlErrorGuardarArchivo" class="col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <asp:Label ID="lblErrorGuardarArchivo" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                                <asp:Panel ID="pnlResumen_CargarArchivo" Visible="false" runat="server">                                  
                                    <div class="col-sm-12 form-group">
                                        <strong>Nombre del Archivo: </strong> <%= this.NombreArchivo %>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-12" style="width:auto; height:auto; overflow-x: auto;">                                        
                                        <asp:GridView ID="gvResumen_CargarArchivo"                                                                                 
                                            runat="server" Width="100%" AutoGenerateColumns="False"
                                            HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None"
                                            RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            >                                        
                                            <Columns>    
                                                <asp:BoundField HeaderText="#" DataField="NoPago" />
                                                <asp:BoundField HeaderText="Canal de Pago" DataField="TipoPago" />
                                                
                                                <asp:BoundField HeaderText="Monto" DataField="Monto" DataFormatString="{0:C2}" />                                                               
                                                <asp:BoundField HeaderText="Fecha de Pago" DataField="FechaPago" DataFormatString="{0:d}" />                                            
                                                <asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />                                            
                                            </Columns>
                                            <EmptyDataTemplate>
                                                No se encontraron Pagos Directos Validos para Registrar
                                            </EmptyDataTemplate>
                                        </asp:GridView>                                        
                                    </div>

                                    <asp:Panel ID="pnlAdvertencia_CargarArchivo" class="col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-warning" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <i class="fas fa-exclamation-circle"></i>
                                            <asp:Label ID="lblAdvertencia_CargarArchivo" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>

                                    <div class="col-sm-12">
                                        <asp:LinkButton ID="lbtnGuardarArchivoPagosDirectos_CargarArchivo" OnClick="btnGuardaArchivoPagos_Click" CssClass="btn btn-sm btn-success" runat="server">
                                            <i class="far fa-check-circle"></i> Guardar Pagos
                                        </asp:LinkButton>

                                        <a href="frmPagosNoIdentificados.aspx" class="btn btn-sm btn-danger">
                                            <i class="far fa-times-circle"></i>
                                            Cancelar
                                        </a>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel> 
                         
                        <ajaxtoolKit:TabPanel HeaderText="Carga Individual" runat="server">
                            <ContentTemplate>
                                <div class="form-group col-sm-12">
                                    <div style="height:auto; width: auto; overflow-x: auto;">                                        
                                        <div class="form-group col-md-4 col-lg-3">
                                            <label class="form-control-label small">Canal de pago</label>                                
                                            <div>                                                                    
                                                <asp:DropDownList id="ddlCanalPago"
                                                        AutoPostBack="True"
                                                        CssClass="form-control"
                                                        runat="server">
                                                    <asp:ListItem Selected="True" Text="--Seleccione--" Value="">--Seleccione--</asp:ListItem>
                                                </asp:DropDownList>                                                                                                                                                              
                                            </div>                                            
                                        </div>
                                        <div class="form-group col-md-4 col-lg-3">
                                            <label class="form-control-label small">Fecha de Pago</label>                                
                                            <div>                    
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtFechaPago" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10"  runat="server"/>                    
                                                    <span class="input-group-addon">
                                                        <asp:LinkButton ID="btnSelectFechaPago_Balance" runat="server">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </asp:LinkButton>
                                                    </span>
                                                </div>                    
                                                <asp:RegularExpressionValidator runat="server"
                                                        ErrorMessage="Formato Incorrecto"
                                                        ControlToValidate="txtFechaPago"
                                                        ForeColor="Red"
                                                        ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                                <ajaxtoolKit:CalendarExtender Format="dd/MM/yyyy" TargetControlID="txtFechaPago" PopupButtonID="btnSelectFechaPago_Balance" runat="server"></ajaxtoolKit:CalendarExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Fecha Pago es obligatorio" ControlToValidate="txtFechaPago" ForeColor="Red" ValidationGroup="GuardaPagoVal" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                            </div>        
                                        </div>
                                        <div class="form-group col-md-4 col-lg-3">
                                            <label class="form-control-label small">Monto</label>                                
                                            <div>                                                                    
                                                <asp:TextBox ID="txtMonto" CssClass="form-control" placeholder="Monto" MaxLength="10" onKeyPress="return EvaluateText('%f', this);" runat="server"/>                                                                                            
                                            </div>     
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Monto es obligatorio" ControlToValidate="txtMonto" ForeColor="Red" ValidationGroup="GuardaPagoVal" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>   
                                        </div>
                                        <div class="form-group col-md-12 col-lg-9">
                                            <label class="form-control-label small">Observaciones</label>                                
                                            <div>                                                                    
                                                <asp:TextBox  ID="txtObserva" TextMode="multiline" CssClass="form-control" placeholder="Observaciones" Columns="50" Rows="5" MaxLength="250"  runat="server"/>                                                 
                                            </div>        
                                        </div>
                                        <div class="visible-md-block clearfix"></div>
                                        <div class="form-group col-md-4 col-lg-12">  
                                           <input type="button" onclick="javascript: document.getElementById('<%=btnGuardaPago.ClientID%>').click();" class="btn btn-sm btn-primary" value="Guardar Pago" />
                                        </div>
                                        <asp:Panel ID="pnlPagoExito" class="col-sm-12" Visible="false" runat="server">                                
                                            <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                <i class="far fa-check-circle"></i>
                                                <asp:Label ID="lblPagoMens" runat="server"></asp:Label>
                                            </div>
                                        </asp:Panel>                                        
                                    </div>  
                                </div> 
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>                                                                  
                    </ajaxtoolKit:TabContainer>
                    <div style="visibility:hidden">
                        <asp:Button ID="btnBusqueda" ValidationGroup="BusquedaVal" OnClick="btnBusqueda_Click" runat="server" />
                        <asp:Button ID="btnGuardaPago" ValidationGroup="GuardaPagoVal" OnClick="btnGuardaPago_Click" runat="server" />
                        <asp:Button ID="btnCargaArchivoPagos" ValidationGroup="CargaArchivoVal" OnClick="btnCargaArchivoPagos_Click" runat="server" />
                        <asp:Button ID="btnGuardaIdSolicitud" ValidationGroup="GuardaSolicitudVal" OnClick="btnGuardaIdSolicitud_Click" runat="server" />
                        <asp:Button ID="btnCancelaGuardaIdSolicitud"  OnClick="btnCancelaGuardaIdSolicitud_Click" runat="server" />
                        <%--<asp:Button ID="CargarResp" ValidationGroup="CargarResp" OnClick="btnCargarRespuesta_Click" runat="server" />--%>
                        
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnBusqueda" />
                    <asp:PostBackTrigger ControlID="btnGuardaPago" />
                    <asp:PostBackTrigger ControlID="btnCargaArchivoPagos" />
                    <asp:PostBackTrigger ControlID="btnGuardaIdSolicitud" />
                    <asp:PostBackTrigger ControlID="btnCancelaGuardaIdSolicitud" />
                    <%--<asp:PostBackTrigger ControlID="CargarResp" />--%>
                </Triggers>           
            </asp:UpdatePanel>                                               
        </div>
    </div>
</asp:Content>

