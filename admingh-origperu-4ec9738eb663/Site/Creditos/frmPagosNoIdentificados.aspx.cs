﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Ionic;
using Ionic.Zlib;
using Ionic.Zip;
using System.ComponentModel;
using SpreadsheetLight;

public partial class Site_Creditos_frmPagosNoIdentificados : System.Web.UI.Page
{
    byte[] contenido = null;
    string nombreArchivo = string.Empty;
    protected string NombreArchivo
    {
        get
        {
            return (ViewState[this.UniqueID + "_NombreArchivo"] != null) ? ViewState[this.UniqueID + "_NombreArchivo"].ToString() : string.Empty;
        }
        set
        {
            ViewState[this.UniqueID + "_NombreArchivo"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // Ocultamos los paneles de detalle de inicio
            //pnlAprobadosDetalle.Visible = false;
            //pnlErrorDetalle.Visible = false;
            List<wsSOPF.CanalPago> canalesPago = new List<wsSOPF.CanalPago>();
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                canalesPago = ws.CanalPago_ListarActivos().ToList();
                
                ddlCanalPago.DataSource = canalesPago;
                ddlCanalPago.DataTextField = "Nombre";
                ddlCanalPago.DataValueField = "IdCanalpago";


                ddlCanalPago.Items.Insert(0, new ListItem("<Seleccione un Item>", "0"));

                // Bind the data to the control.
                ddlCanalPago.DataBind();
            }

        }
    }


   
    #region "Domiciliacion BBVA"

    protected void btnBusqueda_Click(object sender, EventArgs e)
    {
        pnlError_BuscarPago.Visible = false;

        try
        {
            buscarPagos();
        }
        catch (Exception ex)
        {
            pnlCarga_BuscarPagos.Visible = true;
            lblError_BuscarPago.Text = ex.Message;
            pnlError_BuscarPago.Visible = true;
        }


    }

    private void buscarPagos()
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
        {

            DateTime? fchCreditoDesde = null;
            DateTime? fchCreditoHasta = null;

            if (!string.IsNullOrEmpty(txtFechaReciboDesde.Text.Trim()))
                fchCreditoDesde = DateTime.ParseExact(txtFechaReciboDesde.Text.Trim(), "dd/MM/yyyy", null);

            if (!string.IsNullOrEmpty(txtFechaReciboHasta.Text.Trim()))
                fchCreditoHasta = DateTime.ParseExact(txtFechaReciboHasta.Text.Trim(), "dd/MM/yyyy", null);
            
            List<wsSOPF.PagosNoIdentificados> pagos = ws.ObtenerPagosNoIdentificados(fchCreditoDesde, fchCreditoHasta).ToList();
            gvPagosNoIdentificados.DataSource = pagos;
            gvPagosNoIdentificados.DataBind();
            
            pnlCarga_BuscarPagos.Visible = true;
            pnlGuardaSolicitud.Visible = false;

        }
    }

    protected void btnCargaArchivoPagos_Click(object sender, EventArgs e)
    {
       
        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                //// Validar antes de cargar el archivo
                if (!archPagos.HasFile)
                {
                    lblErrorSubirArchivopagos.Visible = true;
                }
                else
               
                {
                    // Guardar el archivo a la carpeta local de manera TEMPORAL (manejo en memoria)
                    string folderPath = Server.MapPath("~/Archivos/PagosDirectos/");
                    archPagos.SaveAs(folderPath + "TMP_" + Path.GetFileName(archPagos.FileName));
                    this.NombreArchivo = archPagos.FileName.Trim();
                    
                    ProcesarXLS();


                    pnlCarga_CargarArchivoPagos.Visible = false;
                    pnlResumen_CargarArchivo.Visible = true;

                }

            }
        }
        catch (Exception ex)
        {
            pnlCarga_CargarArchivoPagos.Visible = false;
            pnlResumen_CargarArchivo.Visible = true;

            pnlAdvertencia_CargarArchivo.Visible = true;
            lblAdvertencia_CargarArchivo.Text = ex.Message;
        }


    }

    protected void btnGuardaArchivoPagos_Click(object sender, EventArgs e)
    {
        lblErrorSubirArchivopagos.Visible = false;
        int idUsuario = 0;
        try
        {

            string filePath = "";

            string folderPath = Server.MapPath("~/Archivos/PagosDirectos/");
            filePath = folderPath + "TMP_" + Path.GetFileName(this.NombreArchivo);

            byte[] bytes = File.ReadAllBytes(filePath);
            
            if (bytes.Length > 0)
            {
                contenido = bytes;
                nombreArchivo = this.NombreArchivo;
            }
            if (Session["UsuarioId"] != null)
            {
                int.TryParse(Session["UsuarioId"].ToString(), out idUsuario);
            }

            wsSOPF.GeneraPagoMavisoResponse respPagoMasivo = new wsSOPF.GeneraPagoMavisoResponse();
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                respPagoMasivo = ws.GenPagosNoIdentificadosMasivo(new wsSOPF.GeneraPagoMavisoRequest
                {
                    IdUsuario = idUsuario,
                    NombreArchivo = nombreArchivo,
                    ContenidoArchivo = contenido
                });
            }
            if (respPagoMasivo != null)
            {
                if (!respPagoMasivo.Error)
                {
                    //Response.Redirect(Request.RawUrl);
                    pnlExito_ArchivosCargados.Visible = true;
                    //lblExito_ArchivosCargados.Visible = true;
                    pnlResumen_CargarArchivo.Visible = false;
                    pnlCarga_CargarArchivoPagos.Visible = true;
                }
            }
        }
        catch (Exception er)
        {
            pnlErrorGuardarArchivo.Visible = true;
            lblErrorGuardarArchivo.Text = er.Message;
        }
    }

    private void ProcesarXLS()
    {
        lbtnGuardarArchivoPagosDirectos_CargarArchivo.Enabled = true;

        string folderPath = Server.MapPath("~/Archivos/PagosDirectos/");
        List<PagosDirectosExcelResumen> PagosCSV = new List<PagosDirectosExcelResumen>();
        PagosDirectosExcelResumen pagoCSV = null;

        string path = folderPath + "TMP_" + this.NombreArchivo;
        SLDocument sl = new SLDocument(path);

        string errLines = string.Empty;
        int lineNum = 0;
        int IdSolicitud = 0;
        decimal Monto = 0;
        DateTime FechaPago;

        // Obtener los canales de Pago Activos para validar los pagos Directos antes de guardarlos.
        List<wsSOPF.CanalPago> canalesPago = new List<wsSOPF.CanalPago>();
        using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
        {
            canalesPago = ws.CanalPago_ListarActivos().ToList();
        }

         int iRow = 2;
         while (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 1)))
         {
             try
             {
           
                 pagoCSV = new PagosDirectosExcelResumen();
                string fec = sl.GetCellValueAsString(iRow, 3);
                 // Validar los tipos de dato antes de agregarlos como renglon Valido
                 if (
                     !decimal.TryParse(sl.GetCellValueAsString(iRow, 2), out Monto) ||
                     !DateTime.TryParseExact(sl.GetCellValueAsString(iRow, 3), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out FechaPago) ||
                     !canalesPago.Any(s => s.Nombre.ToUpper() == sl.GetCellValueAsString(iRow, 1)))
                     throw new Exception(iRow.ToString());

                 pagoCSV.NoPago = iRow;
                 pagoCSV.TipoPago = sl.GetCellValueAsString(iRow, 1);
                 pagoCSV.Monto = decimal.Parse(sl.GetCellValueAsString(iRow, 2));
                 pagoCSV.FechaPago = DateTime.ParseExact(sl.GetCellValueAsString(iRow, 3), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                 pagoCSV.Observaciones = sl.GetCellValueAsString(iRow, 4);

                 PagosCSV.Add(pagoCSV);
             
             }
             catch (Exception ex)
             {
                 errLines += ex.Message + ", ";
             }

             iRow++;
         }

         // Mostrar el Resumen del archivo CSV Procesado
         gvResumen_CargarArchivo.DataSource = PagosCSV;
         gvResumen_CargarArchivo.DataBind();

         if (!string.IsNullOrEmpty(errLines))
         {
             lbtnGuardarArchivoPagosDirectos_CargarArchivo.Enabled = false;

             pnlAdvertencia_CargarArchivo.Visible = true;
             lblAdvertencia_CargarArchivo.Text = string.Format("Las siguientes pagos presentan formato incorrecto en su información o el canal de pago no existe:<br/>{0}", errLines.Remove(errLines.LastIndexOf(","), 1).Trim());
         }
 
    }


    protected void btnGuardaPago_Click(object sender, EventArgs e)
    {
        
        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                DateTime? fchPago = null;
                int IdCanalPago = 0;
                decimal Monto = 0;
                int IdUsuario = 0;



                if (!string.IsNullOrEmpty(txtFechaPago.Text.Trim()))
                    fchPago = DateTime.ParseExact(txtFechaPago.Text.Trim(), "dd/MM/yyyy", null);

                if (!string.IsNullOrEmpty(ddlCanalPago.SelectedValue))
                    IdCanalPago = Convert.ToInt32(ddlCanalPago.SelectedValue);

                if (!string.IsNullOrEmpty(txtMonto.Text.Trim()))
                    Monto = Convert.ToDecimal(txtMonto.Text.Trim());
                if (Session["UsuarioId"] != null)
                {
                    int.TryParse(Session["UsuarioId"].ToString(), out IdUsuario);
                }

                wsSOPF.PagosNoIdentificados pagos = new wsSOPF.PagosNoIdentificados();

                pagos.Fch_Pago = fchPago;
                pagos.IdCanalPAgo = IdCanalPago;
                pagos.Monto = Monto;
                pagos.IdUsuario = IdUsuario;
                pagos.Observaciones = txtObserva.Text;

                wsSOPF.ResultadoOfboolean res = ws.GuardaPagoNoIdentificadoIndiv(pagos);
                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);
                else
                {
                    pnlPagoExito.Visible = true;
                    lblPagoMens.Text = res.Mensaje;
                }
                   

            }

            ddlCanalPago.SelectedIndex = 0;
            txtFechaPago.Text = "";
            txtMonto.Text = "";
            txtObserva.Text = "";

        }
        catch (Exception er)
        {
        }
    }

    protected void btnGuardaIdSolicitud_Click(object sender, EventArgs e)
    {
        

        try
        {
            using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
            {
                int idPagoNoident = 0;
                int idSolicitud = 0;

                idPagoNoident = Convert.ToInt32(lblIdPagoNoIdentificado.Value);
                idSolicitud = Convert.ToInt32(txtIdSolicitud.Text.Trim());

                wsSOPF.PagosNoIdentificados pagos = new wsSOPF.PagosNoIdentificados();


                pagos.IdPagoNoIdentificado = idPagoNoident;
                pagos.IdSolicitud = idSolicitud;

                wsSOPF.ResultadoOfboolean res = ws.ActuPagoNoIdentificadoIndiv(pagos);
                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);
                else
                {
                    pnlGuardaSoliExito.Visible = true;
                    lblGuardaSoliExito.Text = res.Mensaje;
                    pnlErrorBusqueda.Visible = false;
                }

                buscarPagos();

                lblIdPagoNoIdentificado.Value = "";
                lblMonto.Text = "";
                lblFechaPago.Text = "";
                txtIdSolicitud.Text = "";
                lblCanalPago.Text = "";
                pnlGuardaSolicitud.Visible = false;
                pnlCarga_BuscarPagos.Visible = true;
            }
        }
        catch (Exception ex)
        {
            pnlErrorBusqueda.Visible = true;
            lblErrorBusqueda.Text = ex.Message;
        }

        
    }

    protected void btnCancelaGuardaIdSolicitud_Click(object sender, EventArgs e)
    {
        lblIdPagoNoIdentificado.Value = "";
        lblMonto.Text = "";
        lblFechaPago.Text = "";
        txtIdSolicitud.Text = "";
        lblCanalPago.Text = "";
        pnlGuardaSolicitud.Visible = false;
        pnlCarga_BuscarPagos.Visible = true;
        pnlErrorBusqueda.Visible = false;
    }

    protected void CustomersGridView_SelectedIndexChanged(Object sender, EventArgs e)
    {
        GridViewRow row = gvPagosNoIdentificados.SelectedRow;

        lblIdPagoNoIdentificado.Value = row.Cells[1].Text;
        lblMonto.Text = row.Cells[3].Text;
        lblFechaPago.Text = row.Cells[4].Text;
        lblCanalPago.Text = row.Cells[2].Text;

        pnlGuardaSolicitud.Visible = true;
        pnlCarga_BuscarPagos.Visible = false;
    }

    

    protected void tcPagosNoIndentificados_ActiveTabChanged(object sender, EventArgs e)
    {
        switch (tcPagosNoIndentificados.ActiveTab.HeaderText.Trim())
        {
            case "Buscar":
                pnlError_BuscarPago.Visible = false;
                pnlPagoExito.Visible = false;
                lblErrorSubirArchivopagos.Visible = false;
                pnlExito_ArchivosCargados.Visible = false;
                break;
            case "Carga Masiva":
                pnlGuardaSoliExito.Visible = false;
                //pnlExito_ArchivosCargados.Visible = false;
                pnlPagoExito.Visible = false;
                pnlErrorBusqueda.Visible = false;

                break;
            case "Carga Individual":
                pnlGuardaSoliExito.Visible = false;
                lblErrorSubirArchivopagos.Visible = false;
                pnlExito_ArchivosCargados.Visible = false;
                pnlErrorBusqueda.Visible = false;
                break;
        }
    }

   

    protected void gvPagosNoIdentificados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gvPagosNoIdentificados.PageIndex = e.NewPageIndex;
        //CargarArchivosAprobados();

        buscarPagos();
    }

   

    #endregion
    
    private class PagosDirectosExcelResumen
    {
        public int NoPago { get; set; }
        public string TipoPago { get; set; }
        public int IdSolicitud { get; set; }
        public decimal Monto { get; set; }
        public DateTime FechaPago { get; set; }
        public string Observaciones { get; set; }
    }
}