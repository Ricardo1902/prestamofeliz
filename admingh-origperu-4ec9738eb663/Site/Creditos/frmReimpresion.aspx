﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmReimpresion.aspx.cs" Inherits="Creditos_frmReimpresion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    




        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">REIMPRESION DE FORMATOS</h3>
            </div>
            <div class="panel-body col-xs-12 col-md-12">
                <div class="form-group">
                    <div class="form-group row">
                        <div>

                            <br />
                            <br />
                            <br />
                            <br />
                            <br />

                            <asp:Label ID="lblFormato" runat="server" Text="Seleccione Formato"></asp:Label>&nbsp&nbsp
            <asp:DropDownList ID="cmbFormato" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cmbFormato_OnSelectedIndexChanged "></asp:DropDownList>
                            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp&nbsp
            <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOK_Click" Visible="False" />

                            <br />
                            <br />
                            <br />

                            <asp:Label ID="lblRango" runat="server" Text="Rango Folios (Obligatorio)"></asp:Label>
                            <asp:TextBox ID="txtCanIni" runat="server" Width="50px"></asp:TextBox>
                            <asp:TextBox ID="txtCanFin" runat="server" Width="50px"></asp:TextBox>

                            <br />

                            <asp:Label ID="lblComentario" runat="server" Text="Comentario(Obligatorio)"></asp:Label>
                            <asp:TextBox ID="txtComentario" runat="server" Width="350px"></asp:TextBox>

                            &nbsp&nbsp&nbsp
            <asp:Button ID="btnAgregar" runat="server" Text="+" OnClick="btnAgregar_Click" />
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <asp:Button ID="btnActualizar" runat="server" Text="Revisar Impresiones Pendientes" OnClick="btnActualizar_Click" Font-Bold="true" />

                        </div>
                    </div>

                    <div class="center">
                        <asp:GridView runat="server" ID="grdReImpresiones" DataKeyNames="IdReImp" Width="95%"
                            AutoGenerateColumns="False" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow"
                            AlternatingRowStyle-CssClass="GridRowAlternate" CssClass="Grid" EmptyDataRowStyle-CssClass="GridEmptyData"
                            GridLines="None" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false" OnRowCommand="grdReImpresiones_RowCommand">

                            <Columns>

                                <asp:BoundField HeaderText="Folio" DataField="IdReImp" />

                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnReImp" runat="server" Value='<%# Eval("IdReImp") %>' />
                                        <asp:HiddenField ID="hdnFolIni" runat="server" Value='<%# Eval("FolioInicial") %>' />
                                        <asp:HiddenField ID="hdnFolFin" runat="server" Value='<%# Eval("FolioFinal") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField HeaderText="Formato" DataField="Formato" />
                                <asp:BoundField HeaderText="FolioInicial" DataField="FolioInicial" />
                                <asp:BoundField HeaderText="FolioFinal" DataField="FolioFinal" />
                                <asp:BoundField HeaderText="Comentario" DataField="Comentario" />

                                <asp:TemplateField HeaderText="ACCION">
                                    <ItemStyle Width="10px" />
                                    <ItemTemplate>
                                        <asp:Button ID="btnReImprimir" runat="server" CommandName="reimprimir"
                                            CommandArgument="<%# ((GridViewRow)Container).RowIndex %>"
                                            Text="ReImprimir" Height="20" Width="70"></asp:Button>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- <asp:TemplateField HeaderText="">
                        <ItemStyle Width ="10px"/>
                        <ItemTemplate>
                            <asp:Button ID="btnEliminar" runat="server"  CommandName="eliminar"  
                                        CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" 
                                        Text="Eliminar" Height="20" Width="70"></asp:Button>
                            
                        </ItemTemplate>
                    </asp:TemplateField> --%>
                            </Columns>

                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>

                    </div>
                </div>
            </div>

</asp:Content>


