﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using wsSOPF;

using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text.RegularExpressions;
using System.Windows.Forms;


public partial class Creditos_frmReimpresion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = "";

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        if (!IsPostBack)
        {
            List<TBCATFormatos> ds = new List<TBCATFormatos>();

            TBCATFormatos[] Array = ds.ToArray();

            using (wsSOPF.CatalogoClient wsFormato = new CatalogoClient())
            {

                Array = wsFormato.ConsultaFormatos(0, 99, "", 0, idSucursal);

                cmbFormato.Items.Clear();
                cmbFormato.DataTextField = "Formato";
                cmbFormato.DataValueField = "IdFormato";
                cmbFormato.DataSource = Array;

                cmbFormato.DataBind();

            }

        }

    }

    protected void cmbFormato_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        lblError.Text = "";

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        //Consulta de reimpresiones
        ReimpresionFormatos[] Agregados = null;
        using (wsSOPF.CreditoClient wsAgregados = new CreditoClient())
        {
            Agregados = wsAgregados.ConsultarReimpresion(0, 0, DateTime.Now, idSucursal, idUsu, 0, 0, 1, Convert.ToInt32(cmbFormato.SelectedValue));
            grdReImpresiones.DataSource = Agregados;
            grdReImpresiones.DataBind();
        }

        txtCanIni.Text = "";
        txtCanFin.Text = "";
        txtComentario.Text = "";

    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        lblError.Text = "";

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        // Validacion de texto que solo sean numeros 
        String expValEntero;
        expValEntero = "[1234567890]";

        if (Regex.Replace(txtCanIni.Text, expValEntero, String.Empty).Length != 0)
        {
            lblError.Text = "ERROR: Solo puede capturar numeros";
            btnOk.Visible = true;
            //MessageBox.Show("Solo puede capturar numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (Regex.Replace(txtCanFin.Text, expValEntero, String.Empty).Length != 0)
        {
            lblError.Text = "ERROR: Solo puede capturar numeros";
            btnOk.Visible = true;
            //MessageBox.Show("Solo puede capturar numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (txtCanIni.Text.Trim() == "")
        {
            lblError.Text = "ERROR: Debe de capturar un numero";
            btnOk.Visible = true;
            //MessageBox.Show("Debe de capturar un numero", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (txtCanFin.Text.Trim() == "")
        {
            lblError.Text = "ERROR: Debe de capturar un numero";
            btnOk.Visible = true;
            //MessageBox.Show("Debe de capturar un numero", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (txtCanIni.Text.Trim() == "0")
        {
            lblError.Text = "ERROR: La cantidad no puede ser cero";
            btnOk.Visible = true;
            //MessageBox.Show("La cantidad no puede ser cero", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (txtCanFin.Text.Trim() == "0")
        {
            lblError.Text = "ERROR: La cantidad no puede ser cero";
            btnOk.Visible = true;
            //MessageBox.Show("La cantidad no puede ser cero", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (txtComentario.Text.Trim() == "")
        {
            lblError.Text = "ERROR: El comentario es obligatorio";
            btnOk.Visible = true;
            //MessageBox.Show("El comentario es obligatorio", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        //Sacamos la consulta del minimo y maximo rango que se ha generado en los lotes
        LotesImpresion[] Rangos = null;
        using (wsSOPF.CreditoClient wsDetallePen = new CreditoClient())
        {
            Rangos = wsDetallePen.ConsultarLotesImpresion(3, 0, DateTime.Now, idSucursal, idUsu, 0, 0, 1, Convert.ToInt32(cmbFormato.SelectedValue));
        }

        int FolioMax;
        int FolioMin;

        FolioMin = Convert.ToInt32(Rangos[0].FolioMin);
        FolioMax = Convert.ToInt32(Rangos[0].FolioMax);

        // Insertamos el lote a imprimir con estatus 1 - Alta de Impresion
        int FolioInicial;
        int FolioFinal;

        FolioInicial = Convert.ToInt32(txtCanIni.Text);
        FolioFinal = Convert.ToInt32(txtCanFin.Text);

        if (FolioInicial > FolioFinal)
        {
            lblError.Text = "ERROR: El Folio Inicial no puede ser mayor al folio final";
            btnOk.Visible = true;
            //MessageBox.Show("El Folio Inicial no puede ser mayor al folio final", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if ((FolioInicial < FolioMin) || (FolioInicial > FolioMax) || (FolioFinal < FolioMin) || (FolioFinal > FolioMax))
        {
            lblError.Text = "ERROR: Alguno de los folios no puede reimprimirse debido a que no se ha generado o confirmado su lote, favor de revisar";
            btnOk.Visible = true;
            //MessageBox.Show("Alguno de los folios no puede reimprimirse debido a que no se ha generado o confirmado su lote, favor de revisar", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            txtCanIni.Text = "";
            txtCanFin.Text = "";
            txtComentario.Text = "";
            return;
        }

        wsSOPF.CreditoClient wsAgregar = new CreditoClient();
        wsAgregar.InsertarReimpresion(0, DateTime.Now, idUsu, Convert.ToInt32(cmbFormato.SelectedValue), idSucursal, FolioInicial, FolioFinal, 0, Convert.ToString(txtComentario.Text.Trim()), 1);

        //Consulta de reimpresiones
        ReimpresionFormatos[] Agregados = null;
        using (wsSOPF.CreditoClient wsAgregados = new CreditoClient())
        {
            Agregados = wsAgregados.ConsultarReimpresion(0, 0, DateTime.Now, idSucursal, idUsu, 0, 0, 1, Convert.ToInt32(cmbFormato.SelectedValue));
            grdReImpresiones.DataSource = Agregados;
            grdReImpresiones.DataBind();
        }

        txtCanIni.Text = "";
        txtCanFin.Text = "";
        txtComentario.Text = "";

    }


    protected void grdReImpresiones_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "reimprimir")
        {
            System.Web.UI.WebControls.Button comando = e.CommandSource as System.Web.UI.WebControls.Button;
            GridViewRow registro = comando.Parent.Parent as GridViewRow;

            HiddenField hdnReImp = registro.FindControl("hdnReImp") as HiddenField;
            HiddenField hdnFolIni = registro.FindControl("hdnFolIni") as HiddenField;
            HiddenField hdnFolFin = registro.FindControl("hdnFolFin") as HiddenField;

            int idSucursal;
            idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

            int idUsu;
            idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

            int _hdnReImp;
            int _hdnFolIni;
            int _hdnFolFin;
            
            _hdnReImp = 0;
            _hdnFolIni = 0;
            _hdnFolFin = 0;

            _hdnReImp = Convert.ToInt32(hdnReImp.Value);
            _hdnFolIni = Convert.ToInt32(hdnFolIni.Value);
            _hdnFolFin = Convert.ToInt32(hdnFolFin.Value);

            //Mandamos imprimir

            TBCATSucursal Suc = null;
            TBCATSucursal[] Sucursal = null;

            using (wsSOPF.UsuarioClient wsSuc = new UsuarioClient())
            {
                Suc = new TBCATSucursal();
                Suc.Accion = 1;
                Suc.IdSucursal = idSucursal;
                Suc.Sucursal = "";
                Suc.IdEstatus = 0;
                Suc.IdAfiliado = 0;

                Sucursal = wsSuc.ObtenerSucursal(Suc);

            }

            string cveSucurs;
            cveSucurs = Sucursal[0].cve_sucursal;

            // Consultamos los datos del formato para sacar plantilla
            TBCATFormatos[] Datos = null;
            using (wsSOPF.CatalogoClient wsDatosFormato = new CatalogoClient())
            {
                Datos = wsDatosFormato.ConsultaFormatos(1, Convert.ToInt32(cmbFormato.SelectedValue), "", 0, idSucursal);
            }

            string Plantilla;
            Plantilla = ("PlantillasPDF/" + Datos[0].NombrePlantilla).ToString() + ".pdf";

            // Mandamos imprimir
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Formulario.pdf");
            //FillPDF(Server.MapPath("Plantilla.pdf"), Response.OutputStream);
            byte[] outputs = null;

            Document document = new Document();
            PdfCopy copy = new PdfCopy(document, Response.OutputStream);
            document.Open();
            for (int j = _hdnFolIni; j <= _hdnFolFin; j++)
            {

                outputs = GeneratePDF(Server.MapPath(Plantilla), cveSucurs + " N°" + Convert.ToString(j));
                PdfReader reader = new PdfReader(outputs);

                for (int pageCounter = 1; pageCounter < reader.NumberOfPages + 1; pageCounter++)
                {
                    //byte[] page = reader.GetPageContent(pageCounter);
                    copy.AddPage(copy.GetImportedPage(reader, pageCounter));
                }
                reader.Close();
            }

            document.Close();

            //Actualizamos el estatus de la impresión
            wsSOPF.CreditoClient wsEstatusReimp = new CreditoClient();
            wsEstatusReimp.ActualizarReimpresion(2, DateTime.Now, idUsu, Convert.ToInt32(cmbFormato.SelectedValue), idSucursal, 0, 0, _hdnReImp, "", 2);

            /*//Consulta de reimpresiones
            ReimpresionFormatos[] Confirmados = null;
            using (wsSOPF.CreditoClient wsConfirmados = new CreditoClient())
            {
                Confirmados = wsConfirmados.ConsultarReimpresion(0, 0, DateTime.Now, idSucursal, idUsu, 0, 0, 1, Convert.ToInt32(cmbFormato.SelectedValue));
                grdReImpresiones.DataSource = Confirmados;
                grdReImpresiones.DataBind();
            }*/

        }

        if (e.CommandName == "eliminar")
        {
            lblError.Text = "";

            System.Web.UI.WebControls.Button comando = e.CommandSource as System.Web.UI.WebControls.Button;
            GridViewRow registro = comando.Parent.Parent as GridViewRow;

            HiddenField hdnReImp = registro.FindControl("hdnReImp") as HiddenField;

            int idSucursal;
            idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

            int idUsu;
            idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

            int _hdnReImp;
            _hdnReImp = 0;

            _hdnReImp = Convert.ToInt32(hdnReImp.Value);

            //Actualizamos el estatus de la impresión
            wsSOPF.CreditoClient wsEstatusReimp = new CreditoClient();
            wsEstatusReimp.ActualizarReimpresion(1, DateTime.Now, idUsu, Convert.ToInt32(cmbFormato.SelectedValue), idSucursal, 0, 0, _hdnReImp, "", 2);

            //Consulta de reimpresiones
            ReimpresionFormatos[] Confirmados = null;
            using (wsSOPF.CreditoClient wsConfirmados = new CreditoClient())
            {
                Confirmados = wsConfirmados.ConsultarReimpresion(0, 0, DateTime.Now, idSucursal, idUsu, 0, 0, 1, Convert.ToInt32(cmbFormato.SelectedValue));
                grdReImpresiones.DataSource = Confirmados;
                grdReImpresiones.DataBind();
            }

        }

        txtCanIni.Text = "";
        txtCanFin.Text = "";
        txtComentario.Text = "";
        lblError.Text = "";

    }

    public static byte[] GeneratePDF(string pdfPath, string Folio)
    {
        var output = new MemoryStream();
        var reader = new PdfReader(pdfPath);
        var stamper = new PdfStamper(reader, output);
        var formFields = stamper.AcroFields;

        stamper.AcroFields.SetField("Folio", Folio.ToString());
        stamper.FormFlattening = true;
        stamper.Close();
        reader.Close();

        return output.ToArray();
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        // Sacamos la clave de la sucursal
        lblError.Text = "";

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        //Consulta de reimpresiones
        ReimpresionFormatos[] Confirmados = null;
        using (wsSOPF.CreditoClient wsConfirmados = new CreditoClient())
        {
            Confirmados = wsConfirmados.ConsultarReimpresion(0, 0, DateTime.Now, idSucursal, idUsu, 0, 0, 1, Convert.ToInt32(cmbFormato.SelectedValue));
            grdReImpresiones.DataSource = Confirmados;
            grdReImpresiones.DataBind();
        }

        txtCanIni.Text = "";
        txtCanFin.Text = "";
        txtComentario.Text = "";
        lblError.Text = "";

    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Sacamos la clave de la sucursal
        lblError.Text = "";

        int idSucursal;
        idSucursal = Convert.ToInt32(Session["cve_sucurs"]);

        int idUsu;
        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        //Consulta de reimpresiones
        ReimpresionFormatos[] Confirmados = null;
        using (wsSOPF.CreditoClient wsConfirmados = new CreditoClient())
        {
            Confirmados = wsConfirmados.ConsultarReimpresion(0, 0, DateTime.Now, idSucursal, idUsu, 0, 0, 1, Convert.ToInt32(cmbFormato.SelectedValue));
            grdReImpresiones.DataSource = Confirmados;
            grdReImpresiones.DataBind();
        }

        txtCanIni.Text = "";
        txtCanFin.Text = "";
        txtComentario.Text = "";
        lblError.Text = "";
        
        btnOk.Visible = false;

    }
}

    
