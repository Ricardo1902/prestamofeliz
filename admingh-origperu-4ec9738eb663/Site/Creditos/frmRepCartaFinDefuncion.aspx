﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmRepCartaFinDefuncion.aspx.cs" Inherits="Creditos_frmRepCartaFinDefuncion" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="frmRepCartaFinDefuncion" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    </div>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server">
            <LocalReport ReportPath="site\ReportesEstructura\rptCartaFinDefuncion.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dtsCartaFiniquito_dtLiquidaciones" />
            </DataSources>
        </LocalReport>
        </rsweb:ReportViewer>
        <div>
        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
    </div>
    </form>
</body>
</html>
