﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using wsSOPF;
using Microsoft.Reporting.WebForms;
using System.IO;

public partial class Creditos_frmRepCartaFinNoAdeudo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int idcuenta = 0;
        int idUsu = 0;

        idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

        idcuenta = Convert.ToInt32(Request.QueryString["idsolicitud"]);
        DataSet dsFiniquitos = new DataSet();

        try
        {
            using (wsSOPF.CreditoClient wsCredito = new CreditoClient())
            {
                dsFiniquitos = wsCredito.ObtenerTBCreditosFiniquitos(2, idcuenta, idUsu);

            }

            ReportDataSource datsource1 = new ReportDataSource("dsCartaFiniquito", dsFiniquitos.Tables[0]);
            ReportViewer1.Visible = true;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(datsource1);

            ReportViewer1.LocalReport.Refresh();


            Warning[] warnings;
            String[] streamids;
            String mimeType;
            String encoding;
            String extension;


            byte[] bytes;
            bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);

            //FileStream fs = new FileStream(@"C:\AfiliacionBanorte_SOPF\output.pdf",
            //    FileMode.Create);
            //fs.Write(bytes, 0, bytes.Length);
            //fs.Close();

            System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes);
            Response.ContentType = mimeType;
            Response.Flush();
            Response.BinaryWrite(ms.ToArray());

        }
        catch (Exception ex)
        {
            /*this.lblerror.Text = ex.Message.ToString();*/
        }
    }
}