﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmRepCreditosSucursales.aspx.cs" Inherits="Creditos_frmRepCreditosSucursales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">REPORTE DE CREDITOS</h3>
            </div>
            <div class="panel-body col-xs-12 col-md-12">
                <div class="form-group">
                    <div class="form-group row">
                        <div>
                            <asp:Label ID="Label27" runat="server" Text="Fecha Inicial:" CssClass="Etiqueta" class="col-xs-12 col-md-12 form-control-label "></asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="txtFechaIni" runat="server"></asp:TextBox>
                            <asp:Image ID="Image1" runat="server" ImageUrl="../Images/calendario.png" Height="20px" />
                            <ajaxToolkit:CalendarExtender CssClass="cal_Theme1" ID="CalendarExtender1" runat="server"
                                Format="dd/MM/yyyy" PopupButtonID="Image1" PopupPosition="Right" TargetControlID="txtFechaIni">
                            </ajaxToolkit:CalendarExtender>
                        </div>
                        <div>
                            <asp:Label ID="lblFecha" runat="server" Text="Fecha Final:" CssClass="Etiqueta" class="col-xs-12 col-md-12 form-control-label "></asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="txtFechaFin" runat="server"></asp:TextBox>
                            <asp:Image ID="Image2" runat="server" ImageUrl="../Images/calendario.png" Height="20px" />
                            <ajaxToolkit:CalendarExtender CssClass="cal_Theme1" ID="CalendarExtender2" runat="server"
                                Format="dd/MM/yyyy" PopupButtonID="Image2" PopupPosition="Right" TargetControlID="txtFechaFin">
                            </ajaxToolkit:CalendarExtender>
                        </div>
                        <div>
                            <asp:Label ID="Label1" runat="server" Text="Promotor:" CssClass="Etiqueta" class="col-xs-12 col-md-12 form-control-label "></asp:Label>
                        </div>
                        <div>
                            <asp:DropDownList ID="cboPromotor" runat="server" Width="200px">
                            </asp:DropDownList>
                        </div>
                        <div>
                            <asp:Button ID="btnGenerarReporte" runat="server" Text="Generar" OnClick="btnGenerarReporte_Click" />
                        </div>
                        <div>

                                    <div class="table-responsive">
                                        <asp:GridView runat="server" ID="gv" DataKeyNames="IdCuenta" 
                                            AutoGenerateColumns="False" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow"
                                            AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData"
                                            GridLines="None" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false">

                                            <Columns>
                                                <asp:BoundField HeaderText="Cuenta" DataField="IdCuenta" />
                                                <asp:BoundField HeaderText="Solcitud" DataField="IdSolicitud" />
                                                <asp:BoundField HeaderText="Fecha" DataField="FchCredito" />
                                                <asp:BoundField HeaderText="Capital" DataField="Capital" />
                                                <asp:BoundField HeaderText="Interes" DataField="Interes" />
                                                <asp:BoundField HeaderText="Iva" DataField="IvaIntere" />
                                                <asp:BoundField HeaderText="Capaita D" DataField="capitalD" />
                                                <asp:BoundField HeaderText="Interes D" DataField="interesD" />
                                                <asp:BoundField HeaderText="Iva D" DataField="ivaIntereD" />
                                                <asp:BoundField HeaderText="Estatus" DataField="estatus" />

                                            </Columns>
                                            <EmptyDataTemplate>
                                                No se encontraron registros.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                               
                        </div>
                    </div>
        </div>
        </div>
            </div>

</asp:Content>



