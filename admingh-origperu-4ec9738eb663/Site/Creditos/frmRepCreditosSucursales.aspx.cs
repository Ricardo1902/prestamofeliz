﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;




public partial class Creditos_frmRepCreditosSucursales : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TBCATPromotor[] Promotores = null;
            using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
            {
                Promotores = wsCatalogo.ObtenerPromotores(1, Convert.ToInt32(Session["cve_sucurs"]));
                if (Promotores.Length > 0)
                {
                    cboPromotor.DataValueField = "IdPromotor";
                    cboPromotor.DataTextField = "Promotor";
                    cboPromotor.DataSource = Promotores;
                    cboPromotor.DataBind();
                }
            }
        }

    }
    protected void btnGenerarReporte_Click(object sender, EventArgs e)
    {
        DataResultCreditoUsp_SelCredito[] Creditos = null;
        using (wsSOPF.CreditoClient wsCredito = new CreditoClient())
        {
            Creditos = wsCredito.ObtenerCreditosFechas(Convert.ToDateTime(txtFechaIni.Text), Convert.ToDateTime(txtFechaFin.Text), 2, Convert.ToInt32(cboPromotor.SelectedValue));

            if (Creditos.Length > 0)
            {

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page pagina = new Page();
                HtmlForm form = new HtmlForm();
                GridView dg = new GridView();
                gv.EnableViewState = false;
                gv.DataSource = Creditos;
                gv.DataBind();
                pagina.EnableEventValidation = false;
                pagina.DesignerInitialize();
                pagina.Controls.Add(form);
                form.Controls.Add(gv);
                pagina.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=ReportedeCreditos.xls");
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Response.Write(sb.ToString());
                Response.End();




            }

        }

    }
    public DataTable ConvertToDataTable<T>(IList<T> data)
    {
        PropertyDescriptorCollection properties =
        TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();
        foreach (PropertyDescriptor prop in properties)
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        foreach (T item in data)
        {
            DataRow row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            table.Rows.Add(row);
        }
        return table;
    }
}