﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="frmSeguimientoCredtios.aspx.cs" Inherits="Creditos_frmSeguimientoCredtios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <style type="text/css">
        #Politicas
        {
            width: 735px;
            height: 500px;
        }
    </style>
    <script type="text/javascript">
        function SelectedIndexChange(ddl) {


            if (ddl.selectedIndex == 1) {
                document.getElementById('PorCuenta').style.display = "block";
                document.getElementById('PorCliente').style.display = "none";

            }
            else if (ddl.selectedIndex == 2) {
                document.getElementById('PorCuenta').style.display = "none";
                document.getElementById('PorCliente').style.display = "block";


            } else if (ddl.selectedIndex == 3) {

                document.getElementById('PorCuenta').style.display = "none";
                document.getElementById('PorCliente').style.display = "none";

            }
            else {
                document.getElementById('PorCuenta').style.display = "none";
                document.getElementById('PorCliente').style.display = "none";

            }


        }
    </script>

    
        



        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">SEGUIMIENTO CUENTAS</h3>
            </div>
            <div class="panel-body col-xs-12 col-md-12">
                <div class="form-group">
                    <div class="form-group row">
                        <div>
                            <asp:Label ID="Label1" Text="BUSQUEDA POR:" runat="server" CssClass="Etiqueta" class="col-xs-12 col-md-12 form-control-label "></asp:Label>
                        </div>
                        <div>
                            <asp:DropDownList ID="cboBusqueda" runat="server" onchange="SelectedIndexChange(this);" CssClass="btn btn-default btn-sm">
                                <asp:ListItem Value="-1" Text="Seleccione una opción..." />
                                <asp:ListItem Value="0" Text="Cuenta" />
                                <asp:ListItem Value="1" Text="Cliente" />

                            </asp:DropDownList>


                            <div style="display: none" id="PorCuenta">
                                <div>
                                    <asp:Label ID="Label2" runat="server" Text="Buscar Cuenta" CssClass="Etiqueta" class="col-xs-12 col-md-12 form-control-label "></asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtCuenta" runat="server"></asp:TextBox>
                                </div>
                                <div>
                                    <asp:Button ID="btnBuscarCuenta" runat="server" Text="Buscar" OnClick="btnBuscarCuenta_Click" />
                                </div>
                            </div>
                            <div style="display: none" id="PorCliente">
                                <div>
                                    <asp:Label ID="Label3" runat="server" Text="Buscar Cliente" CssClass="Etiqueta" class="col-xs-12 col-md-12 form-control-label "></asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtCliente" runat="server"></asp:TextBox>
                                </div>
                                <div>
                                    <asp:Button ID="btnCliente" runat="server" Text="Buscar" OnClick="btnCliente_Click" />
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
         <br />
        <br />
        <br />
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Listado de Creditos</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="form-group row">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBuscarCuenta" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnCliente" EventName="Click" />

                                <asp:PostBackTrigger ControlID="gvCuentas" />
                            </Triggers>
                            <ContentTemplate>
                                <div class="table-responsive">
                                    <asp:GridView runat="server" ID="gvCuentas" DataKeyNames="IdCuenta"
                                        Height="25%" AutoGenerateColumns="False" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow"
                                        AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-striped table table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData"
                                        GridLines="None" AllowPaging="true" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false"
                                        HeaderStyle-Wrap="false" OnPageIndexChanging="gvCuentas_PageIndexChanging" OnRowCommand="gvCuentas_RowCommand">

                                        <Columns>

                                            <asp:TemplateField HeaderText="Cuenta">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="btnDetalle" CommandName="editar" OnClientClick="aspnetForm.target ='_blank';"><%#Eval("IdSolicitud") %></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Credito" DataField="IdCuenta" />
                                            <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                            <asp:BoundField HeaderText="Sucursal" DataField="Sucursal" />
                                            <asp:BoundField HeaderText="Ciudad" DataField="Ciudad" />
                                            <asp:BoundField HeaderText="Estado" DataField="estado" />

                                        </Columns>
                                        <EmptyDataTemplate>
                                            No se encontraron registros.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <div class="progress">
                                    Cargando...
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>
            </div>
        </div>

    
</asp:Content>
