﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Creditos_frmSeguimientoCredtios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnBuscarCuenta_Click(object sender, EventArgs e)
    {
        int IdCuenta = 0;
        IdCuenta = Convert.ToInt32(txtCuenta.Text);

        List<TBAsignacion> Asiganciones = new List<TBAsignacion>();
        gvCuentas.DataSource = Asiganciones;
        gvCuentas.DataBind();


        using (wsSOPF.GestionClient wsGestion = new GestionClient())
        {
            DataResultUsp_ObtenerAsignacion[] Cuentas = null;
            
            
            Cuentas = wsGestion.ObtenerAsignacion(0, 4, IdCuenta, "", Convert.ToInt32(Session["cve_sucurs"]),"","");
            if (Cuentas.Length > 0)
            {
                gvCuentas.DataSource = Cuentas;
                gvCuentas.DataBind();

            }
        }
    }
    protected void btnCliente_Click(object sender, EventArgs e)
    {
        int IdCuenta = 0;

        List<TBAsignacion> Asiganciones = new List<TBAsignacion>();
        gvCuentas.DataSource = Asiganciones;
        gvCuentas.DataBind();


        using (wsSOPF.GestionClient wsGestion = new GestionClient())
        {
            DataResultUsp_ObtenerAsignacion[] Cuentas = null;
            
            Cuentas = wsGestion.ObtenerAsignacion(0, 5, IdCuenta, txtCliente.Text, Convert.ToInt32(Session["cve_sucurs"]),"","");
            if (Cuentas.Length > 0)
            {
                gvCuentas.DataSource = Cuentas;
                gvCuentas.DataBind();

            }
        }
    }
    protected void gvCuentas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            Session["idCuenta"] = null;
            if (e.CommandName == "editar")
            {
                int idCuenta = 0;

                LinkButton comando = e.CommandSource as LinkButton;
                GridViewRow registro = comando.Parent.Parent as GridViewRow;
                int.TryParse(gvCuentas.DataKeys[registro.RowIndex]["IdCuenta"].ToString(), out idCuenta);
                Session["idCuenta"] = idCuenta;

                //string ventana="frmGestiones.aspx?idcuenta=" + idCuenta;
                //string Clientscript = "<script>window.open('" +
                //              ventana +
                //              "')</script>";

                //if (!this.ClientScript.IsStartupScriptRegistered("WOpen"))
                //{
                //    this.ClientScript.RegisterStartupScript(typeof(string), "WOpen", Clientscript);
                //}
               

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("frmEdoCtav2.aspx?idcuenta=" + idCuenta, false);


            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('Error Inesperado!!')</script>");
        }
    }
    protected void gvCuentas_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        GridView gv = (GridView)sender;
        gv.PageIndex = e.NewPageIndex;
        DataResultUsp_ObtenerAsignacion[] asignaciones = null;
        using (wsSOPF.GestionClient wsGestiones = new GestionClient())
        {
            TBCatGestores[] Gestores = null;
            Gestores = wsGestiones.ObtenerGestor(3, 0, 0, Convert.ToInt32(Session["UsuarioId"]));

            if (cboBusqueda.SelectedIndex == 2)
            {
                asignaciones = wsGestiones.ObtenerAsignacion(Convert.ToInt32(Gestores[0].IdGestor), 3, 0, txtCliente.Text, Convert.ToInt32(Session["cve_sucurs"]),"","");
            }
            else
            {
                asignaciones = wsGestiones.ObtenerAsignacion(Convert.ToInt32(Gestores[0].IdGestor), 1, 0, "", Convert.ToInt32(Session["cve_sucurs"]),"","");
            }

            if (asignaciones.Length > 0)
            {
                gvCuentas.DataSource = asignaciones;
                gvCuentas.DataBind();
            }
        }


    }
}