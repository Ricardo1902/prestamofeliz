﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmSeguimientoDomiciliacionBBVA.aspx.cs" Inherits="Site_Creditos_frmSeguimientoDomiciliacionBBVA" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Archivos Domiciliacion Debito Net Cash</h3>
        </div>
        <div class="panel-body">
            <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
                <ProgressTemplate>                 
                    <div id="loader-background"></div>
                    <div id="loader-content"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upMain" runat="server">
                <ContentTemplate>
                    <ajaxtoolKit:TabContainer ID="tcCargaArchivos" CssClass="actTab" OnActiveTabChanged="tcCargaArchivos_ActiveTabChanged" AutoPostBack="true" runat="server">
                        <ajaxtoolKit:TabPanel HeaderText="Cargar Archivo" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnlCarga_CargarArchivo" runat="server">
                                    <div class="form-group col-sm-12">           
                                        <label>Formato e Instrucciones</label>
                                        <p>
                                            <ul>
                                                <li>
                                                    Seleccione el archivo separado por comas (.csv) para realizar la carga de Pagos Domiciliados BBVA, el nombre del archivo servira para identificar la carga que se
                                                    realiza, debe ser un nombre único de lo contrario el sistema lo marcara como duplicado.
                                                </li>
                                                <li>
                                                    La información de los Pagos Domiciliados BBVA debe presentar en el siguiente orden la información:<br />
                                                    <span class="small"><strong>SOLICITUD, TIPODOC, NOMBRECLIENTE,IMPORTE,CUENTA</span>
                                                </li>
                                                <li>
                                                    La primer linea del archivo se ignorara ya que deben ser los encabezados en el orden indicado
                                                </li>
                                            </ul>
                                        </p>
                                    </div>

                                    <div class="form-group col-sm-12"> 
                                        <label>Archivo a Cargar</label>                                          
                                        <asp:FileUpload ID="fuArchivo" CssClass="form-control" runat="server" />
                                        <asp:RegularExpressionValidator
                                            id="RegularExpressionValidator1"                                    
                                            ErrorMessage="El archivo debe ser formato CSV"
                                            ValidationExpression ="^.+(.csv|.CSV)$"
                                            ControlToValidate="fuArchivo" ValidationGroup="CargarArchivo" CssClass="small" ForeColor="Red" Display="Dynamic"
                                            runat="server"></asp:RegularExpressionValidator>
                                    </div>
           
                                    <div class="form-group col-sm-12">                                
                                        <input type="button" onclick="javascript: document.getElementById('<%=btnCargarArchivo.ClientID%>').click();" class="btn btn-sm btn-primary" value="Cargar Archivo" />
                                    </div>

                                    <asp:Panel ID="pnlError_CargarArchivo" class="col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <asp:Label ID="lblError_CargarArchivo" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>

                                <asp:Panel ID="pnlResumen_CargarArchivo" Visible="false" runat="server">                                  
                                    <div class="col-sm-12 form-group">
                                        <strong>Nombre del Archivo: </strong> <%= this.NombreArchivo %>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-12" style="width:auto; height:auto; overflow-x: auto;">                                        
                                        <asp:GridView ID="gvResumen_CargarArchivo"                                                                                 
                                            runat="server" Width="100%" AutoGenerateColumns="False"
                                            HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None"
                                            RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            >                                        
                                            <Columns>    
                                                <asp:BoundField HeaderText="#" DataField="NoPago" />
                                                <asp:BoundField HeaderText="Tipo Pago" DataField="TipoPago" />
                                                <asp:BoundField HeaderText="Solicitud" DataField="IdSolicitud" />
                                                <asp:BoundField HeaderText="Monto" DataField="Monto" DataFormatString="{0:C2}" />                                                               
                                                <asp:BoundField HeaderText="Fecha de Pago" DataField="FechaPago" DataFormatString="{0:d}" />                                            
                                                <asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />                                            
                                            </Columns>
                                            <EmptyDataTemplate>
                                                No se encontraron Pagos Validos para Registrar
                                            </EmptyDataTemplate>
                                        </asp:GridView>                                        
                                    </div>

                                    <asp:Panel ID="pnlAdvertencia_CargarArchivo" class="col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-warning" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <i class="fas fa-exclamation-circle"></i>
                                            <asp:Label ID="lblAdvertencia_CargarArchivo" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>

                                    <div class="col-sm-12">
                                        <asp:LinkButton ID="lbtnGuardarArchivoPagosDirectos_CargarArchivo" OnClick="lbtnGuardarArchivoPagosDirectos_CargarArchivo_Click" CssClass="btn btn-sm btn-success" runat="server">
                                            <i class="far fa-check-circle"></i> Guardar Pagos
                                        </asp:LinkButton>

                                        <a href="frmPagosDirectos.aspx" class="btn btn-sm btn-danger">
                                            <i class="far fa-times-circle"></i>
                                            Cancelar
                                        </a>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                        <ajaxtoolKit:TabPanel ID="tpArchivosCargados" HeaderText="Archivos Cargados" runat="server">
                            <ContentTemplate>  
                                <asp:Panel ID="pnlExito_ArchivosCargados" class="col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <i class="far fa-check-circle"></i>
                                        <asp:Label ID="lblExito_ArchivosCargados" Text="El archivo a sido Registrado" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>   
                                                                                  
                                <div class="form-group col-sm-12">
                                    <div style="height:auto; width: auto; overflow-x: auto;">
                                        <asp:Panel ID="pnlAprobadosGV" runat="server">
                                            <asp:GridView ID="gvArchivosAprobados" 
                                                OnRowCommand="gvArchivosAprobados_RowCommand"
                                                OnPageIndexChanging="gvArchivosAprobados_PageIndexChanging"
                                                runat="server" Width="100%" AutoGenerateColumns="False"
                                                Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                CssClass="table table-bordered table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                                PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                Style="overflow-x:auto;">
                                                <PagerStyle CssClass="pagination-ty" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnDetalleArchivo" CommandName="VerDetalle" CommandArgument='<%# Eval("IdArchivo") %>' CssClass="btn btn-sm btn-secondary" ToolTip="Ver Archivo" runat="server">
                                                                <i class="fas fa-file-alt fa-lg"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                                    
                                                    <asp:BoundField HeaderText="Nombre" DataField="Nombre" />
                                                    <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaRegistro" />                                            
                                                    <asp:BoundField HeaderText="Cobros" DataField="TotalRegistros" />
                                                    <asp:BoundField HeaderText="Total a cobrar" DataField="TotalMonto" DataFormatString="{0:C2}" />                                                               
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No se encontraron Archivos Aprobados
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlAprobadosDetalle" Visible="false" runat="server">
                                            <div class="form-group col-xs-12">
                                                <asp:LinkButton ID="lbtnRegresar_Aprobados" CssClass="btn btn-sm btn-default" OnClick="lbtnRegresar_Aprobados_Click" runat="server">
                                                    <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                                                </asp:LinkButton>                                                
                                            </div>                                                                              

                                            <div class="form-group col-sm-12">
                                                <label>Nombre Archivo</label>           
                                                <pre><asp:Label ID="lblNombreArchivo_Aprobados" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6">   
                                                <label>Tipo Archivo</label>        
                                                <pre><asp:Label ID="lblTipoArchivo_Aprobados" runat="server"></asp:Label></pre>
                                            </div>                                           
                                            <div class="form-group col-sm-6"> 
                                                <label>Fecha Carga</label>          
                                                <pre><asp:Label ID="lblFechaRegistro_Aprobados" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6"> 
                                            <br />
                                            
                                            </div>                                          
                                            <div class="form-group col-sm-12">
                                                <hr />
                                                <div class="form-group col-sm-6"> 
                                                    <input type="button" onclick="javascript: document.getElementById('<%=btnDescargar_Aprobados.ClientID%>').click();" class="btn btn-sm btn-primary" value="Descargar Archivo Envio" />
                                                    <asp:Panel ID="pnlDescargaError_Aprobados" class="col-sm-12" Visible="false" runat="server">                                
                                                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                            No se puede descargar el archivo, ha sido movido o eliminado
                                                        </div>
                                                    </asp:Panel>                                                                                                
                                                </div>
                                                
                                                <div class="form-group col-sm-6">                                                   
                                                    <asp:UpdatePanel ID="subirRespuesta" UpdateMode="Always" runat="server">
                                                        <ContentTemplate>
                                                            <div class="form-group col-sm-5">
                                                                <asp:FileUpload ID="respuestaBBVA" CssClass="form-control" runat="server" AllowMultiple="true"/>
                                                                <asp:RegularExpressionValidator
                                                                        id="RegularExpressionValidator2"                                    
                                                                        ErrorMessage="El archivo debe ser formato CSV"
                                                                        ValidationExpression ="^.+(.txt|.TXT|.DVR_|.dvr_)$"
                                                                        ControlToValidate="respuestaBBVA" ValidationGroup="CargarResp" CssClass="small" ForeColor="Red" Display="Dynamic"
                                                                        runat="server"></asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                                <asp:Button ID="btnSubir" runat="server" Text="Cargar Respuesta" OnClick="btnCargarRespuesta_Click" class="btn btn-sm btn-primary"/><br />
                                                            </div>                                                             
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnSubir" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>                                                    
                                                </div>                                                
                                            </div>
                                            <asp:Panel ID="pnlDescargaError_respuesta" class="col-sm-12" Visible="false" runat="server">
                                                <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                    <asp:Label ID="listofuploadedfiles" runat="server" Text=""></asp:Label>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlCargaResp_correcto" class="col-sm-12" Visible="false" runat="server">
                                                <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                    <asp:Label ID="lblaplicadomi_correcto" runat="server" Text=""></asp:Label>
                                                </div>
                                            </asp:Panel>                                            
                                            <br />
                                            <div class="col-sm-12">
                                                <blockquote style="background-color:#F8F8F8;">
                                                    <p>Historial de envios</p>
                                                </blockquote>
                                            </div>
                                            <div class="form-group col-xs-12" style="overflow:auto;">                                                                                           
                                                <asp:GridView ID="gvHistoriaDomiciliado"                                                 
                                                OnPageIndexChanging="gvHistoriaDomiciliado_PageIndexChanging"
                                                runat="server" Width="100%" AutoGenerateColumns="False"
                                                Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                CssClass="table table-bordered table-hover-warning table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="false"
                                                PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                Style="overflow-x:auto; overflow-y:scroll;">
                                                <PagerStyle CssClass="pagination-ty" />
                                                <Columns>                                                                                                                                                       
                                                    <asp:BoundField HeaderText="Reg. Enviados" DataField="total_registros" />                                            
                                                    <asp:BoundField HeaderText="Monto a Cobrar" DataField="monto_cobrar" DataFormatString="{0:C2}"/>
                                                    <asp:BoundField HeaderText="Archivos Enviados" DataField="total_archivos" />
                                                    <asp:BoundField HeaderText="Fecha Envio" DataField="fechaEnvio" DataFormatString="{0:dd/MM/yyyy}"/>
                                                    <asp:BoundField HeaderText="Reg. Cobrados" DataField="numero_registros_cobrados" />
                                                    <asp:BoundField HeaderText="Monto Cobrado" DataField="monto_cobrado" DataFormatString="{0:C2}"/>                                                    
                                                    <asp:BoundField HeaderText="Fecha Respuesta" DataField="fechaAplicado"/>                                                                
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No se encontraron registros de envios
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                            </div>
                                        </asp:Panel>
                                    </div>  
                                </div>                            
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel> 
                        <ajaxtoolKit:TabPanel ID="tpArchivosLayout" HeaderText="Archivos Layout" runat="server">
                            <ContentTemplate>  
                                <asp:Panel ID="pnlExito_ArchivosLayout" class="col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <i class="far fa-check-circle"></i>
                                        <asp:Label ID="lblExito_ArchivosLayout" Text="El archivo a sido Registrado" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>   
                                                                                  
                                <div class="form-group col-sm-12">
                                    <div style="height:auto; width: auto; overflow-x: auto;">
                                        <asp:Panel ID="pnlLayoutGV" runat="server">
                                            <asp:GridView ID="gvArchivosLayout" 
                                                OnRowCommand="gvArchivosLayout_RowCommand"
                                                OnPageIndexChanging="gvArchivosLayout_PageIndexChanging"
                                                runat="server" Width="100%" AutoGenerateColumns="False"
                                                Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                CssClass="table table-bordered table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                                PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                Style="overflow-x:auto;">
                                                <PagerStyle CssClass="pagination-ty" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnDetalleArchivo" CommandName="VerDetalle" CommandArgument='<%# Eval("IdArchivo") %>' CssClass="btn btn-sm btn-secondary" ToolTip="Ver Archivo" runat="server">
                                                                <i class="fas fa-file-alt fa-lg"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                                    
                                                    <asp:BoundField HeaderText="Nombre" DataField="Nombre" />
                                                    <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaRegistro" />                                            
                                                    <asp:BoundField HeaderText="Cobros" DataField="TotalRegistros" />
                                                    <asp:BoundField HeaderText="Total a cobrar" DataField="TotalMonto" DataFormatString="{0:C2}" />                                                               
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No se encontraron Archivos Layout
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlLayoutDetalle" Visible="false" runat="server">
                                            <div class="form-group col-xs-12">
                                                <asp:LinkButton ID="lbtnRegresar_Layout" CssClass="btn btn-sm btn-default" OnClick="lbtnRegresar_Layout_Click" runat="server">
                                                    <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                                                </asp:LinkButton>                                                
                                            </div>                                                                              

                                            <div class="form-group col-sm-12">
                                                <label>Nombre Archivo</label>           
                                                <pre><asp:Label ID="lblNombreArchivo_Layout" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6">   
                                                <label>Tipo Archivo</label>        
                                                <pre><asp:Label ID="lblTipoArchivo_Layout" runat="server"></asp:Label></pre>
                                            </div>                                           
                                            <div class="form-group col-sm-6"> 
                                                <label>Fecha Registro</label>          
                                                <pre><asp:Label ID="lblFechaCarga_Layout" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6"> 
                                            <br />
                                            
                                            </div>                                          
                                            <div class="form-group col-sm-12">
                                                <hr />
                                                <div class="form-group col-sm-6"> 
                                                    <input type="button" onclick="javascript: document.getElementById('<%=btnDescargar_Layout.ClientID%>').click();" class="btn btn-sm btn-primary" value="Descargar Archivo Envio" />
                                                    <asp:Panel ID="pnlDescargaError_Layout" class="col-sm-12" Visible="false" runat="server">                                
                                                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                            No se puede descargar el archivo, ha sido movido o eliminado
                                                        </div>
                                                    </asp:Panel>                                                                                                
                                                </div>
                                                
                                                <div class="form-group col-sm-6">                                                   
                                                    <asp:UpdatePanel ID="subirRespuesta_Layout" UpdateMode="Always" runat="server">
                                                        <ContentTemplate>
                                                            <div class="form-group col-sm-5">
                                                                <asp:FileUpload ID="respuestaBBVA_Layout" CssClass="form-control" runat="server" AllowMultiple="true"/>
                                                                <asp:RegularExpressionValidator
                                                                        id="RegularExpressionValidator3"                                    
                                                                        ErrorMessage="El archivo debe ser formato CSV"
                                                                        ValidationExpression ="^.+(.txt|.TXT|.DVR_|.dvr_)$"
                                                                        ControlToValidate="respuestaBBVA_Layout" ValidationGroup="CargarRespLayout" CssClass="small" ForeColor="Red" Display="Dynamic"
                                                                        runat="server"></asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                                <asp:Button ID="btnSubir_Layout" runat="server" Text="Cargar Respuesta" OnClick="btnCargarRespuesta_Layout_Click" class="btn btn-sm btn-primary"/><br />
                                                            </div>                                                                                                                        
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnSubir_Layout" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>                                                                                                        
                                                </div>                                                
                                            </div>
                                            <asp:Panel ID="pnlDescargaError_respuesta_Layout" class="col-sm-12" Visible="false" runat="server">
                                                <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                    <asp:Label ID="listofuploadedfiles_Layout" runat="server" Text=""></asp:Label>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlCargaResp_correcto_Layout" class="col-sm-12" Visible="false" runat="server">
                                                <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                    <asp:Label ID="lblaplicadomi_correcto_Layout" runat="server" Text=""></asp:Label>
                                                </div>
                                            </asp:Panel>                                            
                                            <br />
                                            <div class="col-sm-12">
                                                <blockquote style="background-color:#F8F8F8;">
                                                    <p>Historial de envios</p>
                                                </blockquote>
                                            </div>
                                            <div class="form-group col-xs-12" style="overflow:auto;">                                            
                                                <asp:GridView ID="gvHistoriaDomiciliado_Layout"                                                 
                                                    OnPageIndexChanging="gvHistoriaDomiciliado_Layout_PageIndexChanging"
                                                    runat="server" Width="100%" AutoGenerateColumns="False"
                                                    Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                    CssClass="table table-bordered table-hover-warning table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="false"
                                                    PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                    Style="overflow-x:auto; overflow-y:scroll;">
                                                <PagerStyle CssClass="pagination-ty" />
                                                <Columns>                                                                                                                                                       
                                                    <asp:BoundField HeaderText="Reg. Enviados" DataField="total_registros" />                                            
                                                    <asp:BoundField HeaderText="Monto a Cobrar" DataField="monto_cobrar" DataFormatString="{0:C2}"/>
                                                    <asp:BoundField HeaderText="Archivos Enviados" DataField="total_archivos" />
                                                    <asp:BoundField HeaderText="Fecha Envio" DataField="fechaEnvio" DataFormatString="{0:dd/MM/yyyy}"/>
                                                    <asp:BoundField HeaderText="Reg. Cobrados" DataField="numero_registros_cobrados" />
                                                    <asp:BoundField HeaderText="Monto Cobrado" DataField="monto_cobrado" DataFormatString="{0:C2}"/>                                                    
                                                    <asp:BoundField HeaderText="Fecha Respuesta" DataField="fechaAplicado"/>                                                                
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No se encontraron registros de envios
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                            </div>
                                        </asp:Panel>
                                    </div>  
                                </div>                            
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                        <ajaxtoolKit:TabPanel HeaderText="Archivos con Error" runat="server">
                            <ContentTemplate>
                                <div class="form-group col-sm-12">
                                    <div style="height:auto; width: auto; overflow-x: auto;">
                                        <asp:Panel ID="pnlErrorGV" runat="server">
                                            <asp:GridView ID="gvArchivosError" 
                                                OnRowCommand="gvArchivosError_RowCommand"
                                                OnPageIndexChanging="gvArchivosError_PageIndexChanging"
                                                runat="server" Width="100%" AutoGenerateColumns="False"
                                                Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                CssClass="table table-bordered table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                                PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                Style="overflow-x:auto;">
                                                <PagerStyle CssClass="pagination" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnDetalleArchivo" CommandName="VerDetalle" CommandArgument='<%# Eval("IdArchivo") %>' CssClass="btn btn-sm btn-secondary" ToolTip="Ver Archivo" runat="server">
                                                                <i class="fas fa-file-alt fa-lg"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                                                                              
                                                    <asp:BoundField HeaderText="Nombre" DataField="Nombre" />
                                                    <asp:BoundField HeaderText="Tipo" DataField="TipoArchivo.Descripcion" />
                                                    <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaRegistro" />                                                                                        
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No se encontraron Archivos con Error
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlErrorDetalle" runat="server">
                                            <div class="form-group col-xs-12">
                                                <asp:LinkButton ID="lbtnRegresar_Error" CssClass="btn btn-sm btn-default" OnClick="lbtnRegresar_Error_Click" runat="server">
                                                    <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                                                </asp:LinkButton>
                                                 <asp:LinkButton ID="lbtnDescargar_Error" CssClass="btn btn-sm btn-default" OnClick="lbtnDescargar_Error_Click" runat="server">
                                                    <i class="fas fa-download fa-lg"></i> Descargar
                                                </asp:LinkButton>
                                                <asp:Panel ID="pnlDescargaError_Error" class="col-sm-12" Visible="false" runat="server">                                
                                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                                        No se puede descargar el archivo, ha sido movido o eliminado
                                                    </div>
                                                </asp:Panel> 
                                                 <hr />
                                            </div>                                                                              

                                            <div class="form-group col-sm-6">
                                                <label>Nombre Archivo</label>           
                                                <pre><asp:Label ID="lblNombreArchivo_Error" runat="server"></asp:Label></pre>
                                            </div>
                                            <div class="form-group col-sm-6">   
                                                <label>Tipo Archivo</label>        
                                                <pre><asp:Label ID="lblTipoArchivo_Error" runat="server"></asp:Label></pre>
                                            </div>                                            
                                            <div class="form-group col-sm-6"> 
                                                <label>Fecha Carga</label>          
                                                <pre><asp:Label ID="lblFechaRegistro_Error" runat="server"></asp:Label></pre>
                                            </div>

                                            <br />
                                            <div class="col-sm-12">
                                                <blockquote style="background-color:#F8F8F8;">
                                                    <p>Contenido del Archivo</p>
                                                </blockquote>
                                            </div>
                                            <div class="form-group col-xs-12" style="overflow:auto; background-color:#fcf8e3;">                                            
                                                <asp:Label ID="lblDetalleArchivo_Error" CssClass="small" runat="server"></asp:Label>
                                            </div>
                                        </asp:Panel>
                                    </div>  
                                </div> 
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>                                                                  
                    </ajaxtoolKit:TabContainer>
                    <div style="visibility:hidden">
                        <asp:Button ID="btnCargarArchivo" ValidationGroup="CargarArchivo" OnClick="btnCargarArchivo_Click" runat="server" />
                        <asp:Button ID="btnDescargar_Aprobados" OnClick="btnDescargar_Aprobados_Click" runat="server" />
                        <asp:Button ID="btnDescargar_Layout" OnClick="btnDescargar_Layout_Click" runat="server" />
                        <asp:Button ID="CargarResp" ValidationGroup="CargarResp" OnClick="btnCargarRespuesta_Click" runat="server" />
                        <asp:Button ID="CargarRespLayout" ValidationGroup="CargarRespLayout" OnClick="btnCargarRespuesta_Layout_Click" runat="server" />
                        
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnCargarArchivo" />
                    <asp:PostBackTrigger ControlID="btnDescargar_Aprobados" />
                    <asp:PostBackTrigger ControlID="btnDescargar_Layout" />
                    <asp:PostBackTrigger ControlID="CargarResp" />
                    <asp:PostBackTrigger ControlID="CargarRespLayout" />
                </Triggers>           
            </asp:UpdatePanel>                                               
        </div>
    </div>
</asp:Content>

