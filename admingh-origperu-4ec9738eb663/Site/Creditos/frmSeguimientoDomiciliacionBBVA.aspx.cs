﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Ionic;
using Ionic.Zlib;
using Ionic.Zip;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Text.RegularExpressions;

public partial class Site_Creditos_frmSeguimientoDomiciliacionBBVA : System.Web.UI.Page
{
    protected string NombreArchivo
    {
        get
        {
            return (ViewState[this.UniqueID + "_NombreArchivo"] != null) ? ViewState[this.UniqueID + "_NombreArchivo"].ToString() : string.Empty;
        }
        set
        {
            ViewState[this.UniqueID + "_NombreArchivo"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // Ocultamos los paneles de detalle de inicio
            pnlAprobadosDetalle.Visible = false;
            pnlErrorDetalle.Visible = false;
        }
    }

    #region "Domiciliacion BBVA"

    protected void btnCargarArchivo_Click(object sender, EventArgs e)
    {
        pnlError_CargarArchivo.Visible = false;

        try
        {
            using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
            {
                // Validar antes de cargar el archivo
                if (!fuArchivo.HasFile)
                    throw new Exception("Seleccione el Archivo a cargar");
                else if (ws.Filtrar_DomiBBVA(new wsSOPF.ArchivoMasivo() { Nombre = Path.GetFileNameWithoutExtension(fuArchivo.FileName.Trim()) + ".txt" }).Count() > 0)
                    throw new Exception("Ya existe un Archivo con el mismo nombre");


                // Guardar el archivo a la carpeta local de manera TEMPORAL (manejo en memoria)
                string folderPath = Server.MapPath("~/Archivos/BBVA/");
                fuArchivo.SaveAs(folderPath + "TMP_" + Path.GetFileName(fuArchivo.FileName));

                this.NombreArchivo = fuArchivo.FileName.Trim();

                
                //procesa y sube el archivo
                CsvToXml();
                GuardarArchivo();

                pnlCarga_CargarArchivo.Visible = true;
                pnlResumen_CargarArchivo.Visible = false;

                pnlExito_ArchivosCargados.Visible = true;
                tcCargaArchivos.ActiveTab = tpArchivosCargados;

                CargarArchivosAprobados();

                
            }
        }
        catch (Exception ex)
        {
            pnlCarga_CargarArchivo.Visible = true;
            pnlResumen_CargarArchivo.Visible = false;

            lblError_CargarArchivo.Text = ex.Message;
            pnlError_CargarArchivo.Visible = true;
        }

        this.NombreArchivo = null;
    }    

    //guardar el archivo en una carpeta del servidor local de la BD
    private void GuardarRespuesta(string nombreArchivo, string ext)
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {

            string folderPath = Server.MapPath("~/Archivos/BBVA/");

            //var extension2  = Path.GetExtension(nombreArchivo.FileName);

            // Copiamos el archivo al servidor de BD
            string localURL = folderPath + Path.GetFileNameWithoutExtension(nombreArchivo) + ext;
            string remoteURL = System.Configuration.ConfigurationManager.AppSettings["URLRedArchivoMasivo"];
            string remoteUser = System.Configuration.ConfigurationManager.AppSettings["UsuarioArchivoMasivo"];
            string remotePass = System.Configuration.ConfigurationManager.AppSettings["ClaveArchivoMasivo"];

            NetworkShare.DisconnectFromShare(remoteURL, true); // Nos desconectamos en caso de que estemos actualmente conectados

            NetworkShare.ConnectToShare(remoteURL, remoteUser, remotePass); // Nos conectamos con las nuevas credenciales

            File.Copy(localURL, remoteURL + Path.GetFileNameWithoutExtension(nombreArchivo) + ext, true);

            NetworkShare.DisconnectFromShare(remoteURL, false); // Nos desconectamos del server

            // Procesar el archivo
            //wsSOPF.ArchivoMasivo gEntity = new wsSOPF.ArchivoMasivo();
            //gEntity.Nombre = Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".xml";

            //wsSOPF.ResultadoOfboolean res = ws.RespuestaDomiBBVA_Registrar(gEntity);

            //if (res.Codigo != 0)
            //    throw new Exception(res.Mensaje);
        }
    }

    private void ProcesarRespuesta(int numArchivos)
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {           
            wsSOPF.ResultadoOfboolean res = ws.RespuestaDomiBBVA_Registrar(this.NombreArchivo, Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + ".XML", numArchivos);

            if (res.Codigo != 0)
                throw new Exception(res.Mensaje);
            else
                listofuploadedfiles.Text = res.Mensaje;
        }
    }

    private void ProcesarRespuestaLayout(int numArchivos)
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            wsSOPF.ResultadoOfboolean res = ws.RespuestaDomiBBVA_Registrar(this.NombreArchivo, lblNombreArchivo_Layout.Text.Trim(), numArchivos);

            if (res.Codigo != 0)
                throw new Exception(res.Mensaje);
            else
                listofuploadedfiles_Layout.Text = res.Mensaje;
        }
    }

    protected void tcCargaArchivos_ActiveTabChanged(object sender, EventArgs e)
    {
        switch (tcCargaArchivos.ActiveTab.HeaderText.Trim())
        {
            case "Cargar Archivo":
                pnlError_CargarArchivo.Visible = false;
                break;
            case "Archivos Cargados":
                CargarArchivosAprobados();

                pnlExito_ArchivosCargados.Visible = false;
                pnlAprobadosGV.Visible = true;
                pnlAprobadosDetalle.Visible = false;
                pnlDescargaError_Aprobados.Visible = false;
                break;
            case "Archivos Layout":
                CargarArchivosLayout();

                pnlExito_ArchivosLayout.Visible = false;
                pnlLayoutGV.Visible = true;
                pnlLayoutDetalle.Visible = false;
                pnlDescargaError_Layout.Visible = false;
                break;
            case "Archivos con Error":
                //CargarArchivosError();

                pnlErrorGV.Visible = true;
                pnlErrorDetalle.Visible = false;
                pnlDescargaError_Error.Visible = false;
                break;
        }
    }

    protected void lbtnGuardarArchivoPagosDirectos_CargarArchivo_Click(object sender, EventArgs e)
    {
        try
        {
            ProcesarCSV();
            //ConvertirCSVToTxt();
            CsvToXml();
            GuardarArchivo();

            pnlCarga_CargarArchivo.Visible = true;
            pnlResumen_CargarArchivo.Visible = false;

            pnlExito_ArchivosCargados.Visible = true;
            tcCargaArchivos.ActiveTab = tpArchivosCargados;

            CargarArchivosAprobados();
        }
        catch (Exception ex)
        {
            pnlCarga_CargarArchivo.Visible = true;
            pnlResumen_CargarArchivo.Visible = false;

            pnlError_CargarArchivo.Visible = true;
            lblError_CargarArchivo.Text = ex.Message;
        }
    }

    #region "Archivos Aprobados"

    protected void lbtnRegresar_Aprobados_Click(object sender, EventArgs e)
    {
        pnlAprobadosGV.Visible = true;
        pnlAprobadosDetalle.Visible = false;
        pnlDescargaError_Aprobados.Visible = false;


        pnlExito_ArchivosCargados.Visible = false;

        pnlDescargaError_respuesta.Visible = false;
        pnlCargaResp_correcto.Visible = false;
    }

    protected void btnCargarRespuesta_Click(object sender, EventArgs e)
    {
        pnlError_CargarArchivo.Visible = false;
        pnlDescargaError_respuesta.Visible = false;
        pnlCargaResp_correcto.Visible = false;

        try
        {

            wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient();
            List<wsSOPF.FitrarRespuestaBBVA> filtrar = ws.FiltrarRespuestaDomiBBVA(Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + ".XML").ToList();

            if (filtrar.Count > 0)
            {
                //DataRow row = dt.Rows[0];

                int Idarchivo = Convert.ToInt32(filtrar[0].idArchivo.ToString());
                int numArchivos = Convert.ToInt32(filtrar[0].TotalArchivos.ToString());
                int TotalRegistros = Convert.ToInt32(filtrar[0].TotalRegistros.ToString());
                float MontoCobrar = float.Parse(filtrar[0].TotalRegistros.ToString());
                if (respuestaBBVA.HasFile)
                {
                    //validar las extensiones de los archivos
                    if (respuestaBBVA.PostedFiles.Count == numArchivos)
                    {
                        foreach (var archivo in respuestaBBVA.PostedFiles)
                        {
                            //var name = Path.GetFileNameWithoutExtension(archivo.FileName); // Get the name only

                            var extension = Path.GetExtension(archivo.FileName); // Get the extension only

                            if (!(extension == ".TXT" || extension == ".txt" || extension == ".DVR_" || extension == ".dvr_"))
                            {
                                pnlDescargaError_respuesta.Visible = true;
                                listofuploadedfiles.Text = "Solo se permiten archivos con extension .TXT, .DVR_";
                                return;
                            }
                        }

                    }
                    //hacer validacion si numero de archivos subidos y regsitros totales coinciden con los guardados en regsitro
                    if (respuestaBBVA.PostedFiles.Count == numArchivos)
                    {
                        //string nombresArray = "";

                        // Tenemos acceso al archivo para realizar lo que necesitemos
                        //var informacionDelArchivo = String.Format("{0} es de tipo {1} y tiene un tamaño de {2} KB.", archivo.FileName, archivo.ContentType, archivo.ContentLength / 1024);
                        var pathCarpetaDestino = System.IO.Path.Combine(Server.MapPath("~/Archivos/BBVA/"));
                        var carpetaDestino = new System.IO.DirectoryInfo(pathCarpetaDestino);
                        var extension2 = "";
                        if (!carpetaDestino.Exists)
                            carpetaDestino.Create();
                        foreach (var archivo in respuestaBBVA.PostedFiles)
                        {
                            var nombreArchivo = System.IO.Path.GetFileName(archivo.FileName);
                            extension2 = Path.GetExtension(archivo.FileName);
                            nombreArchivo = GenerarCodigo() + nombreArchivo;
                            var pathArchivoDestino = System.IO.Path.Combine(pathCarpetaDestino, nombreArchivo);
                            archivo.SaveAs(pathArchivoDestino);
                            this.NombreArchivo += Path.GetFileNameWithoutExtension(nombreArchivo.Trim()) + extension2 + ",";


                            //CsvToXml2(nombreArchivo);
                            GuardarRespuesta(nombreArchivo, extension2);
                        }

                        ProcesarRespuesta(numArchivos);
                        pnlCargaResp_correcto.Visible = true;

                        lblaplicadomi_correcto.Text = "Archivos aplicados correcto";
                        CargarHistorialDomiciliadoBBVA();
                    }
                    else
                    {
                        pnlDescargaError_respuesta.Visible = true;
                        listofuploadedfiles.Text = "Se esperan " + numArchivos + " archivos de respuesta";
                    }

                }
                else
                {

                    pnlDescargaError_respuesta.Visible = true;
                    listofuploadedfiles.Text = "Favor de elegir al menos un archivo";
                }
            }
            else
            {
                pnlDescargaError_respuesta.Visible = true;
                listofuploadedfiles.Text = "No se han creado archivos de envio para este documento";
            }

        }
        catch (Exception ex)
        {
            listofuploadedfiles.Text = ex.Message;
            listofuploadedfiles.Visible = true;

            pnlCargaResp_correcto.Visible = false;
            pnlDescargaError_respuesta.Visible = true;


        }

        this.NombreArchivo = null;
    }

    protected void btnDescargar_Aprobados_Click(object sender, EventArgs e)
    {


        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.EnvioDomiBBVA> archivosProcesados = ws.DescragaTXTDomiBBVA(Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + ".XML").ToList();

            List<wsSOPF.CAT_Parametros> parametro = ws.ObtenerParametros(0, "NRA").ToList();

            int contador = 1;
            int numArchivo = 0;
            int numRegistros = Convert.ToInt32(parametro[0].ValorInt.ToString()); //300
            var result = new StringBuilder();
            
            foreach (wsSOPF.EnvioDomiBBVA row in archivosProcesados)
            {
                if (contador % numRegistros == 0)
                {
                    result.Append(row.RowInfo.Replace("\n", "\r"));
                }else
                {
                    result.Append(row.RowInfo);
                }

                if (contador % numRegistros == 0)
                {
                    numArchivo++;
                    StreamWriter objWriter = new StreamWriter(Server.MapPath("~/Archivos/BBVA/" + Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + numArchivo + ".txt"), false);

                    objWriter.WriteLine(result.ToString());
                    objWriter.Close();

                    BorrarCRLF(Server.MapPath("~/Archivos/BBVA/" + Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + numArchivo + ".txt"));

                    result = new StringBuilder();

                }

                contador++;
            }

            if ((contador - 1) % numRegistros != 0)
            {
                numArchivo++;
                StreamWriter objWriter2 = new StreamWriter(Server.MapPath("~/Archivos/BBVA/" + Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + numArchivo + ".txt"), false);
                objWriter2.WriteLine(result.ToString());
                objWriter2.Close();

                BorrarCRLF(Server.MapPath("~/Archivos/BBVA/" + Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + numArchivo + ".txt"));


            }

            //borramos el zip si ya existe
            if (File.Exists(Server.MapPath("~/Archivos/BBVA/") + Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + DateTime.Now.ToString("dd-MM-yyyy") + ".zip"))
                File.Delete(Server.MapPath("~/Archivos/BBVA/" + Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + DateTime.Now.ToString("dd-MM-yyyy") + ".zip"));

            using (ZipFile zip = new ZipFile())
            {

                for (int i = 1; i <= numArchivo; i++)
                {
                    if (File.Exists(Server.MapPath("~/Archivos/BBVA/") + Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + i + ".txt"))
                        zip.AddFile(Server.MapPath("~/Archivos/BBVA/") + Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + i + ".txt", @"\");
                    else
                        pnlDescargaError_Aprobados.Visible = true;
                }

                zip.Save(Server.MapPath("~/Archivos/BBVA/") + Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
 

                //eliminar archivos creados para descargar
                for (int j = 1; j <= numArchivo; j++)
                {
                    File.Delete(Server.MapPath("~/Archivos/BBVA/" + Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + j + ".txt"));
                }
            }
                                          
        }

        CargarHistorialDomiciliadoBBVA();

        string NombreArchivo_Aprobados = Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + DateTime.Now.ToString("dd-MM-yyyy") + ".zip";
        
        if (File.Exists(Server.MapPath("~/Archivos/BBVA/") + NombreArchivo_Aprobados))
        {
            Response.Redirect(string.Format("~/DownloadFileZ.ashx?fileName={0}", Server.MapPath("~/Archivos/BBVA/") + NombreArchivo_Aprobados));
        }
        else
        {
            pnlDescargaError_Aprobados.Visible = true;
        }

    }

    public static void BorrarCRLF(string filepath)
    {
        String strFile = File.ReadAllText(filepath, Encoding.Default);
        strFile = strFile.Replace("\r\n", "").Replace("\r", "");

        File.WriteAllText(filepath, strFile);
    }

    public void SplitFile(int Size, string File, string OutputBaseFileName, string OutputFilePath)
    {
        int fileNumber = 0;
        string newFileName = OutputFilePath + OutputBaseFileName;

        using (System.IO.StreamReader sr = new System.IO.StreamReader(File))
        {
            while (!sr.EndOfStream)
            {
                int count = 0;
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(newFileName + ++fileNumber + ".txt"))
                {
                    sw.AutoFlush = true;
                    while (!sr.EndOfStream && ++count < Size)
                    {
                        sw.WriteLine(sr.ReadLine());
                    }
                }
            }
        }
    }

    protected void gvArchivosAprobados_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "VerDetalle":
                using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
                {
                    wsSOPF.ArchivoMasivo archivo = ws.DomiciliacionArchivo_ObtenerDetalle(int.Parse(e.CommandArgument.ToString()));

                    lblNombreArchivo_Aprobados.Text = archivo.Nombre;
                    lblTipoArchivo_Aprobados.Text = archivo.TipoArchivo.Descripcion;
                    lblFechaRegistro_Aprobados.Text = archivo.FechaRegistro.GetValueOrDefault().ToString();

   



                    pnlAprobadosGV.Visible = false;
                    pnlAprobadosDetalle.Visible = true;
                    List<wsSOPF.HistorialCobros> archivo2 = ws.ObtenerHistorialSeguimientoDomiBBVA(int.Parse(e.CommandArgument.ToString())).ToList();

                    gvHistoriaDomiciliado.DataSource = archivo2;
                    gvHistoriaDomiciliado.DataBind();
                }
                break;
        }
    }

    protected void gvArchivosAprobados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvArchivosAprobados.PageIndex = e.NewPageIndex;
        CargarArchivosAprobados();
    }

    protected void gvHistoriaDomiciliado_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvHistoriaDomiciliado.PageIndex = e.NewPageIndex;
        
    }

    #endregion

    #region "Archivos Layout"

    protected void lbtnRegresar_Layout_Click(object sender, EventArgs e)
    {
        pnlLayoutGV.Visible = true;
        pnlLayoutDetalle.Visible = false;
        pnlDescargaError_Layout.Visible = false;

        pnlExito_ArchivosCargados.Visible = false;

        pnlDescargaError_respuesta.Visible = false;
        pnlCargaResp_correcto.Visible = false;
    }

    protected void btnCargarRespuesta_Layout_Click(object sender, EventArgs e)
    {
        pnlError_CargarArchivo.Visible = false;
        pnlDescargaError_respuesta_Layout.Visible = false;
        pnlCargaResp_correcto_Layout.Visible = false;

        try
        {
            wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient();
            List<wsSOPF.FitrarRespuestaBBVA> filtrar = ws.FiltrarRespuestaDomiBBVA(lblNombreArchivo_Layout.Text.Trim()).ToList();

            if (filtrar.Count > 0)
            {               
                int Idarchivo = Convert.ToInt32(filtrar[0].idArchivo.ToString());
                int numArchivos = Convert.ToInt32(filtrar[0].TotalArchivos.ToString());
                int TotalRegistros = Convert.ToInt32(filtrar[0].TotalRegistros.ToString());
                float MontoCobrar = float.Parse(filtrar[0].MontoCobrar.ToString());

                if (respuestaBBVA_Layout.HasFile)
                {
                    // Validar las extensiones de los archivos
                    if (respuestaBBVA_Layout.PostedFiles.Count == numArchivos)
                    {
                        foreach (var archivo in respuestaBBVA_Layout.PostedFiles)
                        {                           
                            var extension = Path.GetExtension(archivo.FileName); // Get the extension only

                            if (!(extension == ".TXT" || extension == ".txt" || extension == ".DVR_" || extension == ".dvr_"))
                            {
                                pnlDescargaError_respuesta_Layout.Visible = true;
                                listofuploadedfiles_Layout.Text = "Solo se permiten archivos con extension .TXT, .DVR_";
                                return;
                            }
                        }
                    }
                    
                    // Hacer validacion si numero de archivos subidos y registros totales coinciden con los guardados en regsitro
                    if (respuestaBBVA_Layout.PostedFiles.Count == numArchivos)
                    {                     
                        // Tenemos acceso al archivo para realizar lo que necesitemos                        
                        var pathCarpetaDestino = System.IO.Path.Combine(Server.MapPath("~/Archivos/BBVA/"));
                        var carpetaDestino = new System.IO.DirectoryInfo(pathCarpetaDestino);
                        var extension2 = "";

                        if (!carpetaDestino.Exists)
                            carpetaDestino.Create();

                        foreach (var archivo in respuestaBBVA_Layout.PostedFiles)
                        {
                            var nombreArchivo = System.IO.Path.GetFileName(archivo.FileName);
                            extension2 = Path.GetExtension(archivo.FileName);
                            nombreArchivo = GenerarCodigo() + nombreArchivo;
                            var pathArchivoDestino = System.IO.Path.Combine(pathCarpetaDestino, nombreArchivo);
                            archivo.SaveAs(pathArchivoDestino);                            
                            this.NombreArchivo += Path.GetFileNameWithoutExtension(nombreArchivo.Trim()) + extension2 + ",";

                            GuardarRespuesta(nombreArchivo, extension2);
                        }

                        ProcesarRespuestaLayout(numArchivos);
                        pnlCargaResp_correcto_Layout.Visible = true;

                        lblaplicadomi_correcto_Layout.Text = "Archivos aplicados correcto";
                        CargarHistorialDomiciliadoLayoutBBVA();
                    }
                    else
                    {
                        pnlDescargaError_respuesta_Layout.Visible = true;
                        listofuploadedfiles_Layout.Text = "Se esperan " + numArchivos + " archivos de respuesta";
                    }
                }
                else
                {
                    pnlDescargaError_respuesta_Layout.Visible = true;
                    listofuploadedfiles_Layout.Text = "Favor de elegir al menos un archivo";
                }
            }
            else
            {
                pnlDescargaError_respuesta_Layout.Visible = true;
                listofuploadedfiles_Layout.Text = "No se han creado archivos de envio para este documento";
            }
        }
        catch (Exception ex)
        {
            listofuploadedfiles_Layout.Text = ex.Message;
            listofuploadedfiles_Layout.Visible = true;

            pnlCargaResp_correcto_Layout.Visible = false;
            pnlDescargaError_respuesta_Layout.Visible = true;
        }

        this.NombreArchivo = null;
    }

    protected void btnDescargar_Layout_Click(object sender, EventArgs e)
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.EnvioDomiBBVA> archivosProcesados = ws.DescragaTXTDomiBBVA(lblNombreArchivo_Layout.Text.Trim()).ToList();

            List<wsSOPF.CAT_Parametros> parametro = ws.ObtenerParametros(0, "NRA").ToList();

            int contador = 1;
            int numArchivo = 0;
            int numRegistros = Convert.ToInt32(parametro[0].ValorInt.ToString());
            var result = new StringBuilder();

            foreach (wsSOPF.EnvioDomiBBVA row in archivosProcesados)
            {
                if (contador % numRegistros == 0)
                {
                    result.Append(row.RowInfo.Replace("\n", "\r"));
                }
                else
                {
                    result.Append(row.RowInfo);
                }

                if (contador % numRegistros == 0)
                {
                    numArchivo++;
                    StreamWriter objWriter = new StreamWriter(Server.MapPath("~/Archivos/BBVA/" + lblNombreArchivo_Layout.Text.Trim() + numArchivo + ".txt"), false);

                    objWriter.WriteLine(result.ToString());
                    objWriter.Close();

                    BorrarCRLF(Server.MapPath("~/Archivos/BBVA/" + lblNombreArchivo_Layout.Text.Trim() + numArchivo + ".txt"));

                    result = new StringBuilder();
                }

                contador++;
            }

            if ((contador - 1) % numRegistros != 0)
            {
                numArchivo++;
                StreamWriter objWriter2 = new StreamWriter(Server.MapPath("~/Archivos/BBVA/" + lblNombreArchivo_Layout.Text.Trim() + numArchivo + ".txt"), false);
                objWriter2.WriteLine(result.ToString());
                objWriter2.Close();

                BorrarCRLF(Server.MapPath("~/Archivos/BBVA/" + lblNombreArchivo_Layout.Text.Trim() + numArchivo + ".txt"));
            }

            // Borramos el zip si ya existe
            if (File.Exists(Server.MapPath("~/Archivos/BBVA/") + lblNombreArchivo_Layout.Text.Trim() + DateTime.Now.ToString("dd-MM-yyyy") + ".zip"))
                File.Delete(Server.MapPath("~/Archivos/BBVA/" + lblNombreArchivo_Layout.Text.Trim() + DateTime.Now.ToString("dd-MM-yyyy") + ".zip"));

            using (ZipFile zip = new ZipFile())
            {
                for (int i = 1; i <= numArchivo; i++)
                {
                    if (File.Exists(Server.MapPath("~/Archivos/BBVA/") + lblNombreArchivo_Layout.Text.Trim() + i + ".txt"))
                        zip.AddFile(Server.MapPath("~/Archivos/BBVA/") + lblNombreArchivo_Layout.Text.Trim() + i + ".txt", @"\");
                    else
                        pnlDescargaError_Layout.Visible = true;
                }

                zip.Save(Server.MapPath("~/Archivos/BBVA/") + lblNombreArchivo_Layout.Text.Trim() + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");


                //eliminar archivos creados para descargar
                for (int j = 1; j <= numArchivo; j++)
                {
                    File.Delete(Server.MapPath("~/Archivos/BBVA/" + lblNombreArchivo_Layout.Text.Trim() + j + ".txt"));
                }
            }

        }

        CargarHistorialDomiciliadoLayoutBBVA();

        string NombreArchivo_Layout = lblNombreArchivo_Layout.Text.Trim() + DateTime.Now.ToString("dd-MM-yyyy") + ".zip";

        if (File.Exists(Server.MapPath("~/Archivos/BBVA/") + NombreArchivo_Layout))
        {
            Response.Redirect(string.Format("~/DownloadFileZ.ashx?fileName={0}", Server.MapPath("~/Archivos/BBVA/") + NombreArchivo_Layout));
        }
        else
        {
            pnlDescargaError_Layout.Visible = true;
        }
    }   

    protected void gvArchivosLayout_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "VerDetalle":
                using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
                {
                    wsSOPF.ArchivoMasivo archivo = ws.DomiciliacionArchivo_ObtenerDetalle(int.Parse(e.CommandArgument.ToString()));

                    lblNombreArchivo_Layout.Text = archivo.Nombre;
                    lblTipoArchivo_Layout.Text = archivo.TipoArchivo.Descripcion;
                    lblFechaCarga_Layout.Text = archivo.FechaRegistro.GetValueOrDefault().ToString();

                    pnlLayoutGV.Visible = false;
                    pnlLayoutDetalle.Visible = true;
                    List<wsSOPF.HistorialCobros> archivo2 = ws.ObtenerHistorialSeguimientoDomiBBVA(int.Parse(e.CommandArgument.ToString())).ToList();

                    gvHistoriaDomiciliado_Layout.DataSource = archivo2;
                    gvHistoriaDomiciliado_Layout.DataBind();
                }
                break;
        }
    }

    protected void gvArchivosLayout_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvArchivosLayout.PageIndex = e.NewPageIndex;
        CargarArchivosLayout();
    }

    protected void gvHistoriaDomiciliado_Layout_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvHistoriaDomiciliado_Layout.PageIndex = e.NewPageIndex;
    }

    #endregion

    #region "Archivos con Error"

    protected void lbtnRegresar_Error_Click(object sender, EventArgs e)
    {
        pnlErrorGV.Visible = true;
        pnlErrorDetalle.Visible = false;
        pnlDescargaError_Error.Visible = false;
    }

    protected void lbtnDescargar_Error_Click(object sender, EventArgs e)
    {
        if (File.Exists(Server.MapPath("~/Archivos/PagosDirectos/") + lblNombreArchivo_Error.Text.Trim()))
            Response.Redirect(string.Format("~/DownloadFile.ashx?fileName={0}", Server.MapPath("~/Archivos/DomiciliacionCargaMasiva/") + lblNombreArchivo_Error.Text.Trim()));
        else
            pnlDescargaError_Error.Visible = true;
    }

    protected void gvArchivosError_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "VerDetalle":
                using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
                {
                    wsSOPF.ArchivoMasivo archivo = ws.PagosDirectosArchivo_ObtenerDetalle(int.Parse(e.CommandArgument.ToString()));

                    lblNombreArchivo_Error.Text = archivo.Nombre;
                    lblTipoArchivo_Error.Text = archivo.TipoArchivo.Descripcion;
                    lblFechaRegistro_Error.Text = archivo.FechaRegistro.GetValueOrDefault().ToString();

                    lblDetalleArchivo_Error.Text = string.Empty;
                    foreach (wsSOPF.ArchivoMasivoDetalle d in archivo.Detalle)
                    {
                        lblDetalleArchivo_Error.Text += d.Descripcion.Replace(" ", "&nbsp;") + "<br/>";
                    }

                    pnlErrorGV.Visible = false;
                    pnlErrorDetalle.Visible = true;
                }
                break;
        }
    }

    protected void gvArchivosError_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvArchivosError.PageIndex = e.NewPageIndex;
        CargarArchivosError();
    }

    #endregion

    #endregion

    private void ProcesarCSV()
    {
        string folderPath = Server.MapPath("~/Archivos/BBVA/");
        List<PagosDirectosExcelResumen> PagosCSV = new List<PagosDirectosExcelResumen>();
        PagosDirectosExcelResumen pagoCSV = null;

        using (var reader = new StreamReader(string.Format("{0}{1}", folderPath, "TMP_" + this.NombreArchivo)))
        {
            string errLines = string.Empty;
            int lineNum = 0;
            int IdSolicitud = 0;
            decimal Monto = 0;
            DateTime FechaPago;

            while (!reader.EndOfStream)
            {
                try
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    // Ignorar el encabezado
                    if (lineNum > 0)
                    {
                        pagoCSV = new PagosDirectosExcelResumen();

                        // Validar los tipos de dato antes de agregarlos como renglon Valido
                        if (!int.TryParse(values[1].Trim(), out IdSolicitud) ||
                            !decimal.TryParse(values[2].Trim(), out Monto) ||
                            !DateTime.TryParseExact(values[3].Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out FechaPago))
                            throw new Exception(lineNum.ToString());

                        pagoCSV.NoPago = lineNum;
                        pagoCSV.TipoPago = values[0];
                        pagoCSV.IdSolicitud = int.Parse(values[1]);
                        pagoCSV.Monto = decimal.Parse(values[2]);
                        pagoCSV.FechaPago = DateTime.ParseExact(values[3], "dd/MM/yyyy", null);
                        pagoCSV.Observaciones = (values[4] != null) ? values[4] : string.Empty;

                        PagosCSV.Add(pagoCSV);
                    }
                }
                catch (Exception ex)
                {
                    errLines += ex.Message + ", ";
                }

                lineNum++;
            }

            // Mostrar el Resumen del archivo CSV Procesado
            gvResumen_CargarArchivo.DataSource = PagosCSV;
            gvResumen_CargarArchivo.DataBind();

            if (!string.IsNullOrEmpty(errLines))
            {
                pnlAdvertencia_CargarArchivo.Visible = true;
                lblAdvertencia_CargarArchivo.Text = string.Format("Las siguientes pagos presentan formato incorrecto en su información: {0}", errLines.Remove(errLines.LastIndexOf(","), 1).Trim());
            }
        }
    }

    public bool CsvToXml()
    {
        string folderPath = Server.MapPath("~/Archivos/BBVA/");
        var success = false;

        var fileExists = File.Exists(folderPath + "TMP_" + Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".csv");

        if (!fileExists)
        {
            return success;
        }

        var formatedLines = LoadCsv(folderPath + "TMP_" + Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".csv");
        var headers = formatedLines[0].Split(',').Select(x => x.Trim('\"').Replace(" ", string.Empty)).ToArray();

        var xml = new XElement("ARCH",
           formatedLines.Where((line, index) => index > 0).
               Select(line => new XElement("REG",
                  line.Split(',').Select((field, index) => new XElement(headers[index], field)))));

        try
        {
            xml.Save(folderPath + Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".XML");

            success = true;
        }
        catch (Exception ex)
        {
            success = false;

            var baseException = ex.GetBaseException();
            Debug.Write(baseException.Message);
        }

        File.Delete(folderPath + "TMP_" + this.NombreArchivo);
        return success;
    }

    public bool CsvToXml2(string nombreArchivo)
    {
        string folderPath = Server.MapPath("~/Archivos/BBVA/");
        var success = false;

        var fileExists = File.Exists(folderPath + "TMP_" + Path.GetFileNameWithoutExtension(nombreArchivo) + ".csv");

        if (!fileExists)
        {
            return success;
        }

        var formatedLines = LoadCsv(folderPath + "TMP_" + Path.GetFileNameWithoutExtension(nombreArchivo) + ".csv");
        var headers = formatedLines[0].Split(',').Select(x => x.Trim('\"').Replace(" ", string.Empty)).ToArray();

        var xml = new XElement("ARCH",
           formatedLines.Where((line, index) => index > 0).
               Select(line => new XElement("REG",
                  line.Split(',').Select((field, index) => new XElement(headers[index], field)))));

        try
        {
            xml.Save(folderPath + Path.GetFileNameWithoutExtension(nombreArchivo) + ".XML");

            success = true;
        }
        catch (Exception ex)
        {
            success = false;

            var baseException = ex.GetBaseException();
            Debug.Write(baseException.Message);
        }

        File.Delete(folderPath + "TMP_" + nombreArchivo);
        return success;
    }

    private List<string> LoadCsv(string sourcePath)
    {
        var lines = File.ReadAllLines(sourcePath).ToList();

        var formatedLines = new List<string>();

        foreach (var line in lines)
        {
            var formatedLine = line.TrimEnd(',');
            formatedLines.Add(formatedLine);
        }
        return formatedLines;
    }

    // Guardar en BD el contenido dela rchivo
    private void GuardarArchivo()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {

            string folderPath = Server.MapPath("~/Archivos/BBVA/");

            // Copiamos el archivo al servidor de BD
            string localURL = folderPath + Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".xml";
            string remoteURL = System.Configuration.ConfigurationManager.AppSettings["URLRedArchivoMasivo"];
            string remoteUser = System.Configuration.ConfigurationManager.AppSettings["UsuarioArchivoMasivo"];
            string remotePass = System.Configuration.ConfigurationManager.AppSettings["ClaveArchivoMasivo"];

            NetworkShare.DisconnectFromShare(remoteURL, true); // Nos desconectamos en caso de que estemos actualmente conectados

            NetworkShare.ConnectToShare(remoteURL, remoteUser, remotePass); // Nos conectamos con las nuevas credenciales

            File.Copy(localURL, remoteURL + Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".xml", true);

            NetworkShare.DisconnectFromShare(remoteURL, false); // Nos desconectamos del server

            // Procesar el archivo
            wsSOPF.ArchivoMasivo gEntity = new wsSOPF.ArchivoMasivo();
            gEntity.Nombre = Path.GetFileNameWithoutExtension(this.NombreArchivo) + ".xml";

            wsSOPF.ResultadoOfboolean res = ws.EnvioDomiBBVA_Registrar(gEntity);

            if (res.Codigo != 0)
                throw new Exception(res.Mensaje);
        }
    }

    private void CargarArchivosAprobados()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.ArchivoMasivo> archivosProcesados = ws.ObtenerArchivos_SubidosBBVA().ToList();

            gvArchivosAprobados.DataSource = archivosProcesados;
            gvArchivosAprobados.DataBind();
        }
    }

    private void CargarHistorialDomiciliadoBBVA()
    {
        wsSOPF.TesoreriaClient ws1 = new wsSOPF.TesoreriaClient();
        List<wsSOPF.FitrarRespuestaBBVA> filtrar = ws1.FiltrarRespuestaDomiBBVA(Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim()) + ".XML").ToList();
        if (filtrar.Count > 0)
        {
            //DataRow row = dt.Rows[0];

            int Idarchivo = Convert.ToInt32(filtrar[0].idArchivo.ToString());
            using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
            {

                List<wsSOPF.HistorialCobros> archivo2 = ws.ObtenerHistorialSeguimientoDomiBBVA(Idarchivo).ToList();

                gvHistoriaDomiciliado.DataSource = archivo2;
                gvHistoriaDomiciliado.DataBind();
            }
        }
    }

    private void CargarArchivosLayout()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.ArchivoMasivo> archivosLayout = ws.ObtenerArchivos_GeneradosBBVA().ToList();

            gvArchivosLayout.DataSource = archivosLayout;
            gvArchivosLayout.DataBind();
        }
    }

    private void CargarHistorialDomiciliadoLayoutBBVA()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.FitrarRespuestaBBVA> filtrar = ws.FiltrarRespuestaDomiBBVA(Path.GetFileNameWithoutExtension(lblNombreArchivo_Aprobados.Text.Trim())).ToList();

            if (filtrar.Count > 0)
            {
                int Idarchivo = Convert.ToInt32(filtrar[0].idArchivo.ToString());
                List<wsSOPF.HistorialCobros> archivo2 = ws.ObtenerHistorialSeguimientoDomiBBVA(Idarchivo).ToList();

                gvHistoriaDomiciliado_Layout.DataSource = archivo2;
                gvHistoriaDomiciliado_Layout.DataBind();
            }
        }
    }

    private string GenerarCodigo()
    {
        int maxLenght = 5;
        var obj = new Random();
        const string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        var longitud = allowedChars.Length;
        var res = "";

        for (int i = 0; i < maxLenght; i++)
        {
            res += allowedChars[obj.Next(longitud)];
        }
        return res;
    }

    private void CargarArchivosError()
    {
        using (wsSOPF.TesoreriaClient ws = new wsSOPF.TesoreriaClient())
        {
            List<wsSOPF.ArchivoMasivo> archivosError = ws.PagosDirectosArchivo_ObtenerConError().ToList();

            gvArchivosError.DataSource = archivosError;
            gvArchivosError.DataBind();
        }
    }

    private class PagosDirectosExcelResumen
    {
        public int NoPago { get; set; }
        public string TipoPago { get; set; }
        public int IdSolicitud { get; set; }
        public decimal Monto { get; set; }
        public DateTime FechaPago { get; set; }
        public string Observaciones { get; set; }
    }

    private static List<string> ReadTextFile(string fileName)
    {
        if (File.Exists(fileName))
        {
            List<string> lines = new List<string>();
            string line = string.Empty;

            using (StreamReader reader = new StreamReader(fileName))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }
            return lines;
        }
        return null;
    }
}

