﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmTablaAmortizacion.aspx.cs"
    Inherits="Creditos_frmTablaAmortizacion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cronograma de Pagos</title>
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <style type="text/css">
        #form1 {
            overflow-x: hidden;
        }

        #btnEnviar > span, #btnDescargar > span {
            line-height: 1.42857143 !important;
        }

        #dAcciones {
            margin-bottom: 5px;
        }
    </style>

    <link href="/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="dAcciones" class="row">
            <div id="dDescarga" class="col-md-2">
                <button type="button" id="btnDescargar" class="btn btn-primary">
                    <span class="glyphicon glyphicon-save">&nbsp;Pdf</span>
                </button>
            </div>
            <div id="dEnviar" class="col-md-6">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Correo Cte:</label>
                        <div class="col-sm-9">
                            <input type="text" id="txCorreoCte" class="form-control" value="" disabled="disabled" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cc a:</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <input type="text" id="txCorreoCC" class="form-control" placeholder="copiar al correo" />
                                <span class="input-group-btn">
                                    <button id="btnEnviar" type="button" class="btn btn-success" title="Enviar">
                                        <span class="glyphicon glyphicon-envelope"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:GridView runat="server" ID="gvRecibos" DataKeyNames="IdCuenta" Width="100%"
            AutoGenerateColumns="False" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow"
            AlternatingRowStyle-CssClass="GridRowAlternate" CssClass="Grid table table-hover" EmptyDataRowStyle-CssClass="GridEmptyData"
            GridLines="None" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false">
            <Columns>
                <asp:BoundField HeaderText="#" DataField="Recibo" ItemStyle-CssClass="small" />
                <asp:BoundField HeaderText="Fecha" DataField="FchRecibo" DataFormatString="{0:d}" ItemStyle-CssClass="small" />
                <asp:BoundField HeaderText="Cap. Ini." DataField="CapitalInicial" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                <asp:BoundField HeaderText="S. Capital" DataField="SaldoCapital" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                <asp:BoundField HeaderText="S. Interes" DataField="SaldoInteres" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                <asp:BoundField HeaderText="S. IGV" DataField="SaldoIGV" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                <asp:BoundField HeaderText="S. Seguro" DataField="SaldoSeguro" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                <asp:BoundField HeaderText="S. GAT" DataField="SaldoGAT" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                <asp:BoundField HeaderText="Estatus" DataField="Estatus" ItemStyle-CssClass="small" />
                <asp:BoundField HeaderText="Saldo" DataField="SaldoRecibo" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                <asp:BoundField HeaderText="Total" DataField="TotalRecibo" DataFormatString="{0:c2}" ItemStyle-CssClass="info small" />
                <asp:BoundField HeaderText="Cap. Ins." DataField="CapitalInsoluto" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
            </Columns>
            <EmptyDataTemplate>
                No se encontraron registros.
            </EmptyDataTemplate>
        </asp:GridView>

        <div class="panel-body">
            <div class="col-sm-12">
                <blockquote style="background-color: #F8F8F8;">
                    <p class="text-center">Cronograma de Pagos</p>
                </blockquote>
            </div>

            <div class="col-sm-9 col-print-9">
                <label class="small">Cliente</label>
                <pre><asp:Label ID="lblCliente_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-3 col-print-3">
                <label class="small">DNI</label>
                <pre><asp:Label ID="lblDNI_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-12 col-print-12">
                <label class="small">Dirección</label>
                <pre><asp:Label ID="lblDireccionCliente_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-3 col-print-3">
                <label class="small">Monto Credito</label>
                <pre><asp:Label ID="lblMontoDesembolsado_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-3 col-print-3">
                <label class="small">Tasa Efectiva Anual</label>
                <pre><asp:Label ID="lblTEA_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-3 col-print-3">
                <label class="small">Tasa Costo Efectiva Anual</label>
                <pre><asp:Label ID="lblTCEA_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-3 col-print-3">
                <label class="small">Total Interes</label>
                <pre><asp:Label ID="lblTotalInteres_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-3 col-print-3">
                <label class="small">Numero Cuotas</label>
                <pre><asp:Label ID="lblNumeroCuotas_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-3 col-print-3">
                <label class="small">Fecha 1er. Vcto.</label>
                <pre><asp:Label ID="lblFechaPrimerPago_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-3 col-print-3">
                <label class="small">Fecha Ult. Vcto.</label>
                <pre><asp:Label ID="lblFechaUltimoPago_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-3 col-print-3">
                <label class="small">Fecha Desembolso</label>
                <pre><asp:Label ID="lblFechaDesembolso_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-3 col-print-3">
                <label class="small">Monto Comisión uso de canal</label>
                <pre><asp:Label ID="lblMontoComision_Cronograma" runat="server"></asp:Label></pre>
            </div>

            <div class="col-sm-12 col-print-12">
                <hr />
                <div style="height: auto; width: auto; overflow-x: auto;">
                    <asp:GridView ID="gvRecibos_Cronograma" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="small"
                        CssClass="table table-bordered table-striped table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData">
                        <Columns>
                            <asp:BoundField HeaderText="N° Cuota" DataField="NoRecibo" />
                            <asp:BoundField HeaderText="Fecha" DataField="FechaRecibo" DataFormatString="{0:d}" />
                            <asp:BoundField HeaderText="Capital Inicial" DataField="CapitalInicial" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="Capital Amortizado" DataField="Capital" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="Interes" DataField="Interes" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="IGV" DataField="IGV" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="Seguro" DataField="Seguro" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="GAT" DataField="GAT" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="Comisión" DataField="OtrosCargos" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="IGV Comisión" DataField="IGVOtrosCargos" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="Saldo Capital" DataField="SaldoCapital" DataFormatString="{0:c2}" />
                            <asp:BoundField HeaderText="Monto/Cuota" DataField="Cuota" DataFormatString="{0:c2}" />
                        </Columns>
                        <EmptyDataTemplate>
                            Hubo un problema al cargar los pagos del cronograma
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
