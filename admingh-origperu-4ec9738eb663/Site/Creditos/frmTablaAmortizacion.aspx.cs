﻿using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Creditos_frmTablaAmortizacion : System.Web.UI.Page
{
    static readonly string[] origenForm = { "Gestion.aspx", "lstDashboardCreditos.aspx" };

    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder strScript = new StringBuilder();

        int idSolicitud = 0;
        if (!IsPostBack)
        {
            if (Request.QueryString["idSolicitud"] != null)
            {
                int.TryParse(Request.QueryString["idSolicitud"].ToString(), out idSolicitud);
            }

            int idUsuario = Session.UsuarioActual();

            if (idSolicitud <= 0 && idUsuario <= 0)
                Response.End();

            #region Asignacion de Permisos
            ValidaRecursoAccesoResponse validaRec;
            using (SistemaClient wsS = new SistemaClient())
            {
                string recurso = Request.Url.AbsolutePath;
                recurso = recurso.Substring(recurso.IndexOf("/") + 1, recurso.Length - 1);
                validaRec = wsS.ValidaRecursoAcceso(new ValidaRecursoAccesoRequest
                {
                    IdUsuario = idUsuario,
                    Recurso = recurso
                });
            }
            if (validaRec != null && validaRec.RecursoAcciones != null && validaRec.RecursoAcciones.Length > 0)
            {
                if (!validaRec.RecursoAcciones.Any(ac => ac.Accion == "EnviarCronograma"))
                {
                    strScript.AppendLine("$(\"#dDescarga\").remove();");
                }
                else
                {
                    strScript.AppendLine(Utilidades.CargarCotenido(Server.MapPath("js/cronogramapagos/descargar.js")));
                }

                if (!validaRec.RecursoAcciones.Any(ac => ac.Accion == "DescargarCronograma"))
                {
                    strScript.AppendLine("$(\"#dEnviar\").remove();");
                }
                else
                {
                    strScript.AppendLine(Utilidades.CargarCotenido(Server.MapPath("js/cronogramapagos/enviarcorreo.js")));
                    using (ClientesClient wsC = new ClientesClient())
                    {
                        BuscarClienteResponse resp = wsC.BuscarCliente(new BuscarClienteRequest { IdUsuario = idUsuario, IdSolicitud = idSolicitud });
                        if (resp != null && resp.Cliente != null)
                        {
                            strScript.AppendFormat("correocte={0};", JsonConvert.SerializeObject(resp.Cliente.Email));
                        }
                    }
                }
                strScript.AppendLine(Utilidades.CargarCotenido(Server.MapPath("js/cronogramapagos/cronogramapagos.js")));
            }
            else
            {
                strScript.AppendLine("$(\"#dDescarga\").remove();");
                strScript.AppendLine("$(\"#dEnviar\").remove();");
            }
            #endregion

            #region Cronograma
            using (wsSOPF.SolicitudClient ws = new SolicitudClient())
            {
                CronogramaPagos cronograma = ws.Solicitudes_ObtenerCronograma(idSolicitud, null);

                lblDNI_Cronograma.Text = cronograma.DNICliente;
                lblCliente_Cronograma.Text = string.Format("{0} {1} {2}", cronograma.NombreCliente, cronograma.ApPaternoCliente, cronograma.ApMaternoCliente);
                lblDireccionCliente_Cronograma.Text = cronograma.DireccionCliente;
                lblMontoDesembolsado_Cronograma.Text = cronograma.MontoDesembolsar.ToString("C", CultureInfo.CurrentCulture);
                lblTEA_Cronograma.Text = string.Format("{0:P2}", cronograma.TasaAnual / 100);
                lblTCEA_Cronograma.Text = string.Format("{0:P2}", cronograma.TasaCostoAnual / 100);
                lblTotalInteres_Cronograma.Text = cronograma.TotalInteres.ToString("C", CultureInfo.CurrentCulture);
                lblNumeroCuotas_Cronograma.Text = cronograma.Plazo.ToString();
                lblFechaDesembolso_Cronograma.Text = cronograma.FechaDesembolso.ToString("dd/MM/yyyy");
                lblFechaPrimerPago_Cronograma.Text = cronograma.PrimerFechaPago.ToString("dd/MM/yyyy");
                lblFechaUltimoPago_Cronograma.Text = cronograma.UltimaFechaPago.ToString("dd/MM/yyyy");

                if (cronograma.Recibos.Any(r => r.NoRecibo == "-"))
                {
                    lblMontoComision_Cronograma.Text = cronograma.Recibos.FirstOrDefault(r => r.NoRecibo == "-").OtrosCargos.ToString("C", CultureInfo.CurrentCulture);
                }
                else
                {
                    lblMontoComision_Cronograma.Text = "S/0.00";
                }

                // Si el Monto del Seguro es 0, ocultamos la columna            
                ((DataControlField)gvRecibos_Cronograma.Columns
                   .Cast<DataControlField>()
                   .Where(fld => (fld.HeaderText == "Seguro"))
                   .SingleOrDefault()).Visible = (cronograma.Recibos.Sum(x => x.Seguro) > 0);

                gvRecibos_Cronograma.DataSource = cronograma.Recibos.Where(r => r.NoRecibo != "00");
                gvRecibos_Cronograma.DataBind();
            }
            #endregion

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
        }
    }

    #region WebMethods
    [WebMethod(EnableSession = true)]
    public static object EnviarCronograma(string correoCc, string origen)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            int idSolicitud;
            int.TryParse(HttpContext.Current.QueryString("idSolicitud"), out idSolicitud);
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }
            if (idSolicitud <= 0) throw new Exception("El número de cuenta no es válido.");
            if (!origenForm.Contains(origen)) throw new Exception("No es posible realizar la petición desde este sitio.");

            using (SolicitudClient wsS = new SolicitudClient())
            {
                EnviarCronogramaRequest nuevaNotif = new EnviarCronogramaRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitud = idSolicitud
                };
                switch (origen)
                {
                    case "Gestion.aspx":
                        nuevaNotif.Origen = OrigenEnvioCronograma.Gestion;
                        break;
                    case "lstDashboardCreditos.aspx":
                        nuevaNotif.Origen = OrigenEnvioCronograma.Creditos;
                        break;
                }
                if (!string.IsNullOrEmpty(correoCc)) nuevaNotif.CopiaCC = correoCc;
                EnviarCronogramaResponse respEnvioCron = wsS.EnviarCronograma(nuevaNotif);
                if (!respEnvioCron.Error) mensaje = "Se ha solicitado el envio del cronograma correctamente!";
                else mensaje = respEnvioCron.MensajeOperacion;
            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url };
    }

    [WebMethod(EnableSession = true)]
    public static object Descargar(int idSolicitud)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object documento = null;

        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }
            if (idSolicitud <= 0) throw new Exception("El número de cuenta no es válido.");

            using (SolicitudClient wsS = new SolicitudClient())
            {
                ImpresionDocumentoResponse impresionResp = wsS.ImpresionTablaAmortizacion(new ImpresionTablaAmortizacionRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitud = idSolicitud,
                    EsReimpresion = true
                });

                error = impresionResp.Error;
                mensaje = impresionResp.MensajeOperacion;
                if (impresionResp.Resultado != null && impresionResp.Resultado.ContenidoArchivo != null
                    && impresionResp.Resultado.ContenidoArchivo.Length > 0)
                {
                    documento = new
                    {
                        impresionResp.Resultado.NombreArchivo,
                        ContenidoArchivo = Convert.ToBase64String(impresionResp.Resultado.ContenidoArchivo),
                        impresionResp.Resultado.TipoContenido
                    };
                }
            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url, documento };
    }
    #endregion
}