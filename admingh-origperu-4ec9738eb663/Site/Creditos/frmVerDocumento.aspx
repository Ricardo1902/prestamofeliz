﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmVerDocumento.aspx.cs"
    Inherits="Creditos_frmVerDocumento" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ver Documento</title>
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <style type="text/css">
        #form1 {
            overflow-x: hidden;
        }

        #btnEnviar > span, #btnDescargar > span {
            line-height: 1.42857143 !important;
        }

        #dAcciones {
            margin-bottom: 5px;
        }
    </style>

    <link href="/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css" />    
</head>
<body>
    <form id="form1" runat="server">
        <div class="row">           
            <asp:Panel ID="pnlPDF" CssClass="col-xs-12" runat="server">                
            </asp:Panel>
        </div>                
    </form>
</body>
</html>
