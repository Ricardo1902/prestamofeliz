﻿using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Creditos_frmVerDocumento : System.Web.UI.Page
{
    static readonly string[] origenForm = { "lstDashboardCreditos.aspx" };

    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder strScript = new StringBuilder();

        string IdGoogleDrive = string.Empty;
        if (!IsPostBack)
        {
            if (Request.QueryString["GDriveID"] != null)
            {
                IdGoogleDrive = Request.QueryString["GDriveID"].ToString();
            }

            int idUsuario = Session.UsuarioActual();

            if (string.IsNullOrEmpty(IdGoogleDrive) || idUsuario <= 0)
                Response.End();

            // Ver documento de Google Drive recibido.
            VerDocumentoGoogleDrive(IdGoogleDrive);
        }
    }

    protected void VerDocumentoGoogleDrive(string IdGoogleDrive)
    {
        DescargarArchivoGDResponse rs = new DescargarArchivoGDResponse();

        using (wsSOPF.SolicitudClient ws = new SolicitudClient())
        {
            DescargarArchivoGDRequest rq = new DescargarArchivoGDRequest();
            rq.IdArchivoGD = IdGoogleDrive;
            rq.RegresaURL = false;
            rq.IdUsuario = Session.UsuarioActual();

            rs = ws.DescargaDocumentoGDrive(rq);
            
            if (rs.Error)
                throw new Exception(rs.MensajeErrorException);        
        }

        Response.Clear();
        
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "filename=" + rs.ArchivoGoogle.NombreArchivo);
        Response.Buffer = true;
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.BinaryWrite(rs.ArchivoGoogle.Contenido);
        Response.End();
        Response.Close();
    }

    [WebMethod(EnableSession = true)]
    public static object Descargar(int idSolicitud)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object documento = null;

        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }
            if (idSolicitud <= 0) throw new Exception("El número de cuenta no es válido.");

            using (SolicitudClient wsS = new SolicitudClient())
            {
                ImpresionDocumentoResponse impresionResp = wsS.ImpresionTablaAmortizacion(new ImpresionTablaAmortizacionRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitud = idSolicitud,
                    EsReimpresion = true
                });

                error = impresionResp.Error;
                mensaje = impresionResp.MensajeOperacion;
                if (impresionResp.Resultado != null && impresionResp.Resultado.ContenidoArchivo != null
                    && impresionResp.Resultado.ContenidoArchivo.Length > 0)
                {
                    documento = new
                    {
                        impresionResp.Resultado.NombreArchivo,
                        ContenidoArchivo = Convert.ToBase64String(impresionResp.Resultado.ContenidoArchivo),
                        impresionResp.Resultado.TipoContenido
                    };
                }
            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url, documento };
    }    
}