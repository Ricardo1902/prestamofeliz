if (typeof correocte !== "undefined") {
    $("#txCorreoCte").val(correocte);
}
function validarCorreo(correo) {
    const patron = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return patron.test(String(correo).toLowerCase());
}
function origen() {
    let path = parent.location.pathname;
    try {
        path = path.split('/');
        path = path[path.length - 1];
    } catch (e) {
        path = "";
    }
    return path;
}