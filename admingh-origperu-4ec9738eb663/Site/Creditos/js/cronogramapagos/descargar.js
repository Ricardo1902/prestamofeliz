﻿$("#btnDescargar").click(() => {
    let qs = new URLSearchParams(window.location.search);
    let idSolicitud = qs.has("idSolicitud") ? qs.get("idSolicitud") : 0;
    let parametros = {
        url: "frmTablaAmortizacion.aspx/Descargar" + window.location.search,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ idSolicitud })
    };
    $.ajax(parametros)
        .done(function (respuesta) {
            let resp = respuesta.d;
            if (resp != undefined) {
                if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                if (resp.documento != null) {
                    var link = document.createElement('a');
                    link.download = resp.documento.NombreArchivo;
                    link.href = `data:${resp.documento.TipoContenido};base64, ${resp.documento.ContenidoArchivo}`;
                    document.body.appendChild(link);
                    link.click();
                    link.remove()
                } else { alert("No fue posible obtener el cronograma. Vuelva a intentar."); }
            } else {
                alert("Ocurrió un error al momento de realizar la petición de descarga.");
            }
        })
        .fail(function (error) {
            alert("Ocurrió un error al momento intentar realizar la descarga del cronograma. Vuela a intentar.");
        });
});