﻿$("#btnEnviar").click(function () {
    if ($("#txCorreoCC").val() != "" && !validarCorreo($("#txCorreoCC").val())) { alert("Verifique que ha ingresado la cuenta de correo correctamente."); return; }
    if ($("#txCorreoCC").val() == "") if (!confirm("¿Solo se enviará el cronograma a la cuenta de correo del cliente, desea continuar?")) return;
    let parametros = {
        url: "frmTablaAmortizacion.aspx/EnviarCronograma" + window.location.search,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ correoCc: $("#txCorreoCC").val(), origen: origen() })
    };
    $.ajax(parametros)
        .done(function (respuesta) {
            let resp = respuesta.d;
            if (resp != undefined) {
                if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                else alert("La acción se realizó correctamente!");
                if (!resp.error) {
                    $("#txCorreoCC").val("");
                }
            } else {
                alert("Ocurrió un error al momento de realizar la petición de envio del cronograma");
            }
        })
        .fail(function (error) {
            alert("Ocurrió un error al momento intentar realizar el envio del cronograma. Vuela a intentar.");
        });
});