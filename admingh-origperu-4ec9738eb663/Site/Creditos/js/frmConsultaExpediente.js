﻿function verExpediente(GDriveID, IdSolicitud) {    
    if (GDriveID == null) return;

    let id = Date.now();
    let tabla = `
    <div id=${id} class="modal fade" role="dialog" style="z-index:99999; padding-top:0px;">
        <div class="modal-dialog modal-lg modal-dialog" role="alertdialog" style="width:100%;">
            <div class="modal-content" style="height:95%">
                <div class="modal-header">
                    <label>Expediente ${IdSolicitud}</label>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>                    
                </div>
                <div class="modal-body" style="height:90%">
                    <iframe src="../Creditos/frmVerDocumento.aspx?GDriveID=${GDriveID}" frameborder="0" style="display:block; width:100%; height:82vh; top:0; left:0; bottom:0; right:0;" height="100%" width="100%">
                    </iframe>
                </div>                
            </div>
        </div>
    </div>`;

    $(tabla).modal('show')
        .on("shown.bs.modal", function (e) {
        })
        .on("hidden.bs.modal", function (e) {
            $(`#${id}`).remove();
        });
}