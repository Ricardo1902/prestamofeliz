﻿function cronogramaPagos(idSolicitud) {
    if (idSolicitud == null || idSolicitud <= 0) return;
    let id = Date.now()
    let tabla = `
    <div id=${id} class="modal fade" role="dialog" style="z-index:99999">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="alertdialog" style="width:82%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cronograma de pagos</h4>
                </div>
                <div class="modal-body">
                    <iframe src="../Creditos/frmTablaAmortizacion.aspx?idSolicitud=${idSolicitud}" style="border:none;width:100%;height:500px;overflow-x: hidden; overflow-y: scroll">
                    </iframe>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cerrar" />
                </div>
            </div>
        </div>
    </div>`;
    $(tabla).modal('show')
        .on("shown.bs.modal", function (e) {
        })
        .on("hidden.bs.modal", function (e) {
            $(`#${id}`).remove();
        });
}