﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="lstDashboardCreditos.aspx.cs" Inherits="Site_Creditos_lstDashboardCreditos" MasterPageFile="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link type="text/css" href="css/lstdashboardcreditos.css" rel="Stylesheet" />
    <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
        <ProgressTemplate>
            <div id="loader-background"></div>
            <div id="loader-content"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlBuscar" Visible="true" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Seguimiento de Creditos</h3>
                    </div>
                    <div class="panel-body">
                        <asp:Panel ID="pnlExito_Buscar" class="col-sm-12" Visible="false" runat="server">
                            <div class="alert alert-success" style="padding: 5px 15px; margin: 5px 0px 0px 0px;">
                                <i class="far fa-check-circle"></i>
                                <asp:Label ID="lblExito_Buscar" runat="server"></asp:Label>
                            </div>
                            <hr />
                        </asp:Panel>

                        <div class="form-group col-md-3">
                            <label class="small">Solicitudes</label>
                            <asp:DropDownList ID="cmbPertenece" runat="server" CssClass="form-control" AutoPostBack="true"
                                OnSelectedIndexChanged="cmbPertenece_SelectedIndexChanged">
                                <asp:ListItem Text="Solo Mias" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Todas" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="small">Promotor</label>
                            <asp:DropDownList ID="cmbPromotor" runat="server" CssClass="form-control" AutoPostBack="true"
                                OnSelectedIndexChanged="cmbPromotor_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>

                        <div class="form-group col-md-3">
                            <label class="small">Sucursal</label>
                            <asp:DropDownList ID="cmbSucursal" runat="server" CssClass="form-control" AutoPostBack="true"
                                OnSelectedIndexChanged="cmbSucursal_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>

                        <div class="col-md-3">
                            <label class="small">Estatus</label>
                            <asp:DropDownList ID="cmbEstatus" runat="server" CssClass="form-control" AutoPostBack="true"
                                OnSelectedIndexChanged="cmbEstatus_SelectedIndexChanged">
                                <asp:ListItem Text="Todos" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Autorizado" Value="10"></asp:ListItem>
                                <asp:ListItem Text="Dispersado" Value="11"></asp:ListItem>
                                <asp:ListItem Text="Saldado por Liquidación" Value="8"></asp:ListItem>
                                <asp:ListItem Text="Liquidacion por Ampliacion" Value="16"></asp:ListItem>
                                <asp:ListItem Text="Liquidacion por Defuncion" Value="17"></asp:ListItem>
                                <asp:ListItem Text="Liquidación Reestructura" Value="20"></asp:ListItem>
                                <asp:ListItem Text="Liquidación Reprogramación" Value="21"></asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="small">Búsqueda</label>
                            <asp:TextBox ID="busqueda" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="form-group col-sm-12">
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-sm btn-primary" OnClick="btnBuscar_Click" />
                        </div>

                        <div class="form-group col-sm-12">
                            <div style="height: auto; width: auto; overflow-x: auto;">
                                <asp:GridView ID="gvCreditos" runat="server" Width="100%" AutoGenerateColumns="False"
                                    Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                    CssClass="table table-bordered table-striped  table-hover table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                    OnPageIndexChanging="gvCreditos_PageIndexChanging" OnRowDataBound="gvCreditos_RowDataBound" OnRowCommand="gvCreditos_RowCommand" PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                    DataKeyNames="IdSolicitud" Style="overflow-x: auto;">
                                    <PagerStyle CssClass="pagination-ty" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnVerCronograma" CssClass="btn btn-xs" data-toggle="tooltip" title="Ver Cronograma" Visible="true" runat="server"
                                                    OnClientClick='<%# "cronogramaPagos(" + Eval("IdSolicitud")  + ");return false;" %>'>
                                                <i class="far fa-calendar-alt fa-lg" style="color: #888888;"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbtnLiquidacion" CommandName="VerLiquidacion" CommandArgument='<%# Eval("IdSolicitud") %>' CssClass="btn btn-xs" data-toggle="tooltip" title="Liquidacion" Visible="true" runat="server">
                                                <i class="fas fa-file-invoice-dollar fa-lg" style="color: #888888;"></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Solicitud" DataField="IdSolicitud" />
                                        <asp:BoundField HeaderText="Fecha Credito" DataField="FchCredito" DataFormatString="{0:d}" />
                                        <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                        <asp:BoundField HeaderText="Capital" DataField="capital" DataFormatString="{0:c2}" />
                                        <asp:BoundField HeaderText="Estatus" DataField="Estatus" />
                                        <asp:BoundField HeaderText="Producto" DataField="Producto" />
                                        <asp:BoundField HeaderText="Vendedor" DataField="Promotor" />
                                        <asp:BoundField HeaderText="Sucursal" DataField="Sucursal" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No se encontraron Creditos con los filtros seleccionados
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlLiquidarSolicitud" Visible="false" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Liquidacion de Credito</h3>
                    </div>
                    <div class="panel-body">
                        <asp:Panel ID="pnlSccBtns_Liquidacion" runat="server">
                            <asp:LinkButton ID="lbtnRegresar_Liquidacion" CssClass="btn btn-sm btn-default hidden-print" OnClick="lbtnRegresar_Liquidacion_Click" runat="server">
                            <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                            </asp:LinkButton>
                            <button id="btnImprimirLiquidacion_Liquidacion" class="btn btn-default btn-sm hidden-print" onclick="printPF();" visible="false" runat="server"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>Imprimir</button>
                            <asp:LinkButton ID="lbtnRegistrar_Liquidacion" CssClass="btn btn-sm btn-info hidden-print" OnClick="lbtnRegistrar_Liquidacion_Click" runat="server">
                            <i class="fas fa-fire fa-lg"></i> Liquidar Credito
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbtnConfirmarLiquidacion_Liquidacion" CssClass="btn btn-sm btn-success hidden-print" OnClick="lbtnConfirmarLiquidacion_Liquidacion_Click" Visible="false" runat="server">
                            <i class="far fa-check-circle fa-lg"></i> Confirmar Liquidacion
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbtnCancelarLiquidacion_Liquidacion" CssClass="btn btn-sm btn-danger hidden-print" OnClick="lbtnCancelarLiquidacion_Liquidacion_Click" Visible="false" runat="server">
                            <i class="far fa-times-circle fa-lg"></i> Cancelar Liquidacion
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbtnRegresar_Gestiones" CssClass="btn btn-sm btn-warning hidden-print" OnClick="lbtnRegresar_Gestiones_Click" runat="server">
                            <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar a Gestión
                            </asp:LinkButton>
                        </asp:Panel>

                        <asp:Panel ID="pnlSccConfirmarLiquidacion_Liquidacion" CssClass="col-sm-12 col-md-6" Visible="false" runat="server">
                            <label class="small">N° Transaccion(confirmacion bancaria del pago de la liquidacion)</label>
                            <div class="input-group">
                                <asp:TextBox ID="txtNoTransaccion_ConfirmarLiquidacion" CssClass="form-control input-sm" MaxLength="30" runat="server"></asp:TextBox>
                                <div class="input-group-btn">
                                    <asp:LinkButton ID="lbtnCancelarConfirmacionLiquidacion_Liquidacion" CssClass="btn btn-sm btn-default hidden-print" OnClick="lbtnCancelarConfirmacionLiquidacion_Liquidacion_Click" runat="server">
                                    <i class="far fa-arrow-alt-circle-left fa-lg"></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lbtnGuardarConfirmacionLiquidacion_Liquidacion" CssClass="btn btn-sm btn-success hidden-print" OnClick="lbtnGuardarConfirmacionLiquidacion_Liquidacion_Click" runat="server">
                                    <i class="far fa-check-circle fa-lg"></i> Liquidar
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="pnlError_Liquidacion" class="col-sm-12" Visible="false" runat="server">
                            <div class="alert alert-danger" style="padding: 5px 15px; margin: 5px 0px 0px 0px;">
                                <asp:Label ID="lblError_Liquidacion" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>

                        <div class="col-sm-12">
                            <hr class="hidden-print" />
                        </div>

                        <div class="col-sm-12">
                            <blockquote style="background-color: #F8F8F8;">
                                <p>Liquidación
                                    <asp:Label ID="lblNumeroLiquidacion_Liquidacion" runat="server"></asp:Label></p>
                            </blockquote>
                        </div>

                        <div class="col-sm-3 col-print-3">
                            <label class="small">N° Solicitud</label>
                            <pre><asp:Label ID="lblIdSolicitud_Liquidacion" runat="server" /></pre>
                        </div>

                        <div class="col-sm-3 col-print-3">
                            <label class="small">DNI</label>
                            <pre><asp:Label ID="lblDNI_Liquidacion" runat="server"></asp:Label></pre>
                        </div>

                        <div class="col-sm-6 col-print-6">
                            <label class="small">Cliente</label>
                            <pre><asp:Label ID="lblCliente_Liquidacion" runat="server"></asp:Label></pre>
                        </div>

                        <div class="col-sm-4 col-print-4">
                            <label class="small">Monto Credito</label>
                            <pre><asp:Label ID="lblMontoCredito_Liquidacion" runat="server"></asp:Label></pre>
                        </div>

                        <div class="col-sm-4 col-print-4">
                            <label class="small">Cuota</label>
                            <pre><asp:Label ID="lblCuota_Liquidacion" runat="server"></asp:Label></pre>
                        </div>

                        <div class="col-sm-4 col-print-4">
                            <label class="small">Total a Liquidar</label>
                            <pre><asp:Label ID="lblTotalLiquidar_Liquidacion" runat="server"></asp:Label></pre>
                        </div>

                        <div class="col-sm-4 col-print-4">
                            <label class="small">Fecha Credito</label>
                            <pre><asp:Label ID="lblFechaCredito_Liquidacion" runat="server"></asp:Label></pre>
                        </div>

                        <div class="col-sm-4 col-print-4">
                            <label class="small">Fecha 1er. Pago</label>
                            <pre><asp:Label ID="lblFechaPrimerPago_Liquidacion" runat="server"></asp:Label></pre>
                        </div>

                        <div class="col-sm-4 col-print-4" style="margin-bottom: 17px;">
                            <label class="small">Fecha Liquidacion</label>
                            <pre class="visible-print-block"><asp:Label ID="lblFechaLiquidacion_Liquidacion" runat="server"></asp:Label></pre>
                            <div class="input-group hidden-print">
                                <asp:TextBox ID="txtFechaLiquidar_Liquidacion" OnTextChanged="txtFechaLiquidar_Liquidacion_TextChanged" AutoPostBack="true" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10" Enabled="false" runat="server" />
                                <span class="input-group-addon">
                                    <asp:LinkButton ID="btnFechaLiquidacion" Visible="false" runat="server">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </asp:LinkButton>
                                </span>
                            </div>
                            <ajaxToolkit:CalendarExtender ID="ceFechaLiquidacion_Liquidacion" Format="dd/MM/yyyy" TargetControlID="txtFechaLiquidar_Liquidacion" PopupButtonID="btnFechaLiquidacion" runat="server"></ajaxToolkit:CalendarExtender>
                        </div>

                        <div class="col-sm-4 col-print-4">
                            <label class="small">Monto Comisión uso de canal</label>
                            <pre><asp:Label ID="lblMontoComision_Cronograma" runat="server"></asp:Label></pre>
                        </div>

                        <div class="col-sm-12 col-print-12">
                            <hr />
                            <div style="height: auto; width: auto; overflow-x: auto;">
                                <asp:GridView ID="gvRecibos_Liquidacion" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="small"
                                    CssClass="table table-bordered table-striped table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData"
                                    OnRowDataBound="gvRecibos_Liquidacion_RowDataBound">
                                    <Columns>
                                        <asp:BoundField HeaderText="N° Cuota" DataField="Recibo" />
                                        <asp:BoundField HeaderText="Fecha" DataField="FechaRecibo" DataFormatString="{0:d}" />
                                        <asp:BoundField HeaderText="Capital" DataField="LiquidaCapital" DataFormatString="{0:c2}" />
                                        <asp:BoundField HeaderText="Interes" DataField="LiquidaInteres" />
                                        <asp:BoundField HeaderText="IGV" DataField="LiquidaIGV" />
                                        <asp:BoundField HeaderText="Seguro" DataField="LiquidaSeguro" />
                                        <asp:BoundField HeaderText="GAT" DataField="LiquidaGAT" />
                                        <asp:BoundField HeaderText="Comisión" DataField="LiquidaOtrosCargos" DataFormatString="{0:c2}" />
                                        <asp:BoundField HeaderText="IGV Comisión" DataField="LiquidaIGVOtrosCargos" DataFormatString="{0:c2}" />
                                        <asp:BoundField HeaderText="Pagar" DataField="LiquidaTotal" DataFormatString="{0:c2}" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        Hubo un problema al cargar el calculo de la liquidacion
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>

                        <asp:Panel ID="pnlFirmaCliente_Liquidacion" class="col-sm-12 col-print-12 visible-print-block" runat="server">
                            <p class="text-center" style="margin-top: 85px;">
                                _________________________________________
                            </p>
                            <p class="text-center small">
                                Firma Cliente
                            </p>
                        </asp:Panel>
                    </div>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="filtrarSucursal" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(document).ready(function () {
            // Activar Tooltips primer carga de la pagina
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            // Activar Tooltips
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });
    </script>
</asp:Content>
