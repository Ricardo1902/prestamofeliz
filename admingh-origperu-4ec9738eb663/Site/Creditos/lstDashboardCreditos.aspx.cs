﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Site_Creditos_lstDashboardCreditos : System.Web.UI.Page
{
    #region "Propiedades"

    private bool esConsulta
    {
        get { return (ViewState[this.UniqueID + "_esConsulta"] != null) ? (bool)ViewState[this.UniqueID + "_esConsulta"] : false; }
        set { ViewState[this.UniqueID + "_esConsulta"] = value; }
    }

    private bool esGestion
    {
        get { return (ViewState[this.UniqueID + "_esGestion"] != null) ? (bool)ViewState[this.UniqueID + "_esGestion"] : false; }
        set { ViewState[this.UniqueID + "_esGestion"] = value; }
    }

    private bool puedeLiquidar
    {
        get { return (ViewState[this.UniqueID + "_puedeLiquidar"] != null) ? (bool)ViewState[this.UniqueID + "_puedeLiquidar"] : false; }
        set { ViewState[this.UniqueID + "_puedeLiquidar"] = value; }
    }

    private bool puedeLiquidarFechaAnterior
    {
        get { return (ViewState[this.UniqueID + "_puedeLiquidarFechaAnterior"] != null) ? (bool)ViewState[this.UniqueID + "_puedeLiquidarFechaAnterior"] : false; }
        set { ViewState[this.UniqueID + "_puedeLiquidarFechaAnterior"] = value; }
    }

    private bool puedeConfirmarLiquidacion
    {
        get { return (ViewState[this.UniqueID + "_puedeConfirmarLiquidacion"] != null) ? (bool)ViewState[this.UniqueID + "_puedeConfirmarLiquidacion"] : false; }
        set { ViewState[this.UniqueID + "_puedeConfirmarLiquidacion"] = value; }
    }
    
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder strScript = new StringBuilder();
        int esC = 0, esG = 0;

        if (Request.QueryString["idSolicitud"] != null)
            if (int.TryParse(Request.QueryString["idSolicitud"].ToString(), out esC))
                if (esC > 0)
                    esConsulta = true;
        if (Request.QueryString["esGestion"] != null)
            if (int.TryParse(Request.QueryString["esGestion"].ToString(), out esG))
                if (esG > 0)
                {
                    esConsulta = false;
                    esGestion = true;
                }

        if (!Page.IsPostBack)
        {
            int idSolicitud;
            DateTime fechaLiquidacion;
            if ((idSolicitud = Convert.ToInt32(Request.QueryString["idSolicitud"])) > 0)
            {
                if (Request.QueryString["fechaLiquidacion"] != null)
                {
                    fechaLiquidacion = Convert.ToDateTime(Request.QueryString["fechaLiquidacion"]);
                }
                else
                {
                    fechaLiquidacion = DateTime.Now;
                }

                MostrarLiquidacion(idSolicitud, fechaLiquidacion);
                if (esG <= 0)
                {
                    lbtnRegistrar_Liquidacion.Visible = false;
                    lbtnRegresar_Liquidacion.Visible = false;
                    return;
                }
            }

            /**Seccion para verificar que se deba filtrar por sucursal**/
            if (Convert.ToInt32(Session["Rol"].ToString()) == 17 || Convert.ToInt32(Session["Rol"].ToString()) == 2)
            {
                filtrarSucursal.Value = "1";
            }
            else
            {
                filtrarSucursal.Value = "0";
            }            

            if (esConsulta)
            {
                lbtnRegresar_Liquidacion.Visible = false;
            }

            //llenarPromotor();
            llenarSucursales();
            LlenaDatagrid(0, "1");
            lbtnRegresar_Gestiones.Visible = esGestion;
            strScript.Append(Utilidades.CargarCotenido(Server.MapPath("../Creditos/js/lstdashboardcreditos.js")));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
        }
    }

    protected void gvCreditos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Mostramos la "accion" Liquidacion solo si el Credito esta en estatus Dispersado.
            // Mostramos la "accion" VerCronograma solo si el Credito esta en estatus Dispersado.
            int indexEstatus = ControlsHelper.GetColumnIndexByName(e.Row, "Estatus");

            LinkButton lbtnLiquidacion = (LinkButton)e.Row.FindControl("lbtnLiquidacion");
            lbtnLiquidacion.Visible = (e.Row.Cells[indexEstatus].Text == "Dispersado");

            LinkButton lbtnVerCronograma = (LinkButton)e.Row.FindControl("lbtnVerCronograma");
            lbtnVerCronograma.Visible = (e.Row.Cells[indexEstatus].Text == "Dispersado");
        }
    }

    protected void cmbEstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        LlenaDatagrid();
    }

    protected void cmbPertenece_SelectedIndexChanged(object sender, EventArgs e)
    {
        LlenaDatagrid();
    }

    protected void cmbPromotor_SelectedIndexChanged(object sender, EventArgs e)
    {
        LlenaDatagrid();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        LlenaDatagrid();
    }

    protected void cmbSucursal_SelectedIndexChanged(object sender, EventArgs e)
    {
        LlenaDatagrid(0, "1");
    }

    protected void gvCreditos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "VerLiquidacion")
            {
                int idSolicitud = int.Parse(e.CommandArgument.ToString());
                MostrarLiquidacion(idSolicitud, DateTime.Now);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void gvCreditos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvCreditos.PageIndex = e.NewPageIndex;
        LlenaDatagrid();
    }

    protected void lbtnRegresar_Cronograma_Click(object sender, EventArgs e)
    {
        pnlBuscar.Visible = true;
    }

    protected void lbtnRegresar_Gestiones_Click(object sender, EventArgs e)
    {
        int idSolicitud = 0;
        if (Request.QueryString["idSolicitud"] != null)
        {
            if (int.TryParse(Request.QueryString["idSolicitud"].ToString(), out idSolicitud))
            {
                Response.Redirect("../Gestiones/Gestion.aspx?idsolicitud=" + idSolicitud.ToString());
            }
        }
    }

    #region "Liquidar Credito"

    protected void lbtnRegresar_Liquidacion_Click(object sender, EventArgs e)
    {
        pnlBuscar.Visible = true;
        pnlLiquidarSolicitud.Visible = false;
    }

    protected void lbtnRegistrar_Liquidacion_Click(object sender, EventArgs e)
    {
        pnlError_Liquidacion.Visible = false;

        try
        {
            using (wsSOPF.CreditoClient ws = new CreditoClient())
            {
                wsSOPF.TBCreditos.Liquidacion eLiquidacion = new TBCreditos.Liquidacion();
                eLiquidacion.IdSolicitud = int.Parse(lblIdSolicitud_Liquidacion.Text);
                eLiquidacion.IdUsuarioRegistro = Convert.ToInt32(Session["UsuarioId"].ToString());
                eLiquidacion.FechaLiquidar = new UtilsDateTimeR();
                eLiquidacion.FechaLiquidar.Value = DateTime.Parse(txtFechaLiquidar_Liquidacion.Text.Trim());

                // Registrar la Liquidacion
                wsSOPF.ResultadoOfTBCreditosLiquidacionhDx61Z0Z resInsLiquidacion = ws.RegistrarLiquidacion(eLiquidacion);

                // Si ocurre algun error al Registrar la Liquidacion
                if (resInsLiquidacion.Codigo != 0)
                    throw new Exception(resInsLiquidacion.Mensaje);

                eLiquidacion = resInsLiquidacion.ResultObject;

                lblNumeroLiquidacion_Liquidacion.Text = string.Format("Numero: {0}", eLiquidacion.IdLiquidacion.ToString());
                lblNumeroLiquidacion_Liquidacion.Visible = true;


                btnImprimirLiquidacion_Liquidacion.Visible = true;
                lbtnRegistrar_Liquidacion.Visible = false;
                lbtnConfirmarLiquidacion_Liquidacion.Visible = true;
                // Validar si el usuario tiene acceso a Confirmar Liquidacion
                if (!this.puedeConfirmarLiquidacion)
                    lbtnConfirmarLiquidacion_Liquidacion.Visible = false;
                lbtnCancelarLiquidacion_Liquidacion.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblError_Liquidacion.Text = ex.Message;
            pnlError_Liquidacion.Visible = true;
        }
    }

    protected void lbtnConfirmarLiquidacion_Liquidacion_Click(object sender, EventArgs e)
    {
        txtNoTransaccion_ConfirmarLiquidacion.Text = string.Empty;
        pnlSccBtns_Liquidacion.Visible = false;
        pnlSccConfirmarLiquidacion_Liquidacion.Visible = true;
    }

    protected void lbtnCancelarLiquidacion_Liquidacion_Click(object sender, EventArgs e)
    {
        try
        {
            using (wsSOPF.CreditoClient ws = new CreditoClient())
            {
                wsSOPF.TBCreditos.Liquidacion eLiquidacion = ws.ObtenerLiquidacionActiva(new TBCreditos.Liquidacion() { IdSolicitud = int.Parse(lblIdSolicitud_Liquidacion.Text.Trim()) });
                eLiquidacion.IdUsuarioCancela = Convert.ToInt32(Session["UsuarioId"].ToString());
                wsSOPF.ResultadoOfTBCreditosLiquidacionhDx61Z0Z resCancelaLiq = ws.CancelarLiquidacion(eLiquidacion);

                if (resCancelaLiq.Codigo != 0)
                    throw new Exception(resCancelaLiq.Mensaje);

                btnImprimirLiquidacion_Liquidacion.Visible = false;
                lbtnRegistrar_Liquidacion.Visible = true;
                lbtnConfirmarLiquidacion_Liquidacion.Visible = false;
                lbtnCancelarLiquidacion_Liquidacion.Visible = false;
                lblNumeroLiquidacion_Liquidacion.Text = string.Empty;
            }
        }
        catch (Exception ex)
        {
            lblError_Liquidacion.Text = ex.Message;
            pnlError_Liquidacion.Visible = true;
        }
    }

    protected void lbtnGuardarConfirmacionLiquidacion_Liquidacion_Click(object sender, EventArgs e)
    {
        try
        {
            using (wsSOPF.CreditoClient ws = new CreditoClient())
            {
                wsSOPF.TBCreditos.Liquidacion eLiquidacion = ws.ObtenerLiquidacionActiva(new TBCreditos.Liquidacion() { IdSolicitud = int.Parse(lblIdSolicitud_Liquidacion.Text.Trim()) });
                eLiquidacion.IdUsuarioConfirma = Convert.ToInt32(Session["UsuarioId"].ToString());
                eLiquidacion.NumeroTransaccion = txtNoTransaccion_ConfirmarLiquidacion.Text.Trim();

                wsSOPF.ResultadoOfTBCreditosLiquidacionhDx61Z0Z resLiq = ws.ConfirmarLiquidacion(eLiquidacion);

                if (resLiq.Codigo != 0)
                    throw new Exception(resLiq.Mensaje);

                pnlBuscar.Visible = true;
                pnlLiquidarSolicitud.Visible = false;
                pnlSccBtns_Liquidacion.Visible = true;
                pnlSccConfirmarLiquidacion_Liquidacion.Visible = false;
                btnBuscar_Click(sender, e); // Cargar el grid de busqueda para actualizar el estatus del Credito(s)

                lblExito_Buscar.Text = string.Format("Se ha Liquidado el Credito de la Solicitud {0} con el numero de Liquidacion {1}", eLiquidacion.IdSolicitud, eLiquidacion.IdLiquidacion);
                pnlExito_Buscar.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblError_Liquidacion.Text = ex.Message;
            pnlError_Liquidacion.Visible = true;
        }
    }

    protected void lbtnCancelarConfirmacionLiquidacion_Liquidacion_Click(object sender, EventArgs e)
    {
        pnlSccBtns_Liquidacion.Visible = true;
        pnlSccConfirmarLiquidacion_Liquidacion.Visible = false;
    }

    protected void gvRecibos_Liquidacion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            wsSOPF.TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle objData = (wsSOPF.TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle)e.Row.DataItem;

            // Obtenemos los Index de cada Columna para accesarla posteriormente            
            int idxInteres = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaInteres");
            int idxIGV = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaIGV");
            int idxSeguro = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaSeguro");
            int idxGAT = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaGAT");
            int idxComision = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaOtrosCargos");
            int idxIGVComision = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaIGVOtrosCargos");

            e.Row.Cells[idxInteres].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaInteres.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxIGV].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaIGV.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxSeguro].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaSeguro.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxGAT].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaGAT.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxComision].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaOtrosCargos.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxIGVComision].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaIGVOtrosCargos.ToString("C", CultureInfo.CurrentCulture) : "-";
        }
    }

    protected void txtFechaLiquidar_Liquidacion_TextChanged(object sender, EventArgs e)
    {
        CargarLiquidacion(Convert.ToInt32(lblIdSolicitud_Liquidacion.Text), DateTime.ParseExact(txtFechaLiquidar_Liquidacion.Text.Trim(), "dd/MM/yyyy", null));
    }

    #endregion

    #region "Metodos"

    private void LlenaDatagrid(int PageIndex = 0, string bandera = "0")
    {
        gvCreditos.DataSource = null;
        DataSet solicitudes = new DataSet();
        int idSusursal = 0, idUsuario = 0, idrole = 0, promotor = 0;
        pnlExito_Buscar.Visible = false;

        //CONTROLAMOS ERROR DE FALTA DE SUCURSALES. POSIBLEMENTE FALTE REGISTRO EN TABLA DE REGIONALES PARA GERENTE REGIONAL
        if(cmbSucursal.Items.Count <= 0)
        {
            pnlError_Liquidacion.Visible = true;
            lblError_Liquidacion.Text = "Error de información: revise que el usuario tenga los permisos necesarios de liquidación.";
            lbtnRegistrar_Liquidacion.Visible = false;
        }
        else
        {
            try
            {
                pnlError_Liquidacion.Visible = false;
                lblError_Liquidacion.Text = "";
                int Pertenece = Convert.ToInt32(cmbPertenece.SelectedItem.Value);
                int EstatusSol = Convert.ToInt32(cmbEstatus.SelectedItem.Value);
                if (Pertenece == 0)
                {
                    idSusursal = 0;
                    idUsuario = 0;
                    idrole = 0;
                    if (filtrarSucursal.Value == "1")
                    {

                        idSusursal = Convert.ToInt32(Session["compania"].ToString());
                    }
                    else
                    {
                        idSusursal = Convert.ToInt32(cmbSucursal.SelectedItem.Value);
                    }
                }
                else
                {
                    idrole = Convert.ToInt32(Session["Rol"].ToString());
                    if (filtrarSucursal.Value == "1")
                    {
                        idSusursal = Convert.ToInt32(Session["compania"].ToString());
                    }
                    else
                    {

                        idSusursal = Convert.ToInt32(cmbSucursal.SelectedItem.Value);
                    }
                    //idSusursal =Convert.ToInt32(Session["compania"].ToString());
                    idUsuario = Convert.ToInt32(Session["UsuarioId"].ToString());

                }

                //llenamos promotor
                //checamos si hubo un cambio en llenarSucursales o si se recarga la pagina para filtrar promotores
                if (Convert.ToInt32(bandera) == 1)
                {
                    if (filtrarSucursal.Value == "1")
                    {
                        if (idrole == 2)
                        {
                            llenarPromotor(2, idUsuario);
                        }
                        else
                        {

                            llenarPromotor(1, idSusursal);
                        }
                    }
                    else
                    {
                        if (idSusursal == 0)
                        {
                            llenarPromotor(2, idUsuario);
                        }
                        else
                        {
                            llenarPromotor(1, idSusursal);
                        }
                    }
                }
                //obtenemos promotor de sucursal seleccionada
                if (cmbPromotor.SelectedItem.Value != null)
                {
                    promotor = Convert.ToInt32(cmbPromotor.SelectedItem.Value);
                }

                using (wsSOPF.CreditoClient wsCredito = new CreditoClient())
                {
                    solicitudes = wsCredito.ObtenerCreditoListado(34, promotor, EstatusSol, 0, busqueda.Text, "", "", idSusursal, idUsuario, 0, "");
                } //int Accion, int cuenta, int idcliente, string cliente, string fchInicial, string fchFinal, int idsucursal, int idconvenio, int idlinea

                if (solicitudes.Tables[0].Rows.Count > 0)
                {
                    gvCreditos.DataSource = solicitudes;
                    gvCreditos.DataBind();
                }
                else
                {
                    gvCreditos.DataSource = null;
                    gvCreditos.DataBind();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    private void llenarPromotor(int Accion, int parametrox)
    {
        int rolusuario = Convert.ToInt32(Session["Rol"].ToString());
        //int idSusursal = Convert.ToInt32(Session["compania"].ToString());
        TBCATPromotor[] promotor = null;
        using (wsSOPF.CatalogoClient wsPromotor = new CatalogoClient())
        {
            promotor = wsPromotor.ObtenerPromotores(Accion, parametrox);

        }

        if (promotor.Count() > 0)
        {
            //List<Country> countries = ...;

            cmbPromotor.DataSource = null;//Vaciar comboBox
            //cmbPromotor.Items.Add(new KeyValuePair<string, string>("0", "Todos"));

            cmbPromotor.DataTextField = "Promotor";//Indicar qué propiedad se verá en la lista
            cmbPromotor.DataValueField = "IdPromotor";//Indicar qué valor tendrá cada ítem
            cmbPromotor.DataSource = promotor;//Asignar la propiedad DataSource
            cmbPromotor.DataBind();
            //cmbPromotor.Items[0].Text = "Todos";
            cmbPromotor.Items.Add(new ListItem("Todos", "0"));
            //cmbPromotor.Items[0].Value = "0";    
            cmbPromotor.SelectedIndex = 0;




            // cmbTipoComision.Items.Clear();
            //cmbTipoComision.DataTextField = ds.Tables[0].Columns["Descripcion"].ToString();
            //cmbTipoComision.DataValueField = ds.Tables[0].Columns["Valor"].ToString();

            //cmbTipoComision.DataSource = ds.Tables[0].DefaultView;
            //cmbTipoComision.DataBind();
        }
        else
        {
            cmbPromotor.Items.Clear();
            cmbPromotor.Items.Add(new ListItem("Sin Promotores", "-1"));
            //cmbPromotor.Items[0].Value = "0";    
            cmbPromotor.SelectedIndex = -1;
            //cmbPromotor.DataSource = null;
        }

        if (rolusuario == 2)
        {
            cmbPromotor.Enabled = false;
        }


    }

    private void llenarSucursales()
    {
        int rolusuario = Convert.ToInt32(Session["Rol"].ToString());
        int idUsuario = Convert.ToInt32(Session["UsuarioId"].ToString());

        TBCATSucursal[] sucursalList = null;
        using (wsSOPF.UsuarioClient wsSucursal = new UsuarioClient())
        {
            wsSOPF.TBCATSucursal sucursal = new TBCATSucursal();
            sucursal.IdSucursal = idUsuario;
            sucursal.Accion = 101;
            sucursal.Sucursal = "";
            sucursal.IdEstatus = 0;
            sucursal.IdAfiliado = 0;

            sucursalList = wsSucursal.ObtenerSucursal(sucursal);

            if (sucursalList.Count() > 0)
            {
                //List<Country> countries = ...;

                cmbSucursal.DataSource = null;//Vaciar comboBox
                //cmbPromotor.Items.Add(new KeyValuePair<string, string>("0", "Todos"));

                cmbSucursal.DataTextField = "Sucursal";//Indicar qué propiedad se verá en la lista
                cmbSucursal.DataValueField = "IdSucursal";//Indicar qué valor tendrá cada ítem
                cmbSucursal.DataSource = sucursalList;//Asignar la propiedad DataSource
                cmbSucursal.DataBind();
                //cmbPromotor.Items[0].Text = "Todos";
                cmbSucursal.Items.Add(new ListItem("Todos", "0"));
                //cmbPromotor.Items[0].Value = "0";    
                cmbSucursal.SelectedIndex = 0;




                // cmbTipoComision.Items.Clear();
                //cmbTipoComision.DataTextField = ds.Tables[0].Columns["Descripcion"].ToString();
                //cmbTipoComision.DataValueField = ds.Tables[0].Columns["Valor"].ToString();

                //cmbTipoComision.DataSource = ds.Tables[0].DefaultView;
                //cmbTipoComision.DataBind();
            }

            if (rolusuario == 17 || rolusuario == 2)
            {
                cmbSucursal.Enabled = false;

                //ponemos como seleccionada la suucursal ala cual pertenece el usuario.
                int idSusursal = Convert.ToInt32(Session["compania"].ToString());
                int j = 0;

                if (idSusursal > 0 && idSusursal != null)
                {

                    for (j = 0; j <= cmbSucursal.Items.Count - 1; j++)
                    {
                        if (Convert.ToString(cmbSucursal.Items[j].Value.Trim()) == Convert.ToString(idSusursal))
                        {
                            cmbSucursal.SelectedIndex = idSusursal;
                            break;
                        }

                    }
                }
                else
                {
                    cmbSucursal.SelectedIndex = 0;
                }
            }

        }
    }

    private void CargarLiquidacion(int idSolicitud, DateTime? fechaLiquidacion = null)
    {
        try
        {
            bool fechaAnteriorError = false;

            // Boton fecha liquidacion            
            btnFechaLiquidacion.Visible = (!esConsulta && this.puedeLiquidar);
            
            if (fechaLiquidacion != null && fechaLiquidacion < DateTime.Today && !this.puedeLiquidarFechaAnterior && !esConsulta)
            {
                // NO SE GENERA EXCEPTION PORQUE REQUIERE CARGARSE LA LIQUIDACION CON FECHA ACTUAL PERO MANTENER EL MENSAJE DE ERROR
                btnImprimirLiquidacion_Liquidacion.Visible = false;
                lbtnRegistrar_Liquidacion.Visible = false;
                lbtnConfirmarLiquidacion_Liquidacion.Visible = false;
                lbtnCancelarLiquidacion_Liquidacion.Visible = false;
                lblError_Liquidacion.Text = "No se puede liquidar con una fecha anterior a la actual.";
                pnlError_Liquidacion.Visible = true;
                txtFechaLiquidar_Liquidacion.Text = DateTime.Today.ToString("dd/MM/yyyy");
                fechaLiquidacion = DateTime.Today;
                fechaAnteriorError = true;
            }

            lblNumeroLiquidacion_Liquidacion.Text = string.Empty;

            using (wsSOPF.CreditoClient ws = new CreditoClient())
            {
                wsSOPF.TBCreditos.Liquidacion LiquidacionCalc = new TBCreditos.Liquidacion();
                LiquidacionCalc.IdSolicitud = idSolicitud;
                LiquidacionCalc.FechaLiquidar = new UtilsDateTimeR();
                LiquidacionCalc.FechaLiquidar.Value = (fechaLiquidacion == null) ? DateTime.Today : fechaLiquidacion;

                // Verificar si existe una liquidacion Activa para la solicitud 
                wsSOPF.TBCreditos.Liquidacion LiquidacionActiva = ws.ObtenerLiquidacionActiva(LiquidacionCalc);

                if (LiquidacionActiva != null)
                    LiquidacionCalc = LiquidacionActiva;

                // Calcular la Liquidacion
                wsSOPF.ResultadoOfTBCreditosCalculoLiquidacionhDx61Z0Z resCalculoLiquidacion = ws.CalcularLiquidacion(LiquidacionCalc);
                wsSOPF.TBCreditos.CalculoLiquidacion CalculoLiquidacion = resCalculoLiquidacion.ResultObject;

                if (resCalculoLiquidacion.Codigo != 0)
                    throw new Exception(resCalculoLiquidacion.Mensaje);

                lblIdSolicitud_Liquidacion.Text = CalculoLiquidacion.IdSolicitud.ToString();
                lblDNI_Liquidacion.Text = CalculoLiquidacion.DNICliente;
                lblCliente_Liquidacion.Text = string.Format("{0} {1} {2}", CalculoLiquidacion.NombreCliente, CalculoLiquidacion.ApPaternoCliente, CalculoLiquidacion.ApMaternoCliente);
                lblMontoCredito_Liquidacion.Text = CalculoLiquidacion.MontoCredito.ToString("C", CultureInfo.CurrentCulture);
                lblCuota_Liquidacion.Text = CalculoLiquidacion.Cuota.ToString("C", CultureInfo.CurrentCulture);
                lblTotalLiquidar_Liquidacion.Text = CalculoLiquidacion.TotalLiquidacion.ToString("C", CultureInfo.CurrentCulture);
                lblFechaCredito_Liquidacion.Text = CalculoLiquidacion.FechaCredito.ToString("dd/MM/yyyy");
                lblFechaLiquidacion_Liquidacion.Text = CalculoLiquidacion.FechaLiquidacion.Value.GetValueOrDefault().ToString("dd/MM/yyyy");
                txtFechaLiquidar_Liquidacion.Text = lblFechaLiquidacion_Liquidacion.Text;
                lblFechaPrimerPago_Liquidacion.Text = CalculoLiquidacion.FechaPrimerPago.ToString("dd/MM/yyyy");
                lblMontoComision_Cronograma.Text = CalculoLiquidacion.MontoComision.ToString("C", CultureInfo.CurrentCulture);

                gvRecibos_Liquidacion.DataSource = CalculoLiquidacion.DetalleLiquidacion;
                gvRecibos_Liquidacion.DataBind();

                // Si existe una liquidacion Activa, validamos que el monto de Liquidacion del calculo corresponda al monto de Liquidacion Activo, sino mostramos error
                // Mostramos y ocultamos controles segun el caso de existir una Liquidacion Activa
                if (LiquidacionActiva != null)
                {
                    lbtnRegistrar_Liquidacion.Visible = false;
                    lbtnConfirmarLiquidacion_Liquidacion.Visible = true;
                    lbtnCancelarConfirmacionLiquidacion_Liquidacion.Visible = true;

                    lblNumeroLiquidacion_Liquidacion.Text = string.Format("Numero: {0}", LiquidacionActiva.IdLiquidacion.ToString());
                    lblNumeroLiquidacion_Liquidacion.Visible = true;

                    btnImprimirLiquidacion_Liquidacion.Visible = true;

                    lbtnRegistrar_Liquidacion.Visible = false;
                    lbtnConfirmarLiquidacion_Liquidacion.Visible = true;
                    lbtnCancelarLiquidacion_Liquidacion.Visible = true;

                    if (LiquidacionActiva.TotalLiquidar != CalculoLiquidacion.TotalLiquidacion)
                        throw new LiquidacionException(string.Format("El Total a Liquidar [{0}], no corresponde con el Monto de la Liquidacion {1} [{2}]", CalculoLiquidacion.TotalLiquidacion.ToString("C", CultureInfo.CurrentCulture), LiquidacionActiva.IdLiquidacion, LiquidacionActiva.TotalLiquidar.ToString("C", CultureInfo.CurrentCulture)));
                }
                else
                {
                    btnImprimirLiquidacion_Liquidacion.Visible = false;

                    lbtnRegistrar_Liquidacion.Visible = true;
                    lbtnConfirmarLiquidacion_Liquidacion.Visible = false;
                    lbtnCancelarLiquidacion_Liquidacion.Visible = false;
                }

                // Aplicar Permiso a los controles afectados
                if (!this.puedeLiquidar)
                {
                    lbtnRegistrar_Liquidacion.Visible = false;
                    lbtnConfirmarLiquidacion_Liquidacion.Visible = false;
                    lbtnCancelarLiquidacion_Liquidacion.Visible = false;
                }

                // Validar si el usuario tiene acceso a Confirmar Liquidacion
                if (!this.puedeConfirmarLiquidacion)
                    lbtnConfirmarLiquidacion_Liquidacion.Visible = false;


                if (!fechaAnteriorError)
                {
                    // Limpiamos error
                    lblError_Liquidacion.Text = "";
                    pnlError_Liquidacion.Visible = false;
                }
                else
                {
                    fechaAnteriorError = false;
                }
            }
        }
        catch (LiquidacionException ex)
        {
            btnImprimirLiquidacion_Liquidacion.Visible = false;
            lbtnRegistrar_Liquidacion.Visible = false;
            lbtnConfirmarLiquidacion_Liquidacion.Enabled = false;
            lbtnCancelarLiquidacion_Liquidacion.Enabled = true;

            lblError_Liquidacion.Text = ex.Message;
            pnlError_Liquidacion.Visible = true;
        }
        catch (Exception ex)
        {
            btnImprimirLiquidacion_Liquidacion.Visible = false;
            lbtnRegistrar_Liquidacion.Visible = false;
            lbtnConfirmarLiquidacion_Liquidacion.Visible = false;
            lbtnCancelarLiquidacion_Liquidacion.Visible = false;

            lblError_Liquidacion.Text = ex.Message;
            pnlError_Liquidacion.Visible = true;
        }
    }

    private void MostrarLiquidacion(int idSolicitud, DateTime fechaLiquidacion)
    {
        AsignarPermisosLiquidar();
        CargarLiquidacion(idSolicitud, fechaLiquidacion);

        pnlBuscar.Visible = false;
        pnlLiquidarSolicitud.Visible = true;
    }

    private void AsignarPermisosLiquidar()
    {
        ValidaRecursoAccesoResponse validaRec;
        int idUsuario = HttpContext.Current.ValidarUsuario();
        if (idUsuario <= 0) throw new Exception("La clave del usuario no es válida.");

        using (SistemaClient wsS = new SistemaClient())
        {
            string recurso = Request.Url.AbsolutePath;
            recurso = recurso.Substring(recurso.IndexOf("/") + 1, recurso.Length - 1);
            validaRec = wsS.ValidaRecursoAcceso(new ValidaRecursoAccesoRequest
            {
                IdUsuario = idUsuario,
                Recurso = recurso
            });
        }

        // Reiniciar permisos
        this.puedeLiquidar = false;
        this.puedeLiquidarFechaAnterior = false;
        this.puedeConfirmarLiquidacion = false;

        if (validaRec != null && validaRec.RecursoAcciones != null && validaRec.RecursoAcciones.Length > 0)
        {
            this.puedeLiquidar = validaRec.RecursoAcciones.Any(ac => ac.Accion == "PuedeLiquidar");
            this.puedeLiquidarFechaAnterior = validaRec.RecursoAcciones.Any(ac => ac.Accion == "PuedeLiquidarFechaAnterior");
            this.puedeConfirmarLiquidacion = validaRec.RecursoAcciones.Any(ac => ac.Accion == "PuedeConfirmarLiquidacion");
        }
    }

    #endregion       
}
