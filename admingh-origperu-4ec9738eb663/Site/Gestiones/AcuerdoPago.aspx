﻿<%@ Page Title="Acuerdo de Pago" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AcuerdoPago.aspx.cs" Inherits="Site_Gestiones_AcuerdoPago" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <link href="css/acuerdoPago.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />
    <style type="text/css">
        .modal-pr {
            background: rgba( 255, 255, 255, .8 ) url('../../Imagenes/ajax-loader.gif') 50% 50% no-repeat;
        }
    </style>

    <script type="text/javascript" src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="../../js/datatables/1.10.19/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/1.10.19/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Scripts/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script type="text/javascript" src="../../js/datepicker-1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/dropdown.js"></script>
    <script type="text/javascript" src="../../js/utils.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
    <script type="text/javascript" src="js/acuerdoPago.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="page-header">
        <div class="row inline">
            <h3 style="display: inline;">Estado de Cuenta</h3>
        </div>
    </div>
    <div class="row row-clear">
        <div class="col-md-1">
            <div class="form-group">
                <button id="btnRegresar" type="button" class="btn btn-default" onclick="onClickBtnRegresar()"><i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar</button>
            </div>
        </div>
    </div>
    <div class="row row-clear labels-normal">
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblIdSolicitud" for="txtIdSolicitud">ID de Solicitud</label>
                <input id="txtIdSolicitud" type="text" name="txtIdSolicitud" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label id="lblNombreCliente" for="txtNombreCliente">Nombre de Cliente</label>
                <input id="txtNombreCliente" type="text" name="txtNombreCliente" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label id="lblFechaCredito" for="txtFechaCredito">Fecha de Crédito</label>
                <input id="txtFechaCredito" type="text" name="txtFechaCredito" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblMontoCredito" for="txtMontoCredito">Monto de Crédito</label>
                <input id="txtMontoCredito" type="text" name="txtMontoCredito" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblPlazo" for="txtPlazo">Plazo</label>
                <input id="txtPlazo" type="text" name="txtPlazo" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label id="lblSaldoCapital" for="txtSaldoCapital">Saldo Capital</label>
                <input id="txtSaldoCapital" type="text" name="txtSaldoCapital" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblCostoTotalCredito" for="txtCostoTotalCredito">Costo Total Crédito</label>
                <input id="txtCostoTotalCredito" type="text" name="txtCostoTotalCredito" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblSaldoVencido" for="txtSaldoVencido">Saldo Vencido</label>
                <input id="txtSaldoVencido" type="text" name="txtSaldoVencido" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label id="lblTipoCredito" for="txtTipoCredito">Tipo de Crédito</label>
                <input id="txtTipoCredito" type="text" name="txtTipoCredito" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblRecibosVencidos" for="txtRecibosVencidos">Cuotas Vencidas</label>
                <input id="txtRecibosVencidos" type="text" name="txtRecibosVencidos" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblDiasMora" for="txtDiasMora">Días Mora</label>
                <input id="txtDiasMora" type="text" name="txtDiasMora" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label id="lblFechaUltimoPago" for="txtFechaUltimoPago">Fecha Último Pago</label>
                <input id="txtFechaUltimoPago" type="text" name="txtFechaUltimoPago" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblMontoCuota" for="txtMontoCuota">Monto de Cuota</label>
                <input id="txtMontoCuota" type="text" name="txtMontoCuota" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblMontoLiquidacion" for="txtMontoLiquidacion">Monto de Liquidación</label>
                <input id="txtMontoLiquidacion" type="text" name="txtMontoLiquidacion" class="form-control" disabled="disabled"
                    value="" runat="server" />
            </div>
        </div>
    </div>
    <div class="page-header page-header-corp">
        <h3>Acuerdos de Pago</h3>
    </div>
    <div class="row row-clear">
        <div id="alertErrorPlan" class="alert alert-danger" style="display: none;" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            El plan de acuerdos actual tiene un incumplimiento. Si se desea seguir con acuerdos, es necesaario generar un nuevo plan.
        </div>
    </div>
    <div class="row row-clear">

        <div class="col-md-8">
            <div class="table-responsive">
                <table id="tAcuerdos" class="table table-hover row-border display compact" style="width: 100%"></table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Ingresar Acuerdo</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label id="lblMontoNuevoAcuerdo" for="txtMontoNuevoAcuerdo">Importe Acuerdo</label>
                                <div class="input-group">
                                    <span class="input-group-addon">S/</span>
                                    <input id="txtMontoNuevoAcuerdo" type="number" step="0.01" name="txtMontoNuevoAcuerdo" class="form-control decimal-input"
                                        data-parsley-pattern="^[0-9]*(\.[0-9]{1,2})?$" data-parsley-pattern-message="El monto es inválido"
                                        data-parsley-trigger="change"
                                        value="" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label id="lblFechaNuevoAcuerdo" for="txtFechaNuevoAcuerdo">Fecha</label>
                                <div class='input-group date' id='datetimepicker1'>
                                    <input id="txtFechaNuevoAcuerdo" type="text" name="txtFechaNuevoAcuerdo" class="form-control"
                                        value="" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label id="lblGastoAdminNuevoAcuerdo" for="txtGastoAdminNuevoAcuerdo">Gasto Administrativo</label>
                                <div class="input-group">
                                    <span class="input-group-addon">S/</span>
                                    <input id="txtGastoAdminNuevoAcuerdo" type="number" step="0.01" name="txtGastoAdminNuevoAcuerdo" class="form-control decimal-input"
                                        data-parsley-pattern="^[0-9]*(\.[0-9]{1,2})?$" data-parsley-pattern-message="El monto es inválido"
                                        data-parsley-trigger="change"
                                        value="" />
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label id="lblTotalNuevoAcuerdo" for="txtTotalNuevoAcuerdo">Total a procesar</label><div class="input-group">
                                    <span class="input-group-addon">S/</span>
                                    <input id="txtTotalNuevoAcuerdo" type="text" name="txtTotalNuevoAcuerdo" class="form-control decimal-input" disabled="disabled"
                                        value="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-com-derecha">
                            <button type="button" id="btnGuardarAcuerdo" class="btn btn-success">
                                <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
                            </button>

                            <button type="button" id="btnCancelarAcuerdo" class="btn btn-default">
                                <span class="glyphicon glyphicon-ban"></span>&nbsp;Cancelar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-pr"></div>
</asp:Content>
