﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using wsSOPF;

public partial class Site_Gestiones_AcuerdoPago : System.Web.UI.Page
{
    private int IdSolicitud = 0;
    private int IdUsuario = 0;
    private StringBuilder strScript = new StringBuilder();
    private decimal Cuota = 0.00m;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            IdUsuario = HttpContext.Current.ValidarUsuario();
            if (IdUsuario <= 0) throw new Exception("La clave del usuario no es válida");

            if (Request.QueryString["idsolicitud"] != null)
            {
                try
                {
                    int.TryParse(Request.QueryString["idsolicitud"].ToString(), out IdSolicitud);
                    if (IdSolicitud <= 0) throw new Exception("La clave de la solicitud no es válida");

                    strScript.AppendFormat("idSolicitud={0};", IdSolicitud);
                }
                catch (Exception ex)
                {
                }
            }

            using (GestionClient gestionClient = new GestionClient())
            {
                ObtenerInformacionCliente(gestionClient);
                ObtenerPlanAcuerdoSolicitud(gestionClient);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
        }
    }

    #region PrivateMethods
    private void ObtenerInformacionCliente(GestionClient gestionClient)
    {
        try
        {
            var respuestaDatos = gestionClient.ObtenerDatosClienteAcuerdo(new ObtenerDatosClienteAcuerdoRequest { IdSolicitud = IdSolicitud });
            if (respuestaDatos == null || respuestaDatos.Datos == null) throw new Exception("Ocurrio un error al intentar obtener la informacion del cliente");

            var datos = respuestaDatos.Datos;
            string plazo = "";

            switch (datos.Frecuencia)
            {
                case "M":
                    plazo = datos.Plazo + " MESES";
                    break;
                case "Q":
                    plazo = datos.Plazo + " QUINCENAS";
                    break;
            }

            txtIdSolicitud.Value = IdSolicitud.ToString();
            txtNombreCliente.Value = datos.NombreCliente;

            txtFechaCredito.Value = datos.FechaCredito == null ? "" : ((DateTime)datos.FechaCredito).ToString("dd/MM/yyyy");
            txtMontoCredito.Value = datos.MontoCredito.ToString("C", CultureInfo.CurrentCulture);
            txtPlazo.Value = plazo;

            txtSaldoCapital.Value = datos.SaldoCapital.ToString("C", CultureInfo.CurrentCulture);
            txtCostoTotalCredito.Value = datos.CostoTotalCredito.ToString("C", CultureInfo.CurrentCulture);
            txtSaldoVencido.Value = datos.SaldoVencido.ToString("C", CultureInfo.CurrentCulture);

            txtTipoCredito.Value = datos.TipoCredito;
            txtRecibosVencidos.Value = datos.RecibosVencidos.ToString();
            txtDiasMora.Value = datos.DiasMora.ToString();

            txtFechaUltimoPago.Value = datos.FechaUltimoPago == null ? "" : ((DateTime)datos.FechaUltimoPago).ToString("dd/MM/yyyy");
            txtMontoCuota.Value = datos.MontoCuota.ToString("C", CultureInfo.CurrentCulture);
            txtMontoLiquidacion.Value = datos.MontoLiquidacion.ToString("C", CultureInfo.CurrentCulture);

            Cuota = datos.MontoCuota;
            strScript.AppendFormat("saldoVencido={0};", JsonConvert.SerializeObject(datos.SaldoVencido));
            strScript.AppendFormat("diaPago={0};", JsonConvert.SerializeObject(datos.DiaPago));
            strScript.AppendFormat("cuota={0};", JsonConvert.SerializeObject(datos.MontoCuota));

        }
        catch (Exception ex)
        {
            throw;
        }
    }
    private void ObtenerPlanAcuerdoSolicitud(GestionClient gestionClient)
    {
        string baseError = "Ocurrio un error al obtener la informacion de los acuerdos: ";
        if (IdSolicitud <= 0) throw new Exception(baseError + "La clave de la solicitud no es válida");

        var respuesta = gestionClient.ObtenerAcuerdoPagoConDetallePorSolicitud(new ObtenerAcuerdoPagoRequest { IdSolicitud = IdSolicitud });
        if (respuesta != null && respuesta.AcuerdoPago != null)
        {
            strScript.AppendFormat("acuerdo={0};", JsonConvert.SerializeObject(respuesta.AcuerdoPago));
        }
        if (respuesta.Detalle != null && respuesta.Detalle.Length > 0)
        {

            var acuerdosPago = respuesta.Detalle.Select(a => new
            {
                IdAcuerdoPago = a.IdAcuerdoPago,
                IdAcuerdoDetalle = a.IdAcuerdoPagoDetalle,
                MontoCuota = Cuota,
                Monto = a.Monto,
                GastoAdministrativo = a.GastoAdministrativo,
                MontoTotal = a.MontoTotal,
                IdEstatus = a.IdEstatus,
                Estatus = "",
                FechaAcuerdo = a.FechaAcuerdo,
                FechaEnvioDomiciliacion = a.FechaEnvioDomiciliacion,
                FechaCumplimiento = a.FechaCumplimiento
            });

            strScript.AppendFormat("acuerdosPago={0};", JsonConvert.SerializeObject(acuerdosPago));

        }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GuardarAcuerdosPago(AcuerdoPagoVM acuerdo, List<AcuerdoPagoDetalleVM> detalle, bool esNuevo)
    {
        int idUsuario = HttpContext.Current.ValidarUsuario();
        if (idUsuario <= 0) throw new Exception("La clave del usuario no es válida");
        try
        {
            if (acuerdo == null) throw new Exception("La petición no es válida(acuerdo)");
            if (detalle == null || detalle.Count <= 0) throw new Exception("La petición no es válida(detalle)");

            using (GestionClient gestionClient = new GestionClient())
            {
                var respuestaAcuerdo = gestionClient.AltaAcuerdoPago(new AltaAcuerdoPagoRequest { IdSolicitud = acuerdo.IdSolicitud, IdUsuario = idUsuario, Acuerdos = detalle.ToArray(), esNuevo = esNuevo });
                if (respuestaAcuerdo == null) throw new Exception("Ocurrió un error al registrar la el plan de acuerdos de pago");
                if (respuestaAcuerdo.AcuerdoPago == null || respuestaAcuerdo.AcuerdoPago.IdAcuerdoPago <= 0) return new { IdAcuerdoPago = 0, Error = true, MensajeOperacion = respuestaAcuerdo.MensajeErrorException + ". " + respuestaAcuerdo.MensajeOperacion };//throw new Exception(respuestaAcuerdo.MensajeErrorException + ". " + respuestaAcuerdo.MensajeOperacion);

                return new { IdAcuerdoPago = respuestaAcuerdo.AcuerdoPago.IdAcuerdoPago, Error = false, MensajeOperacion = "El registro se realizó de manera exitosa."};

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object CancelarPlanAcuerdoPago(int idAcuerdoPago, int idSolicitud)
    {
        int idUsuario = HttpContext.Current.ValidarUsuario();
        if (idUsuario <= 0) throw new Exception("La clave del usuario no es válida");
        try
        {
            if (idAcuerdoPago <= 0) throw new Exception("La petición no es válida(acuerdo)");

            using (GestionClient gestionClient = new GestionClient())
            {

                var respuesta = gestionClient.CancelarAcuerdoPago(new CancelarAcuerdoPagoRequest { Accion = 0, IdUsuario = idUsuario, IdAcuerdoPago = idAcuerdoPago, IdSolicitud = idSolicitud });
                if (respuesta != null)
                {
                    return respuesta;
                }
                else
                {
                    throw new Exception("Ocurrio un error en la peticion para cancelar el plan de acuerdos de pago.");
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}