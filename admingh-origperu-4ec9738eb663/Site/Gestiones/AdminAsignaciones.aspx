﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AdminAsignaciones.aspx.cs" Inherits="Site_Gestiones_AdminAsignaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <link href="css/adminasignaciones.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />
    <script src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script src="../../js/datatables/1.10.19/jquery.dataTables.min.js"></script>
    <script src="../../js/datatables/1.10.19/dataTables.bootstrap.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script src="../../js/datepicker-1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="../../js/datepicker-1.9.0/locales/bootstrap-datepicker.es.min.js"></script>
    <script src="js/adminasignaciones.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="page-header">
        <div class="row inline">
            <h3 style="display: inline;">Administrar Asignaciones</h3>
        </div>
    </div>
    <div id="tabasigmanual" class="contenido">
        <div class="comandos">
            <button type="button" class="btn btn-primary" id="btnNuevo"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Nuevo</button>
        </div>
        <table id="tcargas" class="table hover" style="width: 100%"></table>
    </div>

    <div id="nuevoAsig" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Asignaciones</h4>
                </div>
                <div class="modal-body">
                    <div id="dCargaArchivo" class="row">
                        <div class="col-md-12">
                            <input type="file" name="fCargarArchivo" id="fCargarArchivo" class="btn btn-default" style="text-overflow: ellipsis;" accept=".xls,.xlsx" />
                            <button type="button" id="btnCargarArchivo" class="btn btn-success"><span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;Cargar</button>
                        </div>
                    </div>
                    <div id="datosCarga" class="row">
                        <div class="col-md-6">
                            <div>
                                <label>Archivo:</label>
                                <span id="nombreArchivo" class="etiqueta-valor"></span>
                            </div>
                            <div>
                                <label>Total de registros:</label>
                                <span id="totalRegistros" class="etiqueta-valor"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Motivo para las Asignaciones</label>
                            <div class="input-group">
                                <input type="text" id="txActividadAsig" class="form-control" placeholder="Capture el motivo de la asignación" />
                                <span class="input-group-btn">
                                    <button type="button" id="btnGuardarActividadAs" class="btn btn-default">
                                        <span class=" glyphicon glyphicon-floppy-disk"></span>
                                    </button>
                                </span>
                            </div>
                            <label>Estatus:</label>
                            <span id="estatus" class="etiqueta-valor"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon">Mostrando:</span>
                                <select id="selErrores" class="form-control" onchange="detalleCarga()">
                                    <option value="-1">Todos</option>
                                    <option value="0">Corectos</option>
                                    <option value="1">Incorrectos</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button type="button" id="btnAsignar" class="btn btn-success" disabled="disabled"><span class="glyphicon glyphicon-ok"></span>&nbsp;Asignar</button>
                            <button type="button" id="btnEliminarSel" class="btn btn-danger" disabled="disabled"><span class="glyphicon glyphicon-trash"></span>&nbsp;Eliminar</button>
                        </div>
                    </div>
                    <div class="row">
                        <table id="tCargaDetalle" class="table hover" style="width: 100%"></table>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cerrar" />
                </div>
            </div>
        </div>
    </div>
    <div id="asigEdicion" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Editar</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <input id="btnEditarAsig" type="button" class="btn btn-success" value="Guardar" />
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cerrar" />
                </div>
            </div>
        </div>
    </div>
    <div class="modal-pr"></div>
</asp:Content>
