﻿using DataTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using wsSOPF;

public partial class Site_Gestiones_AdminAsignaciones : System.Web.UI.Page
{
    enum Acciones
    {
        EliminarTodo,
        ElimnarParcial,
        Editar
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region WebMethos
    #region GET
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerCargaAsignacion(int idCarga)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object carga = null;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }

            using (GestionClient wsG = new GestionClient())
            {
                CargaAsignacionResponse resp = wsG.CargaAsignacion(new CargaAsignacionRequest { IdUsuario = idUsuario, IdCargaAsignacion = idCarga });
                if (resp != null)
                {
                    error = resp.Error;
                    mensaje = resp.MensajeOperacion;
                    carga = new
                    {
                        resp.CargaAsignacion.IdCargaAsignacion,
                        resp.CargaAsignacion.NombreArchivo,
                        resp.CargaAsignacion.TotalRegistros,
                        resp.CargaAsignacion.Actividad,
                        resp.CargaAsignacion.FechaRegistro,
                        Asignar = resp.CargaAsignacion.IdEstatus == 2,
                        Finalizado = new int[] { 4, 5, 6, 7 }.Contains(resp.CargaAsignacion.IdEstatus),
                        resp.CargaAsignacion.Estatus
                    };
                }

            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url, carga };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object Gestores()
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object gestores = null;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }
            List<GestorVM> listaGest = new List<GestorVM>() { new GestorVM { IdGestor = 0, Nombre = "Seleccione" } };
            using (GestionClient wsG = new GestionClient())
            {
                GestoresResponse gestoresR = wsG.Gestores(new GestoresRequest { IdUsuario = idUsuario, Activo = true });
                if (gestoresR != null)
                {
                    error = gestoresR.Error;
                    mensaje = gestoresR.MensajeOperacion;
                    if (gestoresR.Gestores != null)
                    {
                        listaGest.AddRange(gestoresR.Gestores);
                    }
                }
            }
            gestores = listaGest.Select(g => new
            {
                g.IdGestor,
                g.Nombre,
                g.Usuario
            }).ToList();
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url, gestores };
    }
    #endregion

    #region POST
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerCargas(DataTableAjaxPostModel model, CargaAsignacionFiltro datos)
    {
        string mensaje = string.Empty;
        int totalRegistros = 0;
        int registros = 0;
        object cargas = null;
        string url = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("Usuario Invalido");
            }

            if (datos == null) throw new Exception("Datos no válidos");
            CargaAsignacionesRequest peticion = new CargaAsignacionesRequest
            {
                IdUsuario = idUsuario,
                Pagina = (model.start / model.length) + 1,
                RegistrosPagina = model.length,
                IdEstatus = datos.IdEstatus
            };

            CargaAsignacionesResponse cargaAsignacionesR;
            List<CargaAsignacionVM> cargaAsignaciones = new List<CargaAsignacionVM>();
            using (GestionClient wsG = new GestionClient())
            {
                cargaAsignacionesR = wsG.CargaAsignaciones(peticion);
                if (cargaAsignacionesR != null && cargaAsignacionesR.CargaAsignaciones != null && cargaAsignacionesR.CargaAsignaciones.Length > 0)
                {
                    cargaAsignaciones = cargaAsignacionesR.CargaAsignaciones.ToList();
                }
            }

            if (cargaAsignaciones.Count > 0)
            {
                cargas = cargaAsignaciones.Select(c => new
                {
                    c.IdCargaAsignacion,
                    c.NombreArchivo,
                    c.TotalRegistros,
                    Fecha = c.FechaRegistro.ToString("dd/MM/yyyy"),
                    c.Estatus,
                    c.Actividad,
                    Error = new int[] { 3, 6 }.Contains(c.IdEstatus),
                    Asignar = c.IdEstatus == 2,
                    Finalizado = new int[] { 4, 5, 6, 7 }.Contains(c.IdEstatus)
                }).ToList();

                registros = cargaAsignaciones.Count;
                totalRegistros = cargaAsignacionesR.TotalRegistros;
            }
        }
        catch (Exception ex)
        {
        }
        model.draw++;
        return new
        {
            drawn = model.draw,
            recordsTotal = registros,
            recordsFiltered = totalRegistros,
            data = cargas,
            url
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object CargarAsignaciones(CargaArchivoVM archivo)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object carga = null;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }

            using (GestionClient wsG = new GestionClient())
            {
                CargarAsignacionRequest cargaArchivo = archivo.Convertir<CargarAsignacionRequest>();
                if (cargaArchivo != null)
                {
                    cargaArchivo.IdUsuario = idUsuario;
                    cargaArchivo.Actividad = !string.IsNullOrEmpty(archivo.Comentario) ? archivo.Comentario : string.Empty;
                }
                CargarAsignacionResponse resp = wsG.CargarAsignacion(cargaArchivo);
                if (resp != null)
                {
                    error = resp.Error;
                    mensaje = resp.MensajeOperacion;
                    if (resp.CargaAsignacion != null)
                    {
                        carga = new
                        {
                            resp.CargaAsignacion.IdCargaAsignacion,
                            resp.CargaAsignacion.NombreArchivo,
                            resp.CargaAsignacion.TotalRegistros,
                            resp.CargaAsignacion.Actividad,
                            resp.CargaAsignacion.FechaRegistro,
                            Asignar = resp.CargaAsignacion.IdEstatus == 2,
                            Finalizado = new int[] { 4, 5, 6, 7 }.Contains(resp.CargaAsignacion.IdEstatus),
                            resp.CargaAsignacion.Estatus
                        };
                    }
                }
            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url, carga };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object CargaDetalle(DataTableAjaxPostModel model, CargaAsignacionFiltro datos)
    {
        string mensaje = string.Empty;
        int totalRegistros = 0;
        int registros = 0;
        object asignaciones = null;
        string url = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("Usuario Invalido");
            }
            if (datos == null) throw new Exception("Datos no válidos");
            CargaAsignacionDetallesRequest peticion = new CargaAsignacionDetallesRequest
            {
                IdUsuario = idUsuario,
                Pagina = (model.start / model.length) + 1,
                RegistrosPagina = model.length,
                IdCargaAsignacion = datos.IdCargaAsignacion,
                Error = datos.Error
            };

            CargaAsignacionDetallesResponse cargarAsignacion;
            List<CargaAsignacionDetalleVM> cargasDetalle = new List<CargaAsignacionDetalleVM>();
            using (GestionClient wsG = new GestionClient())
            {
                cargarAsignacion = wsG.CargaAsignacionDetalles(peticion);
                if (cargarAsignacion != null && cargarAsignacion.Asignaciones != null && cargarAsignacion.Asignaciones.Length > 0)
                {
                    cargasDetalle = cargarAsignacion.Asignaciones.ToList();
                }
            }

            if (cargasDetalle.Count > 0)
            {
                asignaciones = cargasDetalle.Select(p => new
                {
                    p.IdCargaAsignacionDetalle,
                    p.IdSolicitud,
                    p.Gestor,
                    p.Actividad,
                    p.Error,
                    p.Mensaje,
                    Editar = (p.IdAsignacion ?? 0) <= 0
                }).ToList();

                registros = cargasDetalle.Count;
                totalRegistros = cargarAsignacion.TotalRegistros;
            }
        }
        catch (Exception ex)
        {
        }
        model.draw++;
        return new
        {
            drawn = model.draw,
            recordsTotal = registros,
            recordsFiltered = totalRegistros,
            data = asignaciones,
            url
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ActualizarCargaDetalle(ActualizarCargaDetalleRequest peticion)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }
            if (peticion == null) throw new Exception("La petición es incorrecta");
            if (peticion.Accion < 0) throw new Exception("La acción no es válida");
            if (peticion.Accion > (int)Acciones.EliminarTodo && (peticion.Asignaciones == null || peticion.Asignaciones.Count == 0))
                throw new Exception("Debe proporcionar al menos asignación");

            using (wsSOPF.GestionClient wsG = new GestionClient())
            {
                ActualizarCargaAsignacionResponse actualizarR = wsG.ActualizarCargaAsignacion(new ActualizarCargaAsignacionRequest
                {
                    IdUsuario = idUsuario,
                    IdCargaAsignacion = peticion.IdCargaAsignacion,
                    Accion = peticion.Accion,
                    Error = peticion.Error,
                    Asignaciones = peticion.Asignaciones != null ? peticion.Asignaciones.ToArray() : null
                });

                if (actualizarR == null)
                {
                    error = true;
                    mensaje = "Ocurrió un error mientras se intentaba realizar las acciones solicitadas";
                }
                else
                {
                    error = actualizarR.Error;
                    mensaje = actualizarR.MensajeOperacion;
                }
            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object AsignarCarga(int idCarga)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object carga = null;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }

            using (GestionClient wsG = new GestionClient())
            {
                AsignarCargaAsignacionResponse asignarR = wsG.AsignarCargaAsignacion(new AsignarCargaAsignacionRequest
                {
                    IdUsuario = idUsuario,
                    IdCargaAsignacion = idCarga
                });

                if (asignarR != null)
                {
                    error = asignarR.Error;
                    mensaje = asignarR.MensajeOperacion;
                }
                else
                {
                    error = true;
                    mensaje = "Ocurrió un error mientras se realizaban las asignaciones";
                }
                CargaAsignacionResponse cargaAsigR = wsG.CargaAsignacion(new CargaAsignacionRequest { IdUsuario = idUsuario, IdCargaAsignacion = idCarga });
                if (cargaAsigR != null)
                {
                    carga = new
                    {
                        cargaAsigR.CargaAsignacion.IdCargaAsignacion,
                        cargaAsigR.CargaAsignacion.NombreArchivo,
                        cargaAsigR.CargaAsignacion.TotalRegistros,
                        cargaAsigR.CargaAsignacion.Actividad,
                        cargaAsigR.CargaAsignacion.FechaRegistro,
                        Asignar = cargaAsigR.CargaAsignacion.IdEstatus == 2,
                        Finalizado = new int[] { 4, 5, 6, 7 }.Contains(cargaAsigR.CargaAsignacion.IdEstatus),
                        cargaAsigR.CargaAsignacion.Estatus
                    };
                }
            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url, carga };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GuardarMotivoAsignacion(int idCarga, string motivo)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }

            using (GestionClient wsG = new GestionClient())
            {
                ActualizarCargaAsignacionResponse actualizarR = wsG.ActualizarCargaAsignacion(new ActualizarCargaAsignacionRequest
                {
                    IdUsuario = idUsuario,
                    IdCargaAsignacion = idCarga,
                    Accion = 3,
                    Actividad = motivo
                });

                if (actualizarR == null)
                {
                    error = true;
                    mensaje = "Ocurrió un error mientras se intentaba realizar las acciones solicitadas";
                }
                else
                {
                    error = actualizarR.Error;
                    mensaje = actualizarR.MensajeOperacion;
                }
            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url };
    }
    #endregion
    #endregion

    public class CargaAsignacionFiltro
    {
        public int IdCargaAsignacion { get; set; }
        public bool? Error { get; set; }
        public int? IdEstatus { get; set; }
    }

    public class ActualizarCargaDetalleRequest
    {
        public int Accion { get; set; }
        public bool? Error { get; set; }
        public int IdCargaAsignacion { get; set; }
        public List<CargaAsignacionDetalleVM> Asignaciones { get; set; }
    }
}