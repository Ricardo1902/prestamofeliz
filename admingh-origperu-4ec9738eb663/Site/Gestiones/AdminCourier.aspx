﻿<%@ Page Title="Administrar Envíos Courier" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AdminCourier.aspx.cs" Inherits="Site_Gestiones_AdminCourier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <%--<link href="css/admincourier.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />--%>
    <link href="css/admincourier.css" rel="stylesheet" />
    <link href="../../js/datatables/css/select.dataTables.min.css" rel="stylesheet" />
    <style>
        /* Start by setting display:none to make this hidden.
           Then we position it in relation to the viewport window
           with position:fixed. Width, height, top and left speak
           for themselves. Background we set to 80% white with
           our animation centered, and no-repeating */
        .modal-pr {
            display: none;
            position: fixed;
            z-index: 9000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 ) url('../../Imagenes/ajax-loader.gif') 50% 50% no-repeat;
        }

        /* When the body has the loading class, we turn
           the scrollbar off with overflow:hidden */
        body.loading .modal-pr {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
           modal element will be visible */
        body.loading .modal-pr {
            display: block;
        }
    </style>
    <script type="text/javascript" src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="../../js/datatables/1.10.19/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/1.10.19/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script type="text/javascript" src="../../js/datepicker-1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="../../js/datepicker-1.9.0/locales/bootstrap-datepicker.es.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="js/admincourier.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Administrar Envíos Courier</h3>
        </div>
        <div class="panel-body">
            <asp:Panel ID="pnlError" class="form-group col-sm-12" Visible="false" runat="server">                                
                <div class="alert alert-danger" style="padding: 5px 15px 5px 15px; margin: 10px 0px;">
                    <asp:Label ID="lblError" runat="server" />
                </div>
            </asp:Panel>
            <button id="btnNuevo" type="button" class="btn btn-success">Nuevo</button><br />
            <table id="tbEnvios" class="table display compact nowrap" style="width: 100%"></table>
        </div>
    </div>
    <div id="modalDetalleEnvio" class="modal fade modal-lg" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detalle de Envio Courier</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <table id="tEnvioCourierDetalle" class="display" style="width: 100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalNuevoEnvio" class="modal fade modal-lg" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Nuevo Envío Courier</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button style="border: none; background: transparent; font-size: 14px;" id="tableCheckAllButton">
                                        <i class="far fa-square"></i>  
                                </button>Seleccionar todo
                                <table id="tSolicitudNotificaciones" class="table table-striped table-bordered display" style="width:100%"></table>
                                <button id="btImprimir" type="button" class="btn btn-primary">Imprimir</button><br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-pr"></div>
    <%--<div id="divLoader" class="hidden">
        <div id="loader-background"></div>
        <div id="loader-content"></div>
    </div>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            closeNav();
        });
    </script>
</asp:Content>
