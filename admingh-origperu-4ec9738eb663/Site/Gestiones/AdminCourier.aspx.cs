﻿using System;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using wsSOPF;

public partial class Site_Gestiones_AdminCourier : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region WebMethos
    #region GET
    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static object CargarEnvios()
    {
        object resultado = null;
        string url = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }
            using (wsSOPF.GestionClient ws = new wsSOPF.GestionClient())
            {
                CargarEnviosCourierResponse res = ws.CargarEnviosCourier();
                if (!res.Error)
                {
                    return (from p in res.Resultado.Envios
                            select new
                            {
                                p.IdEnvioCourier,
                                p.UsuarioRegistro,
                                FechaRegistro = p.FechaRegistro.ToString("dd/MM/yyyy")
                            })
                            .ToList();
                }
                else
                    throw new Exception("No hay envios.");
            }
        }
        catch (Exception e)
        {
            resultado = new { Error = true, Mensaje = "Los envios no pudieron cargarse. " + e.InnerException.Message ?? "", Url = url };
        }
        return resultado;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerSolicitudNotificacionesCourier()
    {
        object resultado = null;
        string url = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }
            using (wsSOPF.GestionClient ws = new wsSOPF.GestionClient())
            {
                ObtenerSolicitudNotificacionesCourierResponse res = ws.ObtenerSolicitudNotificacionesCourier();
                if (!res.Error)
                {
                    resultado = (from p in res.Resultado.Notificaciones
                                select new
                                {
                                    p.IdSolicitudNotificacion,
                                    p.SolicitudId,
                                    p.NombreCliente,
                                    p.Notificacion,
                                    p.Destino,
                                    p.UsuarioRegistroNotificacion,
                                    FechaRegistro = p.FechaRegistroNotificacion.ToString("dd/MM/yyyy"),
                                    Error = p.Error ? "SÍ" : "NO",
                                    p.Mensaje
                                })
                                .ToList();
                }
                else
                {
                    resultado = new { Error = true, Mensaje = "Los envios no pudieron cargarse. " + res.MensajeErrorException ?? "", Url = url };
                }
            }
        }
        catch (Exception e)
        {
            resultado = new { Error = true, Mensaje = "Las notificaciones no pudieron cargarse. " + e.InnerException.Message ?? "", Url = url };
        }
        return resultado;
    }
    #endregion

    #region POST
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object VerEnvio(int IdEnvio)
    {
        object detalle = null;
        VerEnvioCourierDetalleResponse respuesta = new VerEnvioCourierDetalleResponse();
        try
        {
            using (wsSOPF.GestionClient ws = new wsSOPF.GestionClient())
            {
                var resp = ws.VerEnvio(new VerEnvioCourierDetalleRequest
                {
                    IdEnvioCourier = IdEnvio
                });
                if (resp != null && resp.Resultado != null && resp.Resultado.Envios != null)
                {
                    detalle = resp.Resultado.Envios.Select(s => new
                    {
                        s.IdEnvioCourier,
                        s.IdSolicitudNotificacion,
                        s.IdSolicitud,
                        s.Notificacion,
                        s.Destino,
                        s.UsuarioRegistroNotificacion,
                        FechaRegistroNotificacion = s.FechaRegistroNotificacion.ToString("dd/MM/yyyy HH:mm:ss"),
                        Error = s.Error ? "SÍ" : "NO",
                        s.Mensaje
                    });
                }
                else
                {
                    detalle = new { Error = true, MensajeOperacion = resp.MensajeOperacion ?? "Ha ocurrido un error." };
                }
            }
        }
        catch (Exception ex)
        {
            detalle = new { Error = true, MensajeOperacion = ex.InnerException.Message ?? "Ha ocurrido un error." };
        }
        return detalle;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ImprimirNotificacionesCourier(int[] peticion)
    {
        object resultado = null;
        string url = string.Empty;
        ImprimirNotificacionesCourierResponse notificacionResultado = new ImprimirNotificacionesCourierResponse();
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }
            using (wsSOPF.GestionClient ws = new wsSOPF.GestionClient())
            {
                notificacionResultado = ws.ImprimirNotificacionesCourier(new ImprimirNotificacionesCourierRequest()
                {
                    Notificaciones = peticion,
                    IdUsuario = idUsuario
                });
                if (!notificacionResultado.Error && notificacionResultado.Resultado != null && notificacionResultado.Resultado.ContenidoArchivo.Length > 0)
                {
                    string nombreDocumento = "EnvioCourier_" + DateTime.Now.ToString("ddMMyy");
                    resultado = new
                    {
                        Error = false,
                        Mensaje = "Las notificaciones se han generado exitosamente.",
                        Tipo = "application/pdf",
                        Longitud = notificacionResultado.Resultado.ContenidoArchivo.Length.ToString(),
                        NombreArchivo = nombreDocumento,
                        Contenido = Convert.ToBase64String(notificacionResultado.Resultado.ContenidoArchivo)
                    };
                }
                else
                {
                    resultado = new { Error = true, Mensaje = "Las notificaciones no pudieron generarse. " + notificacionResultado.MensajeOperacion, Url = url };
                }
            }
        }
        catch (Exception ex)
        {
            resultado = new { Error = true, Mensaje = ex.InnerException.Message ?? "Ha ocurrido un error.", Url = url };
        }
        return resultado;
    }
    #endregion
    #endregion
}
