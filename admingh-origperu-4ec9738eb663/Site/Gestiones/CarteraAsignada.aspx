<%@ Page Title="Cartera Asignada" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CarteraAsignada.aspx.cs" Inherits="Site_Gestiones_CarteraAsignada" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTables.responsive.min.css" rel="stylesheet" />
    <link href="../../js/datatables/css/buttons.dataTables.min.css" rel="stylesheet" />
    <link href="../../Css/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <link href="css/CarteraAsignada.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />


    <style>
        .modal-pr {
            background: rgba( 255, 255, 255, .8 ) url('../../Imagenes/ajax-loader.gif') 50% 50% no-repeat;
        }
    </style>
    <script type="text/javascript" src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables_plugins/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables_plugins/buttons.flash.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables_plugins/jszip.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables_plugins/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables_plugins/vfs_fonts.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables_plugins/buttons.html5.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables_plugins/buttons.print.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Scripts/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/tooltip.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/dropdown.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="../../js/gestiones/CarteraAsignada.js?v=<%=AppSettings.VersionPub %>"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header page-header-corp">
        <div class="row inline">
            <h3 style="display: inline;">Cartera Asignada</h3>
            <label id="lblFechaActual" class="pull-right badge badge-large btn-primary">{{FechaActual}}</label>
        </div>
    </div>
    <div class="row row-clear resumen-ca collapse" id="rowTotales">
        <div id="secTodas" class="col-md-3 col-resumen-ca default">
            <span>Cuentas Totales</span>
            <p id="bqTodas">{{total}}</p>
            <em></em>
        </div>
        <div id="secAsignadas" class="col-md-3 col-resumen-ca default" grupo="2">
            <span>Cuentas Asignadas</span>
            <p id="bqAsignadas">{{asignadas}}</p>
            <em></em>
        </div>
        <div id="secGestionadas" class="col-md-3 col-resumen-ca text-success" grupo="1">
            <span>Cuentas Gestionadas</span>
            <p id="bqGestionadas">{{gestionadas}}</p>
            <em></em>
        </div>
        <div id="secPendientes" class="col-md-3 col-resumen-ca text-danger" grupo="0">
            <span>Pendientes de Gestion</span>
            <p id="bqPendientes">{{pendientes}}</p>
        </div>
        <%--<div id="fotoGestor" class="col-md-4 col-resumen-ca">
            <span>Pendientes de Gestion</span>
            <p id="bqPendientes">{{pendientes}}</p>
        </div>--%>
    </div>
    <div class="row row-clear">
        <div class="col-md-12">
            <table id="tCarteraAsignada" class="display compact" style="width: 100%"></table>
        </div>
    </div>

    <div class="modal-pr"></div>
</asp:Content>
