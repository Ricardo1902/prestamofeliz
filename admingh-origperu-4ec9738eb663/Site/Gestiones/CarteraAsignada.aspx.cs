﻿using DataTables;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using wsSOPF;

public partial class Site_Gestiones_CarteraAsignada : System.Web.UI.Page
{
    private StringBuilder strScript = new StringBuilder();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0) throw new Exception("La clave del usuario no es válida");

            ValidarPermisoAdmin();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
        }
    }


    #region PrivateMethods
    public void ObtenerGestoresActivos()
    {
        List<TBCatGestores> gestores = new List<TBCatGestores>();
        try
        {
            using (GestionClient wsGestiones = new GestionClient())
            {
                var respuestaCartera = wsGestiones.ObtenerGestor(5, 0, 0, 0);
                if (respuestaCartera != null && respuestaCartera.Length > 0)
                {
                    gestores = respuestaCartera.Select(gestor => new TBCatGestores { IdGestor = gestor.IdGestor, Nombre = gestor.Nombre }).OrderBy(gestor => gestor.IdGestor).ToList();
                    strScript.AppendFormat("gestores={0};", JsonConvert.SerializeObject(gestores));
                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    public void ValidarPermisoAdmin()
    {
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario > 0)
            {
                using (UsuarioClient wsUsuarios = new UsuarioClient())
                {
                    var esAdmin = wsUsuarios.EsAdministrador(0, idUsuario);
                    strScript.AppendFormat("esAdmin={0};", JsonConvert.SerializeObject(esAdmin));

                    if (esAdmin)
                    {
                        ObtenerGestoresActivos();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    #endregion

    #region WebMethods
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerCarteraAsignada(ObtenerCarteraAsignadaRequest peticion)
    {
        if (peticion == null) throw new Exception("La petición no es válida");

        int idUsuario = HttpContext.Current.ValidarUsuario();
        if (idUsuario <= 0) throw new Exception("Usuario Invalido");

        peticion.IdUsuario = idUsuario;
        List<Sel_ObtenerCarteraAsignadaResult> cartera = new List<Sel_ObtenerCarteraAsignadaResult>();
        ObtenerCarteraAsignadaTotales totales = null;

        try
        {
            using (GestionClient wsGestiones = new GestionClient())
            {
                var respuestaCartera = wsGestiones.ObtenerCarteraAsignada(peticion);
                if (respuestaCartera != null && respuestaCartera.Resultado != null)
                {
                    if (respuestaCartera.Resultado.Cartera != null)
                    {
                        //Se crea tabal temporal con la informacion ordenada por recibos vencidos
                        var tempTablaOrdenada = respuestaCartera.Resultado.Cartera.OrderBy(c => c.RecibosVencidos).ToList();

                        ////Se agregan los registros con vencidos entre 1 - 3 primero y despues los demas
                        //cartera = tempTablaOrdenada.Where(c => c.RecibosVencidos > 0 && c.RecibosVencidos < 4).ToList();

                        //Se agregan los registros, dejando aquellos con 0 recibos vencidos al final de la lista
                        cartera.AddRange(tempTablaOrdenada.Where(c => c.RecibosVencidos > 0));
                        cartera.AddRange(tempTablaOrdenada.Where(c => c.RecibosVencidos <= 0));

                        //Se limpia tabla temporal
                        tempTablaOrdenada = null;
                    }

                    if (respuestaCartera.Resultado.Totales != null)
                        totales = respuestaCartera.Resultado.Totales;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Ha ocurrido un error: " + ex);
        }

        return new
        {
            cartera = cartera,
            totales = totales
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerCarteraAsignadaPaginada(DataTableAjaxPostModel model, ObtenerCarteraAsignadaRequest peticion)
    {

        if (peticion == null) throw new Exception("La petición no es válida");

        int idUsuario = HttpContext.Current.ValidarUsuario();
        if (idUsuario <= 0) throw new Exception("Usuario Invalido");

        peticion.IdUsuario = idUsuario;

        string mensaje = string.Empty;
        int totalRegistros = 0;
        int registros = 0;
        ObtenerCarteraAsignadaTotales totales = null;
        object carteraAsignada = null;
        string url = string.Empty;
        try
        {
            peticion.IdUsuario = idUsuario;
            peticion.Pagina = (model.start / model.length) + 1;
            peticion.RegistrosPagina = model.length;

            ObtenerCarteraAsignadaResponse respuestaCartera = null;
            List<Sel_ObtenerCarteraAsignadaResult> cartera = new List<Sel_ObtenerCarteraAsignadaResult>();

            using (GestionClient wsGestiones = new GestionClient())
            {
                respuestaCartera = wsGestiones.ObtenerCarteraAsignada(peticion);
                if (respuestaCartera != null && respuestaCartera.Resultado != null)
                {
                    if (respuestaCartera.Resultado.Cartera != null)
                        cartera = respuestaCartera.Resultado.Cartera.ToList();

                    if (respuestaCartera.Resultado.Totales != null)
                        totales = respuestaCartera.Resultado.Totales;
                }
            }

            if (cartera.Count > 0)
            {
                carteraAsignada = cartera.ToList();
                registros = cartera.Count;
                if (totales != null)
                    totalRegistros = totales.RegistrosTotales;
            }
        }
        catch (Exception ex)
        {
        }
        model.draw++;
        return new
        {
            drawn = model.draw,
            recordsTotal = registros,
            recordsFiltered = totalRegistros,
            data = carteraAsignada,
            url,
            totales
        };
    }


    [WebMethod(EnableSession = true)]
    public static object RegistrarLogDescarga()
    {
        return new { };
    }
    #endregion
}
