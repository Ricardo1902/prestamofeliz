﻿<%@ Page Title="Gestion" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Gestion.aspx.cs" Inherits="Site_Gestiones_Gestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../Content/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="css/Gestion.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />
    <link href="css/ArchivosGestion.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />

    <style>
        /* Start by setting display:none to make this hidden.
           Then we position it in relation to the viewport window
           with position:fixed. Width, height, top and left speak
           for themselves. Background we set to 80% white with
           our animation centered, and no-repeating */
        .modal-pr {
            display: none;
            position: fixed;
            z-index: 9000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 ) url('../../Imagenes/ajax-loader.gif') 50% 50% no-repeat;
        }

        /* When the body has the loading class, we turn
           the scrollbar off with overflow:hidden */
        body.loading .modal-pr {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
           modal element will be visible */
        body.loading .modal-pr {
            display: block;
        }
    </style>
    <script type="text/javascript" src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/tooltip.js"></script>

    <script type="text/javascript" src="../../Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Scripts/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../../Scripts/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="../../js/gestiones/Gestion.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
    <script src="../../js/download2.js"></script>
    <script type="text/javascript" src="../../js/utils.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
    <script type="text/javascript" src="js/ArchivosGestion.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>

    <div class="page-header page-header-corp">
        <h3>Resumen de Gestión</h3>
    </div>
    <div class="row row-clear">
        <div class="col-md-1">
            <div class="form-group">
                <asp:Button ID="btRegresar" CssClass="btn btn-default" Text="Regresar" OnClick="btRegresar_Click" OnClientClick="$('body').addClass('loading'); $('.modal-pr').appendTo('body');" runat="server" />
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">Solicitud</span>
                    <input type="text" id="txCuenta" class="form-control" disabled="disabled" runat="server" />
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <asp:Button ID="btnActualizarCliente" CssClass="btn btn-primary" Text="Actualización de Datos" OnClick="btnActualizarCliente_Click" OnClientClick="$('body').addClass('loading'); $('.modal-pr').appendTo('body');" runat="server" />
            </div>
        </div>
    </div>

    <div class="row row-clear">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos del Cliente</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3"><small><strong>Nombre:</strong></small> </div>
                        <div class="col-md-9">
                            <input type="text" id="txNombreCliente" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"><small><strong>DNI:</strong></small> </div>
                        <div class="col-md-9">
                            <input type="text" id="txDNICliente" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"><small><strong>Domicilio Legal:</strong></small> </div>
                        <div class="col-md-9">
                            <textarea id="txDomicilioLegal" runat="server" class="span6 form-control" rows="2" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"><small><strong>Distrito:</strong></small> </div>
                        <div class="col-md-9">
                            <input type="text" id="txDistrito" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"><small><strong>Referencia Domiciliaria:</strong></small> </div>
                        <div class="col-md-9">
                            <input type="text" id="txReferenciaDomiciliaria" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"><small><strong>Empresa:</strong></small> </div>
                        <div class="col-md-9">
                            <input type="text" id="txRazonSocialEmpresa" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"><small><strong>S. Laboral:</strong></small> </div>
                        <div class="col-md-9">
                            <input type="text" id="txSituacionLaboral" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"><small><strong>Producto:</strong></small> </div>
                        <div class="col-md-9">
                            <input type="text" id="txProducto" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"><small><strong>E. Venta:</strong></small> </div>
                        <div class="col-md-9">
                            <input type="text" id="txEjecutivoVenta" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left" style="padding-top: 7.5px;">Datos del Crédito</h4>
                    <div class="input-group pull-right">
                        <span id="btExpediente" class="btn btn-info" style="cursor: pointer;" title="Ver Expediente Digital" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-paperclip"></span></span>
                        <span id="btVerBalance" class="btn btn-info" style="cursor: pointer;" title="Ver Estado de Cuenta" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-search"></span></span>
                        <span id="btVerLiquidacion" class="btn btn-info" style="cursor: pointer;" title="Ver Liquidación" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-usd"></span></span>
                        <span id="btnCronograma" class="btn btn-info" style="cursor: pointer;" title="Ver Cronograma" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-list-alt"></span></span>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-5"><small><strong>Monto del crédito:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txMontoCredito" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Plazo:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txPlazo" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Cuota:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txCuota" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Cuotas Vencidos:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txRecibosVencidos" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Días Mora:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txDiasMora" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Rango de mora:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txFlagMora" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Saldo Vencido:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txSaldoVencido" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Estatus Cliente:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txEstatusCliente" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Último Pago:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txFechaUltimoPago" runat="server" class="form-control" disabled="disabled" />
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos de Cobro</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-5"><small><strong>Día Pago:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txDiaPago" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Banco:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txBanco" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Cuenta Bancaria:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txCuentaBancaria" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <asp:LinkButton ID="itxNumeroTarjeta" runat="server" Enabled="false">
                                <span aria-hidden="true" class="glyphicon glyphicon-cog" data-toggle="tooltip" data-placement="top" title="Obtenida por Actualizador"></span>
                            </asp:LinkButton>
                            <small><strong>Número Tarjeta:</strong></small>
                        </div>
                        <div class="col-md-7">
                            <input type="text" id="txNumeroTarjeta" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <asp:LinkButton ID="itxMesVencimiento" runat="server" Enabled="false" ToolTip="Obtenida por Actualizador">
                                <span aria-hidden="true" class="glyphicon glyphicon-cog"></span>
                            </asp:LinkButton>
                            <small><strong>Mes Vencimiento:</strong></small>
                        </div>
                        <div class="col-md-7">
                            <input type="text" id="txMesVencimiento" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <asp:LinkButton ID="itxAnioVencimiento" runat="server" Enabled="false" ToolTip="Obtenida por Actualizador">
                                <span aria-hidden="true" class="glyphicon glyphicon-cog"></span>
                            </asp:LinkButton>
                            <small><strong>Año Vencimiento:</strong></small>
                        </div>
                        <div class="col-md-7">                            
                            <input type="text" id="txAnioVencimiento" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Fecha Último Proceso Gestión:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txFechaUltimoProceso" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Proceso Cobro:</strong></small> </div>
                        <div class="col-md-7">
                            <input type="text" id="txProcCobro" runat="server" class="form-control" disabled="disabled" value="Visanet" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5"><small><strong>Proceso Cobro Gestión:</strong></small> </div>
                        <div class="col-md-7">
                            <textarea id="txProcesoCobro" runat="server" class="span6 form-control" rows="4" disabled="disabled" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5" style="display: none;"><small><strong>Mensaje Banco:</strong></small> </div>
                        <div class="col-md-7" style="display: none;">
                            <input type="text" id="txMensajeBanco" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="secTipificacionTel" class="row row-clear">
        <div class="col-md-12">
            <div class="panel panel-default" style="border-color: #32CD32;">
                <div class="panel-heading-custom panel-heading">
                    <h3 class="panel-title">Tipificación de Gestión Telefónica</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <table id="tContactos" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Teléfono</th>
                                    <th>Tipo</th>
                                    <th>Anexo/Referencia</th>
                                    <th style="width: 30px">
                                        <button id="opLlamar" type="button" class="btn btn-success" disabled="disabled" onclick="selTelefonoContacto();">
                                            <span class="glyphicon glyphicon-earphone"></span>
                                        </button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-3">
                                <span class="seccion-etiqueta">Estado de Llamada:</span>
                            </div>
                            <div class="col-md-5">
                                <select id="selEstadoLlamada" class="form-control" disabled="disabled">
                                    <option>Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="seccion-etiqueta">Fecha en Agenda:</span>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <input type="text" id="txFechaAgenda" class="form-control" disabled="disabled" />
                                    <span class="input-group-addon" style="padding: 0px 1px;">
                                        <button id="opAgenda" type="button" class="btn btn-success" disabled>
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="seccion-etiqueta">Observación:</span>
                            </div>
                            <div class="col-md-9">
                                <textarea id="textObservacion" class="form-control" placeholder="Capture la observación con un máximo de 2000 caracteres" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-com-derecha">
                                <button type="button" id="btnGuardarGestTel" class="btn btn-success">
                                    <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row row-clear">
        <div class="col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Ingreso de Gestión</h3>
                </div>
                <div class="panel-body">
                    <table id="tgestiones" class="table table-hover display compact" style="width: 100%;"></table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left" style="padding-top: 7.5px;">Envío de Notificación</h4>
                    <div class="input-group pull-right">
                        <span id="btEscanearNotificacion" class="btn btn-info" style="cursor: pointer;" title="Escanear notificación"><span class="glyphicon glyphicon-upload"></span></span>
                        <span id="btBitacora" class="btn btn-info" style="cursor: pointer;" title="Ver bitácora"><span class="glyphicon glyphicon-book"></span></span>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="selDestino">Destino</label>
                                <select id="selDestino" class="form-control"></select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="checkbox inline">
                                    <label>
                                        <input id="chkEnvioCorreo" type="checkbox">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        Envio correo
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkbox inline">
                                    <label>
                                        <input id="chkCourier" type="checkbox">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        Envío Courier
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <label for="selNotificacion">Notificación</label>
                                <select id="selNotificacion" class="form-control"></select>
                            </div>
                            <br />
                            <div class="col-md-3 input-group pull-right">
                                <span id="btDescargarNotificacion" class="btn btn-info" style="cursor: pointer;" title="Descargar, enviar o guardar notificación"><span class="glyphicon glyphicon-download"></span></span>
                                <span id="btActualizarNotificacion" class="btn btn-info" style="cursor: pointer;" title="Actualizar datos y descargar, enviar o guardar"><span class="glyphicon glyphicon-refresh"></span></span>
                            </div>
                        </div>
                        <div id="dNotificacionCitacion" class="row">
                            <div class="col-sm-6">
                                <label for="txFechaCitacion">¿Fecha de la Citación? (formato dd/MM/yyyy)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span
                                        class="glyphicon glyphicon-calendar"></span></span>
                                    <input type="text" id="txFechaCitacion" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="txHoraCitacion">¿Hora de la Citación? (formato: 23:59)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span
                                        class="glyphicon glyphicon-time"></span></span>
                                    <input type="text" id="txHoraCitacion" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <label for="selNotificacionPromocion">Promoción</label>
                                <select id="selNotificacionPromocion" class="form-control"></select>
                            </div>
                            <br />
                            <div class="col-md-3 input-group pull-right">
                                <span id="btDescargarNotificacionPromocion" class="btn btn-info" style="cursor: pointer;" title="Descargar/enviar promoción"><span class="glyphicon glyphicon-download"></span></span>
                                <span id="btActualizarNotificacionPromocion" class="btn btn-info" style="cursor: pointer;" title="Actualizar promoción y descargar o enviar"><span class="glyphicon glyphicon-refresh"></span></span>
                            </div>
                        </div>
                        <div id="dNotificacionPromocion" class="row">
                            <div class="col-sm-6">
                                <label for="txDescuentoPromocion">Descuento Promoción</label>
                                <div class="input-group">
                                    <span class="input-group-addon">S/</span>
                                    <input type="text" id="txDescuentoPromocion" class="form-control"
                                        onkeypress="return ValidaMonto(this)" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="txVigenciaPromocion">Vigencia Promoción (formato dd/MM/yyyy)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span
                                        class="glyphicon glyphicon-calendar"></span></span>
                                    <input type="text" id="txVigenciaPromocion" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalVerBalance" class="modal fade modal-lg" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Estado de Cuenta</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Tabla de Amortización</h4>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <table id="tTablaAmortizacion" class="display" style="width: 100%"></table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Pagos Realizados</h4>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <table id="tPagosRealizados" class="display" style="width: 100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalVerLiquidacion" class="modal fade modal-lg" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Liquidación</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-3">
                        <label class="small">N° Solicitud</label>
                        <pre><label id="lblIdSolicitud_Liquidacion" /></pre>
                    </div>
                    <div class="col-sm-3">
                        <label class="small">DNI</label>
                        <pre><label id="lblDNI_Liquidacion"></label></pre>
                    </div>
                    <div class="col-sm-6">
                        <label class="small">Cliente</label>
                        <pre><label id="lblCliente_Liquidacion"></label></pre>
                    </div>
                    <div class="col-sm-4">
                        <label class="small">Monto Credito</label>
                        <pre><label id="lblMontoCredito_Liquidacion"></label></pre>
                    </div>
                    <div class="col-sm-4">
                        <label class="small">Cuota</label>
                        <pre><label id="lblCuota_Liquidacion"></label></pre>
                    </div>
                    <div class="col-sm-4">
                        <label class="small">Total a Liquidar</label>
                        <pre><label id="lblTotalLiquidar_Liquidacion"></label></pre>
                    </div>
                    <div class="col-sm-4">
                        <label class="small">Fecha Credito</label>
                        <pre><label id="lblFechaCredito_Liquidacion"></label></pre>
                    </div>
                    <div class="col-sm-4">
                        <label class="small">Fecha 1er. Pago</label>
                        <pre><label id="lblFechaPrimerPago_Liquidacion"></label></pre>
                    </div>
                    <label for="txFechaLiquidacion_Liquidacion">Fecha Liquidación</label>
                    <div class="col-md-4 input-group margin-bottom-lg">
                        <input type="text" id="txFechaLiquidacion_Liquidacion" name="txtMontoLiquidacion" disabled="disabled" class="disabled form-control" />
                        <span class="input-group-addon"><a id="btLiquidar" data-toggle="tooltip" title="Ir a Liquidación" style="cursor: pointer;"><i class="fas fa-search-dollar"></i></a></span>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <table id="tLiquidacion" class="display" style="width: 100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalBitacoraNotificacion" class="modal fade modal-lg" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Bitácora de Notificaciones</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <table id="tBitacora" class="display" style="width: 100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalFechaAgenda" class="modal fade" role="dialog">
        <div class="modal-dialog" role="dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Seleccione una fecha de agenda</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="txFechaAgendaSel">Fecha:</label>
                            <input type="text" id="txFechaAgendaSel" class="form-control" />
                        </div>
                        <div class="col-md-6">
                            <label for="txHoraAgendaSel">Hora:</label>
                            <input type="text" id="txHoraAgendaSel" class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <textarea id="txCometnarioAg" class="form-control" placeholder="Captura un comentario con un máximo de 2000 caracteres"></textarea>
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" id="chSoloAmiAg" class="checkbox-inline" checked="checked" />
                            <label for="chSoloAmiAg">Agenda solamente a mí</label>
                        </div>
                    </div>
                    <div id="dGestores" class="row collapse">
                        <div class="col-md-12 form-group">
                            <label for="selGestores">Gestor a notificar:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-user"></span>
                                </div>
                                <select id="selGestores" class="form-control"></select>
                                <div class="input-group-addon" style="padding: 0px 1px">
                                    <button type="button" id="btnSelGestor" class="btn btn-primary" style="padding: 4px 8px">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <ul id="gestoreNotificar" class="list-group">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-com-derecha">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="button" id="btnAgendar" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalArchivosGestion" class="modal fade modal-lg" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Escaneos</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pArchivosGestion" class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Archivos de Gestión</h3>
                                </div>
                                <div class="panel-body file-manager-body">
                                    <div class="row file-manager-filter">
                                        <div class="col-md-4 collapse">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" id="txtBuscarArchivo" placeholder="Buscar" runat="server"
                                                        class="form-control" />
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="btn-toolbar" role="toolbar" aria-label="Controles archivo">
                                                    <div class="btn-group" role="group" aria-label="...">
                                                        <button id="btnAgregarArchivoGestion" type="button" class="btn btn-primary">
                                                            <i class="fas fa-file-upload"></i>Subir archivo
                                                        </button>
                                                    </div>
                                                    <div class="btn-group" role="group" aria-label="...">
                                                        <button id="btnActualizarListaArchivosGestion" type="button"
                                                            class="btn btn-info">
                                                            <i class="fas fa-sync-alt">Actualizar lista</i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="margin-left: 0px; margin-right: 0px;">
                                            <div id="secListaArchivosGestion" class="file-manager">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalNuevaGestion" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Ingresar Gestión</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="selTipoGestion_AltaGestion">Descripcion de Gestión</label>
                                <select id="selTipoGestion_AltaGestion" name="selTipoGestion_AltaGestion" class="selectpicker form-control" required data-live-search="true">
                                    <option value="0">Seleccione una opcion</option>
                                </select>
                            </div>
                            <div class="form-group tipoCobro_gestion">
                                <label for="selTipoCobro_AltaGestion">Medio de Cobro</label>
                                <select id="selTipoCobro_AltaGestion" name="selTipoGestion_AltaGestion" class="selectpicker form-control" data-live-search="true">
                                    <option value="0">Seleccione una opcion</option>
                                </select>
                            </div>
                            <div class="form-group monto_gestion">
                                <label for="txtMonto_AltaGestion">Monto</label>
                                <div class="input-group">
                                    <span class="input-group-addon">S/</span>
                                    <input type="text" class="form-control" name="txtMonto_AltaGestion" id="txtMonto_AltaGestion"
                                        data-parsley-pattern="^[0-9]*(\.[0-9]+)?$" data-parsley-pattern-message="El monto es inválido"
                                        data-parsley-trigger="change" />
                                </div>
                            </div>
                            <div class="fecha_gestion">
                                <div class="form-group col-md-6">
                                    <label for="txFecha_gestion">Fecha:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        <input type="text" id="txFecha_gestion" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group col-md-6 hora-gestion">
                                    <label for="txFecha_gestion">Hora:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                        </span>
                                        <input type="text" id="txHora_gestion" class="form-control" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group resultado_gestion">
                                <label for="selResultadoGestion_AltaGestion">Resultado de gestión</label>
                                <select id="selResultadoGestion_AltaGestion" name="selResultadoGestion_AltaGestion" class="form-control" required data-live-search="true">
                                    <option value="0">Seleccione una opcion</option>
                                </select>
                            </div>
                            <div class="form-group collapse">
                                <label for="">Generar recordatorio</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" id="btnGuardarGestion" class="btn btn-success"
                        value="Guardar Gestion" />
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cancelar" />
                </div>
            </div>
        </div>
    </div>
    <div id="modalRecordatorio" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Generar Recordatorio</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Medio de envío:</label>
                                <div class="checkbox inline">
                                    <label>
                                        <input id="chkEmail" value="8" type="checkbox">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        Email
                                    </label>
                                    <label>
                                        <input id="chkSMS" value="7" type="checkbox" disabled="disabled">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        SMS (Pendiente)
                                    </label>
                                    <label>
                                        <input id="chkWhatsapp" value="7" type="checkbox" disabled="disabled">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        Whatsapp (Pendiente)
                                    </label>
                                    <label>
                                        <input id="ChkIVR" value="7" type="checkbox" disabled="disabled">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        Mensaje de voz (Pendiente)
                                    </label>
                                </div>
                            </div>
                        </div>
                        <%--    <div class="col-md-6">
                            <div class="form-group">
                                <div class="checkbox inline">
                                </div>
                            </div>
                        </div>--%>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtFecha_recordatorio">Fecha:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    <input type="text" id="txtFecha_recordatorio" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtHora_recordatorio">Hora:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                    <input type="text" id="txtHora_recordatorio" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" id="btnGuardarRecordatorio" class="btn btn-success"
                        value="Guardar Recordatorio" />
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cancelar" />
                </div>
            </div>
        </div>
    </div>

    <div id="secSubirArchivoGestion" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Subir archivo de gestión</h4>
                </div>
                <div class="modal-body">
                    <div id="dmensajes" class="row" style="display: none">
                    </div>
                    <div class="row">
                        <div class="col-md-12 collapse">
                            <div class="form-group">
                                <label>Tipo de Archivo:</label>
                                <select name="seltipoMimeArchivoGestion" id="seltipoMimeArchivoGestion"
                                    class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Tipo de Documento:</label>
                                <select name="selDocumentoDestino" id="selDocumentoDestino" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group" style="overflow: hidden; white-space: nowrap;">
                                <label>Seleccionar archivo:</label>
                                <input type="file" name="fArchivoGestion" id="fArchivoGestion"
                                    style="text-overflow: ellipsis;" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="txtComentarioArchivoGestion">Comentario</label>
                                <input type="text" name="txtComentarioArchivoGestion" class="form-control"
                                    id="txtComentarioArchivoGestion">
                            </div>
                        </div>
                    </div>
                    <div id="dContenido"></div>
                </div>
                <div class="modal-footer">
                    <input type="button" id="btnSubirArchivoGestion" class="btn btn-success"
                        value="Subir Archivo" />
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cancelar" />
                </div>
            </div>
        </div>
    </div>
    <div id="secVisualizarArchivo" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Vista Previa</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div id="dPreview" class="col-md-12">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cerrar" />
                </div>
            </div>
        </div>
    </div>
    <nav id="context-menu" class="context-menu">
        <ul class="context-menu__items">
            <li class="context-menu__item">
                <a href="#" id="preview_link" class="context-menu__link" data-action="View"><i
                        class="fas fa-file-download"></i> Visualizar</a>
                <a href="#" class="context-menu__link" data-action="Download"><i class="fas fa-file-download"></i>
                    Descargar</a>
                <a href="#" class="context-menu__link" data-action="Delete"><i class="fa fa-times"></i> Eliminar</a>
            </li>
        </ul>
    </nav>
    <div class="modal-pr"></div>
    <%--<div id="divLoader" class="hidden">
        <div id="loader-background"></div>
        <div id="loader-content"></div>
    </div>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            closeNav();
        });
    </script>
    <script type="text/javascript" src="js/frmGestionesContextMenu.js"></script>
</asp:Content>
