﻿using DataTables;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using wsSOPF;

public partial class Site_Gestiones_Gestion : System.Web.UI.Page
{
    private int IdGestor = 0;

    #region Datos
    private int IdSolicitud
    {
        get { return ViewState[UniqueID + "_IdSolicitud"] != null ? (int)ViewState[UniqueID + "_IdSolicitud"] : 0; }
        set { ViewState[UniqueID + "_IdSolicitud"] = value; }
    }
    private int IdCliente
    {
        get { return ViewState[UniqueID + "_IdCliente"] != null ? (int)ViewState[UniqueID + "_IdCliente"] : 0; }
        set { ViewState[UniqueID + "_IdCliente"] = value; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        int idCuenta = 0;
        int idSolicitud = 0;
        int idCliente = 0;
        decimal cuotaCliente = 0;
        int cuotasVencidas = 0;

        StringBuilder strScript = new StringBuilder();
        if (Request.QueryString["idsolicitud"] != null)
        {
            try
            {
                int.TryParse(Request.QueryString["idsolicitud"].ToString(), out idSolicitud);
                using (wsSOPF.CreditoClient wsC = new wsSOPF.CreditoClient())
                {
                    var creditos = wsC.ObtenerCredito(1, idSolicitud);
                    if (creditos != null && creditos.Length > 0)
                    {
                        idCuenta = (int)creditos[0].IdCuenta;
                        idCliente = (int)creditos[0].idCliente;
                        strScript.AppendFormat("idCuenta={0};", idCuenta);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        IdSolicitud = idSolicitud;
        IdCliente = idCliente;
        txCuenta.Value = IdSolicitud.ToString();
        int idUsuario = HttpContext.Current.ValidarUsuario();
        if (idUsuario <= 0) throw new Exception("Usuario Invalido");

        if (idCuenta > 0)
        {
            try
            {
                wsSOPF.ObtenerDatosClienteGestionResponse datosClienteRs = new wsSOPF.ObtenerDatosClienteGestionResponse();

                #region LinksReportes
                //imgTabAmorti.Attributes.Add("onClick", "window.open('../Creditos/frmTablaAmortizacion.aspx?idcuenta=" + idCuenta + "', '_TablaAmortizacion', 'width=1000,height=500,resizable=1,scrollbars=yes');return false;");
                //imgHistoPago.Attributes.Add("onClick", "window.open('../Creditos/frmHistorialPagos.aspx?idcuenta=" + idCuenta + "', '_HistoralPagos', 'width=1000,height=500,resizable=1,scrollbars=yes');return false;");
                #endregion

                using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
                {
                    #region Notificaciones
                    List<wsSOPF.TB_CATNotificaciones> notificaciones = new List<wsSOPF.TB_CATNotificaciones>();
                    wsSOPF.TB_CATNotificaciones seleccionNotificaciones = new wsSOPF.TB_CATNotificaciones() { IdNotificacion = -1, Notificacion = "Seleccione una opción" };
                    var respNotificaciones = wsG.ObtenerNotificaciones();
                    if (respNotificaciones.Resultado != null && respNotificaciones.Resultado.Notificaciones != null && respNotificaciones.Resultado.Notificaciones.Length > 0)
                    {
                        notificaciones = respNotificaciones.Resultado.Notificaciones.ToList();
                        notificaciones.Insert(0, seleccionNotificaciones);
                    }
                    else
                    {
                        notificaciones.Add(seleccionNotificaciones);
                    }
                    strScript.AppendFormat("notificaciones={0};", JsonConvert.SerializeObject(notificaciones));
                    #endregion

                    #region NotificacionPromociones
                    List<wsSOPF.TB_CATNotificaciones> notificacionPromociones = new List<wsSOPF.TB_CATNotificaciones>();
                    wsSOPF.TB_CATNotificaciones seleccionNotificacionesPromociones = new wsSOPF.TB_CATNotificaciones() { IdNotificacion = -1, Notificacion = "Seleccione una opción" };
                    var respNotificacionesPromociones = wsG.ObtenerNotificacionPromociones();
                    if (respNotificacionesPromociones.Resultado != null && respNotificacionesPromociones.Resultado.Notificaciones != null && respNotificacionesPromociones.Resultado.Notificaciones.Length > 0)
                    {
                        notificacionPromociones = respNotificacionesPromociones.Resultado.Notificaciones.ToList();
                        notificacionPromociones.Insert(0, seleccionNotificaciones);
                    }
                    else
                    {
                        notificacionPromociones.Add(seleccionNotificaciones);
                    }
                    strScript.AppendFormat("notificacionPromociones={0};", JsonConvert.SerializeObject(notificacionPromociones));
                    #endregion

                    #region Destinos
                    //TODO: BACK ORDER BY IDDESTINO
                    List<wsSOPF.TB_CATDestinoNotificacion> destinos = new List<wsSOPF.TB_CATDestinoNotificacion>();
                    wsSOPF.TB_CATDestinoNotificacion seleccionDestinos = new wsSOPF.TB_CATDestinoNotificacion() { IdDestinoNotificacion = -1, DestinoNotificacion = "Seleccione una opción" };
                    var respDestinos = wsG.ObtenerDestinos();
                    if (respDestinos.Resultado != null && respDestinos.Resultado.Destinos != null && respDestinos.Resultado.Destinos.Length > 0)
                    {
                        destinos = respDestinos.Resultado.Destinos.ToList();
                        destinos.Insert(0, seleccionDestinos);
                    }
                    else
                    {
                        destinos.Add(seleccionDestinos);
                    }
                    strScript.AppendFormat("destinos={0};", JsonConvert.SerializeObject(destinos));
                    # endregion

                    #region TipoContacto
                    List<wsSOPF.CatTipoContaco> tipoContactos = new List<wsSOPF.CatTipoContaco>();
                    wsSOPF.CatTipoContaco seleccionTipoContacto = new wsSOPF.CatTipoContaco() { id = -1, contacto = "Seleccione" };
                    var resp = wsG.TipoContactosGestTelefonico(new Peticion { IdUsuario = idUsuario });
                    if (resp.Resultado != null && resp.Resultado.CatTipoContactos != null && resp.Resultado.CatTipoContactos.Length > 0)
                    {
                        tipoContactos = resp.Resultado.CatTipoContactos.ToList();
                        tipoContactos.Insert(0, seleccionTipoContacto);
                    }
                    else
                    {
                        tipoContactos.Add(seleccionTipoContacto);
                    }
                    strScript.AppendFormat("tipoContactos={0};", JsonConvert.SerializeObject(tipoContactos));
                    #endregion

                    #region ResultadoGestion
                    List<wsSOPF.ResultadoGestionVM> resultadosGest = new List<wsSOPF.ResultadoGestionVM>();
                    wsSOPF.ResultadoGestionVM resltOp = new wsSOPF.ResultadoGestionVM() { IdResultado = 0, Descripcion = "Seleccione" };
                    ResultadosGestionResponse resultadoGestR = wsG.ResultadosGestion(new ResultadosGestionPeticion { IdTipoGestion = 3, IdUsuario = idUsuario, Estatus = true });
                    if (resultadoGestR != null && resultadoGestR.ResultadosGestion != null && resultadoGestR.ResultadosGestion.Length > 0)
                    {
                        resultadosGest = resultadoGestR.ResultadosGestion.ToList();
                        resultadosGest.Insert(0, resltOp);
                    }
                    else
                    {
                        resultadosGest.Add(resltOp);
                    }
                    strScript.AppendFormat("resultadoGestion={0};", JsonConvert.SerializeObject(resultadosGest));
                    #endregion

                    wsSOPF.ObtenerDatosClienteGestionRequest datosClienteRq = new wsSOPF.ObtenerDatosClienteGestionRequest()
                    {
                        Accion = "CONSULTAR_DATOS_CLIENTE_GESTION_POR_IDSOLICITUD",
                        IdSolicitud = idSolicitud
                    };
                    datosClienteRs = wsG.ObtenerDatosClienteGestion(datosClienteRq);
                    if (datosClienteRs.Resultado.Datos != null)
                    {
                        cuotaCliente = datosClienteRs.Resultado.Datos.Cuota;
                        cuotasVencidas = datosClienteRs.Resultado.Datos.RecibosVencidos ?? 0;

                        #region DatosCliente
                        txNombreCliente.Value = datosClienteRs.Resultado.Datos.NombreCliente;
                        txDNICliente.Value = datosClienteRs.Resultado.Datos.DNICliente;
                        txDomicilioLegal.Value = datosClienteRs.Resultado.Datos.DomicilioLegal;
                        txDistrito.Value = datosClienteRs.Resultado.Datos.Distrito;
                        txReferenciaDomiciliaria.Value = datosClienteRs.Resultado.Datos.ReferenciaDomiciliaria;
                        txRazonSocialEmpresa.Value = datosClienteRs.Resultado.Datos.RazonSocialEmpresa;
                        txSituacionLaboral.Value = datosClienteRs.Resultado.Datos.SituacionLaboral;
                        txProducto.Value = datosClienteRs.Resultado.Datos.Producto;
                        txEjecutivoVenta.Value = datosClienteRs.Resultado.Datos.EjecutivoVenta;
                        #endregion
                        #region DatosCredito
                        txMontoCredito.Value = datosClienteRs.Resultado.Datos.MontoCredito.ToString("C", CultureInfo.CurrentCulture);
                        txPlazo.Value = string.Concat(datosClienteRs.Resultado.Datos.Plazo.ToString(), " MESES");
                        txCuota.Value = datosClienteRs.Resultado.Datos.Cuota.ToString("C", CultureInfo.CurrentCulture);
                        txRecibosVencidos.Value = datosClienteRs.Resultado.Datos.RecibosVencidos.ToString();
                        txDiasMora.Value = datosClienteRs.Resultado.Datos.DiasMora.ToString();
                        txFlagMora.Value = datosClienteRs.Resultado.Datos.FlagMora.ToString();
                        txSaldoVencido.Value = datosClienteRs.Resultado.Datos.SaldoVencido != null ?
                            datosClienteRs.Resultado.Datos.SaldoVencido.Value.ToString("C", CultureInfo.CurrentCulture) : "";
                        txEstatusCliente.Value = datosClienteRs.Resultado.Datos.EstatusCliente;
                        txFechaUltimoPago.Value = datosClienteRs.Resultado.Datos.FechaUltimoPago != null ?
                            datosClienteRs.Resultado.Datos.FechaUltimoPago.Value.ToString("dd/MM/yyyy") : "";
                        #endregion
                        #region DatosCobro
                        txDiaPago.Value = datosClienteRs.Resultado.Datos.DiaPago.ToString();
                        txBanco.Value = datosClienteRs.Resultado.Datos.Banco;
                        txCuentaBancaria.Value = datosClienteRs.Resultado.Datos.CuentaBancaria;
                        txNumeroTarjeta.Value = datosClienteRs.Resultado.Datos.NumeroTarjeta;
                        itxNumeroTarjeta.Visible = datosClienteRs.Resultado.Datos.TarjetaActAuto > 0;
                        txMesVencimiento.Value = datosClienteRs.Resultado.Datos.MesVencimiento.ToString();
                        itxMesVencimiento.Visible = datosClienteRs.Resultado.Datos.TarjetaActAuto > 0;
                        txAnioVencimiento.Value = datosClienteRs.Resultado.Datos.AnioVencimiento.ToString();
                        itxAnioVencimiento.Visible = datosClienteRs.Resultado.Datos.TarjetaActAuto > 0;
                        txFechaUltimoProceso.Value = datosClienteRs.Resultado.Datos.FechaUltimoProceso != null ?
                            datosClienteRs.Resultado.Datos.FechaUltimoProceso.Value.ToString("dd/MM/yyyy") : "";
                        txProcesoCobro.Value = datosClienteRs.Resultado.Datos.ProcesoCobro;
                        txMensajeBanco.Value = datosClienteRs.Resultado.Datos.MensajeBanco;
                        #endregion
                        strScript.AppendFormat("idSolicitud={0};", JsonConvert.SerializeObject(idSolicitud));
                        strScript.AppendFormat("cuotaCliente={0};", JsonConvert.SerializeObject(cuotaCliente));
                        strScript.AppendFormat("cuotasVencidas={0};", JsonConvert.SerializeObject(cuotasVencidas));
                    }

                    var gestorUsuario = wsG.ObtenerGestor(4, 0, 0, idUsuario);
                    if (gestorUsuario != null && gestorUsuario.Length > 0)
                    {
                        IdGestor = gestorUsuario[0].IdGestor;
                        Session["IdGestor"] = IdGestor;
                    }

                    #region TipoGestion
                    var tiposGestion = ObtenerCatTipoGestion(wsG);
                    if (tiposGestion != null)
                        strScript.AppendFormat("tiposGestion={0};", JsonConvert.SerializeObject(tiposGestion));
                    #endregion

                    #region TipoCobro
                    var tiposCobro = ObtenerCatTipoCobro();
                    if (tiposCobro != null)
                        strScript.AppendFormat("tiposCobro={0};", JsonConvert.SerializeObject(tiposCobro));
                    #endregion

                    #region ArchivoGestion
                    var tiposArchivo = ObtenerTiposArchivoGestion(wsG);
                    if (tiposArchivo != null)
                        strScript.AppendFormat("tiposArchivo={0};", JsonConvert.SerializeObject(tiposArchivo));

                    var servidoresArchivo = ObtenerCatServidoresArchivos();
                    if (tiposArchivo != null)
                        strScript.AppendFormat("servidoresArchivo={0};", JsonConvert.SerializeObject(servidoresArchivo));

                    var archivosGestion = ObtenerArchivosGestionSolicitud(wsG, IdSolicitud);
                    if (archivosGestion != null)
                        strScript.AppendFormat("archivosGestion={0};", JsonConvert.SerializeObject(archivosGestion));
                    #endregion

                    #region NotificacionesGestion

                    var documentosDestino = ObtenerDocumentosDestino();
                    if (documentosDestino != null)
                        strScript.AppendFormat("documentosDestino={0};", JsonConvert.SerializeObject(documentosDestino));

                    #endregion

                    #region ConfigAgenda
                    var configRes = wsG.ConfiguracionNotificacion(new ConfiguracionNotificacionRequest { IdUsuario = idUsuario });
                    if (configRes != null && !configRes.Error)
                    {
                        var configNotificacion = new { configRes.HoraMinAgenda, configRes.HoraMaxAgenda };
                        strScript.AppendFormat("configAgenda={0};", JsonConvert.SerializeObject(configNotificacion));
                    }
                    #endregion

                    #region GestoresAsignados
                    var resulGestor = wsG.ObtenerGestor(4, 0, 0, idUsuario);
                    var gestoresResp = wsG.GestoresAsignados(new GestoresAsignadosRequest { IdUsuario = idUsuario });
                    List<GestorVM> gestores = new List<GestorVM>() { new GestorVM { IdGestor = 0, Nombre = "Seleccione" } };

                    if (gestoresResp != null && !gestoresResp.Error && gestoresResp.Gestores != null && gestoresResp.Gestores.Length > 0)
                    {
                        gestores.AddRange(gestoresResp.Gestores.ToList().OrderBy(o => o.Nombre));
                        if (resulGestor != null || resulGestor.Length > 0)
                        {
                            int pos = gestores.FindIndex(g => g.IdGestor == resulGestor[0].IdGestor);
                            if (pos >= 0) gestores.RemoveAt(pos);
                        }
                    }
                    strScript.AppendFormat("gestoresA={0};", JsonConvert.SerializeObject(gestores
                        .Select(g => new { g.IdGestor, g.Nombre })));
                    #endregion

                    #region GestionDomi
                    var gestionDom = wsG.TipoGestionDomiciliacion();
                    if (gestionDom != null && gestionDom.TipoGestionDomiciliacion != null && gestionDom.TipoGestionDomiciliacion.Length > 0)
                    {
                        strScript.AppendFormat("tipoGestionDom={0};", JsonConvert.SerializeObject(gestionDom.TipoGestionDomiciliacion
                        .Select(tg => tg.IdTipoGestion)));
                    }
                    #endregion
                    #region GestionPromesa
                    var tiposGestionPromesa = wsG.TipoGestionPromesa();
                    if (tiposGestionPromesa != null && tiposGestionPromesa.Tipos != null && tiposGestionPromesa.Tipos.Length > 0)
                    {
                        strScript.AppendFormat("tiposGestionPromesa={0};", JsonConvert.SerializeObject(tiposGestionPromesa.Tipos
                        .Select(tg => tg.IdTipoGestion)));
                    }
                    #endregion
                    #region TipoGestionTipoNotificacion
                    var tiposNotPorTipoGestion = wsG.TipoGestionTipoNotificacion();
                    if (tiposNotPorTipoGestion != null && tiposNotPorTipoGestion.TipoGestionTipoNotificacion != null && tiposNotPorTipoGestion.TipoGestionTipoNotificacion.Length > 0)
                    {
                        strScript.AppendFormat("tiposNotPorTipoGestion={0};", JsonConvert.SerializeObject(tiposNotPorTipoGestion.TipoGestionTipoNotificacion));
                    }
                    #endregion
                }
                using (wsSOPF.SistemaClient ws = new wsSOPF.SistemaClient())
                {
                    #region RolUsuarioElevado
                    bool rolUsuarioElevado = false;
                    rolUsuarioElevado = ws.EsTipoUsuario("GERENTE REGIONAL", idUsuario) || ws.EsTipoUsuario("ADMINISTRADOR", idUsuario)
                        || ws.EsTipoUsuario("JEFE COBRANZA", idUsuario); ;
                    strScript.AppendFormat("rolUsuarioElevado={0};", JsonConvert.SerializeObject(rolUsuarioElevado ? 1 : 0));
                    #endregion
                }
            }
            catch (Exception ex)
            {
                //TODO: Implementar Log de error.
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
        }
    }

    protected void btnActualizarCliente_Click(object sender, EventArgs e)
    {
        int origenUrl = 0;
        string cadena = HttpContext.Current.Request.Url.AbsolutePath.ToLower();
        using (CatalogoClient ws = new CatalogoClient())
        {
            var res = ws.ObtenerOrigenClienteEdicion();
            if (res != null && res.Origenes != null && res.Origenes.Length > 0)
            {
                foreach (var origen in res.Origenes)
                {
                    if (origen.Url == cadena)
                        origenUrl = origen.IdOrigenClienteEdicion;
                }
            }
        }
        if (IdSolicitud > 0 && IdCliente > 0)
        {
            Response.Redirect("frmActualizarCliente.aspx?IdSolicitud=" + IdSolicitud.ToString() + "&IdCliente=" + IdCliente.ToString() + "&Origen=" + origenUrl.ToString());
        }
    }

    protected void btRegresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("CarteraAsignada.aspx");
    }

    #region PrivateMethods
    private static List<DocumentoDestinoVM> ObtenerDocumentosDestino()
    {
        List<DocumentoDestinoVM> documentos = new List<DocumentoDestinoVM>();
        using (GestionClient gestionClient = new GestionClient())
        {
            var respuestaDocumentos = gestionClient.ObtenerDocumentosDestino();
            if (respuestaDocumentos != null && respuestaDocumentos.Resultado != null && respuestaDocumentos.Resultado.DocumentosDestino != null && respuestaDocumentos.Resultado.DocumentosDestino.Length > 0)
                documentos = respuestaDocumentos.Resultado.DocumentosDestino.ToList();
        }
        return documentos;
    }

    public static DateTime? ValidarFechaHoraCitacion(string fecha, string hora)
    {
        Regex rg = new Regex(@"^(?:[01][0-9]|2[0-3]):[0-5][0-9]$");
        bool esMañana = hora.IndexOf("am") != -1;
        if (!esMañana)
        {
            string horasT = hora.Substring(0, 2);
            string minutosT = hora.Substring(3, 2);
            hora = (Convert.ToInt32(horasT) + 12).ToString() + ":" + minutosT;
        }
        else
            hora = hora.Substring(0, 5);
        if (rg.IsMatch(hora))
        {
            string[] formats = { "dd/MM/yyyy HH:mm" };
            DateTime FechaHoraFinal;
            string fechaHora = string.Format("{0} {1}", fecha, hora);
            if (DateTime.TryParseExact(fechaHora, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaHoraFinal))
            {
                return FechaHoraFinal;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
    private static List<TipoArchivoGestionVM> ObtenerTiposArchivoGestion(GestionClient gestionClient)
    {
        var respuesta = new List<TipoArchivoGestionVM>();
        //using (GestionClient gestionClient = new GestionClient())
        //{
        var respuestaTipos = gestionClient.ObtenerTiposArchivoGestion();
        if (respuestaTipos != null && respuestaTipos.Length > 0)
        {
            respuesta = respuestaTipos.ToList();
        }
        //}
        return respuesta;
    }
    private static List<ServidorArchivosVM> ObtenerCatServidoresArchivos()
    {
        var respuesta = new List<ServidorArchivosVM>();
        using (CatalogoClient catalogoclient = new CatalogoClient())
        {
            var respuestaTipos = catalogoclient.ObtenerServidoresArchivos();
            if (respuestaTipos != null && respuestaTipos.Length > 0)
            {
                respuesta = respuestaTipos.ToList();
            }
        }
        return respuesta;
    }
    private static List<TipoGestion> ObtenerCatTipoGestion(GestionClient gestionClient)
    {
        var respuesta = new List<TipoGestion>();
        var respuestaTipos = gestionClient.ObtenerCatalogoTipoGestion(2);
        if (respuestaTipos != null && respuestaTipos.Length > 0)
        {
            respuesta = respuestaTipos.ToList();
        }

        return respuesta;
    }
    private static List<TipoCobroVM> ObtenerCatTipoCobro()
    {
        var respuesta = new List<TipoCobroVM>();
        using (CatalogoClient catalogoClient = new CatalogoClient())
        {
            var respuestaTipos = catalogoClient.ObtenerTiposCobro();
            if (respuestaTipos != null && respuestaTipos.Tipos != null && respuestaTipos.Tipos.Length > 0)
            {
                respuesta = respuestaTipos.Tipos.ToList();
            }
        }

        return respuesta;
    }
    #endregion

    #region WebMethods
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerGestion(int id)
    {
        object gestion = null;
        try
        {
            if (id <= 0) return gestion;
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var respGestion = wsG.ObtenerGestionPordIdGestion(new wsSOPF.ObtenerGestionRequest { Accion = 4, IdGestion = id });
                if (respGestion != null && respGestion.Resultado != null && respGestion.Resultado.Gestion != null)
                {
                    var respGes = respGestion.Resultado.Gestion;
                    gestion = new
                    {
                        Id = respGes.IdGestion,
                        IdTipoContacto = respGes.IdTipoGestion,
                        IdCampania = respGes.idCampana,
                        respGes.Comentario
                    };
                }
            }
        }
        catch (Exception ex)
        {
        }
        return gestion;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerBalance(int idsolicitud)
    {
        object Balance = null;
        try
        {
            if (idsolicitud <= 0) return Balance;
            List<object> recibosFormato = new List<object>();
            List<object> pagosFormato = new List<object>();
            using (wsSOPF.SolicitudClient ws = new SolicitudClient())
            {
                CronogramaPagos kronos = ws.Solicitudes_ObtenerCronograma(idsolicitud, null);
                foreach (CronogramaPagosRecibo item in kronos.Recibos)
                {
                    var obj = new
                    {
                        item.NoRecibo,
                        FechaRecibo = item.FechaRecibo.ToString("dd/MM/yyyy"),
                        CapitalInicial = item.CapitalInicial.ToString("C", CultureInfo.CurrentCulture),
                        CapitalAmortizado = item.Capital.ToString("C", CultureInfo.CurrentCulture),
                        Interes = item.Interes.ToString("C", CultureInfo.CurrentCulture),
                        IGV = item.IGV.ToString("C", CultureInfo.CurrentCulture),
                        GAT = item.GAT.ToString("C", CultureInfo.CurrentCulture),
                        SaldoCapital = item.SaldoCapital.ToString("C", CultureInfo.CurrentCulture),
                        Cuota = item.Cuota.ToString("C", CultureInfo.CurrentCulture)
                    };
                    recibosFormato.Add(obj);
                }
            }
            using (GestionClient ws = new GestionClient())
            {
                ObtenerPagosGestionesResponse res = ws.ObtenerPagosGestiones(new ObtenerPagosGestionesRequest()
                {
                    Accion = "CONSULTAR_PAGOS_POR_IDSOLICITUD",
                    IdSolicitud = idsolicitud
                });
                if (res != null && !res.Error && res.TotalRegistros > 0)
                {
                    foreach (PagoGestionesVM item in res.Resultado.Pagos)
                    {
                        var obj = new
                        {
                            Cuota = item.Cuota,
                            Fecha = item.Fecha.Value.Value.ToString("dd/MM/yyyy"),
                            CapitalInicial = item.CapitalInicial.ToString("C", CultureInfo.CurrentCulture),
                            Capital = item.Capital.ToString("C", CultureInfo.CurrentCulture),
                            Interes = item.Interes.ToString("C", CultureInfo.CurrentCulture),
                            IGV = item.IGV.ToString("C", CultureInfo.CurrentCulture),
                            Seguro = item.Seguro.ToString("C", CultureInfo.CurrentCulture),
                            GAT = item.GAT.ToString("C", CultureInfo.CurrentCulture),
                            IdEstatus = item.Estatus.IdEstatus,
                            EstatusClave = item.Estatus.EstatusClave,
                            EstatusDesc = item.Estatus.EstatusDesc,
                            SaldoRecibo = item.SaldoRecibo.ToString("C", CultureInfo.CurrentCulture),
                            TotalRecibo = item.TotalRecibo.ToString("C", CultureInfo.CurrentCulture),
                            CanalPagoClave = item.CanalPagoClave,
                            CanalPagoDesc = item.CanalPagoDesc,
                            FechaPago = item.FechaPago.Value.Value.ToString("dd/MM/yyyy"),
                            MontoPago = item.MontoPago.ToString("C", CultureInfo.CurrentCulture)
                        };
                        pagosFormato.Add(obj);
                    }
                }
            }

            Balance = new
            {
                TablaAmortizacion = recibosFormato,
                PagosRealizados = pagosFormato
            };
        }
        catch (Exception ex)
        {
            //TODO: Implementar log de error.
        }
        return Balance;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerLiquidacion(ObtenerLiquidacionRequest peticion)
    {
        object Liquidacion = null;
        try
        {
            if (peticion.IdSolicitud <= 0) return Liquidacion;
            using (wsSOPF.CreditoClient ws = new CreditoClient())
            {
                wsSOPF.TBCreditos.Liquidacion LiquidacionCalc = new TBCreditos.Liquidacion();
                LiquidacionCalc.IdSolicitud = peticion.IdSolicitud;
                LiquidacionCalc.FechaLiquidar = new UtilsDateTimeR();
                DateTime fechaEnvio = DateTime.Today;
                if (!string.IsNullOrWhiteSpace(peticion.FechaLiquidacion))
                {
                    if (DateTime.TryParseExact(peticion.FechaLiquidacion, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaEnvio))
                        LiquidacionCalc.FechaLiquidar.Value = fechaEnvio;
                    else
                        return Liquidacion;
                }
                LiquidacionCalc.FechaLiquidar.Value = fechaEnvio;
                // VERIFICAR SI EXISTE UNA LIQUIDACION ACTIVA PARA LA SOLICITUD
                wsSOPF.TBCreditos.Liquidacion LiquidacionActiva = ws.ObtenerLiquidacionActiva(LiquidacionCalc);
                if (LiquidacionActiva != null)
                    LiquidacionCalc = LiquidacionActiva;
                // CALCULAR LA LIQUIDACION
                wsSOPF.ResultadoOfTBCreditosCalculoLiquidacionhDx61Z0Z resCalculoLiquidacion = ws.CalcularLiquidacion(LiquidacionCalc);
                wsSOPF.TBCreditos.CalculoLiquidacion CalculoLiquidacion = resCalculoLiquidacion.ResultObject;
                if (resCalculoLiquidacion.Codigo != 0)
                    throw new Exception(resCalculoLiquidacion.Mensaje);
                List<object> recibosFormato = new List<object>();
                foreach (wsSOPF.TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle item in CalculoLiquidacion.DetalleLiquidacion)
                {
                    var obj = new
                    {
                        item.Recibo,
                        FechaRecibo = item.FechaRecibo.ToString("dd/MM/yyyy"),
                        item.LiquidaCapital,
                        item.LiquidaInteres,
                        item.LiquidaIGV,
                        item.LiquidaSeguro,
                        item.LiquidaGAT,
                        item.LiquidaTotal
                    };
                    recibosFormato.Add(obj);
                }
                Liquidacion = new
                {
                    CalculoLiquidacion.DNICliente,
                    NombreCliente = string.Format("{0} {1} {2}", CalculoLiquidacion.NombreCliente, CalculoLiquidacion.ApPaternoCliente, CalculoLiquidacion.ApMaternoCliente),
                    MontoCredito = CalculoLiquidacion.MontoCredito.ToString("C", CultureInfo.CurrentCulture),
                    Cuota = CalculoLiquidacion.Cuota.ToString("C", CultureInfo.CurrentCulture),
                    TotalLiquidacion = CalculoLiquidacion.TotalLiquidacion.ToString("C", CultureInfo.CurrentCulture),
                    FechaCredito = CalculoLiquidacion.FechaCredito.ToString("dd/MM/yyyy"),
                    FechaPrimerPago = CalculoLiquidacion.FechaPrimerPago.ToString("dd/MM/yyyy"),
                    FechaLiquidacion = CalculoLiquidacion.FechaLiquidacion.Value.Value.ToString("dd/MM/yyyy"),
                    DetalleLiquidacion = recibosFormato
                };
            }
        }
        catch (Exception ex)
        {
            //TODO: Implementar log de error.
        }
        return Liquidacion;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerEstadoNotificacion(int idsolicitud)
    {
        object Notificacion = null;
        try
        {
            if (idsolicitud <= 0) return Notificacion;
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var respNotif = wsG.ObtenerEstadoNotificacion(new wsSOPF.ObtenerEstadoNotificacionRequest { Accion = "GET_ESTADO_NOTIFICACION_POR_SOLICITUD", IdSolicitud = idsolicitud });
                if (respNotif != null && respNotif.Resultado != null && respNotif.Resultado.Notificacion != null)
                {
                    var respNoti = respNotif.Resultado.Notificacion;
                    Notificacion = new
                    {
                        respNoti.IdNotificacion,
                        respNoti.Notificacion,
                        respNoti.Clave,
                        respNoti.Orden,
                        respNoti.Destino,
                        respNoti.Correo,
                        respNoti.EsCourier
                    };
                }
            }
        }
        catch (Exception ex)
        {
            //TODO: Implementar log de error.
        }
        return Notificacion;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GenerarNotificacion(NotificacionRequest notificacion)
    {
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        if (idUsuario <= 0)
            throw new ValidacionExcepcion("La clave del usuario no es válida.");
        else
            notificacion.IdUsuario = idUsuario;
        if (notificacion.IdSolicitud > 0 && !string.IsNullOrEmpty(notificacion.Clave) && notificacion.IdDestino > 0)
        {
            ImpresionNotificacionResponse notificacionResultado = new ImpresionNotificacionResponse();
            using (wsSOPF.GestionClient ws = new wsSOPF.GestionClient())
            {
                switch (notificacion.Clave)
                {
                    case "AC":
                        ImpresionAvisoCobranzaRequest peticion = new ImpresionAvisoCobranzaRequest()
                        {
                            Clave = notificacion.Clave,
                            IdDestino = notificacion.IdDestino,
                            IdSolicitud = notificacion.IdSolicitud,
                            IdUsuario = notificacion.IdUsuario,
                            Version = 1,
                            Actualizar = notificacion.Actualizar,
                            Correo = notificacion.Correo,
                            Courier = notificacion.Courier
                        };
                        notificacionResultado = ws.ImpresionAvisoCobranza(peticion);
                        break;
                    case "AP":
                        ImpresionAvisoPagoRequest peticion2 = new ImpresionAvisoPagoRequest()
                        {
                            Clave = notificacion.Clave,
                            IdDestino = notificacion.IdDestino,
                            IdSolicitud = notificacion.IdSolicitud,
                            IdUsuario = notificacion.IdUsuario,
                            Version = 1,
                            Actualizar = notificacion.Actualizar,
                            Correo = notificacion.Correo,
                            Courier = notificacion.Courier
                        };
                        notificacionResultado = ws.ImpresionAvisoPago(peticion2);
                        break;
                    case "NP":
                        ImpresionNotificacionPrejudicialRequest peticion3 = new ImpresionNotificacionPrejudicialRequest()
                        {
                            Clave = notificacion.Clave,
                            IdDestino = notificacion.IdDestino,
                            IdSolicitud = notificacion.IdSolicitud,
                            IdUsuario = notificacion.IdUsuario,
                            Version = 1,
                            Actualizar = notificacion.Actualizar,
                            Correo = notificacion.Correo,
                            Courier = notificacion.Courier
                        };
                        notificacionResultado = ws.ImpresionNotificacionPrejudicial(peticion3);
                        break;
                    case "UNP":
                        ImpresionUltimaNotificacionPrejudicialRequest peticion4 = new ImpresionUltimaNotificacionPrejudicialRequest()
                        {
                            Clave = notificacion.Clave,
                            IdDestino = notificacion.IdDestino,
                            IdSolicitud = notificacion.IdSolicitud,
                            IdUsuario = notificacion.IdUsuario,
                            Version = 1,
                            Actualizar = notificacion.Actualizar,
                            Correo = notificacion.Correo,
                            Courier = notificacion.Courier
                        };
                        notificacionResultado = ws.ImpresionUltimaNotificacionPrejudicial(peticion4);
                        break;
                    case "CP":
                        if (string.IsNullOrEmpty(notificacion.FechaCitacion) || string.IsNullOrEmpty(notificacion.HoraCitacion))
                            return new { Error = true, Mensaje = "La citación debe de tener fecha y hora." };
                        DateTime? DatoFechaHora = null;
                        DatoFechaHora = ValidarFechaHoraCitacion(notificacion.FechaCitacion, notificacion.HoraCitacion);
                        if (!DatoFechaHora.HasValue)
                            return new { Error = true, Mensaje = "La citación debe de tener fecha y hora válidas." };
                        ImpresionCitacionPrejudicialRequest peticion5 = new ImpresionCitacionPrejudicialRequest()
                        {
                            Clave = notificacion.Clave,
                            IdDestino = notificacion.IdDestino,
                            IdSolicitud = notificacion.IdSolicitud,
                            IdUsuario = notificacion.IdUsuario,
                            FechaHoraCitacion = DatoFechaHora.Value,
                            Version = 1,
                            Actualizar = notificacion.Actualizar,
                            Correo = notificacion.Correo,
                            Courier = notificacion.Courier
                        };
                        notificacionResultado = ws.ImpresionCitacionPrejudicial(peticion5);
                        break;
                    case "PRM":
                        if (notificacion.DescuentoDeuda == null || string.IsNullOrEmpty(notificacion.FechaVigencia))
                            return new { Error = true, Mensaje = "La promoción debe de tener un valor en descuento y en vigencia." };
                        DateTime? FechaVigencia = null;
                        FechaVigencia = ValidarFechaHoraCitacion(notificacion.FechaVigencia, "11:59 pm");
                        if (!FechaVigencia.HasValue)
                            return new { Error = true, Mensaje = "La promoción debe de tener una fecha válida." };
                        ImpresionPromocionReduccionMoraRequest peticion6 = new ImpresionPromocionReduccionMoraRequest()
                        {
                            Clave = notificacion.Clave,
                            IdDestino = notificacion.IdDestino,
                            IdSolicitud = notificacion.IdSolicitud,
                            IdUsuario = notificacion.IdUsuario,
                            Version = 1,
                            DescuentoDeuda = notificacion.DescuentoDeuda,
                            FechaVigencia = FechaVigencia.Value,
                            Actualizar = notificacion.Actualizar,
                            Correo = notificacion.Correo,
                            Courier = notificacion.Courier
                        };
                        notificacionResultado = ws.ImpresionPromocionReduccionMora(peticion6);
                        break;
                    default:
                        break;
                }
            }
            if (notificacionResultado.Resultado != null && notificacionResultado.Resultado.ContenidoArchivo != null
                && notificacionResultado.Resultado.ContenidoArchivo.Length > 0 && !notificacion.Courier)
            {
                string nombreDocumento = string.Empty;
                nombreDocumento = Notificacion.ObtenerNombreArchivo(notificacion.Clave);
                var resultado = new
                {
                    Error = false,
                    Mensaje = "La notificación se ha generado exitosamente.",
                    Tipo = "application/pdf",
                    Longitud = notificacionResultado.Resultado.ContenidoArchivo.Length.ToString(),
                    NombreArchivo = nombreDocumento,
                    Contenido = Convert.ToBase64String(notificacionResultado.Resultado.ContenidoArchivo)
                };
                return resultado;
            }
            else if (notificacionResultado.Resultado != null && (notificacion.Correo || notificacion.Courier))
            {
                return new
                {
                    Error = true,
                    Mensaje = "La notificación se ha programado para:" + (notificacion.Correo ? " envío por correo" : "")
                        + (notificacion.Courier ? " envío vía Courier" : "") + "."
                };
            }
            else
            {
                return new { Error = true, Mensaje = "La notificación no pudo generarse. " + notificacionResultado.MensajeOperacion };
            }
        }
        return new { Error = true, Mensaje = "Ha ocurrido un error en los datos de petición de generación de notificación." };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerDatosNotificacion(int idsolicitud, int idnotificacion, int iddestino)
    {
        object Notificacion = null;
        try
        {
            if (idsolicitud <= 0)
            {
                Notificacion = new { Error = true, MensajeOperacion = "Ha ocurrido un error." };
                return Notificacion;
            }
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var respNotif = wsG.ObtenerEstadoNotificacion(new wsSOPF.ObtenerEstadoNotificacionRequest { Accion = "GET_DATOS_NOTIFICACION_POR_SOLICITUD", IdSolicitud = idsolicitud, IdNotificacion = idnotificacion, IdDestino = iddestino });
                if (respNotif != null && respNotif.Resultado != null && respNotif.Resultado.Notificacion != null)
                {
                    var s = respNotif.Resultado.Notificacion;
                    Notificacion = new
                    {
                        s.IdNotificacion,
                        s.Notificacion,
                        s.NombreArchivo,
                        s.Clave,
                        s.Orden,
                        FechaCitacion = s.FechaHoraCitacion.GetValueOrDefault().ToString("dd/MM/yyyy"),
                        HoraCitacion = s.FechaHoraCitacion.GetValueOrDefault().ToString("HH:mm"),
                        DescuentoDeuda = s.DescuentoDeuda.GetValueOrDefault(),
                        FechaVigencia = s.FechaVigencia.GetValueOrDefault().ToString("dd/MM/yyyy"),
                        s.Destino,
                        s.Correo,
                        s.EsCourier
                    };
                }
                else
                {
                    Notificacion = new { Error = true, MensajeOperacion = respNotif.MensajeOperacion ?? "Ha ocurrido un error." };
                }
            }
        }
        catch (Exception ex)
        {
            //TODO: Implementar log de error.
        }
        return Notificacion;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerBitacoraNotificacion(int idsolicitud)
    {
        object Bitacora = null;
        try
        {
            if (idsolicitud <= 0) return Bitacora;
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var respNotif = wsG.ObtenerBitacoraNotificacion(new wsSOPF.ObtenerBitacoraNotificacionRequest { Accion = "GET_BITACORA_NOTIFICACION_POR_SOLICITUD", IdSolicitud = idsolicitud });
                if (respNotif != null && respNotif.Resultado != null && respNotif.Resultado.Bitacora != null)
                {
                    Bitacora = respNotif.Resultado.Bitacora.Select(s => new
                    {
                        s.Notificacion,
                        s.Destino,
                        s.Accion,
                        s.Usuario,
                        Fecha = s.Fecha.ToString("dd/MM/yyyy HH:mm:ss")
                    });
                }
                else
                {
                    Bitacora = new { Error = true, MensajeOperacion = respNotif.MensajeOperacion ?? "Ha ocurrido un error." };
                }
            }
        }
        catch (Exception ex)
        {
            //TODO: Implementar log de error.
        }
        return Bitacora;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object TipoTelefonoContactos(int idSolicitud, int idTipoContacto)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object contactos = null;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }

            using (GestionClient wsG = new GestionClient())
            {
                TelefonosTipoContactoResponse contactosR = wsG.TelefonosTipoContacto(new TelefonosTipoContactoRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitud = idSolicitud,
                    IdTipoContacto = idTipoContacto,
                });
                if (contactosR != null)
                {
                    error = contactosR.Error;
                    mensaje = contactosR.MensajeOperacion;
                    if (contactosR.Contactos != null && contactosR.Contactos.Length > 0)
                    {
                        contactos = contactosR.Contactos;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url, contactos };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GestionTelefonica(Gestion gestionTel, DevolverLlamadaRequest devolverLlamada)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }
            if (gestionTel == null) throw new Exception("La petición es incorrecta");

            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var resulGestor = wsG.ObtenerGestor(4, 0, 0, idUsuario);
                if (resulGestor == null || resulGestor.Length == 0) throw new Exception("No fue posible obtener datos del gestor.");
                int idGestor = resulGestor[0].IdGestor;

                #region GuardarGestion
                wsSOPF.Gestion gestionDB = new wsSOPF.Gestion
                {
                    IdCuenta = gestionTel.IdCuenta,
                    IdGestor = idGestor,
                    Comentario = gestionTel.Comentario.Trim(),
                    IdResultadoGestion = gestionTel.IdResultadoGestion,
                    IdTipoGestion = 3,
                    TipoGestion = "Gestión telefónica",
                    idCNT = 0
                };
                int idGestion = wsG.insertarGestion(gestionDB);
                #endregion

                if (idGestion > 0)
                {
                    mensaje = "La gestión se ha guardado correctamente!";

                    #region Devolucion de Llamada - Notificacion
                    if (devolverLlamada != null && devolverLlamada.Fch_DevLlamada != null && devolverLlamada.Fch_DevLlamada != new DateTime())
                    {
                        devolverLlamada.IdGestion = idGestion;
                        devolverLlamada.IdCuenta = gestionTel.IdCuenta;
                        devolverLlamada.IdGestor = idGestor;
                        if (devolverLlamada.IdGestoresNotificar != null && devolverLlamada.IdGestoresNotificar.Length > 0)
                        {
                            var gestoreNot = devolverLlamada.IdGestoresNotificar.ToList();
                            gestoreNot.Add(idGestor);
                            devolverLlamada.IdGestoresNotificar = gestoreNot.ToArray();
                        }
                        var devolverResp = wsG.DevolverLlamada(devolverLlamada);
                    }
                    #endregion
                }
                else
                {
                    error = true;
                    mensaje = "Ocurrió un error mientras se intentaba guardar la gestión telefónica";
                }
            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object CargarGestiones(DataTableAjaxPostModel model, int idCuenta)
    {
        string mensaje = string.Empty;
        int totalRegistros = 0;
        int registros = 0;
        object gestiones = null;
        string url = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("Usuario Invalido");
            }
            ObtenerGestionRequest peticion = new ObtenerGestionRequest
            {
                IdCuenta = idCuenta,
                Pagina = (model.start / model.length) + 1,
                RegistrosPagina = model.length
            };

            ObtenerGestionesResponse respuestaGestiones = null;
            List<GestionVM> listaGestiones = new List<GestionVM>();
            using (GestionClient wsG = new GestionClient())
            {
                respuestaGestiones = wsG.ObtenerGestionesPorIdCuenta(peticion);
                if (respuestaGestiones != null && respuestaGestiones.Gestiones != null && respuestaGestiones.Gestiones.Length > 0)
                {
                    listaGestiones = respuestaGestiones.Gestiones.ToList();
                }
            }

            if (listaGestiones.Count > 0)
            {
                gestiones = listaGestiones.Select(g => new
                {
                    g.IdGestion,
                    g.idTipoGestion,
                    g.Tipo,
                    g.Monto,
                    g.fch_Gestion,
                    g.ResultadoGestion,
                    g.GeneraRecordatorio,
                    g.EstatusPromesa,
                    g.Comentario,
                    NombreGestor = (g.Comentario.LastIndexOf("Realizado por:") >= 0) ? g.Comentario.Substring(g.Comentario.LastIndexOf("Realizado por:") + 15) : null,
                    g.FechaUltimaNotificacion,
                    g.FechaProgUltNotificacion
                }).ToList();

                registros = listaGestiones.Count;
                totalRegistros = respuestaGestiones.TotalRegistros;
            }
        }
        catch (Exception ex)
        {
        }
        model.draw++;
        return new
        {
            drawn = model.draw,
            recordsTotal = registros,
            recordsFiltered = totalRegistros,
            data = gestiones,
            url
        };
    }

    #region ArchivosGestion
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<ArchivoGestionVM> ObtenerArchivosGestionSolicitud(GestionClient gestionClient, int idSolicitud)
    {
        var respuesta = new List<ArchivoGestionVM>();
        //using (GestionClient gestionClient = new GestionClient())
        //{
        var respuestaArchivos = gestionClient.ObtenerArchivoGestionPorSolicitud(idSolicitud);
        if (respuestaArchivos != null && respuestaArchivos.Length > 0)
        {
            respuesta = respuestaArchivos.ToList();
            respuesta.Reverse(); //Se ordenan por mas reciente a mas viejo
        }
        //}
        return respuesta;
    }
    #endregion

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<ResultadoGestionVM> ObtenerResultadoGestionPorTipo(int idTipoGestion)
    {
        List<ResultadoGestionVM> respuesta = new List<ResultadoGestionVM>();
        int idUsuario = HttpContext.Current.ValidarUsuario();
        if (idUsuario <= 0)
        {
            throw new Exception("Usuario Invalido");
        }
        using (GestionClient gestionClient = new GestionClient())
        {
            var respuestaResultados = gestionClient.ResultadosGestion(new ResultadosGestionPeticion { IdTipoGestion = idTipoGestion, IdUsuario = idUsuario, Estatus = true });
            if (respuestaResultados != null && respuestaResultados.ResultadosGestion != null && respuestaResultados.ResultadosGestion.Length > 0)
            {
                respuesta = respuestaResultados.ResultadosGestion.ToList();

            }
        }

        return respuesta;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static int InsertarGestion(Gestion gestion)
    {
        var respuesta = 0;

        int idUsuario = HttpContext.Current.ValidarUsuario();
        if (idUsuario <= 0) throw new Exception("Usuario Invalido");
        if (gestion == null) throw new Exception("La peticion es invalida");
        if ((gestion.MontoDomiciliar ?? 0) < 0 && gestion.FechaDomiciliar == null && gestion.IdResultadoGestion == 0)
            throw new Exception("El resultado de la gestion es invalido");

        using (GestionClient gestionClient = new GestionClient())
        {
            var idGestor = 0;
            int.TryParse(HttpContext.Current.Session["IdGestor"].ToString(), out idGestor);
            if (idGestor <= 0) throw new Exception("Ha ocurrido un error informacion del gestor");
            var gestionDb = new wsSOPF.Gestion
            {
                IdCuenta = gestion.IdCuenta,
                IdGestor = idGestor,
                IdResultadoGestion = gestion.IdResultadoGestion,
                MontoDomiciliar = gestion.MontoDomiciliar,
                FechaDomiciliar = gestion.FechaDomiciliar,
                IdTipoGestion = gestion.IdTipoGestion
            };

            if (gestion.IdResultadoGestion <= 0)
            {
                var respuestaAlta = gestionClient.InsertarGestionConResultado(gestionDb);
                if (respuestaAlta == null) throw new Exception("Ocurrio un error no controlado en la peticion para ingresar la nueva gestión");
                if (respuestaAlta.Error) throw new Exception(respuestaAlta.MensajeOperacion);
                if (respuestaAlta.IdGestion > 0)
                {
                    respuesta = respuestaAlta.IdGestion;

                    if (gestion.FechaPromesa != null && gestion.MontoPromesa != null)
                    {
                        gestionClient.insertarPromesaPago(new PromesaPago
                        {
                            IdCuenta = gestion.IdCuenta,
                            IdGestion = respuestaAlta.IdGestion,
                            FchPromesa = gestion.FechaPromesa ?? DateTime.Today,
                            MontoPromesa = gestion.MontoPromesa ?? 0,
                            idTipoCobro = gestion.IdTipoCobro
                        });
                    }
                }
            }
            else
            {
                respuesta = gestionClient.insertarGestion(gestionDb);
            }
        }

        return respuesta;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static GestionNotificacionResponse RegistrarNotificacionGestion(GestionNotificacionRequest peticion)
    {
        int idUsuario = HttpContext.Current.ValidarUsuario();
        if (idUsuario <= 0) throw new Exception("Usuario Invalido");
        if (peticion == null) throw new Exception("La peticion es invalida");
        if (peticion.IdGestion <= 0) throw new Exception("La clave de la gestión no es válida");
        if (peticion.IdTipoGestion <= 0) throw new Exception("El tipo de gestión no es válido");
        if (peticion.IdTipoNotificacion <= 0) throw new Exception("El tipo de notificación no es válido");

        GestionNotificacionResponse respuesta = new GestionNotificacionResponse();
        using (GestionClient gestionClient = new GestionClient())
        {
            peticion.IdUsuario = idUsuario;
            peticion.EnviarCliente = false;
            var respuestaNotificacion = gestionClient.GestionNotificacion(peticion);
            if (respuestaNotificacion == null) throw new Exception("Ocurrio un error al generar el recordatorio");

            respuesta = respuestaNotificacion;
        }

        return respuesta;
    }

    #endregion
}
