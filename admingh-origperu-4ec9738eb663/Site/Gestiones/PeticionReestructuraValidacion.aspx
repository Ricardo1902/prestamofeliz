﻿<%@ Page Title="Peticiones de Reestructura " Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="PeticionReestructuraValidacion.aspx.cs" Inherits="Site_Gestiones_PeticionReestructuraValidacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <link href="css/peticionreestructuravalidacion.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />
    <script src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script src="../../js/datatables/1.10.19/jquery.dataTables.min.js"></script>
    <script src="../../js/datatables/1.10.19/dataTables.bootstrap.min.js"></script>
    <script src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/datepicker-1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="../../js/datepicker-1.9.0/locales/bootstrap-datepicker.es.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/dropdown.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/tooltip.js"></script>
    <script src="js/peticionreestructuravalidacion.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="page-header">
        <div class="row inline">
            <h3 style="display: inline;">Documentos Peticiones de Reestructura</h3>
        </div>
    </div>
    <div class="filtros">
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>No. Solicitud</label>
                    <input type="number" id="txSolicitud" class="form-control" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Estatus</label>
                    <select id="selEstatus" class="form-control">
                        <option value="-1">Todos</option>
                        <option value="1">Aprobados</option>
                        <option value="0">Rechazados</option>
                    </select>
                </div>
            </div>            
            <div class="col-md-2" style="padding-top: 25px;">
                <div class="form-group">
                    <input type="button" id="btnBuscar" class="btn btn-primary" value="Buscar" onclick="mostrarPeticiones()" />
                </div>
            </div>
        </div>
    </div>
    <div class="contenido">
        <table id="tPeticiones" class="table display compact" style="width: 100%"></table>
    </div>
    <div id="datSesion" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Archivos cargados</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <span id="datSesion-mensaje"></span>
                    </div>
                    <div class="row">
                        <div class="btn-group" role="group" aria-label="...">
                            <button type="button" id="adSelfie" class="btn btn-info" onclick="onclikArchivo(this)">
                                <span class="glyphicon glyphicon-user"></span>&nbsp;Rostro
                            </button>
                            <button type="button" id="adDocFront" class="btn btn-warning" onclick="onclikArchivo(this)">
                                <span class="glyphicon glyphicon-credit-card"></span>&nbsp;Frente
                            </button>
                            <button type="button" id="adDocBack" class="btn btn-warning" onclick="onclikArchivo(this)">
                                <span class="glyphicon glyphicon-credit-card"></span>&nbsp;Reverso
                            </button>
                            <button type="button" id="adVideo" class="btn btn-success" onclick="onclikArchivo(this)">
                                <span class="glyphicon glyphicon-facetime-video"></span>&nbsp;Video
                            </button>
                            <button type="button" id="adCert" class="btn btn-primary" onclick="onclikArchivo(this)">
                                <span class="glyphicon glyphicon-file"></span>&nbsp;Certificado
                            </button>
                        </div>
                    </div>
                    <div id="contenido" class="row">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cerrar" />
                </div>
            </div>
        </div>
    </div>
    <div id="datFirmas" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Archivos cargados</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <span id="datFirmas-mensaje"></span>
                    </div>
                    <div id="archivoPdf">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cerrar" />
                </div>
            </div>
        </div>
    </div>
    <div id="petResValidar" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="alertdialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Autorizar/Rechazar Petición</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="selAccion" class="col-sm-3 control-label">Acción:</label>
                                <div class="col-sm-9">
                                    <select id="selAccion" class="form-control">
                                        <option value="1">Autorizar</option>
                                        <option value="0">Rechazar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txComentario" class="col-sm-3 control-label">Comentario:</label>
                                <div class="col-sm-9">
                                    <textarea id="txComentario" class="form-control" cols="500" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success" value="Guardar" onclick="validarPeticion();" />
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cancelar" />
                </div>
            </div>
        </div>
    </div>
    <div id="divLoader" class="hidden">
        <div id="loader-background"></div>
        <div id="loader-content"></div>
    </div>
</asp:Content>
