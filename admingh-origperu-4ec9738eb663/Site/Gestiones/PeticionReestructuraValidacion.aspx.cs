﻿using DataTables;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Site_Gestiones_PeticionReestructuraValidacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["descargar"] != null && Request.QueryString["idDocumento"] != null)
        {
            DescargarContenido(Request.QueryString["idDocumento"]);
            Response.End();
        }
        StringBuilder strScript = new StringBuilder();
        if (!IsPostBack)
        {
            strScript.AppendFormat("estatus={0};", JsonConvert.SerializeObject(ObtenerCatEstatusPeticionReest()));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
        }
    }

    private void DescargarContenido(string idDocumento)
    {
        try
        {
            int idUsuario = 0;
            if (Session["UsuarioId"] != null) int.TryParse(Session["UsuarioId"].ToString(), out idUsuario);
            if (idUsuario <= 0) throw new Exception();
            string nombreArchivo = "Descarga_" + Request.QueryString["idDocumento"] + ".pdf";
            if (Request.QueryString["nombreArchivo"] != null)
            {
                nombreArchivo = Request.QueryString["nombreArchivo"];
            }

            DescargarArchivoGDResponse archivoResp;
            using (wsSOPF.SistemaClient wsS = new SistemaClient())
            {
                archivoResp = wsS.DescargarArchivoGD(new DescargarArchivoGDRequest
                {
                    IdArchivoGD = idDocumento,
                    IdUsuario = idUsuario
                });
            }
            if (archivoResp != null
                && !archivoResp.Error
                && archivoResp.ArchivoGoogle != null
                && archivoResp.ArchivoGoogle.Contenido != null
                && archivoResp.ArchivoGoogle.Contenido.Length > 0)
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "filename=" + nombreArchivo);
                Response.BinaryWrite(archivoResp.ArchivoGoogle.Contenido);
            }
            else throw new Exception();
        }
        catch (Exception ex)
        {
            Response.Write("No fue posible obtener el archivo");
        }
        finally
        {
            Response.Flush();
        }

    }

    private EstatusPeticionReestructuraVM[] ObtenerCatEstatusPeticionReest()
    {
        EstatusPeticionReestructuraVM[] estatus = null;
        try
        {
            using (GestionClient wsG = new GestionClient())
            {
                var respCat = wsG.EstatusPeticionReestEtapaVal();
                if (respCat.EstatusPeticion != null)
                {
                    estatus = respCat.EstatusPeticion;
                }
            }
        }
        catch (Exception)
        {
        }
        return estatus;
    }

    #region WebMethods
    #region GET
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerCotenido(string idContenido)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object archivo = null;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válido.");
            }
            DescargarArchivoGDResponse archivoResp;
            using (wsSOPF.SistemaClient wsS = new SistemaClient())
            {
                archivoResp = wsS.DescargarArchivoGD(new DescargarArchivoGDRequest
                {
                    IdArchivoGD = idContenido,
                    IdUsuario = idUsuario
                });
            }
            if (archivoResp != null
                && !archivoResp.Error
                && archivoResp.ArchivoGoogle != null
                && archivoResp.ArchivoGoogle.Contenido != null
                && archivoResp.ArchivoGoogle.Contenido.Length > 0)
            {
                archivo = new
                {
                    contenido = Convert.ToBase64String(archivoResp.ArchivoGoogle.Contenido),
                    tipoArchivo = archivoResp.ArchivoGoogle.Mime
                };
            }
            else
            {
                error = true;
                mensaje = "No fue posible obtener el contenido del archivo.";
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url, archivo };
    }
    #endregion

    #region POST
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object PeticionReestDocs(DataTableAjaxPostModel model, PeticionReestFiltro buscar)
    {
        int idUsuario = 0;
        string error = string.Empty;
        string mensaje = string.Empty;
        int totalRegistros = 0;
        int registros = 0;
        object peticiones = null;
        string url = string.Empty;
        idUsuario = HttpContext.Current.ValidarUsuario();
        try
        {
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("Usuario Invalido");
            }

            List<PeticionReestDocDigitalVM> peticionDocs = new List<PeticionReestDocDigitalVM>();
            PeticionReestDocDigitalesRequest peticion = new PeticionReestDocDigitalesRequest
            {
                IdUsuario = idUsuario,
                Pagina = (model.start / model.length) + 1,
                RegistrosPagina = model.length
            };

            if (buscar != null)
            {
                if (buscar.IdSolicitud > 0) peticion.IdSolicitud = buscar.IdSolicitud;
                if (buscar.IdEstatus > 0) peticion.EstatusPeticionReest = new int[] { buscar.IdEstatus };
            }

            using (GestionClient wsG = new GestionClient())
            {
                PeticionReestDocDigitalesResponse peticionReestResp = wsG.PeticionReestDocDigitales(peticion);

                if (peticionReestResp != null && peticionReestResp.PeticionReestDocs != null && peticionReestResp.PeticionReestDocs.Length > 0)
                {
                    peticionDocs = peticionReestResp.PeticionReestDocs.ToList();
                    totalRegistros = peticionReestResp.TotalRegistros;
                }
            }

            if (peticionDocs.Count > 0)
            {
                peticiones = peticionDocs.Select(p => new
                {
                    p.IdPeticionReestructura,
                    p.NoSolicitud,
                    p.NoSolicitudNuevo,
                    p.Cliente,
                    FechaReestructura = p.FechaReestructura.ToString("yyyy-MM-dd HH:mm:ss"),
                    p.IdSolicitudPruebaVida,
                    p.ConErrorPV,
                    p.MensajePV,
                    p.ArchivosPV,
                    p.IdSolicitudFirmaDigital,
                    p.EstatusFirma,
                    p.ConErrorFD,
                    p.MensajeFD,
                    p.ArchivosFD,
                    p.Estatus,
                    Liberado = p.Estatus == "Credito Liberado"
                }).ToList();

                registros = peticionDocs.Count;
            }
        }
        catch (Exception ex)
        {
        }
        model.draw++;
        return new
        {
            drawn = model.draw,
            recordsTotal = registros,
            recordsFiltered = totalRegistros,
            data = peticiones,
            url
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ReenviarProceso(int idSolicitud)
    {
        bool error = true;
        string mensaje = "Se ha iniciado el proceso de actualización.";
        string url = string.Empty;
        int idUsuario = 0;
        try
        {
            #region Validaciones
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            #endregion

            using (wsSOPF.SolicitudClient wsR = new SolicitudClient())
            {
                InicarSolicitudProcesoDigitalResponse iniciarProcesoResp = wsR.IniciarProcesoPeticionReestructura(new InicarSolicitudProcesoDigitalRequest
                {
                    IdSolicitud = idSolicitud,
                    IdUsuario = idUsuario
                });

                if (iniciarProcesoResp == null) throw new ValidacionExcepcion("Ocurrió un error al momento de reiniciar el proceso.");

                error = iniciarProcesoResp.Error;
                if (error && string.IsNullOrEmpty(iniciarProcesoResp.MensajeOperacion)) mensaje = "Ocurrió un erro al momento de reiniciar el proceso.";
                else mensaje = !string.IsNullOrEmpty(iniciarProcesoResp.MensajeOperacion) ? iniciarProcesoResp.MensajeOperacion : "El proceso se ha reiniciado.";
            }
        }
        catch (ValidacionExcepcion vex)
        {
            mensaje = vex.Message;
        }
        catch (Exception)
        {
            mensaje = "Ocurrió un error inesperado";
        }
        return new { error, mensaje, url };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ValidarPeticionReest(wsSOPF.ValidarPeticionReestructuraRequest peticion)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object archivo = null;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válido.");
            }
            if (peticion.Accion == "0") peticion.Accion = "CancelarPetReest";
            else if (peticion.Accion == "1") peticion.Accion = "LiberarPetReest";
            else throw new ValidacionExcepcion("La acción no es correcta. Favor de verificar.");
            peticion.IdUsuario = idUsuario;

            using (GestionClient wsG = new GestionClient())
            {
                ValidarPeticionReestructuraResponse valPetResp = wsG.ValidarPeticionReestructura(peticion);
                error = valPetResp.Error;
                mensaje = string.IsNullOrEmpty(valPetResp.MensajeOperacion) ?
                    error ? "Ocurrió un error al procesar la solicitud. Verifique la información"
                        : "El proceso se ha realizado correctamente"
                    : valPetResp.MensajeOperacion;
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url, archivo };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ReiniciarProceso(int idSolicitud, int idSolicitudPruebaVida, int idSolicitudFirmaDigital)
    {
        bool error = true;
        string mensaje = "Se ha iniciado el proceso de actualización.";
        string url = string.Empty;
        int idUsuario = 0;
        try
        {
            #region Validaciones
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            #endregion

            using (wsSOPF.GestionClient wsG = new GestionClient())
            {
                var respReinicio = wsG.ReiniciarProcesoDigitalPetReest(new ReiniciarProcesoDigitalPetReestRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitud = idSolicitud,
                    IdSolicitudPruebaVida = idSolicitudPruebaVida,
                    IdSolicitudFirmaDigital = idSolicitudFirmaDigital
                });

                if (respReinicio == null) throw new ValidacionExcepcion("Ocurrió un error al momento de reiniciar el proceso.");

                error = respReinicio.Error;
                if (error && string.IsNullOrEmpty(respReinicio.MensajeOperacion)) mensaje = "Ocurrió un erro al momento de reiniciar el proceso.";
                else mensaje = !string.IsNullOrEmpty(respReinicio.MensajeOperacion) ? respReinicio.MensajeOperacion : "El proceso se ha reiniciado.";
            }
        }
        catch (ValidacionExcepcion vex)
        {
            mensaje = vex.Message;
        }
        catch (Exception)
        {
            mensaje = "Ocurrió un error inesperado";
        }
        return new { error, mensaje, url };
    }
    #endregion
    #endregion

    public class PeticionReestFiltro
    {
        public int IdSolicitud { get; set; }
        public int IdEstatus { get; set; }
    }
}