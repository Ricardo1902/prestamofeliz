﻿<%@ Page Title="Validar Gestion Dom" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ValidarGestDomiciliacion.aspx.cs" Inherits="Site_Gestiones_ValidarGestDomiciliacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <link href="css/validargestdomiciliacion.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />

    <script type="text/javascript" src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/tooltip.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/dropdown.js"></script>
    <script type="text/javascript" src="js/validargestdomiciliacion.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="page-header row-clear">
        <div class="row inline">
            <h3 style="display: inline;">Peticiones de Domiciliaciones</h3>
        </div>
    </div>
    <ul id="medioDom" class="nav nav-tabs" role="tablilst">
        <li class="active">
            <a id="tDomBancario" data-toggle="tab" href="#domBancario" aria-expanded="true">Bancario</a>
        </li>
        <li>
            <a id="tDomVisanet" data-toggle="tab" href="#domVisanet">Visanet</a>
        </li>
        <li>
            <a id="tDomRef" data-toggle="tab" href="#domReferenciado">Referenciado</a>
        </li>
    </ul>
    <div id="tabDomi" class="tab-content">
        <div id="domBancario" class="tab-pane fade in active">
            <div class="row acciones">
                <div class="col-md-3 form-group">
                    <label for="selBancoDom">Banco de la Cuenta:</label>
                    <select id="selBancoDom" class="form-control">
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <button type="button" id="btnAutDomBanc" class="btn btn-success" data-claveact="A">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;Autorizar
                    </button>
                    <button type="button" id="btnCancDomBanc" class="btn btn-danger" data-claveact="C">
                        <span class="glyphicon glyphicon-remove"></span>&nbsp;Cancelar
                    </button>
                </div>
            </div>
            <table id="tDomiBancario" class="display table-hover nowrap" style="width: 100%"></table>
        </div>
        <div id="domVisanet" class="tab-pane fade">
            <div class="row acciones">
                <div class="col-md-3 form-group">
                    <button type="button" id="btnAutDomVisa" class="btn btn-success" data-claveact="A">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;Autorizar
                    </button>
                    <button type="button" id="btnCancDomVisa" class="btn btn-danger" data-claveact="C">
                        <span class="glyphicon glyphicon-remove"></span>&nbsp;Cancelar
                    </button>
                </div>
            </div>
            <table id="tDomiVisanet" class="display table-hover nowrap" style="width: 100%"></table>
        </div>
        <div id="domReferenciado" class="tab-pane fade">
            <table id="tDomiReferenciado" class="display table-hover nowrap" style="width: 100%"></table>
        </div>
    </div>
    <div class="modal-pr"></div>
</asp:Content>

