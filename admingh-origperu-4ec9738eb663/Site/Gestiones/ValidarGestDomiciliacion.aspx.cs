﻿using DataTables;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Site_Gestiones_ValidarGestDomiciliacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder strScript = new StringBuilder();

        try
        {
            using (GestionClient wsG = new GestionClient())
            {
                TipoGestionDomiciliacionResponse tipoGest = wsG.TipoGestionDomiciliacion();
                if (tipoGest != null && tipoGest.TipoGestionDomiciliacion != null && tipoGest.TipoGestionDomiciliacion.Length > 0)
                {
                    strScript.AppendFormat("tipoGestion={0};", JsonConvert.SerializeObject(tipoGest.TipoGestionDomiciliacion));
                }
            }

            using (CobranzaAdministrativaClient wsC = new CobranzaAdministrativaClient())
            {
                BancosDomiciliacionCuentaResponse bancoR = wsC.BancosDomiciliacionCuenta();
                if (bancoR != null)
                {
                    if (bancoR.Bancos != null && bancoR.Bancos.Length > 0)
                    {
                        List<BancoVM> bancosDom = new List<BancoVM>() {
                            new BancoVM { IdBanco = 0, Banco = "TODOS" }
                        };

                        bancosDom.AddRange(bancoR.Bancos);
                        bancosDom.Add(new BancoVM { IdBanco = -1, Banco = "OTROS" });

                        strScript.AppendFormat("bancosDomCuenta={0};", JsonConvert.SerializeObject(bancosDom));
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
    }

    #region WebMethods
    #region GET
    #endregion

    #region POST
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GestionDomiciliaciones(DataTableAjaxPostModel model, int tipoGestion, int? bancoDom)
    {
        string mensaje = string.Empty;
        int totalRegistros = 0;
        int registros = 0;
        object domiciliaciones = null;
        string url = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("Usuario Invalido");
            }

            GestionDomiciacionesRequest peticion = new GestionDomiciacionesRequest
            {
                IdUsuario = idUsuario,
                Pagina = (model.start / model.length) + 1,
                RegistrosPagina = model.length,
                IdTipoGestion = tipoGestion,
                IdBanco = bancoDom
            };

            GestionDomiciacionesResponse gestionDomiR;
            List<GestionDomiciliacionVM> gestionDom = new List<GestionDomiciliacionVM>();
            using (GestionClient wsG = new GestionClient())
            {
                gestionDomiR = wsG.GestionDomiciaciones(peticion);
                if (gestionDomiR != null && gestionDomiR.GestionDomiciliaciones != null && gestionDomiR.GestionDomiciliaciones.Length > 0)
                {
                    gestionDom = gestionDomiR.GestionDomiciliaciones.ToList();
                }
            }

            if (gestionDom.Count > 0)
            {
                domiciliaciones = gestionDom.Select(c => new
                {
                    c.IdGestionDomiciliacion,
                    c.IdSolicitud,
                    c.Cliente,
                    Monto = c.Monto.ToString("C"),
                    FechaDomiciliar = c.FechaDomiciliar.ToString("dd/MM/yyyy HH:mm"),
                    c.Banco,
                    c.CuentaBancaria,
                    c.NumeroTarjeta
                }).ToList();

                registros = gestionDom.Count;
                totalRegistros = gestionDomiR.TotalRegistros;
            }
        }
        catch (Exception ex)
        {
        }
        model.draw++;
        return new
        {
            drawn = model.draw,
            recordsTotal = registros,
            recordsFiltered = totalRegistros,
            data = domiciliaciones,
            url
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ValidarPeticionDomiciliacion(ValidarPeticionDomicilacionRequest peticion)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object archivoDom = null;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("La clave del usuario es inválido");
            }
            if (peticion == null) throw new Exception("La petición es incorrecta");
            if (peticion.IdTipoGestion < 0) throw new Exception("El tipo de domiciliación no es válido");
            if (!peticion.Todo && (peticion.IdGestionDomiciliacion == null || peticion.IdGestionDomiciliacion.Length == 0))
                throw new ValidacionExcepcion("Seleccione una cuenta para domiciliar.");

            peticion.IdUsuario = idUsuario;
            peticion.Origen = "ValidacionPeticion";
            using (GestionClient wsG = new GestionClient())
            {
                ValidarPeticionDomicilacionResponse peticionDomR = wsG.ValidarPeticionDomicilacion(peticion);
                if (peticionDomR.Error) throw new Exception(peticionDomR.MensajeOperacion);
                mensaje = "La actualización se realizó correctamente!";
            }
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }

        return new { error, mensaje, url, archivoDom };
    }
    #endregion
    #endregion
}