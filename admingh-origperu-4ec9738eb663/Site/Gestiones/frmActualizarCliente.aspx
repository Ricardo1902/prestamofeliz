﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmActualizarCliente.aspx.cs" Inherits="Site_Gestiones_frmActualizarCliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../Css/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="css/frmActualizarCliente.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />

    <style>
        /* Start by setting display:none to make this hidden.
           Then we position it in relation to the viewport window
           with position:fixed. Width, height, top and left speak
           for themselves. Background we set to 80% white with
           our animation centered, and no-repeating */
        .modal-pr {
            display: none;
            position: fixed;
            z-index: 9000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 ) url('../../Imagenes/ajax-loader.gif') 50% 50% no-repeat;
        }

        /* When the body has the loading class, we turn
           the scrollbar off with overflow:hidden */
        body.loading .modal-pr {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
           modal element will be visible */
        body.loading .modal-pr {
            display: block;
        }
    </style>

    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/datepicker-1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="../../js/parsley-2.9.2.min.js"></script>
    <script type="text/javascript" src="../../js/datepicker-1.9.0/locales/bootstrap-datepicker.es.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/tooltip.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/dropdown.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="../../js/moment.js"></script>
    <script type="text/javascript" src="js/frmActualizarCliente.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="page-header page-header-corp">
        <div class="row inline">
            <h3 style="display: inline;">Datos del Cliente</h3>
            <label href="#" id="lblUsuarioActualiza" class="pull-right badge badge-custom badge-large btn-warning">
                por:
                {{UsuarioActualizacion}}</label>
            <label href="#" id="lblFchActualiza" class="pull-right badge badge-custom badge-large btn-success">
                última actualización:
                {{FechaAcutalizacion}}</label>
        </div>
    </div>

    <div class="row row-clear labels-normal">
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblNombresCliente" for="txtNombresCliente">Nombre(s)</label>
                <input id="txtNombresCliente" type="text" name="txtNombresCliente" class="form-control"
                    disabled="disabled" value="" />
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="form-group">
                <label id="lblApellidoPaterno" for="txtApellidoPaterno">Apellido Paterno</label>
                <input id="txtApellidoPaterno" type="text" name="txtApellidoPaterno" class="form-control"
                    disabled="disabled" value="" />
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="form-group">
                <label id="lblApellidoMaterno" for="txtApellidoMaterno">Apellido Materno</label>
                <input id="txtApellidoMaterno" type="text" name="txtApellidoMaterno" class="form-control"
                    disabled="disabled" value="" />
            </div>
        </div>
        <div class="col-sm-6 col-md-2">
            <div class="form-group">
                <label id="lblFchNacimiento" for="txtFchNacimiento">Fecha Nacimiento</label>
                <div class="input-group">
                    <input id="txtFchNacimiento" type="text" name="txtFchNacimiento" class="form-control"
                        disabled="disabled" value="" />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-2">
            <div class="form-group">
                <label id="lblEdad" for="txtEdad">Edad</label>
                <input id="txtEdad" type="text" name="txtEdad" class="form-control" disabled="disabled" value="" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <div class="checkbox inline">
                    <label>
                        <input id="chkFemenino" value="8" type="checkbox" disabled="disabled">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                        Femenino
                    </label>
                    <label>
                        <input id="chkMasculino" value="7" type="checkbox" disabled="disabled">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                        Masculino
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-clear labels-normal">
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblDireccion" for="selDireccion">Dirección</label>
                <select id="selDireccion" name="selDireccion" class="form-control" required data-parsley-seleccion-combo="selDireccion">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblVia" for="txtVia">Nombre Via</label>
                <input id="txtVia" type="text" name="txtVia" class="form-control" value="" data-parsley-maxlength="100"
                    data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-sm-6 col-md-2">
            <div class="form-group">
                <label id="lblNumExterior" for="txtNumExterior">Num Exterior</label>
                <input id="txtNumExterior" type="text" name="txtNumExterior" class="form-control" value=""
                    data-parsley-maxlength="10" data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-sm-6 col-md-2">
            <div class="form-group">
                <label id="lblNumInterior" for="txtNumInterior">Num Interior</label>
                <input id="txtNumInterior" type="text" name="txtNumInterior" class="form-control" value=""
                    data-parsley-maxlength="10" data-parsley-trigger="change" />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label id="lblTipoZona" for="selTipoZona">Tipo Zona</label>
                <select id="selTipoZona" name="selTipoZona" class="form-control" required data-parsley-seleccion-combo="selTipoZona">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblNombreZona" for="txtNombreZona">Nombre Zona</label>
                <input id="txtNombreZona" type="text" name="txtNombreZona" class="form-control" value=""
                    data-parsley-maxlength="100" data-parsley-trigger="change" />
            </div>
        </div>
    </div>
    <div class="row row-clear labels-normal">
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblDepartamento" for="selDepartamento">Departamento</label>
                <select id="selDepartamento" name="selDepartamento" class="form-control" required data-parsley-seleccion-combo="selDepartamento">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblProvincia" for="selProvincia">Provincia</label>
                <select id="selProvincia" name="selProvincia" class="form-control" required data-parsley-seleccion-combo="selProvincia">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblDistrito" for="selDistrito">Distrito</label>
                <select id="selDistrito" name="selDistrito" class="form-control" required data-parsley-seleccion-combo="selDistrito">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label id="lblReferenciaDomicilio" for="txtReferenciaDomicilio">Referencia del domicilio</label>
                <textarea name="txtReferenciaDomicilio" id="txtReferenciaDomicilio" rows="6" class="form-control"
                    data-parsley-maxlength="500" data-parsley-trigger="change"></textarea>
            </div>
        </div>
    </div>

    <div class="row row-clear labels-normal">
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblTelefono" for="txtTelefono">Telefono de casa</label>
                <input id="txtTelefono" type="text" name="txtTelefono" class="form-control" value=""
                    data-parsley-trigger="change" data-parsley-telefono-peru="selDepartamento" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblTelefonoCelular" for="txtTelefonoCelular">Teléfono celular</label>
                <input id="txtTelefonoCelular" type="text" name="txtTelefonoCelular" class="form-control" value=""
                    data-parsley-trigger="change" data-parsley-pattern="^[0-9]{9}$"
                    data-parsley-pattern-message="El Celular debe ser de 9 digitos" />
            </div>
        </div>
    </div>


    <%--SEPARADOR PARA DATOS LABORALES--%>
    <div class="page-header page-header-corp">
        <h3>Datos Laborales</h3>
    </div>
    <div class="row row-clear labels-normal">
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblOcupacion" for="txtOcupacion">Ocupacion</label>
                <input id="txtOcupacion" type="text" name="txtOcupacion" class="form-control" value=""
                    data-parsley-maxlength="100" data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group form-horizontal">
                <label id="lblEmpresaOficina" for="selEmpresaOficina">Empresa</label>
                <select id="selEmpresaOficina" name="selEmpresaOficina" class="selectpicker from-control" data-live-search="true" required data-parsley-seleccion-combo="selEmpresaOficina">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row row-clear labels-normal">
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblOrganoPago" for="selOrganoPago">Organo de Pago</label>
                <select id="selOrganoPago" name="selOrganoPago" class="form-control" disabled="disabled">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblNivelRiesgo" for="txtNivelRiesgo">Nivel de Riesgo</label>
                <input id="txtNivelRiesgo" type="text" name="txtNivelRiesgo" class="form-control" value="" disabled="disabled" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblSituacionLaboral" for="selSituacionLaboral">Situación laboral</label>
                <select id="selSituacionLaboral" name="selSituacionLaboral" class="form-control">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div id="regimenPension" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblRegimenPension" for="selRegimenPension">Régimen de Pensión</label>
                <select id="selRegimenPension" name="selRegimenPension" class="form-control">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
    </div>
    <%--<div class="col-md-4">
            <div class="form-group">
                <label id="lblRazonSocial" for="txtRazonSocial">Razón Social de la Empresa</label>
                <input id="txtRazonSocial" type="text" name="txtRazonSocial" class="form-control" value=""
                    data-parsley-maxlength="150" data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblRuc" for="txtRuc">RUC de la Empresa</label>
                <input id="txtRuc" type="text" name="txtRuc" class="form-control" value="" data-parsley-maxlength="11"
                    data-parsley-trigger="change" />
            </div>
        </div>--%>
    <%--   <div class="col-md-4">
            <div class="form-group">
                <label id="lblGiroNegocio" for="selGiroNegocio">Giro del Negocio</label>
                <select id="selGiroNegocio" name="selGiroNegocio" class="form-control">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>--%>

    <div class="row row-clear labels-normal">
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblDepartamentoOficina" for="selDepartamentoOficina">Departamento</label>
                <select id="selDepartamentoOficina" name="selDepartamentoOficina" class="form-control" required data-parsley-seleccion-combo="selDepartamentoOficina">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblProvinciaOficina" for="selProvinciaOficina">Provincia</label>
                <select id="selProvinciaOficina" name="selProvinciaOficina" class="form-control" required data-parsley-seleccion-combo="selProvinciaOficina">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblDistritoOficina" for="selDistritoOficina">Distrito</label>
                <select id="selDistritoOficina" name="selDistritoOficina" class="form-control" required data-parsley-seleccion-combo="selDistritoOficina">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label id="lblCalleOficina" for="txtCalleOficina">Calle</label>
                <input id="txtCalleOficina" type="text" name="txtCalleOficina" class="form-control" value=""
                    data-parsley-maxlength="70" data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label id="lblNumExteriorOficina" for="txtNumExteriorOficina">Num Exterior</label>
                <input id="txtNumExteriorOficina" type="text" name="txtNumExteriorOficina" class="form-control" value=""
                    data-parsley-maxlength="10" data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label id="lblNumInteriorOficina" for="txtNumInteriorOficina">Num Interior</label>
                <input id="txtNumInteriorOficina" type="text" name="txtNumInteriorOficina" class="form-control" value=""
                    data-parsley-maxlength="10" data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblDependenciaOficina" for="txtDependenciaOficina">Dependencia</label>
                <input id="txtDependenciaOficina" type="text" name="txtDependenciaOficina" class="form-control" value=""
                    data-parsley-maxlength="50" data-parsley-trigger="change" />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label id="lblUbicacion" for="txtUbicacion">Ubicación</label>
                <input id="txtUbicacion" type="text" name="txtUbicacion" class="form-control" value=""
                    data-parsley-maxlength="100" data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblTelefonoOficina" for="txtTelefonoOficina">Teléfono</label>
                <input id="txtTelefonoOficina" type="text" name="txtTelefonoOficina" class="form-control" value=""
                    data-parsley-trigger="change" data-parsley-telefono-peru="selDepartamentoOficina" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblAnexoOficina" for="txtAnexoOficina">Anexo</label>
                <input id="txtAnexoOficina" type="text" name="txtAnexoOficina" class="form-control" value=""
                    data-parsley-maxlength="6" data-parsley-trigger="change" />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label id="lblPuestoOficina" for="txtPuestoOficina">Puesto</label>
                <input id="txtPuestoOficina" type="text" name="txtPuestoOficina" class="form-control" value=""
                    data-parsley-maxlength="100" data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblAntiguedadOficina" for="txtAntiguedadOficina">Antigüedad Laboral</label>
                <input id="txtAntiguedadOficina" type="text" name="txtAntiguedadOficina" class="form-control" value=""
                    data-parsley-pattern="^[0-9]{1,2}(\.[0-9]{1})?$" data-parsley-pattern-message="Ingrese un numero maximo de 2 digitos con maximo 1 decimal"
                    data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblIngresoBruto" for="txtIngresoBruto">Ingreso Bruto Mensual</label>
                <input id="txtIngresoBruto" type="text" name="txtIngresoBruto" class="form-control" value=""
                    data-parsley-pattern="^[0-9]*(\.[0-9]+)?$" data-parsley-pattern-message="El monto es inválido"
                    data-parsley-trigger="change" />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label id="lblDescuentosOficina" for="txtDescuentosOficina">Descuentos de Ley</label>
                <input id="txtDescuentosOficina" type="text" name="txtDescuentosOficina" class="form-control" value=""
                    data-parsley-pattern="^[0-9]*(\.[0-9]+)?$" data-parsley-pattern-message="El monto es inválido"
                    data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblOtrosDescuentos" for="txtOtrosDescuentos">Otros Descuentos</label>
                <input id="txtOtrosDescuentos" type="text" name="txtOtrosDescuentos" class="form-control" value=""
                    data-parsley-pattern="^[0-9]*(\.[0-9]+)?$" data-parsley-pattern-message="El monto es inválido"
                    data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblIngreso" for="txtIngreso">Ingreso Neto Mensual</label>
                <input id="txtIngreso" type="text" name="txtIngreso" class="form-control" value=""
                    data-parsley-pattern="^[0-9]*(\.[0-9]+)?$" data-parsley-pattern-message="El monto es inválido"
                    data-parsley-trigger="change" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblOtrosIngresos" for="txtOtrosIngresos">Otros Ingresos</label>
                <input id="txtOtrosIngresos" type="text" name="txtOtrosIngresos" class="form-control" value=""
                    data-parsley-pattern="^[0-9]*(\.[0-9]+)?$" data-parsley-pattern-message="El monto es inválido"
                    data-parsley-trigger="change" />
            </div>
        </div>
    </div>

    <%--SEPARADOR PARA DATOS DE COBRO--%>
    <div class="page-header page-header-corp">
        <h3>Datos de Cobro</h3>
    </div>
    <div class="row row-clear labels-normal">
        <div id="secBanco" class="col-md-4">
            <div class="form-group">
                <label id="lblBancoCobro" for="selBancoCobro">Banco</label>
                <select id="selBancoCobro" name="selBancoCobro" class="form-control">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div id="secNombreBancoCobro" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblNombreBancoCobro" for="txtNombreBancoCobro">Nombre del Banco</label>
                <input id="txtNombreBancoCobro" type="text" name="txtNombreBancoCobro" class="form-control" value=""
                    data-parsley-trigger="change" data-parsley-maxlength="100" />
            </div>
        </div>
        <div id="secEntidadCobro" class="col-md-2 collapse">
            <div class="form-group">
                <label id="lblEntidadCobro" for="txtEntidadCobro">Entidad</label>
                <input id="txtEntidadCobro" type="text" name="txtEntidadCobro" class="form-control" sufijo="Cobro"
                    value="" data-parsley-trigger="change" data-parsley-minlength="4" data-parsley-maxlength="4" />
            </div>
        </div>
        <div id="secOficinaCobro" class="col-md-2 collapse">
            <div class="form-group">
                <label id="lblOficinaCobro" for="txtOficinaCobro">Oficina</label>
                <input id="txtOficinaCobro" type="text" name="txtOficinaCobro" class="form-control" sufijo="Cobro"
                    value="" data-parsley-trigger="change" data-parsley-minlength="4" data-parsley-maxlength="4" />
            </div>
        </div>
        <div id="secDCCobro" class="col-md-2 collapse">
            <div class="form-group">
                <label id="lblDCCobro" for="txtDCCobro">DC</label>
                <input id="txtDCCobro" type="text" name="txtDCCobro" class="form-control" sufijo="Cobro" value=""
                    data-parsley-trigger="change" data-parsley-minlength="2" data-parsley-maxlength="2" />
            </div>
        </div>
        <div id="secNoOficinaCobro" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblNoOficinaCobro" for="txtNoOficinaCobro">No. Oficina</label>
                <input id="txtNoOficinaCobro" type="text" name="txtNoOficinaCobro" class="form-control" value=""
                    data-parsley-trigger="change" data-parsley-minlength="3" data-parsley-maxlength="3" />
            </div>
        </div>
        <div id="secCuentaCobro" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblCuentaCobro" for="txtCuentaCobro">Cuenta</label>
                <input id="txtCuentaCobro" type="text" name="txtCuentaCobro" class="form-control" sufijo="Cobro"
                    value="" data-parsley-trigger="change" data-parsley-minlength="10" data-parsley-maxlength="10" />
            </div>
        </div>
        <div id="secCuentaBancariaCobro" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblCuentaBancariaCobro" for="txtCuentaBancariaCobro">Cuenta Bancaria</label>
                <input id="txtCuentaBancariaCobro" type="text" name="txtCuentaBancariaCobro" class="form-control"
                    value="" data-parsley-trigger="change" data-parsley-maxlength="20" />
            </div>
        </div>
        <div id="secCuentaCCICobro" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblCuentaCCICobro" for="txtCuentaCCICobro">Cuenta C.C.I.</label>
                <input id="txtCuentaCCICobro" type="text" name="txtCuentaCCICobro" class="form-control" value=""
                    data-parsley-trigger="change" data-parsley-maxlength="20" />
            </div>
        </div>
    </div>
    <div class="row row-cloear labels-normal">
        <div class="col-md-12">
            <div class="form-group label-control">
                <label class="custom-switch">
                    <input id="chkVisaDom" name="chkVisaDom" type="checkbox">
                    <span class="custom-slider round"></span>
                </label>
                <span for="chkVisaDom" class="label-control-label">Domiciliación Visa</span>
            </div>
        </div>
    </div>

    <div id="secVisaDom" class="row row-clear labels-normal collapse">
        <div class="col-md-4">
            <div id="secMascaraTarjetaVisa" class="form-group">
                <label id="lblMascaraTarjetaVisa" for="txtMascaraTarjetaVisa">Numero de Tarjeta</label>
                <div class="input-group">
                    <input id="txtMascaraTarjetaVisa" type="text" name="txtMascaraTarjetaVisa" disabled="disabled"
                        class="form-control" value="" />
                    <div class="input-group-btn">
                        <button id="btnEditarNumeroTarjetaVisa" name="btnEditarNumeroTarjetaVisa"
                            class="btn btn-default" type="button">
                            Editar</button>
                    </div>
                </div>
            </div>
            <div id="secTarjetaVisa" class="form-group collapse">
                <label id="lblNumeroTarjetaVisa" for="txtNumeroTarjetaVisa">Numero de Tarjeta</label>
                <div class="input-group-separado">
                    <input id="txtNumeroTarjetaVisa" type="text" name="txtNumeroTarjetaVisa" class="form-control"
                        data-parsley-trigger="change" data-parsley-tarjeta-visa=""
                        data-parsley-tarjeta-visa-message="El número de tarjeta no es válido" value="" />
                    <div class="input-group-btn">
                        <button id="btnCancelarCapcturaTarjeta" type="button"
                            class="btn btn-primary btn-danger-line collapse">
                            Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblMesExpiracionVisa" for="selMesExpiracionVisa">Mes de Expiración</label>
                <select id="selMesExpiracionVisa" name="selMesExpiracionVisa" class="form-control"
                    data-parsley-trigger="change" data-parsley-seleccion-combo="selMesExpiracionVisa">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label id="lblAñoExpiracionVisa" for="txtAñoExpiracionVisa">Año Expiración</label>
                <input id="txtAñoExpiracionVisa" type="text" name="txtAñoExpiracionVisa" class="form-control" value=""
                    data-parsley-anio-expiracion="txtAñoExpiracionVisa"
                    data-parsley-anio-expiracion-message="El año de expiracion es inválido"
                    data-parsley-trigger="change" />
            </div>
        </div>
    </div>
    <div class="row row-clear labels-normal">
        <div class="col-md-12">
            <div class="form-group label-control">
                <label class="custom-switch">
                    <input id="chkCuentaDomEmergente" name="chkCuentaDomEmergente" type="checkbox">
                    <span class="custom-slider round"></span>
                </label>
                <span for="chkCuentaDomEmergente" class="label-control-label">Cuenta de Domiciliación Emergente
                </span>
            </div>
        </div>
    </div>
    <div id="secCuentaDomEmergente" class="row row-clear labels-normal collapse">
        <div id="secBancoEmergente" class="col-md-4">
            <div class="form-group">
                <label id="lblBancoEmergente" for="selBancoEmergente">Banco</label>
                <select id="selBancoEmergente" name="selBancoEmergente" class="form-control">
                    <option value="0">0 - SELECCIONE UNA OPCIÓN</option>
                </select>
            </div>
        </div>
        <div id="secNombreBancoEmergente" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblNombreBancoEmergente" for="txtNombreBancoEmergente">Nombre del Banco</label>
                <input id="txtNombreBancoEmergente" type="text" name="txtNombreBancoEmergente" class="form-control"
                    value="" data-parsley-trigger="change" data-parsley-maxlength="100" />
            </div>
        </div>
        <div id="secEntidadEmergente" class="col-md-2 collapse">
            <div class="form-group">
                <label id="lblEntidadEmergente" for="txtEntidadEmergente">Entidad</label>
                <input id="txtEntidadEmergente" type="text" name="txtEntidadEmergente" class="form-control"
                    sufijo="Emergente" value="" data-parsley-trigger="change" data-parsley-minlength="4"
                    data-parsley-maxlength="4" />
            </div>
        </div>
        <div id="secOficinaEmergente" class="col-md-2 collapse">
            <div class="form-group">
                <label id="lblOficinaEmergente" for="txtOficinaEmergente">Oficina</label>
                <input id="txtOficinaEmergente" type="text" name="txtOficinaEmergente" class="form-control"
                    sufijo="Emergente" value="" data-parsley-trigger="change" data-parsley-minlength="4"
                    data-parsley-maxlength="4" />
            </div>
        </div>
        <div id="secDCEmergente" class="col-md-2 collapse">
            <div class="form-group">
                <label id="lblDCEmergente" for="txtDCEmergente">DC</label>
                <input id="txtDCEmergente" type="text" name="txtDCEmergente" class="form-control" sufijo="Emergente"
                    value="" data-parsley-trigger="change" data-parsley-minlength="2" data-parsley-maxlength="2" />
            </div>
        </div>
        <div id="secNoOficinaEmergente" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblNoOficinaEmergente" for="txtNoOficinaEmergente">No. Oficina</label>
                <input id="txtNoOficinaEmergente" type="text" name="txtNoOficinaEmergente" class="form-control" value=""
                    data-parsley-trigger="change" data-parsley-minlength="3" data-parsley-maxlength="3" />
            </div>
        </div>
        <div id="secCuentaEmergente" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblCuentaEmergente" for="txtCuentaEmergente">Cuenta</label>
                <input id="txtCuentaEmergente" type="text" name="txtCuentaEmergente" class="form-control"
                    sufijo="Emergente" value="" data-parsley-trigger="change" data-parsley-minlength="10" data-parsley-maxlength="10" />
            </div>
        </div>
        <div id="secCuentaBancariaEmergente" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblCuentaBancariaEmergente" for="txtCuentaBancariaEmergente">Cuenta Bancaria</label>
                <input id="txtCuentaBancariaEmergente" type="text" name="txtCuentaBancariaEmergente"
                    class="form-control" value="" data-parsley-trigger="change" data-parsley-maxlength="20" />
            </div>
        </div>
        <div id="secCuentaCCIEmergente" class="col-md-4 collapse">
            <div class="form-group">
                <label id="lblCuentaCCIEmergente" for="txtCuentaCCIEmergente">Cuenta C.C.I.</label>
                <input id="txtCuentaCCIEmergente" type="text" name="txtCuentaCCIEmergente" class="form-control" value=""
                    data-parsley-trigger="change" data-parsley-maxlength="20" />
            </div>
        </div>
    </div>

    <div class="page-header page-header-corp">
    </div>
    <div class="row row-clear labels-normal">
        <div class="col-md-6">
            <div class="btn-toolbar" role="toolbar" aria-label="Controles">
                <div class="btn-group" role="group" aria-label="Guardar datos del Cliente">
                    <button id="btnGuardar" class="btn btn-primary" type="submit">Guardar</button>
                </div>
                <div class="btn-group" role="group" aria-label="Cancelar edición">
                    <button id="btnCancelar" class="btn btn-default" type="button" style="margin-left: 16px;" >Cancelar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-pr"></div>
</asp:Content>
