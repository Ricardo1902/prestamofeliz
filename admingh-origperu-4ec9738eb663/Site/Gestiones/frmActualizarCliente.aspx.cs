﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web;
using System.Text.RegularExpressions;
using System.Xml;
using System.Data;

public class ActClienteGest : TBClientes.ActualizaClienteGestiones
{
    public int IdEmpresa { get; set; }
    public int IdOrganoPago { get; set; }
    public int Origen { get; set; }
}
public partial class Site_Gestiones_frmActualizarCliente : System.Web.UI.Page
{
    private int IdSolicitud = 0;
    private int IdCliente = 0;
    private int Origen = 0;
    private StringBuilder strScript = new StringBuilder();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (Request.UrlReferrer == null)
            //{
            //    Response.Redirect("../Principal.aspx");
            //}
            ViewState["PreviousPage"] = Request.UrlReferrer;
            strScript.AppendFormat("previousPage='{0}';", Request.UrlReferrer);
            int.TryParse(Request.QueryString["IdSolicitud"], out IdSolicitud);
            int.TryParse(Request.QueryString["idCliente"], out IdCliente);
            ObtenerEstados();
            ObtenerDetalleCliente();
            Console.WriteLine("strScript " + strScript);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
        }
    }

    private void ObtenerDetalleCliente()
    {
        if (IdSolicitud > 0 && IdCliente > 0)
        {
            using (ClientesClient clientesClient = new ClientesClient())
            {
                var clientes = clientesClient.ObtenerTBClientes(1, IdCliente);
                if (clientes != null && clientes.Length > 0)
                {
                    TBClientes tempCliente = clientes[0];
                    if (tempCliente != null)
                    {
                        strScript.AppendFormat("cliente={0};", JsonConvert.SerializeObject(tempCliente));
                        if (tempCliente.IdUsuarioActualiza > 0)
                        {
                            var detalleUsuarioActualiza = ObtenerDetalleUsuario(tempCliente.IdUsuarioActualiza);
                            if (detalleUsuarioActualiza != null && detalleUsuarioActualiza.IdUsuario > 0)
                                strScript.AppendFormat("usuarioActualiza={0};", JsonConvert.SerializeObject(detalleUsuarioActualiza));
                        }
                        if (tempCliente.IdCoOfic > 0)
                        {
                            var detalleColoniaOficina = ObtenerDetalleColonia((int)tempCliente.IdCoOfic);
                            if (detalleColoniaOficina != null)
                            {
                                strScript.AppendFormat("detColoniaOficina={0};", JsonConvert.SerializeObject(detalleColoniaOficina));
                            }
                        }
                        if (tempCliente.IdConfiguracionEmpresa > 0)
                        {
                            var respuestaConfigEmpresa = ObtenerConfiguracionEmpresa(new ObtenerConfiguracionEmpresaRequest()
                            {
                                Accion = "OBTENER_POR_ID",
                                IdConfiguracion = tempCliente.IdConfiguracionEmpresa
                            });
                            if (!respuestaConfigEmpresa.Error && respuestaConfigEmpresa.Resultado.Count() > 0)
                            {
                                strScript.AppendFormat("empresaOficina={0};", JsonConvert.SerializeObject(respuestaConfigEmpresa.Resultado[0]));
                            }
                            else
                            {
                                throw new Exception(respuestaConfigEmpresa.MensajeOperacion);
                            }
                        }

                        //Se limpia variable temporal de cliente
                        tempCliente = null;

                        var direcciones_PE = ObtenerCatalogoDirecciones();
                        if (direcciones_PE != null)
                            strScript.AppendFormat("direccionesPE={0};", JsonConvert.SerializeObject(direcciones_PE));

                        var tipoZonas_PE = ObtenerCatalogoTipoZona();
                        if (tipoZonas_PE != null)
                            strScript.AppendFormat("tipoZonasPE={0};", JsonConvert.SerializeObject(tipoZonas_PE));

                        var situacionesLaborales_PF = ObtenerCatalogosituacionLaboral();
                        if (situacionesLaborales_PF != null)
                            strScript.AppendFormat("sitLaboralesPF={0};", JsonConvert.SerializeObject(situacionesLaborales_PF));

                        var tiposPension = ObtenerCatalogoTipoPension();
                        if (tiposPension != null)
                            strScript.AppendFormat("tiposPension={0};", JsonConvert.SerializeObject(tiposPension));

                        var giros = ObtenerCatalogoGiro();
                        if (giros != null)
                            strScript.AppendFormat("giros={0};", JsonConvert.SerializeObject(giros));

                        var empresasPE = ObtenerCatalogoEmpresa();
                        if (empresasPE != null)
                            strScript.AppendFormat("empresasPE={0};", JsonConvert.SerializeObject(empresasPE));

                        var organosPE = ObtenerCatalogoOrganoPago();
                        if (organosPE != null)
                            strScript.AppendFormat("organosPE={0};", JsonConvert.SerializeObject(organosPE));
                    }

                    var solicitudes = ObtenerDetalleSolicitud(IdSolicitud);
                    if (solicitudes != null && solicitudes.Count > 0)
                    {
                        var tempSolicitud = solicitudes[0];
                        if (!String.IsNullOrWhiteSpace(tempSolicitud.NumeroTarjetaVisa) && tempSolicitud.NumeroTarjetaVisa.Length > 12)
                        {
                            if (!String.IsNullOrWhiteSpace(tempSolicitud.NumeroTarjetaVisa))
                                Session["TarjetaVisa"] = tempSolicitud.NumeroTarjetaVisa;
                            string patron = @"^\d{12}";
                            tempSolicitud.NumeroTarjetaVisa = Regex.Replace(tempSolicitud.NumeroTarjetaVisa, patron, "**** **** **** ");
                        }
                        if (tempSolicitud.CuentasDomiciliacionEmergentes != null && tempSolicitud.CuentasDomiciliacionEmergentes.Length > 0)
                        {
                            Session["IdSolicitudEmergente"] = tempSolicitud.CuentasDomiciliacionEmergentes[0].IdSolicitud;
                        }
                        strScript.AppendFormat("solicitud={0};", JsonConvert.SerializeObject(tempSolicitud));
                    }
                    var direccionesSolicitud = ObtenerDireccionesSolicitud(IdSolicitud, IdCliente);
                    var countDirecciones = direccionesSolicitud.Count;
                    if (direccionesSolicitud != null && countDirecciones > 0)
                    {
                        var tempDireccion = direccionesSolicitud[0];
                        Session["IdSolicitudDireccion"] = tempDireccion.idSolicitud;
                        strScript.AppendFormat("direccion={0};", JsonConvert.SerializeObject(tempDireccion));

                        // Se consulta los datos de la colonia de dicha direccion
                        if (tempDireccion.idColonia > 0)
                        {
                            var detalleColonia = ObtenerDetalleColonia((int)tempDireccion.idColonia);
                            if (detalleColonia != null)
                                strScript.AppendFormat("detColoniaCliente={0};", JsonConvert.SerializeObject(detalleColonia));
                        }
                    }
                }
            }
        }
    }

    private void ObtenerEstados()
    {
        try
        {
            using (CatalogoClient catalogoClient = new CatalogoClient())
            {
                var estados = catalogoClient.ObtenerTBCatEstado(0, 0, "", 0);
                if (estados != null && estados.Length > 0)
                {
                    strScript.AppendFormat("estados={0};", JsonConvert.SerializeObject(estados));
                }
            }
        }
        catch (Exception)
        {
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<TBCATCiudad> ObtenerCiudades(int idEstado = 0)
    {
        var ciudades = new List<TBCATCiudad>();
        try
        {
            if (HttpContext.Current.ValidarUsuario() > 0)
            {
                using (CatalogoClient catalogoClient = new CatalogoClient())
                {
                    var respuestaCiudades = catalogoClient.ObtenerTbCatCiudadOrig(2, idEstado, 0);
                    if (respuestaCiudades != null && respuestaCiudades.Length > 0)
                    {
                        ciudades = respuestaCiudades.OrderBy(c => c.IdCiudad).ToList();
                    }
                }
            }
        }
        catch (Exception)
        {
        }
        return ciudades;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<TBCATColonia> ObtenerColonias(int idCiudad = 0)
    {
        var colonias = new List<TBCATColonia>();
        try
        {
            if (HttpContext.Current.ValidarUsuario() > 0)
            {
                using (CatalogoClient catalogoClient = new CatalogoClient())
                {
                    var respuestaColonias = catalogoClient.ObtenerTbCatColonia(3, idCiudad);
                    if (respuestaColonias != null && respuestaColonias.Length > 0)
                    {
                        colonias = respuestaColonias.ToList();
                    }
                }
            }
        }
        catch (Exception)
        {
        }
        return colonias;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static DetalleColoniaVM ObtenerDetalleColonia(int idColonia)
    {
        var detalle = new DetalleColoniaVM();
        try
        {
            using (CatalogoClient catalogoClient = new CatalogoClient())
            {
                var respuestadetalle = catalogoClient.ObtenerDetalleColonia(0, idColonia);
                if (respuestadetalle != null && respuestadetalle.IdColonia > 0)
                {
                    detalle = respuestadetalle;
                }
            }
        }
        catch (Exception)
        {
        }
        return detalle;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<Combo> ObtenerCatalogoDirecciones()
    {
        List<Combo> direcciones_PE = new List<Combo>();
        try
        {
            using (CatalogoClient catClient = new CatalogoClient())
            {
                var respuestaDirecciones = catClient.ObtenerCombo_Direccion_PE();
                if (catClient != null)
                {
                    direcciones_PE = respuestaDirecciones.ToList();
                }
            }
        }
        catch (Exception)
        {
        }

        return direcciones_PE;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<DireccionCliente> ObtenerDireccionesSolicitud(int idSolicitud, int idCliente)
    {
        var listaDirecciones = new List<DireccionCliente>();
        try
        {
            if (idSolicitud > 0 && idCliente > 0)
            {
                using (var wsClientes = new ClientesClient())
                {
                    DireccionCliente[] direcciones = wsClientes.ObtenerDireccionesCliente(idCliente);
                    if (direcciones.Length > 0)
                    {
                        listaDirecciones = direcciones.Reverse().ToList(); //Se revierte la lista para obtener siempre al inicio la mas reciente
                    }
                }
            }

        }
        catch (Exception ex)
        {
        }
        return listaDirecciones;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<Combo> ObtenerCatalogoTipoZona()
    {
        List<Combo> tipoZonas_PE = new List<Combo>();
        try
        {
            using (CatalogoClient catClient = new CatalogoClient())
            {
                var respuestaTipoZonas = catClient.ObtenerCombo_TipoZona_PE();
                if (catClient != null)
                {
                    tipoZonas_PE = respuestaTipoZonas.ToList();
                }
            }
        }
        catch (Exception)
        {
        }

        return tipoZonas_PE;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<Combo> ObtenerCatalogosituacionLaboral()
    {
        List<Combo> situacionesLaborales_PE = new List<Combo>();
        try
        {
            using (CatalogoClient catClient = new CatalogoClient())
            {
                var respuestaSituaciones = catClient.ObtenerCombo_SituacionLaboral_PE();
                if (catClient != null)
                {
                    situacionesLaborales_PE = respuestaSituaciones.ToList();
                }
            }
        }
        catch (Exception)
        {
        }

        return situacionesLaborales_PE;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<TipoPension> ObtenerCatalogoTipoPension()
    {
        List<TipoPension> tiposPension = new List<TipoPension>();
        try
        {
            using (CatalogoClient catClient = new CatalogoClient())
            {
                var respuestaTiposPension = catClient.ObtenerTipoPension(1);
                if (catClient != null)
                {
                    tiposPension = respuestaTiposPension.ToList();
                }
            }
        }
        catch (Exception)
        {
        }

        return tiposPension;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<TBCATGiro> ObtenerCatalogoGiro()
    {
        List<TBCATGiro> giros = new List<TBCATGiro>();
        try
        {
            using (CatalogoClient catClient = new CatalogoClient())
            {
                var respuestaGiros = catClient.ObtenerTBCATGiro(0, 0, "", 0);
                if (catClient != null)
                {
                    giros = respuestaGiros.ToList();
                }
            }
        }
        catch (Exception)
        {
        }

        return giros;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<Combo> ObtenerCatalogoEmpresa()
    {
        var empresas = new List<Combo>();
        try
        {
            if (HttpContext.Current.ValidarUsuario() > 0)
            {
                using (CatalogoClient catalogoClient = new CatalogoClient())
                {
                    var respuestaEmpresas = catalogoClient.ObtenerCombo_Empresa_PE();
                    if (respuestaEmpresas != null && respuestaEmpresas.Length > 0)
                    {
                        empresas = respuestaEmpresas.ToList();
                    }
                }
            }
        }
        catch (Exception)
        {
        }
        return empresas;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<Combo> ObtenerCatalogoOrganoPago()
    {
        //ObtenerCombo_OrganoPago_PE
        var organos = new List<Combo>();
        try
        {
            if (HttpContext.Current.ValidarUsuario() > 0)
            {
                using (CatalogoClient catalogoClient = new CatalogoClient())
                {
                    var respuestaOrganos = catalogoClient.ObtenerCombo_OrganoPago_PE();
                    if (respuestaOrganos != null && respuestaOrganos.Length > 0)
                    {
                        organos = respuestaOrganos.ToList();
                    }
                }
            }
        }
        catch (Exception)
        {
        }
        return organos;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static ObtenerConfiguracionEmpresaResponse ObtenerConfiguracionEmpresa(ObtenerConfiguracionEmpresaRequest peticion)
    {
        var configuracion = new ObtenerConfiguracionEmpresaResponse();
        using (CatalogoClient wsCatalogo = new CatalogoClient())
        {
            var respuestaConfig = wsCatalogo.ObtenerConfiguracionEmpresa(peticion);
            if (!respuestaConfig.Error && respuestaConfig.Resultado.Count() > 0)
            {
                configuracion = respuestaConfig;
            }
        }
        return configuracion;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<TBSolicitudes.DatosSolicitud> ObtenerDetalleSolicitud(int idSolicitud)
    {
        List<TBSolicitudes.DatosSolicitud> datosSolicitud = new List<TBSolicitudes.DatosSolicitud>();
        try
        {
            if (idSolicitud > 0)
            {
                using (SolicitudClient solicitudClient = new SolicitudClient())
                {
                    var respuestaDatos = solicitudClient.ObtenerDatosSolicitud(idSolicitud);
                    if (respuestaDatos != null && respuestaDatos.Length > 0)
                    {
                        datosSolicitud = respuestaDatos.ToList();
                    }
                }
            }
        }
        catch (Exception)
        {
        }
        return datosSolicitud;
    }
    private TBCATUsuario ObtenerDetalleUsuario(int idUsuario)
    {
        TBCATUsuario usuario = null;
        try
        {
            using (UsuarioClient usuariosClient = new UsuarioClient())
            {
                var respUsuario = usuariosClient.ObtenerUsuario(5, idUsuario, "");
                if (respUsuario != null && respUsuario.Length > 0)
                    usuario = respUsuario.Select(u => new TBCATUsuario { IdUsuario = u.IdUsuario, NombreCompleto = u.NombreCompleto }).ToList()[0];
            }
        }
        catch (Exception)
        {

        }

        return usuario;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ResultadoOfboolean> ActualizarInformacion(TBSolicitudes.ActualizarSolicitudGestiones solicitud, ActClienteGest cliente, SolicitudDireccion direccion, TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente)
    {
        var respuesta = new List<ResultadoOfboolean>();
        var context = HttpContext.Current;
        try
        {
            var idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0) throw new ValidacionExcepcion("No se encuentra una sesion de usuario.");
            if (solicitud == null) throw new ValidacionExcepcion("Los datos de la solicitud son inválidos");
            if (direccion == null) throw new ValidacionExcepcion("Los datos de la direccion son inválidos");
            if (cliente == null) throw new ValidacionExcepcion("Los datos del cliente son inválidos");

            cliente.IdUsuario = idUsuario;
            if (cliente.IdEmpresa > 0)
            {
                ObtenerConfiguracionEmpresaResponse respuestaConfigEmpresa = new ObtenerConfiguracionEmpresaResponse();
                using (CatalogoClient wsCatalogo = new CatalogoClient())
                {
                    respuestaConfigEmpresa = wsCatalogo.ObtenerConfiguracionEmpresa(new ObtenerConfiguracionEmpresaRequest()
                    {
                        Accion = "OBTENER_ID_POR_CAMPOS",
                        IdEmpresa = cliente.IdEmpresa,
                        IdOrgano = cliente.IdOrganoPago,
                        IdSituacionLaboral = cliente.SituacionLaboral ?? 0,
                        IdRegimenPension = cliente.RegimenPension ?? 0
                    });
                    if (respuestaConfigEmpresa != null && !respuestaConfigEmpresa.Error && respuestaConfigEmpresa.IdConfiguracionEmpresa > 0)
                    {
                        cliente.IdConfiguracionEmpresa = respuestaConfigEmpresa.IdConfiguracionEmpresa;
                    }
                    else
                    {
                        throw new ValidacionExcepcion("La combinación de empresa, situación laboral y/o régimen de pensión no existe o es inválida, por favor intente con otros datos o agrege la configuración faltante.");
                    }
                }
            }

            direccion.IdUsuario = idUsuario;
            var respSolicitud = ActualizarDatosSolicitud(idUsuario, solicitud);
            if (respSolicitud != null) respuesta.Add(respSolicitud);

            ResultadoOfboolean respDireccion = null;
            if (direccion.idClienteDireccion > 0)
            {
                int tempIdSolicitudDireccion = 0;
                if (context.Session["IdSolicitudDireccion"] != null) int.TryParse(context.Session["IdSolicitudDireccion"].ToString(), out tempIdSolicitudDireccion);
                if (direccion.idSolicitud > 0 && tempIdSolicitudDireccion == direccion.idSolicitud)
                    respDireccion = ActualizarDireccionSolicitud(direccion);
                else
                    respDireccion = InsertarDireccionSolicitud(direccion); // Si el la direccion pertenece a una Solicitud diferente, insertar otro registro. Se debe arreglar con catalogo
            }
            else
            {
                respDireccion = InsertarDireccionSolicitud(direccion);
            }

            if (respDireccion != null) respuesta.Add(respDireccion);

            var respCliente = ActualizarClienteGestiones(cliente.Convertir<TBClientes.ActualizaClienteGestiones>());
            if (respCliente != null) respuesta.Add(respCliente);

            ResultadoOfboolean respCuentaEmergente = null;
            if (cuentaEmergente != null)
            {
                int IdSolicitudEmergente = 0;
                if (cuentaEmergente != null)
                {
                    if (context != null && context.Session["IdSolicitudEmergente"] != null)
                        int.TryParse(context.Session["IdSolicitudEmergente"].ToString(), out IdSolicitudEmergente);
                    if (IdSolicitudEmergente > 0) respCuentaEmergente = ActualizarCuentaEmergente(cuentaEmergente, idUsuario);
                    else if (cuentaEmergente.IdSolicitud > 0) respCuentaEmergente = InsertarCuentaEmergente(cuentaEmergente, idUsuario);
                    if (respCuentaEmergente != null) respuesta.Add(respCuentaEmergente);
                }
            }
            int[] OrigenGestion = { 2 };
            cliente.Origen = cliente.Origen == 0 ? 1 : cliente.Origen;
            if (OrigenGestion.Contains(cliente.Origen) && respuesta.Any(r => r.Codigo == 1))
            {
                var respGes = AgregarGestionActCliente(solicitud.IdSolicitud, idUsuario);
                if (respGes != null) respuesta.Add(respGes);
            }
        }
        catch (ValidacionExcepcion ex)
        {
            var tempRespuesta = new ResultadoOfboolean();
            tempRespuesta.Codigo = 0;
            tempRespuesta.Mensaje = ex.Message;
            tempRespuesta.ResultObject = false;
            respuesta.Add(tempRespuesta);
        }
        catch (Exception ex)
        {
            var tempRespuesta = new ResultadoOfboolean();
            tempRespuesta.Codigo = 0;
            tempRespuesta.Mensaje = "Ha ocurrido un error al procesar la petición, favor de contactar al area de sistemas";
            tempRespuesta.ResultObject = false;
            respuesta.Add(tempRespuesta);
        }
        // Se limpia la info de la sesion
        context.Session["IdSolicitudEmergente"] = null;
        context.Session["TarjetaVisa"] = null;
        context.Session["IdSolicitudDireccion"] = null;

        return respuesta;

    }
    private static ResultadoOfboolean AgregarGestionActCliente(int IdSolicitud, int IdUsuario)
    {
        ResultadoOfboolean respuesta = new ResultadoOfboolean();
        try
        {
            using (var clientesClient = new ClientesClient())
            {
                respuesta = clientesClient.AgregarGestionActCliente(IdSolicitud, IdUsuario);
            }
        }
        catch (Exception ex)
        {
            respuesta.Codigo = 0;
            respuesta.ResultObject = false;
            respuesta.Mensaje = "Ha ocurrido un error al agregar gestion de tipo actualización de datos del cliente";
        }
        return respuesta;
    }
    private static ResultadoOfboolean ActualizarClienteGestiones(TBClientes.ActualizaClienteGestiones cliente)
    {
        ResultadoOfboolean respuesta = new ResultadoOfboolean();
        try
        {
            using (var clientesClient = new ClientesClient())
            {
                respuesta = clientesClient.ActualizarClienteGestiones(cliente);
            }
        }
        catch (Exception ex)
        {
            respuesta.Codigo = 0;
            respuesta.ResultObject = false;
            respuesta.Mensaje = "Ha ocurrido un error al enviar los datos del cliente";
        }
        return respuesta;
    }
    private static ResultadoOfboolean ActualizarDatosSolicitud(int idUsuario, TBSolicitudes.ActualizarSolicitudGestiones solicitud)
    {
        ResultadoOfboolean respuesta = new ResultadoOfboolean();
        if (solicitud == null) throw new Exception("La peticion es invalida.");
        try
        {
            var context = HttpContext.Current;
            string TarjetaVisa = null;
            if (solicitud.TieneTarjetaVisa)
            {

                if (context != null && context.Session["TarjetaVisa"] != null)
                    TarjetaVisa = context.Session["TarjetaVisa"].ToString();
                if (String.IsNullOrWhiteSpace(solicitud.NumeroTarjetaVisa) && TarjetaVisa != null) solicitud.NumeroTarjetaVisa = TarjetaVisa;
                var respValTarjeta = esValidoBINTarjetaVISA(solicitud.NumeroTarjetaVisa);
                if (respValTarjeta != null && respValTarjeta.Codigo <= 0)
                {
                    throw new ValidacionExcepcion(respValTarjeta.Mensaje);
                }
            }
            using (var solicitudesClient = new SolicitudClient())
            {

                var respActualizar = solicitudesClient.ActualizarDatosSolicitudGestiones(idUsuario, solicitud);
                if (respActualizar != null)
                {
                    respuesta = respActualizar;
                }
            }
        }
        catch (ValidacionExcepcion ex)
        {
            respuesta.Codigo = 0;
            respuesta.Mensaje = ex.Message;
            respuesta.ResultObject = false;
        }
        catch (Exception ex)
        {
            respuesta.Codigo = 0;
            respuesta.ResultObject = false;
            respuesta.Mensaje = "Ha ocurrido un error al enviar los datos de la solicitud";
        }
        return respuesta;
    }
    private static ResultadoOfboolean InsertarDireccionSolicitud(SolicitudDireccion direccion)
    {
        ResultadoOfboolean respuesta = new ResultadoOfboolean();
        try
        {
            using (var solicitudesClient = new SolicitudClient())
            {
                respuesta.Codigo = solicitudesClient.InsertarDireccionSolicitud(direccion);
                respuesta.ResultObject = true;
                respuesta.Mensaje = "La actualización se realizó con éxito";
            }
        }
        catch (Exception ex)
        {
            respuesta.Codigo = 0;
            respuesta.ResultObject = false;
            respuesta.Mensaje = "Ha ocurrido un error al enviar la peticion del cliente";
        }
        return respuesta;
    }
    private static ResultadoOfboolean ActualizarDireccionSolicitud(SolicitudDireccion direccion)
    {
        ResultadoOfboolean respuesta = new ResultadoOfboolean();
        try
        {
            using (var solicitudesClient = new SolicitudClient())
            {
                respuesta.Codigo = solicitudesClient.ActualizarDireccionSolicitud(direccion);
                respuesta.ResultObject = true;
                respuesta.Mensaje = "La actualización se realizó con éxito";
            }
        }
        catch (Exception ex)
        {
            respuesta.Codigo = 0;
            respuesta.ResultObject = false;
            respuesta.Mensaje = "Ha ocurrido un error al enviar la peticion del cliente";
        }
        return respuesta;
    }
    private static ResultadoOfboolean ActualizarCuentaEmergente(TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente, int idUsuario)
    {
        ResultadoOfboolean respuesta = new ResultadoOfboolean();
        try
        {
            using (var solicitudesClient = new SolicitudClient())
            {
                respuesta = solicitudesClient.ActualizarCuentaEmergente(cuentaEmergente, idUsuario);
            }
        }
        catch (Exception ex)
        {
            respuesta.Codigo = 0;
            respuesta.ResultObject = false;
            respuesta.Mensaje = "Ha ocurrido un error al enviar la peticion de la cuenta";
        }
        return respuesta;
    }
    private static ResultadoOfboolean InsertarCuentaEmergente(TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente, int idUsuario)
    {
        ResultadoOfboolean respuesta = new ResultadoOfboolean();
        try
        {
            using (var solicitudesClient = new SolicitudClient())
            {
                respuesta = solicitudesClient.InsertarCuentaEmergente(cuentaEmergente, idUsuario);
            }
        }
        catch (Exception ex)
        {
            respuesta.Codigo = 0;
            respuesta.ResultObject = false;
            respuesta.Mensaje = "Ha ocurrido un error al enviar la peticion de la cuenta";
        }
        return respuesta;
    }
    private static ResultadoOfboolean esValidoBINTarjetaVISA(string NumeroTarjeta)
    {

        var respuesta = new ResultadoOfboolean();
        respuesta.Codigo = 1;
        respuesta.ResultObject = true;

        using (SolicitudClient ws = new SolicitudClient())
        {
            ResultadoOfboolean res = ws.ValidaTarjetaVISADomiciliacion(NumeroTarjeta);

            if (res.Codigo > 0)
            {
                respuesta.Codigo = 0;
                respuesta.ResultObject = false;
                respuesta.Mensaje = res.Mensaje;
            }
        }
        return respuesta;
    }
}
