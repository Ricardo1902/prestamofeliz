﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="frmAltaGestores.aspx.cs" Inherits="Gestiones_frmAltaGestores" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <style type="text/css">
        #Politicas
        {
            width: 735px;
            height: 500px;
        }
    </style>
    <script type="text/javascript">
        function SelectedIndexChange(ddl) {


            //if (ddl.selectedIndex == 1) {
            //    document.getElementById('PorCuenta').style.display = "block";
            //    document.getElementById('PorCliente').style.display = "none";
            //    document.getElementById('PorDNI').style.display = "none";
            //    document.getElementById('PorConvenio').style.display = "none";
            //    //document.getElementById('lblMessage').style.display = "none";
            //}
            //else if (ddl.selectedIndex == 2) {
            //    document.getElementById('PorCuenta').style.display = "none";
            //    document.getElementById('PorCliente').style.display = "block";
            //    document.getElementById('PorDNI').style.display = "none";
            //    document.getElementById('PorConvenio').style.display = "none";
            //    //document.getElementById('lblMessage').style.display = "none";
            //} else if (ddl.selectedIndex == 3) {
            //    document.getElementById('PorCuenta').style.display = "none";
            //    document.getElementById('PorCliente').style.display = "none";
            //    document.getElementById('PorDNI').style.display = "block";
            //    document.getElementById('PorConvenio').style.display = "none";
            //    //document.getElementById('lblMessage').style.display = "none";
            //} else if (ddl.selectedIndex == 4) {
            //    document.getElementById('PorCuenta').style.display = "none";
            //    document.getElementById('PorCliente').style.display = "none";
            //    document.getElementById('PorDNI').style.display = "none";
            //    document.getElementById('PorConvenio').style.display = "block";
            //    //document.getElementById('lblMessage').style.display = "none";
            //} else {
            //    document.getElementById('PorCuenta').style.display = "none";
            //    document.getElementById('PorCliente').style.display = "none";
            //    document.getElementById('PorDNI').style.display = "none";
            //    document.getElementById('PorConvenio').style.display = "none";
            //    //document.getElementById('lblMessage').style.display = "none";
            //}
        }
    </script>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading panel-heading-custom text-center">
            <h3 class="panel-title">
                <label>ALTA GESTORES</label>
            </h3>
        </div>

        <div class="panel-body" style="overflow: auto;  top: 0%; height: 100%">
            <asp:Panel runat="server">

            <div class="form-group row">
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    <asp:Label ID="Label6" Text="Buscar por:" runat="server" CssClass="Etiqueta"></asp:Label>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <label class="form-control-label small">Sucursal</label>
                    <asp:DropDownList ID="cboSucursal" runat="server" CssClass="form-control">
                        
                        
                    </asp:DropDownList>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <label class="form-control-label small">Nombre usuario</label>
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    <br />
                    <asp:Button ID="btnBuscarUsuario" runat="server" Text="Buscar" OnClick="btnBuscarUsuario_Click" CssClass="btn btn-default" />
                </div>
            </div>
            <br />
            <hr />
            <div class="form-group row">
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    <asp:Label ID="Label1" Text="Guardar Gestor como:" runat="server" CssClass="Etiqueta"></asp:Label>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <label class="form-control-label small">Tipo Gestor</label>
                    <asp:DropDownList ID="cboTipoGestor" runat="server" CssClass="form-control">

                    </asp:DropDownList>
                    <asp:Label ID="lblMessage" runat="server" Text="" Visible="false" style="color: #C94009;" class="form-control-label small"></asp:Label>
                </div>
            </div>
            <div style="display: block" id="mensaje">
                <%--<asp:Label ID="lblMessage" runat="server" Text="" Visible="false" style="color: #C94009;"></asp:Label>--%>
            </div>
            </asp:Panel>
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <blockquote>
                        <p>Usuarios</p>
                    </blockquote>
                    <div class="GridContenedor table-responsive">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBuscarUsuario" EventName="Click" />
                                <%--<asp:AsyncPostBackTrigger ControlID="btnCliente" EventName="Click" />--%>
                                <asp:PostBackTrigger ControlID="gvCuentas"/>
                            </Triggers>
                            <ContentTemplate>
                                <asp:GridView runat="server" ID="gvCuentas" DataKeyNames="IdUsuario" Width="100%"
                                    Height="25%" AutoGenerateColumns="False" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow"
                                    AlternatingRowStyle-CssClass="GridRowAlternate" CssClass="table table-hover table-bordered Grid" EmptyDataRowStyle-CssClass="GridEmptyData"
                                    GridLines="None" AllowPaging="true" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false"
                                    HeaderStyle-Wrap="false" OnPageIndexChanging="gvCuentas_PageIndexChanging" 
                                    OnRowCommand="gvCuentas_RowCommand" OnRowDataBound="gvCuentas_RowDataBound">

                                    <Columns>
                                        <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="Accion">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnAgrega" CommandName="Agregar" CommandArgument='<%# Eval("IdUsuario") %>' CssClass="btn btn-sm btn-secondary" data-toggle="tooltip" title="Agregar a gestor" runat="server">
                                                        <i class="fas fa-plus fa-lg" style="color: #1CC909;"></i>
                                                    </asp:LinkButton>
                                               
                                                    <asp:LinkButton ID="lbtnElimina" CommandName="Eliminar" CommandArgument='<%# Eval("IdUsuario") %>' CssClass="btn btn-sm btn-secondary" data-toggle="tooltip" title="Eliminar de gestor" runat="server">
                                                        <i class="fas fa-minus fa-lg" style="color: #C94009;"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="IdUsuario" DataField="IdUsuario" />
                                        <asp:BoundField HeaderText="Nombre" DataField="NombreCompleto" />                                        
                                        <asp:BoundField HeaderText="Usuario" DataField="Usuario" />
                                        <asp:BoundField HeaderText="TipoGestor" DataField="TipoUsuario" />
                                        <asp:BoundField HeaderText="Sucursal" DataField="Sucursal" />
                                        <asp:BoundField HeaderText="Estatus" DataField="Estatus" />
                                        <%--<asp:TemplateField HeaderText="Seleccionar">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbItem" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField> --%>                                       
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No se encontraron Cuentas Asignadas
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <div class="progress">
                                    Cargando...
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</asp:Content>
