﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using wsSOPF;

public partial class Gestiones_frmAltaGestores : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Page.Header.Title = "Alta Gestores";


            using (wsSOPF.UsuarioClient wsGestores = new UsuarioClient())
            {
                TBCATUsuario[] Usuarios = null;
                Usuarios = wsGestores.BuscarUsuario(4, 0, "", 0);

                gvCuentas.DataSource = Usuarios;
                gvCuentas.DataBind();
            }
            using (wsSOPF.GestionClient wsGestores = new GestionClient())
            {
                TBCatTipoGestor[] tipoGestor = null;
                tipoGestor = wsGestores.ObtenerTipoGestor();

                cboTipoGestor.DataValueField = "IdTipoGestor";
                cboTipoGestor.DataTextField = "Descripcion";
                cboTipoGestor.DataSource = tipoGestor;
                cboTipoGestor.DataBind();
            }

            TBCATSucursal[] sucursalList = null;
            using (wsSOPF.UsuarioClient wsSucursal = new UsuarioClient())
            {

                wsSOPF.TBCATSucursal sucursal = new TBCATSucursal();
                sucursal.IdSucursal = 0;
                sucursal.Accion = 0;
                sucursal.Sucursal = "";
                sucursal.IdEstatus = 0;
                sucursal.IdAfiliado = 0;
                sucursalList = wsSucursal.ObtenerSucursal(sucursal);

                if (sucursalList.Count() > 0)
                {

                    //cboSucursal.DataSource = null;//Vaciar comboBox
                    cboSucursal.DataTextField = "Sucursal";//Indicar qué propiedad se verá en la lista
                    cboSucursal.DataValueField = "IdSucursal";//Indicar qué valor tendrá cada ítem
                    cboSucursal.DataSource = sucursalList;
                    cboSucursal.DataBind();
                    cboSucursal.Items.Add(new ListItem("Todos...", "0"));
                    cboSucursal.SelectedIndex = 0;
                }
            }

       }
    }
    protected void btnBuscarUsuario_Click(object sender, EventArgs e)
    {

        using (wsSOPF.UsuarioClient wsGestores = new UsuarioClient())
        {
           
            TBCATUsuario[] Usuarios = null;
            Usuarios = wsGestores.BuscarUsuario(4, 0, txtNombre.Text, Convert.ToInt32(cboSucursal.SelectedValue));

            gvCuentas.DataSource = Usuarios;
            gvCuentas.DataBind();


        }
        lblMessage.Visible = false;
    }


    protected void gvCuentas_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        GridView gv = (GridView)sender;
        gv.PageIndex = e.NewPageIndex;
        using (wsSOPF.UsuarioClient wsGestores = new UsuarioClient())
        {
            TBCATUsuario[] Usuarios = null;
            Usuarios = wsGestores.BuscarUsuario(4, 0, txtNombre.Text, Convert.ToInt32(cboSucursal.SelectedValue));
            gvCuentas.DataSource = Usuarios;
            gvCuentas.DataBind();
        }
        lblMessage.Visible = false;
    }
    protected void gvCuentas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int tipoGestor = Convert.ToInt32(cboTipoGestor.SelectedValue);
        
            try
            {
                int IdUsuarioGes = 0;
                Session["IdUsuario"] = null;
                if (e.CommandName == "Agregar")
                {

                if (tipoGestor > 0)
                {
                    LinkButton comando = e.CommandSource as LinkButton;
                    GridViewRow registro = comando.Parent.Parent as GridViewRow;
                    int.TryParse(gvCuentas.DataKeys[registro.RowIndex]["IdUsuario"].ToString(), out IdUsuarioGes);
                    Session["IdUsuario"] = IdUsuarioGes;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    using (wsSOPF.GestionClient wsGestion = new GestionClient())
                    {

                        
                        wsGestion.GuardarGestor(tipoGestor, IdUsuarioGes, Convert.ToInt32(Session["UsuarioId"]));

                        
                        BuscarUsuarios();
                    }
                    lblMessage.Visible = false;
                }
                else
                {
                    
                    lblMessage.Text = "Seleccione un tipo de gestor valido!!";
                    lblMessage.Visible = true;
                    BuscarUsuarios();
                }

            }
            else
                if (e.CommandName == "Eliminar")
                {
                    LinkButton comando = e.CommandSource as LinkButton;
                    GridViewRow registro = comando.Parent.Parent as GridViewRow;
                    int.TryParse(gvCuentas.DataKeys[registro.RowIndex]["IdUsuario"].ToString(), out IdUsuarioGes);
                    Session["IdUsuario"] = IdUsuarioGes;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    using (wsSOPF.GestionClient wsGestion = new GestionClient())
                    {
                        
                        wsGestion.EliminarGestor(IdUsuarioGes, Convert.ToInt32(Session["UsuarioId"]));

                   
                        BuscarUsuarios();

                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('Error Inesperado!!')</script>");
            }
        
        
    }

    protected void gvCuentas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            TBCATUsuario prod = e.Row.DataItem as TBCATUsuario;
            
            if (prod.IdGestor > 0)
            {
                LinkButton img = e.Row.FindControl("lbtnAgrega") as LinkButton;
                e.Row.Cells[0].Controls.Remove(img);

            }
            else
            {
                LinkButton img = e.Row.FindControl("lbtnElimina") as LinkButton;
                e.Row.Cells[0].Controls.Remove(img);
                
            }
        }
    }

    private void BuscarUsuarios()
    {
        using (wsSOPF.UsuarioClient wsGestores = new UsuarioClient())
        {
            TBCATUsuario[] Usuarios = null;
            Usuarios = wsGestores.BuscarUsuario(4, 0, "", 0);
            gvCuentas.DataSource = Usuarios;
            gvCuentas.DataBind();
        }
    }

    
}