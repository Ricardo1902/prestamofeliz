﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="frmAsignaciones.aspx.cs" Inherits="Gestiones_frmAsignaciones" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <style type="text/css">
        #Politicas
        {
            width: 735px;
            height: 500px;
        }
    </style>
    <script type="text/javascript">
        function SelectedIndexChange(ddl) {


            if (ddl.selectedIndex == 1) {
                document.getElementById('PorCuenta').style.display = "block";
                document.getElementById('PorCliente').style.display = "none";
                document.getElementById('PorDNI').style.display = "none";
                document.getElementById('PorConvenio').style.display = "none";
                //document.getElementById('lblMessage').style.display = "none";
            }
            else if (ddl.selectedIndex == 2) {
                document.getElementById('PorCuenta').style.display = "none";
                document.getElementById('PorCliente').style.display = "block";
                document.getElementById('PorDNI').style.display = "none";
                document.getElementById('PorConvenio').style.display = "none";
                //document.getElementById('lblMessage').style.display = "none";
            } else if (ddl.selectedIndex == 3) {
                document.getElementById('PorCuenta').style.display = "none";
                document.getElementById('PorCliente').style.display = "none";
                document.getElementById('PorDNI').style.display = "block";
                document.getElementById('PorConvenio').style.display = "none";
                //document.getElementById('lblMessage').style.display = "none";
            } else if (ddl.selectedIndex == 4) {
                document.getElementById('PorCuenta').style.display = "none";
                document.getElementById('PorCliente').style.display = "none";
                document.getElementById('PorDNI').style.display = "none";
                document.getElementById('PorConvenio').style.display = "block";
                //document.getElementById('lblMessage').style.display = "none";
            } else {
                document.getElementById('PorCuenta').style.display = "none";
                document.getElementById('PorCliente').style.display = "none";
                document.getElementById('PorDNI').style.display = "none";
                document.getElementById('PorConvenio').style.display = "none";
                //document.getElementById('lblMessage').style.display = "none";
            }
        }
    </script>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading panel-heading-custom text-center">
            <h3 class="panel-title">
                <label>CARTERA ASIGNADA</label>
            </h3>
        </div>

        <div class="panel-body" style="overflow: auto;  top: 0%; height: 100%">
            <%-- Se deshabilita la busqueda (ocultan controles) --%>
            <asp:Panel runat="server">
             <div class="form-group row">
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                    <asp:Label ID="Label1" Text="BUSQUEDA POR:" runat="server" CssClass="Etiqueta"></asp:Label>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                    <asp:DropDownList ID="cboBusqueda" runat="server" onchange="SelectedIndexChange(this);" CssClass="form-control">
                        <asp:ListItem Value="-1" Text="Seleccione una opción..." />
                        <asp:ListItem Value="0" Text="Cuenta" />
                        <asp:ListItem Value="1" Text="Cliente" />
                        <asp:ListItem Value="2" Text="DNI" />
                        <asp:ListItem Value="3" Text="Convenio" />

                    </asp:DropDownList>
                </div>
            </div>

            <br />
            
            <div style="display: none" id="PorCuenta">

                <div class="form-group row">
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                        <asp:Label ID="Label2" runat="server" Text="Buscar Cuenta" CssClass="Etiqueta"></asp:Label>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <asp:TextBox ID="txtCuenta" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                        <asp:Button ID="btnBuscarCuenta" runat="server" Text="Buscar" OnClick="btnBuscarCuenta_Click" CssClass="btn btn-default" />
                    </div>
                </div>

                <br />

            </div>
            <div style="display: none" id="PorCliente">

                <div class="form-group row">
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                        <asp:Label ID="Label3" runat="server" Text="Buscar Cliente" CssClass="Etiqueta"></asp:Label>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <asp:TextBox ID="txtCliente" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                        <asp:Button ID="btnCliente" runat="server" Text="Buscar" OnClick="btnCliente_Click" CssClass="btn btn-default" />
                    </div>
                </div>

                <br />

            </div>
            <div style="display: none" id="PorDNI">

                <div class="form-group row">
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                        <asp:Label ID="Label4" runat="server" Text="Buscar DNI" CssClass="Etiqueta"></asp:Label>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <asp:TextBox ID="txtDNI" runat="server" CssClass="form-control" MaxLength ="8"></asp:TextBox>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                        <asp:Button ID="btnDNI" runat="server" Text="Buscar" OnClick="btnDNI_Click" CssClass="btn btn-default" />
                    </div>
                </div>

                <br />

            </div>
            <div style="display: none" id="PorConvenio">

                <div class="form-group row">
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                        <asp:Label ID="Label5" runat="server" Text="Buscar Convenio" CssClass="Etiqueta"></asp:Label>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <asp:TextBox ID="txtConvenio" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                        <asp:Button ID="btnConvenio" runat="server" Text="Buscar" OnClick="btnConvenio_Click" CssClass="btn btn-default" />
                    </div>
                </div>

                <br />

            </div>
            <div style="display: block" id="mensaje">
                <asp:Label ID="lblMessage" runat="server" Text="" Visible="false"></asp:Label>
            </div>
            </asp:Panel>
            <hr />
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <blockquote>
                        <p>Cuentas</p>
                    </blockquote>
                    <div class="GridContenedor table-responsive">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBuscarCuenta" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnCliente" EventName="Click" />
                                <asp:PostBackTrigger ControlID="gvCuentas"/>
                            </Triggers>
                            <ContentTemplate>
                                <asp:GridView runat="server" ID="gvCuentas" DataKeyNames="IdCuenta" Width="100%"
                                    Height="25%" AutoGenerateColumns="False" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow"
                                    AlternatingRowStyle-CssClass="GridRowAlternate" CssClass="table table-hover table-bordered Grid" EmptyDataRowStyle-CssClass="GridEmptyData"
                                    GridLines="None" AllowPaging="true" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false"
                                    HeaderStyle-Wrap="false" OnPageIndexChanging="gvCuentas_PageIndexChanging" OnRowCommand="gvCuentas_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Cuenta">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="btnDetalle" CommandName="editar" CommandArgument='<%#Eval("IdSolicitud")%>'><%#Eval("IdSolicitud") %></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Gestor" DataField="Gestor" />
                                        <asp:BoundField HeaderText="Credito" DataField="IdCuenta" />
                                        <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                        <asp:BoundField HeaderText="Sucursal" DataField="Sucursal" />
                                        <asp:BoundField HeaderText="Provincia" DataField="Ciudad" />
                                        <asp:BoundField HeaderText="Departamento" DataField="estado" />
                                        <asp:BoundField HeaderText="Fecha de Asignacion" DataField="FechaAsignacion" />
                                        <asp:BoundField HeaderText="Usuario Asigno" DataField="Usuario" />
                                        <asp:BoundField HeaderText="Tipo" DataField="Tipo" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No se encontraron Cuentas Asignadas
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <div class="progress">
                                    Cargando...
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</asp:Content>
