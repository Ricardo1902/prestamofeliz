﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Gestiones_frmAsignaciones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Page.Header.Title = "Cuentas Asignadas";

            DataResultUsp_ObtenerAsignacion[] asignaciones = null;
            using (wsSOPF.GestionClient wsGestiones = new GestionClient())
            {
                TBCatGestores[] Gestores = null;
                Gestores = wsGestiones.ObtenerGestor(3, 0, 0, Convert.ToInt32(Session["UsuarioId"]));
                if (Gestores != null && Gestores.Length > 0)
                    asignaciones = wsGestiones.ObtenerAsignacion(Convert.ToInt32(Gestores[0].IdGestor), 1, 0, "", 0,"","");

                gvCuentas.DataSource = asignaciones;
                gvCuentas.DataBind();
            }
        }
    }
    protected void btnBuscarCuenta_Click(object sender, EventArgs e)
    {
        int IdCuenta = 0;
        int.TryParse(txtCuenta.Text, out IdCuenta);

        using (wsSOPF.GestionClient wsGestion = new GestionClient())
        {
            DataResultUsp_ObtenerAsignacion[] Cuentas = null;
            TBCatGestores[] Gestores = null;
            Gestores = wsGestion.ObtenerGestor(3, 0, 0, Convert.ToInt32(Session["UsuarioId"]));
            if (Gestores != null && Gestores.Length > 0)
                Cuentas = wsGestion.ObtenerAsignacion(Convert.ToInt32(Gestores[0].IdGestor), 2, IdCuenta, "", 0,"","");

            gvCuentas.DataSource = Cuentas;
            gvCuentas.DataBind();
        }
    }

    protected void btnCliente_Click(object sender, EventArgs e)
    {
        int IdCuenta = 0;

        using (wsSOPF.GestionClient wsGestion = new GestionClient())
        {
            DataResultUsp_ObtenerAsignacion[] Cuentas = null;
            TBCatGestores[] Gestores = null;
            Gestores = wsGestion.ObtenerGestor(3, 0, 0, Convert.ToInt32(Session["UsuarioId"]));
            if (Gestores != null && Gestores.Length > 0 && txtCliente.Text.Length > 2)
                Cuentas = wsGestion.ObtenerAsignacion(Convert.ToInt32(Gestores[0].IdGestor), 3, IdCuenta, txtCliente.Text, 0, "", "");

            gvCuentas.DataSource = Cuentas;
            gvCuentas.DataBind();
        }
    }

    protected void btnDNI_Click(object sender, EventArgs e)
    {
        int IdCuenta = 0;

        using (wsSOPF.GestionClient wsGestion = new GestionClient())
        {
            DataResultUsp_ObtenerAsignacion[] Cuentas = null;
            TBCatGestores[] Gestores = null;
            Gestores = wsGestion.ObtenerGestor(3, 0, 0, Convert.ToInt32(Session["UsuarioId"]));
            if (Gestores != null && Gestores.Length > 0 && txtCliente.Text.Length > 2)
                Cuentas = wsGestion.ObtenerAsignacion(Convert.ToInt32(Gestores[0].IdGestor), 8, IdCuenta, "", 0, txtDNI.Text, "");

            gvCuentas.DataSource = Cuentas;
            gvCuentas.DataBind();
        }
    }

    protected void btnConvenio_Click(object sender, EventArgs e)
    {
        int IdCuenta = 0;

        using (wsSOPF.GestionClient wsGestion = new GestionClient())
        {
            DataResultUsp_ObtenerAsignacion[] Cuentas = null;
            TBCatGestores[] Gestores = null;
            Gestores = wsGestion.ObtenerGestor(3, 0, 0, Convert.ToInt32(Session["UsuarioId"]));
            if (Gestores != null && Gestores.Length > 0 && txtCliente.Text.Length > 2)
                Cuentas = wsGestion.ObtenerAsignacion(Convert.ToInt32(Gestores[0].IdGestor), 9, IdCuenta, "", 0,"", txtConvenio.Text);

            gvCuentas.DataSource = Cuentas;
            gvCuentas.DataBind();
        }
    }

    protected void gvCuentas_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        GridView gv = (GridView)sender;
        gv.PageIndex = e.NewPageIndex;
        DataResultUsp_ObtenerAsignacion[] asignaciones = null;
        using (wsSOPF.GestionClient wsGestiones = new GestionClient())
        {
            TBCatGestores[] Gestores = null;
            Gestores = wsGestiones.ObtenerGestor(3, 0, 0, Convert.ToInt32(Session["UsuarioId"]));
            if (Gestores != null && Gestores.Length > 0)
            {
                if (cboBusqueda.SelectedIndex == 2)
                {
                    asignaciones = wsGestiones.ObtenerAsignacion(Convert.ToInt32(Gestores[0].IdGestor), 3, 0, txtCliente.Text, 0,"","");
                }
                else
                {
                    asignaciones = wsGestiones.ObtenerAsignacion(Convert.ToInt32(Gestores[0].IdGestor), 1, 0, "", 0,"","");
                }
            }

            gvCuentas.DataSource = asignaciones;
            gvCuentas.DataBind();
        }
    }
    protected void gvCuentas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            Session["idCuenta"] = null;
            if (e.CommandName == "editar")
            {
                int idCuenta = 0;
                int idSolicitud = 0;

                LinkButton comando = e.CommandSource as LinkButton;
                GridViewRow registro = comando.Parent.Parent as GridViewRow;
                int.TryParse(gvCuentas.DataKeys[registro.RowIndex]["IdCuenta"].ToString(), out idCuenta);
                int.TryParse(e.CommandArgument.ToString(), out idSolicitud);

                Session["idCuenta"] = idCuenta;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("Gestion.aspx?idsolicitud=" + idSolicitud, false);
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('Error Inesperado!!')</script>");
        }
    }
}