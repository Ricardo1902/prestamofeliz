﻿<%@ Page Title="Gestion de Cuenta" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="frmGestiones.aspx.cs" Inherits="Gestiones_frmGestiones" ValidateRequest="false"
    EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <link href="css/frmGestiones.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />
    <link href="css/ArchivosGestion.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />
    <script src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script src="../../js/datatables/1.10.19/jquery.dataTables.min.js"></script>
    <script src="../../js/datatables/1.10.19/dataTables.bootstrap.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script src="../../js/datepicker-1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="../../js/datepicker-1.9.0/locales/bootstrap-datepicker.es.min.js"></script>
    <script src="../../js/bootstrap/js/tooltip.js"></script>
    <script src="../../js/download2.js"></script>
    <script type="text/javascript" src="../../js/utils.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
    <script src="js/ArchivosGestion.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
    <script src="js/frmGestiones.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
    <div class="page-header page-header-corp">
        <h3>Gestión</h3>
    </div>
    <div class="row row-clear">
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">Cuenta:</span>
                    <input type="text" id="txCuenta" runat="server" class="form-control" />
                    <span class="input-group-btn">
                        <input type="button" id="btnBuscar" class="btn btn-primary" value="Buscar" />
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <asp:Button ID="btnActualizarCliente" CssClass="btn btn-warning" Text="Actualizar datos del cliente"
                    OnClick="btnActualizarCliente_Click" runat="server" />
            </div>
        </div>
    </div>
    <div id="secActividades" runat="server" class="row row-clear">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Actividades importantes en la cuenta</h3>
                </div>
                <div class="panel-body">
                    <div id="gestionInfo">
                        <asp:Repeater ID="rptAcciones" runat="server">
                            <ItemTemplate>
                                <li class="list-group-item"><span
                                        class="badge"><%# (Container.ItemIndex + 1).ToString() %></span>
                                    <%# Container.DataItem.ToString() %></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-clear">
        <div class="col-md-12">
            <div id="pDatosAcredi" class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos del acreditado</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-5">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="<%=txNombre.ClientID %>" class="col-sm-4 control-label">Nombre:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txNombre" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txRfc.ClientID %>" class="col-sm-4 control-label">DNI:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txRfc" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for='<%=txDireccion.ClientID %>' class="col-sm-4 control-label">
                                    Dirección:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txDireccion" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txColonia.ClientID %>" class="col-sm-4 control-label">
                                    Urbanización:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txColonia" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txCiudad.ClientID %>" class="col-sm-4 control-label">Provincia:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txCiudad" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txEstado.ClientID %>" class="col-sm-4 control-label">Distrito:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txEstado" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="<%=txTelefono.ClientID %>" class="col-sm-4 control-label">Teléfono:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txTelefono" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txCelular.ClientID %>" class="col-sm-4 control-label">Celular:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txCelular" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txReferencia.ClientID %>" class="col-sm-4 control-label">
                                    Referencia
                                    Domiciliaria:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txReferencia" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txBanco.ClientID %>" class="col-sm-4 control-label">Banco:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txBanco" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txClabe.ClientID %>" class="col-sm-4 control-label">No. Cuenta:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txClabe" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txEstatusBan.ClientID %>" class="col-sm-4 control-label">
                                    Estatus
                                    Banco:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txEstatusBan" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-clear">
        <div class="col-md-10">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Referencias</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tReferencias" class="display" style="width: 100%"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-clear">
        <div class="col-md-12">
            <div id="pDetalleCuenta" class="panel panel-primary">
                <div class="panel-heading">
                    <div>
                        Detalle de la cuenta
                        <div style="display: inline; margin-left: 20px;">
                            <img id="imgTabAmorti" runat="server" src="../../Imagenes/Gestiones/notebook.png"
                                alt="Tabla de amortización" class="btn btn-info" style="height: 30px"
                                data-toggle="tooltip" data-placement="top" title="Tabla de amortización" />
                            <img id="imgHistoPago" runat="server" src="../../Imagenes/Gestiones/wallet.png"
                                alt="Historial de pagos" class="btn btn-info" style="height: 30px;"
                                data-toggle="tooltip" data-placement="top" title="Historial de pagos" />
                            <img id="imgExpediente" runat="server" src="../../Imagenes/Gestiones/pdf.ico"
                                alt="Expediente digital" class="btn btn-info" style="height: 30px;"
                                data-toggle="tooltip" data-placement="top" title="Expediente digital" />
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="<%=txFechaCred.ClientID %>" class="col-sm-4 control-label">
                                    Fecha
                                    Crédito:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txFechaCred" runat="server" class="form-control" value=""
                                        readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txProducto.ClientID %>" class="col-sm-4 control-label">Plazo:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txProducto" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txErogacion.ClientID %>" class="col-sm-4 control-label">Cuota:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txErogacion" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txCapital.ClientID %>" class="col-sm-4 control-label">Préstamo:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txCapital" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txBalanceT.ClientID %>" class="col-sm-4 control-label">
                                    Balance
                                    Total:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txBalanceT" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txSaldoVig.ClientID %>" class="col-sm-4 control-label">
                                    Saldo
                                    Vigente:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txSaldoVig" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txSaldoVen.ClientID %>" class="col-sm-4 control-label">
                                    Saldo
                                    Vencido:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txSaldoVen" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txSaldoCred.ClientID %>" class="col-sm-4 control-label">
                                    Saldo
                                    Crédito:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txSaldoCred" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="<%=txDiasInac.ClientID %>" class="col-sm-4 control-label">
                                    Días
                                    Inactivo:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txDiasInac" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txDiasVen.ClientID %>" class="col-sm-4 control-label">
                                    Días
                                    Vencidos:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txDiasVen" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txFlagMoraIni.ClientID%>" class="col-sm-4 control-label">
                                    Flag M. Inicio
                                    Mes:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txFlagMoraIni" runat="server" class="form-control"
                                        readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txFlagMoraAct.ClientID%>" class="col-sm-4 control-label">
                                    Flag Mora
                                    Actual:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txFlagMoraAct" runat="server" class="form-control"
                                        readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txRecibosSal.ClientID %>" class="col-sm-4 control-label">
                                    Rec.
                                    Saldados:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txRecibosSal" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txRecibosVen.ClientID %>" class="col-sm-4 control-label">
                                    Rec.
                                    Vencidos:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txRecibosVen" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txRecibosExi.ClientID %>" class="col-sm-4 control-label">
                                    Rec.
                                    Exigibles:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txRecibosExi" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txRecibosTot.ClientID %>" class="col-sm-4 control-label">
                                    Rec.
                                    Totales:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txRecibosTot" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="<%=txFechaUltimoPag.ClientID %>" class="col-sm-4 control-label">
                                    Fecha Ultimo
                                    Pago:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txFechaUltimoPag" runat="server" class="form-control"
                                        readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txFechaSigPago.ClientID %>" class="col-sm-4 control-label">
                                    Fecha Sig.
                                    Pago:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txFechaSigPago" runat="server" class="form-control"
                                        readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="<%=txGestorAsig.ClientID %>" class="col-sm-4 control-label">
                                    Gestor
                                    Asignado:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="txGestorAsig" runat="server" class="form-control" readonly />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-clear">
        <div class="col-md-12">
            <div id="pOtrasCuentas" class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Otras Cuentas</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tOtrasCuentas" class="display" style="width: 100%"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-clear">
        <div class="col-md-10">
            <div id="pArchivosGestion" class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Archivos de Gestión</h3>
                </div>
                <div class="panel-body file-manager-body">
                    <div class="row file-manager-filter">
                        <div class="col-md-4 collapse">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" id="txtBuscarArchivo" placeholder="Buscar" runat="server"
                                        class="form-control" />
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="btn-toolbar" role="toolbar" aria-label="Controles archivo">
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button id="btnAgregarArchivoGestion" type="button" class="btn btn-primary">
                                            <i class="fas fa-file-upload"></i>Subir archivo
                                        </button>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button id="btnActualizarListaArchivosGestion" type="button"
                                            class="btn btn-info">
                                            <i class="fas fa-sync-alt">Actualziar lista</i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="margin-left: 0px; margin-right: 0px;">
                            <div id="secListaArchivosGestion" class="file-manager">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div id="pAvisos" class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Avisos</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="dAvisos" class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input id="opCartero" type="checkbox" value="opCartero" />Cartero
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input id="opSms" type="checkbox" value="opSms" />SMS
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input id="opWatsApp" type="checkbox" value="opWatsApp" />WhatsApp
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input id="opMail" type="checkbox" value="opMail" />Mailing
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input id="opCircCred" type="checkbox" value="opCircCred" />Circulo de Crédito
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input id="opAvisoFis" type="checkbox" value="opAvisoFis" />Aviso Físico
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-clear">
        <div class="col-md-12">
            <div id="pNuevaGestion" class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Nueva Gestión</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-sm-3">
                                    <label for="selTipoContacto">Tipo de Contacto</label>
                                    <select id="selTipoContacto" class="form-control"></select>
                                </div>
                                <div class="form-group col-sm-3">
                                    <label for="selTipoGestor">Tipo de Gestor</label>
                                    <select id="selTipoGestor" class="form-control">
                                    </select>
                                </div>
                                <div class="form-group col-sm-3">
                                    <label for="selCampania">Campaña</label>
                                    <select id="selCampania" class="form-control"></select>
                                </div>
                                <div id="dSelCnt" class="form-group col-sm-3">
                                    <label for="selCnt">Contacto con:</label>
                                    <select id="selCnt" class="form-control"></select>
                                </div>
                                <div class="form-group col-sm-3">
                                    <label for="selResultadoGest">Resultado de Gestión</label>
                                    <select id="selResultadoGest" class="form-control">
                                        <option value="0">Seleccione una opción</option>
                                    </select>
                                </div>
                                <div id="dPromesaPag" class="form-group col-sm-3">
                                    <label for="txFechaProm">¿Fecha y monto del pago?</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                        <input type="text" id="txFechaProm" class="form-control" />
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-usd"></span></span>
                                        <input type="text" id="txMontoProm" class="form-control prom-monto"
                                            onkeypress="return ValidaMonto(this)" />
                                    </div>
                                </div>
                                <div id="dFechaDevLlam" class="form-group col-sm-3">
                                    <label for="txFechaDevolLlam">¿Fecha de la llamada?</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                        <input type="text" id="txFechaDevolLlam" class="form-control" />
                                    </div>
                                </div>
                                <div id="dNotificacion" class="form-group col-sm-12" style="padding: inherit;">
                                    <div class="form-group col-sm-4">
                                        <label for="selNotificacion">Siguiente Documento</label>
                                        <div class="input-group">
                                            <select id="selNotificacion" class="form-control"></select>
                                            <span id="spnDescargaNotificacion" class="input-group-addon"
                                                style="background-color: limegreen; cursor: pointer;"
                                                title="Descargar documento"><span
                                                    class="glyphicon glyphicon-download"></span></span>
                                            <span id="spnActualizaNotificacion" class="input-group-addon"
                                                style="background-color: lightskyblue; cursor: pointer;"
                                                title="Actualizar información"><span
                                                    class="glyphicon glyphicon-refresh"></span></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="selNotificacionPromocion">Promociones</label>
                                        <div class="input-group">
                                            <select id="selNotificacionPromocion" class="form-control"></select>
                                            <span id="spnDescargaNotificacionPromocion" class="input-group-addon"
                                                style="background-color: limegreen; cursor: pointer;"
                                                title="Descargar promoción"><span
                                                    class="glyphicon glyphicon-download"></span></span>
                                            <span id="spnActualizaNotificacionPromocion" class="input-group-addon"
                                                style="background-color: lightskyblue; cursor: pointer;"
                                                title="Actualizar información"><span
                                                    class="glyphicon glyphicon-refresh"></span></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <label for="selDestino">Destino</label>
                                        <div class="input-group">
                                            <select id="selDestino" class="form-control"></select>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-1" style="margin-top: 15px;">
                                        <div class="input-group">
                                            <span id="spnBitacora" class="input-group-addon" style="cursor: pointer;"
                                                title="Mostrar Bitácora"><span
                                                    class="glyphicon glyphicon-file"></span></span>
                                        </div>
                                    </div>
                                    <div id="dNotificacionPromocion" style="padding: inherit;">
                                        <div class="form-group col-sm-6">
                                            <label for="txDescuentoPromocion">Descuento Promoción</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">S/</span></span>
                                                <input type="text" id="txDescuentoPromocion" class="form-control"
                                                    onkeypress="return ValidaMonto(this)" />
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="txVigenciaPromocion">Vigencia Promoción (formato
                                                dd/MM/yyyy)</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                                <input type="text" id="txVigenciaPromocion" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div id="dNotificacionCitacion" style="padding: inherit;">
                                        <div class="form-group col-sm-6">
                                            <label for="txFechaCitacion">¿Fecha de la Citación? (formato
                                                dd/MM/yyyy)</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                                <input type="text" id="txFechaCitacion" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="txHoraCitacion">¿Hora de la Citación? (formato: 23:59)</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-time"></span></span>
                                                <input type="text" id="txHoraCitacion" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <label for="txComentarios">Ingrese un comentario</label>
                                    <textarea id="txComentarios" class="form-control" rows="3" cols="200"
                                        place-holder="Comentarios"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="button" id="btnAgregarGest" class="btn btn-success" value="Agregar Gestión" />
                            <input type="button" id="btnEditarGest" class="btn btn-warning" value="Editar Gestión"
                                style="display: none" />
                            <input type="button" id="btnCancelarGest" class="btn btn-info" value="Cancelar"
                                style="display: none" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-clear">
            <div class="col-md-8">
                <div class="panel panel-success gestion-contenedor">
                    <div class="panel-heading">
                        <h3 class="panel-title">Gestiones realizadas</h3>
                    </div>
                    <div class="panel-body">
                        <div id="contGestiones" class="col-sm-12">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-danger gestion-contenedor">
                    <div class="panel-heading">
                        <h3 class="panel-title">Promesas de Pago</h3>
                    </div>
                    <div class="panel-body">
                        <div id="contPromesas" class="col-sm-12">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-clear">
            <div class="col-md-6">
                <div class="panel panel-primary gestion-contenedor">
                    <div class="panel-heading">
                        <h3 class="panel-title">Devolución de Llamada</h3>
                    </div>
                    <div class="panel-body">
                        <div id="contLlamadas" class="col-sm-12">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalBitacoraNotificacion" class="modal fade modal-lg" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="alertdialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Bitácora de Notificaciones</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <table id="tBitacora" class="display" style="width: 100%"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="secSubirArchivoGestion" class="modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="alertdialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Subir archivo de gestión</h4>
                    </div>
                    <div class="modal-body">
                        <div id="dmensajes" class="row" style="display: none">
                        </div>
                        <div class="row">
                            <div class="col-md-12 collapse">
                                <div class="form-group">
                                    <label>Tipo de Archivo:</label>
                                    <select name="seltipoMimeArchivoGestion" id="seltipoMimeArchivoGestion"
                                        class="form-control">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tipo de Documento:</label>
                                    <select name="selDocumentoDestino" id="selDocumentoDestino" class="form-control">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="overflow: hidden; white-space: nowrap;">
                                    <label>Seleccionar archivo:</label>
                                    <input type="file" name="fArchivoGestion" id="fArchivoGestion"
                                        style="text-overflow: ellipsis;" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txtComentarioArchivoGestion">Comentario</label>
                                    <input type="text" name="txtComentarioArchivoGestion" class="form-control"
                                        id="txtComentarioArchivoGestion">
                                </div>
                            </div>
                        </div>
                        <div id="dContenido"></div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" id="btnSubirArchivoGestion" class="btn btn-success"
                            value="Subir Archivo" />
                        <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cancelar" />
                    </div>
                </div>
            </div>
        </div>

        <div id="secVisualizarArchivo" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="alertdialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Vista Previa</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div id="dPreview" class="col-md-12">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cerrar" />
                    </div>
                </div>
            </div>
        </div>
        <nav id="context-menu" class="context-menu">
            <ul class="context-menu__items">
                <li class="context-menu__item">
                    <a href="#" id="preview_link" class="context-menu__link" data-action="View"><i
                            class="fas fa-file-download"></i> Visualizar</a>
                    <a href="#" class="context-menu__link" data-action="Download"><i class="fas fa-file-download"></i>
                        Descargar</a>
                    <a href="#" class="context-menu__link" data-action="Delete"><i class="fa fa-times"></i> Eliminar</a>
                </li>
            </ul>
        </nav>
        <div class="modal-pr"></div>

        <script src="js/frmGestionesContextMenu.js"></script>
</asp:Content>