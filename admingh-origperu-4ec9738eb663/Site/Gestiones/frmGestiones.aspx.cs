﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ViewModels;
using wsSOPF;

public class ArchivoGoogleDrive
{
    public string NombreArchivo { get; set; }
    public string Comentario { get; set; }
    public int IdTipoArchivoGestion { get; set; }
    public int IdDocumentoDestino { get; set; }
    public string TipoArchivo { get; set; }
    public int IdServidorArchivos { get; set; }
    public int IdSolicitud { get; set; }
    public string Contenido { get; set; }
}
public partial class Gestiones_frmGestiones : System.Web.UI.Page
{
    private int IdSolicitud = 0;
    private int IdCliente = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        int idCuenta = 0;
        int idSolicitud = 0;
        DateTime valFecha;
        StringBuilder strScript = new StringBuilder();

        if (Request.QueryString["descarga"] != null)
        {
            DescargarExpediente();
            Response.End();
        }
        if (Request.QueryString["visualizarArchivo"] != null)
        {
            DescargarArchivoGestion();
            Response.End();
        }
        if (Request.QueryString["bCta"] == null && Request.QueryString["idcuenta"] != null)
        {
            int.TryParse(Request.QueryString["idcuenta"].ToString(), out idCuenta);
        }
        else if (Request.QueryString["bCta"] != null && Request.QueryString["bCta"].ToString() == "1" && Request.QueryString["idsolicitud"] != null)
        {
            try
            {
                int.TryParse(Request.QueryString["idsolicitud"].ToString(), out idSolicitud);
                using (wsSOPF.CreditoClient wsC = new wsSOPF.CreditoClient())
                {

                    var creditos = wsC.ObtenerCredito(1, idSolicitud);
                    if (creditos != null && creditos.Length > 0)
                    {
                        idCuenta = (int)creditos[0].IdCuenta;
                        Response.Redirect("frmGestiones.aspx?idcuenta=" + idCuenta.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        if (idCuenta > 0)
        {
            try
            {
                wsSOPF.DetalleCuenta detalleCuenta;

                #region LinksReportes
                imgTabAmorti.Attributes.Add("onClick", "window.open('../Creditos/frmTablaAmortizacion.aspx?idcuenta=" + idCuenta + "', '_TablaAmortizacion', 'width=1000,height=500,resizable=1,scrollbars=yes');return false;");
                imgHistoPago.Attributes.Add("onClick", "window.open('../Creditos/frmHistorialPagos.aspx?idcuenta=" + idCuenta + "', '_HistoralPagos', 'width=1000,height=500,resizable=1,scrollbars=yes');return false;");
                #endregion

                using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
                {
                    detalleCuenta = wsG.ObtenerDetalleCuenta(idCuenta);
                    if (detalleCuenta != null)
                    {
                        #region DetalleAcreditado
                        IdCliente = detalleCuenta.IdCliente;
                        IdSolicitud = (int)detalleCuenta.IdSolicitud;
                        txNombre.Value = detalleCuenta.Cliente;
                        txRfc.Value = detalleCuenta.Rfc;
                        txDireccion.Value = detalleCuenta.Direccion;
                        txColonia.Value = detalleCuenta.Colonia;
                        txCiudad.Value = detalleCuenta.Ciudad;
                        txEstado.Value = detalleCuenta.Estado;
                        txTelefono.Value = detalleCuenta.Telefono;
                        txCelular.Value = detalleCuenta.Celular;
                        txReferencia.Value = detalleCuenta.ReferenciaB.ToString();
                        txBanco.Value = detalleCuenta.Banco;
                        txClabe.Value = detalleCuenta.Clabe;
                        txEstatusBan.Value = detalleCuenta.EstatusBanco;
                        #endregion

                        strScript.AppendFormat("idSolicitud={0};", JsonConvert.SerializeObject(IdSolicitud));

                        #region DetalleCuenta
                        txFechaCred.Value = detalleCuenta.FchCredito.ToString("yyyy-MM-dd hh:mm tt");
                        txProducto.Value = detalleCuenta.Producto;
                        txErogacion.Value = detalleCuenta.Erogacion.ToString("C");
                        txCapital.Value = detalleCuenta.Capital.ToString("C");
                        txBalanceT.Value = detalleCuenta.BalanceTotal.ToString("C");
                        txSaldoVig.Value = detalleCuenta.SdoVigente.ToString("C");
                        txSaldoVen.Value = detalleCuenta.SdoVencido.ToString("C");
                        txSaldoCred.Value = detalleCuenta.Saldocredi.ToString("C");
                        txDiasInac.Value = detalleCuenta.Diasinac.ToString();
                        txDiasVen.Value = detalleCuenta.Diasven.ToString();
                        txFlagMoraIni.Value = detalleCuenta.FlagMoraInicioMes.ToString();
                        txFlagMoraAct.Value = detalleCuenta.FlagMoraActual.ToString();
                        txRecibosSal.Value = detalleCuenta.Recsal.ToString();
                        txRecibosVen.Value = detalleCuenta.Recven.ToString();
                        txRecibosExi.Value = detalleCuenta.Recexi.ToString();
                        txRecibosTot.Value = detalleCuenta.Rectot.ToString();
                        txCuenta.Value = detalleCuenta.IdSolicitud.ToString();
                        if (!string.IsNullOrEmpty(detalleCuenta.FechaUltimoPago))
                        {

                            txFechaUltimoPag.Value = DateTime.TryParseExact(detalleCuenta.FechaUltimoPago, "yyyy-MM-dd", null, DateTimeStyles.None, out valFecha) ? valFecha.ToString("yyyy-MM-dd")
                                : detalleCuenta.FechaUltimoPago;
                        }
                        valFecha = new DateTime();
                        if (!string.IsNullOrEmpty(detalleCuenta.Sigpag))
                        {

                            txFechaSigPago.Value = DateTime.TryParseExact(detalleCuenta.Sigpag, "yyyy/MM/dd", null, DateTimeStyles.None, out valFecha) ? valFecha.ToString("yyyy-MM-dd")
                                : detalleCuenta.Sigpag;
                        }
                        txGestorAsig.Value = detalleCuenta.GestorAsigando;
                        imgExpediente.Attributes.Add("onClick", "window.open('frmGestiones.aspx?descarga=1&idSolicitud=" + detalleCuenta.IdSolicitud.ToString() + "', '_ExpdienteDigital', 'width=1000,height=500,resizable=1,scrollbars=yes');return false;");
                        #endregion

                        #region Referencias
                        //wsSOPF.Referencias[] resultReferencias = wsG.ObtenerReferencias(idCuenta);
                        using (wsSOPF.ClientesClient clientesClient = new ClientesClient())
                        {
                            wsSOPF.ClientesReferencia[] resultReferencias = clientesClient.ObtenerReferenciasCliente(detalleCuenta.IdCliente);
                            if (resultReferencias != null)
                            {
                                var referencias = resultReferencias.Select(r => new
                                {

                                    Referencia = (r.Nombres + " " + r.ApellidoPaterno + " " + r.ApellidoMaterno).Replace("  ", " ").Trim().ToUpper(),
                                    Lada = r.Lada,
                                    Telefono = (!String.IsNullOrWhiteSpace(r.Lada) ? "(" + r.Lada + ")" + r.Telefono : "" + r.Telefono).Trim(),
                                    TelefonoCelular = (!String.IsNullOrWhiteSpace(r.LadaCelular) ? "(" + r.LadaCelular + ")" + r.TelefonoCelular : "" + r.TelefonoCelular).Trim(),
                                    Parentesco = r.Parentesco.ToUpper()
                                }).ToList();
                                strScript.AppendFormat("referencias={0};", JsonConvert.SerializeObject(referencias));
                            }
                        }

                        #endregion

                        strScript.AppendFormat("idsolicitud={0};", JsonConvert.SerializeObject(detalleCuenta.IdSolicitud));

                        #region OtrasCuentas
                        wsSOPF.TBCreditos[] resultCreditos = wsG.ObtenerOtrasCuentas(idCuenta);
                        if (resultCreditos != null)
                        {
                            var otrasCuentas = resultCreditos.Select(c => new
                            {
                                Cuenta = (int)c.IdSolicitud,
                                Credito = (int)c.IdCuenta,
                                Capital = c.Capital.ToString("C"),
                                FechaCredito = c.FchCredito.ToString("yyyy-MM-dd hh:mm tt"),
                                Interes = c.Interes.ToString("C"),
                                IvaInteres = c.IvaIntere.ToString("C"),
                                Erogacion = c.Erogacion.ToString("C"),
                                SaldoVencido = c.SdoVencido.ToString("C"),
                                SaldoVigente = c.SdoPorApl.ToString("C"),
                                c.Banco,
                                c.Clabe
                            }).ToList();
                            strScript.AppendFormat("otrasCuentas={0};", JsonConvert.SerializeObject(otrasCuentas));
                        }
                        #endregion

                        #region Gestiones
                        strScript.AppendFormat("gestiones={0};", JsonConvert.SerializeObject(ObtenerGestionesVM(idCuenta)));
                        #endregion

                        #region PromesasPago
                        strScript.AppendFormat("promesas={0};", JsonConvert.SerializeObject(ObtenerPromesasVM(idCuenta)));
                        #endregion

                        #region devolverLlamadas
                        strScript.AppendFormat("llamadas={0};", JsonConvert.SerializeObject(ObtenerLlamadasVM(idCuenta)));
                        #endregion

                        #region Avisos
                        wsSOPF.Avisos respAvisos = wsG.ObtenerAvisos(idCuenta);
                        if (respAvisos != null && respAvisos.IdCuenta > 0)
                        {
                            var avisos = new
                            {
                                Id = respAvisos.IdAvisos,
                                respAvisos.IdCuenta,
                                respAvisos.Carteo,
                                respAvisos.sms,
                                respAvisos.watsapp,
                                respAvisos.mailing,
                                respAvisos.circuloCredito,
                                respAvisos.fisico
                            };
                            strScript.AppendFormat("avisos={0};", JsonConvert.SerializeObject(avisos));
                        }
                        #endregion
                    }

                    #region TipoContacto
                    List<wsSOPF.CatTipoContaco> tipoContactos = new List<wsSOPF.CatTipoContaco>();
                    wsSOPF.CatTipoContaco seleccionTipoContacto = new wsSOPF.CatTipoContaco() { id = -1, contacto = "Seleccione una opción" };
                    var resp = wsG.ObtenerTipoContactos(new TipoContactosRequest { IdTipoGestion = 1 });
                    if (resp.Resultado != null && resp.Resultado.CatTipoContactos != null && resp.Resultado.CatTipoContactos.Length > 0)
                    {
                        tipoContactos = resp.Resultado.CatTipoContactos.ToList();
                        tipoContactos.Insert(0, seleccionTipoContacto);
                    }
                    else
                    {
                        tipoContactos.Add(seleccionTipoContacto);
                    }
                    strScript.AppendFormat("tipoContactos={0};", JsonConvert.SerializeObject(tipoContactos));
                    #endregion

                    #region TipoGetorContacto
                    var respTipoGestor = wsG.ObtenerTipoGestorContactos();
                    List<wsSOPF.TipoGestorContacto> tipoGestorContactos = new List<wsSOPF.TipoGestorContacto>();
                    wsSOPF.TipoGestorContacto seleccioneTipoGestorContacto = new wsSOPF.TipoGestorContacto { IdTipoGestorContacto = 0, Contacto = "Seleccione una opción" };
                    if (respTipoGestor.Resultado != null && respTipoGestor.Resultado.TipoGestorContactos != null && respTipoGestor.Resultado.TipoGestorContactos.Length > 0)
                    {
                        tipoGestorContactos = respTipoGestor.Resultado.TipoGestorContactos.ToList();
                        tipoGestorContactos.Insert(0, seleccioneTipoGestorContacto);
                    }
                    else
                    {
                        tipoGestorContactos.Add(seleccioneTipoGestorContacto);
                    }
                    strScript.AppendFormat("tipoGestor={0};", JsonConvert.SerializeObject(tipoGestorContactos));
                    #endregion

                    #region Campanias
                    var respCampanias = wsG.ObtenerCampanas(1, 0);
                    if (respCampanias != null)
                    {
                        strScript.AppendFormat("campanias={0};", JsonConvert.SerializeObject(respCampanias));
                    }
                    #endregion

                    #region Notificaciones
                    List<wsSOPF.TB_CATNotificaciones> notificaciones = new List<wsSOPF.TB_CATNotificaciones>();
                    wsSOPF.TB_CATNotificaciones seleccionNotificaciones = new wsSOPF.TB_CATNotificaciones() { IdNotificacion = -1, Notificacion = "Seleccione una opción" };
                    var respNotificaciones = wsG.ObtenerNotificaciones();
                    if (respNotificaciones.Resultado != null && respNotificaciones.Resultado.Notificaciones != null && respNotificaciones.Resultado.Notificaciones.Length > 0)
                    {
                        notificaciones = respNotificaciones.Resultado.Notificaciones.ToList();
                        notificaciones.Insert(0, seleccionNotificaciones);
                    }
                    else
                    {
                        notificaciones.Add(seleccionNotificaciones);
                    }
                    strScript.AppendFormat("notificaciones={0};", JsonConvert.SerializeObject(notificaciones));
                    #endregion

                    #region NotificacionPromociones
                    List<wsSOPF.TB_CATNotificaciones> notificacionPromociones = new List<wsSOPF.TB_CATNotificaciones>();
                    wsSOPF.TB_CATNotificaciones seleccionNotificacionesPromociones = new wsSOPF.TB_CATNotificaciones() { IdNotificacion = -1, Notificacion = "Seleccione una opción" };
                    var respNotificacionesPromociones = wsG.ObtenerNotificacionPromociones();
                    if (respNotificacionesPromociones.Resultado != null && respNotificacionesPromociones.Resultado.Notificaciones != null && respNotificacionesPromociones.Resultado.Notificaciones.Length > 0)
                    {
                        notificacionPromociones = respNotificacionesPromociones.Resultado.Notificaciones.ToList();
                        notificacionPromociones.Insert(0, seleccionNotificaciones);
                    }
                    else
                    {
                        notificacionPromociones.Add(seleccionNotificaciones);
                    }
                    strScript.AppendFormat("notificacionPromociones={0};", JsonConvert.SerializeObject(notificacionPromociones));
                    #endregion

                    #region Destinos
                    //TODO: BACK ORDER BY IDDESTINO
                    List<wsSOPF.TB_CATDestinoNotificacion> destinos = new List<wsSOPF.TB_CATDestinoNotificacion>();
                    wsSOPF.TB_CATDestinoNotificacion seleccionDestinos = new wsSOPF.TB_CATDestinoNotificacion() { IdDestinoNotificacion = -1, DestinoNotificacion = "Seleccione una opción" };
                    var respDestinos = wsG.ObtenerDestinos();
                    if (respDestinos.Resultado != null && respDestinos.Resultado.Destinos != null && respDestinos.Resultado.Destinos.Length > 0)
                    {
                        destinos = respDestinos.Resultado.Destinos.ToList();
                        destinos.Insert(0, seleccionDestinos);
                    }
                    else
                    {
                        destinos.Add(seleccionDestinos);
                    }
                    strScript.AppendFormat("destinos={0};", JsonConvert.SerializeObject(destinos));
                    # endregion

                    #region ResultadoGestionNotificacion
                    var ResultadoGestionNotificacionLista = wsG.ObtenerResultadoGestionNotificacion();
                    if (ResultadoGestionNotificacionLista.Resultado != null && !string.IsNullOrEmpty(ResultadoGestionNotificacionLista.Resultado.Parametro.valor))
                    {
                        var tagIds = new List<string>(ResultadoGestionNotificacionLista.Resultado.Parametro.valor.Split(',').Select(s => s));
                        strScript.AppendFormat("respNotificacion={0};", JsonConvert.SerializeObject(tagIds));
                    }
                    # endregion
                }
                #region ContactoCon
                using (wsSOPF.CatalogoClient wsC = new wsSOPF.CatalogoClient())
                {
                    var respContacto = wsC.ObtenerCatCNT(1, "");
                    if (respContacto != null)
                    {
                        strScript.AppendFormat("contactos={0};", JsonConvert.SerializeObject(respContacto));
                    }
                }

                #endregion
                #region ActividadesCuenta
                List<string> acciones = new List<string>();
                using (wsSOPF.GestionClient wsG = new GestionClient())
                {
                    var resp = wsG.ObtenerAsignacionAccion(new ObtenerAsignacionAccionRequest
                    {
                        IdCuenta = idCuenta
                    });
                    if (resp != null && resp.Resultado != null && resp.Resultado.AsignacionAccion != null && resp.Resultado.AsignacionAccion.Length > 0)
                    {
                        acciones = resp.Resultado.AsignacionAccion
                            .Select(a => a.DescipcionActividad + (string.IsNullOrEmpty(a.DescripcionFallo) ? "" : " (" + a.DescripcionFallo + ")"))
                            .Distinct()
                            .ToList();
                        rptAcciones.DataSource = acciones;
                        rptAcciones.DataBind();
                    }
                    else
                    {
                        secActividades.Visible = false;
                    }
                }
                #endregion

                #region ArchivoGestion
                var tiposArchivo = ObtenerTiposArchivoGestion();
                if (tiposArchivo != null)
                    strScript.AppendFormat("tiposArchivo={0};", JsonConvert.SerializeObject(tiposArchivo));

                var servidoresArchivo = ObtenerCatServidoresArchivos();
                if (tiposArchivo != null)
                    strScript.AppendFormat("servidoresArchivo={0};", JsonConvert.SerializeObject(servidoresArchivo));

                var archivosGestion = ObtenerArchivosGestionSolicitud(IdSolicitud);
                if (archivosGestion != null)
                    strScript.AppendFormat("archivosGestion={0};", JsonConvert.SerializeObject(archivosGestion));
                #endregion

                #region NotificacionesGestion

                var documentosDestino = ObtenerDocumentosDestino();
                if (documentosDestino != null)
                    strScript.AppendFormat("documentosDestino={0};", JsonConvert.SerializeObject(documentosDestino));

                #endregion
            }
            catch (Exception ex)
            {
                //TODO: Implementar Log de error.
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
        }
    }

    #region PrivateMethods
    private static object ObtenerGestionesVM(int idCuenta)
    {
        object gestiones = null;
        try
        {
            if (idCuenta <= 0) return null;
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {

                wsSOPF.DataResultUsp_ObtenerGestion[] resultGestiones = wsG.ObtenerGestion(idCuenta);
                string[] alertas = new string[] { "No contestan", "Posible Fraude", "Defuncion", "Fallecido", "Ilocalizable",
                            "Negativa de Pago", "Promesa pago incumplida", "Negativa de Pago", "Promesa de Pago incumplida", "Fraude", "Defunción" };
                Dictionary<string, string> extraInfo = new Dictionary<string, string>();
                extraInfo.Add("1", "glyphicon glyphicon-headphones");
                extraInfo.Add("2", "glyphicon glyphicon-user");
                extraInfo.Add("3", "glyphicon glyphicon-alert");
                string valExtraInfo = string.Empty;
                if (resultGestiones != null)
                {
                    gestiones = resultGestiones
                        .Select(g => new
                        {
                            Id = g.IdGestion,
                            Fecha = g.FchGestion.ToString("yyyy-MM-dd hh:mm tt"),
                            Gestor = g.Nombre,
                            ResultadoGestion = g.Descripcion,
                            Contacto = g.CNT,
                            Campania = g.Campana,
                            g.Comentario,
                            g.IdResultadoGestion,
                            Alerta = alertas.Contains(g.Descripcion),
                            Tipo = extraInfo.TryGetValue(g.IdTipoGestion.ToString(), out valExtraInfo) ? valExtraInfo : string.Empty
                        })
                        .OrderByDescending(g => g.Id)
                        .ToList();
                }
            }
        }
        catch (Exception ex)
        {
            //TODO: Implementar Log de errores
        }
        return gestiones;
    }

    private static object ObtenerPromesasVM(int idCuenta)
    {
        object promesas = null;
        try
        {
            if (idCuenta <= 0) return null;
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                wsSOPF.PromesaPago[] resultPromesas = wsG.ObtenerPromesaPago(idCuenta);
                if (resultPromesas != null)
                {
                    promesas = resultPromesas
                        .Select(p => new
                        {
                            FchaPromesa = p.FchPromesa.ToString("yyyy-MM-dd"),
                            Monto = p.MontoPromesa.ToString("C"),
                            p.TipoCobro
                        })
                        .OrderByDescending(p => p.FchaPromesa)
                        .ToList();
                }
            }
        }
        catch (Exception ex)
        {
            //TODO: Implementar Log de errores
        }
        return promesas;
    }

    private static object ObtenerLlamadasVM(int idCuenta)
    {
        object promesas = null;
        try
        {
            if (idCuenta <= 0) return null;
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                wsSOPF.TBDevolverLlamada[] resultLlammadas = wsG.ObtenerTBDevolverLlamada(idCuenta);
                if (resultLlammadas != null)
                {
                    promesas = resultLlammadas
                        .Select(p => new
                        {

                            FchDevLlamada = p.FchDevLlamada.ToString("yyyy-MM-dd"),
                            p.ResultadoGestion,
                            p.Comentario

                        })
                        .OrderByDescending(p => p.FchDevLlamada)
                        .ToList();
                }
            }
        }
        catch (Exception ex)
        {
            //TODO: Implementar Log de errores
        }
        return promesas;
    }
    private static List<TipoArchivoGestionVM> ObtenerTiposArchivoGestion()
    {
        var respuesta = new List<TipoArchivoGestionVM>();
        using (GestionClient gestionClient = new GestionClient())
        {
            var respuestaTipos = gestionClient.ObtenerTiposArchivoGestion();
            if (respuestaTipos != null && respuestaTipos.Length > 0)
            {
                respuesta = respuestaTipos.ToList();
            }
        }
        return respuesta;
    }
    private static List<ServidorArchivosVM> ObtenerCatServidoresArchivos()
    {
        var respuesta = new List<ServidorArchivosVM>();
        using (CatalogoClient catalogoclient = new CatalogoClient())
        {
            var respuestaTipos = catalogoclient.ObtenerServidoresArchivos();
            if (respuestaTipos != null && respuestaTipos.Length > 0)
            {
                respuesta = respuestaTipos.ToList();
            }
        }
        return respuesta;
    }

    private static List<DocumentoDestinoVM> ObtenerDocumentosDestino()
    {
        List<DocumentoDestinoVM> documentos = new List<DocumentoDestinoVM>();
        using (GestionClient gestionClient = new GestionClient())
        {
            var respuestaDocumentos = gestionClient.ObtenerDocumentosDestino();
            if (respuestaDocumentos != null && respuestaDocumentos.Resultado != null && respuestaDocumentos.Resultado.DocumentosDestino != null && respuestaDocumentos.Resultado.DocumentosDestino.Length > 0)
                documentos = respuestaDocumentos.Resultado.DocumentosDestino.ToList();
        }
        return documentos;
    }


    private void DescargarExpediente()
    {
        wsSOPF.TBSolicitudes.Expediente _EX = new TBSolicitudes.Expediente();
        TBSolicitudes.ExpedienteB64 _EXB64 = new TBSolicitudes.ExpedienteB64();
        int idSolicitud = 0;
        if (Request.QueryString["idSolicitud"] != null)
        {
            int.TryParse(Request.QueryString["idSolicitud"].ToString(), out idSolicitud);
        }
        if (idSolicitud <= 0) Response.End();
        using (SolicitudClient _Cliente = new SolicitudClient())
        {
            _EX.IDSolicitud = idSolicitud;
            _EXB64 = _Cliente.DescargaExpediente(_EX);
            if (_EXB64.IDResultado != -1 && !string.IsNullOrEmpty(_EXB64.B64))
            {
                try
                {
                    byte[] arrayByte = Convert.FromBase64String(_EXB64.B64);
                    if (arrayByte != null && arrayByte.Length > 0)
                    {
                        Response.ContentType = "Application/pdf";
                        Response.BinaryWrite(arrayByte);
                        Response.Flush();
                    }
                    else
                    {
                        Response.Write("No fue posible encontrar el expediente.");
                    }
                }
                catch
                {
                    Response.Write("Ocurrió un error mientras se intentaba obtener la ubicación del archivo.");
                }
            }
        }
    }
    private void DescargarArchivoGestion()
    {
        string idArchivoGestion = null;
        if (Request.QueryString["idArchivoGestion"] != null)
        {
            idArchivoGestion = Request.QueryString["idArchivoGestion"].ToString();
        }
        try
        {
            if (String.IsNullOrEmpty(idArchivoGestion)) Response.End();
            using (GestionClient gestionClient = new GestionClient())
            {
                var respuestaArchivo = gestionClient.DescargarArchivoGoogleDrive(idArchivoGestion);
                if (respuestaArchivo != null && respuestaArchivo.ResultObject != null)
                {
                    var tempBase64String = respuestaArchivo.ResultObject.Contenido != null ? Convert.ToBase64String(respuestaArchivo.ResultObject.Contenido) : null;

                    byte[] arrayByte = Convert.FromBase64String(tempBase64String);
                    if (arrayByte != null && arrayByte.Length > 0)
                    {
                        Response.ContentType = "Application/pdf";
                        Response.BinaryWrite(arrayByte);
                        Response.Flush();
                    }
                    else
                    {
                        Response.Write("No fue posible encontrar el expediente.");
                    }
                }
            }
        }
        catch
        {
            Response.Write("Ocurrió un error mientras se intentaba obtener la ubicación del archivo.");
        }
    }
    #endregion

    #region WebMethods
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerTipoContacto()
    {
        wsSOPF.CatTipoContaco[] tipoContactos = null;
        try
        {
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var resp = wsG.ObtenerTipoContactos(new TipoContactosRequest { IdTipoGestion = 1 });
                if (resp.Resultado != null && resp.Resultado.CatTipoContactos != null)
                {
                    tipoContactos = resp.Resultado.CatTipoContactos;
                }
            }
        }
        catch (Exception ex)
        {
        }
        return tipoContactos;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerCatalogoResultados(int opcionGestor)
    {
        List<wsSOPF.CatResultadoGestion> resultados = new List<wsSOPF.CatResultadoGestion>();
        wsSOPF.CatResultadoGestion selccioneResultado = new wsSOPF.CatResultadoGestion { IdResultado = 0, Descripcion = "Seleccione una opción" };
        try
        {
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                int opcion = 0;
                if (opcionGestor == 1 || opcionGestor == 4) opcion = 1;
                else if (opcionGestor == 2) opcion = 3;
                else if (opcionGestor == 3) opcion = 6;
                if (opcionGestor > 0)
                {
                    var resulResultados = wsG.ObtenerCatalogoResultadoAccion(opcion, "");
                    if (resulResultados != null && resulResultados.Length > 0)
                    {
                        resultados = resulResultados.ToList();
                    }
                    else
                    {
                        resultados.Add(selccioneResultado);
                    }
                }
                else
                {
                    resultados.Add(selccioneResultado);
                }
            }
        }
        catch (Exception ex)
        {
        }
        return resultados;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GuardarGestion(Gestion gestion)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        int idUsuario = 0;
        int idCuenta = 0;
        object gestiones = null;
        object promesas = null;
        object llamadas = null;
        int[] IdsPromesasPago = new int[] { 1, 2, 28, 102 };
        int[] IdsDevolverLlam = new int[] { 29, 89 };
        int[] IdsNotificacion = new int[] { 119 };
        try
        {
            #region Validaciones
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }
            if (HttpContext.Current.Request.QueryString["idcuenta"] != null)
            {
                int.TryParse(HttpContext.Current.Request.QueryString["idcuenta"].ToString(), out idCuenta);
            }
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            if (gestion.IdCampania <= 0)
            {
                throw new ValidacionExcepcion("Debe seleccionar una campaña.");
            }
            if (idCuenta <= 0)
            {
                throw new ValidacionExcepcion("El número de cuenta no es válida.");
            }
            if (IdsPromesasPago.Contains(gestion.IdResultadoGestion))
            {
                if ((gestion.FechaPromesa ?? DateTime.Now.Date) < DateTime.Now.Date)
                {
                    throw new ValidacionExcepcion("La fecha de promesa de pago no es válida");
                }
                if ((gestion.MontoPromesa ?? 0) <= 0) throw new ValidacionExcepcion("El monto de la promesa no es váida.");
                //TODO: Se deja comentado por si quieren implementarlo para Perú.
                //using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
                //{
                //    var respValidaProm = wsG.ValidaPromesa(new wsSOPF.ValidaPromesaRequest
                //    {
                //        IdCuenta = idCuenta,
                //        FechaPromesa = gestion.FechaPromesa ?? DateTime.Now,
                //        MontoPromesa = gestion.MontoPromesa ?? 0,
                //        NivelValidacion = wsSOPF.ValidaPromesaRequest.Validacion.Intermedio
                //    });
                //    if (respValidaProm != null && respValidaProm.Resultado != null && respValidaProm.Resultado.Mensajes != null && respValidaProm.Resultado.Mensajes.Length > 0)
                //    {
                //        throw new ValidacionExcepcion(string.Join(",", respValidaProm.Resultado.Mensajes));
                //    }
                //}
            }
            if (IdsDevolverLlam.Contains(gestion.IdResultadoGestion))
            {
                if ((gestion.FechaLlamada ?? DateTime.Now.Date) <= DateTime.Now.Date) throw new ValidacionExcepcion("La fecha de devolución de llamada no es válida.");
            }
            if (string.IsNullOrEmpty((gestion.Comentario ?? "").Trim())) throw new ValidacionExcepcion("Ingrese un comentario para guardar la gestión.");
            #endregion

            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var resulGestor = wsG.ObtenerGestor(4, 0, 0, idUsuario);
                if (resulGestor != null && resulGestor.Length > 0)
                {
                    #region Guardado de la gestión
                    var gestor = resulGestor[0];
                    wsSOPF.Gestion gestionDB = new wsSOPF.Gestion
                    {
                        IdCuenta = idCuenta,
                        IdGestor = gestor.IdGestor,
                        Comentario = gestion.Comentario.Trim(),
                        IdResultadoGestion = gestion.IdResultadoGestion,
                        idCNT = 0
                    };
                    if (gestion.IdResultadoGestion == 15)
                    {
                        gestionDB.idCNT = gestion.IdContacto;
                    }
                    if (gestion.IdCampania > 0) gestionDB.idCampana = gestion.IdCampania;

                    //Actualizacion
                    if (gestion.Id > 0)
                    {
                        gestionDB.IdGestion = gestion.Id;
                        mensaje = wsG.ActualizaGestion(gestionDB);
                        if (string.IsNullOrEmpty(mensaje))
                        {
                            mensaje = "Actualización realizada correctamente!";
                        }
                        else mensaje = mensaje.Replace("0-", "").Replace("1-", "");
                        gestiones = ObtenerGestionesVM(idCuenta);
                    }
                    else
                    {

                        int idGestion = wsG.insertarGestion(gestionDB);
                        mensaje = "La gestión se ha guardado correctamente!";
                        #endregion

                        #region ResultadosGestion
                        if (idGestion > 0)
                        {
                            #region Guardado de la promesa
                            int[] IdsTipoCobro = new int[] { 1, 2 };
                            if (IdsPromesasPago.Contains(gestionDB.IdResultadoGestion))
                            {
                                wsSOPF.PromesaPago promesa = new wsSOPF.PromesaPago();
                                promesa.IdCuenta = idCuenta;
                                promesa.IdGestion = idGestion;
                                promesa.FchPromesa = gestion.FechaPromesa ?? DateTime.Now.Date;
                                promesa.MontoPromesa = gestion.MontoPromesa ?? 0;
                                if (IdsTipoCobro.Contains(gestionDB.IdResultadoGestion))
                                {
                                    promesa.idTipoCobro = gestionDB.IdResultadoGestion;
                                }
                                wsG.insertarPromesaPago(promesa);

                                promesas = ObtenerPromesasVM(idCuenta);
                            }
                            #endregion

                            #region DevovlerLlamada
                            if (IdsDevolverLlam.Contains(gestionDB.IdResultadoGestion))
                            {
                                wsG.InsertarDevolverLlamada(idGestion, idCuenta, gestion.FechaLlamada ?? DateTime.Now.Date, 0, gestor.IdGestor);
                                llamadas = ObtenerLlamadasVM(idCuenta);
                            }
                            #endregion
                        }
                        else
                        {
                            mensaje += "\n\nSin embargo la promesa o devolución de llamada no fueron guardadas, contacte a Sistemas.";
                        }

                        gestiones = ObtenerGestionesVM(idCuenta);
                        #endregion
                    }
                }
                else
                {
                    error = true;
                    mensaje = "No fue posible registrar la gestión, si el problema continua contacte a soporte técnico.";
                }
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            //TODO: Implementar Log de error.
            error = true;
            mensaje = "Ocurrió un error desconocido al tratar de guardar la gestion. Contacte a Sistemas.";
        }

        return new { Error = error, Mensaje = mensaje, Url = url, Gestiones = gestiones, Promesas = promesas, Llamadas = llamadas };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static void GuardarAvisos(int idCuenta, string[] actAvisos)
    {
        try
        {
            if (idCuenta > 0)
            {
                using (var wsG = new GestionClient())
                {
                    #region GuardarAvisos
                    Avisos avisos = new Avisos();
                    if (actAvisos != null && actAvisos.Length > 0)
                    {
                        avisos.Carteo = actAvisos.Contains("opCartero") ? 1 : 0;
                        avisos.sms = actAvisos.Contains("opSms") ? 1 : 0;
                        avisos.watsapp = actAvisos.Contains("opWatsApp") ? 1 : 0;
                        avisos.mailing = actAvisos.Contains("opMail") ? 1 : 0;
                        avisos.fisico = actAvisos.Contains("opAvisoFis") ? 1 : 0;
                        avisos.circuloCredito = actAvisos.Contains("opCircCred") ? 1 : 0;
                    }
                    wsG.InsertarAvisos(idCuenta, avisos.Carteo, avisos.sms, avisos.watsapp, avisos.mailing, avisos.circuloCredito, avisos.fisico);
                }
            }
        }
        catch (Exception)
        {
        }
        #endregion
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerGestion(int id)
    {
        object gestion = null;
        try
        {
            if (id <= 0) return gestion;
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var respGestion = wsG.ObtenerGestionPordIdGestion(new wsSOPF.ObtenerGestionRequest { Accion = 4, IdGestion = id });
                if (respGestion != null && respGestion.Resultado != null && respGestion.Resultado.Gestion != null)
                {
                    var respGes = respGestion.Resultado.Gestion;
                    gestion = new
                    {
                        Id = respGes.IdGestion,
                        IdTipoContacto = respGes.IdTipoGestion,
                        IdCampania = respGes.idCampana,
                        respGes.Comentario
                    };
                }
            }
        }
        catch (Exception ex)
        {
        }
        return gestion;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerEstadoNotificacion(int idsolicitud)
    {
        object Notificacion = null;
        try
        {
            if (idsolicitud <= 0) return Notificacion;
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var respNotif = wsG.ObtenerEstadoNotificacion(new wsSOPF.ObtenerEstadoNotificacionRequest { Accion = "GET_ESTADO_NOTIFICACION_POR_SOLICITUD", IdSolicitud = idsolicitud });
                if (respNotif != null && respNotif.Resultado != null && respNotif.Resultado.Notificacion != null)
                {
                    var respNoti = respNotif.Resultado.Notificacion;
                    Notificacion = new
                    {
                        respNoti.IdNotificacion,
                        respNoti.Notificacion,
                        respNoti.Clave,
                        respNoti.Orden,
                        respNoti.Destino
                    };
                }
            }
        }
        catch (Exception ex)
        {
            //TODO: Implementar log de error.
        }
        return Notificacion;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GenerarNotificacion(NotificacionRequest notificacion)
    {
        string url = string.Empty;
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }
        if (idUsuario <= 0)
        {
            url = @"..\..\frmLogin.aspx";
            throw new ValidacionExcepcion("La clave del usuario no es válida.");
        }
        else
        {
            notificacion.IdUsuario = idUsuario;
        }
        string nombreDocumento = string.Empty;
        if (notificacion.IdSolicitud > 0 && !string.IsNullOrEmpty(notificacion.Clave) && notificacion.IdDestino > 0)
        {
            if (notificacion.Clave == "CP" && (string.IsNullOrEmpty(notificacion.FechaCitacion) || string.IsNullOrEmpty(notificacion.HoraCitacion)))
                return new { Error = true, Mensaje = "La citación debe de tener fecha y hora.", Url = url };
            else if (notificacion.Clave == "PRM" && (notificacion.DescuentoDeuda == null || string.IsNullOrEmpty(notificacion.FechaVigencia)))
                return new { Error = true, Mensaje = "La promoción debe de tener un valor en descuento y en vigencia.", Url = url };
            nombreDocumento = Notificacion.ObtenerNombreArchivo(notificacion.Clave);
            GenerarNotificacionResultado notificacionResultado = Notificacion.GenerarNotificacion(notificacion, nombreDocumento);
            if (notificacionResultado.Resultado != null && notificacionResultado.Resultado.Length > 0)
            {
                var resultado = new
                {
                    Error = false,
                    Mensaje = "La notificación se ha generado exitosamente.",
                    Url = url
                ,
                    Tipo = "application/pdf",
                    Longitud = notificacionResultado.Resultado.Length.ToString()
                ,
                    NombreArchivo = nombreDocumento,
                    Contenido = Convert.ToBase64String(notificacionResultado.Resultado)
                };

                return resultado;
            }
            return new { Error = true, Mensaje = "La notificación no pudo generarse. " + notificacionResultado.MensajeOperacion, Url = url };
        }
        return new { Error = true, Mensaje = "Ha ocurrido un error en los datos de petición de generación de notificación.", Url = url };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerBitacoraNotificacion(int idsolicitud)
    {
        object Bitacora = null;
        try
        {
            if (idsolicitud <= 0) return Bitacora;
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var respNotif = wsG.ObtenerBitacoraNotificacion(new wsSOPF.ObtenerBitacoraNotificacionRequest { Accion = "GET_BITACORA_NOTIFICACION_POR_SOLICITUD", IdSolicitud = idsolicitud });
                if (respNotif != null && respNotif.Resultado != null && respNotif.Resultado.Bitacora != null)
                {
                    Bitacora = respNotif.Resultado.Bitacora.Select(s => new
                    {
                        s.Notificacion,
                        s.Destino,
                        s.Accion,
                        s.Usuario,
                        Fecha = s.Fecha.ToString("dd/MM/yyyy HH:mm:ss")
                    });
                }
                else
                {
                    Bitacora = new { Error = true, MensajeOperacion = respNotif.MensajeOperacion ?? "Ha ocurrido un error." };
                }
            }
        }
        catch (Exception ex)
        {
            //TODO: Implementar log de error.
        }
        return Bitacora;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static List<ArchivoGestionVM> ObtenerArchivosGestionSolicitud(int idSolicitud)
    {
        var respuesta = new List<ArchivoGestionVM>();
        using (GestionClient gestionClient = new GestionClient())
        {
            var respuestaArchivos = gestionClient.ObtenerArchivoGestionPorSolicitud(idSolicitud);
            if (respuestaArchivos != null && respuestaArchivos.Length > 0)
            {
                respuesta = respuestaArchivos.ToList();
                respuesta.Reverse(); //Se ordenan por mas reciente a mas viejo
            }
        }
        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerDatosNotificacion(int idsolicitud, int idnotificacion, int iddestino)
    {
        object Notificacion = null;
        try
        {
            if (idsolicitud <= 0)
            {
                Notificacion = new { Error = true, MensajeOperacion = "Ha ocurrido un error." };
                return Notificacion;
            }
            using (wsSOPF.GestionClient wsG = new wsSOPF.GestionClient())
            {
                var respNotif = wsG.ObtenerEstadoNotificacion(new wsSOPF.ObtenerEstadoNotificacionRequest { Accion = "GET_DATOS_NOTIFICACION_POR_SOLICITUD", IdSolicitud = idsolicitud, IdNotificacion = idnotificacion, IdDestino = iddestino });
                if (respNotif != null && respNotif.Resultado != null && respNotif.Resultado.Notificacion != null)
                {
                    var s = respNotif.Resultado.Notificacion;
                    Notificacion = new
                    {
                        s.IdNotificacion,
                        s.Notificacion,
                        s.NombreArchivo,
                        s.Clave,
                        s.Orden,
                        FechaCitacion = s.FechaHoraCitacion.GetValueOrDefault().ToString("dd/MM/yyyy"),
                        HoraCitacion = s.FechaHoraCitacion.GetValueOrDefault().ToString("HH:mm"),
                        DescuentoDeuda = s.DescuentoDeuda.GetValueOrDefault(),
                        FechaVigencia = s.FechaVigencia.GetValueOrDefault().ToString("dd/MM/yyyy"),
                        s.Destino
                    };
                }
                else
                {
                    Notificacion = new { Error = true, MensajeOperacion = respNotif.MensajeOperacion ?? "Ha ocurrido un error." };
                }
            }
        }
        catch (Exception ex)
        {
            //TODO: Implementar log de error.
        }
        return Notificacion;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ActualizarNotificacion(NotificacionRequest notificacion)
    {
        string url = string.Empty;
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }
        if (idUsuario <= 0)
        {
            url = @"..\..\frmLogin.aspx";
            throw new ValidacionExcepcion("La clave del usuario no es válida.");
        }
        else
        {
            notificacion.IdUsuario = idUsuario;
        }
        string nombreDocumento = string.Empty;
        if (notificacion.IdSolicitud > 0 && !string.IsNullOrEmpty(notificacion.Clave) && notificacion.IdDestino > 0)
        {
            if (notificacion.Clave == "CP" && (string.IsNullOrEmpty(notificacion.FechaCitacion) || string.IsNullOrEmpty(notificacion.HoraCitacion)))
                return new { Error = true, Mensaje = "La citación debe de tener fecha y hora.", Url = url };
            else if (notificacion.Clave == "PRM" && (notificacion.DescuentoDeuda == null || string.IsNullOrEmpty(notificacion.FechaVigencia)))
                return new { Error = true, Mensaje = "La promoción debe de tener un valor en descuento y en vigencia.", Url = url };
            nombreDocumento = Notificacion.ObtenerNombreArchivo(notificacion.Clave);
            GenerarNotificacionResultado notificacionResultado = Notificacion.GenerarNotificacion(notificacion, nombreDocumento, true);
            if (notificacionResultado.Resultado != null && notificacionResultado.Resultado.Length > 0)
            {
                var resultado = new
                {
                    Error = false,
                    Mensaje = "La notificación se ha generado exitosamente.",
                    Url = url,
                    Tipo = "application/pdf",
                    Longitud = notificacionResultado.Resultado.Length.ToString(),
                    NombreArchivo = nombreDocumento,
                    Contenido = Convert.ToBase64String(notificacionResultado.Resultado)
                };

                return resultado;
            }
            return new { Error = true, Mensaje = "La notificación no pudo generarse. " + notificacionResultado.MensajeOperacion, Url = url };
        }
        return new { Error = true, Mensaje = "Ha ocurrido un error en los datos de petición de generación de notificación o la notificación es inválida.", Url = url };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object SubirArchivoGestion(ArchivoGoogleDrive archivo)
    {
        var respuesta = new ResultadoOfboolean();
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0) throw new ValidacionExcepcion("La clave del usuario no es válida");
            if (archivo == null) throw new ValidacionExcepcion("La petición es inválida. El conetenido del archivo no es correcto");
            if (String.IsNullOrEmpty(archivo.NombreArchivo)) throw new ValidacionExcepcion("El nombre del archivo no es válido");
            if (String.IsNullOrEmpty(archivo.Contenido)) throw new ValidacionExcepcion("El contenido del archivo no es válido");
            if (archivo.IdSolicitud <= 0) throw new ValidacionExcepcion("La clave de la solicitud no es válida");
            if (archivo.IdDocumentoDestino <= 0) throw new ValidacionExcepcion("La clave del documento destino no es válida");
            if (archivo.IdTipoArchivoGestion <= 0) throw new ValidacionExcepcion("No se pudo detectar el tipo de archivo");


            byte[] contenidoBytes = Convert.FromBase64String(archivo.Contenido);
            if (contenidoBytes == null) throw new ValidacionExcepcion("No fue posible procesar el contenido del archivo. Favor de verificarlo");

            using (GestionClient gestionesClient = new GestionClient())
            {
                var respSubirArchivo = gestionesClient.SubirArchivoGoogleDrive(archivo.IdSolicitud, idUsuario, new CargaArchivoGoogleDrive
                {
                    NombreArchivo = archivo.NombreArchivo,
                    Comentario = archivo.Comentario,
                    IdTipoArchivo = archivo.IdTipoArchivoGestion,
                    IdDocumentoDestino = archivo.IdDocumentoDestino,
                    TipoArchivo = archivo.TipoArchivo,
                    IdServidorArchivos = archivo.IdServidorArchivos,
                    Contenido = contenidoBytes
                });
                if (respSubirArchivo != null)
                {
                    respuesta = respSubirArchivo;
                }
            }
        }
        catch (ValidacionExcepcion ex)
        {
            respuesta.Codigo = 0;
            respuesta.Mensaje = ex.Message;
            respuesta.ResultObject = false;
        }
        catch (Exception)
        {
            respuesta.Codigo = 0;
            respuesta.Mensaje = "Ha ocurrido un error al procesar la petición";
            respuesta.ResultObject = false;
        }

        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object DescargarArchivo(string idArchivo)
    {
        object respuesta = null;
        try
        {
            if (String.IsNullOrEmpty(idArchivo)) throw new ValidacionExcepcion("La petición es inválida");
            using (GestionClient gestionClient = new GestionClient())
            {
                var respuestaArchivo = gestionClient.DescargarArchivoGoogleDrive(idArchivo);
                if (respuestaArchivo != null && respuestaArchivo.ResultObject != null)
                {
                    var tempBase64String = respuestaArchivo.ResultObject.Contenido != null ? Convert.ToBase64String(respuestaArchivo.ResultObject.Contenido) : null;
                    respuesta = new
                    {
                        NombreArchivo = respuestaArchivo.ResultObject.NombreArchivo,
                        Contenido = tempBase64String,
                        Extension = respuestaArchivo.ResultObject.Extension,
                        Mime = respuestaArchivo.ResultObject.Mime
                    };
                }
            }

        }
        catch (Exception ex)
        {
        }

        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object EliminarArchivoGestion(int idArchivoGestion, int idSolicitud)
    {
        //Delete logico en BDD
        var respuesta = new ResultadoOfboolean();
        try
        {

            if (idArchivoGestion <= 0) throw new Exception("La clave del archivo es inválida");
            if (idSolicitud <= 0) throw new Exception("La clave de la solicitud es inválida");

            using (GestionClient gestionesClient = new GestionClient())
            {
                var respSubirArchivo = gestionesClient.EliminarArchivoGestion(idArchivoGestion, idSolicitud);
                if (respSubirArchivo != null)
                {
                    respuesta = respSubirArchivo;
                }
            }
        }
        catch (Exception)
        {
            respuesta.Codigo = 0;
            respuesta.Mensaje = "Ha ocurrido un error al procesar la petición";
            respuesta.ResultObject = false;
        }
        return respuesta;
    }
    #endregion

    protected void btnActualizarCliente_Click(object sender, EventArgs e)
    {
        if (IdSolicitud > 0 && IdCliente > 0)
        {
            Response.Redirect("frmActualizarCliente.aspx?IdSolicitud=" + IdSolicitud.ToString() + "&IdCliente=" + IdCliente.ToString());
        }
    }
}
