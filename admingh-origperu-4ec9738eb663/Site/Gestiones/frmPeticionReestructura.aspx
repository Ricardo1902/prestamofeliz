﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmPeticionReestructura.aspx.cs" EnableEventValidation="true" Inherits="Site_Gestiones_frmPeticionReestructura" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolKit" %>

<asp:Content ID="head" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../jQuery/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../jQuery/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="../../jQuery/jquery-ui.theme.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../jQuery/external/jquery/jquery.js"></script>
    <script type="text/javascript" src="../../jQuery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../js/parsley.min.js"></script>
    <script type="text/javascript">        
        var solicitudOperacion;
        $(function () {
            $("#divAccordion").accordion({ header: "h3", collapsible: true, heightStyle: "content" });
            $('#aspnetForm').parsley();
        });
    </script>
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="form-group">
        <div class="form-group row">
            <div class="form-group col-md-6">
                <h3>Petición de Reestructura</h3>
            </div>
        </div>
    </div>
        <div id="divAccordion" class="clearfix" style="height: auto;">
            <h3>BÚSQUEDA DE CLIENTES PARA PETICIONES</h3>
            <asp:Panel ID="pnlBusqueda" runat="server" Visible="true"><!--BÚSQUEDA DE CLIENTES-->
                <div class="panel-body">
                    <asp:UpdateProgress AssociatedUpdatePanelID="upBusquedaCliente" DisplayAfter="100" runat="server">
                        <ProgressTemplate>
                            <div id="loader-background"></div>
                            <div id="loader-content"></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upBusquedaCliente" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <div id="divClienteBusqueda" style="margin-top: 10px;">
                                <div class="form-group col-md-4">
                                    <label for="txtBuscarSolicitud" class="form-control-label small">Solicitud</label>
                                    <asp:TextBox runat="server" ID="txtBuscarSolicitud" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="txtBuscarCliente" class="form-control-label small">Cliente o DNI</label>
                                    <asp:TextBox runat="server" ID="txtBuscarCliente" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="cboBucket" class="form-control-label small">Bucket</label>
                                    <asp:DropDownList ID="cboBucket" runat="server" CssClass="form-control" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="txtBuscarRazon" class="form-control-label small">Razon Social Empresa</label>
                                    <asp:TextBox runat="server" ID="txtBuscarRazon" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="txtBuscarDependencia" class="form-control-label small">Dependencia</label>
                                    <asp:TextBox runat="server" ID="txtBuscarDependencia" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="txtBuscarDireccion" class="form-control-label small">Dirección</label>
                                    <asp:TextBox runat="server" ID="txtBuscarDireccion" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-2 pull-right">
                                    <asp:Button ID="btnBuscarCliente" runat="server" Text="Buscar peticiones" CssClass="btn btn-sm btn-primary" OnClick="btnBuscarCliente_Click" ValidationGroup="SinValidar" />
                                </div>
                                <asp:Panel ID="pnlError_FltBusqueda" class="form-group col-sm-12" Visible="false" runat="server">
                                    <div class="alert alert-danger" style="padding: 5px 15px 5px 15px; margin: 10px 0px;">
                                        <asp:Label ID="lblError_FltBusqueda" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>
                                <div id="ResultadoClientes" class="form-group col-md-12">
                                    <div class="GridContenedor">
                                        <asp:GridView ID="gvResultadoClientes" runat="server" Width="100%" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-striped table-hover table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                            PageSize="5" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false" EmptyDataText="No se encontraron resultados de clientes"
                                            DataKeyNames="IdSolicitud" Style="overflow-x: auto;" OnPageIndexChanging="gvResultadoClientes_PageIndexChanging" OnRowCommand="gvResultadoClientes_RowCommand">
                                            <PagerStyle CssClass="pagination-ty warning" />
                                            <Columns>
                                                <asp:BoundField HeaderText="Solicitud" DataField="IdSolicitud" />
                                                <asp:BoundField HeaderText="NombreCliente" DataField="NombreCliente" />
                                                <asp:BoundField HeaderText="FechaNacimiento" DataField="FechaNacimiento" DataFormatString="{0:dd/MM/yyyy}" />
                                                <asp:BoundField HeaderText="Vencido" DataField="SaldoVencido" DataFormatString="S/ {0:0.00}" />
                                                <asp:BoundField HeaderText="Mora" DataField="DiasMora" />
                                                <asp:BoundField HeaderText="Bucket" DataField="Bucket" />
                                                <asp:BoundField HeaderText="Estatus" DataField="EstatusPeticion" />
                                                <asp:TemplateField HeaderText="Acción">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lnkCrear" CssClass='<%# GetColorBotonPeticionText(Eval("EstatusPeticion")) %>' Text='<%# GetLabelBotonPeticionText(Eval("EstatusPeticion")) %>'
                                                            CommandName='<%# GetCommandNameBotonPeticion(Eval("EstatusPeticion")) %>' CommandArgument='<%#Eval("IdSolicitud") + ";" + Eval("IdPeticionReestructura")%>' Style="color: white;" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="" DataField="IdPeticionReestructura" ShowHeader="false" Visible="false" />
                                            </Columns>
                                            <EmptyDataTemplate>
                                                No se encontraron resultados de clientes habilitados para reestructurar.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div><!--ResultadoClientes-->
                            </div><!--divBusquedaCliente-->
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div><!--panel-body-->
            </asp:Panel><!--BÚSQUEDA DE CLIENTES-->
            <asp:Panel ID="pnlDatos" runat="server" Enabled="false"><!--HISTORIAL DE PAGOS-->
                <asp:Panel ID="pnlHistorialPagos" runat="server">
                    <h3><a href="#">HISTORIAL DE PAGOS</a></h3>
                    <div>
                        <div class="panel-body">
                            <asp:UpdatePanel ID="upHistorialPagos" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <div class="izq">
                                        <div style="height: auto; width: auto; overflow-x: auto;">
                                            <asp:GridView ID="gvHistorialPagos" runat="server" Width="100%" AutoGenerateColumns="False" HeaderStyle-CssClass="info" RowStyle-CssClass="GridRow"
                                                AlternatingRowStyle-CssClass="GridRowAlternate" CssClass="table table-bordered table-responsive table-condensed table-hover"
                                                EmptyDataRowStyle-CssClass="GridEmptyData" RowStyle-Wrap="false" HeaderStyle-Wrap="false" OnRowDataBound="gvHistorialPagos_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Canal de Pago" DataField="CanalPagoDesc" />
                                                    <asp:BoundField HeaderText="Fecha Pago" DataField="FechaPago.Value" DataFormatString="{0:d}" />
                                                    <asp:BoundField HeaderText="Monto del Pago" DataField="MontoCobrado" DataFormatString="{0:c2}" />
                                                    <asp:BoundField HeaderText="Aplicado" DataField="MontoAplicado" DataFormatString="{0:c2}" />
                                                    <asp:BoundField HeaderText="Por Aplicar" DataField="MontoPorAplicar" DataFormatString="{0:c2}" />
                                                    <asp:BoundField HeaderText="Estatus" DataField="Estatus.EstatusDesc" />
                                                </Columns>
                                                <EmptyDataRowStyle CssClass="warning small" />
                                                <EmptyDataTemplate>
                                                    <i class="fas fa-exclamation-circle" style="color: #888888;"></i>La cuenta no tiene pagos registrados
                                                </EmptyDataTemplate>
                                            </asp:GridView><!--gvHistorialPagos-->
                                        </div><!--div style height auto-->
                                    </div><!--izq-->
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </asp:Panel><!--HISTORIAL DE PAGOS-->
                <asp:Panel ID="pnlTablaRecibos" runat="server"><!--TABLA DE RECIBOS-->
                    <h3><a href="#">TABLA DE CUOTAS</a></h3>
                    <div>
                        <div class="panel-body">
                            <asp:UpdateProgress AssociatedUpdatePanelID="upTablaRecibos" DisplayAfter="100" runat="server">
                                <ProgressTemplate>
                                    <div id="loader-background"></div>
                                    <div id="loader-content"></div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:UpdatePanel ID="upTablaRecibos" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <div class="izq">
                                        <div class="row">
                                            <div class="form-group col-md-3 col-md-offset-9">
                                                <label for="txtFechaPeticion" class="form-control-label small">Fecha Peticion</label>
                                                <div>
                                                    <asp:TextBox ID="txtFechaPeticion" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label id="lbCboMotivo" runat="server" for="cboMotivo" class="form-control-label small">Motivo de Reestructura</label>
                                                <asp:DropDownList ID="cboMotivo" runat="server" CssClass="form-control" />
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label id="lbCboTipoReestructura" runat="server" for="cboTipoReestructura" class="form-control-label small">Tipo Reestructura</label>
                                                <asp:DropDownList ID="cboTipoReestructura" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboTipoReestructura_SelectedIndexChanged" AutoPostBack="true" />
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label id="lbCboNuevoPlazo" runat="server" for="cboNuevoPlazo" class="form-control-label small">Nuevo Plazo</label>
                                                <asp:DropDownList ID="cboNuevoPlazo" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboNuevoPlazo_SelectedIndexChanged" AutoPostBack="true" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-print-3">
                                            <label class="small">N° Solicitud</label>
                                            <pre><asp:Label ID="lblIdSolicitud_Liquidacion" runat="server" /></pre>
                                        </div>
                                        <div class="col-sm-3 col-print-3">
                                            <label class="small">DNI</label>
                                            <pre><asp:Label ID="lblDNI_Liquidacion" runat="server"></asp:Label></pre>
                                        </div>
                                        <div class="col-sm-6 col-print-6">
                                            <label class="small">Cliente</label>
                                            <pre><asp:Label ID="lblCliente_Liquidacion" runat="server"></asp:Label></pre>
                                        </div>
                                        <div class="col-sm-4 col-print-4">
                                            <label class="small">Monto Credito</label>
                                            <pre><asp:Label ID="lblMontoCredito_Liquidacion" runat="server"></asp:Label></pre>
                                        </div>
                                        <div class="col-sm-4 col-print-4">
                                            <label class="small">Cuota</label>
                                            <pre><asp:Label ID="lblCuota_Liquidacion" runat="server"></asp:Label></pre>
                                        </div>
                                        <div class="col-sm-4 col-print-4">
                                            <label class="small">Total a Liquidar Original</label>
                                            <pre><asp:Label ID="lblTotalLiquidar_Liquidacion" runat="server"></asp:Label></pre>
                                        </div>
                                        <div class="col-sm-4 col-print-4">
                                            <label class="small">Fecha Credito</label>
                                            <pre><asp:Label ID="lblFechaCredito_Liquidacion" runat="server"></asp:Label></pre>
                                        </div>
                                        <div class="col-sm-4 col-print-4">
                                            <label class="small">Fecha 1er. Pago</label>
                                            <pre><asp:Label ID="lblFechaPrimerPago_Liquidacion" runat="server"></asp:Label></pre>
                                        </div>
                                        <div class="col-sm-4 col-print-4">
                                            <label class="small">Fecha Liquidacion</label>
                                            <pre class="visible-print-block"><asp:Label ID="lblFechaLiquidacion_Liquidacion" runat="server"></asp:Label></pre>
                                            <div class="input-group hidden-print">
                                                <asp:TextBox ID="txtFechaLiquidar_Liquidacion" AutoPostBack="true" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10" Enabled="false" runat="server" />
                                                <span class="input-group-addon">
                                                    <asp:LinkButton ID="btnFechaLiquidacion" Visible="false" runat="server">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                                    </asp:LinkButton>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-print-12">
                                            <hr />
                                            <asp:PlaceHolder ID="phHeaderControles" runat="server"></asp:PlaceHolder>
                                            <div style="height: auto; width: auto; overflow-x: auto;">
                                                <asp:GridView ID="gvRecibos_Liquidacion" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="small"
                                                    CssClass="table table-bordered table-striped table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="N° Cuota" DataField="Recibo" />
                                                        <asp:BoundField HeaderText="Fecha" DataField="FechaRecibo" DataFormatString="{0:d}" />
                                                        <asp:BoundField HeaderText="Capital" DataField="LiquidaCapital" DataFormatString="{0:c2}" />
                                                        <asp:BoundField HeaderText="Interes" DataField="LiquidaInteres" />
                                                        <asp:BoundField HeaderText="IGV" DataField="LiquidaIGV" />
                                                        <asp:BoundField HeaderText="Seguro" DataField="LiquidaSeguro" />
                                                        <asp:BoundField HeaderText="GAT" DataField="LiquidaGAT" />
                                                        <asp:BoundField HeaderText="Pagar" DataField="LiquidaTotal" DataFormatString="{0:c2}" />
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        Hubo un problema al cargar el calculo de la liquidacion
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                                <asp:GridView ID="gvRecibos_Liquidacion_Totales" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="small"
                                                    CssClass="table table-bordered table-striped table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData"
                                                    OnRowDataBound="gvRecibos_Liquidacion_Totales_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Cuotas" DataField="TotalRecibos" />
                                                        <asp:BoundField HeaderText="Concepto" DataField="Concepto" />
                                                        <asp:BoundField HeaderText="TotalLiquidaCapital" DataField="TotalLiquidaCapital" DataFormatString="{0:c2}" />
                                                        <asp:BoundField HeaderText="TotalInteres" DataField="TotalLiquidaInteres" />
                                                        <asp:BoundField HeaderText="TotalIGV" DataField="TotalLiquidaIGV" />
                                                        <asp:BoundField HeaderText="TotalSeguro" DataField="TotalLiquidaSeguro" />
                                                        <asp:BoundField HeaderText="TotalGAT" DataField="TotalLiquidaGAT" />
                                                        <asp:BoundField HeaderText="TotalPagar" DataField="TotalLiquidaTotal" DataFormatString="{0:c2}" />
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        Hubo un problema al cargar el calculo de la liquidacion
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                                <h4>Simulación de Resultado</h4>
                                                <hr />
                                                <asp:GridView ID="gvRecibosNuevos" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="small"
                                                    CssClass="table table-bordered table-striped table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="N° Cuota" DataField="NoRecibo" />
                                                        <asp:BoundField HeaderText="Fecha" DataField="FechaRecibo" DataFormatString="{0:d}" />
                                                        <asp:BoundField HeaderText="Capital Inicial" DataField="CapitalInicial" DataFormatString="{0:c2}" />
                                                        <asp:BoundField HeaderText="Capital" DataField="Capital" DataFormatString="{0:c2}" />
                                                        <asp:BoundField HeaderText="Interes" DataField="Interes" DataFormatString="{0:c2}" />
                                                        <asp:BoundField HeaderText="IGV" DataField="IGV" DataFormatString="{0:c2}" />
                                                        <asp:BoundField HeaderText="Seguro" DataField="Seguro" DataFormatString="{0:c2}" />
                                                        <asp:BoundField HeaderText="GAT" DataField="GAT" DataFormatString="{0:c2}" />
                                                        <asp:BoundField HeaderText="Saldo Capital" DataField="SaldoCapital" DataFormatString="{0:c2}" />
                                                        <asp:BoundField HeaderText="Cuota" DataField="Cuota" DataFormatString="{0:c2}" />
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        Hubo un problema al cargar la simulación del crédito nuevo.
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div><!--izq-->
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </asp:Panel><!--TABLA DE RECIBOS-->
                <asp:Panel ID="pnlDatosComentarios" runat="server"><!--VALIDAR Y CONTINUAR-->
                    <h3><a href="#">VALIDAR Y CONTINUAR</a></h3>
                    <div>
                        <div class="panel-body">
                            <asp:UpdatePanel ID="upDatosComentarios" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <div class="form-group col-md-12">
                                        <label for="txtComentariosCapturista" class="form-control-label small">Comentarios Capturista</label>
                                        <div>
                                            <asp:TextBox ID="txtComentariosCapturista" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="1000" ToolTip="Máximo 1000 caracteres." />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upGuardar" class="form-group row inline" runat="server">
                                <ContentTemplate>
                                    <div class="col-sm-12">
                                        <asp:Button runat="server" ID="btnValidar" Text="Validar y continuar" CssClass="btn btn-sm btn-primary" OnClick="btnGuardarClick" style="margin-right: 55px;" />
                                        <asp:ValidationSummary runat="server" ID="vsError" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos:" ForeColor="Red" />
                                        <asp:ValidationSummary runat="server" ID="vsDatosSolicitante" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos del solicitante:" ForeColor="Red" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                        <asp:ValidationSummary runat="server" ID="vsDatosLaborales" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos laborales:" ForeColor="Red" ValidationGroup="DatosLaborales" CssClass="small" Display="Dynamic" />
                                        <asp:ValidationSummary runat="server" ID="vsDatosCobro" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos de cobro del solicitante:" ForeColor="Red" ValidationGroup="DatosCobro" CssClass="small" Display="Dynamic" />
                                    </div>
                                    <asp:Panel ID="pnlError" class="col-sm-12" Visible="false" runat="server">
                                        <div class="alert alert-danger" style="padding: 5px 15px 5px 15px; margin: 10px 0px;">
                                            <asp:Label ID="lblError" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress AssociatedUpdatePanelID="upGuardar" DisplayAfter="100" runat="server">
                                <ProgressTemplate>
                                    <div class="animationload">
                                        <div class="osahanloading"></div>
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </div>
                </asp:Panel><!--VALIDAR Y CONTINUAR-->
                <br />
            </asp:Panel><!--pnlDatos-->
            <asp:Panel ID="pnlAdmin" runat="server">
                <asp:UpdatePanel ID="upAdmin" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <label for="txtComentariosAutorizador" class="form-control-label small">Comentarios Autorizador</label>
                        <asp:TextBox ID="txtComentariosAutorizador" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="1000" ToolTip="Máximo 1000 caracteres." />
                        <div class="btn-group">
                            <asp:Button runat="server" ID="btnAutorizar" Text="Autorizar" CssClass="btn btn-sm btn-success" OnClientClick="return confirm('¿Está seguro(a) que desea realizar esta acción?');" OnClick="btnAutorizarClick" style="margin: 5px;" />
                            <asp:Button runat="server" ID="btnRechazar" Text="Rechazar" CssClass="btn btn-sm btn-danger" OnClientClick="return confirm('¿Está seguro(a) que desea realizar esta acción?');" OnClick="btnRechazarClick" style="margin: 5px;" /><br /><br />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress AssociatedUpdatePanelID="upAdmin" DisplayAfter="100" runat="server">
                    <ProgressTemplate>
                        <div class="animationload">
                            <div class="osahanloading"></div>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </asp:Panel>
        </div>
    <script type="text/javascript" src="/js/siteCommon.js"></script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
    </script>
</asp:Content>
