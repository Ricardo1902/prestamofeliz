﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Web;

public partial class Site_Gestiones_frmPeticionReestructura : System.Web.UI.Page
{
    #region Constantes
    private string STR_PORCENTAJES_DIFERENTES = ". EL VALOR NO COINCIDE CON TOTAL";
    private string STR_VALORES_DIFERENTES = "VALOR ACTUAL: ";
    private string[] ACCIONES = { "Crear", "Analizar", "Preautorizar", "Consultar", "Regularizar" };
    private int[] EstatusPeticionFinalizada = { 5, 9, 10 };
    #endregion

    #region Propiedades
    private int SolicitudOperacion
    {
        get { return ViewState[UniqueID + "_SolicitudOperacion"] != null ? (int)ViewState[UniqueID + "_SolicitudOperacion"] : 0; }
        set { ViewState[UniqueID + "_SolicitudOperacion"] = value; }
    }

    private int PeticionReestructuraOperacion
    {
        get { return ViewState[UniqueID + "_PeticionReestructuraOperacion"] != null ? (int)ViewState[UniqueID + "_PeticionReestructuraOperacion"] : 0; }
        set { ViewState[UniqueID + "_PeticionReestructuraOperacion"] = value; }
    }

    private int ValorMaximoPorcentaje
    {
        get { return ViewState[UniqueID + "_ValorMaximoPorcentaje"] != null ? (int)ViewState[UniqueID + "_ValorMaximoPorcentaje"] : 0; }
        set { ViewState[UniqueID + "_ValorMaximoPorcentaje"] = value; }
    }

    private string[] EstatusReciboCondonables = new string[] { "VENCIDO", "ACTUAL" };

    private decimal TotalReestructurar
    {
        get { return ViewState[UniqueID + "_TotalReestructurar"] != null ? (decimal)ViewState[UniqueID + "_TotalReestructurar"] : 0; }
        set { ViewState[UniqueID + "_TotalReestructurar"] = value; }
    }
    #endregion

    public class Lada
    {
        public Lada(string _Codigo, string _Estado)
        {
            Codigo = _Codigo;
            Estado = _Estado;
        }

        public string Codigo { get; set; }
        public string Estado { get; set; }
    }
    List<Lada> lstLada = new List<Lada>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Combo[] Bucket_PE;
            Combo[] MotivoPeticionReestructura_PE;
            Combo[] TipoReestructura_PE;
            Combo[] PlazosHabilitados;

            if (Request.QueryString["idsolicitud"] != null)
            {
                int idSolicitud = 0;
                int.TryParse(Request.QueryString["idsolicitud"].ToString(), out idSolicitud);
                if (idSolicitud > 0) txtBuscarSolicitud.Text = idSolicitud.ToString();
            }

            using (wsSOPF.GestionClient ws = new GestionClient())
            {
                Bucket_PE = ws.ObtenerCombo_Bucket_PE();
                MotivoPeticionReestructura_PE = ws.ObtenerCombo_MotivoPeticionReestructura_PE();
                TipoReestructura_PE = ws.ObtenerCombo_TipoReestructura_PE();
                PlazosHabilitados = ws.ObtenerCombo_PlazosReestructura(new ObtenerComboPlazosReestructuraRequest()
                {
                    Accion = "CONSULTAR_PLAZOS_PARA_REESTRUCTURA",
                    IdSolicitud = 0
                });
            }
            if (Bucket_PE.Length > 0)
            {
                cboBucket.DataSource = Bucket_PE;
                cboBucket.DataValueField = "Valor";
                cboBucket.DataTextField = "Descripcion";
                cboBucket.DataBind();
                cboBucket.Items.Insert(0, new ListItem("TODOS", "0"));
            }
            if (MotivoPeticionReestructura_PE.Length > 0)
            {
                cboMotivo.DataSource = MotivoPeticionReestructura_PE;
                cboMotivo.DataValueField = "Valor";
                cboMotivo.DataTextField = "Descripcion";
                cboMotivo.DataBind();
                cboMotivo.SelectedIndex = 0;
                //lbCboMotivo.Style["display"] = "none";
                //cboMotivo.Style["display"] = "none";
            }
            if (TipoReestructura_PE.Length > 0)
            {
                cboTipoReestructura.DataSource = TipoReestructura_PE;
                cboTipoReestructura.DataValueField = "Valor";
                cboTipoReestructura.DataTextField = "Descripcion";
                cboTipoReestructura.DataBind();
                cboTipoReestructura.SelectedIndex = 0;
                //lbCboTipoReestructura.Style["display"] = "none";
                //cboTipoReestructura.Style["display"] = "none";
            }
            if (PlazosHabilitados.Length > 0)
            {
                cboNuevoPlazo.DataSource = PlazosHabilitados;
                cboNuevoPlazo.DataValueField = "Valor";
                cboNuevoPlazo.DataTextField = "Descripcion";
                cboNuevoPlazo.DataBind();
                cboNuevoPlazo.SelectedIndex = 0;
            }
            pnlDatos.Enabled = false;
            ValidarPermisoAdmin();
        }// !PostBack
        else
        {
            if (ViewState["RecibosLiquidacion"] != null)
            {
                ConstruirControlesGvRecibosLiquidacion(sender, e);
            }
        }
    } // LOAD

    #region BusquedaClientes
    protected void btnBuscarCliente_Click(object sender, EventArgs e)
    {
        pnlError_FltBusqueda.Visible = false;

        try
        {
            Clientes_Filtrar();
        }
        catch (Exception ex)
        {
            lblError_FltBusqueda.Text = ex.Message;
            pnlError_FltBusqueda.Visible = true;
        }
    }

    protected void gvResultadoClientes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvResultadoClientes.PageIndex = e.NewPageIndex;
        Clientes_Filtrar();
    }

    protected void gvResultadoClientes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //Si es cambio de pagina envia el numero de pagina
        if (e.CommandArgument.ToString().IndexOf(";") == -1)
            return;
        string[] comArgs = new string[2];
        comArgs = e.CommandArgument.ToString().Split(';');
        string idSolicitud = comArgs[0];
        string idPeticionReestructuraStr = comArgs[1];
        int idPeticionReestructura = 0;
        int.TryParse(idPeticionReestructuraStr, out idPeticionReestructura);
        string accion = e.CommandName.ToString();

        using (wsSOPF.CreditoClient ws = new CreditoClient())
        {
            // VERIFICAR SI EXISTE UNA LIQUIDACION ACTIVA PARA LA SOLICITUD 
            wsSOPF.TBCreditos.Liquidacion LiquidacionActiva = ws.ObtenerLiquidacionActiva(new TBCreditos.Liquidacion() { IdSolicitud = Convert.ToInt32(idSolicitud) });
            if (LiquidacionActiva != null) accion = "Liq. Activa";
        }

        switch (accion)
        {
            case "Crear":
                SolicitudOperacion = int.Parse(idSolicitud);
                PeticionReestructuraOperacion = 0;
                CargarHistorialPagos();
                CargarTablaRecibos(sender, e);
                pnlDatos.Enabled = true;
                upBusquedaCliente.Update();
                pnlHistorialPagos.Enabled = true;
                upHistorialPagos.Update();
                pnlTablaRecibos.Enabled = true;
                upTablaRecibos.Update();
                pnlDatosComentarios.Enabled = true;
                upDatosComentarios.Update();
                ScriptManager.RegisterStartupScript(this, typeof(string), "Accordion", "$(function(){ $('#divAccordion').accordion({active: 2}); });", true);
                break;
            case "Analizar":
            case "Preautorizar":
            case "Consultar":
                SolicitudOperacion = int.Parse(idSolicitud);
                if (idPeticionReestructura > 0)
                    PeticionReestructuraOperacion = idPeticionReestructura;
                CargarHistorialPagos();
                CargarTablaRecibos(sender, e);
                upBusquedaCliente.Update();
                pnlHistorialPagos.Enabled = false;
                upHistorialPagos.Update();
                pnlTablaRecibos.Enabled = false;
                upTablaRecibos.Update();
                pnlDatosComentarios.Enabled = false;
                upDatosComentarios.Update();
                Clientes_Filtrar();
                ScriptManager.RegisterStartupScript(this, typeof(string), "Accordion", "$(function(){ $('#divAccordion').accordion({active: 2}); });", true);
                break;
            case "Liq. Activa":
                ScriptManager.RegisterStartupScript(this, typeof(string), "Alerta", "alert('No se puede crear la reestructura: existe una liquidación activa para esta solicitud.');", true);
                break;
            case "Regularizar":
            default:
                break;
        }
        ValidarPermisoAdmin();
    }

    private void Clientes_Filtrar()
    {
        wsSOPF.PeticionReestructura.BusquedaClienteSolicitudPRReq entity = new PeticionReestructura.BusquedaClienteSolicitudPRReq();
        int IdSolicitud = 0;

        if (int.TryParse(txtBuscarSolicitud.Text.Trim(), out IdSolicitud))
            entity.IdSolicitud = IdSolicitud;

        entity.NombreCliente = txtBuscarCliente.Text.Trim();
        entity.Bucket = Convert.ToInt32(cboBucket.SelectedValue);
        entity.RazonSocialEmpresa = txtBuscarRazon.Text.Trim();
        entity.DependenciaEmpresa = txtBuscarDependencia.Text.Trim();
        entity.UbicacionEmpresa = txtBuscarDireccion.Text.Trim();

        using (wsSOPF.GestionClient ws = new GestionClient())
        {
            List<wsSOPF.PeticionReestructura.BusquedaClienteSolicitudPRRes> clientes = ws.Clientes_Filtrar("BUSCAR_CLIENTE_REESTRUCTURA", entity).ToList();
            gvResultadoClientes.DataSource = clientes;
            gvResultadoClientes.DataBind();
        }
    }

    public string GetColorBotonPeticionText(object dataItem)
    {
        string clase = string.Empty;
        string val = dataItem as string;
        switch (val)
        {
            case "Inexistente":
            case "Rechazada por Autorizador":
                clase = "btn btn-success btn-sm";
                break;
            case "Registrada":
            case "Asignada":
                clase = "btn btn-primary btn-sm";
                break;
            case "Análisis":
            case "Preautorizada":
                clase = "btn btn-warning btn-sm";
                break;
            case "Autorizada":
            case "Credito Liberado":
                clase = "btn btn-info btn-sm";
                break;
            case "Condicionada por Analista":
            case "Condicionada por Autorizador":
            case "Rechazada por Analista":
                clase = "btn btn-danger btn-sm";
                break;
        }
        return clase;
    }

    public string GetLabelBotonPeticionText(object dataItem)
    {
        string text = string.Empty;
        string val = dataItem as string;
        // TODO: CON BASE EN PERFIL ALTERAR EL VERBO DEL TEXTO
        switch (val)
        {
            case "Inexistente":
            case "Rechazada por Autorizador":
                text = "Crear";
                break;
            case "Registrada":
            case "Asignada":
                text = "Analizar";
                break;
            case "Análisis":
            case "Preautorizada":
                text = "Preautorizar";
                break;
            case "Autorizada":
            case "Rechazada por Analista":
            case "Credito Liberado":
                text = "Consultar";
                break;
            case "Condicionada por Analista":
            case "Condicionada por Autorizador":
                text = "Regularizar";
                break;
        }
        return text;
    }

    public string GetCommandNameBotonPeticion(object dataItem)
    {
        string text = string.Empty;
        string val = dataItem as string;
        // TODO: CON BASE EN PERFIL ALTERAR EL VERBO DEL TEXTO
        switch (val)
        {
            case "Inexistente":
            case "Rechazada por Autorizador":
                text = "Crear";
                break;
            case "Registrada":
            case "Asignada":
                text = "Analizar";
                break;
            case "Análisis":
            case "Preautorizada":
                text = "Preautorizar";
                break;
            case "Autorizada":
            case "Rechazada por Analista":
            case "Credito Liberado":
                //case "Rechazada por Autorizador":
                text = "Consultar";
                break;
            case "Condicionada por Analista":
            case "Condicionada por Autorizador":
                text = "Regularizar";
                break;
        }
        return text;
    }
    #endregion

    #region HistorialPagos
    private void CargarHistorialPagos()
    {
        using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
        {
            wsSOPF.BalanceCuenta eCuenta = new wsSOPF.BalanceCuenta() { IdSolicitud = SolicitudOperacion };
            eCuenta = ws.Balance_Obtener(eCuenta);
            List<wsSOPF.Pago> ePagos = ws.Pagos_Filtrar(new Pago() { IdSolicitud = eCuenta.IdSolicitud }).ToList();
            gvHistorialPagos.DataSource = ePagos;
            gvHistorialPagos.DataBind();
            upHistorialPagos.Update();
        }
    }

    protected void gvHistorialPagos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            wsSOPF.Pago dataItem = (wsSOPF.Pago)e.Row.DataItem;

            // OBTENEMOS LOS INDEX DE CADA COLUMNA PARA ACCESARLA POSTERIORMENTE
            int indexEstatusPago = ControlsHelper.GetColumnIndexByName(e.Row, "Estatus.EstatusDesc");

            switch (dataItem.Estatus.EstatusClave)
            {
                case "PR":
                    e.Row.Cells[indexEstatusPago].CssClass = "active";
                    break;
                case "PP":
                    e.Row.Cells[indexEstatusPago].CssClass = "active";
                    break;
                case "PA":
                    e.Row.Cells[indexEstatusPago].CssClass = "success";
                    break;
                case "PC":
                    e.Row.Cells[indexEstatusPago].CssClass = "warning";
                    break;
                case "PD":
                    e.Row.Cells[indexEstatusPago].CssClass = "";
                    break;
                case "DA":
                    e.Row.Cells[indexEstatusPago].CssClass = "danger";
                    break;
                default:
                    break;
            }
        }
    }
    #endregion

    #region RecibosPago
    private void CalcularLiquidacion(object sender, EventArgs e)
    {
        bool esPorcentaje = false;
        string[] camposPorcentajes = { "txtInteresPorcentaje", "txtIGVPorcentaje", "txtSeguroPorcentaje", "txtGATPorcentaje" };
        string type = sender.GetType().ToString();
        switch (type)
        {
            case "System.Web.UI.WebControls.TextBox":
                TextBox t = (TextBox)sender;
                if (string.IsNullOrWhiteSpace(t.Text))
                {
                    t.Text = "0";
                    break;
                }
                esPorcentaje = camposPorcentajes.Contains(t.ID);
                if (esPorcentaje)
                {
                    int valor = 0;
                    if (int.TryParse(t.Text, out valor))
                    {
                        if (valor < 0)
                            t.Text = "0";
                        else if (valor > ValorMaximoPorcentaje)
                            t.Text = ValorMaximoPorcentaje.ToString();
                    }
                    else
                    {
                        t.Text = "0";
                    }
                    //OBTENER ID PARA SABER A QUE COLUMNA DISTRIBUIR EL PORCENTAJE DE DESCUENTO
                    DescontarPorcentajePorColumna(t.ID, t.Text);
                }
                else
                {
                    //ES TXT DE RECIBOS
                    if (ViewState["RecibosLiquidacion"] != null)
                    {
                        string rec = Regex.Match(t.ID, @"\d+").Value;
                        TBCreditos.CalculoLiquidacion liqData = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacion"];
                        TBCreditos.CalculoLiquidacion liqDataCond = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacionCondonado"];
                        TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle recData = liqData.DetalleLiquidacion.Where(res => res.Recibo == Convert.ToInt32(rec)).FirstOrDefault();
                        TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle recDataCond = liqDataCond.DetalleLiquidacion.Where(res => res.Recibo == Convert.ToInt32(rec)).FirstOrDefault();
                        if (recData != null)
                        {
                            decimal valor = 0;
                            if (decimal.TryParse(t.Text, out valor))
                            {
                                if (valor < 0)
                                {
                                    t.Text = "0";
                                }
                                else
                                {
                                    string concepto = string.Empty;
                                    if (t.ID.IndexOf("Interes") != -1)
                                    {
                                        if (valor < recData.LiquidaInteres)
                                        {
                                            t.Text = decimal.Round(valor, 2, MidpointRounding.AwayFromZero).ToString();
                                            recDataCond.LiquidaInteres = decimal.Round(recData.LiquidaInteres - valor, 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            t.Text = recData.LiquidaInteres.ToString();
                                            recDataCond.LiquidaInteres = 0;
                                        }
                                    }
                                    else if (t.ID.IndexOf("IGV") != -1)
                                    {
                                        if (valor < recData.LiquidaIGV)
                                        {
                                            t.Text = decimal.Round(valor, 2, MidpointRounding.AwayFromZero).ToString();
                                            recDataCond.LiquidaIGV = decimal.Round(recData.LiquidaIGV - valor, 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            t.Text = recData.LiquidaIGV.ToString();
                                            recDataCond.LiquidaIGV = 0;
                                        }
                                    }
                                    else if (t.ID.IndexOf("Seguro") != -1)
                                    {
                                        if (valor < recData.LiquidaSeguro)
                                        {
                                            t.Text = decimal.Round(valor, 2, MidpointRounding.AwayFromZero).ToString();
                                            recDataCond.LiquidaSeguro = decimal.Round(recData.LiquidaSeguro - valor, 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            t.Text = recData.LiquidaSeguro.ToString();
                                            recDataCond.LiquidaSeguro = 0;
                                        }
                                    }
                                    else if (t.ID.IndexOf("GAT") != -1)
                                    {
                                        if (valor < recData.LiquidaGAT)
                                        {
                                            t.Text = decimal.Round(valor, 2, MidpointRounding.AwayFromZero).ToString();
                                            recDataCond.LiquidaGAT = decimal.Round(recData.LiquidaGAT - valor, 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            t.Text = recData.LiquidaGAT.ToString();
                                            recDataCond.LiquidaGAT = 0;
                                        }
                                    }
                                }

                            }
                            else
                            {
                                t.Text = "0";
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                CalcularColumnaTotalPagar();
                //COMPARAR POSICION PARA GUARDAR EN VIEWSTATE DE CONDONACION PARA CALCULAR TOTALES
                CargarTotales();
                CalcularSimulacion();
                break;
            case "System.Web.UI.WebControls.DropDownList":
                CalcularSimulacion();
                break;
            default:
                break;
        }
    }

    protected void ConstruirControlesGvRecibosLiquidacion(object sender, EventArgs e)
    {
        bool esPB = false;
        bool esReset = false;
        bool esPrimerPB = false;
        Button btnRst = Master.FindControl("MainContent").FindControl("btnResetRecibos") as Button;
        string type = sender.GetType().ToString();
        switch (type)
        {
            case "System.Web.UI.WebControls.Button":
                Button b = (Button)sender;
                esPB = b.ID == "btnResetRecibos";
                esReset = b.ID == "btnResetRecibos";
                break;
            case "System.Web.UI.WebControls.TextBox":
            case "System.Web.UI.WebControls.GridView":
                //if (ACCIONES.Contains(((CommandEventArgs)e).CommandName.ToString()))
                //    break;
                esPB = btnRst != null;
                break;
            case "ASP.site_solicitudes_frmpeticionreestructura_aspx":
                //esPrimerPB
                esPB = btnRst != null;
                break;
            default:
                break;
        }
        if (ViewState["RecibosLiquidacion"] != null && ViewState["RecibosLiquidacionCondonado"] != null)
        {
            GridView hGrid = gvRecibos_Liquidacion;

            // OBTENEMOS LOS INDEX DE CADA COLUMNA PARA ACCESARLA POSTERIORMENTE
            int idxCuota = ControlsHelper.GetColumnIndexByName(hGrid.Rows[0], "Cuota");
            int idxFecha = ControlsHelper.GetColumnIndexByName(hGrid.Rows[0], "Fecha");
            int idxCapital = ControlsHelper.GetColumnIndexByName(hGrid.Rows[0], "Capital");

            int idxInteres = ControlsHelper.GetColumnIndexByName(hGrid.Rows[0], "LiquidaInteres");
            int idxIGV = ControlsHelper.GetColumnIndexByName(hGrid.Rows[0], "LiquidaIGV");
            int idxSeguro = ControlsHelper.GetColumnIndexByName(hGrid.Rows[0], "LiquidaSeguro");
            int idxGAT = ControlsHelper.GetColumnIndexByName(hGrid.Rows[0], "LiquidaGAT");
            int idxRecibo = ControlsHelper.GetColumnIndexByName(hGrid.Rows[0], "Recibo");

            if (!esPB)
            {
                Panel pnlRowTitulosPorcentajes = new Panel();
                pnlRowTitulosPorcentajes.CssClass = "form-inline row";
                pnlRowTitulosPorcentajes.Attributes.CssStyle["text-align"] = "center";
                pnlRowTitulosPorcentajes.Attributes.CssStyle["vertical-align"] = "middle";

                HtmlGenericControl spanInteresTituloPorcentaje = new HtmlGenericControl("span");
                spanInteresTituloPorcentaje.InnerHtml = "Condonar Interés";
                spanInteresTituloPorcentaje.Attributes.CssStyle["font-weight"] = "bold";
                Panel pnlSpanInteresTituloPorcentajes = new Panel();
                pnlSpanInteresTituloPorcentajes.CssClass = "col-md-3";
                pnlSpanInteresTituloPorcentajes.Attributes["style"] = "padding: 0;";
                pnlSpanInteresTituloPorcentajes.Controls.Add(spanInteresTituloPorcentaje);

                pnlRowTitulosPorcentajes.Controls.Add(pnlSpanInteresTituloPorcentajes);

                HtmlGenericControl spanIGVTituloPorcentaje = new HtmlGenericControl("span");
                spanIGVTituloPorcentaje.InnerHtml = "Condonar IGV";
                spanIGVTituloPorcentaje.Attributes.CssStyle["font-weight"] = "bold";
                Panel pnlSpanIGVTituloPorcentajes = new Panel();
                pnlSpanIGVTituloPorcentajes.CssClass = "col-md-3";
                pnlSpanIGVTituloPorcentajes.Attributes["style"] = "padding: 0;";
                pnlSpanIGVTituloPorcentajes.Controls.Add(spanIGVTituloPorcentaje);

                pnlRowTitulosPorcentajes.Controls.Add(pnlSpanIGVTituloPorcentajes);

                HtmlGenericControl spanSeguroTituloPorcentaje = new HtmlGenericControl("span");
                spanSeguroTituloPorcentaje.InnerHtml = "Condonar Seguro";
                spanSeguroTituloPorcentaje.Attributes.CssStyle["font-weight"] = "bold";
                Panel pnlSeguroTituloPorcentajes = new Panel();
                pnlSeguroTituloPorcentajes.CssClass = "col-md-3";
                pnlSeguroTituloPorcentajes.Attributes["style"] = "padding: 0;";
                pnlSeguroTituloPorcentajes.Controls.Add(spanSeguroTituloPorcentaje);

                pnlRowTitulosPorcentajes.Controls.Add(pnlSeguroTituloPorcentajes);

                HtmlGenericControl spanGATTituloPorcentaje = new HtmlGenericControl("span");
                spanGATTituloPorcentaje.InnerHtml = "Condonar GAT";
                spanGATTituloPorcentaje.Attributes.CssStyle["font-weight"] = "bold";
                Panel pnlGATIGVTituloPorcentajes = new Panel();
                pnlGATIGVTituloPorcentajes.CssClass = "col-md-3";
                pnlGATIGVTituloPorcentajes.Attributes["style"] = "padding: 0;";
                pnlGATIGVTituloPorcentajes.Controls.Add(spanGATTituloPorcentaje);

                pnlRowTitulosPorcentajes.Controls.Add(pnlGATIGVTituloPorcentajes);

                Panel pnlRowPorcentajes = new Panel();
                pnlRowPorcentajes.CssClass = "form-inline row";
                pnlRowPorcentajes.Attributes.CssStyle["text-align"] = "center";
                pnlRowPorcentajes.Attributes.CssStyle["vertical-align"] = "middle";
                Button btnResetRecibos = new Button
                {
                    ID = "btnResetRecibos",
                    CssClass = "form-control",
                    Text = "Reset",
                    ForeColor = System.Drawing.Color.Green,
                    BorderColor = System.Drawing.Color.Green
                };
                btnResetRecibos.Attributes["style"] = "text-align: center;";
                btnResetRecibos.Click += new EventHandler(CargarTablaRecibos);
                #region Interes
                TextBox txtInteresPorcentaje = new TextBox
                {
                    ID = "txtInteresPorcentaje",
                    CssClass = "form-control",
                    Width = 65,
                    MaxLength = 6,
                    ToolTip = "Porcentaje a condonar",
                    AutoPostBack = true,
                    TextMode = TextBoxMode.Number
                };
                txtInteresPorcentaje.Attributes["style"] = "text-align: right;";
                txtInteresPorcentaje.Attributes["min"] = "0";
                txtInteresPorcentaje.Attributes["max"] = "100";
                txtInteresPorcentaje.Attributes["step"] = "1";
                txtInteresPorcentaje.TextChanged += new EventHandler(CalcularLiquidacion);
                RegularExpressionValidator revPorcentajeInteres = new RegularExpressionValidator
                {
                    ControlToValidate = "txtInteresPorcentaje",
                    ValidationExpression = "\\d+"
                };
                HtmlGenericControl spanPorcentajeInteres = new HtmlGenericControl("span");
                spanPorcentajeInteres.InnerHtml = "&nbsp;%&nbsp;";
                Button btnInteresPorcentajeMax = new Button
                {
                    ID = "btnInteresPorcentajeMax",
                    CssClass = "form-control",
                    Text = "+",
                    ForeColor = System.Drawing.Color.Green
                };
                btnInteresPorcentajeMax.Attributes["style"] = "padding: 3px;";
                btnInteresPorcentajeMax.Click += new EventHandler(MaxMinPorcentaje);
                Button btnInteresPorcentajeMin = new Button
                {
                    ID = "btnInteresPorcentajeMin",
                    CssClass = "form-control",
                    Text = " - ",
                    ForeColor = System.Drawing.Color.Red
                };
                btnInteresPorcentajeMin.Attributes["style"] = "padding: inherit;";
                btnInteresPorcentajeMin.Click += new EventHandler(MaxMinPorcentaje);
                Panel pnlCellInteresPorcentajes = new Panel();
                pnlCellInteresPorcentajes.CssClass = "col-md-3";
                pnlCellInteresPorcentajes.Attributes["style"] = "padding: 0;";
                pnlCellInteresPorcentajes.Controls.Add(txtInteresPorcentaje);
                pnlCellInteresPorcentajes.Controls.Add(revPorcentajeInteres);
                pnlCellInteresPorcentajes.Controls.Add(spanPorcentajeInteres);
                pnlCellInteresPorcentajes.Controls.Add(btnInteresPorcentajeMax);
                pnlCellInteresPorcentajes.Controls.Add(btnInteresPorcentajeMin);
                #endregion

                #region IGV
                TextBox txtIGVPorcentaje = new TextBox
                {
                    ID = "txtIGVPorcentaje",
                    CssClass = "form-control",
                    Width = 65,
                    MaxLength = 6,
                    ToolTip = "Porcentaje a condonar",
                    AutoPostBack = true,
                    TextMode = TextBoxMode.Number
                };
                txtIGVPorcentaje.Attributes["style"] = "text-align: right;";
                txtIGVPorcentaje.Attributes["min"] = "0";
                txtIGVPorcentaje.Attributes["max"] = "100";
                txtIGVPorcentaje.Attributes["step"] = "1";
                txtIGVPorcentaje.TextChanged += new EventHandler(CalcularLiquidacion);
                RegularExpressionValidator revPorcentajeIGV = new RegularExpressionValidator
                {
                    ControlToValidate = "txtIGVPorcentaje",
                    ValidationExpression = "\\d+"
                };
                HtmlGenericControl spanPorcentajeIGV = new HtmlGenericControl("span");
                spanPorcentajeIGV.InnerHtml = "&nbsp;%&nbsp;";
                Button btnIGVPorcentajeMax = new Button
                {
                    ID = "btnIGVPorcentajeMax",
                    CssClass = "form-control",
                    Text = "+",
                    ForeColor = System.Drawing.Color.Green
                };
                btnIGVPorcentajeMax.Attributes["style"] = "padding: 3px;";
                btnIGVPorcentajeMax.Click += new EventHandler(MaxMinPorcentaje);
                Button btnIGVPorcentajeMin = new Button
                {
                    ID = "btnIGVPorcentajeMin",
                    CssClass = "form-control",
                    Text = " - ",
                    ForeColor = System.Drawing.Color.Red
                };
                btnIGVPorcentajeMin.Attributes["style"] = "padding: 3px;";
                btnIGVPorcentajeMin.Click += new EventHandler(MaxMinPorcentaje);
                Panel pnlCellIGVPorcentajes = new Panel();
                pnlCellIGVPorcentajes.CssClass = "col-md-3";
                pnlCellIGVPorcentajes.Attributes["style"] = "padding: 0;";
                pnlCellIGVPorcentajes.Controls.Add(txtIGVPorcentaje);
                pnlCellIGVPorcentajes.Controls.Add(revPorcentajeIGV);
                pnlCellIGVPorcentajes.Controls.Add(spanPorcentajeIGV);
                pnlCellIGVPorcentajes.Controls.Add(btnIGVPorcentajeMax);
                pnlCellIGVPorcentajes.Controls.Add(btnIGVPorcentajeMin);
                #endregion

                #region Seguro
                TextBox txtSeguroPorcentaje = new TextBox
                {
                    ID = "txtSeguroPorcentaje",
                    CssClass = "form-control",
                    Width = 65,
                    MaxLength = 6,
                    ToolTip = "Porcentaje a condonar",
                    AutoPostBack = true,
                    TextMode = TextBoxMode.Number
                };
                txtSeguroPorcentaje.Attributes["style"] = "text-align: right;";
                txtSeguroPorcentaje.Attributes["min"] = "0";
                txtSeguroPorcentaje.Attributes["max"] = "100";
                txtSeguroPorcentaje.Attributes["step"] = "1";
                txtSeguroPorcentaje.TextChanged += new EventHandler(CalcularLiquidacion);
                RegularExpressionValidator revPorcentajeSeguro = new RegularExpressionValidator
                {
                    ControlToValidate = "txtSeguroPorcentaje",
                    ValidationExpression = "\\d+"
                };
                HtmlGenericControl spanPorcentajeSeguro = new HtmlGenericControl("span");
                spanPorcentajeSeguro.InnerHtml = "&nbsp;%&nbsp;";
                Button btnSeguroPorcentajeMax = new Button
                {
                    ID = "btnSeguroPorcentajeMax",
                    CssClass = "form-control",
                    Text = "+",
                    ForeColor = System.Drawing.Color.Green
                };
                btnSeguroPorcentajeMax.Attributes["style"] = "padding: 3px;";
                btnSeguroPorcentajeMax.Click += new EventHandler(MaxMinPorcentaje);
                Button btnSeguroPorcentajeMin = new Button
                {
                    ID = "btnSeguroPorcentajeMin",
                    CssClass = "form-control",
                    Text = " - ",
                    ForeColor = System.Drawing.Color.Red
                };
                btnSeguroPorcentajeMin.Attributes["style"] = "padding: 3px;";
                btnSeguroPorcentajeMin.Click += new EventHandler(MaxMinPorcentaje);
                Panel pnlCellSeguroPorcentajes = new Panel();
                pnlCellSeguroPorcentajes.CssClass = "col-md-3";
                pnlCellIGVPorcentajes.Attributes["style"] = "padding: 0;";
                pnlCellSeguroPorcentajes.Controls.Add(txtSeguroPorcentaje);
                pnlCellSeguroPorcentajes.Controls.Add(revPorcentajeSeguro);
                pnlCellSeguroPorcentajes.Controls.Add(spanPorcentajeSeguro);
                pnlCellSeguroPorcentajes.Controls.Add(btnSeguroPorcentajeMax);
                pnlCellSeguroPorcentajes.Controls.Add(btnSeguroPorcentajeMin);
                #endregion

                #region GAT
                TextBox txtGATPorcentaje = new TextBox
                {
                    ID = "txtGATPorcentaje",
                    CssClass = "form-control",
                    Width = 65,
                    MaxLength = 6,
                    ToolTip = "Porcentaje a condonar",
                    AutoPostBack = true,
                    TextMode = TextBoxMode.Number

                };
                txtGATPorcentaje.Attributes["style"] = "text-align: right;";
                txtGATPorcentaje.Attributes["min"] = "0";
                txtGATPorcentaje.Attributes["max"] = "100";
                txtGATPorcentaje.Attributes["step"] = "1";
                txtGATPorcentaje.TextChanged += new EventHandler(CalcularLiquidacion);
                RegularExpressionValidator revPorcentajeGAT = new RegularExpressionValidator
                {
                    ControlToValidate = "txtGATPorcentaje",
                    ValidationExpression = "\\d+"
                };
                HtmlGenericControl spanPorcentajeGAT = new HtmlGenericControl("span");
                spanPorcentajeGAT.InnerHtml = "&nbsp;%&nbsp;";
                Button btnGATPorcentajeMax = new Button
                {
                    ID = "btnGATPorcentajeMax",
                    CssClass = "form-control",
                    Text = "+",
                    ForeColor = System.Drawing.Color.Green
                };
                btnGATPorcentajeMax.Attributes["style"] = "padding: 3px;";
                btnGATPorcentajeMax.Click += new EventHandler(MaxMinPorcentaje);
                Button btnGATPorcentajeMin = new Button
                {
                    ID = "btnGATPorcentajeMin",
                    CssClass = "form-control",
                    Text = " - ",
                    ForeColor = System.Drawing.Color.Red
                };
                btnGATPorcentajeMin.Attributes["style"] = "padding: 3px;";
                btnGATPorcentajeMin.Click += new EventHandler(MaxMinPorcentaje);
                Panel pnlCellGATPorcentajes = new Panel();
                pnlCellGATPorcentajes.CssClass = "col-md-3";
                pnlCellIGVPorcentajes.Attributes["style"] = "padding: 0;";
                pnlCellGATPorcentajes.Controls.Add(txtGATPorcentaje);
                pnlCellGATPorcentajes.Controls.Add(revPorcentajeGAT);
                pnlCellGATPorcentajes.Controls.Add(spanPorcentajeGAT);
                pnlCellGATPorcentajes.Controls.Add(btnGATPorcentajeMax);
                pnlCellGATPorcentajes.Controls.Add(btnGATPorcentajeMin);
                #endregion

                Panel pnlReset = new Panel();
                pnlReset.CssClass = "col-md-2 col-md-offset-10";
                pnlReset.Attributes["style"] += "padding-bottom: 20px;";
                pnlReset.Controls.Add(btnResetRecibos);

                pnlRowPorcentajes.Controls.Add(pnlReset);
                pnlRowPorcentajes.Controls.Add(pnlRowTitulosPorcentajes);
                pnlRowPorcentajes.Controls.Add(pnlCellInteresPorcentajes);
                pnlRowPorcentajes.Controls.Add(pnlCellIGVPorcentajes);
                pnlRowPorcentajes.Controls.Add(pnlCellSeguroPorcentajes);
                pnlRowPorcentajes.Controls.Add(pnlCellGATPorcentajes);

                pnlRowPorcentajes.EnableViewState = true;
                phHeaderControles.Controls.Add(pnlRowPorcentajes);
            }
            else if (esPB && esReset)
            {
                TextBox txtInteresPorcentaje = this.Master.FindControl("MainContent").FindControl("txtInteresPorcentaje") as TextBox;
                txtInteresPorcentaje.Text = txtInteresPorcentaje == null ? "(null)" : "0";
                TextBox txtIGVPorcentaje = this.Master.FindControl("MainContent").FindControl("txtIGVPorcentaje") as TextBox;
                txtIGVPorcentaje.Text = txtIGVPorcentaje == null ? "(null)" : "0";
                TextBox txtSeguroPorcentaje = this.Master.FindControl("MainContent").FindControl("txtSeguroPorcentaje") as TextBox;
                txtSeguroPorcentaje.Text = txtSeguroPorcentaje == null ? "(null)" : "0";
                TextBox txtGATPorcentaje = this.Master.FindControl("MainContent").FindControl("txtGATPorcentaje") as TextBox;
                txtGATPorcentaje.Text = txtGATPorcentaje == null ? "(null)" : "0";
            }
            // esPB

            foreach (DataControlFieldHeaderCell cell in hGrid.HeaderRow.Cells)
            {
                cell.Attributes.CssStyle["text-align"] = "center";
                cell.Attributes.CssStyle["vertical-align"] = "middle";
            }

            foreach (GridViewRow row in gvRecibos_Liquidacion.Rows)
            {
                foreach (TableCell cell in row.Cells)
                {
                    cell.Attributes.CssStyle["text-align"] = "center";
                    cell.Attributes.CssStyle["vertical-align"] = "middle";
                }

                TBCreditos.CalculoLiquidacion objDataCalculoLiquidacion = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacion"];
                TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle objData = objDataCalculoLiquidacion.DetalleLiquidacion
                        .Where(resultado => resultado.Recibo == Convert.ToInt32(row.Cells[idxRecibo].Text)).FirstOrDefault();

                TBCreditos.CalculoLiquidacion objDataCalculoLiquidacionC = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacionCondonado"];
                TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle objDataC = objDataCalculoLiquidacionC.DetalleLiquidacion
                        .Where(resultado => resultado.Recibo == Convert.ToInt32(row.Cells[idxRecibo].Text)).FirstOrDefault();

                if (EstatusReciboCondonables.Contains(objData.TipoReciboCalculo))
                {
                    int i = 0;
                    row.Enabled = true;
                    TextBox txt = new TextBox();

                    if (PeticionReestructuraOperacion > 0)
                    {
                        row.Cells[idxInteres].Controls.Add(new TextBox
                        {
                            ID = "txtInteres" + row.Cells[idxRecibo].Text,
                            CssClass = "form-control",
                            Text = (objData.LiquidaInteres - objDataC.LiquidaInteres).ToString(),
                            ToolTip = "Inicial: " + objData.LiquidaInteres.ToString("C", CultureInfo.CurrentCulture),
                            AutoPostBack = true,
                            TextMode = TextBoxMode.Number
                        });
                        txt = (TextBox)row.Cells[idxInteres].Controls[0].FindControl("txtInteres" + row.Cells[idxRecibo].Text);
                        txt.TextChanged += new EventHandler(CalcularLiquidacion);
                        FormatearCargaDatosPeticion(txt, objData.LiquidaInteres.ToString("C", CultureInfo.CurrentCulture), (objData.LiquidaInteres - objDataC.LiquidaInteres).ToString("C", CultureInfo.CurrentCulture));
                        i++;

                        row.Cells[idxIGV].Controls.Add(new TextBox
                        {
                            ID = "txtIGV" + row.Cells[idxRecibo].Text,
                            CssClass = "form-control",
                            Text = (objData.LiquidaIGV - objDataC.LiquidaIGV).ToString(),
                            ToolTip = "Inicial: " + objData.LiquidaIGV.ToString("C", CultureInfo.CurrentCulture),
                            AutoPostBack = true,
                            TextMode = TextBoxMode.Number
                        });
                        txt = (TextBox)row.Cells[idxIGV].Controls[0].FindControl("txtIGV" + row.Cells[idxRecibo].Text);
                        txt.TextChanged += new EventHandler(CalcularLiquidacion);
                        FormatearCargaDatosPeticion(txt, objData.LiquidaIGV.ToString("C", CultureInfo.CurrentCulture), (objData.LiquidaIGV - objDataC.LiquidaIGV).ToString("C", CultureInfo.CurrentCulture));
                        i++;

                        row.Cells[idxSeguro].Controls.Add(new TextBox
                        {
                            ID = "txtSeguro" + row.Cells[idxRecibo].Text,
                            CssClass = "form-control",
                            Text = (objData.LiquidaSeguro - objDataC.LiquidaSeguro).ToString(),
                            ToolTip = "Inicial: " + objData.LiquidaSeguro.ToString("C", CultureInfo.CurrentCulture),
                            AutoPostBack = true,
                            TextMode = TextBoxMode.Number
                        });
                        txt = (TextBox)row.Cells[idxSeguro].Controls[0].FindControl("txtSeguro" + row.Cells[idxRecibo].Text);
                        txt.TextChanged += new EventHandler(CalcularLiquidacion);
                        FormatearCargaDatosPeticion(txt, objData.LiquidaSeguro.ToString("C", CultureInfo.CurrentCulture), (objData.LiquidaSeguro - objDataC.LiquidaSeguro).ToString("C", CultureInfo.CurrentCulture));
                        i++;

                        row.Cells[idxGAT].Controls.Add(new TextBox
                        {
                            ID = "txtGAT" + row.Cells[idxRecibo].Text,
                            CssClass = "form-control",
                            Text = (objData.LiquidaGAT - objDataC.LiquidaGAT).ToString(),
                            ToolTip = "Inicial: " + objData.LiquidaGAT.ToString("C", CultureInfo.CurrentCulture),
                            AutoPostBack = true,
                            TextMode = TextBoxMode.Number
                        });
                        txt = (TextBox)row.Cells[idxGAT].Controls[0].FindControl("txtGAT" + row.Cells[idxRecibo].Text);
                        txt.TextChanged += new EventHandler(CalcularLiquidacion);
                        FormatearCargaDatosPeticion(txt, objData.LiquidaGAT.ToString("C", CultureInfo.CurrentCulture), (objData.LiquidaGAT - objDataC.LiquidaGAT).ToString("C", CultureInfo.CurrentCulture));
                        i++;
                    }
                    else
                    {
                        row.Cells[idxInteres].Controls.Add(new TextBox
                        {
                            ID = "txtInteres" + row.Cells[idxRecibo].Text,
                            CssClass = "form-control",
                            Text = objData.LiquidaInteres.ToString(),
                            ToolTip = "Original: " + objData.LiquidaInteres.ToString("C", CultureInfo.CurrentCulture),
                            AutoPostBack = true,
                            TextMode = TextBoxMode.Number
                        });
                        txt = (TextBox)row.Cells[idxInteres].Controls[0].FindControl("txtInteres" + row.Cells[idxRecibo].Text);
                        txt.TextChanged += new EventHandler(CalcularLiquidacion);
                        i++;

                        row.Cells[idxIGV].Controls.Add(new TextBox
                        {
                            ID = "txtIGV" + row.Cells[idxRecibo].Text,
                            CssClass = "form-control",
                            Text = objData.LiquidaIGV.ToString(),
                            ToolTip = "Original: " + objData.LiquidaIGV.ToString("C", CultureInfo.CurrentCulture),
                            AutoPostBack = true,
                            TextMode = TextBoxMode.Number
                        });
                        txt = (TextBox)row.Cells[idxIGV].Controls[0].FindControl("txtIGV" + row.Cells[idxRecibo].Text);
                        txt.TextChanged += new EventHandler(CalcularLiquidacion);
                        i++;

                        row.Cells[idxSeguro].Controls.Add(new TextBox
                        {
                            ID = "txtSeguro" + row.Cells[idxRecibo].Text,
                            CssClass = "form-control",
                            Text = objData.LiquidaSeguro.ToString(),
                            ToolTip = "Original: " + objData.LiquidaSeguro.ToString("C", CultureInfo.CurrentCulture),
                            AutoPostBack = true,
                            TextMode = TextBoxMode.Number
                        });
                        txt = (TextBox)row.Cells[idxSeguro].Controls[0].FindControl("txtSeguro" + row.Cells[idxRecibo].Text);
                        txt.TextChanged += new EventHandler(CalcularLiquidacion);
                        i++;

                        row.Cells[idxGAT].Controls.Add(new TextBox
                        {
                            ID = "txtGAT" + row.Cells[idxRecibo].Text,
                            CssClass = "form-control",
                            Text = objData.LiquidaGAT.ToString(),
                            ToolTip = "Original: " + objData.LiquidaGAT.ToString("C", CultureInfo.CurrentCulture),
                            AutoPostBack = true,
                            TextMode = TextBoxMode.Number
                        });
                        txt = (TextBox)row.Cells[idxGAT].Controls[0].FindControl("txtGAT" + row.Cells[idxRecibo].Text);
                        txt.TextChanged += new EventHandler(CalcularLiquidacion);
                        i++;
                    }

                    row.BackColor = System.Drawing.ColorTranslator.FromHtml(ObtenerColorEstatusRecibo(objData.TipoReciboCalculo, false));
                }
                else
                {
                    row.Enabled = false;

                    row.Cells[idxInteres].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaInteres.ToString("C", CultureInfo.CurrentCulture) : "-";
                    row.Cells[idxIGV].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaIGV.ToString("C", CultureInfo.CurrentCulture) : "-";
                    row.Cells[idxSeguro].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaSeguro.ToString("C", CultureInfo.CurrentCulture) : "-";
                    row.Cells[idxGAT].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaGAT.ToString("C", CultureInfo.CurrentCulture) : "-";
                }
            }
        }
    }

    private void CargarTablaRecibos(object sender, EventArgs e)
    {
        try
        {
            if (PeticionReestructuraOperacion > 0)
            {
                PeticionReestructura peticion = null;
                using (wsSOPF.GestionClient ws = new GestionClient())
                {
                    PeticionReestructura entity = new PeticionReestructura()
                    {
                        Id_PeticionReestructura = PeticionReestructuraOperacion
                    };
                    ResultadoOfArrayOfPeticionReestructuraXs2b9qnq peticionRes = ws.ObtenerPeticionReestructura("CONSULTAR_FOTO_PETICION_REESTRUCTURA_POR_ID", entity);
                    if (peticionRes != null)
                        peticion = peticionRes.ResultObject.FirstOrDefault();
                    if (peticionRes.Codigo != 1)
                        throw new Exception(peticionRes.Mensaje);
                    txtComentariosCapturista.Text = peticionRes.Mensaje;
                    cboNuevoPlazo.SelectedIndex = cboNuevoPlazo.Items.IndexOf(cboNuevoPlazo.Items.FindByText(peticion.Pagos.ToString()));
                }

                List<TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle> listaRecibos = new List<TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle>();
                List<TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle> listaRecibosC = new List<TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle>();
                List<TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle> listaRecibosF = new List<TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle>();
                foreach (FotoPeticionReestructura.FotoReciboPeticionReestructura item in peticion.FotoPeticionReestructura.FotoReciboPeticionReestructuras)
                {
                    TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle d = new TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle()
                    {
                        Recibo = item.Recibo,
                        TipoReciboCalculo = item.TipoRecibo,
                        FechaRecibo = item.Fch_Recibo,
                        LiquidaCapital = item.Liquida_Capital,
                        LiquidaInteres = item.Liquida_Interes,
                        LiquidaIGV = item.Liquida_IGV,
                        LiquidaSeguro = item.Liquida_Seguro,
                        LiquidaGAT = item.Liquida_GAT,
                        LiquidaTotal = item.Liquida_Capital + item.Liquida_Interes + item.Liquida_IGV
                            + item.Liquida_Seguro + item.Liquida_GAT
                    };
                    if (item.FotoTipoReciboPetRes_Id == 1)
                    {
                        listaRecibos.Add(d);
                    }
                    else
                    {
                        listaRecibosC.Add(d);
                    }
                }
                TBCreditos.CalculoLiquidacion calc = new TBCreditos.CalculoLiquidacion()
                {
                    NombreCliente = peticion.FotoPeticionReestructura.Cliente,
                    Cuota = peticion.FotoPeticionReestructura.erogacion,
                    DetalleLiquidacion = listaRecibos.ToArray(),
                    DireccionCliente = peticion.FotoPeticionReestructura.Direccion,
                    DNICliente = peticion.FotoPeticionReestructura.NumeroDocumento,
                    FechaCredito = peticion.FotoPeticionReestructura.fch_Credito,
                    FechaLiquidacion = new UtilsDateTimeR() { Value = peticion.FechaRegistro },
                    FechaPrimerPago = peticion.FotoPeticionReestructura.FechaPrimerPago,
                    IdSolicitud = int.Parse(peticion.Solicitud_Id.ToString()),
                    MontoCredito = peticion.FotoPeticionReestructura.capital,
                    TotalLiquidacion = peticion.FotoPeticionReestructura.TotalLiquidacion
                };
                TBCreditos.CalculoLiquidacion calcC = new TBCreditos.CalculoLiquidacion()
                {
                    NombreCliente = peticion.FotoPeticionReestructura.Cliente,
                    Cuota = peticion.FotoPeticionReestructura.erogacion,
                    DetalleLiquidacion = listaRecibosC.ToArray(),
                    DireccionCliente = peticion.FotoPeticionReestructura.Direccion,
                    DNICliente = peticion.FotoPeticionReestructura.NumeroDocumento,
                    FechaCredito = peticion.FotoPeticionReestructura.fch_Credito,
                    FechaLiquidacion = new UtilsDateTimeR() { Value = peticion.FechaRegistro },
                    FechaPrimerPago = peticion.FotoPeticionReestructura.FechaPrimerPago,
                    IdSolicitud = int.Parse(peticion.Solicitud_Id.ToString()),
                    MontoCredito = peticion.FotoPeticionReestructura.capital,
                    TotalLiquidacion = peticion.FotoPeticionReestructura.TotalLiquidacion
                };
                //CREAMOS DT CON LA RESTA FINAL
                foreach (TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle item in calc.DetalleLiquidacion)
                {
                    TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle detC = calcC.DetalleLiquidacion
                    .Where(r => r.Recibo == item.Recibo).FirstOrDefault();
                    TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle d = new TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle()
                    {
                        Recibo = item.Recibo,
                        TipoReciboCalculo = item.TipoReciboCalculo,
                        FechaRecibo = item.FechaRecibo,
                        LiquidaCapital = item.LiquidaCapital - detC.LiquidaCapital,
                        LiquidaInteres = item.LiquidaInteres - detC.LiquidaInteres,
                        LiquidaIGV = item.LiquidaIGV - detC.LiquidaIGV,
                        LiquidaSeguro = item.LiquidaSeguro - detC.LiquidaSeguro,
                        LiquidaGAT = item.LiquidaGAT - detC.LiquidaGAT,
                        LiquidaTotal = item.LiquidaTotal - detC.LiquidaTotal
                    };
                    listaRecibosF.Add(d);
                }
                TBCreditos.CalculoLiquidacion calcF = new TBCreditos.CalculoLiquidacion()
                {
                    NombreCliente = peticion.FotoPeticionReestructura.Cliente,
                    Cuota = peticion.FotoPeticionReestructura.erogacion,
                    DetalleLiquidacion = listaRecibosF.ToArray(),
                    DireccionCliente = peticion.FotoPeticionReestructura.Direccion,
                    DNICliente = peticion.FotoPeticionReestructura.NumeroDocumento,
                    FechaCredito = peticion.FotoPeticionReestructura.fch_Credito,
                    FechaLiquidacion = new UtilsDateTimeR() { Value = peticion.FechaRegistro },
                    FechaPrimerPago = peticion.FotoPeticionReestructura.FechaPrimerPago,
                    IdSolicitud = int.Parse(peticion.Solicitud_Id.ToString()),
                    MontoCredito = peticion.FotoPeticionReestructura.capital,
                    TotalLiquidacion = peticion.FotoPeticionReestructura.TotalLiquidacion
                };

                DateTime fechaHoy = DateTime.Now;
                wsSOPF.TBCreditos.Liquidacion LiqActual = new TBCreditos.Liquidacion() { IdSolicitud = SolicitudOperacion };
                LiqActual.FechaLiquidar = new UtilsDateTimeR();
                LiqActual.FechaLiquidar.Value = (fechaHoy == null) ? DateTime.Today : fechaHoy;
                wsSOPF.ResultadoOfTBCreditosCalculoLiquidacionhDx61Z0Z resLiqActual = new wsSOPF.ResultadoOfTBCreditosCalculoLiquidacionhDx61Z0Z();
                using (wsSOPF.GestionClient ws = new GestionClient())
                {
                    // CALCULAR LA LIQUIDACION ACTUAL PARA COMPARAR VALORES
                    resLiqActual = ws.CalcularLiquidacionPeticionReestructura(LiqActual);
                    if (resLiqActual.Codigo != 0)
                        throw new Exception(resLiqActual.Mensaje);
                }

                txtFechaPeticion.Text = PeticionReestructuraOperacion > 0 ? FormatearCargaDatosPeticion(txtFechaPeticion, DateTime.Now.ToString("dd/MM/yyyy"), peticion.FechaRegistro.ToString("dd/MM/yyyy")) : DateTime.Now.ToString("dd/MM/yyyy");
                lblIdSolicitud_Liquidacion.Text = resLiqActual.ResultObject.IdSolicitud.ToString();
                lblDNI_Liquidacion.Text = FormatearCargaDatosPeticion(lblDNI_Liquidacion, resLiqActual.ResultObject.DNICliente, calc.DNICliente);
                string NombreClienteActual = string.Format("{0} {1} {2}", resLiqActual.ResultObject.NombreCliente, resLiqActual.ResultObject.ApPaternoCliente, resLiqActual.ResultObject.ApMaternoCliente);
                string NombreClientePeticion = calc.NombreCliente;
                lblCliente_Liquidacion.Text = FormatearCargaDatosPeticion(lblCliente_Liquidacion, NombreClienteActual, NombreClientePeticion);

                lblMontoCredito_Liquidacion.Text = calc.MontoCredito.ToString("C", CultureInfo.CurrentCulture);
                lblCuota_Liquidacion.Text = calc.Cuota.ToString("C", CultureInfo.CurrentCulture);

                lblTotalLiquidar_Liquidacion.Text = FormatearCargaDatosPeticion(lblTotalLiquidar_Liquidacion,
                    resLiqActual.ResultObject.TotalLiquidacion.ToString("C", CultureInfo.CurrentCulture),
                    calc.TotalLiquidacion.ToString("C", CultureInfo.CurrentCulture));
                lblFechaCredito_Liquidacion.Text = calc.FechaCredito.ToString("dd/MM/yyyy");
                lblFechaLiquidacion_Liquidacion.Text = calc.FechaLiquidacion.Value.GetValueOrDefault().ToString("dd/MM/yyyy");
                txtFechaLiquidar_Liquidacion.Text = lblFechaLiquidacion_Liquidacion.Text;
                lblFechaPrimerPago_Liquidacion.Text = calc.FechaPrimerPago.ToString("dd/MM/yyyy");

                gvRecibos_Liquidacion.DataSource = calcF.DetalleLiquidacion;
                gvRecibos_Liquidacion.DataBind();
                ViewState["RecibosLiquidacion"] = calc;
                ViewState["RecibosLiquidacionCondonado"] = calcC;
            }
            else
            {
                DateTime fechaLiquidacion = DateTime.Now;
                using (wsSOPF.GestionClient ws = new GestionClient())
                {
                    wsSOPF.TBCreditos.Liquidacion LiquidacionCalc = new TBCreditos.Liquidacion();
                    LiquidacionCalc.IdSolicitud = SolicitudOperacion;
                    LiquidacionCalc.FechaLiquidar = new UtilsDateTimeR();
                    LiquidacionCalc.FechaLiquidar.Value = (fechaLiquidacion == null) ? DateTime.Today : fechaLiquidacion;
                    // CALCULAR LA LIQUIDACION
                    wsSOPF.ResultadoOfTBCreditosCalculoLiquidacionhDx61Z0Z resCalculoLiquidacion = ws.CalcularLiquidacionPeticionReestructura(LiquidacionCalc);
                    if (resCalculoLiquidacion.Codigo != 0)
                        throw new Exception(resCalculoLiquidacion.Mensaje);
                    wsSOPF.TBCreditos.CalculoLiquidacion CalculoLiquidacion = resCalculoLiquidacion.ResultObject;
                    wsSOPF.ResultadoOfTBCreditosCalculoLiquidacionhDx61Z0Z resCalculoLiquidacionCondonacion = ws.CalcularLiquidacionPeticionReestructura(LiquidacionCalc);
                    if (resCalculoLiquidacionCondonacion.Codigo != 0)
                        throw new Exception(resCalculoLiquidacionCondonacion.Mensaje);
                    wsSOPF.TBCreditos.CalculoLiquidacion CalculoLiquidacionCondonacion = resCalculoLiquidacionCondonacion.ResultObject;

                    txtFechaPeticion.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    lblIdSolicitud_Liquidacion.Text = CalculoLiquidacion.IdSolicitud.ToString();
                    lblDNI_Liquidacion.Text = CalculoLiquidacion.DNICliente;
                    lblCliente_Liquidacion.Text = string.Format("{0} {1} {2}", CalculoLiquidacion.NombreCliente, CalculoLiquidacion.ApPaternoCliente, CalculoLiquidacion.ApMaternoCliente);
                    lblMontoCredito_Liquidacion.Text = CalculoLiquidacion.MontoCredito.ToString("C", CultureInfo.CurrentCulture);
                    lblCuota_Liquidacion.Text = CalculoLiquidacion.Cuota.ToString("C", CultureInfo.CurrentCulture);
                    lblTotalLiquidar_Liquidacion.Text = CalculoLiquidacion.TotalLiquidacion.ToString("C", CultureInfo.CurrentCulture);
                    lblFechaCredito_Liquidacion.Text = CalculoLiquidacion.FechaCredito.ToString("dd/MM/yyyy");
                    lblFechaLiquidacion_Liquidacion.Text = CalculoLiquidacion.FechaLiquidacion.Value.GetValueOrDefault().ToString("dd/MM/yyyy");
                    txtFechaLiquidar_Liquidacion.Text = lblFechaLiquidacion_Liquidacion.Text;
                    lblFechaPrimerPago_Liquidacion.Text = CalculoLiquidacion.FechaPrimerPago.ToString("dd/MM/yyyy");
                    gvRecibos_Liquidacion.DataSource = CalculoLiquidacion.DetalleLiquidacion;
                    gvRecibos_Liquidacion.DataBind();
                    ViewState["RecibosLiquidacion"] = CalculoLiquidacion;
                    ResetearFormato(txtFechaPeticion);
                    ResetearFormato(lblDNI_Liquidacion);
                    ResetearFormato(lblCliente_Liquidacion);
                    ResetearFormato(lblTotalLiquidar_Liquidacion);
                    txtComentariosCapturista.Text = "";
                    foreach (TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle item in CalculoLiquidacionCondonacion.DetalleLiquidacion)
                    {
                        item.LiquidaCapital = 0;
                        item.LiquidaInteres = 0;
                        item.LiquidaIGV = 0;
                        item.LiquidaSeguro = 0;
                        item.LiquidaGAT = 0;
                        item.LiquidaTotal = 0;
                    }
                    ViewState["RecibosLiquidacionCondonado"] = CalculoLiquidacionCondonacion;
                }
            }
            ConstruirControlesGvRecibosLiquidacion(sender, e);
            CargarTotales();
            CalcularSimulacion();
            upTablaRecibos.Update();
        }
        catch (LiquidacionException ex)
        {
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            pnlError.Visible = true;
        }
    }

    private void CargarTotales()
    {
        if (ViewState["RecibosLiquidacion"] != null && ViewState["RecibosLiquidacionCondonado"] != null)
        {
            decimal TotalLiquidaInteres = decimal.Zero;
            decimal TotalLiquidaIGV = decimal.Zero;
            decimal TotalLiquidaSeguro = decimal.Zero;
            decimal TotalLiquidaGAT = decimal.Zero;
            decimal TotalLiquidaTotal = decimal.Zero;

            TBCreditos.CalculoLiquidacion CalculoLiquidacion = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacion"];
            TBCreditos.CalculoLiquidacion CalculoLiquidacionCondonacion = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacionCondonado"];

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("TotalRecibos", typeof(int)));
            dt.Columns.Add(new DataColumn("Concepto", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalLiquidaCapital", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalLiquidaInteres", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalLiquidaIGV", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalLiquidaSeguro", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalLiquidaGAT", typeof(string)));
            dt.Columns.Add(new DataColumn("TotalLiquidaTotal", typeof(string)));

            DataRow dr = dt.NewRow();
            dr["TotalRecibos"] = CalculoLiquidacion.DetalleLiquidacion.Count(r => r.TipoReciboCalculo != "CUBIERTO");
            dr["Concepto"] = "Original";
            dr["TotalLiquidaCapital"] = CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaCapital).ToString("C", CultureInfo.CurrentCulture) + "\n (100%)";
            dr["TotalLiquidaInteres"] = CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaInteres).ToString("C", CultureInfo.CurrentCulture) + "\n (100%)";
            dr["TotalLiquidaIGV"] = CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaIGV).ToString("C", CultureInfo.CurrentCulture) + "\n (100%)";
            dr["TotalLiquidaSeguro"] = CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaSeguro).ToString("C", CultureInfo.CurrentCulture) + "\n (100%)";
            dr["TotalLiquidaGAT"] = CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaGAT).ToString("C", CultureInfo.CurrentCulture) + "\n (100%)";
            dr["TotalLiquidaTotal"] = CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaTotal).ToString("C", CultureInfo.CurrentCulture) + "\n (100%)";
            dt.Rows.Add(dr);

            DataRow dr1 = dt.NewRow();
            dr1["TotalRecibos"] = CalculoLiquidacionCondonacion.DetalleLiquidacion.Count(r => r.TipoReciboCalculo != "CUBIERTO"
                && (r.LiquidaInteres > 0 || r.LiquidaIGV > 0 || r.LiquidaSeguro > 0 || r.LiquidaGAT > 0));
            dr1["Concepto"] = "Condonar";
            dr1["TotalLiquidaCapital"] = CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaCapital).ToString("C", CultureInfo.CurrentCulture) + "\n (0.00%)";
            string valorPorcentajeInteres = CalcularPorcentaje(CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaInteres), CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaInteres));
            dr1["TotalLiquidaInteres"] = CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaInteres).ToString("C", CultureInfo.CurrentCulture)
                + "(" + valorPorcentajeInteres + "%)";
            CompararPorcentajeColumnaConTotal(valorPorcentajeInteres, "txtInteresPorcentaje");
            string valorPorcentajeIGV = CalcularPorcentaje(CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaIGV), CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaIGV));
            dr1["TotalLiquidaIGV"] = CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaIGV).ToString("C", CultureInfo.CurrentCulture)
                + "(" + valorPorcentajeIGV + "%)";
            CompararPorcentajeColumnaConTotal(valorPorcentajeIGV, "txtIGVPorcentaje");
            string valorPorcentajeSeguro = CalcularPorcentaje(CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaSeguro), CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaSeguro));
            dr1["TotalLiquidaSeguro"] = CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaSeguro).ToString("C", CultureInfo.CurrentCulture)
                + "(" + valorPorcentajeSeguro + "%)";
            CompararPorcentajeColumnaConTotal(valorPorcentajeSeguro, "txtSeguroPorcentaje");
            string valorPorcentajeGAT = CalcularPorcentaje(CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaGAT), CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaGAT));
            dr1["TotalLiquidaGAT"] = CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaGAT).ToString("C", CultureInfo.CurrentCulture)
                + "(" + valorPorcentajeGAT + "%)";
            CompararPorcentajeColumnaConTotal(valorPorcentajeGAT, "txtGATPorcentaje");
            dr1["TotalLiquidaTotal"] = CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaTotal).ToString("C", CultureInfo.CurrentCulture)
                + "(" + CalcularPorcentaje(CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaTotal), CalculoLiquidacionCondonacion.DetalleLiquidacion.Sum(item => item.LiquidaTotal)) + "%)";
            dt.Rows.Add(dr1);

            for (int i = 0; i < CalculoLiquidacion.DetalleLiquidacion.Length; i++)
            {
                TotalLiquidaInteres += CalculoLiquidacion.DetalleLiquidacion[i].LiquidaInteres - CalculoLiquidacionCondonacion.DetalleLiquidacion[i].LiquidaInteres;
                TotalLiquidaIGV += CalculoLiquidacion.DetalleLiquidacion[i].LiquidaIGV - CalculoLiquidacionCondonacion.DetalleLiquidacion[i].LiquidaIGV;
                TotalLiquidaSeguro += CalculoLiquidacion.DetalleLiquidacion[i].LiquidaSeguro - CalculoLiquidacionCondonacion.DetalleLiquidacion[i].LiquidaSeguro;
                TotalLiquidaGAT += CalculoLiquidacion.DetalleLiquidacion[i].LiquidaGAT - CalculoLiquidacionCondonacion.DetalleLiquidacion[i].LiquidaGAT;
                TotalLiquidaTotal += CalculoLiquidacion.DetalleLiquidacion[i].LiquidaTotal - CalculoLiquidacionCondonacion.DetalleLiquidacion[i].LiquidaTotal;
            }

            DataRow dr2 = dt.NewRow();
            dr2["TotalRecibos"] = CalculoLiquidacion.DetalleLiquidacion.Count(r => r.TipoReciboCalculo != "CUBIERTO");
            dr2["Concepto"] = "Liquidar";
            dr2["TotalLiquidaCapital"] = CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaCapital).ToString("C", CultureInfo.CurrentCulture) + " (100%)";
            dr2["TotalLiquidaInteres"] = TotalLiquidaInteres.ToString("C", CultureInfo.CurrentCulture)
            + "(" + CalcularPorcentaje(CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaInteres), TotalLiquidaInteres) + "%)";
            dr2["TotalLiquidaIGV"] = TotalLiquidaIGV.ToString("C", CultureInfo.CurrentCulture)
            + "(" + CalcularPorcentaje(CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaIGV), TotalLiquidaIGV) + "%)";
            dr2["TotalLiquidaSeguro"] = TotalLiquidaSeguro.ToString("C", CultureInfo.CurrentCulture)
            + "(" + CalcularPorcentaje(CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaSeguro), TotalLiquidaSeguro) + "%)";
            dr2["TotalLiquidaGAT"] = TotalLiquidaGAT.ToString("C", CultureInfo.CurrentCulture)
            + "(" + CalcularPorcentaje(CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaGAT), TotalLiquidaGAT) + "%)";
            dr2["TotalLiquidaTotal"] = TotalLiquidaTotal.ToString("C", CultureInfo.CurrentCulture)
            + "(" + CalcularPorcentaje(CalculoLiquidacion.DetalleLiquidacion.Sum(item => item.LiquidaTotal), TotalLiquidaTotal) + "%)";
            dt.Rows.Add(dr2);
            gvRecibos_Liquidacion_Totales.DataSource = dt;
            gvRecibos_Liquidacion_Totales.DataBind();
            TotalReestructurar = TotalLiquidaTotal;
        }
    }

    protected void MaxMinPorcentaje(object sender, EventArgs e)
    {
        string type = sender.GetType().ToString();
        switch (type)
        {
            case "System.Web.UI.WebControls.Button":
                Button b = (Button)sender;
                if (b != null)
                {
                    string ctrl = b.ID;
                    int posicionInicialPorcentaje = ctrl.IndexOf("Porcentaje");
                    string etiqueta = ctrl.Substring(3, posicionInicialPorcentaje - 3);
                    string comando = ctrl.Substring(ctrl.Length - 3, 3);
                    TextBox txt = this.Master.FindControl("MainContent").FindControl("txt" + etiqueta + "Porcentaje") as TextBox;
                    txt.Text = comando == "Min" ? "0" : "100";
                    CalcularLiquidacion(txt, e);
                }
                break;
            default:
                break;
        }
    }

    private string ObtenerColorEstatusRecibo(string estatus, bool regresaClaseBootstrap)
    {
        switch (estatus)
        {
            case "CUBIERTO":
                // #DADADA GRIS
                return regresaClaseBootstrap ? "table-active" : "#DADADA";
            case "VENCIDO":
                // #F5C6CB ROJO
                return regresaClaseBootstrap ? "table-danger" : "#F5C6CB";
            case "ACTUAL":
                // #C3E6CB VERDE
                return regresaClaseBootstrap ? "table-success" : "#C3E6CB";
            case "FUTURO":
                // #B8DAFF AZUL
                return regresaClaseBootstrap ? "table-primary" : "#B8DAFF";
            default:
                // #FFFFFF BLANCO
                return regresaClaseBootstrap ? "table-light" : "#FFFFFF";
        }
    }

    private int ObtenerIndiceColumnaPorTxtIdPorcentaje(string ctrl)
    {
        if (ctrl.IndexOf("Interes") != -1)
            return 3;
        else if (ctrl.IndexOf("IGV") != -1)
            return 4;
        else if (ctrl.IndexOf("Seguro") != -1)
            return 5;
        else if (ctrl.IndexOf("GAT") != -1)
            return 6;
        else
            return -1;
    }

    private void DescontarPorcentajePorColumna(string controlId, string porcentaje)
    {
        int indice = ObtenerIndiceColumnaPorTxtIdPorcentaje(controlId);
        decimal porcen = 0;

        if (decimal.TryParse(porcentaje, out porcen) && ViewState["RecibosLiquidacion"] != null && ViewState["RecibosLiquidacionCondonado"] != null)
        {
            TBCreditos.CalculoLiquidacion CalculoLiquidacion = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacion"];
            TBCreditos.CalculoLiquidacion CalculoLiquidacionCondonacion = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacionCondonado"];

            decimal multiplicador = porcen / 100;
            foreach (GridViewRow row in gvRecibos_Liquidacion.Rows)
            {
                decimal valor = decimal.Zero;
                if (controlId.IndexOf("Interes") != -1)
                {
                    string control = "txtInteres" + row.Cells[0].Text;
                    TextBox ctrl = (TextBox)row.FindControl(control);
                    if (ctrl != null)
                    {
                        TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle reciboOriginal = CalculoLiquidacion.DetalleLiquidacion.Where(r => r.Recibo == Convert.ToInt32(row.Cells[0].Text)).FirstOrDefault();
                        if (reciboOriginal != null)
                            valor = reciboOriginal.LiquidaInteres;

                        TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle recibo = CalculoLiquidacionCondonacion.DetalleLiquidacion.Where(r => r.Recibo == Convert.ToInt32(row.Cells[0].Text)).FirstOrDefault();
                        if (recibo != null)
                            recibo.LiquidaInteres = decimal.Round(valor * multiplicador, 2, MidpointRounding.AwayFromZero);
                        valor = valor - decimal.Round(valor * multiplicador, 2, MidpointRounding.AwayFromZero);
                        ctrl.Text = valor.ToString();
                    }
                    continue;
                }
                else if (controlId.IndexOf("IGV") != -1)
                {
                    string control = "txtIGV" + row.Cells[0].Text;
                    TextBox ctrl = (TextBox)row.FindControl(control);
                    if (ctrl != null)
                    {
                        TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle reciboOriginal = CalculoLiquidacion.DetalleLiquidacion.Where(r => r.Recibo == Convert.ToInt32(row.Cells[0].Text)).FirstOrDefault();
                        if (reciboOriginal != null)
                            valor = reciboOriginal.LiquidaIGV;

                        TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle recibo = CalculoLiquidacionCondonacion.DetalleLiquidacion.Where(r => r.Recibo == Convert.ToInt32(row.Cells[0].Text)).FirstOrDefault();
                        if (recibo != null)
                            recibo.LiquidaIGV = decimal.Round(valor * multiplicador, 2, MidpointRounding.AwayFromZero); ;
                        valor = valor - decimal.Round(valor * multiplicador, 2, MidpointRounding.AwayFromZero); ;
                        ctrl.Text = valor.ToString();
                    }
                    continue;
                }
                else if (controlId.IndexOf("Seguro") != -1)
                {
                    string control = "txtSeguro" + row.Cells[0].Text;
                    TextBox ctrl = (TextBox)row.FindControl(control);
                    if (ctrl != null)
                    {
                        TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle reciboOriginal = CalculoLiquidacion.DetalleLiquidacion.Where(r => r.Recibo == Convert.ToInt32(row.Cells[0].Text)).FirstOrDefault();
                        if (reciboOriginal != null)
                            valor = reciboOriginal.LiquidaSeguro;

                        TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle recibo = CalculoLiquidacionCondonacion.DetalleLiquidacion.Where(r => r.Recibo == Convert.ToInt32(row.Cells[0].Text)).FirstOrDefault();
                        if (recibo != null)
                            recibo.LiquidaSeguro = decimal.Round(valor * multiplicador, 2, MidpointRounding.AwayFromZero); ;
                        valor = valor - decimal.Round(valor * multiplicador, 2, MidpointRounding.AwayFromZero); ;
                        ctrl.Text = valor.ToString();
                    }
                    continue;
                }
                else if (controlId.IndexOf("GAT") != -1)
                {
                    string control = "txtGAT" + row.Cells[0].Text;
                    TextBox ctrl = (TextBox)row.FindControl(control);
                    if (ctrl != null)
                    {
                        TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle reciboOriginal = CalculoLiquidacion.DetalleLiquidacion.Where(r => r.Recibo == Convert.ToInt32(row.Cells[0].Text)).FirstOrDefault();
                        if (reciboOriginal != null)
                            valor = reciboOriginal.LiquidaGAT;

                        TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle recibo = CalculoLiquidacionCondonacion.DetalleLiquidacion.Where(r => r.Recibo == Convert.ToInt32(row.Cells[0].Text)).FirstOrDefault();
                        if (recibo != null)
                            recibo.LiquidaGAT = decimal.Round(valor * multiplicador, 2, MidpointRounding.AwayFromZero); ;
                        valor = valor - decimal.Round(valor * multiplicador, 2, MidpointRounding.AwayFromZero); ;
                        ctrl.Text = valor.ToString();
                    }
                    continue;
                }
            }
        }
    }

    protected void gvRecibos_Liquidacion_Totales_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            for (int colIndex = 0; colIndex < e.Row.Cells.Count; colIndex++)
            {
                e.Row.Cells[colIndex].Attributes.Add("style", "text-align: center;");
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                cell.Attributes.CssStyle["text-align"] = "center";
                cell.Attributes.CssStyle["vertical-align"] = "middle";
            }
            switch (e.Row.Cells[1].Text)
            {
                case "Original":
                    for (int colIndex = 2; colIndex < e.Row.Cells.Count; colIndex++)
                    {
                        e.Row.Cells[colIndex].Attributes.Add("title", "100%");
                    }
                    break;
                case "Condonar":
                    e.Row.CssClass = "info";
                    break;
                case "Liquidar":
                    e.Row.Cells[e.Row.Cells.Count - 1].CssClass = "warning";
                    e.Row.Cells[e.Row.Cells.Count - 1].Attributes.Add("style", "font-weight: bold; color: red;");
                    break;
                default:
                    break;
            }
        }
    }

    protected void CalcularColumnaTotalPagar()
    {
        if (ViewState["RecibosLiquidacionCondonado"] != null)
        {
            TBCreditos.CalculoLiquidacion CalculoLiquidacion = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacion"];
            TBCreditos.CalculoLiquidacion CalculoLiquidacionCondonacion = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacionCondonado"];
            foreach (TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle rec in CalculoLiquidacionCondonacion.DetalleLiquidacion)
            {
                rec.LiquidaTotal = rec.LiquidaCapital + rec.LiquidaInteres + rec.LiquidaIGV + rec.LiquidaSeguro + rec.LiquidaGAT;
            }
            foreach (GridViewRow row in gvRecibos_Liquidacion.Rows)
            {
                var rec = CalculoLiquidacion.DetalleLiquidacion.Where(r => r.Recibo == Convert.ToInt32(row.Cells[0].Text)).FirstOrDefault();
                var recCond = CalculoLiquidacionCondonacion.DetalleLiquidacion.Where(r => r.Recibo == Convert.ToInt32(row.Cells[0].Text)).FirstOrDefault();
                if (rec != null && recCond != null)
                {
                    row.Cells[7].Text = (rec.LiquidaTotal - recCond.LiquidaTotal).ToString("C", CultureInfo.CurrentCulture);
                    row.Cells[7].ToolTip = "Original: " + rec.LiquidaTotal.ToString("C", CultureInfo.CurrentCulture);
                }
            }
        }
    }

    private void CompararPorcentajeColumnaConTotal(string valor, string txt)
    {
        TextBox ctrl = this.Master.FindControl("MainContent").FindControl(txt) as TextBox;
        if (ctrl != null)
        {
            string cadena = string.IsNullOrWhiteSpace(ctrl.Text) ? "0" : ctrl.Text;
            if (!(cadena.Replace(".00", "")).Equals(valor.Replace(".00", "")))
            {
                ctrl.BackColor = System.Drawing.Color.LightYellow;
                ctrl.ToolTip += ctrl.ToolTip.IndexOf(STR_PORCENTAJES_DIFERENTES) > 0 ? "" : STR_PORCENTAJES_DIFERENTES;
            }
            else
            {
                ctrl.BackColor = System.Drawing.Color.White;
                ctrl.ToolTip = ctrl.ToolTip.Replace(STR_PORCENTAJES_DIFERENTES, "");
            }
        }
    }

    private void CalcularSimulacion()
    {
        try
        {
            string[] pFecha = txtFechaPeticion.Text.Split('/');
            string fechaSimula = string.Concat(pFecha[2], pFecha[1], pFecha[0]);
            using (CreditoClient ws = new CreditoClient())
            {
                ResultadoOfObtenerSimulacionCreditoResponseK9cGEP0x res = ws.ObtenerSimulacionCredito(new ObtenerSimulacionCreditoRequest()
                {
                    IdSolicitud = SolicitudOperacion,
                    IdTipoReestructura = Convert.ToInt32(cboTipoReestructura.SelectedValue),
                    Importe = TotalReestructurar,
                    Plazo = Convert.ToInt32(cboNuevoPlazo.SelectedItem.Text),
                    FechaDesembolso = (PeticionReestructuraOperacion > 0 ? fechaSimula : "")
                });
                if (res.Codigo > 0)
                {
                    throw new Exception(res.Mensaje);
                }
                if (res != null && res.ResultObject != null && res.ResultObject.Resultado != null
                    && res.ResultObject.Resultado.Length > 0)
                {
                    bool existeTotal = res.ResultObject.Resultado.Where(r => r.NoRecibo == "TOTAL").FirstOrDefault() != null;
                    gvRecibosNuevos.Columns.Cast<DataControlField>().Where(fld => (fld.HeaderText == "Seguro"))
                        .SingleOrDefault().Visible = (res.ResultObject.Resultado.Sum(x => x.Seguro) > 0);
                    gvRecibosNuevos.DataSource = res.ResultObject.Resultado;
                    gvRecibosNuevos.DataBind();
                    if (existeTotal)
                    {
                        GridViewRow tot = gvRecibosNuevos.Rows[gvRecibosNuevos.Rows.Count - 1];
                        tot.Cells[1].Text = "";
                        tot.Cells[2].Text = "";
                        tot.Cells[8].Text = "";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblError.Text = "Simulación de resultado: " + ex.Message;
            pnlError.Visible = true;
        }
    }

    protected void cboNuevoPlazo_SelectedIndexChanged(object sender, EventArgs e)
    {
        CalcularLiquidacion(sender, e);
    }

    protected void cboTipoReestructura_SelectedIndexChanged(object sender, EventArgs e)
    {
        CalcularLiquidacion(sender, e);
    }
    #endregion

    protected void btnGuardarClick(object sender, EventArgs e)
    {
        pnlError.Visible = false;

        if (Page.IsValid)
        {
            try
            {
                int IdCliente = 0;
                int idSolicitud = 0;

                if (SolicitudOperacion > 0)
                {
                    idSolicitud = SolicitudOperacion;
                    using (wsSOPF.ClientesClient wsCliente = new ClientesClient())
                    {
                        TBClientes[] res = wsCliente.ObtenerTBClientes(3, idSolicitud);
                        if (res != null)
                            IdCliente = res.Length > 0 ? Convert.ToInt32(res[0].IdCliente) : 0;
                    }
                }

                PeticionReestructura peticion = new PeticionReestructura();
                peticion.Solicitud_Id = idSolicitud;
                peticion.TipoReestructura_Id = Convert.ToInt32(cboTipoReestructura.SelectedValue);
                peticion.MotivoPeticionReestructura_Id = Convert.ToInt32(cboMotivo.SelectedValue);
                peticion.Pagos = Convert.ToInt32(cboNuevoPlazo.SelectedItem.Text);
                peticion.CapturistaUsuario_Id = Convert.ToInt32(Session["UsuarioId"]);
                peticion.CapturistaUsuario_Id = Convert.ToInt32(Session["UsuarioId"]);
                peticion.Id_PeticionReestructura = PeticionReestructuraOperacion;

                FotoPeticionReestructura foto = new FotoPeticionReestructura();
                foto.PeticionReestructura_Id = PeticionReestructuraOperacion;

                TBCreditos.CalculoLiquidacion rl = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacion"];
                TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle[] rlDet = rl.DetalleLiquidacion;
                TBCreditos.CalculoLiquidacion rlc = (TBCreditos.CalculoLiquidacion)ViewState["RecibosLiquidacionCondonado"];
                TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle[] rlcDet = rlc.DetalleLiquidacion;

                foto.fch_Credito = rl.FechaCredito;
                foto.erogacion = rl.Cuota;
                foto.NumeroDocumento = rl.DNICliente;
                foto.Cliente = string.Format("{0} {1} {2}", rl.NombreCliente, rl.ApPaternoCliente, rl.ApMaternoCliente);
                foto.Direccion = rl.DireccionCliente;
                foto.capital = rl.MontoCredito;
                DateTime FechaRegistro = DateTime.Today;
                DateTime.TryParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null, DateTimeStyles.None, out FechaRegistro);
                foto.FechaRegistro = FechaRegistro;
                foto.FechaPrimerPago = rl.FechaPrimerPago;
                foto.TotalLiquidacion = rl.TotalLiquidacion;

                List<FotoPeticionReestructura.FotoReciboPeticionReestructura> FotoReciboPeticionReestructuras = new List<FotoPeticionReestructura.FotoReciboPeticionReestructura>();

                foreach (TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle r in rlDet)
                {
                    FotoPeticionReestructura.FotoReciboPeticionReestructura rec = new FotoPeticionReestructura.FotoReciboPeticionReestructura()
                    {
                        Recibo = r.Recibo,
                        FotoTipoReciboPetRes_Id = 1,
                        TipoRecibo = r.TipoReciboCalculo,
                        Fch_Recibo = r.FechaRecibo,
                        Liquida_Capital = r.LiquidaCapital,
                        Liquida_Interes = r.LiquidaInteres,
                        Liquida_IGV = r.LiquidaIGV,
                        Liquida_Seguro = r.LiquidaSeguro,
                        Liquida_GAT = r.LiquidaGAT
                    };
                    FotoReciboPeticionReestructuras.Add(rec);
                }

                foreach (TBCreditos.CalculoLiquidacion.CalculoLiquidacionDetalle r in rlcDet)
                {
                    FotoPeticionReestructura.FotoReciboPeticionReestructura rec = new FotoPeticionReestructura.FotoReciboPeticionReestructura()
                    {
                        Recibo = r.Recibo,
                        FotoTipoReciboPetRes_Id = 2,
                        TipoRecibo = r.TipoReciboCalculo,
                        Fch_Recibo = r.FechaRecibo,
                        Liquida_Capital = r.LiquidaCapital,
                        Liquida_Interes = r.LiquidaInteres,
                        Liquida_IGV = r.LiquidaIGV,
                        Liquida_Seguro = r.LiquidaSeguro,
                        Liquida_GAT = r.LiquidaGAT
                    };
                    FotoReciboPeticionReestructuras.Add(rec);
                }

                foto.FotoReciboPeticionReestructuras = FotoReciboPeticionReestructuras.ToArray();

                peticion.FotoPeticionReestructura = foto;

                using (wsSOPF.GestionClient wsCliente = new wsSOPF.GestionClient())
                {
                    ResultadoOfboolean resultado = new ResultadoOfboolean();
                    resultado = wsCliente.InsertarPeticionReestructura("ALTA_PETICION_REESTRUCTURA", new InsertarPeticionReestructuraRequest()
                    {
                        peticion = peticion,
                        ComentarioCapturista = txtComentariosCapturista.Text.Trim()
                    });
                    if (resultado.CodigoStr == "0")
                    {
                        throw new Exception(resultado.Mensaje ?? "Error interno.");
                    }
                    else
                    {
                        int petres = 0;
                        if (int.TryParse(resultado.CodigoStr, out petres))
                            PeticionReestructuraOperacion = petres;
                        if (petres > 0)
                        {
                            pnlHistorialPagos.Enabled = false;
                            upHistorialPagos.Update();
                            pnlTablaRecibos.Enabled = false;
                            upTablaRecibos.Update();
                            pnlDatosComentarios.Enabled = false;
                            upDatosComentarios.Update();
                            txtBuscarSolicitud.Text = idSolicitud.ToString();
                            btnBuscarCliente_Click(sender, e);
                            upBusquedaCliente.Update();
                            TextBox txtInteresPorcentaje = this.Master.FindControl("MainContent").FindControl("txtInteresPorcentaje") as TextBox;
                            txtInteresPorcentaje.Text = txtInteresPorcentaje == null ? "(null)" : "";
                            TextBox txtIGVPorcentaje = this.Master.FindControl("MainContent").FindControl("txtIGVPorcentaje") as TextBox;
                            txtIGVPorcentaje.Text = txtIGVPorcentaje == null ? "(null)" : "";
                            TextBox txtSeguroPorcentaje = this.Master.FindControl("MainContent").FindControl("txtSeguroPorcentaje") as TextBox;
                            txtSeguroPorcentaje.Text = txtSeguroPorcentaje == null ? "(null)" : "";
                            TextBox txtGATPorcentaje = this.Master.FindControl("MainContent").FindControl("txtGATPorcentaje") as TextBox;
                            txtGATPorcentaje.Text = txtGATPorcentaje == null ? "(null)" : "";
                            ScriptManager.RegisterStartupScript(this, typeof(string), "Accordion", "$(function(){ alert('Petición de reestructurada generada correctamente.'); $('#divAccordion').accordion({active: 0}); });", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                pnlError.Visible = true;
            }
        }
    }

    protected void btnAutorizarClick(object sender, EventArgs e)
    {
        try
        {
            if (PeticionReestructuraOperacion > 0)
            {
                using (wsSOPF.GestionClient ws = new GestionClient())
                {
                    AutorizarPeticionReestructuraRequest entity = new AutorizarPeticionReestructuraRequest()
                    {
                        Accion = "AUTORIZAR_PETICION_REESTRUCTURA_POR_ID",
                        Id_PeticionReestructura = PeticionReestructuraOperacion,
                        AutorizadorUsuario_Id = HttpContext.Current.ValidarUsuario(),
                        ComentarioAutorizador = txtComentariosAutorizador.Text.Trim()
                    };
                    ResultadoOfboolean res = ws.AutorizarPeticionReestructura(entity);
                    if (res != null)
                    {
                        if (res.Codigo > 0)
                            throw new Exception(res.CodigoStr);
                        btnBuscarCliente_Click(sender, e);
                        upBusquedaCliente.Update();
                        TextBox txtInteresPorcentaje = this.Master.FindControl("MainContent").FindControl("txtInteresPorcentaje") as TextBox;
                        txtInteresPorcentaje.Text = txtInteresPorcentaje == null ? "(null)" : "";
                        TextBox txtIGVPorcentaje = this.Master.FindControl("MainContent").FindControl("txtIGVPorcentaje") as TextBox;
                        txtIGVPorcentaje.Text = txtIGVPorcentaje == null ? "(null)" : "";
                        TextBox txtSeguroPorcentaje = this.Master.FindControl("MainContent").FindControl("txtSeguroPorcentaje") as TextBox;
                        txtSeguroPorcentaje.Text = txtSeguroPorcentaje == null ? "(null)" : "";
                        TextBox txtGATPorcentaje = this.Master.FindControl("MainContent").FindControl("txtGATPorcentaje") as TextBox;
                        txtGATPorcentaje.Text = txtGATPorcentaje == null ? "(null)" : "";
                        txtComentariosCapturista.Text = "";
                        txtComentariosAutorizador.Text = "";
                        ValidarPermisoAdmin();
                        ScriptManager.RegisterStartupScript(this, typeof(string), "Alerta", "alert('Petición autorizada. Nueva solicitud: " + res.Mensaje.Replace("'", " ") + "'); $('#divAccordion').accordion({active: 0});", true);

                        int idSolicitudNueva;
                        if (int.TryParse(res.Mensaje, out idSolicitudNueva) && idSolicitudNueva > 0)
                        {
                            using (wsSOPF.SolicitudClient wsS = new SolicitudClient())
                            {
                                wsS.AltaSolicitudProcesoDigital(new NuevoSolicitudProcesoDigitalRequest
                                {
                                    IdSolicitud = idSolicitudNueva,
                                    Proceso = Proceso.PeticionReestructura,
                                    IdUsuario = entity.AutorizadorUsuario_Id
                                });

                                wsS.IniciarProcesoPeticionReestructura(new InicarSolicitudProcesoDigitalRequest
                                {
                                    IdUsuario = entity.AutorizadorUsuario_Id,
                                    IdSolicitud = idSolicitudNueva
                                });
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, typeof(string), "Alerta", "alert('Ocurrió un error no controlado: " + ex.Message.Replace("'", " ") + ".'); $('#divAccordion').accordion({active: 3}); });", true);
            lblError.Text = ex.Message;
            pnlError.Visible = true;
        }
    }

    protected void btnRechazarClick(object sender, EventArgs e)
    {
        try
        {
            if (PeticionReestructuraOperacion > 0)
            {
                using (wsSOPF.GestionClient ws = new GestionClient())
                {
                    RechazarPeticionReestructuraRequest entity = new RechazarPeticionReestructuraRequest()
                    {
                        Accion = "RECHAZAR_PETICION_REESTRUCTURA_POR_ID",
                        Id_PeticionReestructura = PeticionReestructuraOperacion,
                        AutorizadorUsuario_Id = HttpContext.Current.ValidarUsuario(),
                        ComentarioAutorizador = txtComentariosAutorizador.Text.Trim()
                    };
                    ResultadoOfboolean res = ws.RechazarPeticionReestructura(entity);
                    if (res != null)
                    {
                        if (res.Codigo > 0)
                            throw new Exception(res.CodigoStr);
                        btnBuscarCliente_Click(sender, e);
                        upBusquedaCliente.Update();
                        TextBox txtInteresPorcentaje = this.Master.FindControl("MainContent").FindControl("txtInteresPorcentaje") as TextBox;
                        txtInteresPorcentaje.Text = txtInteresPorcentaje == null ? "(null)" : "";
                        TextBox txtIGVPorcentaje = this.Master.FindControl("MainContent").FindControl("txtIGVPorcentaje") as TextBox;
                        txtIGVPorcentaje.Text = txtIGVPorcentaje == null ? "(null)" : "";
                        TextBox txtSeguroPorcentaje = this.Master.FindControl("MainContent").FindControl("txtSeguroPorcentaje") as TextBox;
                        txtSeguroPorcentaje.Text = txtSeguroPorcentaje == null ? "(null)" : "";
                        TextBox txtGATPorcentaje = this.Master.FindControl("MainContent").FindControl("txtGATPorcentaje") as TextBox;
                        txtGATPorcentaje.Text = txtGATPorcentaje == null ? "(null)" : "";
                        txtComentariosCapturista.Text = "";
                        txtComentariosAutorizador.Text = "";
                        ValidarPermisoAdmin();
                        ScriptManager.RegisterStartupScript(this, typeof(string), "Alerta", "alert('Petición rechazada exitosamente.'); $('#divAccordion').accordion({active: 0});", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, typeof(string), "Alerta", "alert('Ocurrió un error no controlado: " + ex.Message.Replace("'", "") + ".'); $('#divAccordion').accordion({active: 3});", true);
            lblError.Text = ex.Message;
            pnlError.Visible = true;
        }
    }

    #region Metodos
    private string CalcularPorcentaje(decimal total, decimal parte)
    {
        if (total > 0)
            return decimal.Round((parte * 100) / total, 2, MidpointRounding.AwayFromZero) > 0 ?
               decimal.Round((parte * 100) / total, 2, MidpointRounding.AwayFromZero).ToString("0.00")
               : "0.00";
        else
            return "0.00";
    }

    private string FormatearCargaDatosPeticion(object ctrl, string valorActual, string valorPeticion)
    {
        string tipo = ctrl.GetType().ToString();
        switch (tipo)
        {
            case "System.Web.UI.WebControls.Label":
                Label lbl = ctrl as Label;
                if (!valorActual.Equals(valorPeticion))
                {
                    lbl.BackColor = System.Drawing.Color.LightYellow;
                    lbl.ToolTip = (lbl.ToolTip.IndexOf(STR_VALORES_DIFERENTES) > 0 ? lbl.ToolTip : STR_VALORES_DIFERENTES + valorActual);
                    return valorPeticion;
                }
                return valorActual;
            case "System.Web.UI.WebControls.TextBox":
                TextBox txt = ctrl as TextBox;
                if (!valorActual.Equals(valorPeticion))
                {
                    txt.BackColor = System.Drawing.Color.LightYellow;
                    txt.ToolTip = (txt.ToolTip.IndexOf(STR_VALORES_DIFERENTES) > 0 ? txt.ToolTip : STR_VALORES_DIFERENTES + valorActual);
                    return valorPeticion;
                }
                return valorActual;
            case "System.Web.UI.WebControls.DropDownList":
                DropDownList ddl = ctrl as DropDownList;
                if (!valorActual.Equals(valorPeticion))
                {
                    ddl.BackColor = System.Drawing.Color.LightYellow;
                    if (ddl.ID.IndexOf("Colonia") > 0)
                    {
                        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
                        {
                            TBCATColonia[] colonias = null;
                            colonias = wsCatalogo.ObtenerTbCatColoniaId(3, 0, Convert.ToInt32(valorActual));
                            valorActual = colonias.FirstOrDefault().Colonia;
                            ddl.ToolTip = (ddl.ToolTip.IndexOf(STR_VALORES_DIFERENTES) > 0 ? ddl.ToolTip : STR_VALORES_DIFERENTES + valorActual);
                        }
                    }
                    else
                    {
                        ddl.ToolTip = (ddl.ToolTip.IndexOf(STR_VALORES_DIFERENTES) > 0 ? ddl.ToolTip : STR_VALORES_DIFERENTES + ddl.Items.FindByValue(valorActual).Text);
                    }
                    return valorPeticion;
                }
                return valorActual;
            default:
                return valorActual;
        }
    }

    private bool ValidarPermisoAdmin()
    {
        bool esAdmin = false;
        bool peticionFinalizada = false;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario > 0)
            {
                if (PeticionReestructuraOperacion > 0)
                {
                    PeticionReestructura peticion = null;
                    using (wsSOPF.GestionClient wsSolicitud = new GestionClient())
                    {
                        PeticionReestructura entity = new PeticionReestructura()
                        {
                            Id_PeticionReestructura = PeticionReestructuraOperacion
                        };
                        ResultadoOfArrayOfPeticionReestructuraXs2b9qnq peticionRes = wsSolicitud.ObtenerPeticionReestructura("CONSULTAR_FOTO_PETICION_REESTRUCTURA_POR_ID", entity);
                        if (peticionRes != null)
                            peticion = peticionRes.ResultObject.FirstOrDefault();
                        if (peticionRes.Codigo != 1)
                            throw new Exception(peticionRes.Mensaje);
                        peticionFinalizada = EstatusPeticionFinalizada.Contains(peticion.EstatusPeticionReestructuras_Id);
                        pnlAdmin.Enabled = !peticionFinalizada;
                        upAdmin.Update();
                    }
                }
                else
                {
                    pnlAdmin.Enabled = false;
                    upAdmin.Update();
                }
                using (UsuarioClient wsUsuarios = new UsuarioClient())
                {
                    esAdmin = wsUsuarios.EsAdministrador(0, idUsuario);
                    pnlAdmin.Visible = esAdmin;
                    upAdmin.Update();
                    ValorMaximoPorcentaje = esAdmin ? 100 : 30;
                    gvRecibos_Liquidacion.Enabled = esAdmin;
                    upTablaRecibos.Update();
                }
            }
        }
        catch (Exception ex)
        {
            ValorMaximoPorcentaje = 30;
            esAdmin = false;
            gvRecibos_Liquidacion.Enabled = false;
            upTablaRecibos.Update();
            throw ex;
        }
        return esAdmin;
    }

    private void ResetearFormato(object ctrl)
    {
        string tipo = ctrl.GetType().ToString();
        switch (tipo)
        {
            case "System.Web.UI.WebControls.Label":
                Label lbl = ctrl as Label;
                lbl.BackColor = System.Drawing.Color.Empty;
                lbl.ToolTip = "";
                break;
            case "System.Web.UI.WebControls.TextBox":
                TextBox txt = ctrl as TextBox;
                txt.BackColor = System.Drawing.Color.Empty;
                txt.ToolTip = "";
                break;
            default:
                break;
        }
    }
    #endregion
}
