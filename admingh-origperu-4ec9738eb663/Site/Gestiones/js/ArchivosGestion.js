﻿const formatosVisualizar = ["application/pdf", "image/jpeg", "image/png"];
let configuracionArchivsosGestion = {
    peticiones: {
        obtenerLista: {
            url: "frmGestiones.aspx/ObtenerArchivosGestionSolicitud",
            urlPeticion: function (urlParams) {
                return this.url + "?" + urlParams;
            }
        },
        descargarArchivo: {
            url: "frmGestiones.aspx/DescargarArchivo",
            contentType: "application/json",
            async: false,
            dataType: "json",
            method: "POST"
        },
        visualizarArchivo: {
            url: "frmGestiones.aspx",
            urlPeticion: function (urlParams) {
                return this.url + "?" + urlParams;
            }
        },
        eliminarArchivo: {
            url: "frmGestiones.aspx/EliminarArchivoGestion",
            contentType: "application/json",
            dataType: "json",
            method: "POST"
        }
    },
    contenedorArchivos: "secListaArchivosGestion",
    modalSubirArchivo: "secSubirArchivoGestion",
    modalPreview: "secVisualizarArchivo",
    contenedorPreview: "dPreview",
    btnAgregar: "btnAgregarArchivoGestion",
    btnSubir: "btnSubirArchivoGestion",
    btnActualizarLista: "btnActualizarListaArchivosGestion",
    camposFormularioSubida: {
        tipoArchivo: "seltipoMimeArchivoGestion",
        tipoDocumento: "selDocumentoDestino",
        inputArchivo: "fArchivoGestion",
        comentario: "txtComentarioArchivoGestion"
    }
};

let _archivosGestion = {
    initializarCampos: function () {
        $("#" + configuracionArchivsosGestion.btnAgregar).bind("click", onClickBtnAgregarArchivoGestion);
        $("#" + configuracionArchivsosGestion.btnSubir).bind("click", onClickBtnSubirArchivoGestion);
        $("#" + configuracionArchivsosGestion.btnActualizarLista).bind("click", onClickBtnActualizarListaArchivosGestion);
        $("#" + configuracionArchivsosGestion.camposFormularioSubida.tipoArchivo).bind("change", onChangeSeltipoMimeArchivoGestion);

        if (tiposArchivo) poblarControlSeleccion("#" + configuracionArchivsosGestion.camposFormularioSubida.tipoArchivo, tiposArchivo, 'TipoArchivo', 'IdTipoArchivoGestion');
        if (documentosDestino) poblarControlSeleccion("#" + configuracionArchivsosGestion.camposFormularioSubida.tipoDocumento, documentosDestino, 'Descripcion', 'IdDocumentoDestino');
    }
};

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) return false;
    }
    return true;
}

function llenarListaArchivosGestion() {
    let secListaArchivosGestion = $("#" + configuracionArchivsosGestion.contenedorArchivos);
    secListaArchivosGestion.html("");
    if (!isEmpty(archivosGestion)) {
        $.each(archivosGestion, function (i, valor) {
            let tempFechaArchivo = getDateFromAspNetFormat(valor.FechaRegistro);
            tempFechaArchivo = (tempFechaArchivo) ? new Date(tempFechaArchivo) : new Date(valor.FechaRegistro);
            if (tempFechaArchivo) tempFechaArchivo = tempFechaArchivo.toLocaleString();
            let tempIcono = valor.Icono ? valor.Icono : 'fa-file';
            secListaArchivosGestion.append('<div class="file-manager-file" data-toggle="tooltip" data-placement="top" title="' + valor.NombreDocumento + '\n' + tempFechaArchivo + ": " + valor.Comentario + '" data-mime-type="' + valor.TipoMime + '" data-id="' + valor.IdArchivoGestion + '" >' +
                '<i class="fas ' + tempIcono + '"></i>' +
                '<span>' + valor.Nombre + '</span>' +
                '</div>');
        });
        $('[data-toggle="tooltip"]').tooltip();
    }
}
function actualziarListaArchivosGestion() {
    let parametros = {
        url: configuracionArchivsosGestion.peticiones.obtenerLista.urlPeticion("idSolicitud=" + idSolicitud),
        contentType: "application/json",
        async: false,
        method: 'GET'
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d != null) {
                archivosGestion = data.d;
                llenarListaArchivosGestion();
            }
        })
        .fail(function (err) {
        });

}

function onClickBtnSubirArchivoGestion() {
    obtenerContenidoArchivo()
        .then(archivo => {
            let qStr = "?" + window.location.href.split("?")[1];
            let comentario = $("#" + configuracionArchivsosGestion.camposFormularioSubida.comentario).val();
            let seleccionDocumentoDestino = $("#" + configuracionArchivsosGestion.camposFormularioSubida.tipoDocumento + " option:selected").val();
            let IdTipoArchivo = 0;

            if (seleccionDocumentoDestino == null || seleccionDocumentoDestino == undefined || seleccionDocumentoDestino == 0) {
                alert("Seleccione un tipo de documento");
                return;
            }
            if (archivo == null || archivo == undefined) {
                alert("Seleccione un archivo para subir.");
                return;
            } else {
                let tempExtValidas = [];
                $.each(tiposArchivo, function (i, val) {
                    let tempExtensiones = val.Extension.split(",");

                    $.each(tempExtensiones, function (x, ext) {
                        tempExtValidas.push(ext);
                    })
                });

                let matchExtension = archivo.NombreArchivo.match(/.[a-zA-Z0-9]+$/g);
                if (matchExtension.length > 0) {
                    let tempExtensionArchivo = matchExtension[0].toString();

                    $.each(tiposArchivo, function (i, val) {
                        let tempExtensiones = val.Extension.split(",");

                        $.each(tempExtensiones, function (x, ext) {
                            if (tempExtensionArchivo.toString().toLowerCase() == ext.toString().toLowerCase()) {
                                IdTipoArchivo = val.IdTipoArchivoGestion;
                            }
                        });
                    });

                    if (IdTipoArchivo <= 0) {
                        alert("El formato no es permitido o no fue posible detectarlo.");
                        return;
                    }
                }
            }
            if (archivo.Size > 15728640) {
                alert("El tamaño del archivo supera el límite permitido (15MB).");
                return;
            }
            if (comentario == null || comentario == undefined || comentario == "") {
                alert("Por favor llene un comentario para el archivo");
                return;
            }
            let peticion = {
                NombreArchivo: archivo.NombreArchivo,
                Comentario: comentario,
                IdSolicitud: idSolicitud,
                IdServidorArchivos: (servidoresArchivo) ? servidoresArchivo[0].IdServidorArchivos : 0,
                IdTipoArchivoGestion: IdTipoArchivo,
                IdDocumentoDestino: parseInt(seleccionDocumentoDestino),
                TipoArchivo: archivo.TipoArchivo,
                Contenido: archivo.Contenido
            };

            let parametros = {
                url: "frmGestiones.aspx/SubirArchivoGestion" + qStr,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                method: 'POST',
                data: JSON.stringify({ archivo: peticion })
            };

            $.ajax(parametros)
                .done(function (respuesta) {
                    let resp = respuesta.d;
                    if (resp) {
                        alert(resp.Mensaje);
                        if (resp.Codigo <= 0) {
                            console.log(resp.Codigo);
                        } else {
                            $("#" + configuracionArchivsosGestion.modalSubirArchivo).modal("hide");
                            limpiarCamposSubirArchivoGestion();
                            actualziarListaArchivosGestion();
                        }
                    } else {
                        alert("Ocurrió un error al momento de realizar la carga del archivo.");

                    }
                })
                .fail(function (error) {
                    alert("Ocurrió un error al momento intentar realizar carga del archivo. Vuela a intentar.");
                });
        });
}
function onClickBtnActualizarListaArchivosGestion() {
    actualziarListaArchivosGestion();
}
function onClickBtnAgregarArchivoGestion() {
    limpiarCamposSubirArchivoGestion();
    asignarArchivosPermitidos();
    $("#" + configuracionArchivsosGestion.modalSubirArchivo).modal("show");
    $("#" + configuracionArchivsosGestion.modalSubirArchivo).appendTo("body");
}
function onChangeSeltipoMimeArchivoGestion(e) {
    let tempSeleccionTipo = $("#" + configuracionArchivsosGestion.camposFormularioSubida.tipoArchivo + " option:selected").val();
    let fArchivoGestion = $("#" + configuracionArchivsosGestion.camposFormularioSubida.inputArchivo);
    let mimeIndex = tiposArchivo.indexOfAttribute("IdTipoArchivoGestion", parseInt(tempSeleccionTipo));
    if (mimeIndex >= 0) {
        let tipoArchivo = tiposArchivo[mimeIndex];
        fArchivoGestion.attr("accept", tipoArchivo.Extension);
    }
    else
        fArchivoGestion.attr("accept", "");
}


function limpiarCamposSubirArchivoGestion() {
    $("#" + configuracionArchivsosGestion.camposFormularioSubida.tipoArchivo).val(0);
    $("#" + configuracionArchivsosGestion.camposFormularioSubida.tipoDocumento).val(0);
    $("#" + configuracionArchivsosGestion.camposFormularioSubida.comentario).val("");
    $("#" + configuracionArchivsosGestion.camposFormularioSubida.inputArchivo).val("");
    $("#" + configuracionArchivsosGestion.camposFormularioSubida.inputArchivo).attr("accept", "");
}
function obtenerContenidoArchivo() {
    return new Promise((resolve, reject) => {
        var file = $("#" + configuracionArchivsosGestion.camposFormularioSubida.inputArchivo)[0].files[0];
        if (file) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (e) {
                let archivo = { NombreArchivo: file.name, TipoArchivo: file.type, Contenido: e.target.result.substr(e.target.result.indexOf(',') + 1), Size: file.size };
                resolve(archivo);
            };
            reader.onerror = function (e) {
                resolve(null);
            };
        }
    });
}
function asignarArchivosPermitidos() {
    if (!isEmpty(tiposArchivo)) {
        let fArchivoGestion = $("#" + configuracionArchivsosGestion.camposFormularioSubida.inputArchivo);
        let extensiones = "";
        $.each(tiposArchivo, function (i, val) {
            extensiones += val.Extension + ',';
        });
        $("#" + configuracionArchivsosGestion.camposFormularioSubida.inputArchivo).attr("accept", extensiones);
    }
}
function obtenerContenidoArchivoGestion(idArchivo, callback) {
    let parametros = {
        url: configuracionArchivsosGestion.peticiones.descargarArchivo.url,
        contentType: configuracionArchivsosGestion.peticiones.descargarArchivo.contentType,
        async: configuracionArchivsosGestion.peticiones.descargarArchivo.async,
        dataType: configuracionArchivsosGestion.peticiones.descargarArchivo.dataType,
        method: configuracionArchivsosGestion.peticiones.descargarArchivo.method,
        data: JSON.stringify({ idArchivo: idArchivo })
    };
    $.ajax(parametros)
        .done(function (data) {
            let respuestaDescarga = {};
            if (data.d != null) {
                respuestaDescarga = data.d;
                if (respuestaDescarga != null || respuestaDescarga != undefined) {
                    callback(respuestaDescarga);

                } else {
                    alert("Ocurrió un error en la petición.");
                }
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar enviar la petición.");
        });
}

function visualizarArchivoGestion(idArchivo, mimeType) {
    if (mimeType == "application/pdf") {
        window.open(configuracionArchivsosGestion.peticiones.visualizarArchivo.urlPeticion("visualizarArchivo=1&idArchivoGestion=" + idArchivo, "_ArchivoGestion", "width=1000,height=500,resizable=1,scrollbars=yes"));
        return false;
    }
    else {
        obtenerContenidoArchivoGestion(idArchivo, function (respuestaDescarga) {
            if (respuestaDescarga != null && respuestaDescarga != undefined && !isEmpty(respuestaDescarga)) {
                let dPreview = $("#" + configuracionArchivsosGestion.contenedorPreview);
                dPreview.html("");
                dPreview.append('<object class="preview-object" data="data:' + respuestaDescarga.Mime + ';base64,' + respuestaDescarga.Contenido + '"></object>');
                $("#" + configuracionArchivsosGestion.modalPreview).modal("show");
                $("#" + configuracionArchivsosGestion.modalPreview).appendTo("body");
            }
        });
    }
}
function descargarArchivoGestion(idArchivo) {
    obtenerContenidoArchivoGestion(idArchivo, function (respuestaDescarga) {
        console.log("respuesta de descarga");
        if (respuestaDescarga != null && respuestaDescarga != undefined && !isEmpty(respuestaDescarga)) {
            var blob;
            var byteString = atob(respuestaDescarga.Contenido);
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);

            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            blob = new Blob([ab], { type: respuestaDescarga.Mime });

            if (blob) {
                var objectUrl = URL.createObjectURL(blob);
                download(blob, respuestaDescarga.NombreArchivo, respuestaDescarga.Mime)
            }
        }
    });
}
function eliminarArchivoGestion(idArchivoGestion) {
    let tempIdArchivoGestion = (idArchivoGestion) ? idArchivoGestion : 0;
    let parametros = {
        url: configuracionArchivsosGestion.peticiones.eliminarArchivo.url,
        contentType: configuracionArchivsosGestion.peticiones.eliminarArchivo.contentType,
        dataType: configuracionArchivsosGestion.peticiones.eliminarArchivo.dataType,
        method: configuracionArchivsosGestion.peticiones.eliminarArchivo.method,
        data: JSON.stringify({ idArchivoGestion: tempIdArchivoGestion, idSolicitud: idSolicitud })
    };
    $.ajax(parametros)
        .done(function (data) {
            let respuestaeliminar = {};
            if (data.d != null) {
                respuestaeliminar = data.d;
                if (respuestaeliminar != null || respuestaeliminar != undefined) {
                    alert(respuestaeliminar.Mensaje);
                    actualziarListaArchivosGestion();
                } else {
                    alert("Ocurrió un error en la petición.");
                }
            }
        })
        .fail(function (err) {
        });
}
