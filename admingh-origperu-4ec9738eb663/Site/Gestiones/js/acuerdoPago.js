﻿var idSolicitud = 0;
var saldoVencido = 0.00;
var acuerdo = {};
var acuerdosPago = [];
var diaPago = 0;
var cuota = 0;
var flagPlanValido = true;
var flagCambios = false;
var flagNuevoPlan = false;
var flagAcuerdoValido = false;

const prefijoSoles = 'S/ ';

$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); $(".modal-pr").appendTo("body"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});

$(document).ready(function () {
    validarPlanAcuerdo();
    mostrarAcuerdosPago();
    inicializarCamposAlta();
});

//Misc Functions
function mostrarAcuerdosPago() {
    var table = $('#tAcuerdos').DataTable({
        dom: '<"toolbar">fBrtip',
        destroy: true,
        searching: false,
        ordering: true,
        scrollX: true,
        data: acuerdosPago,
        columns: [
            { data: "IdAcuerdoDetalle", title: "IdAcuerdo", orderable: false, searchable: false, visible: false },
            { data: null, title: "#", className: "dt-center", orderable: false, searchable: false },
            { data: "FechaAcuerdo", title: "Fecha" },
            { data: "MontoCuota", title: "Cuota", className: "dt-right dt-nowrap", orderable: false, searchable: false, render: $.fn.dataTable.render.number(',', '.', 2, prefijoSoles) },
            { data: "Monto", title: "Importe Acuerdo", className: "dt-right dt-nowrap", orderable: false, searchable: false, render: $.fn.dataTable.render.number(',', '.', 2, prefijoSoles) },
            { data: "GastoAdministrativo", title: "Gasto Admin", className: "dt-right dt-nowrap", orderable: false, searchable: false, render: $.fn.dataTable.render.number(',', '.', 2, prefijoSoles) },
            { data: "MontoTotal", title: "Total", className: "dt-right dt-nowrap", orderable: false, searchable: false, render: $.fn.dataTable.render.number(',', '.', 2, prefijoSoles) },
            { data: "IdEstatus", title: "IdEstatus", orderable: false, searchable: false, visible: false },
            {
                data: "IdEstatus", title: "Estatus Acuerdo", orderable: false, searchable: false, render: function (data, type, row) {
                    switch (data) {
                        case 0:
                            return "Incumplido";
                        case 1:
                            //Si el registro tiene fecha de envio a domiciliacion, asignar estatus de pendiente por cobrar
                            if (row && row.FechaEnvioDomiciliacion)
                                return '<span data-toggle="tooltip" title="Se ha intentado domiciliar el acuerdo. Esperando respuesta">Por Cobrar</span>';
                            return "Pendiente";
                        case 2:
                            return "Cumplido";
                        default:
                            return "No Identificado";
                    }
                }
            },
            {
                data: "IdEstatus", title: "Acciones", className: "dt-center", orderable: false, searchable: false, render: function (data, type, row, meta) {
                    var control = null;
                    if (type === 'display' && data === 1 && row && !row.FechaEnvioDomiciliacion) {
                        var disabled = (flagPlanValido) ? '' : 'disabled="disabled"';
                        control = '<div class="btn-group" role="group" aria-label="controles">' +
                            '<button type="button" class="btn btn-sm btn-danger" onclick="eliminarAcuerdo(' + meta.row + ')" ' + disabled + '><span class="glyphicon glyphicon-trash"></span></button></div>';
                    }

                    return control;
                }
            }
        ],
        columnDefs: [
            {
                targets: 2, render: function (data) {
                    if (data) {
                        return moment(data).format('YYYY/MM/DD');
                    }
                    else return null;
                }
            }],
        createdRow: function (row, data, index) {
            var estatus = data.IdEstatus;
            switch (estatus) {
                case 0:
                    $(row).css({ 'background-color': '#ffe7e8', 'color': '#bb0400' });
                    break;
                case 1:
                    //Si el registro tiene fecha de envio a domiciliacion, asignar color de pendiente por cobrar
                    if (data && data.FechaEnvioDomiciliacion)
                        $(row).css({ 'background-color': '#f0f4c3' });
                    break;
                case 2:
                    $(row).css({ 'background-color': '#c8e6c9' });
                    break;
            }
        },
        language: español,
        autoWidth: true,
        order: [[2, "asc"]]
    });
    table.on('order.dt search.dt', function () {
        table.column(1, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).columns.adjust().draw();


    $("div.toolbar").html(
        '<div class="col-md-12">' +
        '<div class="btn-toolbar" role="toolbar" aria-label="controles-toolbar">' +
        '<div class="btn-group" role="group" aria-label="controles">' +
        '<button id="btnCancelarPlan" class="btn btn-danger" style="display:none;" type="button">Cancelar plan actual</button>' +
        '</div>' +
        '<div class="btn-group" role="group" aria-label="controles" style="float: right;">' +
        '<button id="btnGuardarCambios" class="btn btn-success collapse in" style="display:none;" type="button">Guardar cambios</button>' +
        '<button id="btnCancelarCambios" class="btn collapse in" style="display:none;" type="button"><span><span class="fa fa-ban"></span></button>' +
        '</div>' +
        '</div>' +
        '</div>'
    );
    $("#btnCancelarPlan").bind("click", onClickBbtnCancelarPlan);
    $("#btnGuardarCambios").bind("click", onClickBtnGuardarCambios);
    $("#btnCancelarCambios").bind("click", onClickBtnCancelarCambios);

    flagCambios = false;
    toggleBtnGuardarCambios();
    toggleControlesPlan();
}
function inicializarCamposAlta() {
    flagAcuerdoValido = false;

    $("#txtMontoNuevoAcuerdo").bind("change", onChangeTxtMontoNuevoAcuerdo);
    $("#txtGastoAdminNuevoAcuerdo").bind("change", onChangeTxtGastoAdminNuevoAcuerdo);

    $("#btnGuardarAcuerdo").bind("click", onClickBtnGuardarAcuerdo);
    $("#btnCancelarAcuerdo").bind("click", onClickBtnCancelarAcuerdo);

    var fechaAcuerdo = $("#txtFechaNuevoAcuerdo");
    var configFechaAcuerdo = {
        startDate: moment().format("DD/MM/YYYY"),
        locale: 'es', format: 'dd/mm/yyyy'
    };

    if (diaPago) {
        configFechaAcuerdo = {
            ...configFechaAcuerdo, beforeShowDay: function (date) {
                var ultimoDiaMes = LastDayOfMonth(date.getFullYear(), date.getMonth());
                var diaPermitido = (diaPago > ultimoDiaMes) ? ultimoDiaMes : diaPago;
                if (date.getDate() == diaPermitido) {
                    return true;
                }
                return false;
            }
        };
    }
    fechaAcuerdo.datepicker(configFechaAcuerdo);
}
function limpiarCamposAlta() {
    $("#txtMontoNuevoAcuerdo").val("");
    $("#txtFechaNuevoAcuerdo").val("");
    $("#txtGastoAdminNuevoAcuerdo").val("");
    $("#txtTotalNuevoAcuerdo").val("");
    flagAcuerdoValido = false;
}
function calcularMontoTotal() {
    var txtMontoNuevoAcuerdo = $("#txtMontoNuevoAcuerdo").val();
    var txtGastoAdminNuevoAcuerdo = $("#txtGastoAdminNuevoAcuerdo").val();
    var montoAcuerdo = 0.00;
    var montoGastoAdmin = 0.00;
    var montoFinal = 0.00;

    if (txtMontoNuevoAcuerdo) {
        montoAcuerdo = parseFloat(txtMontoNuevoAcuerdo.replace(",", ""), 10);
        montoFinal += montoAcuerdo;
    }
    if (txtGastoAdminNuevoAcuerdo) {
        montoGastoAdmin = parseFloat(txtGastoAdminNuevoAcuerdo.replace(",", ""), 10);
        montoFinal += montoGastoAdmin;
    }

    $("#txtTotalNuevoAcuerdo").val(montoFinal);

    let inputs = $(".form-control.decimal-input").toArray();

    inputs.forEach(i => {
        let input = $("#" + i.id);
        let value = input.val(),
            //num = parseFloat(value.replace(",", "")).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            num = parseFloat(value.replace(",", "")).toFixed(2);
        input.val(num);
    });
}
function agregarAcuerdoDataTable(acuerdo) {
    var table = $("#tAcuerdos").DataTable();
    table.row.add({
        "IdAcuerdoDetalle": 0,
        "IdAcuerdoPago": 0,
        "FechaAcuerdo": acuerdo.FechaAcuerdo,
        "MontoCuota": cuota,
        "Monto": acuerdo.Monto,
        "GastoAdministrativo": acuerdo.GastoAdministrativo,
        "MontoTotal": acuerdo.MontoTotal,
        "IdEstatus": 1
    }).draw();
}
function toggleBtnGuardarCambios() {
    var btnGuardar = $("#btnGuardarCambios");
    var btnCancelar = $("#btnCancelarCambios");

    if (flagCambios && flagPlanValido) {
        if (btnGuardar.is(":hidden")) {
            btnGuardar.show("fast");
        }
        if (btnCancelar.is(":hidden")) {
            btnCancelar.show("fast");
        }
    } else {
        if (btnGuardar.is(":visible")) {
            btnGuardar.hide("fast");
        }
        if (btnCancelar.is(":visible")) {
            btnCancelar.hide("fast");
        }
    }
}
function toggleControlesPlan() {
    var btnCancelar = $("#btnCancelarPlan");
    var alertErrorPlan = $("#alertErrorPlan");

    //Validar si el plan actual tiene algun registro con estatus pendiente de respuesta domi
    //no dejar al usuario cancelar el plan hasta que se reciba la respuesta correspondiente
    var acuerdoEsperandoDomi = false;
    var BreakException = {};
    try {
        acuerdosPago.forEach(acuerdo => {
            if (acuerdo.IdEstatus === 1 && acuerdo.FechaEnvioDomiciliacion) {
                acuerdoEsperandoDomi = true;
                throw BreakException;
            }
        });
    } catch (e) {
        if (e !== BreakException) throw e;
        else return;
    }

    if (!isEmpty(acuerdo) && !acuerdoEsperandoDomi) {
        if (btnCancelar.is(":hidden")) {
            btnCancelar.show("fast");
        }
    }

    if (!flagPlanValido) {
        inhabilitarCapturaAcuerdo();
        alertErrorPlan.show("fast");
    } else {
        habilitarCapturaAcuerdo();
        alertErrorPlan.hide("fast");
    }
}
function validarPlanAcuerdo() {
    var BreakException = {};
    try {
        acuerdosPago.forEach(acuerdo => {
            if (acuerdo.IdEstatus == 0) {
                flagPlanValido = false;
                throw BreakException;
            }
        });
    } catch (e) {
        if (e !== BreakException) throw e;
        else return;
    }
}
function inhabilitarCapturaAcuerdo() {
    $("#txtMontoNuevoAcuerdo").prop("disabled", true);
    $("#txtFechaNuevoAcuerdo").prop("disabled", true);
    $("#txtGastoAdminNuevoAcuerdo").prop("disabled", true);
    $("#txtTotalNuevoAcuerdo").prop("disabled", true);
    $("#btnGuardarAcuerdo").prop("disabled", true);
    $("#btnCancelarAcuerdo").prop("disabled", true);
}
function habilitarCapturaAcuerdo() {
    $("#txtMontoNuevoAcuerdo").prop("disabled", false);
    $("#txtFechaNuevoAcuerdo").prop("disabled", false);
    $("#txtGastoAdminNuevoAcuerdo").prop("disabled", false);
    $("#txtTotalNuevoAcuerdo").prop("disabled", true);
    $("#btnGuardarAcuerdo").prop("disabled", false);
    $("#btnCancelarAcuerdo").prop("disabled", false);
}
function eliminarAcuerdo(row) {
    console.log(row);
    var table = $("#tAcuerdos").DataTable();
    if (row >= 0) {
        table.row(':eq(' + row + ')').remove();
        table.draw();
    }

    var acuerdosOriginales = 0;
    table.data().toArray().forEach(acuerdo => {
        if (acuerdo && acuerdo.IdAcuerdoDetalle > 0) {
            acuerdosOriginales++;
        }
    });

    if (acuerdosOriginales != acuerdosPago.length) {
        flagCambios = true;
    } else {
        flagCambios = false;
    }
    toggleBtnGuardarCambios();
}
function compararInformacion() {
    flagCambios = true;
}
function esDuplicado(nuevoAcuerdo) {
    var table = $("#tAcuerdos").DataTable();

    var BreakException = {};
    try {
        table.data().toArray().forEach(acuerdo => {
            if (acuerdo) {
                var fechaAcuerdo = new Date(acuerdo.FechaAcuerdo);
                if (acuerdo.FechaAcuerdo && nuevoAcuerdo.FechaAcuerdo.getTime() === fechaAcuerdo.getTime()) {
                    throw BreakException;
                }
            }
        });
    } catch (e) {
        if (e !== BreakException) throw e;
        else return true;
    }

    return false;
}
function validarAcuerdo() {
    var monto = 0.00, fecha, gastoAdmin = 0.00, total = 0.00;
    flagAcuerdoValido = false;

    var txtMontoNuevoAcuerdo = $("#txtMontoNuevoAcuerdo").val();
    var txtFechaNuevoAcuerdo = $("#txtFechaNuevoAcuerdo").val();
    var txtGastoAdminNuevoAcuerdo = $("#txtGastoAdminNuevoAcuerdo").val();
    var txtTotalNuevoAcuerdo = $("#txtTotalNuevoAcuerdo").val();

    if (!txtMontoNuevoAcuerdo || txtMontoNuevoAcuerdo == "") {
        alert("Por favor asigne un monto al acuerdo.");
        return;
    }
    if (!txtFechaNuevoAcuerdo || txtFechaNuevoAcuerdo == "") {
        alert("Por favor asigne una fecha al acuerdo.");
        return;
    }

    fecha = moment(txtFechaNuevoAcuerdo);

    if (txtMontoNuevoAcuerdo) {
        monto = parseFloat(txtMontoNuevoAcuerdo.replace(",", ""), 10);
    }
    if (txtGastoAdminNuevoAcuerdo) {
        gastoAdmin = parseFloat(txtGastoAdminNuevoAcuerdo.replace(",", ""), 10);
    }
    if (txtTotalNuevoAcuerdo) {
        total = parseFloat(txtTotalNuevoAcuerdo.replace(",", ""), 10);
    }
    if (fecha._d == "Invalid Date") {
        fecha = moment(txtFechaNuevoAcuerdo, "DD/MM/YYYY");
    }
    if (fecha._d == "Invalid Date") {
        alert("La fecha es invalida");
        return;
    }
    fecha = fecha._d;

    if (!fecha) {
        alert("La fecha no es valida");
        return;
    }
    if (!monto) {
        alert("El monto no es valido. Debe ser mayor a 0, con un máximo de 2 posiciones decimales");
        return;
    }
    else if (!total) {
        alert("Ocurrio un error al calcular el monto total. Por favor revise el  monto del acuerdo o el monto de gasto administrativo");
        return;
    }

    var acuerdo = {
        Monto: monto,
        FechaAcuerdo: fecha,
        GastoAdministrativo: gastoAdmin,
        MontoTotal: total
    };
    if (esDuplicado(acuerdo)) {
        //Si es necesario validar mas datos ademas de la fecha como duplicado, cambiar el mensaje.
        alert("No deben haber dos acuerdos con la misma fecha. Por favor, asigne una fecha diferente.");
        return;
    }

    flagCambios = true;
    flagAcuerdoValido = true;
    return acuerdo;
}

//Eventos
function onChangeTxtMontoNuevoAcuerdo() {
    //TODO: mostrar tooltip o mensaje si el monto es mayor a la cuota actual
    calcularMontoTotal();
}
function onChangeTxtGastoAdminNuevoAcuerdo() {
    calcularMontoTotal();
}
function onClickBtnGuardarAcuerdo() {
    var acuerdo = validarAcuerdo();
    if (flagAcuerdoValido) {
        agregarAcuerdoDataTable(acuerdo);
        toggleBtnGuardarCambios();
        limpiarCamposAlta();
    }
}
function onClickBtnCancelarAcuerdo() {
    limpiarCamposAlta();
}
function onClickBtnCancelarCambios() {
    //mostrarAcuerdosPago();
    //var table = $("#tAcuerdos").DataTable();
    //table.draw();
    location.reload();
}
function onClickBtnGuardarCambios() {
    let table = $("#tAcuerdos").DataTable();
    if (table.data().count() < 2) {
        alert("Error: Para registrar un plan de acuerdos, es necesario ingresar al menos 2 acuerdos.");
        return;
    }

    if (isEmpty(acuerdo)) {
        acuerdo = {
            IdSolicitud: idSolicitud
        };
        flagNuevoPlan = true;
    }

    let BreakException = {};
    let totalAcuerdos = 0.00;
    acuerdosPago = [];
    try {
        table.data().toArray().forEach(row => {
            var fecha = moment(row.FechaAcuerdo);

            if (fecha._d == "Invalid Date") {
                fecha = moment(txtFechaNuevoAcuerdo, "DD/MM/YYYY");
            }
            if (fecha._d == "Invalid Date") {
                alert("El formato de fecha de al menos un registro de acuerdo no es correcto.");
                throw BreakException;
            }

            let tempMontoAcuerdo = parseFloat(row.Monto);
            if (tempMontoAcuerdo)
                totalAcuerdos += tempMontoAcuerdo;

            acuerdosPago.push({
                IdAcuerdoPagoDetalle: row.IdAcuerdoDetalle,
                IdAcuerdoPago: row.IdAcuerdoPago,
                Monto: row.Monto,
                GastoAdministrativo: row.GastoAdministrativo,
                MontoTotal: row.MontoTotal,
                FechaAcuerdo: moment(row.FechaAcuerdo)._d
            });
        });

        if (totalAcuerdos > saldoVencido && !confirm("El total de los acuerdos no debe ser mayor al saldo vencido del cliente. ¿Desea realizar el acuerdo?")) {
            return;
        }

    } catch (e) {
        if (e !== BreakException) throw e;
        else return;
    }

    let parametros = {
        url: "AcuerdoPago.aspx/GuardarAcuerdosPago",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ acuerdo: acuerdo, detalle: acuerdosPago, esNuevo: flagNuevoPlan })
    };

    $.ajax(parametros)
        .done(function (data) {
            if (!isEmpty(data.d)) {
                var acuerdo = data.d;
                console.log(acuerdo);
                if (!isEmpty(acuerdo)) {
                    if (acuerdo.Error) {
                        alert(acuerdo.MensajeOperacion);
                    } else if (acuerdo.IdAcuerdoPago) {
                        location.reload();
                    }
                }
                else {
                    alert("Ocurrió un error no controlado en la operación.");
                }
            } else {
                alert("No fue posible guardar los acuerdos de pago. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            console.log(err);
            alert("Ocurrió un error al intentar guardar los acuerdos de pago. " + err.responseJSON.Message);
        });

}
function onClickBbtnCancelarPlan() {

    if (isEmpty(acuerdo) || acuerdo.IdAcuerdoPago <= 0 || !acuerdo.IdAcuerdoPago) {
        alert("El acuerdo actual no contiene informacion valida.");
        return;
    }

    let parametros = {
        url: "AcuerdoPago.aspx/CancelarPlanAcuerdoPago",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ idAcuerdoPago: acuerdo.IdAcuerdoPago, idSolicitud: idSolicitud })
    };

    $.ajax(parametros)
        .done(function (data) {
            if (!isEmpty(data.d)) {
                var respuesta = data.d;
                if (respuesta.Error) {
                    alert(respuesta.MensajeOperacion);
                } else {
                    location.reload();
                }
            } else {
                alert("No fue posible cancenlar el plan de los acuerdos de pago. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar cancelar el plan de los acuerdos de pago. " + err.responseJSON.Message);
        });
}
function onClickBtnRegresar() {
    $("body").addClass("loading"); $(".modal-pr").appendTo("body");
    if (idSolicitud) {
        window.location.href = "./Gestion.aspx?idsolicitud=" + idSolicitud;
    } else {
        window.history.back();
    }
}