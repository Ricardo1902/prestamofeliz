﻿var cargasControl = [];
var finalizado = false;
$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); $(".modal-pr").appendTo("body"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});
let dcont = new ResizeObserver((e) => {
    if ($.fn.dataTable.isDataTable("#tcargas")) { $("#tcargas").DataTable().columns.adjust(); }
});
$(document).ready(function () {
    cargas();
    dcont.observe($("#sidenav")[0]);
    $("#btnNuevo").click(nuevaAsignacion);
    $("#btnCargarArchivo").click(cargarAsignaciones);
    $("#nuevoAsig").on("hide.bs.modal", function () {
        if ($.fn.dataTable.isDataTable("#tCargaDetalle")) { $('#tCargaDetalle').DataTable().destroy(); $('#tCargaDetalle').empty(); }
        cargasControl = [];
        $("#nuevoAsig").removeData("value");
        $('#tcargas').DataTable().ajax.reload();
        $("#nombreArchivo").html("");
        $("#totalRegistros").html("");
        $("#estatus").html("");
        $("#selErrores").val("-1");
        $("#txActividadAsig").val("");
        $("#fCargarArchivo").val('');
    });
    $("#btnEliminarSel").click(eliminarSeleccionCargaDetalle);
    $("#btnAsignar").click(cargaAsignar);
    $("#btnGuardarActividadAs").click(actualizarActividad);
    $("#asigEdicion").on("hide.bs.modal", function () {
        $("#asigEdicion").removeData("value");
        $("#asigEdicion .modal-body").empty();
        $("#btnEditarAsig").off("click");
    });
});
function cargas() {
    let buscar = {};
    $('#tcargas').DataTable({
        destroy: true,
        searching: false,
        ordering: false,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "AdminAsignaciones.aspx/ObtenerCargas",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d,
                    datos: buscar
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d != undefined) {
                    if (json.d.url.length > 0)
                        window.location = json.d.url;
                    if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                }
                return JSON.stringify(json.d);
            }
        },
        columns: [
            { data: "IdCargaAsignacion", title: "IdArchivo" },
            {
                data: "NombreArchivo", title: "Archivo", render: function (data, type, row, meta) {
                    if (type === 'display') {
                        let html = `<a onclick="cargarAsignacion(${meta.row})">${data}</a>`;
                        return html;
                    } else { return ""; }
                }
            },
            { data: "TotalRegistros", title: "Total" },
            { data: "Fecha", title: "Fecha de Carga" },
            { data: "Estatus", title: "Estatus" }
        ],
        columnDefs: [
            { targets: 0, width: "60px" }
        ],
        createdRow: function (row, data, index) {
            if (data.Error) {
                $('td', row).addClass("carga-error");
            } else if (data.Asignar) {
                $('td', row).addClass("carga-asignar");
            }
        },
        language: español,
        order: [[0, "asc"]]
    });
}
function nuevaAsignacion() {
    finalizado = false;
    $("#dCargaArchivo").show();
    $("#nuevoAsig").appendTo('body');
    $("#nuevoAsig").css("z-index", 9999);
    $("#selErrores").val("-1");
    $("#nuevoAsig").modal('show', { keyboard: false });
    $("#btnGuardarActividadAs").prop("disabled", true);
}
function cargarAsignaciones() {
    new Promise((resolve, reject) => {
        var file = $("#fCargarArchivo")[0].files[0];
        if (file) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (e) {
                let archivo = { NombreArchivo: file.name, TipoArchivo: file.type, Contenido: e.target.result.substr(e.target.result.indexOf(',') + 1), Tamanio: file.size };
                if ($("#txActividadAsig").val().trim() != "") archivo.Comentario = $("#txActividadAsig").val().trim();
                resolve(archivo);
            }
            reader.onerror = function (e) {
                resolve(null);
            }
        } else { alert("Seleccione un archivo.") }
    })
        .then(archivo => {
            $("#fCargarArchivo").val('');
            return new Promise((resolve, reject) => {
                if (archivo != undefined && archivo != null && archivo.Tamanio > 0) {
                    let parametros = {
                        url: "AdminAsignaciones.aspx/CargarAsignaciones",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        method: 'POST',
                        data: JSON.stringify({ archivo: archivo })
                    };
                    $.ajax(parametros)
                        .done(function (respuesta) {
                            let resp = respuesta.d;
                            if (resp != undefined) {
                                if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                                if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                                resolve(resp.carga);
                            } else {
                                alert("Ocurrió un error al momento de realizar la carga del archivo.");
                                resolve();
                            }
                        })
                        .fail(function (error) {
                            alert("Ocurrió un error al momento intentar realizar carga del archivo. Vuela a intentar.");
                            resolve();
                        });
                } else { alert("No fue posible realizar la carga del archivo. Verifique el archivo no se encuentre dañando."); }
            });
        })
        .then(carga => {
            $("#nuevoAsig").data("value", carga.IdCargaAsignacion);
            cargaAsignacion(carga);
            detalleCarga();
        });
}
function cargarAsignacion(registro) {
    let carga = $("#tcargas").DataTable().rows().data()[registro];
    if (carga != undefined && carga != null) {
        $("#nuevoAsig").data("value", carga.IdCargaAsignacion);
        nuevaAsignacion();
        cargaAsignacion(carga);
        detalleCarga();
    }
}
function cargaAsignacion(carga) {
    if (carga != undefined && carga != null && carga.IdCargaAsignacion > 0) {
        $("#dCargaArchivo").hide();
        $("#nombreArchivo").html(carga.NombreArchivo);
        $("#totalRegistros").html(carga.TotalRegistros);
        $("#estatus").html(carga.Estatus);
        $("#txActividadAsig").val(carga.Actividad);
        $("#btnAsignar").prop("disabled", !carga.Asignar);
        $("#btnGuardarActividadAs").prop("disabled", carga.Finalizado);
        if (carga.Finalizado) {
            $("#btnAsignar").prop("disabled", true);
            $("#btnEliminarSel").prop("disabled", true);
            $("#chSelTodo").prop("disabled", true);
            finalizado = carga.Finalizado;
        }
    }
}
function detalleCarga() {
    let idCargaAsignacion = $("#nuevoAsig").data("value");
    if (idCargaAsignacion != undefined && idCargaAsignacion != null && idCargaAsignacion > 0) {
        let buscar = { IdCargaAsignacion: idCargaAsignacion };
        if ($("#selErrores").val() != -1) buscar.Error = $("#selErrores").val() == 0 ? false : true;
        $('#tCargaDetalle').DataTable({
            destroy: true,
            searching: false,
            ordering: false,
            serverSide: true,
            scrollX: true,
            ajax: {
                url: "AdminAsignaciones.aspx/CargaDetalle",
                type: 'POST',
                contentType: 'application/json',
                data: function (d) {
                    return JSON.stringify({
                        model: d,
                        datos: buscar
                    });
                },
                dataFilter: function (data) {
                    var json = jQuery.parseJSON(data);
                    if (json.d != undefined) {
                        if (json.d.url.length > 0)
                            window.location = json.d.url;
                        if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                    }
                    return JSON.stringify(json.d);
                }
            },
            columns: [
                {
                    data: "IdCargaAsignacionDetalle", title: function () {
                        return `<input type="checkbox" id="chSelTodo" onclick="seleccionTodo(this.checked)"/>&nbsp;Clave Carga`;
                    }, render: function (data, type, row, meta) {
                        if (type === 'display') {
                            return `<input type="checkbox" value="${data}" onclick="seleccionRegCarga(this.value, this.checked)" />&nbsp;${data}`;
                        } else { return ""; }
                    }
                },
                { data: "IdSolicitud", title: "Solicitud" },
                { data: "Gestor", title: "Gestor" },
                { data: "Actividad", title: "Motivo" },
                { data: "Mensaje", title: "Mensaje" },
                {
                    title: "&nbsp;", render: function (data, type, row, meta) {
                        if (type === 'display' && row.Editar) {
                            let html = `<dvi class="carga-acciones">`;
                            html += `<span class="glyphicon glyphicon-pencil" onclick="cargaAccion(${meta.row}, 1)"></span>`;
                            html += "</div>";
                            return html;
                        } else {
                            return "";
                        }
                    }
                }
            ],
            columnDefs: [
                { targets: 0, width: "100px" },
                { targets: 1, width: "90px" },
                { targets: 4, width: "200px" }
            ],
            createdRow: function (row, data, index) {
                if (data.Error) {
                    $('td', row).addClass("carga-error");
                }
                if ($("#chSelTodo").prop("checked")) $('td input[type=checkbox]', row).prop("checked", true).prop("disabled", true);
                if (cargasControl != null && cargasControl.length > 0) {
                    cargasControl.forEach((o, i) => {
                        if (data.IdCargaAsignacionDetalle == o.IdCargaAsignacionDetalle) $('td input[type=checkbox]', row).prop("checked", true);
                    });
                }
            },
            language: español,
            order: [[0, "asc"]]
        });
    }
}
function cargaAccion(reg, accion) {
    let carga = $("#tCargaDetalle").DataTable().rows().data()[reg];
    if (carga != undefined && carga != null && [0, 1].includes(accion)) {
        new Promise((resolve, reject) => {
            let parametros = {
                url: "AdminAsignaciones.aspx/Gestores",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                method: 'GET'
            };
            $.ajax(parametros)
                .done(function (respuesta) {
                    let resp = respuesta.d;
                    if (resp != undefined) {
                        if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                        if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                        resolve(resp.gestores);
                    } else {
                        resolve(null);
                    }
                })
                .fail(function (error) {
                    resolve(null);
                });
        })
            .then(gestores => {
                carga.accion = accion;
                $("#asigEdicion").css("z-index", 9999);
                $("#asigEdicion").appendTo("body").modal('show');
                $("#asigEdicion .modal-body").empty();
                $("#btnEditarAsig").off("click");
                $("#btnEditarAsig").data("value", carga.IdCargaAsignacionDetalle);
                let cont =
                    `<div class="row">
                    <div class="col-md-4 form-group">
                        <label for="txSol${carga.IdCargaAsignacionDetalle}">Solicitud</label><input type="text" id="txSol${carga.IdCargaAsignacionDetalle}" class="form-control" value="${carga.IdSolicitud}"/>
                    </div>
                    <div class="col-md-8 form-group">
                        <label for="selGes${carga.IdCargaAsignacionDetalle}">Gestor</label><select type="text" id="selGes${carga.IdCargaAsignacionDetalle}" class="form-control"></select>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="txAct${carga.IdCargaAsignacionDetalle}">Motivo Asignación</label><input type="text" id="txAct${carga.IdCargaAsignacionDetalle}" 
                                class="form-control" value="${carga.Actividad == null ? "" : carga.Actividad}"/>
                        </div>
                    </div>`;
                $("#asigEdicion .modal-body").append(cont);
                $("#btnEditarAsig").click(function () {
                    let peticion = {};
                    let idCarga = $("#nuevoAsig").data("value");
                    let idCargaDet = $("#btnEditarAsig").data("value");
                    if (idCarga > 0) { peticion.IdCargaAsignacion = idCarga };
                    peticion.Accion = 2;
                    peticion.Asignaciones = [
                        {
                            IdCargaAsignacionDetalle: ((idCargaDet > 0) ? idCargaDet : 0),
                            IdSolicitud: $(`#txSol${idCargaDet}`).val(),
                            IdGestor: $(`#selGes${idCargaDet}`).val(),
                            Actividad: $(`#txAct${idCargaDet}`).val()
                        }
                    ];
                    if (peticion != null && peticion.Accion >= 0 && peticion.IdCargaAsignacion > 0) {
                        editarCargaDetalle(peticion);
                    }
                });

                let selGest = $(`#selGes${carga.IdCargaAsignacionDetalle}`);
                $(selGest).empty();
                if (gestores != undefined && gestores != null && gestores.length > 0) {
                    gestores.forEach(function (gestor) {
                        $(selGest)
                            .append($('<option>', { value: gestor.IdGestor, selected: gestor.Usuario == carga.Gestor })
                                .text(gestor.Nombre));
                    });
                }
            });
    }
}
function seleccionTodo(seleccionado) {
    if (finalizado) return;
    $("#tCargaDetalle tr td input[type=checkbox]").map(function (i, o) {
        $(o).prop("checked", seleccionado).prop("disabled", seleccionado);
    });
    $("#btnEliminarSel").prop("disabled", !seleccionado);
    cargasControl = [];
}
function seleccionRegCarga(valor, seleccionado) {
    if (finalizado) return;
    if (cargasControl == undefined || cargasControl == null) cargasControl = [];
    let posicion = -1;
    cargasControl.map(function (ob, pos) {
        if (ob.IdCargaAsignacionDetalle == valor) {
            posicion = pos;
            return;
        }
    });
    if (seleccionado && posicion < 0) cargasControl.push({ IdCargaAsignacionDetalle: valor });
    else if (!seleccionado && cargasControl.length > 0 && posicion >= 0) cargasControl.splice(posicion, 1);
    if (cargasControl != null && cargasControl.length > 0) $("#btnEliminarSel").prop("disabled", false);
    else $("#btnEliminarSel").prop("disabled", true);
}
function editarCargaDetalle(peticion) {
    if (peticion != null && peticion.Accion >= 0 && peticion.IdCargaAsignacion > 0) {
        let parametros = {
            url: "AdminAsignaciones.aspx/ActualizarCargaDetalle",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            method: 'POST',
            data: JSON.stringify({ peticion: peticion })
        };
        $.ajax(parametros)
            .done(function (respuesta) {
                let resp = respuesta.d;
                if (resp != undefined) {
                    if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                    if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                    cargasControl = [];
                    $("#chSelTodo").prop("checked", false);
                    actualizarCargaAsignacion(peticion.IdCargaAsignacion);
                    $('#tCargaDetalle').DataTable().ajax.reload();
                } else {
                    alert("Ocurrió un error al momento de ejecutar las acciones.");
                }
            })
            .fail(function (error) {
                alert("Ocurrió un error al momento intentar realizar las acciones. Vuela a intentar.");
            });
    }
}
function eliminarSeleccionCargaDetalle() {
    let peticion = {};
    let idCarga = $("#nuevoAsig").data("value");
    if (idCarga > 0) { peticion.IdCargaAsignacion = idCarga };
    if ($("#selErrores").val() != -1) peticion.Error = $("#selErrores").val() == 0 ? false : true;
    if ($("#chSelTodo").prop("checked")) {
        peticion.Accion = 0;
    } else {
        peticion.Accion = 1;
        if (cargasControl == undefined || cargasControl == null || cargasControl.length == 0) { alert("Seleccione al menos un registro para eliminar"); return; }
        peticion.Asignaciones = cargasControl
    }
    if (peticion != null && peticion.Accion >= 0 && peticion.IdCargaAsignacion > 0) {
        editarCargaDetalle(peticion);
    }
}
function actualizarCargaAsignacion(idCarga) {
    let parametros = {
        url: "AdminAsignaciones.aspx/ObtenerCargaAsignacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'GET',
        data: { idCarga: idCarga }
    };
    $.ajax(parametros)
        .done(function (respuesta) {
            let resp = respuesta.d;
            if (resp != undefined) {
                if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                cargaAsignacion(resp.carga);
            } else {
                alert("Ocurrió un error al momento de actualizar los datos.");
            }
        })
        .fail(function (error) {
            alert("Ocurrió un error al momento actualizar los datos.");
        });
}
function cargaAsignar() {
    let idCarga = $("#nuevoAsig").data("value");
    if (idCarga > 0) {
        let parametros = {
            url: "AdminAsignaciones.aspx/AsignarCarga",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            method: 'POST',
            data: JSON.stringify({ idCarga: idCarga })
        };
        $.ajax(parametros)
            .done(function (respuesta) {
                let resp = respuesta.d;
                if (resp != undefined) {
                    if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                    if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                    cargaAsignacion(resp.carga);
                } else {
                    alert("Ocurrió un error al momento de realizar la asignación");
                }
            })
            .fail(function (error) {
                alert("Ocurrió un error al momento de intentar realizar la asignación");
            });
    } else { alert("No es posible realizar la asignación.") }
}
function actualizarActividad() {
    let idCargaAsignacion = $("#nuevoAsig").data("value");
    if (idCargaAsignacion > 0) {
        let parametros = {
            url: "AdminAsignaciones.aspx/GuardarMotivoAsignacion",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            method: 'POST',
            data: JSON.stringify({
                idCarga: idCargaAsignacion, motivo: $("#txActividadAsig").val()
            })
        };
        $.ajax(parametros)
            .done(function (respuesta) {
                let resp = respuesta.d;
                if (resp != undefined) {
                    if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                    if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                    $('#tCargaDetalle').DataTable().ajax.reload();
                } else {
                    alert("Ocurrió un error al momento de ejecutar las acciones.");
                }
            })
            .fail(function (error) {
                alert("Ocurrió un error al momento intentar realizar las acciones. Vuela a intentar.");
            });
    }
}
