﻿$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); $(".modal-pr").appendTo("body"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});

var envios = [];
var envioDetalle = [];
var solicitudNotificaciones = [];

$(document).ready(function () {
    $("#btnNuevo").bind("click", OnbtbtnNuevoClick);
    $("#btImprimir").bind("click", OnbtImprimir);
    cargarEnvios();
});

function cargarEnvios() {
    let parametros = {
        url: "AdminCourier.aspx/CargarEnvios",
        contentType: "application/json",
        dataType: "json",
        method: 'GET'
    };
    $.ajax(parametros)
        .done(function (data) {
            let resp = data.d;
            if (resp !== null && !resp.error) {
                envios = resp;
                mostrarEnvios();
            } else {
                alert("No fue posible obtener los envíos. " + resp.Mensaje);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de obtener los envíos. " + err.Message !== null ? err.Message : "");
        });
}

function mostrarEnvios(){
    $("#tbEnvios").dataTable({
        responsive: true,
        destroy: true,
        data: envios,
        scrollX: true,
        columns: [
            { title: 'IdEnvio', data: 'IdEnvioCourier' },
            { title: 'UsuarioRegistro', data: 'UsuarioRegistro' },
            { title: 'FechaRegistro', data: 'FechaRegistro' },
            {
                mRender: function (data, type, row) {
                    return '<a class="btn btn-info btn-sm" onclick="verEnvio(' + row.IdEnvioCourier + ')" data-id="' + row.IdEnvioCourier + '">VER DETALLE</a>';
                }
            }
        ],
        columnDefs: [
            { "orderable": false, "targets": 0 },
            { "orderable": true, "targets": 1 },
            { "orderable": true, "targets": 2 },
            { "orderable": false, "targets": 3 }
        ],
        "order": [[3, "desc"]],
        language: español
    });
}

function verEnvio(idEnvio) {
    obtenerDetalleEnvioCourier(idEnvio);
}

function obtenerDetalleEnvioCourier(idEnvio) {
    let parametros = {
        url: "AdminCourier.aspx/VerEnvio",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ IdEnvio: idEnvio })
    };
    $.ajax(parametros)
        .done(function (data) {
            let resp = data.d;
            if (resp !== null) {
                envioDetalle = resp;
                if (envioDetalle.length <= 0)
                    return false;
                var table = $("#tEnvioCourierDetalle").DataTable({
                                destroy: true,
                                searching: false,
                                data: envioDetalle,
                                columns: [
                                    { data: "IdEnvioCourier", title: "IdEnvio" },
                                    { data: "IdSolicitudNotificacion", title: "IdSolicitudNotificacion" },
                                    { data: "IdSolicitud", title: "IdSolicitud" },
                                    { data: "Notificacion", title: "Notificacion" },
                                    { data: "Destino", title: "Destino" },
                                    { data: "UsuarioRegistroNotificacion", title: "UsuarioRegistro" },
                                    { data: "FechaRegistroNotificacion", title: "FechaRegistroNotificacion" },
                                    { data: "Error", title: "Reimpresion" },
                                    { data: "Mensaje", title: "MensajeImpresion" }
                                ],
                                language: español,
                                scrollX: true,
                                autoWidth: true,
                                order: [[7, "des"]]
                            });
                $("#modalDetalleEnvio").modal("show");
                $('#modalDetalleEnvio').css('display', 'block');
                table.columns.adjust().draw();
                $("#modalDetalleEnvio").appendTo("body");
            } else {
                alert("No fue posible obtener el detalle.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de obtener el detalle.");
        });
}

function OnbtbtnNuevoClick() {
    let parametros = {
        url: "AdminCourier.aspx/ObtenerSolicitudNotificacionesCourier",
        contentType: "application/json",
        dataType: "json",
        method: 'GET'
    };
    $.ajax(parametros)
        .done(function (data) {
            let resp = data.d;
            if (resp !== null) {
                if (resp.Error)
                    return alert("Ha ocurrido un error. " + resp.Mensaje !== null ? resp.Mensaje : "");
                solicitudNotificaciones = resp;
                //if (solicitudNotificaciones.length <= 0)
                //    return false;
                // Se queda metodo sin buttons ni checkboxes extension porque
                //I tried all the answers here, and a number from other places online.This was the only solution which(a) 
                //properly kept the state of the checkboxes on each row in sync with the selected state of the row; (b) 
                //provided the correct indeterminate state in the header; and(c) maintained correct state even when filters 
                //were applied to the table.Bravo! 
                //https://stackoverflow.com/questions/42570465/datatables-select-all-checkbox
                var table = $("#tSolicitudNotificaciones").DataTable({
                    destroy: true, searching: true, pagingType: "full_numbers", pageLength: 10,
                    language: español, scrollX: true, autoWidth: true, deferRender: true,
                    data: solicitudNotificaciones,
                    columns: [
                        { data: "IdSolicitudNotificacion", title: "Id" },
                        { data: "SolicitudId", title: "Solicitud" },
                        { data: "NombreCliente", title: "NombreCliente" },
                        { data: "Notificacion", title: "Notificacion" },
                        { data: "Destino", title: "Destino" },
                        { data: "UsuarioRegistroNotificacion", title: "UsuarioRegistro" },
                        { data: "FechaRegistro", title: "FechaRegistro" },
                        { data: "Error", title: "Reimpresion" },
                        { data: "Mensaje", title: "MensajeImpresion" }
                    ],
                    columnDefs: [{
                        targets: 0, className: "select-checkbox"
                    }],
                    select: {
                        style: "multi+shift"
                    },
                    order: [[7, "des"]]
                });
                $('#tableCheckAllButton').click(function () {
                    if (table.rows({
                        selected: true
                    }).count() > 0) {
                        table.rows().deselect();
                        return;
                    }
                    table.rows().select();
                });
                table.on('select deselect', function (e, dt, type, indexes) {
                    if (type === 'row') {
                        // We may use dt instead of table to have the freshest data.
                        if (dt.rows().count() === dt.rows({
                            selected: true
                        }).count()) {
                            // Deselect all items button.
                            $('#tableCheckAllButton i').attr('class', 'far fa-check-square');
                            return;
                        }
                        if (dt.rows({
                            selected: true
                        }).count() === 0) {
                            // Select all items button.
                            $('#tableCheckAllButton i').attr('class', 'far fa-square');
                            return;
                        }
                        // Deselect some items button.
                        $('#tableCheckAllButton i').attr('class', 'far fa-minus-square');
                    }
                });
                $("#modalNuevoEnvio").modal("show");
                $('#modalNuevoEnvio').css("display", "block");
                table.columns.adjust().draw();
                $("#modalNuevoEnvio").appendTo("body");
            } else {
                alert("No fue posible obtener las notificaciones.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de obtener las notificaciones.");
        });
}

function seleccionTodo() {
    console.log("asd");
    //// Handle form submission event 
    //$('#frm-example').on('submit', function (e) {
    //    var form = this;

    //    var rows_selected = table.column(0).checkboxes.selected();

    //    // Iterate over all selected checkboxes
    //    $.each(rows_selected, function (index, rowId) {
    //        // Create a hidden element 
    //        $(form).append(
    //            $('<input>')
    //                .attr('type', 'hidden')
    //                .attr('name', 'id[]')
    //                .val(rowId)
    //        );
    //    });

    //    // FOR DEMONSTRATION ONLY
    //    // The code below is not needed in production

    //    // Output form data to a console     
    //    $('#example-console-rows').text(rows_selected.join(","));

    //    // Output form data to a console     
    //    $('#example-console-form').text($(form).serialize());

    //    // Remove added elements
    //    $('input[name="id\[\]"]', form).remove();

    //    // Prevent actual form submission
    //    e.preventDefault();
    //});
}

function OnbtImprimir() {
    var table = $('#tSolicitudNotificaciones').DataTable();
    var ids = table.rows({ selected: true }).data().pluck('IdSolicitudNotificacion').toArray();
    if (ids.length <= 0)
        return alert("Debe seleccionar las notificaciones a imprimir.");
    let parametros = {
        url: "AdminCourier.aspx/ImprimirNotificacionesCourier",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ peticion: ids })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);
                    alert("Archivo descargado.");
                    $("#modalNuevoEnvio").modal("hide");
                    $('#modalNuevoEnvio').css("display", "none");
                    cargarEnvios();
                }
                else {
                    alert("Ocurrió un error. " + resp.Mensaje);
                }
            } else {
                alert("No fue posible obtener las notificaciones.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de obtener las notificaciones.");
        });
}