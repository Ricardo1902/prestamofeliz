﻿$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); $(".modal-pr").appendTo("body"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});

var cliente = {};
var usuarioActualiza = {};
var solicitud = {};
var direccion = {};
var estados = [];
var ciudadesCliente = [];
var coloniasCliente = [];
var ciudadesOficina = [];
var coloniasOficina = [];
var detColoniaCliente = {};
var detColoniaOficina = {};
var direccionesPE = [];
var tipoZonasPE = [];
var empresasPE = [];
var organosPE = [];
var empresaOficina = {};
var sitLaboralesPF = [];
var tiposPension = [];
var giros = [];
var bancos = [];
var previousPage = "";
const MESES_EXPIRACION = [
    { Valor: 1, Descripcion: "ENERO" },
    { Valor: 2, Descripcion: "FEBRERO" },
    { Valor: 3, Descripcion: "MARZO" },
    { Valor: 4, Descripcion: "ABRIL" },
    { Valor: 5, Descripcion: "MAYO" },
    { Valor: 6, Descripcion: "JUNIO" },
    { Valor: 7, Descripcion: "JULIO" },
    { Valor: 8, Descripcion: "AGOSTO" },
    { Valor: 9, Descripcion: "SEPTIEMBRE" },
    { Valor: 10, Descripcion: "OCTUBRE" },
    { Valor: 11, Descripcion: "NOVIEMBRE" },
    { Valor: 12, Descripcion: "DICIEMBRE" }
];
const BANCOS_COBRO = [
    { Valor: "CONTINENTAL", Descripcion: "CONTINENTAL" },
    { Valor: "INTERBANK", Descripcion: "INTERBANK" },
    { Valor: "OTRO", Descripcion: "OTRO" }
];
const MASCULINO = 7;
const FEMENINO = 8;
const LADAS_PERU = [
    { Codigo: "41", Estado: "AMAZONAS" },
    { Codigo: "43", Estado: "ÁNCASH" },
    { Codigo: "83", Estado: "APURÍMAC" },
    { Codigo: "54", Estado: "AREQUIPA" },
    { Codigo: "66", Estado: "AYACUCHO" },
    { Codigo: "76", Estado: "CAJAMARCA" },
    { Codigo: "1", Estado: "CALLAO" },
    { Codigo: "84", Estado: "CUSCO" },
    { Codigo: "67", Estado: "HUANCAVELICA" },
    { Codigo: "62", Estado: "HUÁNUCO" },
    { Codigo: "56", Estado: "ICA" },
    { Codigo: "64", Estado: "JUNÍN" },
    { Codigo: "44", Estado: "LA LIBERTAD" },
    { Codigo: "74", Estado: "LAMBAYEQUE" },
    { Codigo: "1", Estado: "LIMA" },
    { Codigo: "65", Estado: "LORETO" },
    { Codigo: "82", Estado: "MADRE DE DIOS" },
    { Codigo: "53", Estado: "MOQUEGUA" },
    { Codigo: "63", Estado: "PASCO" },
    { Codigo: "73", Estado: "PIURA" },
    { Codigo: "51", Estado: "PUNO" },
    { Codigo: "42", Estado: "SAN MARTÍN" },
    { Codigo: "52", Estado: "TACNA" },
    { Codigo: "72", Estado: "TUMBES" },
    { Codigo: "61", Estado: "UCAYALI" }
];

// se agrega metodo a tipo Array para buscar indice dentro de un arreglo de objetos
Array.prototype.indexOfAttribute = function (attr, value) {
    for (var i = 0; i < this.length; i += 1) {
        if (this[i][attr] === value) {
            return i;
        }
    }
    return -1;
};

// Validaciones personalizadas de parsley
window.Parsley.addValidator("anioExpiracion", {
    requirementType: "string",
    validateNumber: function (valor, control) {
        let esRequerido = $("#" + control).prop("required");
        if (esRequerido) {
            let currentDate = new Date();
            return valor >= currentDate.getFullYear();
        }
    },
    messages: {
        es: "El año de expiración no es correcto"
    }
});

window.Parsley.addValidator("telefonoPeru", {
    requirementType: "string",
    validateString: function (valor, comboEstado) {
        let coincidenciaLada = LADAS_PERU.indexOfAttribute(
            "Estado",
            $("#" + comboEstado + " option:selected").text()
        );
        let caracteres = 0;
        if (coincidenciaLada.length > 1) caracteres = 6;
        else caracteres = 7;

        if (caracteres != valor.length)
            return $.Deferred().reject(
                "El telefono debe consistir de " + caracteres + " catacteres"
            );
        else return true;
    }
});
window.Parsley.addValidator("seleccionCombo", {
    requirementType: "string",
    validateString: function (valor, control) {
        let esRequerido = $("#" + control).prop("required");
        if (esRequerido) {
            return valor != "" && valor != 0;
        } else return valor != "" && (valor > 0 || valor);
    },
    messages: {
        es: "La selección no es válida"
    }
});

window.Parsley.addValidator("tarjetaVisa", {
    requirementType: "string",
    validateString: function (valor) {
        let num = valor.replace(/ /, "");
        if (num.length >= 16 && num.length <= 18) {
            let pattern = "^(([0-9]{16}$)|([0-9]{4}(\\s+)?){4})$";
            let result = valor.match(pattern);
            if (result == -1 || result == null || result == undefined) return false;
            else return true;
        }
        return false;
    },
    messages: {
        es: "El número de tarjeta no es válido"
    }
});

$(document).on({
    ajaxStart: function () {
        $("body").addClass("loading");
    },
    ajaxStop: function () {
        $("body").removeClass("loading");
    }
});

$(document).ready(function () {
    $("#aspnetForm")
        .parsley()
        .on("field:validated", function () {
            var ok = $(".parsley-error").length === 0;
        })
        .on("form:submit", function (data) {
            actualizarInformacion();
            return false;
        });
    $("#txtFchNacimiento").datepicker();
    $("#btnCancelar").bind("click", onClickBtnCancelar);
    $("#chkMasculino").bind("click", onClickChkGenero);
    $("#chkFemenino").bind("click", onClickChkGenero);
    $("#chkVisaDom").bind("click", onClickChkVisaDom);
    $("#chkCuentaDomEmergente").bind("click", onClickChkCuentaDomEmergente);
    $("#btnEditarNumeroTarjetaVisa").bind(
        "click",
        onClickBtnEditarNumeroTarjetaVisa
    );
    $("#btnCancelarCapcturaTarjeta").bind(
        "click",
        onClickBtnCancelarCapcturaTarjeta
    );

    $("#selDepartamento").bind("change", onChangeSelDepartamento);
    $("#selProvincia").bind("change", onChangeSelProvincia);

    $("#selDepartamentoOficina").bind("change", onChangeSelDepartamentoOficina);
    $("#selProvinciaOficina").bind("change", onChangeSelProvinciaOficina);

    $("#selBancoCobro").bind("change", onChangeSelBancoCobro);
    $("#selBancoEmergente").bind("change", onChangeSelBancoEmergente);

    $("#txtEntidadCobro").bind("change", onChangeCuenta);
    $("#txtOficinaCobro").bind("change", onChangeCuenta);
    $("#txtDCCobro").bind("change", onChangeCuenta);
    $("#txtCuentaCobro").bind("change", onChangeCuenta);
    $("#txtNoOficinaCobro").bind("change", onChangeCuenta);

    $("#txtEntidadEmergente").bind("change", onChangeCuenta);
    $("#txtOficinaEmergente").bind("change", onChangeCuenta);
    $("#txtDCEmergente").bind("change", onChangeCuenta);
    $("#txtCuentaEmergente").bind("change", onChangeCuenta);
    $("#txtNoOficinaEmergente").bind("change", onChangeCuenta);

    LlenarDatosCliente();
});
function LlenarDatosCliente() {
    if (!isEmpty(cliente)) {
        var tempFechaActual = moment(new Date());
        let lblUsuarioActualiza = $("#lblUsuarioActualiza");
        let lblFchActualiza = $("#lblFchActualiza");

        if (!isEmpty(usuarioActualiza) && usuarioActualiza.IdUsuario > 0)
            lblUsuarioActualiza.text(
                lblUsuarioActualiza
                    .text()
                    .replace("{{UsuarioActualizacion}}", usuarioActualiza.NombreCompleto)
            );
        else
            lblUsuarioActualiza.text(
                lblUsuarioActualiza.text().replace("{{UsuarioActualizacion}}", "-")
            );
        if (cliente.FechaActualizacion)
            lblFchActualiza.text(
                lblFchActualiza
                    .text()
                    .replace("{{FechaAcutalizacion}}", cliente.FechaActualizacion)
            );
        else
            lblFchActualiza.text(
                lblFchActualiza.text().replace("{{FechaAcutalizacion}}", "-")
            );

        //Datos Generales
        $("#txtNombresCliente").val(cliente.Nombres);
        $("#txtApellidoPaterno").val(cliente.Apellidop);
        $("#txtApellidoMaterno").val(cliente.Apellidom);
        var tempFechaNacimiento = moment(cliente.FchNacimiento, "DD/MM/YYYY")._d;
        if (esFechaValida(tempFechaNacimiento)) {
            var tempDif = tempFechaActual.diff(tempFechaNacimiento);
            $("#txtFchNacimiento").datepicker("update", tempFechaNacimiento);
            if (tempDif !== undefined && tempDif > 0) {
                tempEdad = Math.floor(tempDif / 3.154e10);
                $("#txtEdad").val(tempEdad);
            }
        }
        asignarGenero(cliente.Sexo);

        // Direccion del Cliente
        if (!isEmpty(direccionesPE)) {
            poblarControlSeleccion("#selDireccion", direccionesPE, "Descripcion", "Valor");
        }
        if (!isEmpty(tipoZonasPE)) {
            poblarControlSeleccion("#selTipoZona", tipoZonasPE, "Descripcion", "Valor");
        }
        if (!isEmpty(estados)) {
            poblarControlSeleccion("#selDepartamento", estados, "Estado", "IdEstado");
            poblarControlSeleccion("#selDepartamentoOficina", estados, "Estado", "IdEstado");
        }
        if (!isEmpty(direccion)) {
            $("#selDireccion").val(direccion.idTipoDireccion);
            $("#txtVia").val(direccion.nombreVia);
            $("#txtNumExterior").val(direccion.numeroExterno);
            $("#txtNumInterior").val(direccion.numeroInterno);
            $("#selTipoZona").val(direccion.tipoZona);
            $("#txtNombreZona").val(direccion.nombreZona);

            if (!isEmpty(detColoniaCliente)) {
                $("#selDepartamento").val(detColoniaCliente.IdEstado);
                onChangeSelDepartamento();
                $("#selProvincia").val(detColoniaCliente.IdCiudad);
                onChangeSelProvincia();
                $("#selDistrito").val(detColoniaCliente.IdColonia);
            }
        }

        $("#txtReferenciaDomicilio").val(cliente.ReferenciaDomicilio);
        $("#txtTelefono").val(cliente.Telefono);
        $("#txtTelefonoCelular").val(cliente.Celular);

        //Informacion Laboral
        $("#txtOcupacion").val(cliente.Ocupacion);
        //if (!isEmpty(sitLaboralesPF)) {
        //    poblarControlSeleccion("#selSituacionLaboral", sitLaboralesPF, "Descripcion", "Valor");
        //    $("#selSituacionLaboral").val(cliente.SituacionLaboral);
        //}
        //if (!isEmpty(tiposPension)) {
        //    poblarControlSeleccion("#selRegimenPension", tiposPension, "Descripcion", "IdTipoPension");
        //    $("#selRegimenPension").val(cliente.RegimenPension);
        //}
        $("#txtRazonSocial").val(cliente.Compania);
        $("#txtRuc").val(cliente.RUC);

        let nombreComboEmpresa = "#selEmpresaOficina";
        let selEmpresaOficina = $(nombreComboEmpresa);

        let nombreComboOrganos = "#selOrganoPago";
        let selOrganoPago = $(nombreComboOrganos);

        let nombreComboSituacion = "#selSituacionLaboral";
        let selSituacionLaboral = $(nombreComboSituacion);

        let nombreComboRegimen = "#selRegimenPension";
        let selRegimenPension = $(nombreComboRegimen);

        if (!isEmpty(empresasPE))
            poblarControlSeleccion(nombreComboEmpresa, empresasPE, "Descripcion", "Valor");

        if (!isEmpty(organosPE))
            poblarControlSeleccion(nombreComboOrganos, organosPE, "Descripcion", "Valor");

        if (!isEmpty(sitLaboralesPF))
            poblarControlSeleccion(nombreComboSituacion, sitLaboralesPF, "Descripcion", "Valor");

        if (!isEmpty(tiposPension))
            poblarControlSeleccion(nombreComboRegimen, tiposPension, "Descripcion", "IdTipoPension");

        if (!isEmpty(empresaOficina)) {
            let configEmpresa = empresasPE.find(ce => ce.Valor == empresaOficina.IdEmpresa);
            let configOrgano = organosPE.find(op => op.Valor == empresaOficina.IdOrganoPago);
            let configSituacion = sitLaboralesPF.find(sl => sl.Valor == empresaOficina.IdSituacionLaboral);
            let configRegimen = tiposPension.find(tp => tp.IdTipoPension == empresaOficina.IdRegimenPension);

            if (!isEmpty(configEmpresa)) {
                selEmpresaOficina.val(configEmpresa.Valor);
            } else { selEmpresaOficina.val(0); }

            if (!isEmpty(configOrgano)) {
                selOrganoPago.val(configOrgano.Valor);
            } else { selOrganoPago.val(0); }

            if (!isEmpty(configSituacion)) {
                selSituacionLaboral.val(configSituacion.Valor);
            } else { selSituacionLaboral.val(0); }

            if (!isEmpty(configRegimen)) {
                selRegimenPension.val(configRegimen.IdTipoPension);
            } else { selRegimenPension.val(0); }

            $("#txtNivelRiesgo").val(empresaOficina.NivelRiesgo);
        } else {
            if (cliente.SituacionLaboral) {
                selSituacionLaboral.val(cliente.SituacionLaboral);
            } else {
                selSituacionLaboral.val(0);
            }

            if (cliente.RegimenPension) {
                selSituacionLaboral.val(cliente.RegimenPension);
            } else {
                selSituacionLaboral.val(0);
            }
        }
        selEmpresaOficina.selectpicker();
        selEmpresaOficina.bind("change", onChangeSelEmpresaOficina);

        $("#selSituacionLaboral").bind("change", onChangeSelSituacionLaboral);
        $("#selRegimenPension").bind("change", onChangeSelRegimenPension);

        let seleccionSituacion = $("#selSituacionLaboral option:selected").text();
        seleccionSituacion = seleccionSituacion.toUpperCase();
        if (seleccionSituacion === "JUBILADO" || seleccionSituacion === "PENSIONISTA")
            $("#regimenPension").show("fast");
        else
            $("#regimenPension").hide("fast");
    }



    //if (!isEmpty(giros)) {
    //    poblarControlSeleccion("#selGiroNegocio", giros, "Giro", "IdGiro");
    //    $("#selGiroNegocio").val(cliente.IdGiro);
    //}
    if (!isEmpty(detColoniaOficina)) {
        $("#selDepartamentoOficina").val(detColoniaOficina.IdEstado);
        onChangeSelDepartamentoOficina();
        $("#selProvinciaOficina").val(detColoniaOficina.IdCiudad);
        onChangeSelProvinciaOficina();
        $("#selDistritoOficina").val(detColoniaOficina.IdColonia);
    }
    $("#txtCalleOficina").val(cliente.DireOfic);
    $("#txtNumExteriorOficina").val(cliente.NumExteriorOfic);
    $("#txtNumInteriorOficina").val(cliente.NumInteriorOfic);
    $("#txtDependenciaOficina").val(cliente.DependenciaOfic);
    $("#txtUbicacion").val(cliente.UbicacionOfic);
    $("#txtTelefonoOficina").val(cliente.TeleOfic);
    $("#txtAnexoOficina").val(cliente.ExteOfic);
    $("#txtPuestoOficina").val(cliente.Puesto);
    $("#txtAntiguedadOficina").val(cliente.AntigOfic);
    $("#txtIngresoBruto").val(cliente.IngresoBruto);
    $("#txtDescuentosOficina").val(cliente.DescuentosLey);
    $("#txtOtrosDescuentos").val(cliente.OtrosDescuentos);
    $("#txtIngreso").val(cliente.Ingreso);
    $("#txtOtrosIngresos").val(cliente.OtrosIngr);

    poblarControlSeleccion("#selBancoCobro", BANCOS_COBRO, "Descripcion", "Valor");
    poblarControlSeleccion("#selBancoEmergente", BANCOS_COBRO, "Descripcion", "Valor");

    if (!isEmpty(solicitud)) {
        let tempBancoCobro = solicitud.Banco.toUpperCase();
        if (tempBancoCobro) {
            let txtEntidadCobro = $("#txtEntidadCobro"),
                txtOficinaCobro = $("#txtOficinaCobro"),
                txtDCCobro = $("#txtDCCobro"),
                txtNoOficinaCobro = $("#txtNoOficinaCobro"),
                txtCuentaCobro = $("#txtCuentaCobro"),
                txtCuentaBancariaCobro = $("#txtCuentaBancariaCobro"),
                txtCuentaCCICobro = $("#txtCuentaCCICobro");

            let seleccionBancoCobro =
                BANCOS_COBRO.indexOfAttribute("Valor", tempBancoCobro) < 0
                    ? "OTRO"
                    : tempBancoCobro;
            $("#selBancoCobro").val(seleccionBancoCobro);
            MostrarSeccionBanco("Cobro", seleccionBancoCobro);
            switch (tempBancoCobro) {
                case "CONTINENTAL":
                    txtEntidadCobro.val(solicitud.CuentaDesembolso.substring(0, 4));
                    txtOficinaCobro.val(solicitud.CuentaDesembolso.substring(4, 8));
                    txtDCCobro.val(solicitud.CuentaDesembolso.substring(8, 10));
                    txtCuentaCobro.val(solicitud.CuentaDesembolso.substring(10, 20));
                    break;
                case "INTERBANK":
                    txtNoOficinaCobro.val(solicitud.CuentaDesembolso.substring(0, 3));
                    txtCuentaCobro.val(solicitud.CuentaDesembolso.substring(3, 13));
                    break;
                default:
                    txtCuentaCCICobro.val(solicitud.CuentaCCI);
                    break;
            }
            txtCuentaBancariaCobro.val(solicitud.CuentaDesembolso);
            $("#txtNombreBancoCobro").val(solicitud.Banco);
        }
        let txtAñoExpiracionVisa = $("#txtAñoExpiracionVisa"),
            secMascaraTarjetaVisa = $("#secMascaraTarjetaVisa"),
            secTarjetaVisa = $("#secTarjetaVisa"),
            chkVisaDom = $("#chkVisaDom");
        if (!isEmpty(MESES_EXPIRACION)) {
            poblarControlSeleccion("#selMesExpiracionVisa", MESES_EXPIRACION, "Descripcion", "Valor");
            $("#selMesExpiracionVisa").val(solicitud.MesExpiraTarjetaVisa);
        }
        if (solicitud.NumeroTarjetaVisa) {
            let txtMascaraTarjetaVisa = $("#txtMascaraTarjetaVisa");

            txtMascaraTarjetaVisa.val(solicitud.NumeroTarjetaVisa);
            txtMascaraTarjetaVisa.prop("disabled", true);
            secMascaraTarjetaVisa.show();
            secTarjetaVisa.hide();
            txtAñoExpiracionVisa.val(solicitud.AnioExpiraTarjetaVisa);
            txtAñoExpiracionVisa.prop("required", true);
            chkVisaDom.prop("checked", true);
            $("#selMesExpiracionVisa").prop("required", true);
            $("#secVisaDom").show("fast");
        } else {
            txtAñoExpiracionVisa.prop("required", false);
            secMascaraTarjetaVisa.hide();
            secTarjetaVisa.show();
            chkVisaDom.prop("checked", false);
            $("#selMesExpiracionVisa").prop("required", false);
            $("#secVisaDom").hide("fast");
        }

        if (!isEmpty(solicitud.CuentasDomiciliacionEmergentes)) {
            // TODO: Llenar campos cuenta emergente

            let cuentaEmergente = solicitud.CuentasDomiciliacionEmergentes[0];
            let tempBancoEmergente = cuentaEmergente.Banco.toUpperCase();
            if (tempBancoEmergente) {
                let txtEntidadEmergente = $("#txtEntidadEmergente"),
                    txtOficinaEmergente = $("#txtOficinaEmergente"),
                    txtDCEmergente = $("#txtDCEmergente"),
                    txtNoOficinaEmergente = $("#txtNoOficinaEmergente"),
                    txtCuentaEmergente = $("#txtCuentaEmergente"),
                    txtCuentaBancariaEmergente = $("#txtCuentaBancariaEmergente"),
                    txtCuentaCCIEmergente = $("#txtCuentaCCIEmergente");

                let seleccionBancoEmergente =
                    BANCOS_COBRO.indexOfAttribute("Valor", tempBancoEmergente) < 0
                        ? "OTRO"
                        : tempBancoEmergente;
                $("#selBancoEmergente").val(seleccionBancoEmergente);
                MostrarSeccionBanco("Emergente", seleccionBancoEmergente);
                switch (tempBancoEmergente) {
                    case "CONTINENTAL":
                        txtEntidadEmergente.val(cuentaEmergente.Cuenta.substring(0, 4));
                        txtOficinaEmergente.val(cuentaEmergente.Cuenta.substring(4, 8));
                        txtDCEmergente.val(cuentaEmergente.Cuenta.substring(8, 10));
                        txtCuentaEmergente.val(cuentaEmergente.Cuenta.substring(10, 20));
                        break;
                    case "INTERBANK":
                        txtNoOficinaEmergente.val(cuentaEmergente.Cuenta.substring(0, 3));
                        txtCuentaEmergente.val(cuentaEmergente.Cuenta.substring(3, 13));
                        break;
                    default:
                        txtCuentaCCIEmergente.val(cuentaEmergente.CuentaCCI);
                        break;
                }
                txtCuentaBancariaEmergente.val(cuentaEmergente.Cuenta);
                $("#txtNombreBancoEmergente").val(cuentaEmergente.Banco);
            }

            $("#txtEntidadEmergente").val();
            $("#chkCuentaDomEmergente").prop("checked", true);
            $("#secCuentaDomEmergente").show("fast");
        } else {
            $("#chkCuentaDomEmergente").prop("checked", false);
            $("#secCuentaDomEmergente").hide("fast");
        }
    }
}

function obtenerCiudades(idEstado, callback) {
    if (idEstado && idEstado > 0) {
        $.ajax({
            type: "GET",
            url: "frmActualizarCliente.aspx/ObtenerCiudades?idEstado=" + idEstado,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                callback(data.d);
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}
function obtenerColonias(idCiudad, callback) {
    if (idCiudad && idCiudad > 0) {
        $.ajax({
            type: "GET",
            url: "frmActualizarCliente.aspx/ObtenerColonias?idCiudad=" + idCiudad,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                callback(data.d);
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}
function onClickBtnCancelar() {
    $('body').addClass('loading'); $('.modal-pr').appendTo('body');
    if (previousPage) window.location.href = previousPage;
}
// Eventos
function onClickChkGenero(e) {
    asignarGenero(e.target.value);
}
function onChangeSelDepartamento() {
    let idEstado = $("#selDepartamento option:selected").val();
    obtenerCiudades(idEstado, function (data) {
        ciudadesCliente = data;
    });
    if (ciudadesCliente && !isEmpty(ciudadesCliente)) {
        poblarControlSeleccion("#selProvincia", ciudadesCliente, "Ciudad", "IdCiudad");
        onChangeSelProvincia();
    }
}
function onClickChkVisaDom(e) {
    if (e.target.checked) {
        $("#secVisaDom").show("fast");
        $("#selMesExpiracionVisa").prop("required", true);
        $("#txtAñoExpiracionVisa").prop("required", true);
    } else {
        $("#secVisaDom").hide("fast");
        $("#txtNumeroTarjetaVisa").prop("required", false);
        $("#selMesExpiracionVisa").prop("required", false);
        $("#txtAñoExpiracionVisa").prop("required", false);
    }
}
function onClickChkCuentaDomEmergente(e) {
    if (e.target.checked) {
        $("#secCuentaDomEmergente").show("fast");
    } else {
        $("#secCuentaDomEmergente").hide("fast");
    }
}
function onClickBtnEditarNumeroTarjetaVisa() {
    let txtNumeroTarjetaVisa = $("#txtNumeroTarjetaVisa");
    txtNumeroTarjetaVisa.val("");
    txtNumeroTarjetaVisa.prop("required", true);
    $("#secMascaraTarjetaVisa").hide();
    $("#secTarjetaVisa").show();
    $("#btnCancelarCapcturaTarjeta").show();
    txtNumeroTarjetaVisa.parent().addClass("input-group");
    txtNumeroTarjetaVisa.focus();
}
function onChangeSelProvincia() {
    let idCiudad = $("#selProvincia option:selected").val();
    obtenerColonias(idCiudad, function (data) {
        coloniasCliente = data;
    });
    if (coloniasCliente && !isEmpty(ciudadesCliente)) {
        poblarControlSeleccion("#selDistrito", coloniasCliente, "Colonia", "IdColonia");
    }
}
function onClickBtnCancelarCapcturaTarjeta(e) {
    let txtNumeroTarjetaVisa = $("#txtNumeroTarjetaVisa");
    txtNumeroTarjetaVisa.val("");
    txtNumeroTarjetaVisa.prop("required", false);
    txtNumeroTarjetaVisa.parent().removeClass("input-group");
    $("#secMascaraTarjetaVisa").show();
    $("#secTarjetaVisa").hide();
    $("#btnCancelarCapcturaTarjeta").hide();
}
function onChangeSelEmpresaOficina() {
    let idEmpresa = parseInt($("#selEmpresaOficina option:selected").val());
    let idSituacionLaboral = parseInt($("#selSituacionLaboral option:selected").val());
    let idRegimen = parseInt($("#selRegimenPension option:selected").val());

    if (!isNaN(idEmpresa) && !isNaN(idSituacionLaboral)) {
        //TODO: actualizar los combos dependientes de la configracion Empresa
        console.log("Actualizando combos empresa...");

        let parametros = {
            url: "frmActualizarCliente.aspx/ObtenerConfiguracionEmpresa",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            method: 'POST',
            data: JSON.stringify({
                peticion: {
                    Accion: "OBTENER_POR_CAMPOS",
                    IdEmpresa: idEmpresa,
                    IdSituacionLaboral: idSituacionLaboral,
                    IdRegimenPension: idRegimen
                }
            })
        };

        $.ajax(parametros)
            .done(function (data) {
                if (!isEmpty(data.d)) {
                    let respuesta = data.d;
                    if (!isEmpty(respuesta.Resultado) && respuesta.Resultado.length > 0) {
                        let configEmpresa = respuesta.Resultado[0];
                        console.log(configEmpresa);

                        //Se actualizan combos correspondientes a la configuracion de la empresa seleccionada
                        $("#selSituacionLaboral").val(configEmpresa.IdSituacionLaboral);
                        $("#selOrganoPago").val(configEmpresa.IdOrganoPago);
                        $("#selRegimenPension").val(configEmpresa.IdRegimenPension);
                        $("#txtNivelRiesgo").val(configEmpresa.NivelRiesgo);

                        //Se actualiza objeto que contiene la configuracion de la empresa
                        empresaOficina = {
                            ...empresaOficina,
                            IdEmpresa: idEmpresa,
                            IdSituacionLaboral: configEmpresa.IdSituacionLaboral,
                            IdRegimenPension: configEmpresa.IdRegimenPension,
                            IdOrganoPago: configEmpresa.IdOrganoPago,
                        };
                    } else {
                        empresaOficina = {
                            ...empresaOficina,
                            IdEmpresa: idEmpresa
                        };
                    }
                } else {
                    alert("No fue posible obtener la informacion de la empresa seleccionada. " + data.MensajeOperacion);
                }
            })
            .fail(function (err) {
                alert("Ocurrió un error al intentar al obtener la informacion de la empresa seleccionada. " + err.responseJSON.Message);
            });
    }
}
function onChangeSelSituacionLaboral() {
    let seleccionSituacion = $("#selSituacionLaboral option:selected").text();

    if (seleccionSituacion) {
        seleccionSituacion = seleccionSituacion.toUpperCase();
        let idSituacion = parseInt($("#selSituacionLaboral").val());
        if (seleccionSituacion == "JUBILADO" || seleccionSituacion == "PENSIONISTA") {
            $("#regimenPension").show("fast");
        }
        else {
            $("#regimenPension").hide("fast");
        }
        if (!isNaN(idSituacion)) {
            empresaOficina = {
                ...empresaOficina,
                IdSituacionLaboral: idSituacion
            };
        }
        onChangeSelEmpresaOficina();
    }
}
function onChangeSelRegimenPension() {
    let idRegimenPension = parseInt($("#selRegimenPension").val());

    if (!isNaN(idRegimenPension)) {
        empresaOficina = {
            ...empresaOficina,
            IdRegimenPension: idRegimenPension
        };
    }
    onChangeSelEmpresaOficina();
}
function onChangeSelDepartamentoOficina() {
    let idEstado = $("#selDepartamentoOficina option:selected").val();
    obtenerCiudades(idEstado, function (data) {
        ciudadesOficina = data;
    });
    if (ciudadesOficina && !isEmpty(ciudadesOficina)) {
        poblarControlSeleccion(
            "#selProvinciaOficina",
            ciudadesOficina,
            "Ciudad",
            "IdCiudad"
        );
        onChangeSelProvinciaOficina();
    }
}
function onChangeSelProvinciaOficina() {
    let idCiudad = $("#selProvinciaOficina option:selected").val();
    obtenerColonias(idCiudad, function (data) {
        coloniasOficina = data;
    });
    if (coloniasOficina && !isEmpty(coloniasOficina)) {
        poblarControlSeleccion(
            "#selDistritoOficina",
            coloniasOficina,
            "Colonia",
            "IdColonia"
        );
    }
}
function onChangeSelBancoCobro(e) {
    let banco = $("#selBancoCobro option:selected").val();
    MostrarSeccionBanco("Cobro", banco);
}
function onChangeSelBancoEmergente(e) {
    let banco = $("#selBancoEmergente option:selected").val();
    MostrarSeccionBanco("Emergente", banco);
}
function MostrarSeccionBanco(sufijo, valorSeleccionado) {
    let txtNombreBanco = $("#txtNombreBanco" + sufijo),
        txtCuentaBancaria = $("#txtCuentaBancaria" + sufijo);

    let txtEntidad = $("#txtEntidad" + sufijo),
        txtOficina = $("#txtOficina" + sufijo),
        txtDC = $("#txtDC" + sufijo),
        txtCuenta = $("#txtCuenta" + sufijo),
        txtNoOficina = $("#txtNoOficina" + sufijo),
        txtCuentaCCI = $("#txtCuentaCCI" + sufijo);

    let secNombreBanco = $("#secNombreBanco" + sufijo),
        secEntidad = $("#secEntidad" + sufijo),
        secOficina = $("#secOficina" + sufijo),
        secDC = $("#secDC" + sufijo),
        secCuenta = $("#secCuenta" + sufijo),
        secNoOficina = $("#secNoOficina" + sufijo),
        secCuentaBancaria = $("#secCuentaBancaria" + sufijo),
        secCuentaCCI = $("#secCuentaCCI" + sufijo);

    switch (valorSeleccionado) {
        case "CONTINENTAL":
            txtNombreBanco.val(valorSeleccionado);
            txtNombreBanco.prop("disabled", true);
            txtCuentaBancaria.prop("disabled", true);

            //Limpiar contorles
            txtEntidad.val("");
            txtOficina.val("");
            txtDC.val("");
            txtCuenta.val("");
            txtNoOficina.val("");
            txtCuentaBancaria.val("");
            txtCuentaCCI.val("");

            txtNombreBanco.prop("required", true);
            secNombreBanco.show();
            txtEntidad.prop("required", true);
            secEntidad.show();
            txtOficina.prop("required", true);
            secOficina.show();
            txtDC.prop("required", true);
            secDC.show();
            txtCuenta.prop("required", true);
            secCuenta.show();
            txtCuentaBancaria.prop("required", true);
            secCuentaBancaria.show();

            txtNoOficina.prop("required", false);
            secNoOficina.hide();
            txtCuentaCCI.prop("required", false);
            secCuentaCCI.hide();
            break;

        case "INTERBANK":
            txtNombreBanco.val(valorSeleccionado);
            txtNombreBanco.prop("disabled", true);
            txtCuentaBancaria.prop("disabled", true);

            //Limpiar contorles
            txtEntidad.val("");
            txtOficina.val("");
            txtDC.val("");
            txtCuenta.val("");
            txtNoOficina.val("");
            txtCuentaBancaria.val("");
            txtCuentaCCI.val("");

            txtNombreBanco.prop("required", true);
            secNombreBanco.show();
            txtCuenta.prop("required", true);
            secCuenta.show();
            txtNoOficina.prop("required", true);
            secNoOficina.show();
            txtCuentaBancaria.prop("required", true);
            secCuentaBancaria.show();

            txtEntidad.prop("required", false);
            secEntidad.hide();
            txtOficina.prop("required", false);
            secOficina.hide();
            txtDC.prop("required", false);
            secDC.hide();
            txtCuentaCCI.prop("required", false);
            secCuentaCCI.hide();
            break;
        case "OTRO":
            txtNombreBanco.val("");
            txtNombreBanco.prop("disabled", false);
            txtCuentaBancaria.prop("disabled", false);

            //Limpiar contorles
            txtEntidad.val("");
            txtOficina.val("");
            txtDC.val("");
            txtCuenta.val("");
            txtNoOficina.val("");
            txtCuentaBancaria.val("");
            txtCuentaCCI.val("");

            txtNombreBanco.prop("required", true);
            secNombreBanco.show();
            txtCuentaBancaria.prop("required", true);
            secCuentaBancaria.show();
            txtCuentaCCI.prop("required", true);
            secCuentaCCI.show();

            txtEntidad.prop("required", false);
            secEntidad.hide();
            txtOficina.prop("required", false);
            secOficina.hide();
            txtDC.prop("required", false);
            secDC.hide();
            txtCuenta.prop("required", false);
            secCuenta.hide();
            txtNoOficina.prop("required", false);
            secNoOficina.hide();
            break;
        default:
            txtNombreBanco.val("");
            secNombreBanco.hide();

            txtEntidad.val("");
            txtOficina.val("");
            txtDC.val("");
            txtCuenta.val("");
            txtNoOficina.val("");
            txtCuentaBancaria.val("");
            txtCuentaCCI.val("");

            txtEntidad.prop("required", false);
            secEntidad.hide();
            txtOficina.prop("required", false);
            secOficina.hide();
            txtDC.prop("required", false);
            secDC.hide();
            txtCuenta.prop("required", false);
            secCuenta.hide();
            txtNoOficina.prop("required", false);
            secNoOficina.hide();
            txtCuentaBancaria.prop("required", false);
            secCuentaBancaria.hide();
            txtCuentaCCI.prop("required", false);
            secCuentaCCI.hide();
            break;
    }
}
function onChangeCuenta(e) {
    let control = $("#" + e.target.id);
    let sufijo = "";
    if (!isEmpty(control)) {
        sufijo = control.attr("sufijo");
    }
    let seleccion = "";
    if (sufijo) {
        let tempNombreControl = "#selBanco" + sufijo + " option:selected";
        let seleccion = $(tempNombreControl).val();
        if (seleccion) {
            alimentarCampoCuentaBanco(sufijo, seleccion);
        }
    }
}
function alimentarCampoCuentaBanco(sufijo, seleccion) {
    let tempCuentaBancaria = "";
    let txtCuentaBancaria = $("#txtCuentaBancaria" + sufijo),
        txtCuenta = $("#txtCuenta" + sufijo);
    switch (seleccion) {
        case "CONTINENTAL":
            let txtEntidad = $("#txtEntidad" + sufijo),
                txtOficina = $("#txtOficina" + sufijo),
                txtDC = $("#txtDC" + sufijo);

            tempCuentaBancaria =
                txtEntidad.val() + txtOficina.val() + txtDC.val() + txtCuenta.val();
            break;
        case "INTERBANK":
            let txtNoOficina = $("#txtNoOficina" + sufijo);

            tempCuentaBancaria = txtNoOficina.val() + txtCuenta.val();
            break;
        case "OTRO":
            tempCuentaBancaria = txtCuentaBancaria.val();
            break;
    }
    txtCuentaBancaria.val(tempCuentaBancaria);
}
function actualizarInformacion() {
    let actCuentaEmergente = {};
    let actCliente = {};
    let actDireccion = {};
    let actSolicitud = {};
    //Seccion Clientes
    actCliente.IdCliente = cliente.IdCliente;
    //Seccion Direccion
    if (!isEmpty(direccion)) {
        actDireccion.idClienteDireccion = direccion.idClienteDireccion;
    }
    if (!isEmpty(solicitud)) {
        actDireccion.IdSolicitud = solicitud.IDSolictud;
    }
    if (!isEmpty(cliente)) {
        actDireccion.IdCliente = cliente.IdCliente;
    }
    let selDireccion = $("#selDireccion option:selected").val();
    if (selDireccion) {
        actCliente.TipoDireccion = parseInt(selDireccion);
        actDireccion.idTipoDireccion = parseInt(selDireccion);
    } else {
        actCliente.TipoDireccion = 0;
        actDireccion.idTipoDireccion = 0;
    }
    actCliente.NumExterior = $("#txtNumExterior").val();
    actDireccion.numeroExterno = $("#txtNumExterior").val();
    actCliente.NumInterior = $("#txtNumInterior").val();
    actDireccion.numeroInterno = $("#txtNumInterior").val();
    actCliente.NombreVia = $("#txtVia").val();
    actDireccion.nombreVia = $("#txtVia").val();
    let selTipoZona = $("#selTipoZona option:selected").val();
    if (selTipoZona) {
        actCliente.TipoZona = parseInt(selTipoZona);
        actDireccion.tipoZona = parseInt(selTipoZona);
    } else {
        actCliente.TipoZona = 0;
        actDireccion.tipoZona = 0;
    }
    actCliente.NombreZona = $("#txtNombreZona").val();
    actDireccion.nombreZona = $("#txtNombreZona").val();
    let selDistrito = $("#selDistrito option:selected").val();
    if (selDistrito) {
        actCliente.IdColonia = parseInt(selDistrito);
        actDireccion.idColonia = parseInt(selDistrito);
    } else {
        actCliente.IdColonia = 0;
        actDireccion.idColonia = 0;
    }

    actCliente.ReferenciaDomicilio = $("#txtReferenciaDomicilio").val();
    actCliente.Telefono = $("#txtTelefono").val();
    actCliente.Celular = $("#txtTelefonoCelular").val();
    actCliente.Ocupacion = $("#txtOcupacion").val();

    //Se asignan valores actuales del cliente
    actCliente.Compania = cliente.Compania;
    actCliente.RUC = cliente.RUC;
    actCliente.RegimenPension = cliente.RegimenPension;

    if (!isEmpty(empresaOficina)) {
        actCliente.IdEmpresa = empresaOficina.IdEmpresa;
        actCliente.IdOrganoPago = empresaOficina.IdOrganoPago;
        actCliente.RegimenPension = empresaOficina.IdRegimenPension;
        actCliente.SituacionLaboral = empresaOficina.IdSituacionLaboral;

        //Se intenta normalizar el nombre de la compania en la tabla de clientes para que concuerde con la configuracion de empresa
        let empresa = empresasPE.find(ep => ep.Valor == empresaOficina.IdEmpresa);
        if (!isEmpty(empresa)) {
            let descRuc = empresa.Descripcion.split("-");
            if (!isEmpty(descRuc)) {
                if (descRuc.length > 0)
                    actCliente.RUC = descRuc[0].trim();
                if (descRuc.length > 1)
                    actCliente.Compania = descRuc[1].trim();

                console.log("split descripcion: ", descRuc);
            }
        }
    }
    //actCliente.Compania = $("#txtRazonSocial").val();
    //actCliente.RUC = $("#txtRuc").val();
    //let selGiroNegocio = $("#selGiroNegocio option:selected").val();
    //if (selGiroNegocio) actCliente.IdGiro = parseInt(selGiroNegocio);
    //else actCliente.IdGiro = 0;
    actCliente.DireOfic = $("#txtCalleOficina").val();
    let selIdColOfic = $("#selDistritoOficina option:selected").val();
    if (selIdColOfic) actCliente.IdCoOfic = parseInt(selIdColOfic);
    else actCliente.IdCoOfic = 0;
    actCliente.NumExteriorOfic = $("#txtNumExteriorOficina").val();
    actCliente.NumInteriorOfic = $("#txtNumInteriorOficina").val();
    actCliente.DependenciaOfic = $("#txtDependenciaOficina").val();
    actCliente.UbicacionOfic = $("#txtUbicacion").val();
    actCliente.TeleOfic = $("#txtTelefonoOficina").val();
    actCliente.ExteOfic = $("#txtAnexoOficina").val();
    actCliente.Puesto = $("#txtPuestoOficina").val();
    let txtAntiguedadOficina = $("#txtAntiguedadOficina").val();
    if (txtAntiguedadOficina)
        actCliente.AntigOfic = parseFloat(txtAntiguedadOficina);
    else actCliente.AntigOfic = 0;
    let txtIngresoBruto = $("#txtIngresoBruto").val();
    if (txtIngresoBruto) actCliente.IngresoBruto = parseFloat(txtIngresoBruto);
    else actCliente.IngresoBruto = 0;
    let txtDescuentosOficina = $("#txtDescuentosOficina").val();
    if (txtDescuentosOficina)
        actCliente.DescuentosLey = parseFloat(txtDescuentosOficina);
    else actCliente.DescuentosLey = 0;
    let txtOtrosDescuentos = $("#txtOtrosDescuentos").val();
    if (txtOtrosDescuentos)
        actCliente.OtrosDescuentos = parseFloat(txtOtrosDescuentos);
    else actCliente.OtrosDescuentos = 0;
    let txtIngreso = $("#txtIngreso").val();
    if (txtIngreso) actCliente.Ingreso = parseFloat(txtIngreso);
    else actCliente.Ingreso = 0;
    let txtOtrosIngresos = $("#txtOtrosIngresos").val();
    if (txtOtrosIngresos) actCliente.OtrosIngr = parseFloat(txtOtrosIngresos);
    else actCliente.OtrosIngr = 0;

    // Seccion Cuenta Cobro
    actSolicitud.IdSolicitud = solicitud.IDSolictud;
    let seleccionCobro = $("#selBancoCobro option:selected").val();
    if (seleccionCobro) {
        // Intentar alimentar por ultima vez el campo de cuenta, para evitar inconsistencias
        alimentarCampoCuentaBanco("Cobro", seleccionCobro);
        let CuentaBancariaCobro = $("#txtCuentaBancariaCobro").val();
        actSolicitud.TipoDomiciliacion = 1;
        actSolicitud.Banco = $("#txtNombreBancoCobro").val();

        switch (seleccionCobro) {
            case "CONTINENTAL":
                actSolicitud.BCEntidad = $("#txtEntidadCobro").val();
                actSolicitud.BCOficina = $("#txtOficinaCobro").val();
                actSolicitud.BCDC = $("#txtDCCobro").val();
                actSolicitud.BCCuenta = $("#txtCuentaCobro").val();
                break;
            case "INTERBANK":
                break;
            case "OTRO":
                actSolicitud.CuentaCCI = $("#txtCuentaCCICobro").val();
                break;
        }
        actSolicitud.CuentaDesembolso = CuentaBancariaCobro;
        actSolicitud.CuentaDebito = null;
    }
    // Seccion Domiciliacion Visa
    let guardarVisa = $("#chkVisaDom").prop("checked");
    actSolicitud.TieneTarjetaVisa = guardarVisa;
    if (guardarVisa == true) {
        // TODO: preparar informacion para actualziar
        if ($("#secTarjetaVisa").is(":visible")) {
            actSolicitud.NumeroTarjetaVisa = $("#txtNumeroTarjetaVisa").val();
        }
        actSolicitud.MesExpiraTarjetaVisa = $(
            "#selMesExpiracionVisa option:selected"
        ).val();
        actSolicitud.AnioExpiraTarjetaVisa = $("#txtAñoExpiracionVisa").val();
    }

    // Seccion Cuenta Emergente
    let guardarCuentaEmergente = $("#chkCuentaDomEmergente").prop("checked");
    if (guardarCuentaEmergente == true) {
        //TODO: preparar informacion para actualizar
        let seleccionEmergente = $("#selBancoEmergente option:selected").val();
        if (seleccionEmergente) {
            // Intentar alimentar por ultima vez el campo de cuenta, para evitar inconsistencias
            alimentarCampoCuentaBanco("Emergente", seleccionEmergente);
            let CuentaBancariaEmergente = $("#txtCuentaBancariaEmergente").val();
            actCuentaEmergente.TipoDomiciliacion = 1;
            actCuentaEmergente.Banco = $("#txtNombreBancoEmergente").val();

            switch (seleccionEmergente) {
                case "CONTINENTAL":
                    break;
                case "INTERBANK":
                    break;
                case "OTRO":
                    actCuentaEmergente.CuentaCCI = $("#txtCuentaCCIEmergente").val();
                    break;
            }
            actCuentaEmergente.IdSolicitud = solicitud.IDSolictud;
            actCuentaEmergente.Banco = $("#txtNombreBancoEmergente").val();
            actCuentaEmergente.Cuenta = CuentaBancariaEmergente;
        }
    }
    //   Logs develop
    // console.log("actCuentaEmergente", actCuentaEmergente);
    // console.log("actCliente", actCliente);
    // console.log("actDireccion", actDireccion);
    // console.log("actSolicitud", actSolicitud);
    let seleccionSituacion = $("#selSituacionLaboral option:selected").text();
    seleccionSituacion = seleccionSituacion.toUpperCase();
    if (seleccionSituacion === "JUBILADO" || seleccionSituacion === "PENSIONISTA") {
        actCliente = {
            ...actCliente,
            RegimenPension: 0
        };
        $("#selRegimenPension").val(0);
    }
    actCliente = {
        ...actCliente,
        Origen: getParameterByName('Origen')
    };
    $.ajax({
        type: "POST",
        url: "frmActualizarCliente.aspx/ActualizarInformacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
            solicitud: actSolicitud,
            cliente: actCliente,
            direccion: actDireccion,
            cuentaEmergente: actCuentaEmergente
        }),
        async: false,
        success: function (data) {
            let errores = 0;
            let mensajeError = "";
            if (!isEmpty(data.d)) {
                $.each(data.d, function (i, respuesta) {
                    if (respuesta.Codigo <= 0) {
                        errores++;
                        mensajeError += respuesta.Mensaje + "\n";
                    }
                });

                if (errores > 0) {
                    alert("Ha ocurrido un problema(s) en la operación \n" + mensajeError);
                } else {
                    if (previousPage) window.location.href = previousPage;
                    else {
                        alert("La informacion se actualizó con éxito.");
                    }
                }
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

// Metodos utileria
function esFechaValida(d) {
    return d instanceof Date && !isNaN(d);
}
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) return false;
    }
    return true;
}
function poblarControlSeleccion(control, arregloValores, textoMostrar, valorOpcion) {
    try {
        let objeto = $(control);
        if (
            objeto !== null &&
            arregloValores !== null &&
            arregloValores.length > 0
        ) {
            $(objeto).empty();
            if (!arregloValores.find(objeto => objeto[valorOpcion] == 0))
                $(objeto).append(
                    $("<option>", { value: 0 }).text("0 - SELECCIONE UNA OPCION")
                );
            arregloValores.forEach(function (opcion) {
                $(objeto).append(
                    $("<option>", { value: opcion[valorOpcion] }).text(
                        opcion[textoMostrar]
                    )
                );
            });
        }
    } catch (e) {
        console.log(e);
    }
}
function asignarGenero(genero) {
    if (genero == MASCULINO) {
        $("#chkMasculino").prop("checked", true);
        $("#chkFemenino").prop("checked", false);
    } else if (genero == FEMENINO) {
        $("#chkFemenino").prop("checked", true);
        $("#chkMasculino").prop("checked", false);
    } else {
        $("#chkFemenino").prop("checked", false);
        $("#chkMasculino").prop("checked", false);
    }
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}