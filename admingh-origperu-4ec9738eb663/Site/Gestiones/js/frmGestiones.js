﻿var referencias = [];
var otrasCuentas = [];
var gestiones = [];
var promesas = [];
var llamadas = [];
var avisos = null;
var tipoContactos = [];
var tipoGestor = [];
var campanias = [];
var resultadoGestion = [];
var contactos = [];
var notificaciones = [];
var notificacionPromociones = [];
var destinos = [];
var bitacora = [];
var gestion = null;
var respPago = ["1", "2", "28", "102"];
var respLlamada = ["29", "89"];
var respNotificacion = [];
var id = 0;
var idsolicitud = 0;
var notifActual = 0;
var notifPromoActual = 0;
var idSolicitud = 0;
var tiposArchivo = [];
var documentosDestino = [];
var servidoresArchivo = [];
var archivosGestion = [];

$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); $(".modal-pr").appendTo("body"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});

$(document).ready(function () {
    MostrarReferencias();
    MostrarOtrasCuentas();
    MostrarGestiones();
    MostrarPromesas();
    MostrarDevLlamadas();
    MostrarAvisos();
    MostrarTipoContactos();
    MostrarTipoGestor();
    MostrarCampanias();
    MostrarContacto();
    MostrarNotificaciones();
    MostrarNotificacionPromociones();
    MostrarDestinos();
    //Archivos Gestion
    llenarListaArchivosGestion();

    $("#dSelCnt").hide();
    $("#dNotificacion").hide();
    $("#selTipoContacto").bind("change", OnSelTipoContactoChange);
    $("#selTipoGestor").bind("change", OnTipoGestorChange);
    $("#btnAgregarGest").bind("click", OnbtnAgregarGestClic);
    $("#btnEditarGest").bind("click", OnbtnAgregarGestClic);
    $("#btnCancelarGest").bind("click", LimpiarGestion);
    $("#selResultadoGest").bind("change", OnSelResultadoGestChange);
    $("#btnBuscar").bind("click", OnBtnBuscarClic);
    $("#selNotificacion").bind("change", onSelNotificacionChange);
    $("#selNotificacionPromocion").bind("change", onSelNotificacionPromocionChange);

    //Archivos Gestion
    _archivosGestion.initializarCampos();

    $("#selDestino").bind("change", onSelDestinoChange);
    $('#txFechaProm').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: true,
        language: "es",
        todayHighlight: true
    });
    $('#txFechaDevolLlam').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: true,
        language: "es",
        todayHighlight: true
    });
    $("#spnDescargaNotificacion").bind("click", OnspnDescargaNotificacionClick);
    $("#spnActualizaNotificacion").bind("click", OnspnActualizaNotificacionClick);
    $("#spnDescargaNotificacionPromocion").bind("click", OnspnDescargaNotificacionPromocionClick);
    $("#spnActualizaNotificacionPromocion").bind("click", OnspnActualizaNotificacionPromocionClick);
    $('#txFechaCitacion').datepicker({
        format: "dd/mm/yyyy",
        todayBtn: true,
        language: "es",
        todayHighlight: true
    });
    $('#txVigenciaPromocion').datepicker({
        format: "dd/mm/yyyy",
        todayBtn: true,
        language: "es",
        todayHighlight: true
    });
    $('[data-toggle="tooltip"]').tooltip();
    $("#spnBitacora").bind("click", OnspnBitacoraClick);
    //AQUI
    //$("#selTipoContacto").val(1);
    //$("#selTipoGestor").val(1);
    //OnTipoGestorChange();
    if (tiposArchivo) poblarControlSeleccion("#seltipoMimeArchivoGestion", tiposArchivo, 'TipoArchivo', 'IdTipoArchivoGestion');
    if (documentosDestino) poblarControlSeleccion("#selDocumentoDestino", documentosDestino, 'Descripcion', 'IdDocumentoDestino');
    let selOpAvisos = $("#dAvisos [type=checkbox]");
    if (selOpAvisos.length > 0) selOpAvisos.each(function () {
        $("#" + this.id).bind("change", onChangeChkAviso);;
    });
});

function MostrarReferencias() {
    $("#tReferencias").DataTable({
        destroy: true,
        searching: false,
        data: referencias,
        columns: [
            { data: "Referencia", title: "Referencia" },
            { data: "Lada", title: "Lada" },
            { data: "Telefono", title: "Telefono" },
            { data: "TelefonoCelular", title: "Celular" },
            { data: "Parentesco", title: "Parentesco" }
        ],
        language: español,
        scrollX: true,
        order: [[0, "asc"]]
    });
}

function MostrarOtrasCuentas() {
    $("#tOtrasCuentas").DataTable({
        destroy: true,
        searching: false,
        data: otrasCuentas,
        columns: [
            {
                data: "Cuenta", title: "Cuenta", render: function (data, type, row) {
                    if (type === "display") {
                        let strOtraC = '<a href="frmGestiones.aspx?idcuenta=' + row.Credito + '">' + data + '</a>';
                        return strOtraC;
                    } else { return ""; }
                }
            },
            { data: "Credito", title: "Credito" },
            { data: "Capital", title: "Capital" },
            { data: "FechaCredito", title: "Fecha de Credito" },
            { data: "Interes", title: "Interes" },
            { data: "IvaInteres", title: "Iva Interes" },
            { data: "Erogacion", title: "Erogacion" },
            { data: "SaldoVencido", title: "Saldo Vencido" },
            { data: "SaldoVigente", title: "Saldo Vigente" },
            { data: "Banco", title: "Banco" },
            { data: "Clabe", title: "CLABE" },
        ],
        language: español,
        scrollX: true,
        order: [[0, "asc"]]
    });
}

function MostrarGestiones() {
    var contGestiones = $("#contGestiones");
    $(contGestiones).empty();
    if (gestiones != null) {
        gestiones.forEach(function (gestion) {
            var strGest = '<div id="gestion_' + gestion.Id + '" class="row gestion">';
            if (gestion.Tipo.length > 0)
                strGest += '<span class="gestion-Tipo ' + gestion.Tipo + '"></span>';
            strGest += '<span class="gestion-gestor">' + gestion.Gestor + '</span>';
            strGest += '<span class="glyphicon glyphicon-menu-right"></span>';
            if (gestion.Contacto.length > 0)
                strGest += '<span class="gestion-resultado label-primary">' + gestion.Contacto + '</span>';
            strGest += '<span class="gestion-resultado label-';
            strGest += gestion.Alerta ? 'danger' : 'default';
            strGest += '">' + gestion.ResultadoGestion + '</span>';
            if (gestion.Campania.length > 0)
                strGest += '<span class="gestion-resultado label-primarygest">' + gestion.Campania + '</span>';
            strGest += '<span class="gestion-fch">' + gestion.Fecha + '</span>';
            strGest += '<span class="gestion-comando editar glyphicon glyphicon-pencil"></span>';
            strGest += '<span class="gestion-com">' + gestion.Comentario + '</span>';
            strGest += '</div>';
            $(contGestiones).append(strGest);
            $("#gestion_" + gestion.Id + " .editar").bind("click", function () { OnEditarGestionClic(gestion.Id); });
        });
    }
}

function MostrarPromesas() {
    var contPromesas = $("#contPromesas");
    $(contPromesas).empty();
    if (promesas != null) {
        promesas.forEach(function (promesa) {
            var strProm = '<div class="row gestion">';
            strProm += '<div class="col-sm-8 gestion-prom">';
            strProm += '<span class="promesa-fecha">' + promesa.FchaPromesa + '</span>';
            strProm += '<span class="promesa-tipoCob">' + promesa.TipoCobro + '</span>';
            strProm += '</div>';
            strProm += '<div class="col-sm-4 promesa-colmonto">';
            strProm += '<span class="promesa-monto"> ' + promesa.Monto + '</span > ';
            strProm += '</div>';
            strProm += '</div>';
            $(contPromesas).append(strProm);
        });
    }
}

function MostrarDevLlamadas() {
    var contLlamadas = $("#contLlamadas");
    $(contLlamadas).empty();
    if (llamadas != null) {
        llamadas.forEach(function (promesa) {
            var strProm = '<div class="row gestion">';
            strProm += '<div class="col-sm-3 DevLlamada-fecha">';
            strProm += '<span class="llamada-fecha">' + promesa.FchDevLlamada + '</span>';

            strProm += '</div>';

            strProm += '<div class="col-sm-8 DevLlamada-resultado">';
            strProm += '<span class="llamada-resul">' + promesa.ResultadoGestion + '</span>';
            strProm += '</div>';


            strProm += '<div class="col-sm-12 DevLlamada-coment">';
            strProm += '<span class="llamada-comentario">' + promesa.Comentario + '</span > ';
            strProm += '</div>';
            strProm += '</div>';
            $(contLlamadas).append(strProm);
        });
    }
}

function MostrarAvisos() {
    if (avisos != null) {
        if (avisos.Carteo == 1) $("#opCartero").prop("checked", true);
        if (avisos.sms == 1) $("#opSms").prop("checked", true);
        if (avisos.watsapp == 1) $("#opWatsApp").prop("checked", true);
        if (avisos.mailing == 1) $("#opMail").prop("checked", true);
        if (avisos.circuloCredito == 1) $("#opCircCred").prop("checked", true);
        if (avisos.fisico == 1) $("#opAvisoFis").prop("checked", true);
    }
}

function MostrarTipoContactos() {
    $("#selTipoContacto").empty();
    if (tipoContactos != null && tipoContactos.length > 0) {
        tipoContactos.forEach(function (tipoContacto) {
            $('#selTipoContacto')
                .append($('<option>', { value: tipoContacto.id })
                    .text(tipoContacto.contacto));
        });
    }
}

function MostrarTipoGestor() {
    $("#selTipoGestor").empty();
    if (tipoGestor != null && tipoGestor.length > 0) {
        tipoGestor.forEach(function (tipoGestor) {
            $('#selTipoGestor')
                .append($('<option>', { value: tipoGestor.IdTipoGestorContacto })
                    .text(tipoGestor.Contacto));
        });
    }
}

function MostrarCampanias() {
    $("#selCampania").empty();
    if (campanias != null && campanias.length > 0) {
        campanias.forEach(function (campania) {
            $('#selCampania')
                .append($('<option>', { value: campania.IdCampana })
                    .text(campania.Descripcion));
        });
    }
}

function MostrarNotificaciones() {
    $("#selNotificacion").empty();
    if (notificaciones != null && notificaciones.length > 0) {
        notificaciones.forEach(function (notificacion) {
            $('#selNotificacion')
                .append($('<option>', { value: notificacion.IdNotificacion })
                    .text(notificacion.Notificacion));
        });
    }
}

function MostrarDestinos() {
    $("#selDestino").empty();
    if (destinos != null && destinos.length > 0) {
        destinos.forEach(function (destino) {
            $('#selDestino')
                .append($('<option>', { value: destino.IdDestinoNotificacion })
                    .text(destino.DestinoNotificacion));
        });
    }
}

function MostrarNotificacionPromociones() {
    $("#selNotificacionPromocion").empty();
    if (notificacionPromociones != null && notificacionPromociones.length > 0) {
        notificacionPromociones.forEach(function (notificacionPromocion) {
            $('#selNotificacionPromocion')
                .append($('<option>', { value: notificacionPromocion.IdNotificacion })
                    .text(notificacionPromocion.Notificacion));
        });
    }
}

function OnSelTipoContactoChange() {
    if ($("#selTipoContacto").val() == 1) {
        $("#dSelCnt").show();
    } else {
        $("#dSelCnt").hide();
    }
}

function OnTipoGestorChange() {
    let selGestor = $("#selTipoGestor").val();
    sucursal = $("#selSucursales").val();

    let parametros = {
        url: "frmGestiones.aspx/ObtenerCatalogoResultados",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { opcionGestor: selGestor }
    };
    $.ajax(parametros)
        .done(function (data) {
            resultadoGestion = [];
            if (data.d != null) {
                resultadoGestion = data.d;
                MostrarResultadosGestion();
            }
        })
        .fail(function (err) {
        });
}

function MostrarResultadosGestion() {
    $("#selResultadoGest").empty();
    if (resultadoGestion != null && resultadoGestion.length > 0) {
        resultadoGestion.forEach(function (resultdoGestion) {
            $('#selResultadoGest')
                .append($('<option>', { value: resultdoGestion.IdResultado })
                    .text(resultdoGestion.Descripcion));
        });
    }
}

function MostrarContacto() {
    $("#selCnt").empty();
    if (contactos != null && contactos.length > 0) {
        contactos.forEach(function (contacto) {
            $('#selCnt')
                .append($('<option>', { value: contacto.id })
                    .text(contacto.descripcion));
        });
    }
}

function DatosGestion() {
    gestion = {
        Id: id,
        IdCampania: 0,
        IdResultadoGestion: 0,
        IdContacto: 0,
        Comentario: "",
        FechaPromesa: null,
        MontoPromesa: null,
        FechaLlamada: null,
        Avisos: []
    }

    gestion.IdCampania = $("#selCampania").val();
    gestion.IdResultadoGestion = $("#selResultadoGest").val();
    gestion.IdContacto = $("#selCnt").val();
    gestion.Comentario = $("#txComentarios").val();
    if (respPago.includes(gestion.IdResultadoGestion)) {
        gestion.FechaPromesa = $("#txFechaProm").val();
        gestion.MontoPromesa = $("#txMontoProm").val();
    }
    if (respLlamada.includes(gestion.IdResultadoGestion)) {
        gestion.FechaLlamada = $("#txFechaDevolLlam").val();
    }
    if (respNotificacion.includes(gestion.IdResultadoGestion)) {
        $("#dNotificacion").show();
        $("#dNotificacionCitacion").hide();
        $("#dNotificacionPromocion").hide();
        ObtenerEstadoNotificacion();
        ObtenerBitacoraNotificacion();
    }

}

function OnbtnAgregarGestClic() {
    DatosGestion();
    if (ValidarDatosGestion())
        GuardarGestion();
}

function OnSelResultadoGestChange() {
    $("#txFechaProm").val("");
    $("#txMontoProm").val("");
    $("#txFechaDevolLlam").val("");
    $("#txFechaCitacion").val("");
    $("#txHoraCitacion").val("");
    $("#dFechaDevLlam").hide();
    $("#dPromesaPag").hide();
    $("#dNotificacion").hide();

    let opSel = $("#selResultadoGest").val();
    if (respPago.includes(opSel)) {
        $("#dPromesaPag").show();
    }
    else if (respLlamada.includes(opSel)) {
        $("#dFechaDevLlam").show();
    }
    else if (respNotificacion.includes(opSel)) {
        $("#dNotificacion").show();
        $("#dNotificacionCitacion").hide();
        $("#dNotificacionPromocion").hide();
        ObtenerEstadoNotificacion();
        ObtenerBitacoraNotificacion();
    }
}

function ValidarDatosGestion() {
    let valCampos = [];
    if (gestion != null) {
        if (gestion.IdCampania <= 0) valCampos.push("Elija una camapaña");
        if (gestion.IdResultadoGestion <= 0) valCampos.push("Elija un resultado para la gestión");
        if (gestion.Comentario == "" || gestion.Comentario.trim().length <= 0) valCampos.push("Ingrese un comentario para la gestión");
    }
    if (valCampos.length > 0) {
        alert(valCampos.join('\n'));
    }
    return valCampos.length == 0;
}

function GuardarGestion() {
    if (gestion != null) {
        let qStr = "?" + window.location.href.split("?")[1];
        let parametros = {
            url: "frmGestiones.aspx/GuardarGestion" + qStr,
            contentType: "application/json",
            dataType: "json",
            method: 'POST',
            timeout: 2 * 60 * 1000,
            data: JSON.stringify({ gestion: gestion })
        };
        $.ajax(parametros)
            .done(function (respuesta) {
                let resultado = respuesta.d;
                if (resultado != null) {
                    gestion = null;
                    if (resultado.Error) {
                        if (resultado.Mensaje != null && resultado.Mensaje.length > 0) {
                            alert(resultado.Mensaje);
                        } else {
                            alert("Ocurrió un error al momento de guarda la gestión");
                        }
                        if (resultado.Url != null && resultado.Url.length > 0) {
                            window.location = resultado.Url;
                        }
                    } else {
                        if (resultado.Mensaje != null && resultado.Mensaje.length > 0) {
                            alert(resultado.Mensaje);
                        } else {
                            alert("La gestión se guardó correctamente!");
                        }
                        LimpiarGestion();
                    }
                    if (resultado.Gestiones != null && resultado.Gestiones.length > 0) {
                        gestiones = resultado.Gestiones;
                        MostrarGestiones();
                    }
                    if (resultado.Promesas != null && resultado.Promesas.length > 0) {
                        promesas = resultado.Promesas;
                        MostrarPromesas();
                    }
                    if (resultado.Llamadas != null && resultado.Llamadas.length > 0) {
                        llamadas = resultado.Llamadas;
                        MostrarDevLlamadas();
                    }
                } else {
                    alert("Ocurrió un error al momento de guarda la gestión");
                }
            })
            .fail(function (error) {
                alert("Ocurrió un error al momento intentar guardar la gestión. Vuelva a intentar.");
            });
    }
}

function ValidaMonto(obj) {
    let valido = false;
    valido = (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46;
    if (event.charCode == 46) {
        valido = !$(obj).val().includes(".");
    }
    if (valido && $(obj).val().includes(".")) {
        let str = $(obj).val().split('.');
        valido = !(str[1].length + 1 > 2);
    }
    return valido;
}

function LimpiarGestion() {
    $("#selTipoContacto").val("-1");
    $("#selTipoGestor").val("0");
    $("#selCampania").val("0");
    $("#selResultadoGest").val("0");
    $("#selCnt").val("2");
    $("#txComentarios").val("");
    $("#txFechaProm").val("");
    $("#txMontoProm").val("");
    $("#txFechaDevolLlam").val("");
    $("#selNotificacion").val("-1");
    $("#selNotificacionPromocion").val("-1");
    $("#txFechaCitacion").val("");
    $("#txHoraCitacion").val("");
    gestion = null;
    id = 0;
    $("#btnAgregarGest").show();
    $("#btnEditarGest").hide();
    $("#btnCancelarGest").hide();
    $("#contGestiones .row.gestion").removeClass("gestion-editando");
}

function OnEditarGestionClic(gestEditar) {
    let parametros = {
        url: "frmGestiones.aspx/ObtenerGestion",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { id: gestEditar }
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d != null) {
                let respGest = data.d;
                if (gestEditar == respGest.Id) {
                    $("#btnAgregarGest").hide();
                    $("#btnEditarGest").show();
                    $("#btnCancelarGest").show();
                    $("#contGestiones .row.gestion").removeClass("gestion-editando");
                    $("#gestion_" + gestEditar).addClass("gestion-editando");
                    $("#selTipoContacto").val(respGest.IdTipoContacto);
                    $("#selCampania").val(respGest.IdCampania);
                    $("#txComentarios").val(respGest.Comentario);
                    id = gestEditar;
                } else { alert("Los datos de la gestion no son correctos.") }
            } else {
                alert("No fue posible obtener la gestión");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de intentar obtener la gestión");
        });
}

function OnBtnBuscarClic() {
    let cuenta = $("input[id$='txCuenta']").val().trim();
    window.location = "frmGestiones.aspx?bCta=1&idsolicitud=" + cuenta;
}

function OnspnDescargaNotificacionClick() {
    if (parseInt($("#selDestino").val()) <= 0) {
        alert("Debe seleccionar una notificación y destino válidos.");
        return false;
    }
    if ($("#selNotificacion").val() === "5" && (!document.getElementById("txFechaCitacion").value || !document.getElementById("txHoraCitacion").value)) {
        alert("Debe seleccionar una fecha y hora para la cita.");
        return false;
    }
    const notificacion = {
        IdUsuario: 0,
        IdSolicitud: parseInt(idsolicitud),
        Clave: notificaciones.filter(x => x.IdNotificacion == $("#selNotificacion").val())[0].Clave,
        IdDestino: parseInt($("#selDestino").val()),
        FechaCitacion: $("#txFechaCitacion").val(),
        HoraCitacion: $("#txHoraCitacion").val()
    };
    let parametros = {
        url: "frmGestiones.aspx/GenerarNotificacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ notificacion: notificacion })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);

                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                alert("No fue posible obtener la notificación.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de intentar obtener la notificación.");
        });
}

function OnspnActualizaNotificacionClick() {
    if (parseInt($("#selDestino").val()) <= 0) {
        alert("Debe seleccionar una notificación y destino válidos.");
        return false;
    }
    if ($("#selNotificacion").val() === "5" && (!document.getElementById("txFechaCitacion").value || !document.getElementById("txHoraCitacion").value)) {
        alert("Debe seleccionar una fecha y hora para la cita.");
        return false;
    }
    const notificacion = {
        IdUsuario: 0,
        IdSolicitud: parseInt(idsolicitud),
        Clave: notificaciones.filter(x => x.IdNotificacion == $("#selNotificacion").val())[0].Clave,
        IdDestino: parseInt($("#selDestino").val()),
        FechaCitacion: $("#txFechaCitacion").val(),
        HoraCitacion: $("#txHoraCitacion").val()
    };
    let parametros = {
        url: "frmGestiones.aspx/ActualizarNotificacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ notificacion: notificacion })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);
                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                alert("No fue posible obtener la notificación.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de intentar obtener la notificación.");
        });
}

function ObtenerEstadoNotificacion() {
    let parametros = {
        url: "frmGestiones.aspx/ObtenerEstadoNotificacion",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { idsolicitud: idsolicitud }
    };

    $.ajax(parametros)
        .done(function (data) {
            if (data.d !== null) {
                let resNotificacion = data.d;
                notifActual = resNotificacion.IdNotificacion;
                $("#selNotificacion").val(resNotificacion.IdNotificacion);
                $("#selDestino").val(resNotificacion.Destino === 0 ? -1 : resNotificacion.Destino);
                RevisaNotificacionCitacion();
                ValidarPermisosImpresion();
            } else {
                alert("No fue posible obtener el estado de envios de notificación. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener el estado de envios de notificación. " + data.MensajeOperacion);
        });
}

function ObtenerBitacoraNotificacion() {
    let parametrosBitacora = {
        url: "frmGestiones.aspx/ObtenerBitacoraNotificacion",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { idsolicitud: idsolicitud }
    };

    $.ajax(parametrosBitacora)
        .done(function (data) {
            if (data.d !== null) {
                bitacora = data.d;
                $("#tBitacora").DataTable({
                    destroy: true,
                    searching: false,
                    data: bitacora,
                    columns: [
                        { data: "Notificacion", title: "Notificacion" },
                        { data: "Destino", title: "Destino" },
                        { data: "Accion", title: "Acción" },
                        { data: "Usuario", title: "Usuario" },
                        { data: "Fecha", title: "Fecha" }
                    ],
                    language: español,
                    scrollX: true,
                    autoWidth: true,
                    order: [[4, "des"]]
                });
            } else {
                alert("No fue posible obtener la bitácora de notificaciones, o no hay movimientos registrados. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener la bitácora de notificaciones. " + data.MensajeOperacion);
        });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function onSelNotificacionChange() {
    RevisaNotificacionCitacion();
    ValidarPermisosImpresion();
}

function ValidarPermisosImpresion() {
    // BLOQUEAR NOTIFICACIONES POSTERIORES A LA NOTIFICACION ACTUAL
    if (typeof notificaciones !== "undefined" && notificaciones !== null && notificaciones.length !== null && notificaciones.length > 0) {
        let ordenActual = notificaciones.filter(x => x.IdNotificacion == notifActual)[0].Orden;
        let opcsNotifsBloquear = notificaciones.filter(x => x.Orden > ordenActual).map(x => x.IdNotificacion);
        $("#selNotificacion > option").each(function () {
            if (jQuery.inArray(parseInt(this.value), opcsNotifsBloquear) !== -1) {
                $('#selNotificacion > option[value="' + this.value + '"]').attr('disabled', 'disabled');
                //TODO: NO SE VE REFLEJADO EL CURSOR not-allowed Y NO ACTUALIZA
                $('#selNotificacion > option[value="' + this.value + '"]').css('cursor', 'not-allowed');
            }
        });
    }
}

function onSelNotificacionPromocionChange() {
    RevisaNotificacionPromocionReduccionMora();
}

function OnspnDescargaNotificacionPromocionClick() {
    if (parseInt($("#selDestino").val()) <= 0) {
        alert("Debe seleccionar un destino válido.");
        return false;
    }
    if ($("#selNotificacionPromocion").val() === "1" && (!document.getElementById("txDescuentoPromocion").value || !document.getElementById("txVigenciaPromocion").value)) {
        alert("Debe seleccionar un descuento y una vigencia para la promoción.");
        return false;
    }
    const notificacion = {
        IdUsuario: 0,
        IdSolicitud: parseInt(idsolicitud),
        Clave: notificacionPromociones.filter(x => x.IdNotificacion == $("#selNotificacionPromocion").val())[0].Clave,
        IdDestino: parseInt($("#selDestino").val()),
        DescuentoDeuda: parseFloat($("#txDescuentoPromocion").val()),
        FechaVigencia: $("#txVigenciaPromocion").val()
    };
    let parametros = {
        url: "frmGestiones.aspx/GenerarNotificacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ notificacion: notificacion })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);
                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                alert("No fue posible obtener la promoción.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de intentar obtener la promoción.");
        });
}

function OnspnActualizaNotificacionPromocionClick() {
    if (parseInt($("#selDestino").val()) <= 0) {
        alert("Debe seleccionar un destino válido.");
        return false;
    }
    if ($("#selNotificacionPromocion").val() === "1" && (!document.getElementById("txDescuentoPromocion").value || !document.getElementById("txVigenciaPromocion").value)) {
        alert("Debe seleccionar un descuento y una vigencia para la promoción.");
        return false;
    }
    const notificacion = {
        IdUsuario: 0,
        IdSolicitud: parseInt(idsolicitud),
        Clave: notificacionPromociones.filter(x => x.IdNotificacion == $("#selNotificacionPromocion").val())[0].Clave,
        IdDestino: parseInt($("#selDestino").val()),
        DescuentoDeuda: parseFloat($("#txDescuentoPromocion").val()),
        FechaVigencia: $("#txVigenciaPromocion").val()
    };
    let parametros = {
        url: "frmGestiones.aspx/ActualizarNotificacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ notificacion: notificacion })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);
                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                alert("No fue posible obtener la promoción.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de intentar obtener la promoción.");
        });
}

function RevisaNotificacionCitacion() {
    if ($("#selNotificacion").val() === "5") {
        if (parseInt($("#selDestino").val()) > 0) {
        $("#dNotificacionCitacion").show();
            CargaDatosNotificacion($("#selNotificacion").val());
        }
    }
    else
        $("#dNotificacionCitacion").hide();
}

function RevisaNotificacionPromocionReduccionMora() {
    if ($("#selNotificacionPromocion").val() === "6") {
        if (parseInt($("#selDestino").val()) > 0) {
        $("#dNotificacionPromocion").show();
            CargaDatosNotificacion($("#selNotificacionPromocion").val());
        }
    }
    else
        $("#dNotificacionPromocion").hide();
}

function OnspnBitacoraClick() {
    ObtenerBitacoraNotificacion();
    if (bitacora.length <= 0)
        return false;
    $("#modalBitacoraNotificacion").modal("show");
    var table = $('#tBitacora').DataTable();
    $('#modalBitacoraNotificacion').css('display', 'block');
    table.columns.adjust().draw();
    $("#modalBitacoraNotificacion").appendTo("body");
}

function CargaDatosNotificacion(idnotificacion) {
    let parametros = {
        url: "frmGestiones.aspx/ObtenerDatosNotificacion",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { idsolicitud: idsolicitud, idnotificacion: idnotificacion, iddestino: parseInt($("#selDestino").val()) }
    };

    $.ajax(parametros)
        .done(function (data) {
            if (data.d !== null) {
                let resNotificacion = data.d;
                switch (idnotificacion) {
                    case "5":
                        $("#txFechaCitacion").val(resNotificacion.FechaCitacion);
                        $("#txHoraCitacion").val(resNotificacion.HoraCitacion);
                        break;
                    case "6":
                        $("#txDescuentoPromocion").val(resNotificacion.DescuentoDeuda);
                        $("#txVigenciaPromocion").val(resNotificacion.FechaVigencia);
                        break;
                    default:
                        break;
                }
                if (resNotificacion.Destino > 0)
                    $("#selDestino").val(resNotificacion.Destino);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener los datos de la citación. " + data.MensajeOperacion);
        });
}

function onSelDestinoChange() {
    RevisaNotificacionCitacion();
    RevisaNotificacionPromocionReduccionMora();
}
function onChangeChkAviso() {
    guardarAvisosGestion();
}
function guardarAvisosGestion() {
    let selOpAvisos = $("#dAvisos [type=checkbox]:checked");
    let tempAvisos = [];
    if (selOpAvisos.length > 0) selOpAvisos.each(function () {
        tempAvisos.push(this.value);
    });

    console.log("Guardando avisos...");
    let idCuenta = getParameterByName('idcuenta');
    let parametros = {
        url: "frmGestiones.aspx/GuardarAvisos",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ idCuenta: idCuenta, actAvisos: tempAvisos })
    };
    $.ajax(parametros)
        .done(function (data) {
        })
        .fail(function (err) {
        });
}
function getDateFromAspNetFormat(date) {
    const re = /^\/Date\((-?[\+\d]+)\)\/$/;
    const m = re.exec(date);
    if (m && m.length > 1) {
        return parseInt(m[1]);
    }
    return null;
}
