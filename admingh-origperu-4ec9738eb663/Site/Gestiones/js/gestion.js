﻿var tipoContactos = [];
var resultadoGestion = [];
var configAgenda = {};
$(document).ready(function () {
    mostrarResultadosGestion();
    $("#btnGuardarGestTel").click(gestionTelefonica);
    $("#opAgenda").click(agendarFecha);
    $('#txFechaAgendaSel').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    if (configAgenda.HoraMinAgenda != null) {
        let fechaMin = moment();
        if (configAgenda.HoraMaxAgenda == null) configAgenda.HoraMaxAgenda = moment(moment().format("YYYY-MM-DD") + " 23:00").format();
        if (!(moment(configAgenda.HoraMinAgenda).format("HH:mm") > moment().format("HH:mm") < moment(configAgenda.HoraMaxAgenda).format("HH:mm"))) {
            if (moment().format("HH:mm") >= moment(configAgenda.HoraMaxAgenda).format("HH:mm"))
                fechaMin.add(1, 'day');
        }
        $("#txFechaAgendaSel").data("DateTimePicker").minDate(moment(fechaMin).format("DD/MM/YYYY"));
    }
    $("#txFechaAgendaSel").on("dp.hide", horasValidas);
    $('#txHoraAgendaSel').datetimepicker({ locale: 'es', format: 'hh:mm a' });
    if (configAgenda.HoraMaxAgenda != null) {
        $("#txHoraAgendaSel").data("DateTimePicker").maxDate(moment(configAgenda.HoraMaxAgenda).format("hh:mm a"));
    }
    $("#btnAgendar").click(guardarAgenda);
    $("#opLlamar").on("DOMSubtreeModified", function () {
        if ($(this).prop("disabled")) {
            selTelefonoContacto();
        }
    });
    $("#btnSelGestor").click(function () {
        gestorNotificar($("#selGestores").val(), true);
    });
    $("#chSoloAmiAg").click(function () {
        let sel = $("#chSoloAmiAg").prop("checked");
        $("#dGestores").collapse(sel ? "hide" : "show");
        if (sel) {
            configAgenda.Gestores = [];
            gestoresAsignados();
            gestoresNotificar();
        }
    });
    $("#modalFechaAgenda").on("hide.bs.modal", function () {
        validaAgenda();
    });
    tipoTelefonoContactos();
    gestiones();
    gestoresAsignados();
});
function mostrarResultadosGestion() {
    $("#selEstadoLlamada").empty();
    if (resultadoGestion != null && resultadoGestion.length > 0) {
        resultadoGestion.forEach(function (resultado) {
            $('#selEstadoLlamada')
                .append($('<option>', { value: resultado.IdResultado })
                    .text(resultado.Descripcion));
        });
    }
}
function tipoTelefonoContactos() {
    let qs = new URLSearchParams(window.location.search);
    let solicitud = qs.has("idsolicitud") ? qs.get("idsolicitud") : 0;
    $('#selTelefono').empty();
    let parametros = {
        url: "Gestion.aspx/TipoTelefonoContactos",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'GET',
        data: { idSolicitud: solicitud, idTipoContacto: 0 }
    };
    $("#tContactos tbody").empty();
    $.ajax(parametros)
        .done(function (respuesta) {
            let resp = respuesta.d;
            if (resp != undefined) {
                if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                if (resp.contactos != null) {
                    resp.contactos.forEach((contacto) => {
                        let extRef = [];
                        let reg = `<tr>`
                        reg += `<td>${contacto.Telefono}</td><td>${contacto.TipoTelefono}${contacto.Parentesco != null && contacto.Parentesco !== "" ? " / " + contacto.Parentesco : ""}</td>`;
                        reg += `<td>`;
                        if (contacto.Extension != null && contacto.Extension !== "") extRef.push(`Ext - ${contacto.Extension}`);
                        if (contacto.NombreContacto != null && contacto.NombreContacto !== "") extRef.push(`${contacto.NombreContacto}`);
                        if (extRef.length > 0) {
                            reg += extRef.join(" / ");
                        }
                        reg += `</td><td class="selTelefono"><input type="checkbox" value="${contacto.Telefono}" onclick="selContacto(this)" class="checkbox-inline" /></td></tr>`;
                        $("#tContactos tbody").append(reg);
                    });
                }
            } else {
                alert("Ocurrió un error al momento de actualizar los datos.");
            }
        })
        .fail(function (error) {
            alert("Ocurrió un error al momento actualizar los datos.");
        });
}
function selTelefonoContacto() {
    let activo = $("#tContactos input[type=checkbox]:checked").val() > 0 ? true : false;
    $("#selEstadoLlamada").prop("disabled", !activo);
    $("#textObservacion").prop("disabled", !activo);
    $("#opAgenda").prop("disabled", !activo);
    $("#btnGuardarGestTel").prop("disabled", !activo);
}
function gestionTelefonica() {
    let gestionTel = {
        IdCuenta: idCuenta,
        IdTipoContacto: 0,
        IdResultadoGestion: $("#selEstadoLlamada").val()
    }
    let agendar = {};
    if ($("#txFechaAgenda").val() != "") {
        agendar.Fch_DevLlamada = moment($("#txFechaAgenda").val(), "DD-MM-YYYY hh:mm a").format();
        agendar.Telefono = $("#tContactos input[type=checkbox]:checked").val();
        agendar.Comentario = $("#textObservacion").val();
        $("#tContactos tbody tr").map(function (i, o) {
            let cols = $(o).find("td");
            if (cols != null) {
                if (cols[0].innerHTML == agendar.Telefono) {
                    agendar.Tipo = cols[1].innerHTML;
                    agendar.AnexoReferencia = cols[2].innerHTML;
                }
            }
        });
    }
    if (gestionTel.IdResultadoGestion <= 0) { alert("Capture el resultado de la llamada"); return };
    if ($("#textObservacion").val() == "") { alert("Capture un comentario"); return; }
    gestionTel.Comentario = `Llamada al teléfono: ${agendar.Telefono} - ${agendar.AnexoReferencia}. ${$("#textObservacion").val()}`;
    if (configAgenda.Gestores != null && configAgenda.Gestores.length > 0)
        agendar.IdGestoresNotificar = configAgenda.Gestores;
    let parametros = {
        url: "Gestion.aspx/GestionTelefonica",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ gestionTel: gestionTel, devolverLlamada: agendar })
    };
    $.ajax(parametros)
        .done(function (respuesta) {
            let resp = respuesta.d;
            if (resp != undefined) {
                if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                nuevoGestionTel();
            } else {
                alert("Ocurrió un error al momento de guardar la gestión telefónica");
            }
        })
        .fail(function (error) {
            alert("Ocurrió un error al momento intentar guardar la gestión telefónica. Vuela a intentar.");
        });
}
function nuevoGestionTel() {
    $("#txFechaAgenda").val("");
    $("#selEstadoLlamada").val("0");
    $("#textObservacion").val("");
    $("#txFechaAgendaSel").val("");
    $("#txHoraAgendaSel").val("");
    $("#txCometnarioAg").val("");
    $("#tContactos input[type=checkbox]:checked").map(function (i, o) {
        $(o).prop("checked", false);
    });
    $("#opLlamar").prop("disabled", true);
    configAgenda.Gestores = [];
    gestoresAsignados();
    gestoresNotificar();
}
function agendarFecha() {
    $("#modalFechaAgenda").modal('show').appendTo("body");
    $("#modalFechaAgenda").css("z-index", 9999);
}
function gestiones() {
    $('#tgestiones').DataTable({
        destroy: true,
        searching: false,
        ordering: false,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "Gestion.aspx/CargarGestiones",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d != undefined) {
                    if (json.d.url.length > 0)
                        window.location = json.d.url;
                    if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                }
                return JSON.stringify(json.d);
            }
        },
        columns: [
            { data: "IdSolicitud", title: "Descripción de Gestión" },
            { data: "Fecha", title: "Fecha" },
            { data: "Monto", title: "Monto" },
            { data: "ResultadoGestion", title: "Resultado de gestión" },
            { data: "&nbsp;", title: "Generar recordatorio" },
        ],
        columnDefs: [
        ],
        createdRow: function (row, data, index) {
        },
        language: español,
        order: [[0, "asc"]]
    });
}
function selContacto(o) {
    let checks = $("#tContactos input[type=checkbox]:checked");
    if (checks.length > 1) {
        checks.map(function (i, d) {
            if ($(d).val() != $(o).val()) {
                $(d).prop("checked", !$(o).prop("checked"));
            }
        });
    }
    $("#opLlamar").prop("disabled", !$(o).prop("checked"));
}
function horasValidas(e) {
    $('#txHoraAgendaSel').val("");
    if (configAgenda.HoraMinAgenda != null && configAgenda.HoraMaxAgenda != null) {
        let fsel = moment($('#txFechaAgendaSel').val(), "DD-MM-YYYY");
        let factual = moment();
        let horaMin = moment();
        if (fsel.format("YYYY-MM-DD") > factual.format("YYYY-MM-DD")
            || factual.format("HH:mm") <= moment(configAgenda.HoraMinAgenda).format("HH:mm")) horaMin = moment(configAgenda.HoraMinAgenda).format("HH:mm");
        else horaMin = factual.format("HH:mm");

        $('#txHoraAgendaSel').data("DateTimePicker").minDate(horaMin);
    }
}
function guardarAgenda() {
    if (!validaAgenda()) {
        $("#modalFechaAgenda").modal("hide");
    }
}
function validaAgenda() {
    $("#txFechaAgenda").val("");
    $("#textObservacion").val("");
    $("#modalFechaAgenda .has-error").map((i, o) => {
        $(o).removeClass("has-error");
    });
    if ($("#txFechaAgendaSel").val() == "") $("#txFechaAgendaSel").parent().addClass("has-error");
    if ($("#txHoraAgendaSel").val() == "") $("#txHoraAgendaSel").parent().addClass("has-error");
    if ($("#txCometnarioAg").val() == "") $("#txCometnarioAg").parent().addClass("has-error");
    let errores = $("#modalFechaAgenda .has-error").length > 0;
    if (!errores) {
        $("#textObservacion").val($("#txCometnarioAg").val());
        $("#txFechaAgenda").val(`${$("#txFechaAgendaSel").val()} ${$("#txHoraAgendaSel").val()}`);
    }
    return errores;
}
function gestoresAsignados() {
    $("#selGestores").empty();
    if (gestoresA != undefined && gestoresA != null) {
        gestoresA.forEach((o, i) => {
            if (configAgenda.Gestores != null && configAgenda.Gestores.length > 0 && configAgenda.Gestores.includes(o.IdGestor.toString())) return;
            $("#selGestores").append($("<option>", { value: o.IdGestor, text: o.Nombre }));
        });
    }
    $("#selGestores").val("0");
}
function gestorNotificar(idGestor, seleccionado) {
    if (configAgenda.Gestores == undefined || configAgenda.Gestores == null) configAgenda.Gestores = [];
    if (idGestor <= 0) return;
    let posicion = -1;
    configAgenda.Gestores.map((o, i) => {
        if (o == idGestor) {
            posicion = i;
            return;
        }
    });
    if (seleccionado && posicion < 0) configAgenda.Gestores.push(idGestor);
    else if (!seleccionado && configAgenda.Gestores.length > 0 && posicion >= 0) configAgenda.Gestores.splice(posicion, 1);
    gestoresAsignados();
    gestoresNotificar();
}
function gestoresNotificar() {
    $("#gestoreNotificar").empty();
    if (configAgenda.Gestores != null && configAgenda.Gestores.length > 0
        && gestoresA != null && gestoresA.length > 0) {
        configAgenda.Gestores.forEach((o, i) => {
            for (let i = 0; i < gestoresA.length; i++) {
                if (gestoresA[i].IdGestor.toString() == o) {
                    let gest = `<li class="list-group-item">
                        <span class="glyphicon glyphicon-user"></span>&nbsp;${gestoresA[i].Nombre}
                        <i class="remover glyphicon glyphicon-remove" onclick="gestorNotificar(${o}, false);"></i>
                        </li > `;
                    $("#gestoreNotificar").append(gest);
                    return;
                }
            }
        });
    }
}