﻿var contenido = [];
$(document).on({
    ajaxStart: function () { $("#divLoader").removeClass("hidden"); },
    ajaxStop: function () { $("#divLoader").addClass("hidden"); }
});
let dcont = new ResizeObserver((e) => {
    if ($.fn.dataTable.isDataTable("#tPeticiones")) { $("#tPeticiones").DataTable().columns.adjust(); }
});
$(document).ready(function () {
    dcont.observe(tPeticiones);
    $("#divLoader").appendTo("body");
    mostrarCatEstatus();

    $("#datSesion").on("hide.bs.modal", function () { $("#contenido").empty(); $("#datSesion .btn-group button[type=button]").each(function (o, i) { $(i).removeData("value"); $("#datSesion-mensaje").html(""); }); });
    $("#petResValidar").on("hide.bs.modal", function () { limpiarValidaPetRees(); });
});
function mostrarPeticiones() {
    let buscar = {
        IdEstatus: $("#selEstatus").val()
    };
    if ($("#txSolicitud").val() !== '' && !isNaN($("#txSolicitud").val())) {
        buscar.IdSolicitud = $("#txSolicitud").val();
    }
    $('#tPeticiones').DataTable({
        destroy: true,
        searching: false,
        ordering: false,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "PeticionReestructuraValidacion.aspx/PeticionReestDocs",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d,
                    buscar: buscar
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d != undefined) {
                    if (json.d.url.length > 0)
                        window.location = json.d.url;
                    if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                }
                return JSON.stringify(json.d);
            }
        },
        columns: [
            {
                data: "NoSolicitud", title: "Solicitud", render: function (data, type, row, meta) {
                    if (type === 'display') {
                        let cont = $("<div>");
                        let valida = (row.ArchivosPV != null && row.ArchivosPV.length > 0 && row.ArchivosFD != null && row.ArchivosFD.length > 0 && !row.Liberado);
                        cont.append($("<span>", {
                            text: data, class: "solicitud" + `${valida ? "" : " pendiente"}`,
                            onclick: valida ? `validarPetReest(${JSON.stringify({ "NoSolicitudNuevo": row.NoSolicitudNuevo, "IdPeticionReestructura": row.IdPeticionReestructura })});` : ""
                        }));
                        return cont[0].outerHTML;
                    } else return "";
                }
            },
            { data: "NoSolicitudNuevo", title: "Solicitud Nueva" },
            { data: "Cliente", title: "Cliente" },
            { data: "FechaReestructura", title: "Fecha de Reestructura" },
            {
                data: "IdSolicitudPruebaVida", title: "P. Vida", render: function (data, type, row, meta) {
                    if (type === 'display') {
                        let cont = $("<div>", { class: "sesion" });
                        if (row.ConErrorPV) {
                            cont.append($("<span>", { class: "glyphicon glyphicon-remove" }));
                            cont.append($("<span>", { title: "Mensaje", class: "glyphicon glyphicon-question-sign pop-over-dt icono", "data-toggle": "popover", "data-content": row.MensajePV + '.Volver a intentar.' }));
                            cont.append($("<span>", { class: "glyphicon glyphicon-refresh icono", onclick: `onReenviar(${row.NoSolicitudNuevo})` }));
                        } else {
                            cont.append($("<span>", {
                                class: `glyphicon glyphicon-facetime-video icono ${row.ArchivosPV != null && row.ArchivosPV.length > 0 == true ? " sesion-disponible" : ""}`,
                                onclick: `${data ? `datosSesion(${row.ArchivosPV}, ${row.IdSolicitudPruebaVida}, "${row.MensajePV}")` : ""}`
                            }));
                            if (row.ArchivosPV != null && row.ArchivosPV.length > 0 == true) {
                                cont.append($("<span>", {
                                    class: "glyphicon glyphicon-repeat", title: "Volver a enviar",
                                    onclick: `onReiniciarProceso(${row.NoSolicitudNuevo}, ${row.IdSolicitudPruebaVida}, 0);`
                                }));
                            }
                        }
                        return cont[0].outerHTML;
                    } else return "";
                }
            },
            {
                data: "IdSolicitudFirmaDigital", title: "Documentos", render: function (data, type, row, meta) {
                    if (type === 'display') {
                        let cont = $("<div>", { class: "sesion" });
                        if (row.EstatusFirma != "signed") {
                            cont.append($("<span>", {
                                class: "glyphicon glyphicon-info-sign" + ` ${row.EstatusFirma}`, title: row.EstatusFirma
                            }));
                        }
                        if (row.EstatusFirma == "signed") {
                            cont.append($("<span>", {
                                class: "glyphicon glyphicon-duplicate", title: "Descargar",
                                onclick: `firmasDoc('${row.ArchivosFD}', ${row.IdSolicitudFirmaDigital}, '${row.MensajeFD}')`
                            }));

                            cont.append($("<span>", {
                                class: "glyphicon glyphicon-repeat", title: "Volver a enviar",
                                onclick: `onReiniciarProceso('${row.NoSolicitudNuevo}', 0, '${row.IdSolicitudFirmaDigital}');`
                            }));
                        }
                        return cont[0].outerHTML;
                    } else return "";
                }
            },
            { data: "Estatus", title: "Estatus" },
        ],
        columnDefs: [
        ],
        drawCallback: function () {
        },
        language: español,
        order: [[0, "asc"]]
    });
}
function onReenviar(id) {
    if (id && id > 0) {
        let parametros = {
            url: `PeticionReestructuraValidacion.aspx/ReenviarProceso`,
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ idSolicitud: id })
        }
        $.ajax(parametros)
            .done(function (data) {
                let resultado = data.d;
                if (resultado != null) {
                    if (resultado.url != null && resultado.url.length > 0) {
                        window.location = resultado.Url;
                    } else {
                        alert(resultado.mensaje);
                    }
                } else {
                    alert("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.")
                }
                mostrarPeticiones();
            })
            .fail(function (error) {
                alert("Ocurrio un error al actualizar la petición.")
            });
    }
}
function datosSesion(sesion, peticion, mensaje) {
    if (sesion == null || sesion == undefined) contenido = [];
    if (peticion == null) return;
    contenido = sesion;
    if (mensaje != null && mensaje.length > 0 && mensaje !== "null") {
        $("#datSesion-mensaje").html(mensaje);
    }
    if (contenido != null) {
        contenido.map(function (v, i) {
            if (v.Archivo.includes("Video")) $("#adVideo").data("value", i);
            else if (v.Archivo.includes("Face")) $("#adSelfie").data("value", i);
            else if (v.Archivo.includes("IdFront")) $("#adDocFront").data("value", i);
            else if (v.Archivo.includes("IdBack")) $("#adDocBack").data("value", i);
            else if (v.Archivo.includes("Cetificate")) $("#adCert").data("value", i);
        });
    }
    $("#datSesion").modal("show", { keyboard: false });
    $("#datSesion").appendTo("body");
}
function onclikArchivo(e) {
    let d = $(e).data("value");
    $("#contenido").empty();
    if (!isNaN(d)) {
        let parametros = {
            url: "PeticionReestructuraValidacion.aspx/ObtenerCotenido",
            contentType: "application/json",
            dataType: "json",
            method: 'GET',
            data: { idContenido: JSON.stringify(contenido[d].Id) }
        };
        new Promise((resolve, reject) => {
            if (contenido[d].archivo == undefined || contenido[d].archivo == null || !contenido[d].Archivo.includes('.pdf')) {
                $.ajax(parametros)
                    .done(function (data) {
                        let resultado = data.d;
                        if (resultado && resultado.url != null && resultado.url.length > 0) {
                            window.location = resultado.url;
                        }
                        if (resultado && !resultado.error && resultado.archivo != null && resultado.archivo.contenido.length > 0) {
                            contenido[d].archivo = resultado.archivo;
                            resolve();
                        } else {
                            $("#contenido").append($("<div>", { class: "error-carga", html: "No fue posible realizar la carga del documento." }));
                            resolve();
                        }
                    })
                    .fail(function (err) {
                        $("#contenido").append($("<div>", { class: "error-carga", html: "No fue posible realizar la carga del documento." }));
                        resolve();
                    });
            } else { resolve(); }
        })
            .then(() => {
                if (["image/jpg", "image/jpeg"].includes(contenido[d].archivo.tipoArchivo)) {
                    $("#contenido").append($("<img>", { src: `data:image/jpg;base64,${contenido[d].archivo.contenido}` }));
                } else if (["video/mp4"].includes(contenido[d].archivo.tipoArchivo)) {
                    $("#contenido").append($("<video>", { controls: "true" })
                        .append($("<source>", { type: "video/mp4", src: `data:video/mp4;base64,${contenido[d].archivo.contenido}` })));
                } else if (["application/pdf"].includes(contenido[d].archivo.tipoArchivo)) {
                    $("#contenido").append($("<iframe>", { src: `PeticionReestructuraValidacion.aspx?descargar=true&idDocumento=${contenido[d].Id}&nombreArchivo=${contenido[d].Archivo}`, width: "100%", height: "400px" })
                        .append($("<a>", { href: `PeticionReestructuraValidacion.aspx?descargar=true&idDocumento=${contenido[d].Id}&nombreArchivo=${contenido[d].Archivo}` })));

                } else {
                    $("#contenido").append($("<div>", { class: "error-carga", html: "No fue posible realizar la carga del documento." }));
                }
            });
    }
}
function firmasDoc(sesion, peticion, mensaje) {
    sesion = JSON.parse(sesion);
    $("#archivoPdf").empty();
    if (sesion != null && sesion != undefined && typeof sesion != 'undefined') {
        if (mensaje != null && mensaje != 'null' && mensaje.length > 0) {
            $("#datFirmas-mensaje").html(mensaje);
        }
        sesion.forEach(archivo => {
            if (archivo.Archivo.includes('.pdf') && archivo.Id != null && archivo.Id != '') {
                $("#archivoPdf").append($("<iframe>", { src: `PeticionReestructuraValidacion.aspx?descargar=true&idDocumento=${archivo.Id}&nombreArchivo=${archivo.Archivo}`, width: "100%", height: "400px" })
                    .append($("<a>", { href: `PeticionReestructuraValidacion.aspx?descargar=true&idDocumento=${archivo.Id}&nombreArchivo=${archivo.Archivo}` })));
            }
        });
    }
    $("#datFirmas").modal("show", { keyboard: false });
    $("#datFirmas").appendTo("body");
}
function validarPetReest(peticion) {
    $("#petResValidar").data("value", null);
    if (peticion.IdPeticionReestructura > 0 && peticion.NoSolicitudNuevo > 0) {
        $("#petResValidar").data("value", peticion);
        $("#petResValidar").modal("show", { keyboard: false });
        $("#petResValidar .modal-title").text(`Autorizar/Rechazar Petición ${peticion.NoSolicitudNuevo}`)
        $("#petResValidar").appendTo("body");
    }
}
function validarPeticion() {
    let peticion = {
        Accion: $("#selAccion").val(),
        Comentario: $("#txComentario").val()
    };
    let petRes = $("#petResValidar").data("value");
    peticion.IdSolicitudNuevo = petRes.NoSolicitudNuevo;
    peticion.IdPeticionReestructura = petRes.IdPeticionReestructura;

    if (peticion.IdSolicitudNuevo <= 0 || peticion.IdPeticionReestructura <= 0) { alert("La Solicitud no es válida."); return; }

    peticion.Comentario = peticion.Comentario.trim();
    if (peticion.Accion == "0" &&
        (peticion.Comentario == null || peticion.Comentario == '' || peticion.Comentario.length == 0)) { alert("Ingrese un comentario."); return; }

    let parametros = {
        url: `PeticionReestructuraValidacion.aspx/ValidarPeticionReest`,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ peticion: peticion })
    }
    $.ajax(parametros)
        .done(function (data) {
            let resultado = data.d;
            if (resultado != null) {
                if (resultado.url != null && resultado.url.length > 0) {
                    window.location = resultado.Url;
                } else {
                    alert(resultado.mensaje);
                    if (!resultado.error) {
                        $("#petResValidar").modal("hide");
                    }
                }
            } else {
                alert("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.")
            }
            mostrarPeticiones();
        })
        .fail(function (error) {
            alert("Ocurrio un error al intentar actualizar.")
        });
}
function limpiarValidaPetRees() {
    $("#petResValidar").removeData("value");
    $("#selAccion").val("1");
    $("#txComentario").val("");
}
function mostrarCatEstatus() {
    let combo = $("#selEstatus");
    let i = -1;
    $(combo).empty();
    if (estatus == null) estatus = [];
    estatus.unshift({ IdEstatusPeticionReestructura: 0, EstatusPeticionReestructura: "Todos" });
    if (combo != undefined && combo != null) {
        estatus.forEach((e, index) => {
            $(combo).append($("<option>").val(e.IdEstatusPeticionReestructura).text(e.EstatusPeticionReestructura));
            if (e.EstatusPeticionReestructura == "Autorizada") i = index;
        });
        if (i >= 0 && estatus.length > 0) $(combo).val(estatus[i].IdEstatusPeticionReestructura);
    }
    mostrarPeticiones();
}
function onReiniciarProceso(idSolicitud, idSolicitudPruebaVida, idSolicitudFirmaDigital) {
    if (idSolicitud > 0 && (idSolicitudPruebaVida > 0 || idSolicitudFirmaDigital > 0)) {
        let parametros = {
            url: `PeticionReestructuraValidacion.aspx/ReiniciarProceso`,
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                idSolicitud: idSolicitud,
                idSolicitudPruebaVida: idSolicitudPruebaVida,
                idSolicitudFirmaDigital: idSolicitudFirmaDigital
            })
        }
        $.ajax(parametros)
            .done(function (data) {
                let resultado = data.d;
                if (resultado != null) {
                    if (resultado.url != null && resultado.url.length > 0) {
                        window.location = resultado.Url;
                    } else {
                        alert(resultado.mensaje);
                    }
                } else {
                    alert("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.")
                }
                mostrarPeticiones();
            })
            .fail(function (error) {
                alert("Ocurrio un error al actualizar la petición.")
            });
    }
}