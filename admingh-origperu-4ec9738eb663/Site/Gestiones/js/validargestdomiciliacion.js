﻿$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); $(".modal-pr").appendTo("body"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});
let dcont = new ResizeObserver((e) => {
    if ($.fn.dataTable.isDataTable("#tDomiBancario")) { $("#tDomiBancario").DataTable().columns.adjust(); }
    if ($.fn.dataTable.isDataTable("#tDomiVisanet")) { $("#tDomiVisanet").DataTable().columns.adjust(); }
    if ($.fn.dataTable.isDataTable("#tDomiReferenciado")) { $("#tDomiReferenciado").DataTable().columns.adjust(); }
});
$(document).ready(function () {
    dcont.observe(tDomiBancario);
    dcont.observe(tDomiVisanet);
    dcont.observe(tDomiReferenciado);
    if (tipoGestion != undefined && tipoGestion != null && tipoGestion.length > 0) {
        tDomBancario.value = tipoGestion[0].IdTipoGestion;
        tDomBancario.oDom = "tDomiBancario";
        if (tipoGestion[0].LayoutDomiciliacion != null && tipoGestion[0].LayoutDomiciliacion.length > 0) {
            $("#tDomiBancario")[0].IdBanco = function () { return $("#selBancoDom").val() };
        }
        tDomVisanet.value = tipoGestion[1].IdTipoGestion;
        tDomVisanet.oDom = "tDomiVisanet";
        tDomRef.value = tipoGestion[2].IdTipoGestion;
        tDomRef.oDom = "tDomiReferenciado";
        mostrarDomBancario();
        mostrarDomVisanet();
        mostrarDomRef();
        $("#btnAutDomBanc").click(actualizaPeticion);
        $("#btnCancDomBanc").click(actualizaPeticion);
        $("#btnAutDomVisa").click(actualizaPeticion);
        $("#btnCancDomVisa").click(actualizaPeticion);
    }
    bancosDomiCuenta();
    $("#selBancoDom").change(mostrarDomBancario);
});
function mostrarDomBancario() {
    selTodoDom("tDomiBancario", false);
    $('#tDomiBancario').DataTable({
        destroy: true,
        searching: false,
        ordering: false,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "ValidarGestDomiciliacion.aspx/GestionDomiciliaciones",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d,
                    tipoGestion: tDomBancario.value,
                    bancoDom: $("#selBancoDom").val()
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d != undefined) {
                    if (json.d.url.length > 0)
                        window.location = json.d.url;
                    if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                }
                return JSON.stringify(json.d);
            }
        },
        columns: [
            {
                data: "IdGestionDomiciliacion", title: `<label style="width:100%; text-align: center;"><input type="checkbox" onclick="selTodoDom('tDomiBancario', this.checked);" /></label>`,
                render: function (data, type, row, meta) {
                    if (type === 'display') { return `<label style="width:100%; text-align: center;"><input type="checkbox" value="${data}" onclick="selPeticionDom('tDomiBancario', this.value, this.checked)" /></label>`; }
                    else return "";
                }
            },
            { data: "IdGestionDomiciliacion", title: "# Dom." },
            { data: "IdSolicitud", title: "Cuenta" },
            { data: "Cliente", title: "Cliente" },
            { data: "Banco", title: "Banco" },
            { data: "CuentaBancaria", title: "CuentaBancaria" },
            { data: "FechaDomiciliar", title: "Fecha Domiciliar" },
            { data: "Monto", title: "Monto" }
        ],
        columnDefs: [
            { targets: 0, width: "20px" },
            { targets: 1, width: "60px" },
            { targets: 3, width: "250px" },
            { targets: 7, width: "120px" },
        ],
        createdRow: function (row, data, index) {
            let selTodo = $(`#tDomiBancario`)[0].selTodo ?? false;
            let seleccion = $(`#tDomiBancario`)[0].seleccion ?? [];
            if (selTodo) $('td input[type=checkbox]', row).prop("checked", true).prop("disabled", true);
            else if (seleccion != null && seleccion.length > 0) {
                seleccion.forEach((val) => {
                    if (data.IdGestionDomiciliacion == val) $("td input[type=checkbox]", row).prop("checked", true);
                });
            }
        },
        language: español,
        order: [[0, "asc"]]
    });
}
function mostrarDomVisanet() {
    $('#tDomiVisanet').DataTable({
        destroy: true,
        searching: false,
        ordering: false,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "ValidarGestDomiciliacion.aspx/GestionDomiciliaciones",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d,
                    tipoGestion: tDomVisanet.value,
                    bancoDom: 0
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d != undefined) {
                    if (json.d.url.length > 0)
                        window.location = json.d.url;
                    if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                }
                return JSON.stringify(json.d);
            }
        },
        columns: [
            {
                data: "IdGestionDomiciliacion", title: `<label style="width:100%; text-align: center;"><input type="checkbox" onclick="selTodoDom('tDomiVisanet', this.checked);" /></label>`,
                render: function (data, type, row, meta) {
                    if (type === 'display') { return `<label style="width:100%; text-align: center;"><input type="checkbox" value="${data}" onclick="selPeticionDom('tDomiVisanet', this.value, this.checked)" /></label>`; }
                    else return "";
                }
            },
            { data: "IdGestionDomiciliacion", title: "# Dom." },
            { data: "IdSolicitud", title: "Cuenta" },
            { data: "Cliente", title: "Cliente" },
            { data: "NumeroTarjeta", title: "Num. de Tarjeta" },
            { data: "FechaDomiciliar", title: "Fecha Domiciliar" },
            { data: "Monto", title: "Monto" }
        ],
        columnDefs: [
            { targets: 0, width: "20px" },
            { targets: 1, width: "60px" },
            { targets: 5, width: "120px" },
        ],
        createdRow: function (row, data, index) {
            let selTodo = $(`#tDomiVisanet`)[0].selTodo ?? false;
            let seleccion = $(`#tDomiVisanet`)[0].seleccion ?? [];
            if (selTodo) $('td input[type=checkbox]', row).prop("checked", true).prop("disabled", true);
            else if (seleccion != null && seleccion.length > 0) {
                seleccion.forEach((val) => {
                    if (data.IdGestionDomiciliacion == val) $("td input[type=checkbox]", row).prop("checked", true);
                });
            }
        },
        language: español,
        order: [[0, "asc"]]
    });
}
function mostrarDomRef() {
    $('#tDomiReferenciado').DataTable({
        destroy: true,
        searching: false,
        ordering: false,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "ValidarGestDomiciliacion.aspx/GestionDomiciliaciones",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d,
                    tipoGestion: tDomRef.value,
                    bancoDom: 0
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d != undefined) {
                    if (json.d.url.length > 0)
                        window.location = json.d.url;
                    if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                }
                return JSON.stringify(json.d);
            }
        },
        columns: [
            {
                data: "IdGestionDomiciliacion", title: `<label style="width:100%; text-align: center;"><input type="checkbox" onclick="selTodoDom('tDomiReferenciado', this.checked);" /></label>`,
                render: function (data, type, row, meta) {
                    if (type === 'display') { return `<label style="width:100%; text-align: center;"><input type="checkbox" value="${data}" onclick="selPeticionDom('tDomiReferenciado', this.value, this.checked)" /></label>`; }
                    else return "";
                }
            },
            { data: "IdGestionDomiciliacion", title: "# Dom." },
            { data: "IdSolicitud", title: "Cuenta" },
            { data: "Cliente", title: "Cliente" },
            { data: "FechaDomiciliar", title: "Fecha Domiciliar" },
            { data: "Monto", title: "Monto" }
        ],
        columnDefs: [
            { targets: 0, width: "20px" },
            { targets: 1, width: "60px" },
            { targets: 5, width: "120px" },
        ],
        createdRow: function (row, data, index) {
            let selTodo = $(`#tDomiReferenciado`)[0].selTodo ?? false;
            let seleccion = $(`#tDomiReferenciado`)[0].seleccion ?? [];
            if (selTodo) $('td input[type=checkbox]', row).prop("checked", true).prop("disabled", true);
            else if (seleccion != null && seleccion.length > 0) {
                seleccion.forEach((val) => {
                    if (data.IdGestionDomiciliacion == val) $("td input[type=checkbox]", row).prop("checked", true);
                });
            }
        },
        language: español,
        order: [[0, "asc"]]
    });
}
function selTodoDom(obj, seleccionado) {
    obj = $(`#${obj}`);
    if (obj != undefined && obj != null && obj.length > 0) {
        obj = obj[0];
        obj.selTodo = seleccionado;
        $(obj).find("tbody tr input[type=checkbox]").map((i, o) => {
            $(o).prop("checked", seleccionado).prop("disabled", seleccionado);
        });
        obj.seleccion = [];
    }
}
function selPeticionDom(obj, valor, seleccionado) {
    obj = $(`#${obj}`);
    if (obj != undefined && obj != null && obj.length > 0) {
        obj = obj[0];
        let posicion = -1;
        if (obj.seleccion == undefined || obj.seleccion == null) obj.seleccion = [];
        obj.seleccion.map((o, i) => {
            if (o.toString() == valor) {
                posicion = i;
                return;
            }
        });
        if (seleccionado && posicion < 0) obj.seleccion.push(valor);
        else if (!seleccionado && obj.seleccion.length > 0 && posicion >= 0) obj.seleccion.splice(posicion, 1);
    }
}
function actualizaPeticion() {
    let tipoGestion = $("#medioDom a[aria-expanded=true]").val();
    if (tipoGestion <= 0) return;
    let oDom = $("#medioDom a[aria-expanded=true]")[0].oDom;
    if ($(`#${oDom}`) == undefined || $(`#${oDom}`).length == 0) return;
    oDom = $(`#${oDom}`)[0];
    let peticion = {
        IdTipoGestion: tipoGestion,
        Todo: oDom.selTodo ?? false,
        IdGestionDomiciliacion: oDom.seleccion ?? [],
        IdBanco: oDom.IdBanco != undefined ? oDom.IdBanco() : 0,
        ClaveEstatus: $(this).data("claveact")
    };
    let parametros = {
        url: "ValidarGestDomiciliacion.aspx/ValidarPeticionDomiciliacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ peticion: peticion })
    };
    $.ajax(parametros)
        .done(function (respuesta) {
            let resp = respuesta.d;
            if (resp != undefined) {
                if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                nuevaDomicliacion(oDom);
            } else {
                alert("Ocurrió un error al momento de actualizar los registros.");
            }
            $(`#${$("#medioDom a[aria-expanded=true]")[0].oDom}`).DataTable().ajax.reload();
        })
        .fail(function (error) {
            alert("Ocurrió un error al momento intentar realizar las acciones. Vuela a intentar.");
        });
}
function bancosDomiCuenta() {
    $("#selBancoDom").empty();
    if (bancosDomCuenta != null && bancosDomCuenta.length > 0) {
        bancosDomCuenta.forEach((v, i) => {
            $("#selBancoDom").append($("<option>", { value: v.IdBanco, text: v.Banco }));
        });
    }
}
function selLayoutDomi() {
    let tipoGestion = $("#medioDom a[aria-expanded=true]").val();
    if (tipoGestion <= 0) return;
    let oDom = $("#medioDom a[aria-expanded=true]")[0].oDom;
    if ($(`#${oDom}`) == undefined || $(`#${oDom}`).length == 0) return;
    oDom = $(`#${oDom}`)[0];
    if ($(`#${oDom.selLayout}`) == undefined || $(`#${oDom.selLayout}`).length == 0) return;
    oDom.IdLayoutArchivo = $(`#${oDom.selLayout}`).val();
}
function nuevaDomicliacion(obj) {
    if (obj != null) {
        obj.selTodo = false;
        obj.seleccion = [];
        selTodoDom(obj.Id, false);
    }
}