﻿<%@ Page Title="Firmas Generadas" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Firmas.aspx.cs" Inherits="Site_Lleida_Firmas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../js/datepicker-1.9.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <link href="css/firmas.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />
    <script src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script src="../../js/datatables/1.10.19/jquery.dataTables.min.js"></script>
    <script src="../../js/datatables/1.10.19/dataTables.bootstrap.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/datatables/js/dataTables.languague.sp.js"></script>
    <script src="../../js/datepicker-1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="../../js/datepicker-1.9.0/locales/bootstrap-datepicker.es.min.js"></script>
    <script src="js/firmas.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="page-header">
        <div class="row inline">
            <h3 style="display: inline;">Envios de Firmas Generados</h3>
        </div>
    </div>
    <div class="filtros">
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>No. Solicitud</label>
                    <input type="number" id="txSolicitud" class="form-control" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Estatus</label>
                    <select id="txEstatus" class="form-control"></select>
                </div>
            </div>
            <%--<div class="col-md-2">
                <div class="form-group">
                    <label>Desde</label>
                    <input type="text" id="txDesde" class="form-control" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Hasta</label>
                    <input type="text" id="txHasta" class="form-control" />
                </div>
            </div>--%>
            <div class="col-md-2" style="padding-top: 25px;">
                <div class="form-group">
                    <input type="button" id="btnBuscar" class="btn btn-primary" value="Buscar" onclick="mostrarSolicitudes()" />
                </div>
            </div>
        </div>
    </div>

    <div class="contenido">
        <table id="tEnvios" class="table display compact nowrap" style="width: 100%"></table>
    </div>
    <div class="modal-pr"></div>
    <div id="divLoader" class="hidden">
        <div id="loader-background"></div>
        <div id="loader-content"></div>
    </div>
</asp:Content>
