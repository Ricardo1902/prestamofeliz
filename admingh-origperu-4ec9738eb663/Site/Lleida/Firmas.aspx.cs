﻿using DataTables;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using wsSOPF;

public partial class Site_Lleida_Firmas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder strScript = new StringBuilder();
        if (!IsPostBack)
        {
            strScript.AppendFormat("estatus={0};", JsonConvert.SerializeObject(EstatusProcesoFirma()));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
        }
    }

    #region WebMethods

    #region GET
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object EstatusProcesoFirma()
    {
        object estatus = null;
        List<EstatusVM> estatusR = new List<EstatusVM>();
        using (SolicitudClient wsS = new SolicitudClient())
        {
            EstatusVM[] respEst = wsS.EstatusProcesoFirmaDigital();
            if (respEst != null && respEst.Length > 0)
            {
                estatusR = respEst.ToList();
            }
            estatusR.Insert(0, new EstatusVM { IdEstatus = -1, Descripcion = "Todos" });
            estatus = estatusR.Select(e => new
            {
                e.IdEstatus,
                Estatus = e.Descripcion
            });
        }
        return estatus;
    }
    #endregion

    #region POST
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerEnvios(DataTableAjaxPostModel model, FirmaFiltro buscar)
    {
        int idUsuario = 0;
        string error = string.Empty;
        string mensaje = string.Empty;
        int totalRegistros = 0;
        int registros = 0;
        object firmas = null;
        string url = string.Empty;
        idUsuario = HttpContext.Current.ValidarUsuario();
        string[] estatusCancela = new string[] { "new", "ready" };
        try
        {
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("Usuario Invalido");
            }
            SolicitudFirmaDigitalRequest peticion = new SolicitudFirmaDigitalRequest
            {
                IdUsuario = idUsuario,
                Pagina = (model.start / model.length) + 1,
                RegistrosPagina = model.length
            };
            ObtenerFirmasRequest peticionWsL = new ObtenerFirmasRequest { };

            if (buscar != null)
            {
                if (buscar.IdEstatus != -1)
                {
                    peticion.IdEstatus = buscar.IdEstatus;
                }
                if (buscar.IdSolicitud > 0) peticion.IdSolicitud = buscar.IdSolicitud;
            }

            SolicitudFirmaDigitalResponse respFimasDb = new SolicitudFirmaDigitalResponse();
            List<FirmaVM> firmasL = new List<FirmaVM>();
            List<SolicitudFirmaDigitalVM> firmasDb = new List<SolicitudFirmaDigitalVM>();

            using (SolicitudClient wsS = new SolicitudClient())
            {
                respFimasDb = wsS.SolicitudFirmaDigital(peticion);
                if (respFimasDb != null && respFimasDb.Firmas != null && respFimasDb.Firmas.Length > 0)
                {
                    firmasDb = respFimasDb.Firmas.ToList();
                }
            }

            using (LleidaClient wsS = new LleidaClient())
            {
                ObtenerFirmasResponse respL = wsS.ObtenerFirmas(peticionWsL);
                if (respL != null && respL.Firmas != null && respL.Firmas.Length > 0)
                {
                    firmasL = respL.Firmas.ToList();
                }
            }

            if (respFimasDb != null && respFimasDb.Firmas != null && respFimasDb.Firmas.Length > 0)
            {
                firmas = (from f in firmasDb
                          join fl in firmasL on f.ClaveFirma equals fl.signature_id into tfl
                          from lfl in tfl.DefaultIfEmpty()
                          select new
                          {
                              IdEnvio = f.IdSolicitudFirmaDigital,
                              f.IdSolicitud,
                              f.ClaveEnvio,
                              FechaEnvio = f.FechaRegistro.ToString("dd/MM/yyyy HH:mm"),
                              FechaActualizacion = f.FechaActualizacion == null ? "" : f.FechaActualizacion.Value.ToString("dd/MM/yyyy HH:mm"),
                              f.EstatusFirma,
                              f.EstatusDescripcion,
                              f.ClaveFirma,
                              f.Mensaje,
                              f.Cliente,
                              EstatusLeida = lfl == null ? "" : lfl.signature_status,
                              Cancelar = lfl == null ? false : estatusCancela.Contains(lfl.signature_status),
                              CancelarL = estatusCancela.Contains(f.EstatusFirma) && (lfl == null ? "" : lfl.signature_status) == "failed",
                              Actualizar = f.EstatusFirma == "ready" && (lfl == null ? "" : lfl.signature_status) == "signed",
                              f.Reenviar,
                              f.CargaEnCurso
                          }).ToList();

                registros = respFimasDb.Firmas.Length;
                totalRegistros = respFimasDb.TotalRegistros;
            }
        }
        catch (Exception ex)
        {
        }
        model.draw++;
        return new
        {
            drawn = model.draw,
            recordsTotal = registros,
            recordsFiltered = totalRegistros,
            data = firmas,
            url
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ActualizarFirma(int peticion, string estatus)
    {
        bool error = true;
        string mensaje = "Se ha iniciado el proceso de actualización.";
        string url = string.Empty;
        int idUsuario = 0;
        try
        {
            #region Validaciones
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            #endregion

            using (wsSOPF.LleidaClient wsR = new LleidaClient())
            {
                var wsResp = wsR.GenerarNotificacion(new GenerarNotificacionRequest { IdSolicitudFirmaDigital = peticion, Estatus = estatus });
                if (wsResp != null)
                {
                    error = wsResp.Error;
                    mensaje = error ? "Ocurrió un error al momento realizar la acutalización" : "La actualización se realizó correctamente.";

                    //Esperar unos 5 segundos en lo que se descarga información de Lleida (suele tardar entre 3 a 5 segundos en retornar info)
                    Thread.Sleep(5000);
                }
                else
                {
                    mensaje = "Ocurrió un error al momento de actualizar la petición.";
                }
            }
        }
        catch (ValidacionExcepcion vex)
        {
            mensaje = vex.Message;
        }
        catch (Exception)
        {
            mensaje = "Ocurrió un error inesperado";
        }
        return new { error, mensaje, url };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object CancelarFirma(int peticion, int accion)
    {
        bool error = true;
        string mensaje = "Se ha iniciado el proceso de actualización.";
        string url = string.Empty;
        int idUsuario = 0;
        try
        {
            #region Validaciones
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            #endregion

            using (wsSOPF.LleidaClient wsR = new LleidaClient())
            {
                var wsResp = wsR.CancelarFirma(new CancelarFirmaRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitudFirmaDigital = peticion,
                    Accion = accion
                });
                if (wsResp != null)
                {
                    error = wsResp.Error;
                    mensaje = error ? "Ocurrió un error al momento realizar la acutalización" : "La actualización se realizó correctamente.";

                    //Esperar unos 5 segundos en lo que se descarga información de Lleida (suele tardar entre 3 a 5 segundos en retornar info)
                    Thread.Sleep(5000);
                }
                else
                {
                    mensaje = "Ocurrió un error al momento de actualizar la petición.";
                }
            }
        }
        catch (ValidacionExcepcion vex)
        {
            mensaje = vex.Message;
        }
        catch (Exception)
        {
            mensaje = "Ocurrió un error inesperado";
        }
        return new { error, mensaje, url };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ReenviarFirma(int peticion, int solicitud)
    {
        bool error = true;
        string mensaje = "Se ha iniciado el proceso de envio.";
        string url = string.Empty;
        int idUsuario = 0;
        try
        {
            #region Validaciones
            if (HttpContext.Current.Session["UsuarioId"] != null)
            {
                int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
            }
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            if (solicitud <= 0)
            {
                throw new ValidacionExcepcion("La clave de solicitud no es válida.");
            }
            #endregion

            using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
            {
                var wsResp = wsSolicitud.IniciarProcesoOriginacionD(new InicarSolicitudProcesoDigitalRequest
                {
                    IdSolicitud = solicitud,
                    IdUsuario = idUsuario
                });
                if (wsResp != null)
                {
                    mensaje = string.IsNullOrEmpty(wsResp.MensajeOperacion) ? "Se ha enviado al proceso de firmas. Espere un momento y actualice la pagina." : wsResp.MensajeOperacion;
                }
                else
                {
                    mensaje = "Ocurrió un error al momento de realizar el reenvio";
                }
            }
        }
        catch (ValidacionExcepcion vex)
        {
            mensaje = vex.Message;
        }
        catch (Exception)
        {
            mensaje = "Ocurrió un error inesperado";
        }
        return new { error, mensaje, url };

    }
    #endregion
    #endregion

    public class FirmaFiltro
    {
        public int IdSolicitud { get; set; }
        public int IdEstatus { get; set; }
        public DateTime? Desde { get; set; }
        public DateTime? Hasta { get; set; }
    }
}