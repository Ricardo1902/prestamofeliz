﻿<%@ Page Title="Plantillas" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Plantillas.aspx.cs" Inherits="Site_Lleida_Plantillas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.css" />
    <link href="../../js/datatables/css/dataTable.peru.css" rel="stylesheet" />
    <style>
        /*
            Start by setting display:none to make this hidden.
            Then we position it in relation to the viewport window
            with position:fixed. Width, height, top and left speak
            for themselves. Background we set to 80% white with
            our animation centered, and no-repeating
        */
        .modal-pr {
            display: none;
            position: fixed;
            z-index: 9000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 ) url('../../Imagenes/ajax-loader.gif') 50% 50% no-repeat;
        }

        /*
            When the body has the loading class, we turn
            the scrollbar off with overflow:hidden
        */
        body.loading .modal-pr {
            overflow: hidden;
        }

        /*
            Anytime the body has the loading class, our
            modal element will be visible
        */
        body.loading .modal-pr {
            display: block;
        }
    </style>
    <script type="text/javascript" src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../js/datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
    <script type="text/javascript" type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
    <script type="text/javascript" src="../../js/lleida/Plantillas.js?v=<%=AppSettings.VersionPub %>"></script>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Plantillas</h3>
        </div>
        <div class="panel-body">
            <asp:Panel ID="pnlError" class="form-group col-sm-12" Visible="false" runat="server">                                
                <div class="alert alert-danger" style="padding: 5px 15px 5px 15px; margin: 10px 0px;">
                    <asp:Label ID="lblError" runat="server" />
                </div>
            </asp:Panel>
            <table id="tbPlantillas" class="table display compact nowrap" style="width: 100%"></table>
        </div>
    </div>
    <div class="modal-pr"></div>
    <div id="divLoader" class="hidden">
        <div id="loader-background"></div>
        <div id="loader-content"></div>
    </div>
</asp:Content>
