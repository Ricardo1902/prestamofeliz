﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Visanet.Model.Request;
using Visanet.Model.Response;
using wsSOPF;

public partial class Site_Lleida_Plantillas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    #region WebMethods
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object AsignarPlantilla(int plantilla)
    {
        PlantillaResponse respuesta = new PlantillaResponse();
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }
        try
        {
            using (LleidaClient ws = new wsSOPF.LleidaClient())
            {
                var resp = ws.AsignarPlantilla(new GeneraPlantillaRequest
                {
                    IdUsuario = idUsuario,
                    IdPlantilla = plantilla
                });
                if (resp != null)
                {
                    respuesta.Error = resp.Error;
                    respuesta.CodigoError = resp.CodigoError;
                    respuesta.MensajeOperacion = "Ocurrió un error al momento de asignar la plantilla seleccionada. Error: " + resp.MensajeErrorException;
                    respuesta.StatusCode = resp.StatusCode;
                    respuesta.TotalRegistros = resp.TotalRegistros;
                }
            }
        }
        catch (Exception ex)
        {
            respuesta.Error = true;
            respuesta.MensajeOperacion = ex.Message;
        }
        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ActivarPlantilla(int plantilla)
    {
        ActivaPlantillaResponse respuesta = new ActivaPlantillaResponse();
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }
        try
        {
            using (LleidaClient ws = new wsSOPF.LleidaClient())
            {
                var resp = ws.ActivarPlantilla(new ActivaPlantillaRequest
                {
                    IdUsuario = idUsuario,
                    IdPlantilla = plantilla
                });
                if (resp != null)
                {
                    respuesta.Error = resp.Error;
                    respuesta.CodigoError = resp.CodigoError;
                    respuesta.MensajeOperacion = "Ocurrió un error al momento de activar la plantilla seleccionada. Error: " + resp.MensajeErrorException;
                    respuesta.StatusCode = resp.StatusCode;
                    respuesta.TotalRegistros = resp.TotalRegistros;
                }
            }
        }
        catch (Exception ex)
        {
            respuesta.Error = true;
            respuesta.MensajeOperacion = ex.Message;
        }
        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object DesactivarPlantilla(int plantilla)
    {
        DesactivaPlantillaResponse respuesta = new DesactivaPlantillaResponse();
        int idUsuario = 0;
        if (HttpContext.Current.Session["UsuarioId"] != null)
        {
            int.TryParse(HttpContext.Current.Session["UsuarioId"].ToString(), out idUsuario);
        }
        try
        {
            using (LleidaClient ws = new wsSOPF.LleidaClient())
            {
                var resp = ws.DesactivarPlantilla(new DesactivaPlantillaRequest
                {
                    IdUsuario = idUsuario,
                    IdPlantilla = plantilla
                });
                if (resp != null)
                {
                    respuesta.Error = resp.Error;
                    respuesta.CodigoError = resp.CodigoError;
                    respuesta.MensajeOperacion = "Ocurrió un error al momento de desactivar la plantilla seleccionada. Error: " + resp.MensajeErrorException;
                    respuesta.StatusCode = resp.StatusCode;
                    respuesta.TotalRegistros = resp.TotalRegistros;
                }
            }
        }
        catch (Exception ex)
        {
            respuesta.Error = true;
            respuesta.MensajeOperacion = ex.Message;
        }
        return respuesta;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static object CargarPlantillas()
    {
        object resultado = null;
        try
        {
            using (wsSOPF.LleidaClient ws = new wsSOPF.LleidaClient())
            {
                ConfiguracionesResponse res = ws.Plantillas();
                if (!res.Error)
                {
                    return (from p in res.Resultado.Configuraciones
                            select new
                            {
                                IdPlantilla = p.config_id,
                                Nombre = p.name,
                                Estatus = p.status,
                                p.Activo
                            })
                            .OrderBy(p => p.IdPlantilla)
                            .ToList();
                }
            }
        }
        catch (Exception e)
        {
        }
        return resultado;
    } 
    #endregion
}