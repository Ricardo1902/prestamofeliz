﻿using DataTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Site_Lleida_PruebasVida : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region WebMethods

    #region GET
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerCotenido(string idContenido)
    {
        bool error = false;
        string mensaje = string.Empty;
        string url = string.Empty;
        object archivo = null;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válido.");
            }
            DescargarArchivoGDResponse archivoResp;
            using (wsSOPF.SistemaClient wsS = new SistemaClient())
            {
                archivoResp = wsS.DescargarArchivoGD(new DescargarArchivoGDRequest
                {
                    IdArchivoGD = idContenido,
                    IdUsuario = idUsuario
                });
            }
            if (archivoResp != null
                && !archivoResp.Error
                && archivoResp.ArchivoGoogle != null
                && archivoResp.ArchivoGoogle.Contenido != null
                && archivoResp.ArchivoGoogle.Contenido.Length > 0)
            {
                archivo = new
                {
                    contenido = Convert.ToBase64String(archivoResp.ArchivoGoogle.Contenido),
                    tipoArchivo = archivoResp.ArchivoGoogle.Mime
                };
            }
            else
            {
                error = true;
                mensaje = "No fue posible obtener el contenido del archivo.";
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url, archivo };
    }
    #endregion

    #region POST
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ObtenerPruebasVida(DataTableAjaxPostModel model, PruebasVidaFiltro buscar)
    {
        int idUsuario = 0;
        string error = string.Empty;
        string mensaje = string.Empty;
        int totalRegistros = 0;
        int registros = 0;
        object pruebas = null;
        string url = string.Empty;
        idUsuario = HttpContext.Current.ValidarUsuario();
        try
        {
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new Exception("Usuario Invalido");
            }
            SolicitudPruebaVidaRequest peticion = new SolicitudPruebaVidaRequest
            {
                IdUsuario = idUsuario,
                Pagina = (model.start / model.length) + 1,
                RegistrosPagina = model.length
            };
            ObtenerFirmasRequest peticionWsL = new ObtenerFirmasRequest { };

            if (buscar != null)
            {
                if (buscar.Aprobado != null)
                {
                    peticion.Aprobado = buscar.Aprobado;
                }
                if (buscar.IdSolicitud > 0) peticion.IdSolicitud = buscar.IdSolicitud;
            }

            SolicitudPruebaVidaResponse pruebasVidaR = new SolicitudPruebaVidaResponse();
            List<FirmaVM> firmasL = new List<FirmaVM>();
            List<SolicitudPruebaVidaVM> pruebasVida = new List<SolicitudPruebaVidaVM>();

            using (SolicitudClient wsS = new SolicitudClient())
            {
                pruebasVidaR = wsS.PruebasVida(peticion);
                if (pruebasVidaR != null && pruebasVidaR.PruebasVida != null && pruebasVidaR.PruebasVida.Length > 0)
                {
                    pruebasVida = pruebasVidaR.PruebasVida.ToList();
                }
            }

            if (pruebasVida.Count > 0)
            {
                pruebas = pruebasVida.Select(p => new
                {
                    p.IdSolicitudPruebaVida,
                    p.IdPeticion,
                    p.IdSolicitud,
                    p.Cliente,
                    p.ClaveEnvio,
                    FechaEnvio = p.FechaRegistro.ToString("yyyy-MM-dd HH:mm:ss"),
                    p.SesionRealizada,
                    p.ValidacionCorrecta,
                    p.Aprobado,
                    p.Mensaje,
                    p.Archivos,
                    Reiniciar = !(p.Aprobado ?? false) && p.SesionRealizada
                }).ToList();

                registros = pruebasVida.Count;
                totalRegistros = pruebasVidaR.TotalRegistros;
            }
        }
        catch (Exception ex)
        {
        }
        model.draw++;
        return new
        {
            drawn = model.draw,
            recordsTotal = registros,
            recordsFiltered = totalRegistros,
            data = pruebas,
            url
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ActualizarPeticion(int idPeticion)
    {
        bool error = false;
        string url = string.Empty;
        object archivo = null;
        string mensaje;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válido.");
            }
            ActualizarEstatusPruebaVidaResponse actualizaPrueba;
            using (wsSOPF.SolicitudClient wsS = new SolicitudClient())
            {
                actualizaPrueba = wsS.ActualizarPeticionEstatus(new ActualizarPeticionEstatusRequest
                {
                    IdPeticion = idPeticion,
                    IncluirArchivos = true,
                    IdUsuario = idUsuario
                });
            }
            if (actualizaPrueba != null
                && !actualizaPrueba.Error)
            {
                mensaje = "La actualización se ha realizado correctamente!";
            }
            else
            {
                error = true;
                mensaje = "No fue posible actualizar la petición.";
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url, archivo };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ReiniciarPruebaVida(int idPeticion)
    {
        bool error = false;
        string url = string.Empty;
        object archivo = null;
        string mensaje;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válido.");
            }
            ReiniciarPruebaVidaResponse actualizaPrueba;
            using (wsSOPF.SolicitudClient wsS = new SolicitudClient())
            {
                actualizaPrueba = wsS.ReiniciarPruebaVida(new ReiniciarPruebaVidaRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitudPruebaVida = idPeticion
                });
            }
            if (actualizaPrueba != null)
            {
                if (!actualizaPrueba.Error)
                {
                    mensaje = "La actualización se ha realizado correctamente!";
                }
                else
                {
                    mensaje = string.IsNullOrEmpty(actualizaPrueba.MensajeOperacion) ? "No fue posible actualizar la petición." : actualizaPrueba.MensajeOperacion;
                }
            }
            else
            {
                error = true;
                mensaje = "No fue posible actualizar la petición.";
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url, archivo };
    }
    #endregion

    #endregion

    public class PruebasVidaFiltro
    {
        public int IdSolicitud { get; set; }
        public bool? Aprobado { get; set; }
        public DateTime? Desde { get; set; }
        public DateTime? Hasta { get; set; }
    }
}