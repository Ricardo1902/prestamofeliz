﻿var estatus = [];
$(document).on({
    ajaxStart: function () { $("#divLoader").removeClass("hidden"); },
    ajaxStop: function () { $("#divLoader").addClass("hidden"); }
});
$(document).ready(function () {
    mostrarEstatus();
    mostrarSolicitudes();
});
function mostrarSolicitudes() {
    let buscar = {
        IdEstatus: $("#txEstatus").val(),
        Desde: $("#txDesde").val(),
        Hasta: $("#txHasta").val(),
    };
    if ($("#txSolicitud").val() !== '' && !isNaN($("#txSolicitud").val())) {
        buscar.IdSolicitud = $("#txSolicitud").val();
    }
    $('#tEnvios').DataTable({
        destroy: true,
        searching: false,
        ordering: false,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "Firmas.aspx/ObtenerEnvios",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d,
                    buscar: buscar
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d != undefined) {
                    if (json.d.url.length > 0)
                        window.location = json.d.url;
                    if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                }
                return JSON.stringify(json.d);
            }
        },
        columns: [
            { data: "IdEnvio", title: "Clave Peticion" },
            { data: "IdSolicitud", title: "Solicitud" },
            { data: "Cliente", title: "Cliente" },
            { data: "ClaveEnvio", title: "Clave de Envio" },
            {
                data: "EstatusDescripcion", title: "Estatus Firma", render: function (data, type, row, meta) {
                    if (type === 'display') {
                        let cont = $("<div>", { html: data });
                        if (data != null && row.Actualizar == true) {
                            if (row.CargaEnCurso == false) {
                                cont.append($("<span>", {
                                    class: "glyphicon glyphicon-download accion accion-actualizar", title: "Actualizar",
                                    onclick: `actualizar(${row.IdEnvio}, '${row.EstatusLeida}')`
                                }));
                            } else {
                                cont.append($("<span>", {
                                    class: "glyphicon glyphicon-info-sign accion accion-proceso", title: "Cargando documentos"
                                }));
                            }
                        }
                        if (data != null && row.CancelarL == true) {
                            cont.append($("<span>", {
                                class: "glyphicon glyphicon-trash accion accion-cancelar", title: "Cancelar Proceso",
                                onclick: `cancelar(${row.IdEnvio}, 1)`
                            }));
                        }
                        if (data != null && row.Reenviar == true) {
                            cont.append($("<span>", {
                                class: " glyphicon glyphicon-repeat accion accion-reenviar", title: "Volver a enviar",
                                onclick: `reenviar(${row.IdEnvio}, ${row.IdSolicitud})`
                            }));
                        }
                        return cont[0].outerHTML;
                    } else { return ""; }
                }
            },
            { data: "FechaEnvio", title: "Fecha de Envio" },
            { data: "FechaActualizacion", title: "Fecha de Actualizacion" },
            {
                data: "ClaveFirma", title: "Firma Lleida", render: function (data, type, row, meta) {
                    if (type === 'display') {
                        let cont = $("<div>", { html: data });
                        if (data != null && row.Cancelar == true) {
                            cont.append($("<span>", {
                                class: "glyphicon glyphicon-trash accion accion-cancelar", title: "Cancelar",
                                onclick: `cancelar(${row.IdEnvio}, 0)`
                            }));
                        }
                        return cont[0].outerHTML;
                    } else { return ""; }
                }
            },
            { data: "EstatusLeida", title: "Estatus en Lleida" },
            { data: "Mensaje", title: "Mensaje" }
        ],
        createdRow: function (row, data, index) {
            $('td', row).eq(0).addClass("estatus").addClass(data.EstatusFirma);
            $('td', row).eq(8).addClass("t" + data.EstatusLeida);
        },
        language: español,
        order: [[0, "asc"]]
    });
}
function mostrarEstatus() {
    let combo = $("#txEstatus");
    $(combo).empty();
    $(combo).val("-1");
    if (estatus == null) estatus = [];
    if (combo != undefined && combo != null) {
        estatus.forEach(e => {
            $(combo).append($("<option>").val(e.IdEstatus).text(e.Estatus));
        });
    }
}
function actualizar(idEnvio, estatus) {
    let parametros = {
        url: `Firmas.aspx/ActualizarFirma`,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ peticion: idEnvio, estatus: estatus })
    }
    $.ajax(parametros)
        .done(function (data) {
            let resultado = data.d;
            if (resultado != null) {
                if (resultado.url != null && resultado.url.length > 0) {
                    window.location = resultado.Url;
                } else {
                    alert(resultado.mensaje);
                }
            } else {
                alert("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.")
            }
            mostrarSolicitudes();
        })
        .fail(function (error) {
            alert("Ocurrio un error al intentar actualizar.")
        });
}
function cancelar(idEnvio, accion) {
    let parametros = {
        url: `Firmas.aspx/CancelarFirma`,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ peticion: idEnvio, accion: accion })
    }
    $.ajax(parametros)
        .done(function (data) {
            let resultado = data.d;
            if (resultado != null) {
                if (resultado.url != null && resultado.url.length > 0) {
                    window.location = resultado.Url;
                } else {
                    alert(resultado.mensaje);
                }
            } else {
                alert("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.")
            }
            mostrarSolicitudes();
        })
        .fail(function (error) {
            alert("Ocurrio un error al intentar cancelar.")
        });
}
function reenviar(idEnvio, solicitud) {
    let parametros = {
        url: `Firmas.aspx/ReenviarFirma`,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ peticion: idEnvio, solicitud: solicitud })
    }
    $.ajax(parametros)
        .done(function (data) {
            let resultado = data.d;
            if (resultado != null) {
                if (resultado.url != null && resultado.url.length > 0) {
                    window.location = resultado.Url;
                } else {
                    alert(resultado.mensaje);
                }
            } else {
                alert("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.")
            }
            mostrarSolicitudes();
        })
        .fail(function (error) {
            alert("Ocurrio un error al intentar el reenvio.")
        });
}