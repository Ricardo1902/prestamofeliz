﻿var contenido = [];
$(document).on({
    ajaxStart: function () { $("#divLoader").removeClass("hidden"); },
    ajaxStop: function () { $("#divLoader").addClass("hidden"); }
});
$(document).ready(function () {
    $("#divLoader").appendTo("body");
    mostrarSolicitudes();
    $("#datSesion").on("hide.bs.modal", function () { $("#contenido").empty(); $("#datSesion .btn-group button[type=button]").each(function (o, i) { $(i).removeData("value"); $("#datSesion-mensaje").html(""); }); });
});
function mostrarSolicitudes() {
    let buscar = {};
    if ($("#selEstatus").val() != -1) buscar.Aprobado = $("#selEstatus").val() == 0 ? false : true;
    if ($("#txSolicitud").val() !== '' && !isNaN($("#txSolicitud").val())) {
        buscar.IdSolicitud = $("#txSolicitud").val();
    }
    $('#tpruebas').DataTable({
        destroy: true,
        searching: false,
        ordering: false,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "PruebasVida.aspx/ObtenerPruebasVida",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d,
                    buscar: buscar
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d != undefined) {
                    if (json.d.url.length > 0)
                        window.location = json.d.url;
                    if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                }
                return JSON.stringify(json.d);
            }
        },
        columns: [
            {
                data: "IdPeticion", title: "Clave Peticion", render: function (data, type, row, meta) {
                    if (type === 'display') {
                        let con = `<span>${data}</span>`;
                        if (row.Reiniciar) con += `<span class="glyphicon glyphicon-refresh" title="Vovler a enviar" onclick="onReiniciar(${row.IdSolicitudPruebaVida})"></span>`;
                        return con;
                    } else return "";
                }
            },
            { data: "IdSolicitud", title: "Solicitud" },
            { data: "Cliente", title: "Cliente" },
            { data: "ClaveEnvio", title: "Clave de Envio" },
            { data: "FechaEnvio", title: "Fecha de Envio" },
            {
                data: "SesionRealizada", title: "Sesión", render: function (data, type, row, meta) {
                    if (type === 'display') {
                        let cont = $("<div>", { class: "sesion" });
                        let sesion = $("<span>", {
                            class: `glyphicon glyphicon-facetime-video ${data == true ? " sesion-disponible" : ""}`,
                            onclick: `${data ? `datosSesion(${row.Archivos}, ${row.IdPeticion}, "${row.Mensaje}")` : ""}`
                        });
                        cont.append(sesion[0].outerHTML);
                        if (data) {
                            cont.append($("<span>", { class: `${(row.ValidacionCorrecta ? "glyphicon glyphicon-ok" : "glyphicon glyphicon-remove")}` }));
                            cont.append($("<span>", { class: "glyphicon glyphicon-refresh", title: "Actualizar", onclick: `onActualizar(${row.IdPeticion})` }));
                        }
                        return cont[0].outerHTML;
                    } else { return ""; }
                }
            },
            {
                data: "Aprobado", title: "Aprobado", render: function (data, type, row, meta) {
                    if (type === 'display') {
                        let cont = $("<div>", { class: "sesion" });
                        if (data != null) {
                            $(cont).append($("<span>", { class: `${(data ? "glyphicon glyphicon-ok" : "glyphicon glyphicon-remove")}` }));
                        }
                        return cont[0].outerHTML;
                    } else { return ""; }
                }
            }
        ],
        columnDefs: [
            { targets: 5, width: "80px" },
            { targets: 6, width: "60px" },
        ],
        language: español,
        order: [[0, "asc"]]
    });
}
function datosSesion(sesion, peticion, mensaje) {
    if (sesion == null || sesion == undefined) contenido = [];
    contenido = sesion;
    contenido.IdPeticion = peticion;
    if (mensaje != null && mensaje.length > 0) {
        $("#datSesion-mensaje").html(mensaje);
    }
    contenido.map(function (v, i) {
        if (v.Archivo.includes("Video")) $("#adVideo").data("value", i);
        else if (v.Archivo.includes("Face")) $("#adSelfie").data("value", i);
        else if (v.Archivo.includes("IdFront")) $("#adDocFront").data("value", i);
        else if (v.Archivo.includes("IdBack")) $("#adDocBack").data("value", i);
        else if (v.Archivo.includes("Cetificate")) $("#adCert").data("value", i);
    });
    $("#datSesion").modal("show", { keyboard: false });
    $("#datSesion").appendTo("body");
}
function onclikArchivo(e) {
    let d = $(e).data("value");
    $("#contenido").empty();
    if (!isNaN(d)) {
        let parametros = {
            url: "PruebasVida.aspx/ObtenerCotenido",
            contentType: "application/json",
            dataType: "json",
            method: 'GET',
            data: { idContenido: JSON.stringify(contenido[d].Id) }
        };
        new Promise((resolve, reject) => {
            if (contenido[d].archivo == undefined || contenido[d].archivo == null) {
                $.ajax(parametros)
                    .done(function (data) {
                        let resultado = data.d;
                        if (resultado && resultado.url != null && resultado.url.length > 0) {
                            window.location = resultado.url;
                        }
                        if (resultado && !resultado.error && resultado.archivo != null && resultado.archivo.contenido.length > 0) {
                            contenido[d].archivo = resultado.archivo;
                            resolve();
                        } else {
                            $("#contenido").append($("<div>", { class: "error-carga", html: "No fue posible realizar la carga del documento." }));
                            resolve();
                        }
                    })
                    .fail(function (err) {
                        $("#contenido").append($("<div>", { class: "error-carga", html: "No fue posible realizar la carga del documento." }));
                        resolve();
                    });
            } else { resolve(); }
        })
            .then(() => {
                if (["image/jpg", "image/jpeg"].includes(contenido[d].archivo.tipoArchivo)) {
                    $("#contenido").append($("<img>", { src: `data:image/jpg;base64,${contenido[d].archivo.contenido}` }));
                } else if (["video/mp4"].includes(contenido[d].archivo.tipoArchivo)) {
                    $("#contenido").append($("<video>", { controls: "true" })
                        .append($("<source>", { type: "video/mp4", src: `data:video/mp4;base64,${contenido[d].archivo.contenido}` })));
                } else if (["application/pdf"].includes(contenido[d].archivo.tipoArchivo)) {
                    var link = document.createElement('a');
                    link.download = `Certificado_${contenido.IdPeticion}.pdf`;
                    link.href = 'data:application/pdf;base64,' + contenido[d].archivo.contenido;
                    document.body.appendChild(link);
                    link.click();
                    link.remove()
                } else {
                    $("#contenido").append($("<div>", { class: "error-carga", html: "No fue posible realizar la carga del documento." }));
                }
            });
    }
}
function onActualizar(peticion) {
    if (peticion > 0) {
        let parametros = {
            url: `PruebasVida.aspx/ActualizarPeticion`,
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ idPeticion: peticion })
        }
        $.ajax(parametros)
            .done(function (data) {
                let resultado = data.d;
                if (resultado != null) {
                    if (resultado.url != null && resultado.url.length > 0) {
                        window.location = resultado.Url;
                    } else {
                        alert(resultado.mensaje);
                    }
                } else {
                    alert("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.")
                }
                mostrarSolicitudes();
            })
            .fail(function (error) {
                alert("Ocurrio un error al actualizar la petición.")
            });
    }
}
function onReiniciar(peticion) {
    if (peticion > 0 && confirm("¿Está seguro de reiniciar el proceso actual?")) {
        let parametros = {
            url: `PruebasVida.aspx/ReiniciarPruebaVida`,
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ idPeticion: peticion })
        }
        $.ajax(parametros)
            .done(function (data) {
                let resultado = data.d;
                if (resultado != null) {
                    if (resultado.url != null && resultado.url.length > 0) {
                        window.location = resultado.Url;
                    } else {
                        alert(resultado.mensaje);
                    }
                } else {
                    alert("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.")
                }
                mostrarSolicitudes();
            })
            .fail(function (error) {
                alert("Ocurrio un error al actualizar la petición.")
            });
    }
}