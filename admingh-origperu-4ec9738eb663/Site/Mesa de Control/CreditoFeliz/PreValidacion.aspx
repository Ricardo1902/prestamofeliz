﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PreValidacion.aspx.cs" Inherits="Mesa_de_Control_CreditoFeliz_PreValidacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH1" runat="Server">
    <style type="text/css">
        .Clientes {
            width: 100%;
            height: 100px;
            float: left;
        }
    </style>
    </style>
    <link href="../../Styles/EstilosMaster.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <script src="../../Styles/bootstrap-3.3.2-dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.11.2.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <form id="Form1" runat="server">
        <h1>Pre-Validación de la Solicitud</h1>
        <div style="margin: auto; width: 35px; float: right;">
            <asp:ImageButton ID="btnPDF" ToolTip="Expediente" runat="server" Height="30px" Width="30px"
                ImageUrl="../../Images/pdf.ico" Visible="true" />
        </div>
        <div class="center">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Favor de llenar la sigueinte infomración en base a la Solicitud del Crédito</h3>
                </div>
                <div class="panel-body">
                    <div class="izq">
                        <div class="form-group">
                            <div class="form-group row">
                                <label for="txtCURP" class="col-sm-2 form-control-label">
                                    CURP</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtCURP" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="txtClabe" class="col-sm-2 form-control-label">
                                    Clabe Interbancaria</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtClabe" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cboConvenio" class="col-sm-2 form-control-label">
                                    Convenio</label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="cboConvenio" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="txtNombre" class="col-sm-2 form-control-label">
                                    Nombre:</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="txtApePat" class="col-sm-2 form-control-label">
                                    Apellido Paterno:</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtApePat" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="txtApeMat" class="col-sm-2 form-control-label">
                                    Apellido Materno:</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtApeMat" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnSiguienteTipo" runat="server" Text="Validar Información" CssClass="btn-primary" />
                </div>
            </div>
            <br />
            <br />
            <div id="Buscar" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Acciones</h3>
                    </div>
                    <div class="panel-body">
                        <div class="izq">
                            <div class="form-group">
                                <div class="form-group row">
                                    <label for="rdAcepatcion" class="col-sm-120 form-control-label">
                                        La solicitud es cumple los requisitos para continuar con el ánalisis</label>
                                    <div class="col-sm-5">
                                        <asp:RadioButtonList ID="rdAcepatcion" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="8">SI</asp:ListItem>
                                            <asp:ListItem Value="7">NO</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="cboConvenio" class="col-sm-2 form-control-label">
                                        Motivo de Rechazao</label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList ID="cborechazo" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtComentarios" class="col-sm-2 form-control-label">
                                        Comentarios:</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtComentarios" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <asp:Button ID="btnSigueinteBusqueda" runat="server" Text="Terminar" CssClass="btn-primary" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>
