﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmdocumentosAnalisis.aspx.cs" Inherits="Mesa_de_Control_frmdocumentosAnalisis" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        #Clientes
        {
            width: 100%;
            height: 200px;
            float: left;
        }
    </style>
    <link href="../Styles/EstilosMaster.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <script src="../Styles/bootstrap-3.3.2-dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.11.2.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="../Scripts/moment.js" type="text/javascript"></script>
    <script src="../Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <title></title>
</head>
<body class="center" style="height:920px">
    <form id="form1" runat="server">
        <div>
            <div class="GridContenedor" style="height: 320px">
                <asp:GridView ID="gvreferencias" runat="server" Width="100%" AutoGenerateColumns="False"
                    Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                    CssClass="Grid" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                    PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                    DataKeyNames="IdDocumento">
                    <Columns>
                        <asp:BoundField HeaderText="IdDocumento" DataField="IdDocumento" />
                        <asp:BoundField HeaderText="Documento" DataField="Documento" />
                        <asp:BoundField HeaderText="Direccion" DataField="direccion" />
                        <asp:TemplateField HeaderText="Decision">
                            <ItemTemplate>
                               
                                <asp:RadioButtonList ID="rblChoices" runat="server" RepeatDirection="Horizontal" >
                                    <asp:ListItem Value="0" Selected="True">SI</asp:ListItem>
                                    <asp:ListItem Value="1">No</asp:ListItem>
                                </asp:RadioButtonList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Motivo">
                            <ItemTemplate>
                                <asp:DropDownList ID="cboMotivo" runat="server">


                                    <asp:ListItem Value="0">Documentación no pertenece al cliente</asp:ListItem>
                                    <asp:ListItem Value="1">Datos erróneos (captura y llenado)</asp:ListItem>
                                    <asp:ListItem Value="2">Documentos ilegibles</asp:ListItem>
                                    <asp:ListItem Value="3">Sin sello de cotejado</asp:ListItem>
                                    <asp:ListItem Value="4">Documentos no vigentes</asp:ListItem>

                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No se encontraron registros.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </form>
</body>
</html>
