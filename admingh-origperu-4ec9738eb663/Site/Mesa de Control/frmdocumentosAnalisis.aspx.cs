﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Mesa_de_Control_frmdocumentosAnalisis : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataSet dsDocumentos = new DataSet();
            DataTable dtDocumento = new DataTable("Documento");

            
            dtDocumento.Columns.Add("IdDocumento", typeof(Int32));
            dtDocumento.Columns.Add("Documento", typeof(string));
            dtDocumento.Columns.Add("Direccion", typeof(string));

            dsDocumentos.Tables.Add(dtDocumento);

            DataRow rowDocumento = dsDocumentos.Tables["Documento"].NewRow();
            rowDocumento["IdDocumento"] = 1;
            rowDocumento["Documento"] = "1";
            rowDocumento["Direccion"] = "1";

            dsDocumentos.Tables["Documento"].Rows.Add(rowDocumento);

            gvreferencias.DataSource = dsDocumentos;
            gvreferencias.DataBind();



        }
    }
}