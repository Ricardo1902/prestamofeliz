﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class Principal : System.Web.UI.Page
{
    cls_Rep Obj_Rep = new cls_Rep();
    static SqlConnection conBd;
    #region Load
    protected void Page_Load(object sender, EventArgs e)
    {        
        DataSet Das_Avisos = new DataSet();
        Das_Avisos = Obj_Rep.Con_Avisos(Convert.ToInt32(Session["UsuarioId"]), 1);
        if (Das_Avisos.Tables[0].Rows.Count > 0)
        {
            int i;
            for (i = 0; i < Das_Avisos.Tables[0].Rows.Count; i++)
            {
                TableRow row = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                cell1.Text = Das_Avisos.Tables[0].Rows[i]["Fecha"].ToString().Trim();
                cell2.Text = Das_Avisos.Tables[0].Rows[i]["Noticia"].ToString().Trim();
                row.Cells.Add(cell1);
                row.Cells.Add(cell2);
                tblGeneral.Rows.Add(row);
            }
        }
        else
        {
            TableRow row = new TableRow();
            TableCell cell1 = new TableCell();
            cell1.Text = "No Existen Avisos";
            cell1.ColumnSpan = 2;
            row.Cells.Add(cell1);
            tblGeneral.Rows.Add(row);
        }
    }
    #endregion
}