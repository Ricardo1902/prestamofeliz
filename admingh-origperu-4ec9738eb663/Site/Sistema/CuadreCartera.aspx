﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CuadreCartera.aspx.cs" MasterPageFile="~/Site.master" Inherits="Site_Sistema_CuadreCartera" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function ()
        {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            // Activar Tooltips
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })                   
        });
    </script>

     <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
        <ProgressTemplate>                 
            <div id="loader-background"></div>
            <div id="loader-content"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upMain" runat="server">
    <ContentTemplate>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Cuadre Cartera</h3>
            </div>
            <div class="panel-body">
                <asp:Panel ID="pnlExito" class="form-group col-sm-12" Visible="false" runat="server">                                
                    <div class="alert alert-success" style="padding:5px 15px 5px 15px;">
                        <asp:Label ID="lblExito" Text="Solicitud Actualizada" runat="server"></asp:Label>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pnlError" class="form-group col-sm-12" Visible="false" runat="server">                                
                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px;">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </div>
                </asp:Panel>

                <div class="form-group col-xs-12">                
                    <div class="col-md-6">           
                            <label class="small">Estatus Credito</label>
                            <asp:dropdownlist id="ddlEstatusCredito" runat="server" CssClass="form-control">
                                <asp:ListItem Text="Todos"								        Value="0"	></asp:ListItem>
                                <asp:ListItem Text="Evaluacion de Analisis"						Value="1"	></asp:ListItem>
                                <asp:ListItem Text="Cancelado por Cliente"                      Value="2"   ></asp:ListItem>
                                <asp:ListItem Text="Cancelado por Error"                        Value="3"   ></asp:ListItem>
                                <asp:ListItem Text="Saldado"                                    Value="4"   ></asp:ListItem>
                                <asp:ListItem Text="Saldado pot Nota de Crédito"                Value="5"   ></asp:ListItem>
                                <asp:ListItem Text="Saldado por Reposición"                     Value="6"   ></asp:ListItem>                    
                                <asp:ListItem Text="Saldado por Liquidación"                    Value="8"   ></asp:ListItem>
                                <asp:ListItem Text="Saldado por Auditoria"                      Value="9"   ></asp:ListItem>
                                <asp:ListItem Text="Autorizado"                                 Value="10"  ></asp:ListItem>
                                <asp:ListItem Text="Dispersado"                                 Value="11"  ></asp:ListItem>
                                <asp:ListItem Text="PLiquidacion Parcial"                       Value="12"  ></asp:ListItem>
                                <asp:ListItem Text="Liquidado(Solo Capital)"                    Value="13"  ></asp:ListItem>
                                <asp:ListItem Text="Liquidado( Total )"                         Value="14"  ></asp:ListItem>
                                <asp:ListItem Text="Liquidado( 30% Resto Intereses + iva)"      Value="15"  ></asp:ListItem>
                                <asp:ListItem Text="Liquidacion por Renovación"                 Value="16"  ></asp:ListItem>
                                <asp:ListItem Text="Liquidacion por Defuncion"                  Value="17"  ></asp:ListItem>                    
                                <asp:ListItem Text="Liquidación Estandar"                       Value="19"  ></asp:ListItem>				                                                                
                                <asp:ListItem Text="Liquidación Reestructura"                       Value="20"  ></asp:ListItem>
                                <asp:ListItem Text="Liquidación Reprogramación"                       Value="21"  ></asp:ListItem>
                            </asp:dropdownlist>
                    </div>

                    <div class="form-group col-md-6">
                        <label class="small">Estatus Solicitud</label>
                        <asp:DropDownList ID="ddlEstatusSolicitud" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Todas" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Activa" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Cancelada" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Declinada" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Pendiente" Value="4"></asp:ListItem>
                            <asp:ListItem Text="Operada" Value="5"></asp:ListItem>
                            <asp:ListItem Text="Condicionada" Value="6"></asp:ListItem>
                            <asp:ListItem Text="Declina por Sistema" Value="7"></asp:ListItem>
                        </asp:DropDownList>
                    </div> 
                
                    <div class="col-sm-3 col-print-3">
                        <label class="small">Solicitud</label>
                        <div class="col-sm-12 input-group">                                            
                            <asp:TextBox ID="txtIdSolicitud_VerBalance" OnTextChanged="txtIdSolicitud_VerBalance_TextChanged" MaxLength="8" CssClass="form-control" AutoPostBack="true" onKeyPress="return EvaluateText('%d', this);" runat="server"></asp:TextBox>
                            <span class="input-group-addon">
                                <asp:LinkButton ID="lbtnRefresh" OnClick="txtIdSolicitud_VerBalance_TextChanged" data-toggle="tooltip" title="Recargar" runat="server"><i class="fas fa-sync-alt"></i></asp:LinkButton>                                
                            </span>                            
                        </div>
                    </div>

                    <div class="col-sm-6 col-print-6">   
                        <label class="small">Cliente</label>        
                        <asp:TextBox ID="lblCliente_VerBalance" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                    </div>

                    <div class="col-sm-3 col-print-3"> 
                        <label class="small">Fecha Credito</label>          
                        <asp:TextBox ID="txtFechaCredito_VerBalance" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                                    
                    <div class="col-sm-3 col-print-3"> 
                        <label class="small">Monto Uso Canal</label>          
                        <asp:TextBox ID="txtMontoUsoCanal_VerBalance" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                    <div class="col-sm-3 col-print-3"> 
                        <label class="small">Monto GAT</label>          
                        <asp:TextBox ID="txtMontoGAT_VerBalance" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                    <div class="col-sm-3 col-print-3"> 
                        <label class="small">Cuota</label>          
                        <asp:TextBox ID="txtCuota_VerBalance" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                    <div class="col-sm-3 col-print-3"> 
                        <label class="small">Importe Credito</label>          
                        <asp:TextBox ID="txtImporteCredito_VerBalance" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>                                                                         

                    <div class="col-sm-3 col-print-3"> 
                        <label class="small">Capital</label>          
                        <asp:TextBox ID="txtCapital" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                    </div>

                    <div class="col-sm-3 col-print-3"> 
                        <label class="small">Saldo Vencido</label>          
                        <asp:TextBox ID="lblSaldoVencido_VerBalance" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                    </div>  

                    <div class="col-sm-3 col-print-3"> 
                        <label class="small">Costo Total Credito</label>          
                        <asp:TextBox ID="lblCostoTotalCredito_VerBalance" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                    </div>           
                    
                     <div class="col-sm-3 col-print-3"> 
                        <label class="small">Seguro Desgravamen</label>          
                        <asp:TextBox ID="txtSeguroDesgravamen" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>           
                    
                    <div class="clearfix"></div>

                    <div class="col-sm-3 col-print-3 form-group"> 
                        <label class="small">Tipo Credito</label>          
                        <asp:DropDownList ID="ddlTipoCredito" OnSelectedIndexChanged="ddlTipoCredito_SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control" DataTextField="Descripcion" DataValueField="Valor">
                        </asp:DropDownList>  
                    </div>                

                    <div class="col-sm-6 col-print-6"> 
                        <label class="small">Promotor</label>          
                        <asp:DropDownList ID="ddlPromotor" runat="server" CssClass="form-control" DataTextField="Promotor" DataValueField="IdPromotor">
                        </asp:DropDownList>  
                    </div>  

                     <div class="col-sm-3 col-print-3">
                        <label class="small">Tasa Interes</label>          
                        <asp:TextBox ID="txtTasaInteres" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>  

                    <div class="clearfix"></div>

                    <div class="col-sm-4 form-group"> 
                        <label class="small">Empresa</label>
                        <asp:DropDownList ID="ddlTipoConvenio" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlTipoConvenio_SelectedIndexChanged" DataValueField="IdTipoConvenio" DataTextField="TipoConvenio">
                        </asp:DropDownList>                    
                    </div> 

                    <div class="col-sm-4 form-group"> 
                        <label class="small">Producto</label>
                        <asp:DropDownList ID="ddlConvenio" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlConvenio_SelectedIndexChanged" DataTextField="Descripcion" DataValueField="Valor">
                        </asp:DropDownList>                             
                    </div> 
                
                    <div class="col-sm-4 form-group"> 
                        <label class="small">Plazo</label>
                        <asp:DropDownList ID="ddlProducto" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlProducto_SelectedIndexChanged" DataTextField="Producto" DataValueField="IdProducto">
                        </asp:DropDownList>                             
                    </div>
                    
                    <div class="col-sm-4 form-group"> 
                        <label class="small">Origen</label>          
                        <asp:DropDownList ID="ddlOrigen" runat="server" CssClass="form-control" DataTextField="Origen" DataValueField="IdOrigen">
                        </asp:DropDownList>  
                    </div>

                    <div class="col-sm-12 form-group">
                        <asp:LinkButton ID="btnActualizar" CssClass="btn btn-sm btn-success" OnClick="btnActualizar_Click" runat="server">
                            Actualizar
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbtnSolicitudAmplio" CssClass="btn btn-sm btn-info" OnClick="lbtnSolicitudAmplio_Click" Visible="false" runat="server">
                            <i class="fas fa-plane-departure"></i>  
                            Ver Solicitud Posterior                          
                        </asp:LinkButton>
                    </div>
                      
                    <div class="clearfix" style="margin:10px 0px;"></div>              

                    <ajaxtoolKit:TabContainer ID="tcDetalle_VerBalance" CssClass="actTab" runat="server">
                        <ajaxtoolKit:TabPanel HeaderText="Recibos" runat="server">
                            <ContentTemplate>
                            <asp:Panel ID="pnlActCobranza_Recibos" CssClass="form-group col-sm-12" Visible="false" runat="server">
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtFechaInicioTablaAmor" placeholder="R#1 Fecha" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:LinkButton ID="lbtnAjustarFechaTablaAmor" OnClick="lbtnAjustarFechaTablaAmor_Click" CssClass="btn btn-sm btn-info" runat="server">
                                         Ajustar Fecha
                                     </asp:LinkButton>
                                </div>

                                <div class="col-sm-3">
                                     <asp:DropDownList ID="ddlTablaAmortizacionPRO" CssClass="form-control" runat="server">
                                         <asp:ListItem Value="ANTERIOR" Text="ANTERIOR"></asp:ListItem>
                                         <asp:ListItem Value="HISTORICO" Text="HISTORICO"></asp:ListItem>
                                         <asp:ListItem Value="ACTUAL" Text="ACTUAL"></asp:ListItem>
                                     </asp:DropDownList>
                                </div>

                                <div class="col-sm-4">
                                     <asp:LinkButton ID="lbtnReprocesarTablaAmortizacion" OnClick="lbtnReprocesarTablaAmortizacion_Click" CssClass="btn btn-sm btn-default" runat="server">
                                         Tabla Amortizacion
                                     </asp:LinkButton>

                                     <asp:LinkButton ID="lbtnHRCobranza" OnClick="lbtnHRCobranza_Click" CssClass="btn btn-sm btn-warning" runat="server">
                                         HReset Cobranza
                                     </asp:LinkButton>
                                </div>
                            </asp:Panel>

                            <div class="col-sm-12 col-print-12">
                                <div style="height:auto; width: auto; overflow-x: auto;">                                                    
                                    <asp:GridView ID="gvRecibos_VerBalance" runat="server" Width="100%" AutoGenerateColumns="False"
                                        HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData"
                                        RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        OnRowDataBound="gvRecibos_VerBalance_RowDataBound">                                        
                                        <Columns>
                                            <asp:BoundField HeaderText="#" DataField="Recibo" ItemStyle-CssClass="small" />                                            
                                            <asp:BoundField HeaderText="Fecha" DataField="FechaRecibo.Value" DataFormatString="{0:d}" ItemStyle-CssClass="small" />    
                                            <asp:BoundField HeaderText="Cap. Ini." DataField="CapitalInicial" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />                  
                                            <asp:BoundField HeaderText="S. Capital" DataField="SaldoCapital" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                            <asp:BoundField HeaderText="S. Interes" DataField="SaldoInteres" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                            <asp:BoundField HeaderText="S. IGV" DataField="SaldoIGV" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                            <asp:BoundField HeaderText="S. Seguro" DataField="SaldoSeguro" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                            <asp:BoundField HeaderText="S. GAT" DataField="SaldoGAT" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />                                                            
                                            <asp:BoundField HeaderText="S. OC" DataField="SaldoOtrosCargos" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />                                                            
                                            <asp:BoundField HeaderText="S. IGV OC" DataField="SaldoIGVOtrosCargos" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />                                                            
                                            <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="small">
                                                <ItemTemplate>
                                                    <span class="d-inline-block tooltipIdeaLeft" data-toggle="tooltip" title="<%# Eval("EstatusDesc") %>">
                                                        <%# Eval("EstatusClave") %>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                                                                
                                            <asp:BoundField HeaderText="Saldo" DataField="SaldoRecibo" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                            <asp:BoundField HeaderText="Total" DataField="TotalRecibo" DataFormatString="{0:c2}" ItemStyle-CssClass="info small"  />
                                            <asp:BoundField HeaderText="Cap. Ins." DataField="CapitalInsoluto" DataFormatString="{0:c2}" ItemStyle-CssClass="small" />
                                        </Columns>                                                                                      
                                    </asp:GridView>
                                </div>  
                            </div>
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                        <ajaxtoolKit:TabPanel HeaderText="Pagos" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnlError_Balance_VerBalance_Pagos" class="form-group col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <asp:Label ID="lblError_Balance_VerBalance_Pagos" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="pnlAplicarPagos" CssClass="form-group col-sm-12" Visible="false" runat="server">
                                    <label>Aplicar Unico</label>
                                    <asp:CheckBox ID="chkAplicarPagosUnico" runat="server" />
                                    <asp:LinkButton ID="lbtnAplicarPagos" OnClick="lbtnAplicarPagos_Click" CssClass="btn btn-sm btn-default" runat="server">
                                         Aplicar Todos los Pagos
                                     </asp:LinkButton>
                                </asp:Panel>

                                <div class="col-sm-12">
                                    <div style="height:auto; width: auto; overflow-x: auto;">                                                    
                                        <asp:GridView ID="gvPagos_VerBalance" runat="server" Width="100%" AutoGenerateColumns="False"
                                            HeaderStyle-CssClass="info" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-responsive table-condensed table-hover" EmptyDataRowStyle-CssClass="GridEmptyData"
                                            RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            OnRowDataBound="gvPagos_VerBalance_RowDataBound"
                                            OnRowCommand="gvPagos_VerBalance_RowCommand">                                        
                                            <Columns>                                                                                                                                                                                                           
                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>">
                                                    <ItemTemplate>
                                                            <asp:Panel runat="server" ID="pnlOpcionesPago">
                                                        <div class="dropdown">                                                        
                                                            <button class="btn btn-xs btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">                                                                
                                                                <i class="fas fa-bars" style="color: #888888;"></i>
                                                            <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                                <li>                                                                                                                                    
                                                                    <asp:LinkButton ID="lbtnAplicarPago" CommandName="AplicarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                        Aplicar
                                                                    </asp:LinkButton>                                                                
                                                                </li>
                                                                <li>                                                                                                                                    
                                                                    <asp:LinkButton ID="lbtnAplicarPagoUnico" CommandName="AplicarPagoUnico" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                        Aplicar Unico
                                                                    </asp:LinkButton>                                                                
                                                                </li>                                                                
                                                                <li>
                                                                    <asp:LinkButton ID="lbtnDesaplicarPago" CommandName="DesaplicarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                        Desaplicar
                                                                    </asp:LinkButton>                                                                
                                                                </li>
                                                                <li>
                                                                    <asp:LinkButton ID="lbtnCancelarPago" CommandName="CancelarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                        Cancelar
                                                                    </asp:LinkButton>                                                                
                                                                </li>
                                                                <li>
                                                                    <asp:LinkButton ID="lbtnDevolverPago" CommandName="DevolverPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                        Devuelto
                                                                    </asp:LinkButton>                                                                
                                                                </li>
                                                                <li>
                                                                    <asp:LinkButton ID="lbtnLiquidarPago" CommandName="LiquidarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                        Liquidación
                                                                    </asp:LinkButton>                                                                
                                                                </li>
                                                                <li>
                                                                    <asp:LinkButton ID="lbtnRegistrarPago" CommandName="RegistrarPago" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                        Registrar
                                                                    </asp:LinkButton>
                                                                </li>
                                                                <li>
                                                                    <asp:LinkButton ID="lbtnEliminarDevolucion" CommandName="EliminarDevolucion" CommandArgument='<%# Eval("IdPago") %>' Visible="false" runat="server">
                                                                        Eliminar
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </div>  
                                                    </asp:Panel>                                                       
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Id" DataField="IdPago" />
                                                <asp:BoundField HeaderText="Canal de Pago" DataField="CanalPagoDesc" />
                                                <asp:BoundField HeaderText="Fecha Pago" DataField="FechaPago.Value" DataFormatString="{0:d}" />
                                                <asp:BoundField HeaderText="Monto del Pago" DataField="MontoCobrado" DataFormatString="{0:c2}" />
                                                <asp:BoundField HeaderText="Aplicado" DataField="MontoAplicado" DataFormatString="{0:c2}" />                                                                                                                 
                                                <asp:BoundField HeaderText="Por Aplicar" DataField="MontoPorAplicar" DataFormatString="{0:c2}" />   
                                                <asp:BoundField HeaderText="Estatus" DataField="Estatus.EstatusDesc" />
                                            </Columns>
                                            <EmptyDataRowStyle CssClass="warning small" />                                                                                          
                                        </asp:GridView>
                                    </div>  
                                </div>
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                        <ajaxtoolKit:TabPanel ID="tpLiquidacion_VerBalance" HeaderText="Liquidacion" Visible="false" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnlError_VerBalance_Liquidacion" class="form-group col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <asp:Label ID="lblError_VerBalance_Liquidacion" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Numero Liquidacion</label>          
                                    <pre><asp:Label ID="lblIdLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Numero Transaccion (Autorizacion)</label>          
                                    <pre><asp:Label ID="lblNumeroTransaccionLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Fecha Liquidacion</label>          
                                    <pre><asp:Label ID="lblFechaLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                </div>

                                    <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Capital</label>          
                                    <pre><asp:Label ID="lblCapitalLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Interes</label>          
                                    <pre><asp:Label ID="lblInteresLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">IGV</label>          
                                    <pre><asp:Label ID="lblIGVLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Seguro</label>          
                                    <pre><asp:Label ID="lblSeguroLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">GAT</label>          
                                    <pre><asp:Label ID="lblGATLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Total Liquidacion</label>          
                                    <pre><asp:Label ID="lblTotalLiquidacion_VerBalance_Liquidacion" runat="server"></asp:Label></pre>
                                </div>

                                <div class="col-sm-12 col-print-12">
                                    <hr />
                                    <div style="height:auto; width: auto; overflow-x: auto;">                                                    
                                        <asp:GridView ID="gvRecibos_VerBalance_Liquidacion" runat="server" Width="100%" AutoGenerateColumns="False"
                                            HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData"
                                            RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            OnRowDataBound="gvRecibos_VerBalance_Liquidacion_RowDataBound">                                        
                                            <Columns>
                                                <asp:BoundField HeaderText="#" DataField="Recibo" />      
                                                <asp:BoundField HeaderText="Fecha" DataField="FechaRecibo" DataFormatString="{0:d}" />                      
                                                <asp:BoundField HeaderText="Tipo Recibo" DataField="TipoReciboCalculo" />  
                                                <asp:BoundField HeaderText="Liquida Capital" DataField="LiquidaCapital" DataFormatString="{0:c2}" />
                                                <asp:BoundField HeaderText="Liquida Interes" DataField="LiquidaInteres" DataFormatString="{0:c2}" />
                                                <asp:BoundField HeaderText="Liquida IGV" DataField="LiquidaIGV" DataFormatString="{0:c2}" />
                                                <asp:BoundField HeaderText="Liquida Seguro" DataField="LiquidaSeguro" DataFormatString="{0:c2}"/>
                                                <asp:BoundField HeaderText="Liquida GAT" DataField="LiquidaGAT" DataFormatString="{0:c2}" />                                                                
                                            </Columns>                                                
                                            <EmptyDataTemplate>
                                                Hay un problema con la Liquidacion, no tiene Información de Detalle
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>  
                                </div>
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                        <ajaxtoolKit:TabPanel ID="tpAmpliacion" HeaderText="Ampliacion" Visible="false" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnlError_Ampliacion" class="form-group col-sm-12" Visible="false" runat="server">                                
                                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                        <asp:Label ID="lblError_Ampliacion" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>

                                <div class="col-sm-12">                                    
                                    <asp:LinkButton ID="btnDatosAmpliacion" CssClass="btn btn-sm btn-default" OnClick="btnDatosAmpliacion_Click" runat="server">
                                        <i class="fab fa-searchengin"></i>
                                        Calcular Ampliacion
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lbtnSolicitudAmplia" CssClass="btn btn-sm btn-info" OnClick="lbtnSolicitudAmplia_Click" Enabled="false" runat="server">
                                        <i class="fas fa-plane-arrival"></i>
                                        Ver Solicitud Anterior
                                    </asp:LinkButton>
                                    <hr />
                                </div>
                                
                                <div class="col-sm-4 col-print-4">
                                    <label class="small">Solicitud Ampliacion</label>          
                                    <asp:TextBox ID="txtIdSolicitudAmplia" MaxLength="8" CssClass="form-control" onKeyPress="return EvaluateText('%d', this);" runat="server"></asp:TextBox>
                                </div>

                                <div class="col-sm-4 col-print-4">
                                    <label class="small">Fecha Ampliacion</label>          
                                    <asp:TextBox ID="txtFechaAmpliacion" MaxLength="10" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>                                
                                
                                <div class="clearfix"></div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Capital Liquida</label>          
                                    <asp:TextBox ID="txtCapitalLiquida" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Interes Liquida</label>          
                                    <asp:TextBox ID="txtInteresLiquida" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                 <div class="col-sm-4 col-print-4"> 
                                    <label class="small">IGV Liquida</label>          
                                    <asp:TextBox ID="txtIGVLiquida" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Seguro Liquida</label>          
                                    <asp:TextBox ID="txtSeguroLiquida" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">GAT Liquida</label>          
                                    <asp:TextBox ID="txtGATLiquida" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <div class="col-sm-4 col-print-4"> 
                                    <label class="small">Total Liquida</label>          
                                    <asp:TextBox ID="txtTotalLiquida" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <div class="col-sm-12 col-print-12"> 
                                    <label class="small">Comentarios Sistemas</label>          
                                    <asp:TextBox ID="txtComentariosSistemas" CssClass="form-control" MaxLength="500" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>
                                </div>
                            </ContentTemplate>
                        </ajaxtoolKit:TabPanel>
                    </ajaxtoolKit:TabContainer>                                    
                </div>                                                         
            </div>               
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
