﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using wsSOPF;

public partial class Site_Sistema_CuadreCartera : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Permitir el acceso solo al usuario de PRODUCCIONTI (21)
        if (int.Parse(Session["UsuarioId"].ToString()) != 21)
            throw new Exception("Acceso no Permitido");

        if (!Page.IsPostBack)
        {
            btnActualizar.Enabled = false;
        }
    }

    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        try
        {
            pnlExito.Visible = false;
            pnlError.Visible = false;

            using (wsSOPF.SistemaClient ws = new SistemaClient())
            {
                wsSOPF.CuadreDatosSolicitudEntity entity = new CuadreDatosSolicitudEntity();
                entity.IdSolicitud = int.Parse(txtIdSolicitud_VerBalance.Text.Trim());             
                 entity.FechaCredito = DateTime.ParseExact(txtFechaCredito_VerBalance.Text.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None);
                entity.MontoUsoCanal = decimal.Parse(txtMontoUsoCanal_VerBalance.Text.Trim());
                entity.MontoGAT = decimal.Parse(txtMontoGAT_VerBalance.Text.Trim());
                entity.MontoCuota = decimal.Parse(txtCuota_VerBalance.Text.Trim());
                entity.SeguroDesgravamen = decimal.Parse(txtSeguroDesgravamen.Text.Trim());
                entity.ImporteCredito = decimal.Parse(txtImporteCredito_VerBalance.Text.Trim());
                entity.IdProducto = int.Parse(ddlProducto.SelectedValue);
                entity.TasaInteres = (txtTasaInteres.Text.Trim() != string.Empty) ? decimal.Parse(txtTasaInteres.Text.Trim()) : (decimal?)null;
                entity.IdPromotor = int.Parse(ddlPromotor.SelectedValue);
                entity.IdTipoCredito = int.Parse(ddlTipoCredito.SelectedValue);
                entity.EstatusSolicitud = new TBCATEstatus();
                entity.EstatusCredito = new TBCATEstatus();
                entity.EstatusSolicitud.IdEstatus = int.Parse(ddlEstatusSolicitud.SelectedValue);
                entity.EstatusCredito.IdEstatus = int.Parse(ddlEstatusCredito.SelectedValue);
                entity.IdOrigen = int.Parse(ddlOrigen.SelectedValue);

                if (tpAmpliacion.Visible == true)
                {
                    entity.DatosAmpliacion = new DatosAmpliacionEntity();
                    entity.DatosAmpliacion.IdSolicitudAmplia = int.Parse(txtIdSolicitudAmplia.Text.Trim());
                    entity.DatosAmpliacion.FechaLiquidar = DateTime.ParseExact(txtFechaAmpliacion.Text.Trim(), "dd/MM/yyyy", null);
                    entity.DatosAmpliacion.CapitalLiquidar = decimal.Parse(txtCapitalLiquida.Text.Trim());
                    entity.DatosAmpliacion.InteresLiquidar = decimal.Parse(txtInteresLiquida.Text.Trim());
                    entity.DatosAmpliacion.IGVLiquidar = decimal.Parse(txtIGVLiquida.Text.Trim());
                    entity.DatosAmpliacion.SeguroLiquidar = decimal.Parse(txtSeguroLiquida.Text.Trim());
                    entity.DatosAmpliacion.GATLiquidar = decimal.Parse(txtGATLiquida.Text.Trim());
                    entity.DatosAmpliacion.ComentariosSistema = txtComentariosSistemas.Text.Trim();
                }

                ResultadoOfboolean res = ws.Sistema_ActualizarDatosSolicitud(entity);

                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);

                pnlExito.Visible = true;                
            }
        }
        catch (Exception ex)
        {
            pnlError.Visible = true;
            lblError.Text = ex.Message;
        }
    }
    
    protected void lbtnSolicitudAmplio_Click(object sender, EventArgs e)
    {
        txtIdSolicitud_VerBalance.Text = ViewState["IdSolicitudAmplio"].ToString();
        txtIdSolicitud_VerBalance_TextChanged(sender, e);
    }

    protected void lbtnSolicitudAmplia_Click(object sender, EventArgs e)
    {
        txtIdSolicitud_VerBalance.Text = txtIdSolicitudAmplia.Text.Trim();
        txtIdSolicitud_VerBalance_TextChanged(sender, e);
    }

    protected void txtIdSolicitud_VerBalance_TextChanged(object sender, EventArgs e)
    {
        try
        {
            pnlExito.Visible = false;
            pnlError.Visible = false;            

            CargarCombos();
            Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));
            btnActualizar.Enabled = true;
            pnlActCobranza_Recibos.Visible = true;
        }
        catch(Exception ex)
        {
            VaciarCampos();
            btnActualizar.Enabled = false;
            pnlActCobranza_Recibos.Visible = false;
            pnlAplicarPagos.Visible = false;
            pnlError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    protected void lbtnHRCobranza_Click(object sender, EventArgs e)
    {
        try
        {
            using (wsSOPF.SistemaClient ws = new SistemaClient())
            {
                wsSOPF.ResultadoOfboolean res = ws.Sistema_HRCobranza(new CuadreDatosSolicitudEntity() { IdSolicitud = int.Parse(txtIdSolicitud_VerBalance.Text.Trim()) });
                Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));

                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);
            }
        }
        catch (Exception ex)
        {
            pnlError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    protected void lbtnReprocesarTablaAmortizacion_Click(object sender, EventArgs e)
    {
        try
        {
            using (wsSOPF.SistemaClient ws = new SistemaClient())
            {
                wsSOPF.ResultadoOfboolean res = ws.Sistema_ReprocesarTablaAmortizacion(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()), ddlTablaAmortizacionPRO.SelectedValue);
                Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));

                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);
            }
        }
        catch (Exception ex)
        {
            pnlError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    protected void lbtnAjustarFechaTablaAmor_Click(object sender, EventArgs e)
    {
        try
        {
            using (wsSOPF.SistemaClient ws = new SistemaClient())
            {
                wsSOPF.ResultadoOfboolean res = ws.Sistema_CambioFechaTablaAmortizacion(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()), DateTime.ParseExact(txtFechaInicioTablaAmor.Text.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None));

                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);

                Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));
            }
        }
        catch (Exception ex)
        {
            pnlError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    protected void gvRecibos_VerBalance_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            wsSOPF.BalanceCuenta.Recibos recibo = (wsSOPF.BalanceCuenta.Recibos)e.Row.DataItem;

            // Obtenemos los Index de cada Columna para accesarla posteriormente
            int indexSaldoCapital = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoCapital");
            int indexSaldoInteres = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoInteres");
            int indexSaldoIGV = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoIGV");
            int indexSaldoSeguro = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoSeguro");
            int indexSaldoGAT = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoGAT");
            int indexSaldoOtrosCargos = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoOtrosCargos");
            int indexSaldoIGVOtrosCargos = ControlsHelper.GetColumnIndexByName(e.Row, "SaldoIGVOtrosCargos");

            // Damos formato a las celdas de los Saldos Vencidos (recibos que esten Activos y que sean de fechas pasadas)
            if (recibo.FechaRecibo.Value < DateTime.Today.AddDays(1) && recibo.EstatusClave == "A")
            {
                if (recibo.SaldoCapital > 0)
                    e.Row.Cells[indexSaldoCapital].CssClass += "small danger";

                if (recibo.SaldoInteres > 0)
                    e.Row.Cells[indexSaldoInteres].CssClass += "small danger";

                if (recibo.SaldoIGV > 0)
                    e.Row.Cells[indexSaldoIGV].CssClass += "small danger";

                if (recibo.SaldoSeguro > 0)
                    e.Row.Cells[indexSaldoSeguro].CssClass += "small danger";

                if (recibo.SaldoGAT > 0)
                    e.Row.Cells[indexSaldoGAT].CssClass += "small danger";

                if (recibo.SaldoOtrosCargos > 0)
                    e.Row.Cells[indexSaldoOtrosCargos].CssClass += "small danger";

                if (recibo.SaldoIGVOtrosCargos > 0)
                    e.Row.Cells[indexSaldoIGVOtrosCargos].CssClass += "small danger";
            }
            else
            {
                // Damos formato a las celdas de los Recibos Saldados 
                // Damos formato a las celdas de los Recibos Cancelados
                // Damos formato a las celdas de los Recibos Condonados
                string cssEstatusRecibo = "small";
                switch (recibo.EstatusClave)
                {
                    case "S":
                        cssEstatusRecibo += " success";
                        break;
                    case "C":
                        cssEstatusRecibo += " warning";
                        break;
                    case "RC":
                        cssEstatusRecibo += " active";
                        break;
                }

                // Aplicamos el css correspondiente.
                e.Row.Cells[indexSaldoCapital].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoInteres].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoIGV].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoSeguro].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoGAT].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoOtrosCargos].CssClass += string.Format(" {0}", cssEstatusRecibo);
                e.Row.Cells[indexSaldoIGVOtrosCargos].CssClass += string.Format(" {0}", cssEstatusRecibo);
            }
        }
    }

    protected void gvPagos_VerBalance_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            wsSOPF.Pago dataItem = (wsSOPF.Pago)e.Row.DataItem;

            // Obtenemos los Index de cada Columna para accesarla posteriormente
            int indexEstatusPago = ControlsHelper.GetColumnIndexByName(e.Row, "Estatus.EstatusDesc");

            switch (dataItem.Estatus.EstatusClave)
            {
                case "PR":
                    e.Row.Cells[indexEstatusPago].CssClass = "active";
                    break;
                case "PP":
                    e.Row.Cells[indexEstatusPago].CssClass = "active";
                    break;
                case "PA":
                    e.Row.Cells[indexEstatusPago].CssClass = "success";
                    break;
                case "PC":
                    e.Row.Cells[indexEstatusPago].CssClass = "warning";
                    break;
                case "PD":
                    e.Row.Cells[indexEstatusPago].CssClass = "";
                    break;
                case "DR":
                    {
                        e.Row.CssClass = "info";
                        break;
                    }
                case "DA":
                    {
                        e.Row.CssClass = "danger";
                        ((Panel)e.Row.FindControl("pnlOpcionesPago")).Visible = false;
                        break;
                    }
                case "DE":
                    {
                        e.Row.CssClass = "warning";
                        ((Panel)e.Row.FindControl("pnlOpcionesPago")).Visible = false;
                        break;
                    }
            }

            if (dataItem.Estatus.EstatusClave == "PR" || dataItem.Estatus.EstatusClave == "PP")
            {
                // Si hay al menos un pago (PR o PP), mostramos la seccion de aplicar todos los pagos
                pnlAplicarPagos.Visible = true;

                ((LinkButton)e.Row.FindControl("lbtnAplicarPago")).Visible = true;
                ((LinkButton)e.Row.FindControl("lbtnAplicarPagoUnico")).Visible = true;                
            }

            if (dataItem.Estatus.EstatusClave == "PA" || dataItem.Estatus.EstatusClave == "PP")
            {
                ((LinkButton)e.Row.FindControl("lbtnDesaplicarPago")).Visible = true;
            }                

            if (dataItem.Estatus.EstatusClave == "PR")
            {
                ((LinkButton)e.Row.FindControl("lbtnCancelarPago")).Visible = true;
                ((LinkButton)e.Row.FindControl("lbtnDevolverPago")).Visible = true;
                ((LinkButton)e.Row.FindControl("lbtnLiquidarPago")).Visible = true;
            }                

            if (dataItem.Estatus.EstatusClave == "PC")
            {
                ((LinkButton)e.Row.FindControl("lbtnRegistrarPago")).Visible = true;
            }

            if (Convert.ToInt32(Session["UsuarioId"].ToString()) == 21)
            {
                if (dataItem.Estatus.EstatusClave == "PD" || dataItem.Estatus.EstatusClave == "PL")
                {
                    ((LinkButton)e.Row.FindControl("lbtnRegistrarPago")).Visible = true;
                }

                if (dataItem.Estatus.EstatusClave == "DR" || dataItem.Estatus.EstatusClave == "DA")
                {
                    ((LinkButton)e.Row.FindControl("lbtnEliminarDevolucion")).Visible = true;
                }
            }
        }
    }

    protected void gvPagos_VerBalance_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "AplicarPago":
            case "AplicarPagoUnico":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    bool bAplicarUnico = (e.CommandName == "AplicarPagoUnico") ? true : false;
                    ResultadoOfboolean res = ws.Pagos_AplicarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) }, bAplicarUnico);

                    // Si hay algun error al Aplicar el Pago lo Mostramos, Sino Actualizamos los Recibos y Pagos.
                    if (res.Codigo > 0)
                    {
                        lblError_Balance_VerBalance_Pagos.Text = res.Mensaje;
                        pnlError_Balance_VerBalance_Pagos.Visible = true;
                    }
                    else
                    {
                        Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));
                    }
                }
                break;
            case "DesaplicarPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean res = ws.Pagos_DesaplicarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) });

                    // Si hay algun error al Desaplicar el Pago lo Mostramos, Sino Actualizamos los Recibos y Pagos.
                    if (res.Codigo > 0)
                    {
                        lblError_Balance_VerBalance_Pagos.Text = res.Mensaje;
                        pnlError_Balance_VerBalance_Pagos.Visible = true;
                    }
                    else
                    {
                        Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));
                    }
                }
                break;
            case "CancelarPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean res = ws.Pagos_CancelarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) });

                    // Si hay algun error al Desaplicar el Pago lo Mostramos, Sino Actualizamos los Recibos y Pagos.
                    if (res.Codigo > 0)
                    {
                        lblError_Balance_VerBalance_Pagos.Text = res.Mensaje;
                        pnlError_Balance_VerBalance_Pagos.Visible = true;
                    }
                    else
                    {
                        Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));
                    }
                }
                break;
            case "DevolverPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean resultado = ws.Pagos_DevolverPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) });
                    if (resultado.Codigo > 0)
                    {
                        lblError_Balance_VerBalance_Pagos.Text = resultado.Mensaje;
                        pnlError_Balance_VerBalance_Pagos.Visible = true;
                    }
                    else
                    {
                        Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));
                    }
                }
                break;
            case "LiquidarPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean resultado = ws.Pagos_LiquidarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) }, Convert.ToInt32(Session["UsuarioId"].ToString()));
                    if (resultado.Codigo > 0)
                    {
                        lblError_Balance_VerBalance_Pagos.Text = resultado.Mensaje;
                        pnlError_Balance_VerBalance_Pagos.Visible = true;
                    }
                    else
                    {
                        Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));
                    }
                }
                break;
            case "RegistrarPago":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean resultado = ws.Pagos_RegistrarPago(new Pago() { IdPago = int.Parse(e.CommandArgument.ToString()) }, Convert.ToInt32(Session["UsuarioId"].ToString()));
                    if (resultado.Codigo > 0)
                    {
                        lblError_Balance_VerBalance_Pagos.Text = resultado.Mensaje;
                        pnlError_Balance_VerBalance_Pagos.Visible = true;
                    }
                    else
                    {
                        Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));
                    }
                }
                break;
            case "EliminarDevolucion":
                using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                {
                    ResultadoOfboolean res = ws.Devoluciones_EliminarDevolucion(new Devolucion() { idDevolucion = int.Parse(e.CommandArgument.ToString()) }, Convert.ToInt32(Session["UsuarioId"].ToString()));

                    //SI HAY ALGUN ERROR AL ELIMINAR LA DEVOLUCION LO MOSTRAMOS, SINO ACTUALIZAMOS LOS RECIBOS Y PAGOS.
                    if (res.Codigo > 0)
                    {
                        lblError_Balance_VerBalance_Pagos.Text = res.Mensaje;
                        pnlError_Balance_VerBalance_Pagos.Visible = true;
                    }
                    else
                    {
                        Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));
                    }
                }
                break;
        }
    }

    protected void lbtnAplicarPagos_Click(object sender, EventArgs e)
    {
        try
        {
            List<wsSOPF.Pago> pagos = (List<wsSOPF.Pago>)gvPagos_VerBalance.DataSource;            
            using (wsSOPF.CobranzaAdministrativaClient wsCobranza = new CobranzaAdministrativaClient())
            {                
                pagos = wsCobranza.Pagos_Filtrar(new Pago() { IdSolicitud = int.Parse(txtIdSolicitud_VerBalance.Text.Trim()) }).ToList();
            }

            foreach (wsSOPF.Pago p in pagos)
            {
                if (p.Estatus.EstatusClave == "PR" || p.Estatus.EstatusClave == "PP")
                {
                    using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                    { 
                        ResultadoOfboolean res = ws.Pagos_AplicarPago(new Pago() { IdPago = p.IdPago }, chkAplicarPagosUnico.Checked);

                        // Si hay algun error al Aplicar el Pago lo Mostramos, Sino Actualizamos los Recibos y Pagos.
                        if (res.Codigo > 0)
                        {
                            lblError_Balance_VerBalance_Pagos.Text = res.Mensaje;
                            pnlError_Balance_VerBalance_Pagos.Visible = true;
                        }                        
                    }
                }
            }
            using (CobranzaAdministrativaClient ws = new CobranzaAdministrativaClient())
            {
                // BUSCAMOS DEVOLUCIONES SIN FECHA PAGO, EN CASO DE QUE LA DEVOLUCION SEA EL ULTIMO REGISTRO EN TABLA
                ws.AplicarDevolucionesPendientes();
            }
                Balance_VerBalance_Cargar(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()));
        }
        catch (Exception ex)
        {
            pnlError.Visible = true;
            lblError.Text = ex.Message;
        }
    }    

    protected void gvRecibos_VerBalance_Liquidacion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            wsSOPF.TBCreditos.Liquidacion.LiquidacionDetalle objData = (wsSOPF.TBCreditos.Liquidacion.LiquidacionDetalle)e.Row.DataItem;

            // Obtenemos los Index de cada Columna para accesarla posteriormente            
            int idxInteres = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaInteres");
            int idxIGV = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaIGV");
            int idxSeguro = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaSeguro");
            int idxGAT = ControlsHelper.GetColumnIndexByName(e.Row, "LiquidaGAT");

            e.Row.Cells[idxInteres].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaInteres.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxIGV].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaIGV.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxSeguro].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaSeguro.ToString("C", CultureInfo.CurrentCulture) : "-";
            e.Row.Cells[idxGAT].Text = (objData.TipoReciboCalculo != "FUTURO") ? objData.LiquidaGAT.ToString("C", CultureInfo.CurrentCulture) : "-";
        }
    }
    
    protected void ddlTipoConvenio_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Cargar Combo Empresa
        using (wsSOPF.CatalogoClient ws = new CatalogoClient())
        {
            if (ddlTipoConvenio.SelectedValue != string.Empty)
            {
                List<Combo> Productos = ws.Sistema_ObtenerCombo_Convenio(int.Parse(ddlTipoConvenio.SelectedValue)).ToList();

                ddlConvenio.DataSource = Productos;
                ddlConvenio.DataBind();
            }
        }

        ddlConvenio_SelectedIndexChanged(sender, e);
    }

    protected void ddlConvenio_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Cargar Combo Plazo
        using (wsSOPF.CatalogoClient ws = new CatalogoClient())
        {
            if (ddlConvenio.SelectedValue != string.Empty)
            {
                List<TBCATProducto> Plazos = ws.ObtenerTBCATProducto(99, int.Parse(ddlConvenio.SelectedValue), string.Empty, -1, 0, int.Parse(ddlTipoCredito.SelectedValue)).ToList();

                ddlProducto.DataSource = Plazos;
                ddlProducto.DataBind();
            }
        }

        ddlProducto_SelectedIndexChanged(sender, e);
    }

    protected void ddlProducto_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    protected void ddlTipoCredito_SelectedIndexChanged(object sender, EventArgs e)
    {
        tpAmpliacion.Visible = (ddlTipoCredito.SelectedValue == "3"); // AMPLIACIÓN
    }

    protected void btnDatosAmpliacion_Click(object sender, EventArgs e)
    {
        pnlError_Ampliacion.Visible = false;
        lbtnSolicitudAmplia.Enabled = false;

        try
        {
            using (wsSOPF.SistemaClient ws = new SistemaClient())
            {
                wsSOPF.ResultadoOfDatosAmpliacionEntityrcuOa4Td res = ws.Sistema_ObtenerDatosAmpliacion(int.Parse(txtIdSolicitud_VerBalance.Text.Trim()), int.Parse(txtIdSolicitudAmplia.Text.Trim()), DateTime.ParseExact(txtFechaAmpliacion.Text.Trim(), "dd/MM/yyyy", null));

                if (res.Codigo != 0)
                    throw new Exception(res.Mensaje);

                txtIdSolicitudAmplia.Text = res.ResultObject.IdSolicitud.ToString();
                txtCapitalLiquida.Text = res.ResultObject.CapitalLiquidar.ToString();
                txtInteresLiquida.Text = res.ResultObject.InteresLiquidar.ToString();
                txtIGVLiquida.Text = res.ResultObject.IGVLiquidar.ToString();
                txtSeguroLiquida.Text = res.ResultObject.SeguroLiquidar.ToString();
                txtGATLiquida.Text = res.ResultObject.GATLiquidar.ToString();
                txtTotalLiquida.Text = res.ResultObject.TotalLiquidar.ToString();
                lbtnSolicitudAmplia.Enabled = true;
            }
        }
        catch (Exception ex)
        {                                    
            pnlError_Ampliacion.Visible = true;
            lblError_Ampliacion.Text = ex.Message;
        }
    }

    #region "Metodos"

    private void VaciarCampos()
    {
        ddlEstatusCredito.SelectedValue = "0";
        ddlEstatusSolicitud.SelectedValue = "0";
        lblCliente_VerBalance.Text = string.Empty;
        txtFechaCredito_VerBalance.Text = string.Empty;
        txtMontoUsoCanal_VerBalance.Text = string.Empty;
        txtMontoGAT_VerBalance.Text = string.Empty;
        txtCuota_VerBalance.Text = string.Empty;
        txtSeguroDesgravamen.Text = string.Empty;
        txtImporteCredito_VerBalance.Text = string.Empty;
        lblSaldoVencido_VerBalance.Text = string.Empty;
        lblCostoTotalCredito_VerBalance.Text = string.Empty;

        ddlPromotor.Items.Clear();        
        ddlOrigen.Items.Clear();
        ddlTipoCredito.Items.Clear();        
        ddlTipoConvenio.Items.Clear();        
        ddlConvenio.Items.Clear();        
        ddlProducto.Items.Clear();        

        gvPagos_VerBalance.DataSource = null;
        gvPagos_VerBalance.DataBind();
        gvRecibos_VerBalance.DataSource = null;
        gvRecibos_VerBalance.DataBind();

        btnActualizar.Enabled = false;
    }
    
    private void CargarCombos()
    {
        TBCATTipoConvenio[] TipoConvenio;
        Combo[] TipoCredito_PE;
        Combo[] CanalVentas_PE;
        Combo[] TipoEvaluacion_PE;
        TBCATPromotor[] Promotores;
        List<TBCatOrigen> Origenes;

        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            TipoConvenio = wsCatalogo.ObtenerTipoConvenio(2, int.Parse(Session["cve_sucurs"].ToString()), string.Empty, 0, 0);
            TipoCredito_PE = wsCatalogo.ObtenerCombo_TipoCredito_PE();
            CanalVentas_PE = wsCatalogo.ObtenerCombo_CanalVentas_PE();
            TipoEvaluacion_PE = wsCatalogo.ObtenerCombo_TipoEvaluacion_PE();
            Promotores = wsCatalogo.ObtenerPromotores(0, 7);
            Origenes = wsCatalogo.ObtenerTBCatOrigen(2).ToList();
        }

        if (TipoCredito_PE.Length > 0)
        {
            ddlTipoCredito.DataSource = TipoCredito_PE;
            ddlTipoCredito.DataBind();
        }

        if (TipoConvenio.Length > 0)
        {
            ddlTipoConvenio.DataSource = TipoConvenio;
            ddlTipoConvenio.DataBind();

            ddlTipoConvenio_SelectedIndexChanged(null, new EventArgs());
        }        

        //if (CanalVentas_PE.Length > 0)
        //{
        //    ddlCanalVentas.DataSource = CanalVentas_PE;
        //    ddlCanalVentas.DataValueField = "Valor";
        //    ddlCanalVentas.DataTextField = "Descripcion";
        //    ddlCanalVentas.DataBind();
        //}

        //if (TipoEvaluacion_PE.Length > 0)
        //{
        //    ddlTipoEvaluacion.DataSource = TipoEvaluacion_PE;
        //    ddlTipoEvaluacion.DataValueField = "Valor";
        //    ddlTipoEvaluacion.DataTextField = "Descripcion";
        //    ddlTipoEvaluacion.DataBind();
        //}

        if (Promotores.Length > 0)
        {
            ddlPromotor.DataSource = Promotores;            
            ddlPromotor.DataBind();
        }

        if (Origenes.Count() > 0)
        {
            ddlOrigen.DataSource = Origenes;
            ddlOrigen.DataValueField = "IdOrigen";
            ddlOrigen.DataTextField = "Origen";
            ddlOrigen.DataBind();
        }
    }

    private void Balance_VerBalance_Cargar(int IdSolicitud)
    {
        pnlError_Balance_VerBalance_Pagos.Visible = false;
        tpLiquidacion_VerBalance.Visible = false;
        lbtnSolicitudAmplio.Visible = false;
        pnlAplicarPagos.Visible = false;

        using (wsSOPF.SistemaClient ws = new wsSOPF.SistemaClient())
        {
            wsSOPF.CuadreDatosSolicitudEntity eSolicitud = new wsSOPF.CuadreDatosSolicitudEntity() { IdSolicitud = IdSolicitud };
            wsSOPF.ResultadoOfCuadreDatosSolicitudEntityrcuOa4Td resDatosSolicitud = ws.Sistema_ObtenerDatosSolicitud(eSolicitud);

            if (resDatosSolicitud == null)
                throw new Exception("No se encontro la Solicitud: " + eSolicitud.IdSolicitud.ToString());
            if (resDatosSolicitud.Codigo != 0)
                throw new Exception(resDatosSolicitud.Mensaje);

            eSolicitud = resDatosSolicitud.ResultObject;

            // Verificar si el Credito esta Liquidado, cargamos los datos de la Liquidacion
            if (eSolicitud.EstatusCredito.EstatusClave == "L")
                Balance_VerBalance_CargarLiquidacion(IdSolicitud);

            List<wsSOPF.BalanceCuenta.Recibos> eRecibos = null;
            List<wsSOPF.Pago> ePagos = null;
            using (wsSOPF.CobranzaAdministrativaClient wsCobranza = new CobranzaAdministrativaClient())
            {
                eRecibos = wsCobranza.Balance_Obtener_Recibos(new BalanceCuenta() { IdSolicitud = eSolicitud.IdSolicitud }).ToList();
                ePagos = wsCobranza.Pagos_Filtrar(new Pago() { IdSolicitud = eSolicitud.IdSolicitud }).ToList();
            }

            txtIdSolicitud_VerBalance.Text = eSolicitud.IdSolicitud.ToString();
            lblCliente_VerBalance.Text = string.Format("{0} {1} {2}", eSolicitud.ClienteNombre, eSolicitud.ClienteApPaterno, eSolicitud.ClienteApMaterno);
            txtFechaCredito_VerBalance.Text = eSolicitud.FechaCredito.GetValueOrDefault().ToString("dd/MM/yyyy");
            ddlTipoCredito.SelectedValue = eSolicitud.IdTipoCredito.ToString();
            ddlTipoCredito_SelectedIndexChanged(null, new EventArgs());

            ddlPromotor.SelectedValue = eSolicitud.IdPromotor.ToString();
            ddlOrigen.SelectedValue = String.IsNullOrEmpty(eSolicitud.IdOrigen.ToString()) ? "0" : eSolicitud.IdOrigen.ToString();

            ddlTipoConvenio.SelectedValue = eSolicitud.IdTipoConvenio.ToString();
            ddlTipoConvenio_SelectedIndexChanged(null, new EventArgs());
            ddlConvenio.SelectedValue = eSolicitud.IdConvenio.ToString();
            ddlConvenio_SelectedIndexChanged(null, new EventArgs());
            ddlProducto.SelectedValue = eSolicitud.IdProducto.ToString();
            txtTasaInteres.Text = eSolicitud.TasaInteres.ToString();

            txtMontoUsoCanal_VerBalance.Text = eSolicitud.MontoUsoCanal.ToString();
            txtMontoGAT_VerBalance.Text = eSolicitud.MontoGAT.ToString();
            txtCuota_VerBalance.Text = eSolicitud.MontoCuota.ToString();
            txtSeguroDesgravamen.Text = eSolicitud.SeguroDesgravamen.ToString();
            txtCapital.Text = eSolicitud.Capital.ToString("C", CultureInfo.CurrentCulture);
            txtImporteCredito_VerBalance.Text = eSolicitud.ImporteCredito.ToString();
            lblSaldoVencido_VerBalance.Text = eSolicitud.SaldoVencido.ToString("C", CultureInfo.CurrentCulture);
            lblCostoTotalCredito_VerBalance.Text = eSolicitud.CostoTotalCredito.ToString("C", CultureInfo.CurrentCulture);
            ddlEstatusCredito.SelectedValue = eSolicitud.EstatusCredito.IdEstatus.ToString();
            ddlEstatusSolicitud.SelectedValue = eSolicitud.EstatusSolicitud.IdEstatus.ToString();

            txtIdSolicitudAmplia.Text = eSolicitud.DatosAmpliacion.IdSolicitudAmplia.ToString();
            txtFechaAmpliacion.Text = eSolicitud.DatosAmpliacion.FechaLiquidar.GetValueOrDefault().ToString("dd/MM/yyyy");
            txtCapitalLiquida.Text = eSolicitud.DatosAmpliacion.CapitalLiquidar.ToString();
            txtInteresLiquida.Text = eSolicitud.DatosAmpliacion.InteresLiquidar.ToString();
            txtIGVLiquida.Text = eSolicitud.DatosAmpliacion.IGVLiquidar.ToString();
            txtSeguroLiquida.Text = eSolicitud.DatosAmpliacion.SeguroLiquidar.ToString();
            txtGATLiquida.Text = eSolicitud.DatosAmpliacion.GATLiquidar.ToString();
            txtTotalLiquida.Text = eSolicitud.DatosAmpliacion.TotalLiquidar.ToString();
            txtComentariosSistemas.Text = eSolicitud.DatosAmpliacion.ComentariosSistema.ToString();
            
            lbtnSolicitudAmplia.Enabled = (eSolicitud.DatosAmpliacion.IdSolicitudAmplia > 0);            

            if (eSolicitud.IdSolicitudAmplio > 0)
            {
                ViewState["IdSolicitudAmplio"] = eSolicitud.IdSolicitudAmplio;
                lbtnSolicitudAmplio.Visible = true;
            }

            gvRecibos_VerBalance.DataSource = eRecibos;
            gvRecibos_VerBalance.DataBind();

            // AGREGAMOS LAS DEVOLUCIONES AL GRID DE PAGOS

            #region Devoluciones

            wsSOPF.DevolucionCondiciones condiciones = new wsSOPF.DevolucionCondiciones();

            condiciones.idSolicitud = eSolicitud.IdSolicitud;

            using (wsSOPF.CobranzaAdministrativaClient wsCAC = new wsSOPF.CobranzaAdministrativaClient())
            {
                List<wsSOPF.Devolucion> devoluciones = wsCAC.ObtenerDevoluciones(condiciones).ToList();

                if (devoluciones.Count > 0)
                {
                    foreach (wsSOPF.Devolucion dev in devoluciones)
                    {
                        string desc = "DEVOLUCION";
                        string clave = string.Empty;
                        switch(dev.idEstatus) { case 0: clave = "DR";  break; case 1: clave = "DA"; break; case 2: clave = "DE"; break; default: break; }
                        Pago nPago = new Pago()
                        {
                            IdPago = dev.idDevolucion,
                            CanalPagoDesc = desc,
                            FechaPago = new UtilsDateTimeR() { Value = dev.fechaPago.GetValueOrDefault().Date + new TimeSpan(23, 59, 59) },
                            MontoCobrado = dev.montoPago,
                            MontoAplicado = dev.idEstatus == 1 ? dev.montoPago : 0,
                            MontoPorAplicar = dev.idEstatus == 1 ? 0 : dev.montoPago,
                            Estatus = new TBCATEstatus()
                            {
                                EstatusDesc = dev.resultado,
                                EstatusClave = clave
                            }
                        };
                        ePagos.Add(nPago);
                    }
                }
            }

            #endregion

            ePagos.Sort(new Comparison<Pago>((x, y) => DateTime.Compare(x.FechaPago.Value.GetValueOrDefault(), y.FechaPago.Value.GetValueOrDefault())));

            gvPagos_VerBalance.DataSource = ePagos;
            gvPagos_VerBalance.DataBind();
        }
    }

    private void Balance_VerBalance_CargarLiquidacion(int IdSolicitud)
    {
        try
        {
            // Mostrar el Tab Panel Liquidacion que esta oculto por Default
            tpLiquidacion_VerBalance.Visible = true;

            using (wsSOPF.CreditoClient ws = new CreditoClient())
            {
                wsSOPF.TBCreditos.Liquidacion LiquidacionCredito = new TBCreditos.Liquidacion();
                LiquidacionCredito.IdSolicitud = IdSolicitud;

                // Obtener los datos de la Liquidacion del Credito
                LiquidacionCredito = ws.ObtenerLiquidacionCredito(LiquidacionCredito);

                if (LiquidacionCredito == null)
                    throw new Exception("Error al cargar la Liquidacion del Credito");

                lblIdLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.IdLiquidacion.ToString();
                lblNumeroTransaccionLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.NumeroTransaccion;
                lblFechaLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.FechaLiquidar.Value.GetValueOrDefault().ToString("dd/MM/yyyy");
                lblCapitalLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.CapitalLiquidar.ToString("C", CultureInfo.CurrentCulture);
                lblInteresLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.InteresLiquidar.ToString("C", CultureInfo.CurrentCulture);
                lblIGVLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.IGVLiquidar.ToString("C", CultureInfo.CurrentCulture);
                lblSeguroLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.SeguroLiquidar.ToString("C", CultureInfo.CurrentCulture);
                lblGATLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.GATLiquidar.ToString("C", CultureInfo.CurrentCulture);
                lblTotalLiquidacion_VerBalance_Liquidacion.Text = LiquidacionCredito.TotalLiquidar.ToString("C", CultureInfo.CurrentCulture);

                gvRecibos_VerBalance_Liquidacion.DataSource = LiquidacionCredito.Detalle;
                gvRecibos_VerBalance_Liquidacion.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblError_VerBalance_Liquidacion.Text = ex.Message;
            pnlError_VerBalance_Liquidacion.Visible = true;
        }
    }

    #endregion           
}