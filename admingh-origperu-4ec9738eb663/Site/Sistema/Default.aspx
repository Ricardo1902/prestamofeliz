﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" MasterPageFile="~/Site.master" Inherits="Site_Sistema_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function ()
        {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            // Activar Tooltips
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })                   
        });
    </script>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Administracion del Sistema</h3>
        </div>
        <div class="panel-body" style="padding: 40px;">
            <div class="form-group col-xs-4 col-sm-4 col-md-4">
                <a href="frmSeguridad.aspx" class="btn btn-default" data-toggle="tooltip" title="Administrar Usuarios">
                    <i class="fas fa-users fa-5x"></i>
                </a>
            </div>

            <div class="form-group col-xs-4 col-sm-4 col-md-4">
                <a href="CuadreCartera.aspx" class="btn btn-default" data-toggle="tooltip" title="Cuadre Cartera">
                    <i class="fas fa-hand-holding-usd fa-5x"></i>
                </a>
            </div>
            
            <div class="form-group col-xs-4 col-sm-4 col-md-4">
                <a href="Parametros.aspx" class="btn btn-default" data-toggle="tooltip" title="Parametros del Sistema">
                    <i class="fas fa-microchip fa-5x"></i>
                </a>
            </div>                                                          
        </div>               
    </div>
</asp:Content>
