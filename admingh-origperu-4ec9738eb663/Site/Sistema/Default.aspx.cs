﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Site_Sistema_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Permitir el acceso solo al usuario de PRODUCCIONTI (21)
        if (int.Parse(Session["UsuarioId"].ToString()) != 21)
            throw new Exception("Acceso no Permitido");
    }
}