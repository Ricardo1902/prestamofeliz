﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Parametros.aspx.cs" Inherits="Site_Sistema_Parametros" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <h3>Parametros</h3>
            <hr />
        </div>
    </div>

    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <table id="tbParametros" class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th>Parametro</th>
                        <th>Descripcion</th>
                        <th>Valor</th>
                        <th>Estatus</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    <script id="EditTemplate" type="text/template">
        <div class="row text-left" style="margin-top: 15px;">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="inpParametro">Parametro</label>
                    <input class="form-control" id="inpParametro" placeholder="Parametro" value="{Parametro}" />
                </div>
                <div class="form-group">
                    <label for="inpDescripcion">Descripcion</label>
                    <textarea id="inpDescripcion" class="form-control" placeholder="Descripcion" rows="3" cols="1">{Descripcion}</textarea>
                </div>
                <div class="form-group">
                    <label for="inpValor">Valor</label>
                    <input class="form-control" id="inpValor" placeholder="Valor" value="{Valor}" />
                </div>
                <div class="form-group">
                    <label class="radio-inline" style="line-height: 20px;">
                        <input type="radio" name="ParametroEstatus" value="1" {ActivoChecked} /> Activo
                    </label>
                    <label class="radio-inline" style="line-height: 20px;">
                        <input type="radio" name="ParametroEstatus" value="2" {InactivoChecked} /> Inactivo
                    </label>
                </div>
            </div>
        </div>
    </script>

    <script type="text/javascript" src="../../Scripts/sweetalert2.all.min.js"></script>

    <script type="text/javascript">
        // Polyfills
        String.prototype.formatUnicorn = function () {
            var e = this.toString();
            if (!arguments.length)
                return e;
            var t = typeof arguments[0], n = "string" === t || "number" === t ? Array.prototype.slice.call(arguments) : arguments[0];
            for (var i in n)
                e = e.replace(new RegExp("\\{" + i + "\\}", "gi"), n[i]);
            return e;
        }

        // DOM Elements
        const tbodyParametros = document.querySelector("#tbParametros > tbody");
        const EditTemplate = document.getElementById("EditTemplate").innerHTML;

        const CargarParametros = async () => {
            const response = await fetch("Parametros.aspx/ObtenerParametros", {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            const json = await response.json();
            const data = JSON.parse(json.d);

            if (data.Exito) {
                console.log(data.Parametros);
                data.Parametros.forEach(parametro => {
                    tbodyParametros.insertAdjacentHTML('beforeend', `
                        <tr>
                            <td>${parametro.Parametro}</td>
                            <td>${parametro.Descripcion}</td>
                            <td>${parametro.Valor}</td>
                            <td>${(parametro.IdEstatus) == 1 ? "Activo" : "Inactivo"}</td>
                            <td style='cursor: pointer'>
                                <i 
                                    class="fas fa-edit"
                                    data-parametro="${parametro.Parametro}"
                                    data-descripcion="${parametro.Descripcion}"
                                    data-valor="${parametro.Valor}"
                                    data-idestatus="${parametro.IdEstatus}"
                                    onclick="EditarParametro(${parametro.IdParametro}, this)"
                                ></i>
                            </td>
                        </tr>
                    `);
                });
            }
            else {
                alert("Sucedio un error inesperado, favor de intentarlo de nuevo.");
            }
        }

        (function () {
            CargarParametros();
        })();

        const EditarParametro = (IdParametro, Element) => {
            const Parametro = Element.dataset.parametro;
            const Descripcion = Element.dataset.descripcion;
            const Valor = Element.dataset.valor;
            const IdEstatus = Element.dataset.idestatus

            Swal.fire({
                title: `Editando parametro ${Parametro}`,
                html: EditTemplate.formatUnicorn({
                    Parametro: Parametro,
                    Descripcion: Descripcion,
                    Valor: Valor,
                    ActivoChecked: IdEstatus == 1 ? "checked='checked'" : "",
                    InactivoChecked: IdEstatus == 2 ? "checked='checked'" : ""
                }),
                padding: "3em",
                width: 600,
                confirmButtonText: "Editar",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                reverseButtons: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    let newParametro = document.getElementById("inpParametro").value;
                    let newDescripcion = document.getElementById("inpDescripcion").value;
                    let newValor = document.getElementById("inpValor").value;
                    let EstatusRadioButton = document.getElementsByName("ParametroEstatus");
                    let newIdEstatus = 0;

                    EstatusRadioButton.forEach(radio => {
                        if (radio.checked) {
                            newIdEstatus = parseInt(radio.value);
                        }
                    });

                    let json = {
                        IdParametro: IdParametro,
                        Parametro: newParametro,
                        Valor: newValor,
                        Descripcion: newDescripcion,
                        IdEstatus: newIdEstatus
                    };

                    console.log(json);

                    return fetch("Parametros.aspx/ActualizarParametro", {
                        method: 'POST',
                        body: "{'ParametroJSON':'" + JSON.stringify(json) + "'}",
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }
                        return response.json()
                    }).catch(error => {
                        Swal.showValidationMessage(
                            `Request failed: ${error}`
                        )
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then(result => {
                let respuesta = JSON.parse(result.value.d);

                Swal.fire({
                    width: "50rem",
                    padding: "2rem",
                    icon: respuesta.Exito ? "success" : "error",
                    title: respuesta.Exito ? "Edicion correcta" : "Ocurrio un error!",
                    text: respuesta.Mensaje,
                    confirmButtonText: 'Aceptar',
                    confirmButtonColor: '#3085d6',
                    willClose: () => {
                        location.reload();
                    }
                });
            });;
        };
    </script>
</asp:Content>
