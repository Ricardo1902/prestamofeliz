﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Site_Sistema_Parametros : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static string ObtenerParametros()
    {
        wsSOPF.CatalogoClient catalogoClient = new wsSOPF.CatalogoClient();

        try
        {
            List<wsSOPF.TB_CATParametro1> parametros = catalogoClient.ObtenerCATParametros().ToList();

            return JsonConvert.SerializeObject(new
            {
                Exito = true,
                Parametros = parametros
            });
        }
        catch (Exception ex)
        {
            return JsonConvert.SerializeObject(new
            {
                Exito = false,
                Mensaje = ex.Message,
                Parametros = (List<wsSOPF.TB_CATParametro1>)null
            });
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string ActualizarParametro(string ParametroJSON)
    {
        wsSOPF.CatalogoClient catalogoClient = new wsSOPF.CatalogoClient();
        wsSOPF.TB_CATParametro1 Parametro = JsonConvert.DeserializeObject<wsSOPF.TB_CATParametro1>(ParametroJSON);

        try
        {
            bool Exito = catalogoClient.ActualizarCATParametros(
                Parametro.IdParametro,
                Parametro.Parametro,
                Parametro.Valor,
                Parametro.Descripcion,
                Parametro.IdEstatus
            );

            return JsonConvert.SerializeObject(new
            {
                Exito = Exito,
                Mensaje = "Se actualizo el parametro correctamente."
            });
        }
        catch (Exception ex)
        {
            return JsonConvert.SerializeObject(new
            {
                Exito = false,
                Mensaje = ex.Message
            });
        }
    }
}