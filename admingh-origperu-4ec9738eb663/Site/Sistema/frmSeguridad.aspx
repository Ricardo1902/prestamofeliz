﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmSeguridad.aspx.cs" Inherits="Site_Sistema_frmSeguridad" MasterPageFile="~/Site.master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolKit"%>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .menu_item:hover {
            color: #555;
            text-decoration: none;
            background-color: #f5f5f5;
            cursor: pointer;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">     
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Sistema/Seguridad</h3>
        </div>
        <div class="panel-body">
            <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
                <ProgressTemplate>                 
                    <div id="loader-background"></div>
                    <div id="loader-content"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upMain" runat="server">
            <ContentTemplate>                               
                <ajaxtoolKit:TabContainer ID="tcMain" CssClass="actTab" runat="server">
                    <ajaxtoolKit:TabPanel HeaderText="Usuarios" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlBuscar_Usuarios" runat="server">
                            <div class="col-sm-12">
                                <h4>Buscar Usuarios</h4>
                                <hr />
                            </div>
                            <div class="form-group col-sm-6 col-md-4 col-lg-4">
                                <label class="form-control-label small">Nombre/Usuario</label>
                                <asp:textbox id="txtNombre_Usuarios" runat="server" CssClass="form-control"></asp:textbox>
                            </div>            
                            
                            <div class="form-group col-sm-6 col-md-4 col-lg-3">           
                                <label class="form-control-label small">Estatus</label>
                                <asp:dropdownlist id="ddlEstatus_Usuarios" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="Todos" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="Activo" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>                    
                                </asp:dropdownlist>
                            </div>                                                                                              

                            <div class="col-sm-12">            
                                <asp:LinkButton ID="btnBuscar_Usuarios" runat="server" OnClick="btnBuscar_Usuarios_Click" CssClass="btn btn-primary btn-sm">
                                    <i class="fas fa-search"></i> Buscar
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnNuevoUsuario" OnClick="btnNuevoUsuario_Click" runat="server" Text="Nuevo" CssClass="btn btn-success btn-sm">
                                    <i class="fas fa-plus-circle"></i> Nuevo
                                </asp:LinkButton>
                            </div>    
                            
                            <asp:Panel ID="pnlError_Buscar_Usuarios" class="form-group col-sm-12" Visible="false" runat="server">                                
                                <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                    <asp:Label ID="lblError_Buscar_Usuarios" runat="server"></asp:Label>
                                </div>
                            </asp:Panel>                                                                                                         

                            <div class="form-group col-sm-12">
                                <div style="height:auto; width: auto; overflow-x: auto;">
                                    <hr />
                                    <asp:GridView ID="gvBuscar_Usuarios" runat="server" Width="100%" AutoGenerateColumns="False"
                                        HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                        OnPageIndexChanging="gvBuscar_Usuarios_PageIndexChanging"
                                        OnRowCommand="gvBuscar_Usuarios_RowCommand"
                                        PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        Style="overflow-x:auto;">                                        
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnDetalleBalance" CommandName="Detalle" CommandArgument='<%# Eval("IdUsuario") %>' data-toggle="tooltip" title="Detalle" runat="server">
                                                        <i class="far fa-edit"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                            <asp:BoundField HeaderText="Perfil" DataField="TipoUsuario.TipoUsuario" ItemStyle-CssClass="small" />                                           
                                             <asp:TemplateField HeaderText="Nombre" ItemStyle-CssClass="small">
                                                <ItemTemplate>
                                                    <%# Eval("Nombre") + " " + Eval("ApellidoPaterno") + " " + Eval("ApellidoMaterno")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Usuario" DataField="Usuario" ItemStyle-CssClass="small" />
                                            <asp:TemplateField HeaderText="Estatus" ItemStyle-CssClass="small">
                                                <ItemTemplate>
                                                    <%# (int.Parse(Eval("IdEstatus").ToString()) == 1) ? "Activo" : "Inactivo" %>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                            
                                        </Columns>                                        
                                        <PagerStyle CssClass="pagination-ty warning" />                                                                                
                                        <EmptyDataTemplate>
                                            No se encontraron Usuarios con los filtros seleccionados
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>  
                            </div>
                            </asp:Panel>

                            <asp:Panel ID="pnlDetalle_Usuarios" Visible="false" runat="server">
                                <div class="form-group col-xs-12"> 
                                    <asp:Panel ID="pnlError_Detalle_Usuarios" class="form-group col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <asp:Label ID="lblError_Detalle_Usuarios" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                    
                                    <asp:Panel ID="pnlExito_Detalle_Usuarios" class="form-group col-sm-12 fadeSlideUp" Visible="false" runat="server">                                
                                        <div class="alert alert-success" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                                            <asp:Label ID="lblExito_Detalle_Usuarios" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="form-group col-xs-12">
                                    <asp:LinkButton ID="lbtnRegresar_Detalle_Usuarios" CssClass="btn btn-sm btn-default hidden-print" OnClick="lbtnRegresar_Detalle_Usuarios_Click" runat="server">
                                        <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                                    </asp:LinkButton>
                                    
                                    <asp:LinkButton ID="lbtnGuardar_Detalle_Usuarios" CssClass="btn btn-sm btn-success hidden-print" OnClick="lbtnGuardar_Detalle_Usuarios_Click" runat="server">
                                        <i class="fas fa-save"></i> Guardar
                                    </asp:LinkButton>                                                                      
                                                                        
                                    <hr class="hidden-print" />

                                    <div class="col-sm-12">
                                        <h4 id="lblTituloDetalle_Usuarios" runat="server"></h4>
                                        <hr />
                                    </div>                                                                      
                                    
                                    <div class="col-md-6">
                                        <label class="small col-md-4">Nombre</label>
                                        <div class="form-group col-md-8">                                                  
                                            <asp:TextBox ID="txtNombre_Detalle_Usuarios" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                         <label class="small col-md-4">Apellido Paterno</label>
                                        <div class="form-group col-md-8">                                                  
                                            <asp:TextBox ID="txtApellidoPaterno_Detalle_Usuarios" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="small col-md-4">Apellido Materno</label>
                                        <div class="form-group col-md-8">
                                            <asp:TextBox ID="txtApellidoMaterno_Detalle_Usuarios" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="small col-md-4">Correo</label>
                                        <div class="form-group col-md-8">                                        
                                            <asp:TextBox ID="txtCorreo_Detalle_Usuarios" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="small col-md-4">Perfil</label>
                                        <div class="form-group col-md-8">                                        
                                            <asp:DropDownList ID="ddlTipoUsuario_Detalle_Usuarios" CssClass="form-control" DataValueField="IdTipoUsuario" DataTextField="TipoUsuario" AppendDataBoundItems="true" runat="server">                                            
                                                <asp:ListItem Value="-1" Text="- Seleccione-"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>                                                                                                                                             

                                    <div class="col-md-6">                                                                            
                                        <label class="small col-md-4">Departamento</label>
                                        <div class="form-group col-md-8">                                            
                                            <asp:DropDownList ID="ddlDepartamento_Detalle_Usuarios" CssClass="form-control" DataValueField="IdDepartamento" DataTextField="Departamento" AppendDataBoundItems="true" runat="server">                                            
                                                <asp:ListItem Value="-1" Text="- Seleccione-"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    
                                        <label class="small col-md-4">Sucursal</label>
                                        <div class="form-group col-md-8">                                            
                                            <asp:DropDownList ID="ddlSucursal_Detalle_Usuarios" CssClass="form-control" DataValueField="IdSucursal" DataTextField="Sucursal" AppendDataBoundItems="true" runat="server">                                            
                                                <asp:ListItem Value="-1" Text="- Seleccione-"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    
                                        <label class="small col-md-4">Usuario</label>
                                        <div class="form-group col-md-8">                                        
                                            <asp:TextBox ID="txtUsuario_Detalle_Usuarios" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>    
                                    
                                        <label class="small col-md-4">Clave</label>
                                        <div class="form-group col-md-8 input-group">                                            
                                            <span class="input-group-addon">
                                                <asp:LinkButton ID="lbtnCambiarClave" OnClick="lbtnCambiarClave_Click" data-toggle="tooltip" title="Cambiar Clave" runat="server"><i class="fas fa-lock"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lbtnCancelarCambiarClave" OnClick="lbtnCancelarCambiarClave_Click" data-toggle="tooltip" title="Cancelar" runat="server"><i class="fas fa-unlock-alt"></i></asp:LinkButton>
                                            </span>
                                            <asp:TextBox ID="txtClave_Detalle_Usuarios" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="col-md-2 small">Estatus</label>
                                        <div class="form-group col-md-4">                                            
                                            <asp:DropDownList ID="ddlEstatus_Detalle_Usuarios" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1" Text="Activo"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Inactivo"></asp:ListItem>                                            
                                            </asp:DropDownList>
                                        </div>  
                                    
                                        <label class="col-md-3 small">Reset(Clave)</label>
                                        <div class="form-group col-md-3">                                            
                                            <asp:DropDownList ID="ddlInicializar_Detalle_Usuarios" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>                        
                                    </div>

                                    <div class="form-group clearfix"></div>

                                    <asp:HiddenField ID="hdn_idUsuario" runat="server" Value="0" />
                                    <asp:Panel ID="pnlAdministradorMenus" CssClass="col-12" runat="server">
                                        <ajaxtoolKit:TabContainer ID="TabContainer1" CssClass="actTab" runat="server">
                                            <ajaxtoolKit:TabPanel HeaderText="<i class='fas fa-th-list'></i> Menu Acceso" runat="server">
                                                <ContentTemplate>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
                                                            <input type="text" placeholder="Buscar" class="form form-control" oninput="filterList(event, 'contentMenuList')" />

                                                            <ul class="list-group" style="margin-top: 10px; max-height: 350px; overflow-y: auto; height: 350px; border: 0px; box-shadow: none; padding-bottom: 1px;" id="contentMenuList"></ul>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </ajaxtoolKit:TabPanel>
                                            <ajaxtoolKit:TabPanel HeaderText="<i class='fas fa-clipboard-list'></i> Reportes Acceso" runat="server">
                                                <ContentTemplate>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
                                                            <input type="text" placeholder="Buscar" class="form form-control" oninput="filterList(event, 'contentReportList')" />

                                                            <ul class="list-group" style="margin-top: 10px; max-height: 350px; overflow-y: auto; height: 350px; border: 0px; box-shadow: none; padding-bottom: 1px;" id="contentReportList"></ul>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </ajaxtoolKit:TabPanel>
                                            <ajaxtoolKit:TabPanel HeaderText="<i class='fas fa-bolt'></i> Menu Acciones" runat="server">
                                                <ContentTemplate>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
                                                            <input type="text" placeholder="Buscar" class="form form-control" oninput="filterList(event, 'contentActionList')" />

                                                            <ul class="list-group" style="margin-top: 10px; max-height: 350px; overflow-y: auto; height: 350px; border: 0px; box-shadow: none; padding-bottom: 1px;" id="contentActionList"></ul>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </ajaxtoolKit:TabPanel>
                                        </ajaxtoolKit:TabContainer>
                                    </asp:Panel>

                                    <asp:Panel ID="pnlMenuAcceso_Detalle_Usuarios" CssClass="col-sm-12 col-md-6 hidden" runat="server">
                                        <h5><i class="fas fa-th-list"></i> Menu Acceso</h5>
                                        <hr />
                                        <div class="form-group col-sm-12 input-group">                                                                                        
                                            <asp:DropDownList ID="ddlAgregarMenuAcceso_Detalle_Usuarios" DataValueField="IdMenu" DataTextField="Menu" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <span class="input-group-addon">
                                                <asp:LinkButton ID="lbtnAgregarMenuAcceso_Detalle_Usuarios" OnClick="lbtnAgregarMenuAcceso_Detalle_Usuarios_Click" data-toggle="tooltip" title="Agregar Acceso" runat="server"><i class="fas fa-plus-square"></i></asp:LinkButton>
                                            </span>
                                        </div>
                                        <div style="height:auto; width: auto; overflow-x: auto;">                                                    
                                            <asp:GridView ID="gvMenu_Detalle_Usuarios" runat="server" Width="100%" AutoGenerateColumns="False"                                                
                                                HeaderStyle-CssClass="info" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                CssClass="table table-bordered table-striped table-hover-warning table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None"
                                                RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                OnRowCommand="gvMenu_Detalle_Usuarios_RowCommand">
                                                <Columns>                                                                                               
                                                    <asp:BoundField HeaderText="Menu" DataField="Menu" ItemStyle-CssClass="small" />
                                                     <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnQuitaAcceso" CommandName="QuitarAcceso" CommandArgument='<%# Eval("IdMenu") %>' data-toggle="tooltip" title="Quitar Acceso" runat="server">
                                                                <i class="far fa-trash-alt"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>                                                 
                                                <EmptyDataRowStyle CssClass="info small" />                                               
                                                <EmptyDataTemplate>
                                                    <i class="fas fa-info-circle"></i> El usuario no tiene acceso a ninguna opción de Menu del Sistema
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>  
                                    </asp:Panel>
                                    
                                    <asp:Panel ID="pnlReportesAcceso_Detalle_Usuarios" CssClass="col-sm-12 col-md-6 hidden" runat="server">
                                        <h5><i class="fas fa-clipboard-list"></i> Reportes Acceso</h5>
                                        <hr />
                                        <div class="form-group col-sm-12 input-group">                                                                                        
                                            <asp:DropDownList ID="ddlAgregarReporteAcceso_Detalle_Usuarios" DataValueField="IdReporte" DataTextField="Nombre" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <span class="input-group-addon">
                                                <asp:LinkButton ID="lbtnAgregarReporteAcceso" OnClick="lbtnAgregarReporteAcceso_Detalle_Usuarios_Click" data-toggle="tooltip" title="Agregar Acceso" runat="server"><i class="fas fa-plus-square"></i></asp:LinkButton>
                                            </span>
                                        </div>
                                        <div style="height:auto; width: auto; overflow-x: auto;">                                                    
                                            <asp:GridView ID="gvReportes_Detalle_Usuarios" runat="server" Width="100%" AutoGenerateColumns="False"
                                                HeaderStyle-CssClass="info" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                                CssClass="table table-bordered table-striped table-hover-warning table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None"
                                                RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                                OnRowCommand="gvReportes_Detalle_Usuarios_RowCommand">                                        
                                                <Columns>                                                    
                                                    <asp:BoundField HeaderText="Reporte" DataField="Nombre" ItemStyle-CssClass="small" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnQuitaAcceso" CommandName="QuitarAcceso" CommandArgument='<%# Eval("IdReporte") %>' data-toggle="tooltip" title="Quitar Acceso" runat="server">
                                                                <i class="far fa-trash-alt"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataRowStyle CssClass="info small" />                                               
                                                <EmptyDataTemplate>
                                                    <i class="fas fa-info-circle"></i> El usuario no tiene acceso a ningun Reporte
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>  
                                    </asp:Panel>                                                                    
                                </div>                                  
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxtoolKit:TabPanel>                                        
                </ajaxtoolKit:TabContainer>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>       
    <asp:HiddenField ID="filtrarSucursal" runat="server" />
    <script type="text/javascript">     
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            // Activar Tooltips
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            $(".fadeSlideUp").fadeTo(2000, 500).slideUp(500, function () {
                $(".fadeSlideUp").slideUp(500);
            });
        });

        async function toggleInnerCheckbox(event, accessToEdit) {
            event.stopPropagation();

            const el = event.target;
            const innerCheckbox = $(el).find("input")[0];

            if (innerCheckbox) {
                const checked = !$(innerCheckbox).prop("checked");

                switch (accessToEdit) {
                    case "MenuAccess":
                        await updateMenuAction(parseInt($(innerCheckbox).val()), checked ? "add" : "remove");
                        break;
                    case "ReportAccess":
                        await updateReportAction(parseInt($(innerCheckbox).val()), checked ? "add" : "remove");
                        break;
                    case "ActionAccess":
                        await updateActionUser(parseInt($(innerCheckbox).val()), checked ? "add" : "remove");
                        break;
                    default:
                }

                $(innerCheckbox).prop("checked", checked);
            }
        }

        function fillMenuAccessList(menuList, userMenus) {
            menuList.forEach(menu => {
                const userHasMenu = userMenus.find(m => m.IdMenu == menu.IdMenu)?.IdMenu;

                $("#contentMenuList").append(`
                    <li onclick="toggleInnerCheckbox(event, 'MenuAccess')" class="list-group-item menu_item" data-filter="${menu.Menu}">
                        <span style="float: right;">
                            <input type="checkbox" value="${menu.IdMenu}" name="chkMenuAccessList" ${userHasMenu ? "checked='checked'" : ""}">
                        </span>
                        ${menu.Menu}
                    </li>
                `);
            });
        }

        function fillReportAccessList(reportList, userReports) {
            reportList.forEach(report => {
                const userHasReport = userReports.find(r => r.IdReporte == report.IdReporte)?.IdReporte;

                $("#contentReportList").append(`
                    <li onclick="toggleInnerCheckbox(event, 'ReportAccess')" class="list-group-item menu_item" data-filter="${report.Nombre}">
                        <span style="float: right;">
                            <input type="checkbox" value="${report.IdReporte}" name="chkMenuAccessList" ${userHasReport ? "checked='checked'" : ""}">
                        </span>
                        ${report.Nombre}
                    </li>
                `);
            });
        }

        function fillActionAccessList(actionList, userActions) {
            actionList.forEach(action => {
                const userHasAction = userActions.find(r => r.IdMenuAccion == action.IdMenuAccion)?.IdMenuAccion;

                $("#contentActionList").append(`
                    <li onclick="toggleInnerCheckbox(event, 'ActionAccess')" class="list-group-item menu_item" data-filter="${action.Menu} - ${action.Accion}">
                        <span style="float: right;">
                            <input type="checkbox" value="${action.IdMenuAccion}" name="chkMenuAccessList" ${userHasAction ? "checked='checked'" : ""}">
                        </span>
                        ${action.Menu} - ${action.Accion}
                    </li>
                `);
            });
        }

        function filterList(event, contentList) {
            const inp = event.target;
            const items = $(`#${contentList}`).children("li");

            if (items) {
                const arr = Array.from(items);
                const visibles = arr.filter(item => $(item).data("filter").toLowerCase().includes(inp.value.toLowerCase()));

                $(items).each((index, item) => {
                    const isVisible = visibles.find(v => $(v).data("filter").toLowerCase() == $(item).data("filter").toLowerCase());

                    if (isVisible) {
                        $(item).removeClass("hidden");
                    }
                    else {
                        $(item).addClass("hidden");
                    }
                });
            }
        }

        async function updateMenuAction(idMenu, action) {
            const idUsuario = parseInt($("#ctl00_MainContent_tcMain_ctl00_hdn_idUsuario").val());
            const resp = await fetch(`frmSeguridad.aspx/ActualizarMenuAcceso`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    idUsuario: idUsuario,
                    idMenu: idMenu,
                    action: action
                })
            });
            const data = await resp.json();
        }

        async function updateReportAction(idReport, action) {
            const idUsuario = parseInt($("#ctl00_MainContent_tcMain_ctl00_hdn_idUsuario").val());
            const resp = await fetch(`frmSeguridad.aspx/ActualizarReporteAcceso`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    idUsuario: idUsuario,
                    idReport: idReport,
                    action: action
                })
            });
            const data = await resp.json();
        }

        async function updateActionUser(idMenuAccion, action) {
            const idUsuario = parseInt($("#ctl00_MainContent_tcMain_ctl00_hdn_idUsuario").val());
            const resp = await fetch(`frmSeguridad.aspx/ActualizarAccionAcceso`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    idUsuario: idUsuario,
                    idMenuAccion: idMenuAccion,
                    action: action
                })
            });
            const data = await resp.json();
        }
</script>
</asp:Content>
