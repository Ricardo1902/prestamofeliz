﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using wsSOPF;
using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Site_Sistema_frmSeguridad : System.Web.UI.Page
{
    #region "Propiedades"

    private int IdUsuarioDetalle
    {
        set
        {
            ViewState[this.UniqueID + "_IdUsuarioDetalle"] = value;
        }
        get
        {
            return (int)ViewState[this.UniqueID + "_IdUsuarioDetalle"];
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                CargarComboTipoUsuario();
                CargarComboSucursal();
                CargarComboDepartamento();
                CargarComboMenuAcceso();
                CargarComboReporteAcceso();
            }
        }
        catch (Exception ex)
        {
            pnlError_Buscar_Usuarios.Visible = true;
            lblError_Buscar_Usuarios.Text = ex.Message;
        }
    }

    #region "Usuarios"

    protected void btnBuscar_Usuarios_Click(object sender, EventArgs e)
    {
        pnlError_Buscar_Usuarios.Visible = false;

        try
        {
            Usuarios_Filtrar();
        }
        catch (Exception ex)
        {
            lblError_Buscar_Usuarios.Text = ex.Message;
            pnlError_Buscar_Usuarios.Visible = true;
        }
    }

    protected void btnNuevoUsuario_Click(object sender, EventArgs e)
    {
        this.IdUsuarioDetalle = 0;

        lblTituloDetalle_Usuarios.InnerText = "Agregar Usuario";

        pnlBuscar_Usuarios.Visible = false;
        pnlDetalle_Usuarios.Visible = true;

        txtNombre_Detalle_Usuarios.Text = string.Empty;
        txtApellidoPaterno_Detalle_Usuarios.Text = string.Empty;
        txtApellidoMaterno_Detalle_Usuarios.Text = string.Empty;
        txtUsuario_Detalle_Usuarios.Text = string.Empty;
        txtCorreo_Detalle_Usuarios.Text = string.Empty;
        txtClave_Detalle_Usuarios.Text = string.Empty;
        txtClave_Detalle_Usuarios.Enabled = true;

        lbtnCambiarClave.Visible = false;
        lbtnCancelarCambiarClave.Visible = false;

        ddlTipoUsuario_Detalle_Usuarios.SelectedIndex = 0;
        ddlSucursal_Detalle_Usuarios.SelectedIndex = 0;
        ddlDepartamento_Detalle_Usuarios.SelectedIndex = 0;
        ddlEstatus_Detalle_Usuarios.SelectedIndex = 0;
        ddlInicializar_Detalle_Usuarios.SelectedIndex = 0;

        pnlExito_Detalle_Usuarios.Visible = false;
        pnlError_Detalle_Usuarios.Visible = false;
        pnlReportesAcceso_Detalle_Usuarios.Visible = false;
        pnlMenuAcceso_Detalle_Usuarios.Visible = false;
    }

    protected void lbtnRegresar_Detalle_Usuarios_Click(object sender, EventArgs e)
    {
        pnlBuscar_Usuarios.Visible = true;
        pnlDetalle_Usuarios.Visible = false;        
    }

    protected void lbtnAgregarMenuAcceso_Detalle_Usuarios_Click(object sender, EventArgs e)
    {
        using (wsSOPF.SistemaClient ws = new wsSOPF.SistemaClient())
        {
            int IdMenu = 0;

            int.TryParse(ddlAgregarMenuAcceso_Detalle_Usuarios.SelectedValue, out IdMenu);

            if (IdMenu > 0)
            {
                ws.Sistema_InsertarAccesoMenuUsuario(this.IdUsuarioDetalle, IdMenu);
                Detalle_Usuarios_Cargar(this.IdUsuarioDetalle);
            }
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ActualizarMenuAcceso(int idUsuario, int idMenu, string action)
    {
        bool success = false;
        string msg = "";

        try
        {
            using (wsSOPF.SistemaClient ws = new wsSOPF.SistemaClient())
            {
                if (idUsuario > 0 && idMenu > 0)
                {
                    if (action == "add")
                    {
                        ws.Sistema_InsertarAccesoMenuUsuario(idUsuario, idMenu);
                    }
                    else
                    {
                        ws.Sistema_QuitarAccesoMenuUsuario(idUsuario, idMenu);
                    }
                    success = true;
                }
            }
        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }

        return new { success, message = msg };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ActualizarReporteAcceso(int idUsuario, int idReport, string action)
    {
        bool success = false;
        string msg = "";

        try
        {
            using (wsSOPF.SistemaClient ws = new wsSOPF.SistemaClient())
            {
                if (idUsuario > 0 && idReport > 0)
                {
                    if (action == "add")
                    {
                        ws.Sistema_InsertarAccesoReporteUsuario(idUsuario, idReport);
                    }
                    else
                    {
                        ws.Sistema_QuitarAccesoReporteUsuario(idUsuario, idReport);
                    }
                    success = true;
                }
            }
        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }

        return new { success, message = msg };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ActualizarAccionAcceso(int idUsuario, int idMenuAccion, string action)
    {
        bool success = false;
        string msg = "";

        try
        {
            using (wsSOPF.SistemaClient ws = new wsSOPF.SistemaClient())
            {
                if (idUsuario > 0 && idMenuAccion > 0)
                {
                    if (action == "add")
                    {
                        ws.Sistema_InsertarAccesoAccionUsuario(idUsuario, idMenuAccion);
                    }
                    else
                    {
                        ws.Sistema_QuitarAccesoAccionUsuario(idUsuario, idMenuAccion);
                    }
                    success = true;
                }
            }
        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }

        return new { success, message = msg };
    }

    protected void lbtnAgregarReporteAcceso_Detalle_Usuarios_Click(object sender, EventArgs e)
    {
        using (wsSOPF.SistemaClient ws = new wsSOPF.SistemaClient())
        {
            int IdReporte = 0;

            int.TryParse(ddlAgregarReporteAcceso_Detalle_Usuarios.SelectedValue, out IdReporte);

            if (IdReporte > 0)
            {
                ws.Sistema_InsertarAccesoReporteUsuario(this.IdUsuarioDetalle, IdReporte);
                Detalle_Usuarios_Cargar(this.IdUsuarioDetalle);
            }
        }
    }

    protected void gvMenu_Detalle_Usuarios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "QuitarAcceso":
                using (wsSOPF.SistemaClient ws = new SistemaClient())
                {
                    ws.Sistema_QuitarAccesoMenuUsuario(this.IdUsuarioDetalle, int.Parse(e.CommandArgument.ToString()));
                }

                Detalle_Usuarios_Cargar(this.IdUsuarioDetalle);
                break;
        }
    }

    protected void gvReportes_Detalle_Usuarios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "QuitarAcceso":
                using (wsSOPF.SistemaClient ws = new SistemaClient())
                {
                    ws.Sistema_QuitarAccesoReporteUsuario(this.IdUsuarioDetalle, int.Parse(e.CommandArgument.ToString()));
                }

                Detalle_Usuarios_Cargar(this.IdUsuarioDetalle);
                break;
        }
    }

    protected void lbtnGuardar_Detalle_Usuarios_Click(object sender, EventArgs e)
    {
        try
        {
            using (wsSOPF.SistemaClient ws = new SistemaClient())
            {
                wsSOPF.UsuarioEntity entity = new UsuarioEntity();

                entity.IdUsuario = this.IdUsuarioDetalle;
                entity.Nombre = txtNombre_Detalle_Usuarios.Text.Trim().ToUpper();
                entity.ApellidoPaterno = txtApellidoPaterno_Detalle_Usuarios.Text.Trim().ToUpper();
                entity.ApellidoMaterno = txtApellidoMaterno_Detalle_Usuarios.Text.Trim().ToUpper();
                entity.Email = txtCorreo_Detalle_Usuarios.Text.Trim();
                entity.Usuario = txtUsuario_Detalle_Usuarios.Text.Trim().ToUpper();
                entity.Clave = txtClave_Detalle_Usuarios.Text.Trim();

                entity.TipoUsuario = new TipoUsuarioEntity();
                entity.TipoUsuario.IdTipoUsuario = int.Parse(ddlTipoUsuario_Detalle_Usuarios.SelectedValue);
                entity.Sucursal = new SucursalEntity();
                entity.Sucursal.IdSucursal = int.Parse(ddlSucursal_Detalle_Usuarios.SelectedValue);
                entity.Departamento = new DepartamentoEntity();
                entity.Departamento.IdDepartamento = int.Parse(ddlDepartamento_Detalle_Usuarios.SelectedValue);
                entity.IdEstatus = int.Parse(ddlEstatus_Detalle_Usuarios.SelectedValue);
                entity.Inicializar = (int.Parse(ddlInicializar_Detalle_Usuarios.SelectedValue) == 1) ? true : false;

                ResultadoOfUsuarioEntityrcuOa4Td res = ws.Sistema_GuardarUsuario(entity);

                if (res.Codigo > 0)
                    throw new Exception(res.Mensaje);

                entity = res.ResultObject;

                Detalle_Usuarios_Cargar(entity.IdUsuario);
            }            

            pnlExito_Detalle_Usuarios.Visible = true;
            lblExito_Detalle_Usuarios.Text = "Guardado Correctamente!";
        }
        catch (Exception ex)
        {
            pnlError_Detalle_Usuarios.Visible = true;
            lblError_Detalle_Usuarios.Text = ex.Message;
        }
    }

    protected void gvBuscar_Usuarios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "Detalle":
                    using (wsSOPF.CobranzaAdministrativaClient ws = new wsSOPF.CobranzaAdministrativaClient())
                    {
                        pnlExito_Detalle_Usuarios.Visible = false;
                        pnlError_Detalle_Usuarios.Visible = false;
                        Detalle_Usuarios_Cargar(int.Parse(e.CommandArgument.ToString()));
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            lblError_Buscar_Usuarios.Text = ex.Message;
            pnlError_Buscar_Usuarios.Visible = true;
        }
    }

    protected void gvBuscar_Usuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBuscar_Usuarios.PageIndex = e.NewPageIndex;
        Usuarios_Filtrar();
    }

    protected void lbtnCambiarClave_Click(object sender, EventArgs e)
    {
        txtClave_Detalle_Usuarios.Enabled = true;

        lbtnCambiarClave.Visible = false;
        lbtnCancelarCambiarClave.Visible = true;
    }

    protected void lbtnCancelarCambiarClave_Click(object sender, EventArgs e)
    {
        txtClave_Detalle_Usuarios.Enabled = false;
        txtClave_Detalle_Usuarios.Text = string.Empty;

        lbtnCambiarClave.Visible = true;
        lbtnCancelarCambiarClave.Visible = false;
    }

    private void Detalle_Usuarios_Cargar(int IdUsuario)
    {               
        using (wsSOPF.SistemaClient ws = new SistemaClient())
        {
            this.IdUsuarioDetalle = IdUsuario;
            hdn_idUsuario.Value = IdUsuario.ToString();

            wsSOPF.UsuarioEntity usuario = new UsuarioEntity();
            usuario.IdUsuario = this.IdUsuarioDetalle;

            wsSOPF.ResultadoOfUsuarioEntityrcuOa4Td res = ws.Sistema_ObtenerUsuario(usuario);

            if (res.Codigo > 0)
                throw new Exception(res.Mensaje);

            usuario = res.ResultObject;
            txtNombre_Detalle_Usuarios.Text = usuario.Nombre.Trim();
            txtApellidoPaterno_Detalle_Usuarios.Text = usuario.ApellidoPaterno.Trim();
            txtApellidoMaterno_Detalle_Usuarios.Text = usuario.ApellidoMaterno.Trim();
            txtUsuario_Detalle_Usuarios.Text = usuario.Usuario.Trim();
            txtCorreo_Detalle_Usuarios.Text = usuario.Email.ToString();

            ddlDepartamento_Detalle_Usuarios.SelectedValue = usuario.Departamento.IdDepartamento.ToString();
            ddlSucursal_Detalle_Usuarios.SelectedValue = usuario.Sucursal.IdSucursal.ToString();
            ddlTipoUsuario_Detalle_Usuarios.SelectedValue = usuario.TipoUsuario.IdTipoUsuario.ToString();
            ddlEstatus_Detalle_Usuarios.SelectedValue = usuario.IdEstatus.ToString();
            ddlInicializar_Detalle_Usuarios.SelectedValue = (usuario.Inicializar) ? "1" : "0";

            txtClave_Detalle_Usuarios.Text = string.Empty;
            txtClave_Detalle_Usuarios.Enabled = false;
            lbtnCambiarClave.Visible = true;
            lbtnCancelarCambiarClave.Visible = false;

            gvMenu_Detalle_Usuarios.DataSource = usuario.MenuAcceso;
            gvReportes_Detalle_Usuarios.DataSource = usuario.ReportesAcceso;

            gvMenu_Detalle_Usuarios.DataBind();
            gvReportes_Detalle_Usuarios.DataBind();            

            lblTituloDetalle_Usuarios.InnerText = "Actualizar Usuario";
            pnlReportesAcceso_Detalle_Usuarios.Visible = true;
            pnlMenuAcceso_Detalle_Usuarios.Visible = true;

            pnlBuscar_Usuarios.Visible = false;
            pnlDetalle_Usuarios.Visible = true;

            List<MenuEntity> menus = ObtenerMenuAcceso();
            List<ReporteEntity> reports = ObtenerReporteAcceso();
            List<AccionEntity> acciones = ObtenerAccionAcceso();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "fillMenuAccessList(" + JsonConvert.SerializeObject(menus) + ", " + JsonConvert.SerializeObject(usuario.MenuAcceso) + "); fillReportAccessList(" + JsonConvert.SerializeObject(reports.OrderBy(r => r.Nombre)) + ", " + JsonConvert.SerializeObject(usuario.ReportesAcceso) + "); fillActionAccessList(" + JsonConvert.SerializeObject(acciones) + ", " + JsonConvert.SerializeObject(usuario.AccionesAcceso) + ");", true);
        }
    }

    #endregion

    #region "Metodos Privados" 

    private void Usuarios_Filtrar()
    {
        wsSOPF.UsuarioEntity entity = new UsuarioEntity();

        entity.Nombre = txtNombre_Usuarios.Text.Trim();
        entity.IdEstatus = int.Parse(ddlEstatus_Usuarios.SelectedValue);

        using (wsSOPF.SistemaClient ws = new SistemaClient())
        {
            wsSOPF.ResultadoOfArrayOfUsuarioEntityrcuOa4Td res = ws.Sistema_FiltrarUsuario(entity);

            if (res.Codigo > 0)
            {
                throw new Exception(res.Mensaje);
            }

            gvBuscar_Usuarios.DataSource = res.ResultObject.ToList();
            gvBuscar_Usuarios.DataBind();
        }
    }

    private void CargarComboTipoUsuario()
    {
        using (wsSOPF.SistemaClient ws = new SistemaClient())
        {
            ddlTipoUsuario_Detalle_Usuarios.DataSource = ws.Sistema_TipoUsuario_Obtener_Activos();
            ddlTipoUsuario_Detalle_Usuarios.DataBind();
        }
    }

    private void CargarComboSucursal()
    {
        using (wsSOPF.SistemaClient ws = new SistemaClient())
        {
            ddlSucursal_Detalle_Usuarios.DataSource = ws.Sistema_Sucursales_Obtener_Activos();
            ddlSucursal_Detalle_Usuarios.DataBind();
        }
    }

    private void CargarComboDepartamento()
    {
        using (wsSOPF.SistemaClient ws = new SistemaClient())
        {
            ddlDepartamento_Detalle_Usuarios.DataSource = ws.Sistema_Departamentos_Obtener_Activos();
            ddlDepartamento_Detalle_Usuarios.DataBind();
        }
    }

    private List<MenuEntity> ObtenerMenuAcceso()
    {
        List<MenuEntity> menuOrdenados = new List<MenuEntity>();

        using (wsSOPF.SistemaClient ws = new SistemaClient())
        {
            List<MenuEntity> menuActivos = ws.Sistema_Menu_Obtener_Activos().ToList();

            foreach (MenuEntity m in menuActivos.Where(x => x.IdPadre == 0).OrderBy(x => x.Orden))
            {
                menuOrdenados.Add(m);

                menuOrdenados.AddRange(menuActivos.Where(x => x.IdPadre == m.IdMenu).OrderBy(x => x.Orden).Select(x => { x.Menu = m.Menu + " -> " + x.Menu; return x; }).ToList());
            }
        }

        return menuOrdenados;
    }

    private void CargarComboMenuAcceso()
    {
        using (wsSOPF.SistemaClient ws = new SistemaClient())
        {
            List<MenuEntity> menuActivos = ws.Sistema_Menu_Obtener_Activos().ToList();
            List<MenuEntity> menuOrdenados = new List<MenuEntity>();

            foreach (MenuEntity m in menuActivos.Where(x => x.IdPadre == 0).OrderBy(x => x.Orden))
            {
                menuOrdenados.Add(m);

                menuOrdenados.AddRange(menuActivos.Where(x => x.IdPadre == m.IdMenu).OrderBy(x => x.Orden).Select(x => { x.Menu = m.Menu + " -> " + x.Menu; return x; }).ToList());
            }


            ddlAgregarMenuAcceso_Detalle_Usuarios.DataSource = menuOrdenados;
            ddlAgregarMenuAcceso_Detalle_Usuarios.DataBind();
        }
    }

    private List<AccionEntity> ObtenerAccionAcceso()
    {
        List<AccionEntity> acciones = new List<AccionEntity>();

        using (wsSOPF.SistemaClient ws = new SistemaClient())
        {
            acciones = ws.Sistema_Accion_Obtener_Activos().ToList();
        }

        return acciones;
    }

    private List<ReporteEntity> ObtenerReporteAcceso()
    {
        List<ReporteEntity> reportes = new List<ReporteEntity>();

        using (wsSOPF.SistemaClient ws = new SistemaClient())
        {
            reportes = ws.Sistema_Reporte_Obtener_Activos().ToList();
        }

        return reportes;
    }

    private void CargarComboReporteAcceso()
    {
        using (wsSOPF.SistemaClient ws = new SistemaClient())
        {
            ddlAgregarReporteAcceso_Detalle_Usuarios.DataSource = ws.Sistema_Reporte_Obtener_Activos();
            ddlAgregarReporteAcceso_Detalle_Usuarios.DataBind();
        }
    }
  
    #endregion               
}