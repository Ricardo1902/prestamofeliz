﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="true" CodeFile="BuzonCancelacion.aspx.cs" Inherits="Site_Solicitudes_BuzonCancelacion" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%--<link href="css/buzonCancelacion.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />--%>
    <link href="css/buzonCancelacion.css" rel="stylesheet" />

    <script type="text/javascript" src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>

    <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="150" runat="server">
        <ProgressTemplate>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <asp:UpdatePanel ID="upTitulo" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <h3 class="panel-title">
                        <asp:Label ID="lblTitulo" Text="Buzón Cancelación" runat="server" />
                        <small><asp:Label ID="lblSubtitulo" Text="Notificaciones de clientes solicitando una cancelación de crédito" runat="server" /></small>
                    </h3>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="panel-body">
            <asp:UpdatePanel ID="upMain" runat="server">
                <ContentTemplate>
                    <ajaxToolkit:TabContainer ID="tcMain" OnActiveTabChanged="tcMain_ActiveTabChanged" AutoPostBack="true" CssClass="actTab" runat="server">
                        <ajaxToolkit:TabPanel ID="tpPendiente" HeaderText="Pendiente" Visible="true" runat="server">                           
                            <ContentTemplate>
                                <asp:Panel ID="pnlProcesosPendientes" Visible="True" runat="server">
                                    <div class="form-group col-sm-12">
                                        <asp:GridView ID="gvProcesosPendientes" runat="server" Width="100%" AutoGenerateColumns="False"
                                            Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                            OnPageIndexChanging="gvProcesosPendientes_PageIndexChanging"
                                            OnRowCommand="gvProcesosPendientes_RowCommand"
                                            PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            Style="overflow-x: auto;">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnAtender" CommandName="AtenderCancelacion" CommandArgument='<%# Eval("IdProceso") %>' data-toggle="tooltip" title="Atender cancelación" runat="server" OnClientClick="return confirm('¿Estas seguro de atender la cancelación? Una vez atendida, se le podrá dar seguimiento desde la sección \'En proceso\'.');">
                                                        <i class="fas fa-user-plus"></i>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnRechazar" CommandName="RechazarCancelacion" CommandArgument='<%# Eval("IdProceso") %>' data-toggle="tooltip" title="Rechazar cancelación" runat="server" OnClientClick="return confirm('¿Estas seguro de rechazar la cancelación?');">
                                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Solicitud" DataField="Solicitud" />
                                                <asp:BoundField HeaderText="Registro" DataField="Registro" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                                <asp:BoundField HeaderText="Tipo crédito" DataField="TipoCredito" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                                <asp:BoundField HeaderText="Monto Liquidar" DataField="SaldoVigente" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:c2}" />
                                                <asp:BoundField HeaderText="P. Realizados" DataField="PagosRealizados" ItemStyle-HorizontalAlign="Center" />                                                                                                
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ty warning" />
                                            <EmptyDataTemplate>
                                                No se encontraron procesos
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="tpEnProceso" HeaderText="En proceso" Visible="true" runat="server">                            
                            <ContentTemplate>
                                <asp:Panel ID="pnlProcesosEnProceso" Visible="True" runat="server">
                                    <div class="form-group col-sm-12">
                                        <asp:GridView ID="gvProcesosEnProceso" runat="server" Width="100%" AutoGenerateColumns="False"
                                            Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                            OnPageIndexChanging="gvProcesosEnProceso_PageIndexChanging"
                                            OnRowCommand="gvProcesosEnProceso_RowCommand"
                                            PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            Style="overflow-x: auto;">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnEnviarCodigo" CommandName="EnviarCodigo" CommandArgument='<%# Eval("IdProceso") %>' data-toggle="tooltip" title="Enviar código" runat="server">
                                                        <i class="fas fa-check-circle"></i>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnRechazarEP" CommandName="RechazarCancelacion" CommandArgument='<%# Eval("IdProceso") %>' data-toggle="tooltip" title="Rechazar cancelación" runat="server" OnClientClick="return confirm('¿Estas seguro de rechazar la cancelación?');">
                                                        <i class="fas fa-user-times"></i>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Solicitud" DataField="Solicitud" />
                                                <asp:BoundField HeaderText="Registro" DataField="Registro" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                                <asp:BoundField HeaderText="Tipo crédito" DataField="TipoCredito" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                                <asp:BoundField HeaderText="Monto Liquidar" DataField="SaldoVigente" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:c2}" />
                                                <asp:BoundField HeaderText="P. Realizados" DataField="PagosRealizados" ItemStyle-HorizontalAlign="Center" />                                                
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ty warning" />
                                            <EmptyDataTemplate>
                                                No se encontraron procesos
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="tpRecaudoEnviado" HeaderText="Recaudo enviado" Visible="true" runat="server">                            
                            <ContentTemplate>
                                <asp:Panel ID="pnlProcesosRecuadoEnviado" Visible="True" runat="server">
                                    <div class="form-group col-sm-12">
                                        <asp:GridView ID="gvProcesosRecaudoEnviado" runat="server" Width="100%" AutoGenerateColumns="False"
                                            Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                            OnPageIndexChanging="gvProcesosRecaudoEnviado_PageIndexChanging"
                                            OnRowCommand="gvProcesosRecaudoEnviado_RowCommand"
                                            PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            Style="overflow-x: auto;">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnConfirmarRecaudo" CommandName="ConfirmarRecaudo" CommandArgument='<%# Eval("IdProceso") %>' data-toggle="tooltip" title="Confirmar Recaudo" runat="server">
                                                        <i class="fas fa-check-circle"></i>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnRechazarRE" CommandName="RechazarCancelacion" CommandArgument='<%# Eval("IdProceso") %>' data-toggle="tooltip" title="Rechazar cancelación" runat="server" OnClientClick="return confirm('¿Estas seguro de rechazar la cancelación?');">
                                                        <i class="fas fa-user-times"></i>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Solicitud" DataField="Solicitud" />
                                                <asp:BoundField HeaderText="Registro" DataField="Registro" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                                <asp:BoundField HeaderText="Tipo crédito" DataField="TipoCredito" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                                <asp:BoundField HeaderText="Monto Liquidar" DataField="SaldoVigente" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:c2}" />
                                                <asp:BoundField HeaderText="P. Realizados" DataField="PagosRealizados" ItemStyle-HorizontalAlign="Center" />                                                
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ty warning" />
                                            <EmptyDataTemplate>
                                                No se encontraron procesos
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="tpTerminado" HeaderText="Terminado" Visible="true" runat="server">                           
                            <ContentTemplate>
                                <asp:Panel ID="pnlProcesosTerminado" Visible="True" runat="server">
                                    <div class="form-group col-sm-12">
                                        <asp:GridView ID="gvProcesosTerminado" runat="server" Width="100%" AutoGenerateColumns="False"
                                            Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                            OnPageIndexChanging="gvProcesosTerminado_PageIndexChanging"
                                            PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            Style="overflow-x: auto;">
                                            <Columns>
                                                <asp:BoundField HeaderText="Solicitud" DataField="Solicitud" />
                                                <asp:BoundField HeaderText="Registro" DataField="Registro" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                                <asp:BoundField HeaderText="Tipo crédito" DataField="TipoCredito" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                                <asp:BoundField HeaderText="Monto Liquidar" DataField="SaldoVigente" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:c2}" />
                                                <asp:BoundField HeaderText="P. Realizados" DataField="PagosRealizados" ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ty warning" />
                                            <EmptyDataTemplate>
                                                No se encontraron procesos
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="tpRechazado" HeaderText="Rechazado" Visible="true" runat="server">                            
                            <ContentTemplate>
                                <asp:Panel ID="pnlProcesosRechazado" Visible="True" runat="server">
                                    <div class="form-group col-sm-12">
                                        <asp:GridView ID="gvProcesosRechazado" runat="server" Width="100%" AutoGenerateColumns="False"
                                            Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                            CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                            OnPageIndexChanging="gvProcesosRechazado_PageIndexChanging"
                                            PageSize="10" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                            Style="overflow-x: auto;">
                                            <Columns>
                                                <asp:BoundField HeaderText="Solicitud" DataField="Solicitud" />
                                                <asp:BoundField HeaderText="Registro" DataField="Registro" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                                <asp:BoundField HeaderText="Tipo crédito" DataField="TipoCredito" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                                <asp:BoundField HeaderText="Monto Liquidar" DataField="SaldoVigente" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:c2}" />
                                                <asp:BoundField HeaderText="P. Realizados" DataField="PagosRealizados" ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ty warning" />
                                            <EmptyDataTemplate>
                                                No se encontraron procesos
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                <asp:HiddenField ID="hfCodigo" value="0" runat="server" />
                <asp:HiddenField ID="hfFechaLimitePago" value="" runat="server" />
                    <asp:HiddenField ID="hfComentario" value="" runat="server" />
                    <div id="modalEnviarCodigo" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-dialog-centered" role="alertdialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Enviar Codigo</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <asp:Label ID="lbtxCodigo" AssociatedControlID="txCodigo" runat="server" Text="Código" />
                                                <asp:TextBox ID="txCodigo" name="txCodigo" runat="server" CssClass="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbtxFechaLimitePago" AssociatedControlID="txFechaLimitePago" runat="server" Text="Fecha Límite de Pago (dd/MM/yyyy):" />
                                                <asp:TextBox ID="txFechaLimitePago" runat="server" CssClass="form-control" placeholder="(dd/MM/yyyy)" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <asp:LinkButton ID="btEnviarCodigo" runat="server" CssClass="btn btn-success" Text="Enviar" OnClientClick="acoplarValoresEnviarCodigo();" UseSubmitBehavior="false" OnClick="EnviarCodigo" />
                                    <asp:Button ID="btCancelarEnviarCodigo" runat="server" CssClass="btn btn-danger" Text="Cancelar" data-dismiss="modal" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="modalConfirmarRecaudo" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-dialog-centered" role="alertdialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">¿Estas seguro de confirmar el recaudo?</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <asp:Label ID="lbtxComentario" AssociatedControlID="txComentario" runat="server" Text="Una vez confirmado el recaudo ya no sera posible revertir la acción. Favor de proporcionar un comentario para continuar." />
                                                <asp:TextBox ID="txComentario" name="txComentario" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" placeholder="Comentarios" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <asp:LinkButton ID="btConfirmarRecaudo" runat="server" CssClass="btn btn-success" Text="Confirmar" OnClientClick="acoplarValoresConfirmarRecaudo();" UseSubmitBehavior="false" OnClick="ConfirmarRecaudo" />
                                    <asp:Button ID="btCancelarConfirmarRecaudo" runat="server" CssClass="btn btn-danger" Text="Cancelar" data-dismiss="modal" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="100" runat="server">
            <ProgressTemplate>
                  <div class="animationload">
                    <div class="osahanloading"></div>                    
                </div>              
            </ProgressTemplate>
        </asp:UpdateProgress>
        </div>
    </div>
    <div class="modal-pr"></div>
    <%--<script type="text/javascript" src="js/buzonCancelacion.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>--%>
    <script type="text/javascript" src="js/buzonCancelacion.js"></script>
</asp:Content>
