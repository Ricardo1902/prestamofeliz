﻿using System;
using System.Linq;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Site_Solicitudes_BuzonCancelacion : System.Web.UI.Page
{
    private enum Vista
    {
        Pendiente,
        EnProceso,
        RecaudoEnviado,
        Terminado,
        Rechazado
    }

    private Vista VistaPagina
    {
        get { return (ViewState[UniqueID + "_vistaPagina"] != null) ? (Vista)ViewState[UniqueID + "_vistaPagina"] : Vista.Pendiente; }
        set
        {
            ViewState[UniqueID + "_vistaPagina"] = value;
            CargarBuzonCancelacion();
        }
    }

    private int IdProceso
    {
        get { return ViewState[UniqueID + "_IdProceso"] != null ? (int)ViewState[UniqueID + "_IdProceso"] : 0; }
        set { ViewState[UniqueID + "_IdProceso"] = value; }
    }

    private static string FCM_ServerKey = System.Configuration.ConfigurationManager.AppSettings["FCM_ServerKey"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            VistaPagina = Vista.Pendiente;
        }
    }      

    private void CargarBuzonCancelacion()
    {
        string estatus = string.Empty;
        int idUsuario = 0, idEstatus = 0;
        GridView gvActual = new GridView();
        switch (VistaPagina)
        {
            case Vista.Pendiente:
                estatus = "Pendiente";
                gvActual = gvProcesosPendientes;
                break;
            case Vista.EnProceso:
                estatus = "En proceso";
                gvActual = gvProcesosEnProceso;
                idUsuario = int.Parse(Session["UsuarioId"].ToString());
                break;
            case Vista.RecaudoEnviado:
                estatus = "Recaudo enviado";
                gvActual = gvProcesosRecaudoEnviado;
                idUsuario = int.Parse(Session["UsuarioId"].ToString());
                break;
            case Vista.Terminado:
                estatus = "Terminado";
                gvActual = gvProcesosTerminado;
                break;
            case Vista.Rechazado:
                gvActual = gvProcesosRechazado;
                estatus = "Rechazado";
                break;
            default:
                gvActual = gvProcesosPendientes;
                estatus = "Pendiente";
                break;
        }
        using (SolicitudClient ws = new SolicitudClient())
        {
            ObtenerEstatusBuzonCancelacionResponse resE = ws.ObtenerEstatusBuzonCancelacion(new ObtenerEstatusBuzonCancelacionRequest()
            {
                Estatus = estatus
            });

            if (!resE.Error && resE.IdEstatus > 0)
            {
                idEstatus = resE.IdEstatus;
            }
            else
            {
                switch (VistaPagina)
                {
                    case Vista.Pendiente:
                        idEstatus = 1;
                        break;
                    case Vista.EnProceso:
                        idEstatus = 2;
                        break;
                    case Vista.RecaudoEnviado:
                        idEstatus = 3;
                        break;
                    case Vista.Terminado:
                        idEstatus = 4;
                        break;
                    case Vista.Rechazado:
                        idEstatus = 5;
                        break;
                    default: //pendiente
                        idEstatus = 1;
                        break;
                }
            }

            CargarBuzonCancelacionResponse res = ws.CargarBuzonCancelacion(new CargarBuzonCancelacionRequest()
            {
                IdEstatus = idEstatus,
                IdUsuario = idUsuario
            });
            

            if(!res.Error)
            {
                gvActual.DataSource = res.Procesos;
                gvActual.DataBind();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "MyModal", "alert('Ocurrio un error. " + res.MensajeOperacion + "');", true);
            }
        }
    }

    private string AtenderCancelacion(int idProceso)
    {
        using (SolicitudClient ws = new SolicitudClient())
        {
            AtenderBuzonCancelacionResponse res = ws.AtenderBuzonCancelacion(new AtenderBuzonCancelacionRequest()
            {
                IdProceso = idProceso,
                IdUsuario = int.Parse(Session["UsuarioId"].ToString())
            });

            return res.MensajeOperacion;
        }
    }

    private string RechazarCancelacion(int idProceso)
    {
        using (SolicitudClient ws = new SolicitudClient())
        {
            RechazarBuzonCancelacionResponse res = ws.RechazarBuzonCancelacion(new RechazarBuzonCancelacionRequest()
            {
                IdProceso = idProceso,
                IdUsuario = int.Parse(Session["UsuarioId"].ToString())
            });

            return res.MensajeOperacion;
        }
    }

    private string EnviarCodigoCancelacion(string codigo, DateTime fechaLimite)
    {
        using (SolicitudClient ws = new SolicitudClient())
        {
            EnviarCodigoCancelacionResponse res = ws.EnviarCodigoCancelacion(new EnviarCodigoCancelacionRequest()
            {
                IdUsuario = int.Parse(Session["UsuarioId"].ToString()),
                IdProceso = IdProceso,
                Codigo = codigo,
                FechaLimitePago = fechaLimite
            });

            return res.MensajeOperacion;
        }
    }

    protected void EnviarCodigo(object sender, EventArgs e)
    {
        try
        {
            DateTime? fechaLimite = null;
            fechaLimite = ValidarFechaHora(hfFechaLimitePago.Value, string.Empty);

            if (IdProceso < 0)
            {
                throw new Exception("No se ha seleccionado proceso, reportar a sistemas.");
            }
            else if (fechaLimite == null)
            {
                throw new Exception("El formato de la fecha es incorrecto.");
            }
            else if (!ValidarCodigo(hfCodigo.Value))
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "MyModal", "alert('Codigo de recaudo no valido.');", true);
            }
            else
            {
                // Validaciones correctas
                int dif = (fechaLimite.Value.Date - DateTime.Now.Date).Days;
                if (dif < 0 || dif > 1)
                {
                    throw new Exception("La fecha no debe de ser menor a la actual ni mayor a 1 día máximo.");
                }

                string resultado = string.Empty;
                resultado = EnviarCodigoCancelacion(hfCodigo.Value, fechaLimite.Value);
                CargarBuzonCancelacion();

                ScriptManager.RegisterClientScriptBlock(upMain, typeof(UpdatePanel), upMain.ClientID, "alert('" + resultado + "');", true);
            }                      
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(upMain, typeof(UpdatePanel), upMain.ClientID, "alert('" + ex.Message + "');", true);
        }
    }
    
    protected void ConfirmarRecaudo(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(hfComentario.Value.Trim()))
                throw new Exception("El comentario no puede ir vacío");

            using (SolicitudClient ws = new SolicitudClient())
            {
                ConfirmarRecaudoCancelacionResponse res = ws.ConfirmarRecaudoCancelacion(new ConfirmarRecaudoCancelacionRequest()
                {
                    IdUsuario = int.Parse(Session["UsuarioId"].ToString()),
                    IdProceso = IdProceso,
                    Comentario = hfComentario.Value.Trim()
                });

                if (res.Error)
                    throw new Exception(res.MensajeOperacion + " | " + res.MensajeErrorException);

                // Notificacion Push al cliente
                PushNotification("Credito Cancelado", "¡Has terminado de pagar tu crédito!", res.token);

                CargarBuzonCancelacion();
                ScriptManager.RegisterClientScriptBlock(upMain, typeof(UpdatePanel), upMain.ClientID, "alert('" + res.MensajeOperacion + "');", true);
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(upMain, typeof(UpdatePanel), upMain.ClientID, "alert('Error: " + ex.Message + "');", true);
        }
    }

    protected void tcMain_ActiveTabChanged(object sender, EventArgs e)
    {
        switch (tcMain.ActiveTab.ID)
        {
            case "tpPendiente":
                VistaPagina = Vista.Pendiente;
                break;
            case "tpEnProceso":
                VistaPagina = Vista.EnProceso;
                break;
            case "tpRecaudoEnviado":
                VistaPagina = Vista.RecaudoEnviado;
                break;
            case "tpTerminado":
                VistaPagina = Vista.Terminado;
                break;
            case "tpRechazado":
                VistaPagina = Vista.Rechazado;
                break;
            default:
                VistaPagina = Vista.Pendiente;
                break;
        }
    }

    protected void gvProcesosPendientes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProcesosPendientes.PageIndex = e.NewPageIndex;
        CargarBuzonCancelacion();
    }

    protected void gvProcesosPendientes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string resultado = string.Empty;
            switch (e.CommandName)
            {
                case "AtenderCancelacion":
                    resultado = AtenderCancelacion(int.Parse(e.CommandArgument.ToString()));
                    break;
                case "RechazarCancelacion":
                    resultado = RechazarCancelacion(int.Parse(e.CommandArgument.ToString()));
                    break;
                default:
                    break;
            }
            CargarBuzonCancelacion();
            ScriptManager.RegisterClientScriptBlock(upMain, typeof(UpdatePanel), upMain.ClientID, "alert('" + resultado + "');", true);
        }
        catch (Exception)
        {
            ScriptManager.RegisterClientScriptBlock(upMain, typeof(UpdatePanel), upMain.ClientID, "alert('Ha ocurrido un error.');", true);
        }
    }

    protected void gvProcesosEnProceso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProcesosEnProceso.PageIndex = e.NewPageIndex;
        CargarBuzonCancelacion();
    }

    protected void gvProcesosEnProceso_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string resultado = string.Empty;
            switch (e.CommandName)
            {                
                case "EnviarCodigo":
                    IdProceso = int.Parse(e.CommandArgument.ToString());
                    GridViewRow gvr = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                    int RowIndex = gvr.RowIndex;                    
                    txCodigo.Text = gvProcesosEnProceso.Rows[RowIndex].Cells[ControlsHelper.GetColumnIndexByName(gvProcesosEnProceso.Rows[RowIndex], "Solicitud")].Text;
                    txFechaLimitePago.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("$('#modalEnviarCodigo').appendTo('body').modal('show');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "MyModal", sb.ToString(), false);
                    return;
                case "RechazarCancelacion":
                    resultado = RechazarCancelacion(int.Parse(e.CommandArgument.ToString()));
                    break;
                default:
                    break;
            }
            CargarBuzonCancelacion();
            ScriptManager.RegisterClientScriptBlock(upMain, typeof(UpdatePanel), upMain.ClientID, "alert('" + resultado + "');", true);
        }
        catch (Exception eo)
        {
            ScriptManager.RegisterClientScriptBlock(upMain, typeof(UpdatePanel), upMain.ClientID, "alert('Ha ocurrido un error.');", true);
        }
    }

    protected void gvProcesosRecaudoEnviado_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProcesosRecaudoEnviado.PageIndex = e.NewPageIndex;
        CargarBuzonCancelacion();
    }

    protected void gvProcesosRecaudoEnviado_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string resultado = string.Empty;
            switch (e.CommandName)
            {
                case "ConfirmarRecaudo":
                    IdProceso = int.Parse(e.CommandArgument.ToString());
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("if(!$('#modalConfirmarRecaudo').next().length) { $('#modalConfirmarRecaudo').appendTo('body'); } $('#modalConfirmarRecaudo').modal('show');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "MyModal", sb.ToString(), false);
                    break;
                case "RechazarCancelacion":
                    resultado = RechazarCancelacion(int.Parse(e.CommandArgument.ToString()));
                    ScriptManager.RegisterClientScriptBlock(upMain, typeof(UpdatePanel), upMain.ClientID, "alert('" + resultado + "');", true);
                    break;
                default:
                    break;
            }

            CargarBuzonCancelacion();
        }
        catch (Exception)
        {
            ScriptManager.RegisterClientScriptBlock(upMain, typeof(UpdatePanel), upMain.ClientID, "alert('Ha ocurrido un error.');", true);
        }
    }

    protected void gvProcesosTerminado_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProcesosTerminado.PageIndex = e.NewPageIndex;
        CargarBuzonCancelacion();
    }

    protected void gvProcesosRechazado_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProcesosRechazado.PageIndex = e.NewPageIndex;
        CargarBuzonCancelacion();
    }

    #region Métodos Privados
    private bool ValidarCodigo(string codigo)
    {
        try
        {
            return !string.IsNullOrEmpty(codigo) ? Convert.ToInt32(codigo) > 0 : false;
        }
        catch
        {
            return false;
        }
    }

    private static DateTime? ValidarFechaHora(string fecha, string hora)
    {
        if (string.IsNullOrEmpty(fecha)) return null;
        hora = string.IsNullOrEmpty(hora) ? "00:00" : hora;
        Regex rg = new Regex(@"^(?:[01][0-9]|2[0-3]):[0-5][0-9]$");
        hora = hora.Substring(0, 5);
        if (rg.IsMatch(hora))
        {
            string[] formats = { "dd/MM/yyyy HH:mm" };
            DateTime FechaHoraFinal;
            string fechaHora = string.Format("{0} {1}", fecha, hora);
            if (DateTime.TryParseExact(fechaHora, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaHoraFinal))
            {
                return FechaHoraFinal;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
    #endregion

    #region Privado Externos
    private static string PushNotification(string title, string msg, string fcmToken)
    {
        var serverKey = FCM_ServerKey;

        var result = "-1";

        var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
        httpWebRequest.Method = "POST";


        var payload = new
        {
            notification = new
            {
                title = title,
                body = msg,
                sound = "default"
            },
            data = new
            {
                //key = "valor"
            },
            to = fcmToken,
            priority = "high",
        };


        var serializer = new JavaScriptSerializer();

        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
            string json = serializer.Serialize(payload);
            streamWriter.Write(json);
            streamWriter.Flush();
        }

        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        {
            result = streamReader.ReadToEnd();
        }

        return result;
    }
    #endregion    
}
