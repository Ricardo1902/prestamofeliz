﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReimpresionFormularios.aspx.cs" Inherits="Site_Solicitudes_ReimpresionFormularios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript" src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <style>
        .checkbox.checkboxlist input[type="checkbox"]
        {
            position: absolute;
            margin-top: 0.3rem;
            margin-left: -1.25rem;
        }

        .checkbox.checkboxlist label
        {
            margin-bottom: 0;
            display: inline-block;
        }

        fieldset legend
        {
            font-size: inherit;
        }

        fieldset .checkbox
        {
            position: relative;
            display: block;
            padding-left: 1.25rem;
        }
    </style>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Reimpresión por Folio</h3>
        </div>
        <div class="panel-body">
            <asp:UpdatePanel ID="upMain" runat="server">
                <ContentTemplate>
                    <div class="izq">
                        <div class="row">
                            <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                <fieldset>
                                    <legend>Seleccione Formulario</legend>
                                    <div class="checkbox checkboxlist">
                                        <asp:CheckBoxList ID="cblFormatos" RepeatDirection="Vertical" RepeatLayout="Flow" runat="server" />
                                    </div>
                                </fieldset>
                            </div>
                            <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                <label class="form-control-label small">Número de Folio</label>
                                <asp:TextBox ID="txFolio" CssClass="form-control" runat="server" onKeyPress="return EvaluateText('%d', this);" />

                                <br /><br />
                                <div class="form-group row col-sm-12">
                                    <asp:Button ID="btnImprimir" CssClass="btn btn-sm btn-info" runat="server" Text="Imprimir" OnClick="btnImprimir_Click" />
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <asp:Panel ID="pnlError" CssClass="alert alert-danger small" Style="padding: 0px 15px;" runat="server" Visible="false">
                                        <asp:Label ID="lblError" runat="server" Text="" />
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnImprimir" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
