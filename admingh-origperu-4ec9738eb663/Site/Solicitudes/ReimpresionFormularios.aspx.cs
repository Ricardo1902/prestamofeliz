﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Web.Hosting;
using wsSOPF;

public partial class Site_Solicitudes_ReimpresionFormularios : System.Web.UI.Page
{
    private List<FormatoPaginaVM> formatos
    {
        get
        {
            if (ViewState[UniqueID + "_formatos"] != null)
            {
                return (List<FormatoPaginaVM>)ViewState[UniqueID + "_formatos"];
            }
            else
            {
                List<FormatoPaginaVM> formatos = new List<FormatoPaginaVM>();
                return formatos;
            }
        }
        set
        {
            ViewState[UniqueID + "_formatos"] = value;
        }
    }

    private const int posXMax = 612;
    private const int posYMax = 1008;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = "";
        pnlError.Visible = false;

        if (!IsPostBack)
        {
            using (SolicitudClient ws = new SolicitudClient())
            {
                ObtenerPaginasFormatosResponse res = ws.ObtenerPaginasFormatos();
                if(!res.Error && res.formatos.Length > 0)
                {
                    foreach(FormatoPaginaVM item in res.formatos)
                    {
                        cblFormatos.Items.Add(new System.Web.UI.WebControls.ListItem(
                            string.Concat((item.NombrePlantilla == "Ampliacion_Peru" ? "Amp - " : ""), item.NombrePagina)
                            , item.IdFormatoPagina.ToString()));
                    }
                    formatos = new List<FormatoPaginaVM>(res.formatos);
                }
            }
        }
    }

    protected void btnImprimir_Click(object sender, EventArgs e)
    {
        try
        {
            #region Validaciones
            string folioStr = txFolio.Text;
            int folio = 0;
            if (string.IsNullOrEmpty(folioStr))
                throw new Exception("el número de folio no puede ir vacío.");
            if (!int.TryParse(folioStr, out folio) || folio <= 0)
                throw new Exception("el número de folio no puede ir vacío ni con valor 0."); 
            if(cblFormatos.SelectedItem == null)
                throw new Exception("debe seleccionar al menos 1 formato a imprimir.");
            #endregion

            ImprimirPaginas(folioStr);
        }
        catch (Exception ex)
        {
            lblError.Text = "ERROR: " + ex.Message;
            pnlError.Visible = true;
        }
    }

    #region Métodos Privados
    private void ImprimirPaginas(string folio)
    {
        try
        {
            List<int> formatosSeleccionados = new List<int>();
            foreach (System.Web.UI.WebControls.ListItem item in cblFormatos.Items)
                if (item.Selected)
                    formatosSeleccionados.Add(Convert.ToInt32(item.Value));
            bool multiple = formatosSeleccionados.Count > 1;
            int idSucursal = 0;
            if (Session["cve_sucurs"] != null)
                int.TryParse(Session["cve_sucurs"].ToString(), out idSucursal);
            if (idSucursal <= 0)
                throw new Exception("no se pudo recuperar el ID de sucursal o no es válido.");
            TBCATSucursal Suc = null;
            TBCATSucursal[] Sucursal = null;
            using (wsSOPF.UsuarioClient wsSuc = new UsuarioClient())
            {
                Suc = new TBCATSucursal()
                {
                    Accion = 1,
                    IdSucursal = idSucursal,
                    Sucursal = "",
                    IdEstatus = 0,
                    IdAfiliado = 0
                };
                Sucursal = wsSuc.ObtenerSucursal(Suc);
            }

            string cveSucurs;
            cveSucurs = Sucursal[0].cve_sucursal;

            Response.Clear();
            Response.ContentType = string.Concat("application/", (multiple ? "zip" : "pdf"));

            if (multiple)
                Response.AddHeader("Content-Disposition", string.Concat("attachment; filename=Formularios_", folio) + ".zip");

            List<byte[]> archivos = new List<byte[]>();

            foreach (int idPaginaFormato in formatosSeleccionados)
            {
                FormatoPaginaVM formato = new FormatoPaginaVM();
                formato = formatos.Find(f => f.IdFormatoPagina == Convert.ToInt32(idPaginaFormato));
                if (formato.IdFormatoPagina <= 0)
                    throw new Exception("no se encontró el formato a imprimir en memoria.");

                string Plantilla, Descarga;
                Plantilla = string.Concat("../Creditos/PlantillasPDF/", formato.NombrePlantilla, ".pdf");
                Descarga = string.Concat(formato.NombreDescarga.Trim(), "_", formato.NombrePagina.Trim());

                byte[] outputs = null;
                
                using (MemoryStream ms = new MemoryStream())
                {
                    using (Document document = new Document())
                    {
                        PdfCopy copy;
                        copy = new PdfCopy(document, multiple ? ms : Response.OutputStream);

                        document.Open();
                        outputs = GenerarPDF(Server.MapPath(Plantilla), string.Concat(cveSucurs, " N°", folio), formato.PaginaInicial, formato.PaginaFinal);
                        PdfReader reader = new PdfReader(outputs);

                        for (int pageCounter = 1; pageCounter < reader.NumberOfPages + 1; pageCounter++)
                        {
                            copy.AddPage(copy.GetImportedPage(reader, pageCounter));
                        }

                        reader.Close();
                        document.Close();
                        var allPagesContent = ms.GetBuffer();
                        ms.Flush();

                        if (multiple)
                        {
                            archivos.Add(allPagesContent);
                        }
                        else
                        {
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + Descarga + ".pdf");
                        }
                    }
                }
            }

            if (multiple)
            {
                using (var compressedFileStream = new MemoryStream())
                {
                    //Create an archive and store the stream in memory.
                    using (var zipArchive = new ZipArchive(compressedFileStream, ZipArchiveMode.Update, false))
                    {
                        foreach (int idPaginaFormato in formatosSeleccionados)
                        {
                            FormatoPaginaVM formato = new FormatoPaginaVM();
                            formato = formatos.Find(f => f.IdFormatoPagina == Convert.ToInt32(idPaginaFormato));
                            if (formato.IdFormatoPagina <= 0)
                                throw new Exception("no se encontró el formato a imprimir en memoria.");

                            var index = formatosSeleccionados.FindIndex(i => i == idPaginaFormato);

                            byte[] archivoSel = archivos[index];

                            var entry = zipArchive.CreateEntry(string.Concat(formato.NombrePlantilla, "_", formato.NombrePagina, ".pdf"), CompressionLevel.Fastest);

                            //Get the stream of the attachment
                            using (var originalFileStream = new MemoryStream(archivoSel))
                            {
                                using (var zipEntryStream = entry.Open())
                                {
                                    //Copy the attachment stream to the zip entry stream
                                    originalFileStream.CopyTo(zipEntryStream);
                                }
                            }
                        }
                    }
                    sendOutZIP(compressedFileStream.ToArray());
                }
            }

            lblError.Text = "";
            pnlError.Visible = false;
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private void sendOutZIP(byte[] zippedFiles)
    {
        Response.Charset = string.Empty;
        Response.Cache.SetCacheability(System.Web.HttpCacheability.Public);
        Response.BinaryWrite(zippedFiles);
        Response.OutputStream.Flush();
        Response.OutputStream.Close();
        Response.End();
    }

    private static byte[] GenerarPDF(string pdfPath, string folio, int paginaInicial, int paginaFinal)
    {
        BaseFont arial = BaseFont.CreateFont(HostingEnvironment.ApplicationPhysicalPath + "fonts\\arial.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);

        using (var reader = new PdfReader(pdfPath))
        {
            Rectangle tamanioOrientacion = reader.GetPageSizeWithRotation(1);
            int posMaxY = (int)tamanioOrientacion.Height;
            using (MemoryStream ms = new MemoryStream())
            {
                var document = new Document(reader.GetPageSizeWithRotation(1));
                var writer = PdfWriter.GetInstance(document, ms);

                document.Open();

                for (var i = paginaInicial; i <= paginaFinal; i++)
                {
                    document.NewPage();

                    var importedPage = writer.GetImportedPage(reader, i);

                    var contentByte = writer.DirectContent;
                    contentByte.BeginText();
                    EscribirTexto(contentByte, folio, 575, posMaxY - 15, 8, arial, alineacion: PdfContentByte.ALIGN_RIGHT);

                    /* CODIGO PARA DIBUJAR LA CUADRICULA, NO BORRAR */
                    //for (int ai = 0; ai < posYMax; ai += 5)
                    //{
                    //    EscribirTexto(contentByte, PosY(ai).ToString(), 2, ai, 4, arial, 0);
                    //    for (int j = 10; j < posXMax; j += 10)
                    //    {
                    //        EscribirTexto2(contentByte, j.ToString(), j, PosY(ai), 5, arial, 0);
                    //    }
                    //}

                    contentByte.EndText();
                    contentByte.AddTemplate(importedPage, 0, 0);
                }

                document.Close();
                writer.Close();
                return ms.ToArray();
            }
        }
    }

    private static int PosY(int posY)
    {
        return posYMax - posY;
    }

    private static void EscribirTexto2(PdfContentByte contenido, string texto, int x, int y, int tamanioLetra, BaseFont fuente, int alineacion = PdfContentByte.ALIGN_LEFT)
    {
        if (string.IsNullOrEmpty(texto)) texto = "";
        contenido.SetFontAndSize(fuente, tamanioLetra);
        contenido.SetRGBColorFill(200, 100, 200);
        contenido.ShowTextAligned(alineacion, texto, x, y, 0);
    }

    private static void EscribirTexto(PdfContentByte contenido, string texto, int x, int y, int tamanioLetra, BaseFont fuente, int alineacion = PdfContentByte.ALIGN_LEFT)
    {
        if (string.IsNullOrEmpty(texto)) texto = "";
        contenido.SetFontAndSize(fuente, tamanioLetra);
        contenido.SetRGBColorFill(255, 0, 0); //rojo
        contenido.ShowTextAligned(alineacion, texto, x, y, 0);
        contenido.SetRGBColorFill(0, 0, 0); //negro se regresa a negro en caso de existir objetos fill en pdf base
    }
    #endregion
}
