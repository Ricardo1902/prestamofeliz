﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="frmCampanias.aspx.cs" Inherits="Site_Solicitudes_frmCampanias" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="h2">
        CAMPAÑAS
    </div>
    
        

        <div class="form-group row">
            <br />
            <label for="cmbCampania" class="col-sm-2 form-control-label">
                Campaña</label>
            <div class="col-sm-10">
                <asp:DropDownList ID="cmbCampania" runat="server" CssClass="form-control">
                </asp:DropDownList>

            </div>

        </div>

        <div class="panel-body" id="TipoCliente">
            <div class="h3">
                CLIENTE RECOMENDADOR
            </div>
            <asp:RadioButtonList ID="rdTipoCliente" runat="server"
                AutoPostBack="true" OnSelectedIndexChanged="rdTipoCliente_SelectedIndexChange">
                <asp:ListItem Value="1">Existente</asp:ListItem>
                <asp:ListItem Value="2">Nuevo</asp:ListItem>
            </asp:RadioButtonList>
        </div>

        <div class="panel-body" id="Buscar" runat="server">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Busqueda de Cliente</h3>
                </div>
                <div class="panel-body" style="height: 300px">
                    <asp:Label ID="Label1" runat="server" Text="Cliente"></asp:Label>
                    <asp:TextBox ID="txtCliente" runat="server"></asp:TextBox>
                    <asp:Button ID="btnBuscarCliente" runat="server" Text="Buscar" CssClass="btn-info"
                        OnClick="btnBuscarCliente_Click" />
                    <asp:HiddenField ID="hdnIdCliente" runat="server" />

                    <div id="Clientes">
                        <div class="GridEncabezado">
                        </div>
                        <div class="GridContenedor" style="height: 320px">
                            <asp:GridView ID="gvClientes" runat="server" Width="100%" AutoGenerateColumns="False"
                                Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                CssClass="table table-bordered table-striped  table-hover table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                DataKeyNames="clave" Style="overflow-x: auto;"
                                OnRowDataBound="gvClientes_RowDataBound">
                                <PagerStyle CssClass="pagination-ys" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Clave">
                                        <ItemTemplate>
                                            <div class="ObtieneIdCliente"><%# Eval("clave") %></div>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                    <asp:BoundField HeaderText="Nombres" DataField="Nombres" />
                                    <asp:BoundField HeaderText="Ap. Paterno" DataField="apellidop" />
                                    <asp:BoundField HeaderText="Ap. Materno" DataField="apellidom" />
                                    <asp:BoundField HeaderText="Fecha Nac." DataField="fch_Nacimiento" />
                                    <asp:BoundField HeaderText="Direccion" DataField="direccion" />
                                    <asp:BoundField HeaderText="Colonia" DataField="colonia" />
                                    <asp:BoundField HeaderText="Ciudad" DataField="ciudad" />
                                    <asp:BoundField HeaderText="Estado" DataField="estado" />
                                    <asp:BoundField HeaderText="CP" DataField="cp" />
                                    <asp:BoundField HeaderText="Id Cliente" DataField="IdCliente" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnModificar" runat="server" Text="Modificar" CssClass="btn-primary" OnClick="btnModificar_Click" />
                </div>
            </div>
        </div>

        <div class="panel panel-primary" id="DatosClienteExiste" runat="server" style="display: block">
            <asp:Label ID="lblIdCliente" runat="server"></asp:Label>
            <asp:Label ID="lblCliente" runat="server"></asp:Label>

            <div class="panel-heading">
                <h3 class="panel-title">DATOS BANCARIOS</h3>
            </div>
            <div class="panel-body">
                <div class="form-group row">
                    <label for="cboBanco2" class="col-sm-2 form-control-label">
                        Banco</label>
                    <div class="col-sm-10">
                        <asp:DropDownList ID="cboBanco2" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="txtClabe2" class="col-sm-2 form-control-label">
                        CLABE</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtClabe2" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardarClick" CssClass="btn-primary" />
            </div>

        </div>






        <div class="panel panel-primary" id="DatosPrincipales" runat="server" style="display: block">
            <div class="panel-heading">
                <h3 class="panel-title">Datos de Recomendador</h3>
            </div>
            <div class="panel-body">
                <div class="izq">
                    <div class="form-group">
                        <div class="form-group row">
                            <label for="txtNombre" class="col-sm-2 form-control-label ">
                                Nombre(s)</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="txtNombre" ErrorMessage="Favor de introducir nombre del cliente" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtApePat" class="col-sm-2 form-control-label">
                                Apellido Paterno</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtApePat" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtApePat" ErrorMessage="Favor de introducir Apellido Paterno del cliente" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtApeMat" class="col-sm-2 form-control-label">
                                Apellido Materno</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtApeMat" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtApeMat" ErrorMessage="Favor de introducir Apellido Materno del cliente" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rdSexo" class="col-sm-2 form-control-label">
                                Sexo</label>
                            <div class="col-sm-10">
                                <asp:RadioButtonList ID="rdSexo" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="8">Femenino</asp:ListItem>
                                    <asp:ListItem Value="7">Masculino</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="FecNac" class="col-sm-2 form-control-label">
                                Fecha Nacimiento:</label>
                            <div id="FecNac" class="form-grpup">
                                <div class="container">
                                    <div class="row">
                                        <div class='col-sm-6'>
                                            <div class="input-group date">
                                                <asp:TextBox runat="server" class="inputDate" ID="inputDate" />
                                                <label id="closeOnSelect">
                                                    <input type="checkbox" />
                                                    Close on selection</label>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $('#inputDate').DatePicker({
                                                format: 'd/m/Y',
                                                date: $('#inputDate').val(),
                                                current: $('#inputDate').val(),
                                                starts: 1,
                                                position: 'r',
                                                onBeforeShow: function () {
                                                    $('#inputDate').DatePickerSetDate($('#inputDate').val(), true);
                                                },
                                                onChange: function (formated, dates) {
                                                    $('#inputDate').val(formated);
                                                    $('#inputDate').DatePickerHide();

                                                }
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtEdad" class="col-sm-2 form-control-label">
                                Edad</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtEdad" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboEstadoNac" class="col-sm-2 form-control-label">
                                Estado Nacimiento</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboEstadoNac" runat="server" CssClass="form-control">
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboPais" class="col-sm-2 form-control-label">
                                País Nacimiento</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboPais" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtOcupacion" class="col-sm-2 form-control-label">
                                Ocupación</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtOcupacion" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtOcupacion" ErrorMessage="Favor de introducir Ocupacion del cliente" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboTipoPen" class="col-sm-2 form-control-label">
                                Tipo Pensión</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboTipoPen" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="cboTipoPen" ErrorMessage="Favor de introducir Tipo Pension del cliente" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtCURP" class="col-sm-2 form-control-label">
                                CURP</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtCURP" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtCURP" ErrorMessage="Favor de introducir CURP del cliente" ForeColor="Red" />

                                <%--  <asp:RegularExpressionValidator ID="regexpName" runat="server"
                                    ErrorMessage="El Formato del CURP no es correcto."
                                    ControlToValidate="txtCURP"
                                    ForeColor ="Red"
                                    ValidationExpression="^[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$" />
                                --%>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtRFC" class="col-sm-2 form-control-label">
                                RFC</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtRFC" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtRFC" ErrorMessage="Favor de introducir RFC del cliente" ForeColor="Red" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                    ErrorMessage="El Formato del RFC no es correcto."
                                    ControlToValidate="txtRFC"
                                    ForeColor="Red"
                                    ValidationExpression="^[A-Za-z]{4}\d{6}?$" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtFirEle" class="col-sm-2 form-control-label">
                                Firma Electrónica Avanzada</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtFirEle" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtemail" class="col-sm-2 form-control-label">
                                E-Mail</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="der">
                    <div class="form-group">
                        <div class="form-group row">
                            <label for="txtCalle" class="col-sm-2 form-control-label">
                                Calle</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtCalle" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtCalle" ErrorMessage="Favor de introducir la Calle del cliente" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtNumInt" class="col-sm-2 form-control-label">
                                Num Interior</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNumInt" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtNumExt" class="col-sm-2 form-control-label">
                                Num Exterior</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNumExt" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtNumExt" ErrorMessage="Favor de introducir Numero Exterior del cliente" ForeColor="Red" />
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <div class="form-group row">
                                    <label for="cboEstado" class="col-sm-2 form-control-label">
                                        Estado
                                    </label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList ID="cboEstado" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="cboEstado_SelectedIndexCahnge">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group row">
                                            <label for="cboMunicipio" class="col-sm-2 form-control-label">
                                                Municipio
                                            </label>
                                            <div class="col-sm-10">
                                                <asp:DropDownList ID="cboMunicipio" runat="server" CssClass="form-control"
                                                    AutoPostBack="true" OnSelectedIndexChanged="cboCiudad_SelectedIndexCahnge">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="cboColonia" class="col-sm-2 form-control-label">
                                                Colonia
                                            </label>
                                            <div class="col-sm-10">
                                                <asp:DropDownList ID="cboColonia" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="cboColonia" ErrorMessage="Favor de introducir Colonia del cliente" ForeColor="Red" />
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-group row">
                            <label for="cboPaisDom" class="col-sm-2 form-control-label">
                                País Dom</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboPaisDom" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtAntdom" class="col-sm-2 form-control-label">
                                Ant Domicilio</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtAntdom" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="txtAntdom" ErrorMessage="Favor de introducir Antiguedad de Domicilo del cliente" ForeColor="Red" />

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtTelCas" class="col-sm-2 form-control-label">
                                Tel Casa</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtTelCas" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtTelCas" ErrorMessage="Favor de introducir Telefono del cliente" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtTelCel" class="col-sm-2 form-control-label">
                                Tel Cel</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtTelCel" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboEstCiv" class="col-sm-2 form-control-label">
                                Estado Civil</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboEstCiv" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtNumDep" class="col-sm-2 form-control-label">
                                Num Dependientes</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNumDep" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboTipViv" class="col-sm-2 form-control-label">
                                Tipo Vivienda</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboTipViv" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboNacion" class="col-sm-2 form-control-label">
                                Nacionalidad</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboNacion" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboBanco" class="col-sm-2 form-control-label">
                                Banco</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboBanco" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator16" ControlToValidate="cboBanco" ErrorMessage="Favor de seleccionar banco" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtClabe" class="col-sm-2 form-control-label">
                                Clabe</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtClabe" runat="server" CssClass="form-control" MaxLength="18"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <asp:Button ID="btnSiguienteTipo" runat="server" Text="Siguiente" CssClass="btn-primary" OnClick="btnPrincipales_Click" />
            </div>
        </div>
        <br />


        <div class="panel panel-primary" id="DatosLaborales" style="display: block" runat="server">
            <div class="panel-heading">
                <h3 class="panel-title">Datos Laborales del Recomendador</h3>
            </div>
            <div id="body" class="panel-body">
                <div class="izq">
                    <div class="form-group">
                        <div class="form-group row">
                            <label for="txtEmpresa" class="col-sm-2 form-control-label">
                                Empresa</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtEmpresa" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator13" ControlToValidate="txtEmpresa" ErrorMessage="Favor de introducir Empresa del cliente" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtPuesto" class="col-sm-2 form-control-label">
                                Puesto</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtPuesto" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator14" ControlToValidate="txtPuesto" ErrorMessage="Favor de introducir Puesto del cliente" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtAntiguedad" class="col-sm-2 form-control-label">
                                Antiguedad</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtAntiguedad" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtNumEmp" class="col-sm-2 form-control-label">
                                Num Empleado</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNumEmp" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtTelEmp" class="col-sm-2 form-control-label">
                                Telefono</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtTelEmp" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboGiro" class="col-sm-2 form-control-label">
                                Giro Negocio</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboGiro" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator15" ControlToValidate="cboGiro" ErrorMessage="Favor de introducir el Giro de Negocio" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtCalleEmp" class="col-sm-2 form-control-label">
                                Calle</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtCalleEmp" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtNumIntEmp" class="col-sm-2 form-control-label">
                                Num Interior</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNumIntEmp" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtNumExtEmp" class="col-sm-2 form-control-label">
                                Num Exterior</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNumExtEmp" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="der">
                    <div class="form-group">
                        <asp:UpdatePanel ID="UpdatePanel3" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <div class="form-group row">
                                    <label for="cboEstadoEmp" class="col-sm-2 form-control-label">
                                        Estado
                                    </label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList ID="cboEstadoEmp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="cboEstadoEmp_SelectedIndexCahnge">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel4" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group row">
                                            <label for="cboMunicipioEmp" class="col-sm-2 form-control-label">
                                                Municipio
                                            </label>
                                            <div class="col-sm-10">
                                                <asp:DropDownList ID="cboMunicipioEmp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="cboCiudadEmp_SelectedIndexCahnge">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="cboColoniaEmp" class="col-sm-2 form-control-label">
                                                Colonia
                                            </label>
                                            <div class="col-sm-10">
                                                <asp:DropDownList ID="cboColoniaEmp" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="cboColoniaEmp" ErrorMessage="Favor de introducir colonia del Empleo" ForeColor="Red" />
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-group row">
                            <label for="txtIng" class="col-sm-2 form-control-label">
                                Ingreso Mensual</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtIng" runat="server" CssClass="form-control"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtOtrIng" class="col-sm-2 form-control-label">
                                Otros Ingresos</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtOtrIng" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtFuente" class="col-sm-2 form-control-label">
                                Fuente</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtFuente" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rdPEP" class="col-sm-2 form-control-label">
                                PEP</label>
                            <div class="col-sm-10">
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1">SI</asp:ListItem>
                                    <asp:ListItem Value="2">NO</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtNomPEP" class="col-sm-2 form-control-label">
                                Nombre</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNomPEP" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtPuePEP" class="col-sm-2 form-control-label">
                                Puesto</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtPuePEP" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboParPep" class="col-sm-2 form-control-label">
                                Parentesco
                            </label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboParPep" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <asp:Button ID="btnAnteriorTipo2" runat="server" Text="Anterior" CssClass="btn-primary" OnClick="btnLaboralesAnt_Click" />
                <asp:Button ID="btnSiguienteTipo2" runat="server" Text="Siguiente" CssClass="btn-primary" OnClick="btnLaboralesSig_Click" />
            </div>
        </div>
        <br />
        <div class="panel panel-primary" id="DatosReferencias" style="display: block" runat="server">
            <div class="panel-heading">
                <h3 class="panel-title">Referencias Personales o Familiares de Recomendador</h3>
            </div>
            <div id="Div1" class="panel-body">
                <div class="izq">
                    <div class="form-group">
                        <div class="form-group row">
                            <label for="txtNombreRef" class="col-sm-2 form-control-label">
                                Nombre(s)</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNombreRef1" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtApePatRef" class="col-sm-2 form-control-label">
                                Apellido Paterno</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtApePatRef1" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtApeMatRef" class="col-sm-2 form-control-label">
                                Apellido Materno</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtApeMatRef1" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboParRef1" class="col-sm-2 form-control-label">
                                Parentesco
                            </label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboParRef1" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtTelRef1" class="col-sm-2 form-control-label">
                                txtTelRef1</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtTelRef1" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cen">
                </div>
                <div class="der">
                    <div class="form-group">
                        <div class="form-group row">
                            <label for="txtCalleRef" class="col-sm-2 form-control-label">
                                Calle</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtCalleRef" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtNumExtRef" class="col-sm-2 form-control-label">
                                Num Exterior</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNumExtRef" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboEstadoRef" class="col-sm-2 form-control-label">
                                Estado
                            </label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboEstadoRef" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="cboEstadoRef_SelectedIndexCahnge">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboMunicipioRef" class="col-sm-2 form-control-label">
                                Municipio
                            </label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboMunicipioRef" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="cboCiudadRef_SelectedIndexCahnge">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cboColoniaRef" class="col-sm-2 form-control-label">
                                Colonia
                            </label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboColoniaRef" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">

                <asp:Button ID="btnAgregarTipo3" runat="server" Text="Agregar" CssClass="btn-btn-info" OnClick="btnAgregarReferencia" AutoPostBack="true" />
                <asp:Button ID="btnAnteriorTipo3" runat="server" Text="Anterior" CssClass="btn-primary" OnClick="btnReferenciasA_Click" />
                <asp:Button ID="btnSiguienteTipo3" runat="server" Text="Siguiente" CssClass="btn-primary" OnClick="btnGuardarClick" />


                <div class="GridEncabezado">
                    Referencias
                </div>
                <div class="GridContenedor" style="height: 320px">
                    <asp:GridView ID="gvreferencias" runat="server" Width="100%" AutoGenerateColumns="False"
                        Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                        CssClass="table table-bordered table-striped  table-hover table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                        PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                        DataKeyNames="nombres">
                        <Columns>
                            <asp:BoundField HeaderText="Referencia" DataField="nombres" />
                            <asp:BoundField HeaderText="Telefono" DataField="TelRef" />
                            <asp:BoundField HeaderText="Direccion" DataField="direccion" />
                        </Columns>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>


        </div>
        <div id="exito" class="alert alert-success" runat="server">
            <strong>Success!</strong> Indicates a successful or positive action.
        </div>

</asp:Content>


