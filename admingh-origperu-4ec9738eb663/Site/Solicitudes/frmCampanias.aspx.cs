﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Site_Solicitudes_frmCampanias : System.Web.UI.Page
{

    public partial class Referencia
    {
        public string nombres { get; set; }
        public int parentesco { get; set; }
        public string TelRef { get; set; }
        public string direccion { get; set; }
        public string numext { get; set; }
        public int idcolonia { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            
            
            this.Buscar.Visible = false;
            this.DatosClienteExiste.Visible = false;
            this.DatosPrincipales.Visible = false;
            this.DatosLaborales.Visible = false;
            this.DatosReferencias.Visible = false;
            
            exito.Visible = false;
            Session["Referencias"] = null;

            TBCATCampanias[] campanias = null;
            TBCATEstado[] estados = null;

            SIPais[] paises = null;
            TipoPension[] tipoPension = null;
            TBCATTipoVade[] estadoCivil = null;
            TBCATTipoVade[] tipoVivienda = null;
            TBCATTipoVade[] nacionalidad = null;
            TBCATTipoVade[] parentesco = null;
            TBCATGiro[] Giros = null;
            TBCATBanco[] Bancos = null;
            TBCATBanco[] Bancos2 = null;

            List<Referencia> referencias = new List<Referencia>();

            using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
            {

                campanias = wsCatalogo.ConsultaCampania(0, 0, "", 1);
                if (campanias.Length > 0)
                {
                    cmbCampania.DataValueField = "IdCampania";
                    cmbCampania.DataTextField = "Campania";
                    cmbCampania.DataSource = campanias;
                    cmbCampania.DataBind();
                }
                
                Bancos = wsCatalogo.ObtenerBancos(1, 0);
                if (Bancos.Length > 0)
                {
                    cboBanco.DataValueField = "IdBanco";
                    cboBanco.DataTextField = "Banco";
                    cboBanco.DataSource = Bancos;
                    cboBanco.DataBind();
                }

                Bancos2 = wsCatalogo.ObtenerBancos(1, 0);
                if (Bancos2.Length > 0)
                {
                    cboBanco2.DataValueField = "IdBanco";
                    cboBanco2.DataTextField = "Banco";
                    cboBanco2.DataSource = Bancos;
                    cboBanco2.DataBind();
                }

                estados = wsCatalogo.ObtenerTBCatEstado(0, 0, "", 0);
                if (estados.Length > 0)
                {
                    cboEstado.DataValueField = "IdEstado";
                    cboEstado.DataTextField = "Estado";
                    cboEstado.DataSource = estados;
                    cboEstado.DataBind();

                    cboEstadoNac.DataValueField = "IdEstado";
                    cboEstadoNac.DataTextField = "Estado";
                    cboEstadoNac.DataSource = estados;
                    cboEstadoNac.DataBind();

                    cboEstadoEmp.DataValueField = "IdEstado";
                    cboEstadoEmp.DataTextField = "Estado";
                    cboEstadoEmp.DataSource = estados;
                    cboEstadoEmp.DataBind();

                    cboEstadoRef.DataValueField = "IdEstado";
                    cboEstadoRef.DataTextField = "Estado";
                    cboEstadoRef.DataSource = estados;
                    cboEstadoRef.DataBind();
                }

                paises = wsCatalogo.ObtenerSIPais();
                tipoPension = wsCatalogo.ObtenerTipoPension(1);
                estadoCivil = wsCatalogo.ObtenerTBCATTipoVade(1);
                tipoVivienda = wsCatalogo.ObtenerTBCATTipoVade(3);
                nacionalidad = wsCatalogo.ObtenerTBCATTipoVade(6);
                parentesco = wsCatalogo.ObtenerTBCATTipoVade(1);
                Giros = wsCatalogo.ObtenerTBCATGiro(0, 0, "", 0);

                if (paises.Length > 0)
                {
                    cboPais.DataValueField = "Clave";
                    cboPais.DataTextField = "Pais";
                    cboPais.DataSource = paises;
                    cboPais.DataBind();

                    cboPaisDom.DataValueField = "Clave";
                    cboPaisDom.DataTextField = "Pais";
                    cboPaisDom.DataSource = paises;
                    cboPaisDom.DataBind();

                    cboPaisDom.SelectedValue = "0";
                }

                if (tipoPension.Length > 0)
                {
                    cboTipoPen.DataValueField = "IdTipoPension";
                    cboTipoPen.DataTextField = "Descripcion";
                    cboTipoPen.DataSource = tipoPension;
                    cboTipoPen.DataBind();

                }

                if (estadoCivil.Length > 0)
                {
                    cboEstCiv.DataValueField = "IdTipoVade";
                    cboEstCiv.DataTextField = "TipoVade";
                    cboEstCiv.DataSource = estadoCivil;
                    cboEstCiv.DataBind();
                }

                if (tipoVivienda.Length > 0)
                {
                    cboTipViv.DataValueField = "IdTipoVade";
                    cboTipViv.DataTextField = "TipoVade";
                    cboTipViv.DataSource = tipoVivienda;
                    cboTipViv.DataBind();
                }

                if (nacionalidad.Length > 0)
                {
                    cboNacion.DataValueField = "IdTipoVade";
                    cboNacion.DataTextField = "TipoVade";
                    cboNacion.DataSource = nacionalidad;
                    cboNacion.DataBind();
                }

                if (parentesco.Length > 0)
                {
                    cboParPep.DataValueField = "IdTipoVade";
                    cboParPep.DataTextField = "TipoVade";
                    cboParPep.DataSource = parentesco;
                    cboParPep.DataBind();

                    cboParRef1.DataValueField = "IdTipoVade";
                    cboParRef1.DataTextField = "TipoVade";
                    cboParRef1.DataSource = parentesco;
                    cboParRef1.DataBind();

                }

                if (Giros.Length > 0)
                {
                    cboGiro.DataValueField = "IdGiro";
                    cboGiro.DataTextField = "Giro";
                    cboGiro.DataSource = Giros;
                    cboGiro.DataBind();
                }

            }
        }
    }

    protected void cboEstado_SelectedIndexCahnge(object sender, EventArgs e)
    {
        int idestado = 0;
        TBCATCiudad[] ciudades = null;

        idestado = Convert.ToInt32(cboEstado.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            ciudades = wsCatalogo.ObtenerTbCatCiudadOrig(2, idestado, 0);
        }
        if (ciudades.Length > 0)
        {
            cboMunicipio.DataValueField = "IdCiudad";
            cboMunicipio.DataTextField = "Ciudad";
            cboMunicipio.DataSource = ciudades;
            cboMunicipio.DataBind();
        }
    }
    protected void cboEstadoEmp_SelectedIndexCahnge(object sender, EventArgs e)
    {
        int idestado = 0;
        TBCATCiudad[] ciudades = null;

        idestado = Convert.ToInt32(cboEstadoEmp.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            ciudades = wsCatalogo.ObtenerTbCatCiudadOrig(2, idestado, 0);
        }
        if (ciudades.Length > 0)
        {
            cboMunicipioEmp.DataValueField = "IdCiudad";
            cboMunicipioEmp.DataTextField = "Ciudad";
            cboMunicipioEmp.DataSource = ciudades;
            cboMunicipioEmp.DataBind();
        }
    }
    protected void cboEstadoRef_SelectedIndexCahnge(object sender, EventArgs e)
    {
        int idestado = 0;
        TBCATCiudad[] ciudades = null;

        idestado = Convert.ToInt32(cboEstadoRef.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            ciudades = wsCatalogo.ObtenerTbCatCiudadOrig(2, idestado, 0);
        }
        if (ciudades.Length > 0)
        {
            cboMunicipioRef.DataValueField = "IdCiudad";
            cboMunicipioRef.DataTextField = "Ciudad";
            cboMunicipioRef.DataSource = ciudades;
            cboMunicipioRef.DataBind();
        }
    }
    protected void cboCiudad_SelectedIndexCahnge(object sender, EventArgs e)
    {
        int idciudad = 0;
        TBCATColonia[] colonias = null;

        idciudad = Convert.ToInt32(cboMunicipio.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            colonias = wsCatalogo.ObtenerTbCatColonia(2, idciudad);
        }
        if (colonias.Length > 0)
        {
            cboColonia.DataValueField = "IdColonia";
            cboColonia.DataTextField = "Colonia";
            cboColonia.DataSource = colonias;
            cboColonia.DataBind();
        }
    }
    protected void cboCiudadEmp_SelectedIndexCahnge(object sender, EventArgs e)
    {
        int idciudad = 0;
        TBCATColonia[] colonias = null;

        idciudad = Convert.ToInt32(cboMunicipioEmp.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            colonias = wsCatalogo.ObtenerTbCatColonia(2, idciudad);
        }
        if (colonias.Length > 0)
        {
            cboColoniaEmp.DataValueField = "IdColonia";
            cboColoniaEmp.DataTextField = "Colonia";
            cboColoniaEmp.DataSource = colonias;
            cboColoniaEmp.DataBind();
        }
    }
    protected void cboCiudadRef_SelectedIndexCahnge(object sender, EventArgs e)
    {
        int idciudad = 0;
        TBCATColonia[] colonias = null;

        idciudad = Convert.ToInt32(cboMunicipioRef.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            colonias = wsCatalogo.ObtenerTbCatColonia(2, idciudad);
        }
        if (colonias.Length > 0)
        {
            cboColoniaRef.DataValueField = "IdColonia";
            cboColoniaRef.DataTextField = "Colonia";
            cboColoniaRef.DataSource = colonias;
            cboColoniaRef.DataBind();
        }
    }
    protected void btnPrincipales_Click(object sender, EventArgs e)
    {
        this.DatosPrincipales.Visible = false;
        this.DatosLaborales.Visible = true;
        this.DatosReferencias.Visible = false;
    }
    protected void btnLaboralesAnt_Click(object sender, EventArgs e)
    {
        this.DatosPrincipales.Visible = true;
        this.DatosLaborales.Visible = false;
        this.DatosReferencias.Visible = false;
    }
    protected void btnLaboralesSig_Click(object sender, EventArgs e)
    {
        this.DatosPrincipales.Visible = false;
        this.DatosLaborales.Visible = false;
        this.DatosReferencias.Visible = true;
    }
    protected void btnAgregarReferencia(object sender, EventArgs e)
    {
        List<Referencia> referencias = new List<Referencia>();
        referencias = (List<Referencia>)Session["Referencias"];
        Referencia referencia = new Referencia();
        referencia.nombres = txtNombreRef1.Text;
        referencia.parentesco = Convert.ToInt32(cboParRef1.SelectedValue);
        referencia.TelRef = txtTelRef1.Text;
        referencia.direccion = txtCalleRef.Text;

        referencia.numext = txtNumExtRef.Text;
        referencia.idcolonia = Convert.ToInt32(cboColoniaRef.SelectedValue);
        if (referencias != null)
        {
            referencias.Add(referencia);
        }
        else
        {
            referencias = new List<Referencia>();
            referencias.Add(referencia);
        }
        Session["Referencias"] = referencias;

        gvreferencias.DataSource = referencias;
        gvreferencias.DataBind();

        txtNombreRef1.Text = "";
        txtApePatRef1.Text = "";
        txtApeMatRef1.Text = "";
        txtNumExtRef.Text = "";
        txtTelRef1.Text = "";
        cboColoniaRef.SelectedIndex = -1;
        txtCalleRef.Text = "";


    }
    protected void btnReferenciasA_Click(object sender, EventArgs e)
    {
        this.DatosPrincipales.Visible = false;
        this.DatosLaborales.Visible = true;
        this.DatosReferencias.Visible = false;
    }

    protected void btnGuardarClick(object sender, EventArgs e)
    {
        TBClientes cliente = new TBClientes();
        int IdClienteRec = 0;
        int IdClabeRec = 0;
        
        if (rdTipoCliente.SelectedIndex != 0) // Cliente recomendador Nuevo
        {
            using (wsSOPF.ClientesClient wscliente = new ClientesClient())
            {
                cliente.Nombres = txtNombre.Text;
                cliente.Apellidop = txtApePat.Text;
                cliente.Apellidom = txtApeMat.Text;
                cliente.Sexo = rdSexo.SelectedIndex;
                cliente.FchNacimiento = inputDate.Text;
                cliente.IdCNacimiento = Convert.ToInt32(cboEstadoNac.SelectedValue);
                cliente.IdPaisNacimiento = Convert.ToInt32(cboPais.SelectedValue);
                cliente.Ocupacion = txtOcupacion.Text;
                cliente.CURP = txtCURP.Text;
                cliente.Rfc = txtRFC.Text;
                cliente.Direccion = txtCalle.Text;
                cliente.NumInterior = txtNumInt.Text;
                cliente.NumExterior = txtNumExt.Text;
                cliente.IdColonia = Convert.ToInt32(cboColonia.SelectedValue);
                cliente.AntigDom = Convert.ToInt32(txtAntdom.Text);
                cliente.Telefono = txtTelCas.Text;
                cliente.Celular = txtTelCel.Text;
                cliente.EstaCivil = Convert.ToInt32(cboEstCiv.SelectedValue);
            }
            cliente.DepeEcono = Convert.ToInt32(txtNumDep.Text);
            cliente.IdNacionalidad = Convert.ToInt32(cboNacion.SelectedValue);
            cliente.Puesto = txtPuesto.Text;
            cliente.AntigOfic = Convert.ToInt32(txtAntiguedad.Text);
            cliente.NumEmplea = txtNumEmp.Text;
            cliente.TeleOfic = txtTelEmp.Text;
            //cliente.IdGiro = Convert.ToInt32(cboGiro.SelectedValue);
            cliente.DireOfic = txtCalleEmp.Text;
            cliente.NumInterior = txtNumIntEmp.Text;
            cliente.NumExterior = txtNumExtEmp.Text;
            cliente.IdCoOfic = Convert.ToInt32(cboColoniaEmp.SelectedValue);
            cliente.Ingreso = Convert.ToDecimal(txtIng.Text);
            cliente.OtrosIngr = Convert.ToDecimal(txtOtrIng.Text);
            cliente.FuenOtroi = txtFuente.Text;
            /*cliente.IdSucursal = 0;
            cliente.IdUsuario = Convert.ToInt32(Session["UsuarioId"].ToString());
            cliente.IdEst_Cobranza = 0;
            cliente.IdTipoPension = Convert.ToInt32(cboTipoPen.SelectedValue);
            cliente.Email = txtemail.Text;
            cliente.IdTipoPersona = 0;
            cliente.tipoTel1 = 0;
            cliente.tipoTel2 = 0;
            cliente.CurpConyugue = "";
            cliente.IfeConyugue = "";*/


            int i = 0;
            List<Referencia> referencias = new List<Referencia>();
            referencias = (List<Referencia>)Session["Referencias"];

            if (referencias != null)
            {
                foreach (Referencia referencia in referencias)
                {
                    if (i == 0)
                    {
                        cliente.Referencia1 = referencia.nombres;
                        cliente.DireRef1 = referencia.direccion;
                        cliente.ExteRef1 = referencia.numext;
                        cliente.IdCoRef1 = referencia.idcolonia;
                        cliente.TeleRef1 = referencia.TelRef;

                    }
                    if (i == 1)
                    {
                        cliente.Referencia2 = referencia.nombres;
                        cliente.DireRef2 = referencia.direccion;
                        cliente.ExteRef2 = referencia.numext;
                        cliente.IdCoRef2 = referencia.idcolonia;
                        cliente.TeleRef2 = referencia.TelRef;

                    }
                    i++;
                }
            }

        }
        
         using (wsSOPF.ClientesClient wsCliente = new wsSOPF.ClientesClient())
            {
                ResultadoOfboolean resultado = new ResultadoOfboolean();
                ResultadoOfboolean resultado2 = new ResultadoOfboolean();
                int idUsu = 0;

                idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());

                try
                {
                    if (rdTipoCliente.SelectedIndex != 0) // Cliente recomendador Nuevo
                    {
                        
                        cliente.IdCliente = 0;

                        resultado = wsCliente.InsertarTBClientes(cliente);
                        

                        if (resultado.Codigo == 1)
                        {
                            int IdCliente = Convert.ToInt32(Request.QueryString["idcliente"]);
                            IdClienteRec = Convert.ToInt32(resultado.Mensaje);

                            exito.Visible = true;

                            TBClabes.ClabeCliente[] Clabes = null;
                            TBClabes clabe = new TBClabes();
                            TBClabes.DataResultClabe claben = new TBClabes.DataResultClabe();

                            Clabes = wsCliente.ObtenerClabesCliente(1, Convert.ToInt32(txtClabe.Text), 0, txtClabe.ToString());

                            if (Clabes.Length <= 0)
                            {
                                clabe.IdCliente = IdClienteRec;
                                clabe.Clabe = txtClabe.Text;
                                clabe.Activa = 1;
                                clabe.IdBanco = Convert.ToInt16(cboBanco.SelectedValue);
                                claben = wsCliente.InsertarTBClabes(1, clabe);


                                if (Convert.ToInt32(claben.Bandera) >= 0)
                                {
                                    resultado2 = wsCliente.InsertarClienteRecomendador(0, Convert.ToInt32(Request.QueryString["idcliente"]), IdClienteRec, Convert.ToInt32(claben.desBandera), Convert.ToInt32(cmbCampania.SelectedValue), idUsu);
                                }

                            }
                            else
                            {
                                IdClabeRec = Clabes[0].IdClabe;

                                resultado2 = wsCliente.InsertarClienteRecomendador(0, Convert.ToInt32(Request.QueryString["idcliente"]), IdClienteRec, IdClabeRec, Convert.ToInt32(cmbCampania.SelectedValue), idUsu);
                            }



                            this.DatosPrincipales.Visible = false;
                            this.DatosLaborales.Visible = false;
                            this.DatosReferencias.Visible = false;
                            
                            Response.Redirect("frmDatosSolicitud.aspx?idCliente=" + IdCliente);
                        

                        }
                    }
                    else
                    {

                        int IdCliente = Convert.ToInt32(Request.QueryString["idcliente"]);
                        IdClienteRec = Convert.ToInt32(hdnIdCliente.Value);

                        TBClabes.ClabeCliente[] Clabes = null;
                        TBClabes clabe = new TBClabes();
                        TBClabes.DataResultClabe claben = new TBClabes.DataResultClabe();

                        Clabes = wsCliente.ObtenerClabesCliente(1, IdClienteRec, 0, txtClabe2.Text);

                        if (Clabes.Length <= 0)
                        {
                            clabe.IdCliente = IdClienteRec;
                            clabe.Clabe = txtClabe2.Text;
                            clabe.Activa = 1;
                            clabe.IdBanco = Convert.ToInt16(cboBanco2.SelectedValue);
                            claben = wsCliente.InsertarTBClabes(1, clabe);


                            if (Convert.ToInt32(claben.Bandera) >= 0)
                            {
                                resultado2 = wsCliente.InsertarClienteRecomendador(0, IdCliente, IdClienteRec, Convert.ToInt32(claben.desBandera), Convert.ToInt32(cmbCampania.SelectedValue), 0);
                            }

                        }
                        else
                        {
                            IdClabeRec = Clabes[0].IdClabe;
                            //idUsu = Convert.ToInt32(Session["UsuarioId"].ToString());
                            resultado2 = wsCliente.InsertarClienteRecomendador(0, Convert.ToInt32(Request.QueryString["idcliente"]), IdClienteRec, IdClabeRec, Convert.ToInt32(cmbCampania.SelectedValue), 0);
                        }
                        
                    }

                    this.Response.Redirect("frmDatosSolicitud.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&TipoCredito=" + Request.QueryString["TipoCredito"].ToString() + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString() + "&idclienterec=" + IdClienteRec.ToString() + "&idclaberec=" + IdClabeRec.ToString() );
                }
                
                catch(Exception ex)
                {

                }

            }
        

    }

    protected void rdTipoCliente_SelectedIndexChange(object sender, EventArgs e)
    {
        if (rdTipoCliente.SelectedIndex == 0)   //Existente
        {
            this.Buscar.Visible = true;
            this.DatosPrincipales.Visible = false;
            this.DatosLaborales.Visible = false;
            this.DatosReferencias.Visible = false;
            this.DatosClienteExiste.Visible = true;
        }
        else
        {
            this.Buscar.Visible = false;
            this.DatosPrincipales.Visible = true;
            this.DatosLaborales.Visible = false;
            this.DatosReferencias.Visible = false;
            this.DatosClienteExiste.Visible = false;
           
        }
    }

    protected void btnBuscarCliente_Click(object sender, EventArgs e)
    {
        TBClientes.BusquedaCliente[] clientes = null;
        List<TBClientes.BusquedaCliente> listacliente = new List<TBClientes.BusquedaCliente>();

        using (wsSOPF.ClientesClient wsCliente = new ClientesClient())
        {
            clientes = wsCliente.BusquedaCliente(14, 0, 0, txtCliente.Text, DateTime.Now, DateTime.Now, 0);
        }

        if (clientes.Length > 0)
        {
            gvClientes.DataSource = clientes;
            gvClientes.DataBind();
        }

    }
    protected void gvClientes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //check the item being bound is actually a DataRow, if it is, 
        //wire up the required html events and attach the relevant JavaScripts
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            e.Row.Attributes["onmouseover"] = "javascript:setMouseOverColor(this);";
            e.Row.Attributes["onmouseout"] = "javascript:setMouseOutColor(this);";
            //e.Row.Attributes.Add("onClick", "javascript:void SelectRowCli(this);");

        }

 

    }
    protected void gvClientes_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        GridView gv = (GridView)sender;
        gv.PageIndex = e.NewPageIndex;
        TBClientes.BusquedaCliente[] clientes = null;
        List<TBClientes.BusquedaCliente> listacliente = new List<TBClientes.BusquedaCliente>();

        using (wsSOPF.ClientesClient wsCliente = new ClientesClient())
        {
            clientes = wsCliente.BusquedaCliente(14, 0, 0, txtCliente.Text, DateTime.Now, DateTime.Now, 0);
        }

        if (clientes.Length > 0)
        {
            gvClientes.DataSource = clientes;
            gvClientes.DataBind();
        }


    }

    protected void gvClientes_SelectedIndexChanged(Object sender, EventArgs e)
    {
        GridViewRow row = gvClientes.SelectedRow;
        int prueba = 0;
        prueba = Convert.ToInt32(row.Cells[2].Text);

    }

    protected void gvClientes_SelectedIndexChanging(Object sender, GridViewSelectEventArgs e)
    {
        GridViewRow row = gvClientes.Rows[e.NewSelectedIndex];

    }


    protected void btnModificar_Click(object sender, EventArgs e)
    {
        //this.Response.Redirect("frmCliente.aspx?idcliente=9724");
       
        int IdClienteMod;
        IdClienteMod = Convert.ToInt32(hdnIdCliente.Value);

        Response.Redirect("frmCliente.aspx?idCliente=" + IdClienteMod);
    }



}