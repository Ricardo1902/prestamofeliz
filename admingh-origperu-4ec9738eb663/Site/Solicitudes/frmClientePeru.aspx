﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmClientePeru.aspx.cs" EnableEventValidation="true" Inherits="Site_Solicitudes_frmClientePeru" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolKit" %>

<asp:Content ID="head" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../../jQuery/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../jQuery/jquery-ui.structure.min.css" rel="stylesheet" />Situación Laboral
    <link href="../../jQuery/jquery-ui.theme.min.css" rel="stylesheet" />
    <link href="../../Css/select2.min.css" rel="stylesheet" />
    <link href="../../Css/select2-bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../jQuery/external/jquery/jquery.js"></script>
    <script type="text/javascript" src="../../jQuery/jquery-ui.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI6AK8yPcPP78k5P4KGvsU7d_uVCx-kg8" async defer></script>
    <script type="text/javascript" src="../../js/select2.min.js"></script>
    <script type="text/javascript" src="../../js/es.js"></script>

    <script type="text/javascript">        
        $(function () {

            $("#divAccordion").accordion({ collapsible: true, heightStyle: "content" });

            $(".headerAcc").click(function () {
                $(this).find(".blink").css("visibility", "hidden");
            });
        });

        function redirectCliente(row) {
            const idCliente = $(row).children("td:last").text();
            window.location.replace("frmClientePeru.aspx?idcliente=" + idCliente);
        }
    </script>
    <script type="text/javascript">
        var lon = 0;
        var lat = 0;

        $(function () {
            $("#<%=txtNombreVia.ClientID%>").on("blur", ConsultarDireccion);
            $("#<%=txtNumExt.ClientID%>").on("blur", ConsultarDireccion);

            //show map on modal
            $('#mooDirGMaps').on('shown.bs.modal', function () {
                initializeMap();
            });
        });

        //Re-Create for on page postbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //show map on modal
            $('#mooDirGMaps').on('shown.bs.modal', function () {
                initializeMap();
            });
        });

        function initializeMap() {
            var mapOptions = {
                center: new google.maps.LatLng(lat, lon),
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map_canvas"),
                mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lon)
            });
            marker.setMap(map);
        }

        function ConsultarDireccion() {
            $('#btnMapDireccion').hide();
            var vColonia = $("#<%=cboColonia.ClientID%>").children(':selected').val();
            var sCalle = $.trim($("#<%=txtNombreVia.ClientID%>").val());
            var sNumExt = $.trim($("#<%=txtNumExt.ClientID%>").val());

            if (vColonia > 0 && sCalle != "" && sNumExt != "") {
                var sEstado = $("#<%=cboEstado.ClientID%>").children(':selected').text();
                var sMunicipio = $("#<%=cboMunicipio.ClientID%>").children(':selected').text();
                var sColonia = $("#<%=cboColonia.ClientID%>").children(':selected').text();

                var dataValue = { "Calle": sCalle, "NumeroExt": sNumExt, "Colonia": sColonia, "Municipio": sMunicipio, "Estado": sEstado };

                $.ajax({
                    url: "frmCliente.aspx/ConsultarDireccionGMaps",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(dataValue),
                    success: function (response) {
                        var objRes = response.d;

                        lat = JSON.parse(objRes).latitud;
                        lon = JSON.parse(objRes).longuitud;

                        $('#btnMapDireccion').fadeIn("slow", function () {
                        });
                    },
                    failure: function (response) {
                        //alert('failure' + response.d);
                    },
                    error: function (xhr, status, errorThrown) {
                        var objEx = xhr.responseJSON.d;
                        $('#btnMapDireccion').hide();
                        //alert(JSON.parse(objEx).mensaje)
                    },
                    complete: function (jqXHR, status) {
                        //alert("complete: " + status + "\n\nResponse: " + jqXHR.responseText);
                    }
                });
            }
        }
    </script>
    <style type="text/css">
        textarea {
            resize: none;
        }
        .search-dni {
            cursor: pointer;
            color: dodgerblue;
        }
        .search-dni:hover {
            color: #5aaeff;
        }
        .clear-dni {
            cursor: pointer;
            color: #ff0000;
        }
        .clear-dni:hover {
            color: #ff5555;
        }
    </style>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="divAccordion">
        <h3 class="headerAcc">SOLICITUD DE CREDITO
            <asp:UpdatePanel ID="upValDatosSolicitudCredito" UpdateMode="Conditional" RenderMode="Inline" runat="server">
                <ContentTemplate>
                    <span id="valIconDatosSolicitudCredito" class="glyphicon glyphicon-exclamation-sign blink" visible="false" runat="server"></span>
                </ContentTemplate>
            </asp:UpdatePanel>
        </h3>
        <div>
            <div class="panel-body">
                <asp:UpdateProgress AssociatedUpdatePanelID="upSolicitudCredito" DisplayAfter="100" runat="server">
                    <ProgressTemplate>
                        <div id="loader-background"></div>
                        <div id="loader-content"></div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel ID="upSolicitudCredito" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-group col-md-3">
                                        <label for="txtFechaSol" class="form-control-label small">Fecha</label>
                                        <asp:TextBox ID="txtFechaSol" runat="server" CssClass="form-control" Style="text-transform: uppercase;" Enabled="false"></asp:TextBox>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="txtFolioSolicitud" class="form-control-label small">N° Folio</label>
                                        <asp:TextBox ID="txtFolioSolicitud" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvFolioSolicitud" ControlToValidate="txtFolioSolicitud" ErrorMessage="Capture el Numero de folio" ValidationGroup="DatosSolicitudCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-group col-md-3">
                                        <label for="txtDNISolicitante" class="form-control-label small">DNI solicitante:</label>
                                        <div class="input-group">
                                            <asp:TextBox ID="txtDNISolicitante" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="8"></asp:TextBox>
                                            <span class="input-group-addon">
                                                <asp:LinkButton ID="lnkSearchDNI" runat="server" OnClick="lnkSearchDNI_Click">
                                                    <i class="fas fa-search search-dni"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lnkClearDNI" runat="server" OnClick="lnkClearDNI_Click" Visible="false">
                                                    <i class="fas fa-times clear-dni"></i>
                                                </asp:LinkButton>
                                            </span>
                                        </div>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvNumDoc" ControlToValidate="txtDNISolicitante" ErrorMessage="Favor de introducir el DNI" ForeColor="Red" ValidationGroup="DatosSolicitudCredito" CssClass="small" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="cvDNIListaNegra" CssClass="small" ForeColor="Red" ErrorMessage="DNI Lista Negra" ValidationGroup="DatosSolicitudCredito" runat="server"></asp:CustomValidator>
                                    </div>
                                    <div class="form-group col-md-9">
                                        <label for="txtNombreSolicitante" class="form-control-label small">Nombre de solicitante:</label>
                                        <asp:TextBox ID="txtNombreSolicitante" runat="server" CssClass="form-control" Style="text-transform: uppercase;" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-group col-md-4">
                                        <label for="ddlCanalVentas" class="form-control-label small">Canal de ventas:</label>
                                        <asp:DropDownList ID="ddlCanalVentas" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlCanalVentas_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        <asp:CompareValidator ID="cvDdlCanalVentas" runat="server" ControlToValidate="ddlCanalVentas" ValueToCompare="-1" Operator="NotEqual" Type="Integer" ErrorMessage="Favor de introducir el canal de ventas" ForeColor="Red" ValidationGroup="DatosSolicitudCredito" CssClass="small" Display="Dynamic" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ddlPromotor" class="form-control-label small">Promotor:</label>
                                        <asp:DropDownList ID="ddlPromotor" runat="server" CssClass="form-control"></asp:DropDownList>
                                        <asp:CompareValidator ID="cvDdlPromotor" runat="server" ControlToValidate="ddlPromotor" ValueToCompare="-1" Operator="NotEqual" Type="Integer" ErrorMessage="Favor de introducir el Promotor" ForeColor="Red" ValidationGroup="DatosSolicitudCredito" CssClass="small" Display="Dynamic" />
                                    </div>                                    
                                    <div class="form-group col-md-4">
                                        <label for="txtScoreExperian" class="form-control-label small">Score experian:</label>
                                        <asp:TextBox ID="txtScoreExperian" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvScoreExperian" ControlToValidate="txtScoreExperian" ErrorMessage="Capture el Score experian" ValidationGroup="DatosSolicitudCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-group col-md-4">
                                        <label for="ddlOrigen" class="form-control-label small">Origen:</label>
                                        <asp:DropDownList ID="ddlOrigen" runat="server" CssClass="form-control"></asp:DropDownList>
                                        <asp:CompareValidator ID="cvDdlOrigen" runat="server" ControlToValidate="ddlOrigen" ValueToCompare="-1" Operator="NotEqual" Type="Integer" ErrorMessage="Favor de introducir el origen" ForeColor="Red" ValidationGroup="DatosSolicitudCredito" CssClass="small" Display="Dynamic" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <asp:HiddenField ID="hdnTipoCliente" runat="server" />
                                        <label for="txtTipoCliente" class="form-control-label small">Tipo de cliente:</label>
                                        <asp:TextBox ID="txtTipoCliente" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="txtCategoriaCliente" class="form-control-label small">Categoría de cliente:</label>
                                        <asp:TextBox ID="txtCategoriaCliente" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-group col-md-6 col-lg-3 hidden">
                                        <label for="ddlTipoConvenio" class="form-control-label small">Empresa:</label>
                                        <div>
                                            <asp:DropDownList ID="ddlTipoConvenio" DataValueField="IdTipoConvenio" DataTextField="TipoConvenio" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlTipoConvenio_SelectedIndexChanged" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ddlProducto" class="form-control-label small">Producto:</label>
                                        <asp:DropDownList ID="ddlProducto" DataValueField="Valor" DataTextField="Descripcion" OnSelectedIndexChanged="ddlProducto_SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                                        <asp:CompareValidator ID="cvDdlProducto" runat="server" ControlToValidate="ddlProducto" ValueToCompare="-1" Operator="NotEqual" Type="Integer" ErrorMessage="Favor de introducir el producto" ForeColor="Red" ValidationGroup="DatosSolicitudCredito" CssClass="small" Display="Dynamic" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ddlSubProducto" class="form-control-label small">Sub-Producto:</label>
                                        <asp:DropDownList ID="ddlSubProducto" DataValueField="IdSubProducto" DataTextField="SubProducto" runat="server" CssClass="form-control"></asp:DropDownList>
                                        <asp:CompareValidator ID="cvDdlSubProducto" runat="server" ControlToValidate="ddlSubProducto" ValueToCompare="-1" Operator="NotEqual" Type="Integer" ErrorMessage="Favor de introducir el sub-producto" ForeColor="Red" ValidationGroup="DatosSolicitudCredito" CssClass="small" Display="Dynamic" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <asp:HiddenField ID="hdnTipoCredito" runat="server" />
                                        <label for="txtTipoCredito" class="form-control-label small">Tipo de crédito:</label>
                                        <asp:TextBox ID="txtTipoCredito" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!--panel-body-->
        </div>        
        <h3 id="hdrAccBusquedaCliente" runat="server">BÚSQUEDA DE CLIENTE</h3>
        <div id="cntAccBusquedaCliente" runat="server">
            <div class="panel-body">
                <asp:UpdateProgress AssociatedUpdatePanelID="upTipoCliente" DisplayAfter="100" runat="server">
                    <ProgressTemplate>
                        <div id="loader-background"></div>
                        <div id="loader-content"></div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel ID="upTipoCliente" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div id="divClienteExistente" style="margin-top: 10px; display: block;">
                            <div class="form-group row">
                                <hr />
                                <label for="txtBuscarCliente" class="small col-sm-2">Cliente</label>
                                <div class="col-sm-8">
                                    <asp:TextBox runat="server" ID="txtBuscarCliente" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <asp:Button ID="btnBuscarCliente" runat="server" Text="Buscar" CssClass="btn btn-sm btn-primary" OnClick="btnBuscarCliente_Click" ValidationGroup="SinValidar" />
                                </div>
                            </div>

                            <div id="Clientes">
                                <div class="GridContenedor" style="height: 320px">
                                    <asp:GridView ID="gvClientes" runat="server" Width="100%" AutoGenerateColumns="False"
                                        CssClass="table table-bordered table-striped table-hover table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                        PageSize="5" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false" EmptyDataText="No se encontraron Clientes"
                                        DataKeyNames="clave" Style="overflow-x: auto;" OnRowDataBound="gvClientes_RowDataBound" OnPageIndexChanging="gvClientes_PageIndexChanging">
                                        <PagerStyle CssClass="pagination-ty warning" />
                                        <Columns>
                                            <asp:BoundField HeaderText="DNI" DataField="dni" />
                                            <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                            <asp:BoundField HeaderText="Nombre" DataField="Nombres" />
                                            <asp:BoundField HeaderText="Ap. Paterno" DataField="apellidop" />
                                            <asp:BoundField HeaderText="Ap. Materno" DataField="apellidom" />
                                            <asp:BoundField HeaderText="Fecha Nac." DataField="fch_Nacimiento" />
                                            <asp:BoundField HeaderText="Direccion" DataField="direccion" />
                                            <asp:BoundField HeaderText="Colonia" DataField="colonia" />
                                            <asp:BoundField HeaderText="Ciudad" DataField="ciudad" />
                                            <asp:BoundField HeaderText="Estado" DataField="estado" />
                                            <asp:BoundField HeaderText="CP" DataField="cp" />
                                            <asp:BoundField HeaderText="Id Cliente" DataField="IdCliente" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No se encontraron Clientes
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                            <!--Clientes-->
                        </div>
                        <!--divClienteExistente-->
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!--panel-body-->
        </div>
        <!--TIPO DE CLIENTE-->
        <h3 class="headerAcc">DATOS DEL SOLICITANTE
        <asp:UpdatePanel ID="upValDatosSolicitante" UpdateMode="Conditional" RenderMode="Inline" runat="server">
            <ContentTemplate>
                <span id="valIconDatosSolicitante" class="glyphicon glyphicon-exclamation-sign blink" visible="false" runat="server"></span>
            </ContentTemplate>
        </asp:UpdatePanel>
        </h3>
        <div>
            <div class="panel-body">
                <asp:UpdateProgress AssociatedUpdatePanelID="upDatosSolicitante" DisplayAfter="100" runat="server">
                    <ProgressTemplate>
                        <div id="loader-background"></div>
                        <div id="loader-content"></div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel ID="upDatosSolicitante" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="izq">
                            <div class="form-group clearfix">
                                <asp:Panel ID="pnlListaNegra" CssClass="col-sm-12" Visible="false" runat="server">
                                    <div class="form-group col-sm-12 alert alert-danger">
                                        <span style="font-size: 1.5em; color: Tomato;" class="glyphicon glyphicon-exclamation-sign blink"></span>
                                        <asp:Label Text="El cliente tiene coincidencias en Lista Negra" Enabled="false" runat="server"></asp:Label>
                                        <asp:Button ID="lbtnListaNegra" CssClass="btn btn-xs btn-warning" Text="Mostrar" OnClick="lbtnListaNegra_Click" Visible="true" runat="server"></asp:Button>
                                    </div>

                                    <asp:GridView ID="gvListaNegra" Width="100%" AutoGenerateColumns="False"
                                        HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                        CssClass="table table-bordered table-striped table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData"
                                        RowStyle-Wrap="false" HeaderStyle-Wrap="false" Visible="false" runat="server">
                                        <Columns>
                                            <asp:BoundField DataField="DNI" HeaderText="DNI" />
                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                            <asp:BoundField DataField="ApellidoPaterno" HeaderText="ApellidoPaterno" />
                                            <asp:BoundField DataField="ApellidoMaterno" HeaderText="ApellidoMaterno" />
                                            <asp:BoundField DataField="Origen" HeaderText="Origen" />
                                        </Columns>
                                    </asp:GridView>

                                    <hr />
                                </asp:Panel>
                                <div class="form-group col-md-6">
                                    <label for="txtNombre" class="form-control-label small">Nombre(s)</label>
                                    <div>
                                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="50" OnTextChanged="ValidarListaNegraNombre" AutoPostBack="true"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="txtNombre" ErrorMessage="Nombre es requerido" ForeColor="Red" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="txtApePat" class="form-control-label small">Apellido Paterno</label>
                                    <div>
                                        <asp:TextBox ID="txtApePat" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="50" OnTextChanged="ValidarListaNegraNombre" AutoPostBack="true"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtApePat" ErrorMessage="Apellido Paterno es requerido" ForeColor="Red" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="txtApeMat" class="form-control-label small">Apellido Materno</label>
                                    <div>
                                        <asp:TextBox ID="txtApeMat" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="50" OnTextChanged="ValidarListaNegraNombre" AutoPostBack="true"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtApeMat" ErrorMessage="Apellido Materno es requerido" ForeColor="Red" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="rdSexo" class="col-sm-1 form-control-label small">Sexo</label>
                                <div class="col-sm-3">
                                    <asp:RadioButtonList ID="rdSexo" runat="server" RepeatDirection="Horizontal" CssClass="small">
                                        <asp:ListItem Value="8" Selected="True">&nbsp;Femenino&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="7">&nbsp;Masculino&nbsp;</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>

                                <label for="FecNac" class="col-sm-2 form-control-label small">Fecha Nacimiento</label>
                                <div class="form-group col-sm-3">
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFechaNacimiento" OnTextChanged="txtFechaNacimiento_TextChanged" AutoPostBack="true" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10" runat="server" />
                                        <span class="input-group-addon">
                                            <asp:LinkButton ID="btnSelectFechaNacimiento" runat="server">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </asp:LinkButton>
                                        </span>
                                    </div>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="txtFechaNacimiento" ErrorMessage="Capture Fecha Nacimiento" ForeColor="Red" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                        ErrorMessage="Formato Incorrecto"
                                        ControlToValidate="txtFechaNacimiento"
                                        ForeColor="Red"
                                        ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" CssClass="small" Display="Dynamic" />
                                    <ajaxToolkit:CalendarExtender ID="ceFechaNacimiento" Format="dd/MM/yyyy" TargetControlID="txtFechaNacimiento" PopupButtonID="btnSelectFechaNacimiento" runat="server"></ajaxToolkit:CalendarExtender>
                                </div>

                                <label for="txtEdad" class="col-sm-1 form-control-label small">Edad</label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtEdad" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <div class="form-group col-md-6">
                                    <label for="cboEstadoNac" class="form-control-label small">Lugar de Nacimiento</label>
                                    <div>
                                        <asp:DropDownList ID="cboEstadoNac" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="cboPais" class="form-control-label small">Pa&iacute;s Nacimiento</label>
                                    <div>
                                        <asp:DropDownList ID="cboPais" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group row">
                                <label for="txtOcupacion" class="col-sm-2 form-control-label small">Ocupaci&oacute;n</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtOcupacion" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtOcupacion" ErrorMessage="Favor de introducir Ocupacion del cliente" ForeColor="Red" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <div class="form-group row">
                                    <div class="hidden">
                                        <label for="ddlTipoDoc" class="col-md-2 form-control-label small">Tipo de Documento</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlTipoDoc" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        </div>                                   
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="form-group row">
                                    <label for="txtemail" class="col-md-2 form-control-label small">E-Mail</label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" MaxLength="250"></asp:TextBox>
                                    </div>

                                    <label for="cboNacion" class="col-md-2 form-control-label small">Nacionalidad</label>
                                    <div class="col-md-4">
                                        <asp:DropDownList ID="cboNacion" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <hr />
                            </div>
                            <!--form-group-->
                        </div>
                        <div class="der">
                            <div class="form-group clearfix">
                                <asp:UpdatePanel runat="server" ID="udpDireccion" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group row">
                                            <label for="ddlDireccion" class="col-sm-2 form-control-label small">Direcci&oacute;n</label>
                                            <div class="col-sm-10">
                                                <asp:DropDownList ID="ddlDireccion" runat="server" CssClass="form-control" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlDireccion_SelectedIndexChange">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <asp:Panel ID="pnlDatosVia" runat="server">
                                            <div class="form-group col-md-6">
                                                <label for="txtNombreVia" class="form-control-label small">Nombre de V&iacute;a</label>
                                                <div>
                                                    <asp:TextBox ID="txtNombreVia" runat="server" CssClass="form-control" Style="text-transform: uppercase;" Text="" MaxLength="50"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="rfvNombreVia" ControlToValidate="txtNombreVia" ForeColor="Red" ErrorMessage="Nombre de vía es requerido" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="txtNumExt" class="form-control-label small">Num Exterior</label>
                                                <div>
                                                    <asp:TextBox ID="txtNumExt" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="20"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtNumExt" ErrorMessage="Numero Exterior es requerido" ForeColor="Red" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="txtNumInt" class="form-control-label small">Num Interior</label>
                                                <div>
                                                    <asp:TextBox ID="txtNumInt" runat="server" CssClass="form-control" Style="text-transform: uppercase;" Text="0" MaxLength="20"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlDatosLote" runat="server">
                                            <div class="form-group col-md-6">
                                                <label for="txtManzana" class="form-control-label small">Manzana</label>
                                                <div>
                                                    <asp:TextBox ID="txtManzana" runat="server" CssClass="form-control" Style="text-transform: uppercase;" Text="" MaxLength="50"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtManzana" ForeColor="Red" ErrorMessage="Manzana es requerido" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="txtLote" class="form-control-label small">Lote</label>
                                                <div>
                                                    <asp:TextBox ID="txtLote" runat="server" CssClass="form-control" Style="text-transform: uppercase;" Text="" MaxLength="50"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtLote" ForeColor="Red" ErrorMessage="Lote es requerido" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="form-group">
                                <div class="form-group row">
                                    <label for="ddlTipoZona" class="col-md-2 form-control-label small">Tipo de Zona</label>
                                    <div class="col-md-4">
                                        <asp:DropDownList ID="ddlTipoZona" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlTipoZona_SelectedIndexChange" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>

                                    <label for="txtNombreZona" class="col-md-2 form-control-label small">Nombre de Zona</label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="txtNombreZona" runat="server" CssClass="form-control" Style="text-transform: uppercase;" Text="" MaxLength="50"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator runat="server" ID="rfvNombreZona" ControlToValidate="txtNombreZona" ForeColor="Red" ErrorMessage="Favor de introducir el Nombre de Zona"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="form-group col-md-4">
                                    <label for="cboEstado" class="form-control-label small">Departamento</label>
                                    <div>
                                        <asp:DropDownList ID="cboEstado" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboEstado_SelectedIndexChange" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="cboMunicipio" class="form-control-label small">Provincia</label>
                                    <div>
                                        <asp:DropDownList ID="cboMunicipio" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboMunicipio_SelectedIndexChange" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="cboColonia" class="form-control-label small">Distrito</label>
                                    <div>
                                        <asp:DropDownList ID="cboColonia" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="cboColonia" ValueToCompare="0" Operator="NotEqual" Type="Integer" ErrorMessage="Favor de introducir el Distrito del cliente" ForeColor="Red" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                    </div>
                                    <!--
                                        <button id="btnMapDireccion" type="button" class="btn btn-secondary btn-sm" style="display:none;" data-toggle="modal" data-target="#mooDirGMaps"><i class="fa fa-map-marker" aria-hidden="true"></i></button>
                                        <div class="modal fade" id="mooDirGMaps" role="dialog">
                                            <div class="modal-dialog">                                                        
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Google Maps</h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <div id="map_canvas" style="width:auto; height: 500px;"></div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    -->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group row">
                                    <label for="txtReferenciaDomicilio" class="col-sm-2 form-control-label small">Referencia Domicilio</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtReferenciaDomicilio" TextMode="MultiLine" Rows="3" CssClass="form-control" Style="text-transform: uppercase;" Text="" MaxLength="500" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="form-group col-md-2">
                                    <label for="txtLada" class="form-control-label small">C&oacute;digo LDN</label>
                                    <div>
                                        <asp:TextBox ID="txtLada" runat="server" CssClass="form-control" Enabled="false" MaxLength="2"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="txtTelCas" class="form-control-label small">Tel Casa</label>
                                    <div>
                                        <asp:TextBox ID="txtTelCas" runat="server" CssClass="form-control" Text="0" MaxLength="6"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator12" ControlToValidate="txtTelCas" ErrorMessage="Favor de introducir Telefono del cliente" ForeColor="Red" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="revTelCas" runat="server"
                                            ErrorMessage="El telefono deben ser 6 digitos."
                                            ControlToValidate="txtTelCas"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{6}$" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="txtTelCel" class="form-control-label small">Tel Cel</label>
                                    <div>
                                        <asp:TextBox ID="txtTelCel" runat="server" CssClass="form-control" Text="0" MaxLength="9"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                            ErrorMessage="El celular deben ser de 9 dígitos."
                                            ControlToValidate="txtTelCel"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{9}$" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group clearfix">
                                <div class="form-group col-md-2">
                                    <label for="txtNumDep" class="form-control-label small">Num Dependientes</label>
                                    <div>
                                        <asp:TextBox ID="txtNumDep" runat="server" CssClass="form-control" Text="0" MaxLength="2"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                            ErrorMessage="Capture entre 0-99"
                                            ControlToValidate="txtNumDep"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,2}$" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="cboEstCiv" class="form-control-label small">Estado Civil</label>
                                    <div>
                                        <asp:DropDownList ID="cboEstCiv" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboEstCiv_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="cboTipViv" class="form-control-label small">Tipo Vivienda</label>
                                    <div>
                                        <asp:DropDownList ID="cboTipViv" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboTipViv_SelectedIndexChange" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <asp:Panel runat="server" ID="PanArren" CssClass="form-group" Visible="false">
                                     <div class="form-group col-md-6">
                                        <label for="lblNomArren" class="form-control-label small">Nombre de Arrendatario</label>
                                        <div>
                                           <asp:TextBox ID="txtNomArren" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>                                      
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="txtNumAnoViven" class="form-control-label small">Nro Años que Viven en el Lugar</label>
                                        <div>
                                            <asp:TextBox ID="txtNumAnoViven" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>                               
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="PanFam" CssClass="form-group" Visible="false">
                                     <div class="form-group col-md-6">
                                        <label for="lblNomFam" class="form-control-label small">Nombre del Familiar</label>
                                        <div>
                                           <asp:TextBox ID="txtNomFam" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>                                      
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="txtParentesco" class="form-control-label small">Parentesco</label>
                                        <div>
                                           <asp:DropDownList runat="server" ID="txtParentesco" CssClass="form-control" >
	                                         <asp:ListItem Text="PADRES" Value="PADRES"></asp:ListItem>
	                                         <asp:ListItem Text="HERMANOS" Value="HERMANOS"></asp:ListItem>	
                                             <asp:ListItem Text="PRIMOS" Value="PRIMOS"></asp:ListItem>	
                                             <asp:ListItem Text="TIOS" Value="TIOS"></asp:ListItem>	
                                               <asp:ListItem Text="FAMILIAR DIRECTO DE CONYUGE" Value="CONYUGE"></asp:ListItem>
	                                         <asp:ListItem Text="AMIGO CERCANO" Value="AMIGO"></asp:ListItem>	
                                             <asp:ListItem Text="COLABORADOR DE MI HOGAR / EMPRESA / EDIFICIO" Value="COLABORA"></asp:ListItem>	
                                             <asp:ListItem Text="COMPAÑERO DE TRABAJO" Value="TRABAJO"></asp:ListItem>	
                                               <asp:ListItem Text="VECINO" Value="VECINO"></asp:ListItem>
	                                         <asp:ListItem Text="REFERIDO DE CONFIANZA" Value="REFERIDO"></asp:ListItem>	
                                             <asp:ListItem Text="CONOCIDO" Value="CONOCIDO"></asp:ListItem>	
                                       </asp:DropDownList>                                 
                                        </div>
                                    </div>
                                </asp:Panel>

                            </div>
                            <!--form-group-->
                        </div>
                        <!--DER-->
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!--panel-body-->
        </div>
        <!--DATOS DEL SOLICITANTE-->
        <h3 class="headerAcc">DATOS DEL C&Oacute;NYUGE
        <asp:UpdatePanel ID="upValDatosCony" UpdateMode="Conditional" RenderMode="Inline" runat="server">
            <ContentTemplate>
                <span id="valIconDatosCony" class="glyphicon glyphicon-exclamation-sign blink" visible="false" runat="server"></span>
            </ContentTemplate>
        </asp:UpdatePanel>
        </h3>
        <div>
            <asp:UpdateProgress AssociatedUpdatePanelID="upDatosCony" DisplayAfter="100" runat="server">
                <ProgressTemplate>
                    <div id="loader-background"></div>
                    <div id="loader-content"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upDatosCony" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="panel-body">
                         <asp:Panel runat="server" ID="PanelConyu" CssClass="form-group" Visible="false">
                        <div class="izq">
                           
                                   
                            <div class="form-group">
                                <div class="form-group row">
                                    <label for="txtNombreCony" class="col-sm-2 form-control-label small">Nombre</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtNombreCony" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="50" Enabled="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvNombreCony" ControlToValidate="txtNombreCony" ErrorMessage="Favor de introducir nombre de cónyuge" ForeColor="Red" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <%--<div class="form-group row">
                                        <label for="txtApePatCony" class="col-sm-2 form-control-label small">Apellido Paterno</label>
                                        <div class="col-sm-10">
                                        <asp:TextBox ID="txtApePatCony" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="50" Enabled="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvApePatCony" ControlToValidate="txtApePatCony" ErrorMessage="Favor de introducir Apellido Paterno del cónyuge" ForeColor="Red" Enabled="false"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="txtApeMatCony" class="col-sm-2 form-control-label small">Apellido Materno</label>
                                        <div class="col-sm-10">
                                        <asp:TextBox ID="txtApeMatCony" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="50" Enabled="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvApeMatCony" ControlToValidate="txtApeMatCony" ErrorMessage="Favor de introducir Apellido Materno del cónyuge" ForeColor="Red" Enabled="false"/>
                                        </div>
                                </div>--%>
                                <div class="form-group row">
                                    <label for="ddlTipoDocCony" class="col-sm-2 form-control-label small">Tipo de Documento</label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList ID="ddlTipoDocCony" runat="server" CssClass="form-control" Enabled="false">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtNumDocCony" class="col-sm-2 form-control-label small">N&uacute;mero de Documento</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtNumDocCony" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="8" Enabled="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvNumDocCony" ControlToValidate="txtNumDocCony" ErrorMessage="Favor de introducir el Número de documento del cónyuge" ForeColor="Red" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                                
                        </div>
                        <!--izq-->
                        <div class="der">
                            <div class="form-group">
                                <div class="form-group row" style="display: none;">
                                    <label for="txtDireccionCony" class="col-sm-2 form-control-label small">Direcci&oacute;n</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtDireccionCony" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="200" TextMode="MultiLine" Enabled="false" Rows="6"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvDireccionCony" ControlToValidate="txtDireccionCony" ErrorMessage="Favor de introducir la dirección del cónyuge" ForeColor="Red" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtTelefonoCony" class="col-sm-2 form-control-label small">Tel&eacute;fono</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtTelefonoCony" runat="server" CssClass="form-control" MaxLength="9" Enabled="false"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="revTelefonoCony" runat="server"
                                            ErrorMessage="El teléfono debe de ser de 8-9 digitos"
                                            ControlToValidate="txtTelefonoCony"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{8,9}$" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--der-->
                     </asp:Panel>
                    </div>
                    <div class="panel-body" style="display: none;">
                        <div class="izq">
                            <div class="form-group">
                                <!--CONYUGE LABORALES-->
                                <div class="form-group row">
                                    <label for="txtEmpresaCony" class="col-sm-2 form-control-label small">Raz&oacute;n Social de la Empresa</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtEmpresaCony" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="150" Enabled="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvEmpresaCony" ControlToValidate="txtEmpresaCony" ErrorMessage="Favor de introducir Empresa del cónyuge" ForeColor="Red" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtRUCCony" class="col-sm-2 form-control-label small">RUC de la Empresa</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtRUCCony" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="11" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtPuestoCony" class="col-sm-2 form-control-label small">Puesto</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtPuestoCony" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="100" Enabled="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvPuestoCony" ControlToValidate="txtPuestoCony" ErrorMessage="Favor de introducir Puesto del cónyuge" ForeColor="Red" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtAntiguedadCony" class="col-sm-2 form-control-label small">Antiguedad Laboral</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtAntiguedadCony" runat="server" CssClass="form-control" Text="0" MaxLength="2" Enabled="false"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="revAntiguedadCony" runat="server"
                                            ErrorMessage="La antiguedad debe ser un número entero entre 0-99"
                                            ControlToValidate="txtAntiguedadCony"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,2}$" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtIngCony" class="col-sm-2 form-control-label small">Ingreso Neto Mensual</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtIngCony" runat="server" CssClass="form-control" MaxLength="10" Enabled="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvIngCony" ControlToValidate="txtIngCony" ErrorMessage="El Ingreso del Cónyuge es Obligatorio" ForeColor="Red" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="revIngCony" runat="server"
                                            ErrorMessage="Formato Incorrecto en ingreso de cónyuge"
                                            ControlToValidate="txtIngCony"
                                            ForeColor="Red"
                                            ValidationExpression="^(\d+\.\d+)|(\d+\.)|(\.\d+)|(\d+)$" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="der">
                            <div class="form-group">
                                <div class="form-group row">
                                    <label for="txtDireccionLaboralCony" class="col-sm-2 form-control-label small">Direcci&oacute;n de la empresa</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtDireccionLaboralCony" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="200" TextMode="MultiLine" Enabled="false" Rows="6"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="rfvDireccionLaboralCony" ControlToValidate="txtDireccionLaboralCony" ErrorMessage="Favor de introducir la dirección laboral del cónyuge" ForeColor="Red" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="txtTelefonoLaboralCony" class="col-sm-2 form-control-label small">Tel&eacute;fono Empresa</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtTelefonoLaboralCony" runat="server" CssClass="form-control" MaxLength="8" Enabled="false"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="revTelefonoLaboralCony" runat="server"
                                            ErrorMessage="El teléfono deber ser 7 u 8 digitos"
                                            ControlToValidate="txtTelefonoLaboralCony"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{7,8}$" Enabled="false" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <!--DATOS DEL CONYUGE-->
        <h3 class="headerAcc">DATOS LABORALES
        <asp:UpdatePanel ID="upValDatosLaborales" UpdateMode="Conditional" RenderMode="Inline" runat="server">
            <ContentTemplate>
                <span id="valIconDatosLaborales" class="glyphicon glyphicon-exclamation-sign blink" visible="false" runat="server"></span>
            </ContentTemplate>
        </asp:UpdatePanel>
        </h3>
        <div>
            <div class="panel-body">
                <asp:UpdateProgress AssociatedUpdatePanelID="upDatosLaborales" DisplayAfter="100" runat="server">
                    <ProgressTemplate>
                        <div id="loader-background"></div>
                        <div id="loader-content"></div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:UpdatePanel ID="upDatosLaborales" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="izq">

                            <asp:Panel runat="server" ID="divTipoEmp" CssClass="form-group" Visible="false">
                                <label for="cboTipoEmp" class="col-sm-2 form-control-label small">Tipo de Empresa</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="cboTipoEmp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="cboTipoEmp_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </div>
                             </asp:Panel>

                            <div class="form-group clearfix">
                                <div class="form-group col-md-12">
                                    <asp:Label runat="server" ID="lblEmpresaAnterior" CssClass="small" Visible="false" ForeColor="#1E90FF" />
                                    <br /><label for="ddlEmpresa" class="form-control-label small">Empresa</label>
                                    <asp:Label runat="server" ID="lblEmpresaSinConfiguracion" CssClass="small" Visible="false" ForeColor="#FF0000" />
                                    <asp:RequiredFieldValidator runat="server" ID="rfvddlEmpresa" ControlToValidate="ddlEmpresa" ErrorMessage="Requerido" ForeColor="Red" ValidationGroup="DatosLaborales" Display="Dynamic" CssClass="small" InitialValue="NA" />
                                    <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="form-control input-sm" AutoPostBack="true" OnSelectedIndexChanged="ddlEmpresa_SelectedIndexChanged" />
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <div class="form-group col-md-6">
                                    <label for="ddlOrgano" class="form-control-label small">Órgano de Pago</label>
                                    <div>
                                        <asp:DropDownList ID="ddlOrgano" runat="server" CssClass="form-control" AutoPostBack="true" Enabled="false" />
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="txtNivelRiesgo" class="form-control-label small">Nivel Riesgo</label>
                                    <div>
                                        <asp:TextBox ID="txtNivelRiesgo" runat="server" CssClass="form-control" Text="N/A" Enabled="false" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <div class="form-group row">
                                    <label for="ddlSituacionLaboral" class="col-sm-2 form-control-label small">Situaci&oacute;n Laboral</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSituacionLaboral" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSituacionLaboral_SelectedIndexChange" AutoPostBack="true" Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                    <asp:Panel runat="server" ID="divTipoPension" CssClass="form-group" Visible="true">
                                        <label for="cboTipoPen" class="col-sm-2 form-control-label small">R&eacute;gimen de Pensión</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="cboTipoPen" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </asp:Panel>

                                     <asp:Panel runat="server" ID="divTipoCon" CssClass="form-group" Visible="false">
                                        <label for="cboTipoCon" class="col-sm-2 form-control-label small">Tipo de Contrato</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="cboTipoCon" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </asp:Panel>

                                      <asp:Panel runat="server" ID="divTipoInd" CssClass="form-group" Visible="false">
                                        <label for="cboTipoInd" class="col-sm-2 form-control-label small">Tipo de Negocio</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="cboTipoInd" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </asp:Panel>

                                </div>
                            </div>

                            <asp:Panel runat="server" ID="pnlDireccionLaboral" CssClass="form-group" Visible="true">
                                <div class="form-group">
                                    <div class="form-group col-md-4">
                                        <label for="cboEstadoEmp" class="form-control-label small">Departamento</label>
                                        <div>
                                            <asp:DropDownList ID="cboEstadoEmp" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboEstadoEmp_SelectedIndexChange" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="cboMunicipioEmp" class="form-control-label small">Provincia</label>
                                        <div>
                                            <asp:DropDownList ID="cboMunicipioEmp" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboMunicipioEmp_SelectedIndexChange" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="cboColoniaEmp" class="form-control-label small">Distrito</label>
                                        <div>
                                            <asp:DropDownList ID="cboColoniaEmp" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-group col-md-6">
                                        <label for="txtCalleEmp" class="form-control-label small">Calle</label>
                                        <div>
                                            <asp:TextBox ID="txtCalleEmp" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="70"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="txtNumIntEmp" class="form-control-label small">Num Interior</label>
                                        <div>
                                            <asp:TextBox ID="txtNumIntEmp" runat="server" CssClass="form-control" Text="0" MaxLength="20"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="txtNumExtEmp" class="form-control-label small">Num Exterior</label>
                                        <div>
                                            <asp:TextBox ID="txtNumExtEmp" runat="server" CssClass="form-control" Text="0" MaxLength="20"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-group col-md-6">
                                        <label for="txtDependencia" class="form-control-label small">Dependencia</label>
                                        <div>
                                            <asp:TextBox ID="txtDependenciaEmp" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="txtUbicacion" class="form-control-label small">Ubicacion</label>
                                        <div>
                                            <asp:TextBox ID="txtUbicacionEmp" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="100"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <div class="form-group col-md-3">
                                        <label for="txtLadaEmp" class="form-control-label small">Código LDN</label>
                                        <div>
                                            <asp:TextBox ID="txtLadaEmp" runat="server" onKeyPress="return EvaluateText('%d', this);" CssClass="form-control" Enabled="false" MaxLength="2"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="txtTelEmp" class="form-control-label small">Tel&eacute;fono</label>
                                        <div>
                                            <asp:TextBox ID="txtTelEmp" runat="server" onKeyPress="return EvaluateText('%d', this);" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="revTelEmp" runat="server"
                                                ErrorMessage="El telefono debe ser de 6 dígitos"
                                                ControlToValidate="txtTelEmp"
                                                ForeColor="Red"
                                                ValidationExpression="^[0-9]{6}$" ValidationGroup="DatosLaborales" CssClass="small" Display="Dynamic" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="txtTelEmp" class="form-control-label small">Anexo</label>
                                        <div>
                                            <asp:TextBox ID="txtAnexo" runat="server" onKeyPress="return EvaluateText('%d', this);" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <hr />

                            <div class="form-group clearfix">
                                <div class="form-group col-md-3">
                                    <label for="txtPuesto" class="form-control-label small">Puesto</label>
                                    <div>
                                        <asp:TextBox ID="txtPuesto" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator14" ControlToValidate="txtPuesto" ErrorMessage="Favor de introducir Puesto del cliente" ForeColor="Red" ValidationGroup="DatosLaborales" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="txtAntiguedad" class="form-control-label small">Antiguedad Laboral</label>
                                    <div>
                                        <asp:TextBox ID="txtAntiguedad" runat="server" CssClass="form-control" Text="0" MaxLength="2"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                                            ErrorMessage="Capture un número entero entre 0-99"
                                            ControlToValidate="txtAntiguedad"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,2}$" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>                                
                                <asp:Label ID="lblIdEmpresaCurrent" Visible="false" runat="server"></asp:Label>
                                <asp:Panel ID="pnlUsuarioPlanillaVirtual" class="form-group col-md-3" Visible="false" runat="server">
                                    <strong><asp:Label ID="lblUsuarioPlanillaVirtual" CssClass="form-control-label small" runat="server">PV Usuario</asp:Label></strong>
                                    <div>
                                        <asp:TextBox ID="txtPlanillaVirtualUsuario" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator runat="server" ID="rfv_txtPlanillaVirtualUsuario" ControlToValidate="txtPlanillaVirtualUsuario" ErrorMessage="PV Usuario es Requerido" ForeColor="Red" ValidationGroup="DatosLaborales" CssClass="small" Display="Dynamic" />
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlClavePlanillaVirtual" class="form-group col-md-3" Visible="false" runat="server">
                                    <strong><asp:Label ID="lblClavePlanillaVirtual" CssClass="form-control-label small" runat="server">PV Clave</asp:Label></strong>
                                    <div>
                                        <asp:TextBox ID="txtPlanillaVirtualClave" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator runat="server" ID="rfv_txtPlanillaVirtualClave" ControlToValidate="txtPlanillaVirtualClave" ErrorMessage="PV Clave es Requerido" ForeColor="Red" ValidationGroup="DatosLaborales" CssClass="small" Display="Dynamic" />
                                    </div>
                                </asp:Panel>                                
                            </div>

                            <div class="form-group clearfix">
                                <div class="form-group col-md-3">
                                    <label for="txtIngresoBruto" class="form-control-label small">Ingreso Bruto Mensual</label>
                                    <div>
                                        <asp:TextBox ID="txtIngresoBruto" runat="server" CssClass="form-control" onKeyPress="return EvaluateText('%f', this);" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtIngresoBruto" ErrorMessage="Capture el Ingreso Bruto Mensual" ForeColor="Red" ValidationGroup="DatosLaborales" CssClass="small" Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtIngresoBruto"
                                            ForeColor="Red"
                                            ValidationExpression="^(\d+\.\d+)|(\d+\.)|(\.\d+)|(\d+)$" ValidationGroup="DatosLaborales" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="txtOtrIng" class="form-control-label small">Descuentos de Ley</label>
                                    <div>
                                        <asp:TextBox ID="txtDescuentosLey" runat="server" CssClass="form-control" onKeyPress="return EvaluateText('%f', this);" Text="0" MaxLength="10"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtDescuentosLey"
                                            ForeColor="Red"
                                            ValidationExpression="^(\d+\.\d+)|(\d+\.)|(\.\d+)|(\d+)$" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="txtOtrIng" class="form-control-label small">Otros descuentos</label>
                                    <div>
                                        <asp:TextBox ID="txtOtrosDescuentos" runat="server" CssClass="form-control" onKeyPress="return EvaluateText('%f', this);" Text="0" MaxLength="10"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtOtrosDescuentos"
                                            ForeColor="Red"
                                            ValidationExpression="^(\d+\.\d+)|(\d+\.)|(\.\d+)|(\d+)$" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="txtIng" class="form-control-label small">Ingreso Neto Mensual</label>
                                    <div>
                                        <asp:TextBox ID="txtIng" runat="server" CssClass="form-control" onKeyPress="return EvaluateText('%f', this);" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtIng" ErrorMessage="Capture el Ingreso Neto Mensual" ForeColor="Red" ValidationGroup="DatosLaborales" CssClass="small" Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtIng"
                                            ForeColor="Red"
                                            ValidationExpression="^(\d+\.\d+)|(\d+\.)|(\.\d+)|(\d+)$" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                            </div>
                            <!--form-group-->
                            <div class="form-group clearfix">
                                <div class="form-group col-md-3">
                                    <label for="txtOtrIng" class="form-control-label small">Otros Ingresos</label>
                                    <div>
                                        <asp:TextBox ID="txtOtrIng" runat="server" CssClass="form-control" onKeyPress="return EvaluateText('%f', this);" Text="0" MaxLength="10"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                            ErrorMessage="Formato Incorrecto"
                                            ControlToValidate="txtOtrIng"
                                            ForeColor="Red"
                                            ValidationExpression="^(\d+\.\d+)|(\d+\.)|(\.\d+)|(\d+)$" Display="Dynamic" CssClass="small" />
                                    </div>
                                </div>
                                <div class="form-group col-md-9">
                                    <label for="txtFuente" class="form-control-label small">Procedencia de Otros Ingresos</label>
                                    <div>
                                        <asp:TextBox ID="txtFuente" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="35"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--izq-->
                        <div class="der">
                            <hr />
                            <div class="form-group">
                                <asp:UpdatePanel runat="server" ID="udpPEP" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group row">
                                            <label for="rdPEP" class="col-sm-2 form-control-label small">PEP</label>
                                            <div class="col-sm-10">
                                                <asp:RadioButtonList ID="rdoPEP" runat="server" RepeatDirection="Horizontal" CssClass="small" AutoPostBack="true" OnSelectedIndexChanged="rdoPEP_SelectedIndexChange">
                                                    <asp:ListItem Value="1" Selected="False">&nbsp;SI&nbsp;</asp:ListItem>
                                                    <asp:ListItem Value="2" Selected="True">&nbsp;NO&nbsp;</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <asp:Panel runat="server" ID="divPEP" CssClass="form-group" Visible="false">
                                            <div class="form-group col-md-5">
                                                <label for="txtNomPEP" class="form-control-label small">Nombre</label>
                                                <div>
                                                    <asp:TextBox ID="txtNomPEP" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <label for="txtPuePEP" class="form-control-label small">Puesto</label>
                                                <div>
                                                    <asp:TextBox ID="txtPuePEP" runat="server" CssClass="form-control" Style="text-transform: uppercase;"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <label for="cboParPep" class="form-control-label small">Parentesco</label>
                                                <div>
                                                    <asp:DropDownList ID="cboParPep" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <!--form-group-->
                        </div>
                        <!--der-->
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <!--DATOS LABORALES-->
        <h3 class="headerAcc">REFERENCIAS PERSONALES O FAMILIARES               
        <asp:UpdatePanel ID="upValReferencias" UpdateMode="Conditional" RenderMode="Inline" runat="server">
            <ContentTemplate>
                <span id="valIconReferencias" class="glyphicon glyphicon-exclamation-sign blink" visible="false" runat="server"></span>
            </ContentTemplate>
        </asp:UpdatePanel>
        </h3>
        <div>
            <asp:UpdateProgress AssociatedUpdatePanelID="upReferenciasPersonales" DisplayAfter="100" runat="server">
                <ProgressTemplate>
                    <div id="loader-background"></div>
                    <div id="loader-content"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upReferenciasPersonales" runat="server">
                <ContentTemplate>
                    <div class="panel-body">
                        <div class="izq">
                            <div class="form-group">
                                <div class="form-group col-md-12">
                                    <asp:HiddenField ID="hfIdReferencia" runat="server" Value="0" />
                                    <label for="txtNombreRef1" class="form-control-label small">Nombre(s)</label>
                                    <div>
                                        <asp:TextBox ID="txtNombreRef1" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLengt="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtNombreRef1" ErrorMessage="Capture el Nombre de la referencia" ForeColor="Red" ValidationGroup="ValReferencias" Display="Dynamic" CssClass="small" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="txtApellidoPaternoRef1" class="form-control-label small">Apellido Paterno</label>
                                    <div>
                                        <asp:TextBox ID="txtApellidoPaternoRef1" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLengt="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="txtApellidoPaternoRef1" ErrorMessage="Capture el apellido paterno de la referencia" ForeColor="Red" ValidationGroup="ValReferencias" Display="Dynamic" CssClass="small" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="txtApellidoMaternoRef1" class="form-control-label small">Apellido Materno</label>
                                    <div>
                                        <asp:TextBox ID="txtApellidoMaternoRef1" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLengt="30"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cboParRef1" class="form-control-label small">Parentesco</label>
                                    <div>
                                        <asp:DropDownList ID="cboParRef1" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="display: none;">
                                <div class="form-group col-md-6">
                                    <label for="txtCalleRef1" class="form-control-label small">Nombre de Vía</label>
                                    <div>
                                        <asp:TextBox ID="txtCalleRef1" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator17" ControlToValidate="txtCalleRef1" ErrorMessage="Capture la calle" ForeColor="Red" ValidationGroup="ValReferencias" Display="Dynamic" CssClass="small" />--%>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="txtNumExteriorRef1" class="form-control-label small">Numero exterior</label>
                                    <div>
                                        <asp:TextBox ID="txtNumExteriorRef1" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator18" ControlToValidate="txtNumExteriorRef1" ErrorMessage="Capture el numero exterior" ForeColor="Red" ValidationGroup="ValReferencias" Display="Dynamic" CssClass="small" />--%>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="txtNumInteriorRef1" class="form-control-label small">Numero interior</label>
                                    <div>
                                        <asp:TextBox ID="txtNumInteriorRef1" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cboEstadoRef1" class="form-control-label small">Departamento</label>
                                    <asp:DropDownList ID="cboEstadoRef1" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboEstadoRef1_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="dboMunicipioRef1" class="form-control-label small">Provincia</label>
                                    <asp:DropDownList ID="dboMunicipioRef1" runat="server" CssClass="form-control" OnSelectedIndexChanged="dboMunicipioRef1_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cboColoniaRef1" class="form-control-label small">Distrito</label>
                                    <asp:DropDownList ID="cboColoniaRef1" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <div class="form-group col-md-2" style="display: none;">
                                    <label for="txtLadaTelRef1" class="form-control-label small">Lada Telefono</label>
                                    <div>
                                        <asp:TextBox ID="txtLadaTelRef1" runat="server" CssClass="form-control" MaxLength="3"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="txtTelRef1" class="form-control-label small">Tel&eacute;fono</label>
                                    <div>
                                        <asp:TextBox ID="txtTelRef1" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                            ErrorMessage="El teléfono deber ser 7 u 8 digitos"
                                            ControlToValidate="txtTelRef1"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{7,8}$" ValidationGroup="ValReferencias" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>
                                <div class="form-group col-md-2" style="display: none;">
                                    <label for="txtLadaCelRef1" class="form-control-label small">Lada Celular</label>
                                    <div>
                                        <asp:TextBox ID="txtLadaCelRef1" runat="server" CssClass="form-control" MaxLength="3"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="txtTelCelRef" class="form-control-label small">Celular</label>
                                    <div>
                                        <asp:TextBox ID="txtTelCelRef" runat="server" CssClass="form-control" MaxLength="9"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="rev1" runat="server"
                                            ErrorMessage="El celular debe de ser de 9 dígitos"
                                            ControlToValidate="txtTelCelRef"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{9}$" ValidationGroup="ValReferencias" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12 text-right">
                                        <asp:Button ID="Button2" runat="server" Text="Agregar" CssClass="btn btn-xs btn-success" OnClick="btnAgregarReferencia" AutoPostBack="true" ValidationGroup="ValReferencias" />
                                    </div>
                                    <asp:Panel ID="pnlErrorReferencias" class="col-sm-12" Visible="false" runat="server">
                                        <div class="alert alert-danger" style="padding: 5px 15px 5px 15px; margin: 10px 0px;">
                                            <asp:Label ID="lblErrorReferencias" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <!--form-group-->
                        </div>
                        <!--izq-->
                        <div class="panel-footer">
                            <div class="GridContenedor" style="height: 250px">
                                <asp:GridView ID="gvreferencias" runat="server" Width="100%" AutoGenerateColumns="False"
                                    HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                    CssClass="table table-bordered table-striped table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                    PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false" OnRowCommand="gvreferencias_RowCommand"
                                    DataKeyNames="nombres">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Editar" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEditar" class="btn btn-xs btn-default" title="Editar" role="button" runat="server" CausesValidation="False" CommandName="Editar" CommandArgument='<%#Container.DataItemIndex%>'>                                        
                                        <span class="glyphicon glyphicon-pencil"></span>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Eliminar" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEliminar" class="btn btn-xs btn-default" title="Eliminar" role="button" runat="server" CausesValidation="False" CommandName="Eliminar" CommandArgument='<%#Container.DataItemIndex%>'>                                        
                                        <span class="glyphicon glyphicon-minus-sign"></span>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Num" DataField="IdReferencia" ItemStyle-CssClass="small" />
                                        <asp:TemplateField HeaderText="Referencia">
                                            <ItemTemplate>
                                                <%# string.Format("<span>{0} {1} {2} <small>({3})</small></span>", Eval("Nombres"), Eval("ApellidoPaterno"),Eval("ApellidoMaterno"), Eval("Parentesco")).Replace("()", "").Replace("  ", " ").Trim()%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Teléfono" DataField="Telefono" ItemStyle-CssClass="small" />
                                        <asp:BoundField HeaderText="Celular" DataField="TelefonoCelular" ItemStyle-CssClass="small" />
                                    </Columns>
                                    <EmptyDataTemplate>No se han agregado referencias</EmptyDataTemplate>
                                </asp:GridView>
                                <asp:CustomValidator ID="cvReferencias" runat="server" ErrorMessage="Debe capturar al menos 2 referencias" ValidationGroup="Referencias" CssClass="small" ForeColor="Red" Display="Dynamic"
                                    EnableClientScript="False"></asp:CustomValidator>
                            </div>
                        </div>
                    </div>
                    <!--panel-body-->
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <!--REFERENCIAS PERSONALES O FAMILIARES-->
        <h3 class="headerAcc">DATOS DEL NEGOCIO
           <asp:UpdatePanel ID="upValDatosNegocio" UpdateMode="Conditional" RenderMode="Inline" runat="server">
              <ContentTemplate>
                  <span id="valIconDatosNegocio" class="glyphicon glyphicon-exclamation-sign blink" visible="false" runat="server"></span>
              </ContentTemplate>
           </asp:UpdatePanel>
        </h3>

     <div>
	    <div class="panel-body">
		    <asp:UpdateProgress AssociatedUpdatePanelID="upDatosNegocio" DisplayAfter="100" runat="server">
			    <ProgressTemplate>
				    <div id="loader-background"></div>
				    <div id="loader-content"></div>
			    </ProgressTemplate>
		    </asp:UpdateProgress>
		    <asp:UpdatePanel ID="upDatosNegocio" UpdateMode="Conditional" runat="server">

			    <ContentTemplate>
                      
				    <div class="izq" id="PanelDNegocio">
                     
                        <div class="form-group clearfix">
						    <div class="form-group col-md-12">
							    <label for="txtDNCliente" class="form-control-label small">Cliente</label>
							    <div>
								     <asp:TextBox ID="txtDNCliente" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="100"></asp:TextBox>							
							        <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rvdDNCliente" ControlToValidate="txtDNCliente" ErrorMessage="Ingrese nombre del Cliente" ValidationGroup="DatosNegocio" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                 </div>
						    </div>		
                          
                               </div>
                        <div class="form-group clearfix">
                                <div class="form-group col-md-6">
                                    <label for="txtDNRazon" class="form-control-label small">Razon Social/Nombre Comercial</label>
                                    <div>
                                        <asp:TextBox ID="txtDNRazon" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="100"></asp:TextBox>							
                                        <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rvdDNRazon" ControlToValidate="txtDNRazon" ErrorMessage="Ingrese Razon Social/Nombre Comercial" ValidationGroup="DatosNegocio" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                   </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="txtDNTipPer" class="form-control-label small">Tipo de personeria</label>
                                    <div>
                                        <asp:DropDownList runat="server" ID="ddlDNTipPer" CssClass="form-control" AutoPostBack="true">
	                                         <asp:ListItem Text="NATURAL" Value="0"></asp:ListItem>
	                                         <asp:ListItem Text="JURIDICA" Value="1"></asp:ListItem>	
                                       </asp:DropDownList>
                                    </div>
                                </div>
                        </div>
                        <hr />
                        <div class="form-group clearfix">
                                <div class="form-group col-md-6">
                                    <label for="txtDNTipSoc" class="form-control-label small">Tipo Sociedad</label>
                                    <div>                                        
                                        <asp:DropDownList runat="server" ID="ddlDNTipSoc" CssClass="form-control" AutoPostBack="true">
	                                         <asp:ListItem Text="NINGUNA" Value="NINGUNA"></asp:ListItem>
	                                         <asp:ListItem Text="EIRL" Value="EIRL"></asp:ListItem>	
                                             <asp:ListItem Text="SRL" Value="SRL"></asp:ListItem>	
                                             <asp:ListItem Text="SAC" Value="SAC"></asp:ListItem>	
                                       </asp:DropDownList>                                   
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="txtDNTipNeg" class="form-control-label small">Tipo Negocio</label>
                                    <div>
                                        <asp:DropDownList runat="server" ID="ddlDNTipNeg" CssClass="form-control" AutoPostBack="true">
	                                         <asp:ListItem Text="FORMAL" Value="0"></asp:ListItem>
	                                         <asp:ListItem Text="INFORMAL" Value="1"></asp:ListItem>	
                                       </asp:DropDownList>
                                    </div>
                                </div>
                        </div>
                        <div class="form-group clearfix">
                                <div class="form-group col-md-6">
                                    <label for="txtDNRegTri" class="form-control-label small">Regimen Tributario</label>
                                    <div>
                                        <asp:DropDownList runat="server" ID="ddlDNRegTri" CssClass="form-control" AutoPostBack="true">
	                                         <asp:ListItem Text="NUEVO RÉGIMEN ÚNICO SIMPLIFICADO" Value="0"></asp:ListItem>
	                                         <asp:ListItem Text="RÉGIMEN ESPECIAL DE RENTA" Value="1"></asp:ListItem>	
                                             <asp:ListItem Text="RÉGIMEN MYPE TRIBUTARIO" Value="2"></asp:ListItem>	
                                             <asp:ListItem Text="RÉGIMEN GENERAL DE RENTA" Value="3"></asp:ListItem>	
                                       </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="txtDNRuc" class="form-control-label small">RUC</label>
                                    <div>                                        
                                        <asp:TextBox ID="txtDNRuc" runat="server" CssClass="form-control"  MaxLength="11"></asp:TextBox>	
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="txtDNRuc"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{0,11}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
                                    </div>
                                </div>                                
                        </div>
                        <div class="form-group clearfix">
                                <div class="form-group col-md-6">
                                    <label for="txtDNSector" class="form-control-label small">Sector</label>
                                    <div>
                                        <asp:DropDownList runat="server" ID="ddlDNSector" CssClass="form-control" OnSelectedIndexChanged="ddlDNSector_SelectedIndexChange" AutoPostBack="true">
	                                         <asp:ListItem Text="COMERCIO" Value="0"></asp:ListItem>
	                                         <asp:ListItem Text="PRODUCCIÓN" Value="1"></asp:ListItem>	
                                             <asp:ListItem Text="SERVICIOS" Value="2"></asp:ListItem>	                                           	
                                       </asp:DropDownList>
                                    </div>
                                </div>
                               <div class="form-group col-md-6">
                                    <label for="txtDNActividad" class="form-control-label small">Actividad</label>
                                    <div>
                                        <asp:DropDownList runat="server" ID="ddlDNActividad" CssClass="form-control" AutoPostBack="true">	                                                      	
                                       </asp:DropDownList>
                                    </div>
                                </div>                                
                        </div>
                        <div class="form-group row">
                             <div class="form-group col-md-4">
                                 <label for="lblDNMAGir" class="form-control-label small"></label>
                                 <div>
                                    <label for="lblDNMAGiro" class="form-control-label small">Mes/Año Inicio. Experiencia en el Giro</label>
                                 </div>
                             </div>
                             <div class="form-group col-md-4">
                                <label for="" class="form-control-label small">Mes</label>                                                                                                 
                                <div>                                    
                                    <asp:DropDownList ID="ddlDMGMes" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="1">Enero</asp:ListItem>
                                        <asp:ListItem Value="2">Febrero</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Abril</asp:ListItem>
                                        <asp:ListItem Value="5">Mayo</asp:ListItem>
                                        <asp:ListItem Value="6">Junio</asp:ListItem>
                                        <asp:ListItem Value="7">Julio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                        <asp:ListItem Value="10">Octubre</asp:ListItem>
                                        <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                        <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="" class="form-control-label small">Año</label>                                                                                                 
                                <div>                                    
                                     <asp:TextBox ID="txtDNGAno" runat="server" CssClass="form-control" Text="0" MaxLength="4"></asp:TextBox>
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="txtDNGAno"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,4}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
                                </div>
                            </div>
                        </div>
                         <div class="form-group row">
                             <div class="form-group col-md-4">
                                 <label for="lblDNMAAct" class="form-control-label small"></label>
                                 <div>
                                    <label for="lblDNMAAct" class="form-control-label small">Mes/Año Inicio Actividades (Según RUC)</label>
                                 </div>
                             </div>
                             <div class="form-group col-md-4">
                                <label for="" class="form-control-label small">Mes</label>                                                                                                 
                                <div>                                    
                                    <asp:DropDownList ID="ddlDMAMes" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="1">Enero</asp:ListItem>
                                        <asp:ListItem Value="2">Febrero</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Abril</asp:ListItem>
                                        <asp:ListItem Value="5">Mayo</asp:ListItem>
                                        <asp:ListItem Value="6">Junio</asp:ListItem>
                                        <asp:ListItem Value="7">Julio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                        <asp:ListItem Value="10">Octubre</asp:ListItem>
                                        <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                        <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="" class="form-control-label small">Año</label>                                                                                                 
                                <div>                                    
                                     <asp:TextBox ID="txtDNAAno" runat="server" CssClass="form-control" Text="0"  MaxLength="4"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="txtDNAAno"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,4}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                             <label for="ddlDNNumEmp" class="col-sm-2 form-control-label small">Numero de Empleados</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtDNNumEmp" runat="server" CssClass="form-control" Text="0"  MaxLength="3"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="txtDNNumEmp"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,3}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
                            </div>
                            <label for="ddlDNNumSuc" class="col-sm-2 form-control-label small">Numero Sucursales-Puntos de venta</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtDNNumSuc" runat="server" CssClass="form-control" Text="0" MaxLength="2"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="txtDNNumSuc"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,2}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
                            </div>
                        </div>
                         <hr />
                         <div class="form-group row">   
                             
                             <div class="form-group col-md-3">
                                 <label for="lblDNCue" class="form-control-label small"></label>
                                 <div>
                                    <label for="lblDNCue" class="form-control-label small">Domicilio del Negocio</label>
                                 </div>
                             </div>
                             
					    </div>
                         <div class="form-group row">   
                             <div class="form-group col-md-4">
							        <label for="txtDNDireccion" class="form-control-label small">Dirección</label>
							         <div>                                    
                                         <asp:DropDownList ID="ddlDNDireccion" runat="server" CssClass="form-control" AutoPostBack="true">
                                         </asp:DropDownList>
                                    </div>
						        </div>	
                                <div class="form-group col-md-5">
							        <label for="txtDNVia" class="form-control-label small">Nombre Via</label>
							        <div>
								        <asp:TextBox ID="txtDNVia" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="40"></asp:TextBox>							
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvDNVia" ControlToValidate="txtDNVia" ErrorMessage="Ingrese Nombre Via del Negocio" ValidationGroup="DatosNegocio" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                    </div>
						        </div>	
                                <div class="form-group col-md-2">
							        <label for="txtDNNro" class="form-control-label small">Nro</label>
							        <div>
								        <asp:TextBox ID="txtDNNro" runat="server" CssClass="form-control" Text="0" MaxLength="6" ></asp:TextBox>							
							          <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="txtDNNro"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,6}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
                                   </div>
						        </div>
                                <div class="form-group col-md-1">
							        <label for="txtDNLote" class="form-control-label small">Lote</label>
							        <div>
								        <asp:TextBox ID="txtDNLote" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="2"></asp:TextBox>							
							        </div>
						        </div>
					    </div>
                         <div class="form-group row">   
                             
                             <div class="form-group col-md-1">
                                 <div>
                                    <label for="lblDNManzana" class="form-control-label small">Manzana</label>
                                 </div>
                             </div>
						        <div class="form-group col-md-2">
							        <div>
								        <asp:TextBox ID="TxtDNManzana" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="1" ></asp:TextBox>							
							        </div>
						        </div>
                              <div class="form-group col-md-1">
                                
                             </div>
                             <div class="form-group col-md-1">
                                 <div>
                                    <label for="lblDNInterior" class="form-control-label small">Interior</label>
                                 </div>
                             </div>
                                <div class="form-group col-md-3">
							        <div>
								        <asp:TextBox ID="TxtDNInterior" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="30"></asp:TextBox>							
							        </div>
						        </div>	
                               <div class="form-group col-md-1">
                                
                             </div>
                             <div class="form-group col-md-1">
                                 <div>
                                    <label for="lblDNPiso" class="form-control-label small">Piso</label>
                                 </div>
                             </div>
                                <div class="form-group col-md-2">
							        <div>
								        <asp:TextBox ID="TxtDNPiso" runat="server" CssClass="form-control" Text="0"  MaxLength="2" ></asp:TextBox>	
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="TxtDNPiso"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,2}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
							        </div>
						        </div>
					    </div>
                         <div class="form-group row">   
                             
                             <div class="form-group col-md-2">
                                 <label for="lblDNZona" class="form-control-label small"></label>
                                 <div>
                                    <label for="lblDNZona" class="form-control-label small">Urb.Zona</label>
                                 </div>
                             </div>
						        <div class="form-group col-md-10">
							        <div>
								        <asp:TextBox ID="TxtZona" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="50" ></asp:TextBox>							
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvZona" ControlToValidate="TxtZona" ErrorMessage="Ingrese Urb.Zona del Negocio" ValidationGroup="DatosNegocio" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                    </div>
						        </div>
                             
					    </div>
                        <div class="form-group row">   
						        <div class="form-group col-md-4">
							        <label for="ddlDNDepartamento" class="form-control-label small">Departamento</label>
							         <div>                                    
                                       <asp:DropDownList ID="ddlDNDepartamento" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlDNDepartamento_SelectedIndexChange" AutoPostBack="true">
                                        </asp:DropDownList>
                                         <asp:CompareValidator ID="rfvddlDNDepartamento" runat="server" ControlToValidate="ddlDNDepartamento" ValueToCompare="0" Operator="NotEqual" Type="Integer" ErrorMessage="Favor seleccione Departamento" ForeColor="Red" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
                                    </div>
						        </div>	
                                <div class="form-group col-md-4">
							        <label for="ddlDNProvincia" class="form-control-label small">Provincia</label>
							         <div>                                    
                                        <asp:DropDownList ID="ddlDNProvincia" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlDNProvincia_SelectedIndexChange" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:CompareValidator ID="rfvddlDNProvincia" runat="server" ControlToValidate="ddlDNProvincia" ValueToCompare="0" Operator="NotEqual" Type="Integer" ErrorMessage="Favor seleccione Provincia" ForeColor="Red" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
                                   
                                </div>
						        </div>	
                                <div class="form-group col-md-4">
							        <label for="ddlDNDistrito" class="form-control-label small">Distrito</label>
							       <div>                                    
                                      <asp:DropDownList ID="ddlDNDistrito" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                       <asp:CompareValidator ID="rfvddlDNDistrito" runat="server" ControlToValidate="ddlDNDistrito" ValueToCompare="0" Operator="NotEqual" Type="Integer" ErrorMessage="Favor seleccione Distrito" ForeColor="Red" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
                                   
                                </div>
						        </div>
					    </div>                         
                         <div class="form-group row">   
						        <div class="form-group col-md-12">
							        <label for="txtDNReferencia" class="form-control-label small">Referencia</label>
							        <div>
								          <asp:TextBox ID="TxtDNReferencia" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="100"></asp:TextBox>							
							                <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvDNReferencia" ControlToValidate="TxtDNReferencia" ErrorMessage="Ingrese Referencia del Negocio" ValidationGroup="DatosNegocio" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                     </div>
						        </div>	
                                
					    </div>
                        <hr />
                         <div class="form-group row">   
                             <div class="form-group col-md-6">
							        <label for="txtDNTipPropiedad" class="form-control-label small">Tipo de Propiedad Local</label>
							         <div>                                    
                                        <asp:DropDownList ID="ddlDNTipPropiedad" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1">Propia</asp:ListItem>
                                            <asp:ListItem Value="2">Alquilada</asp:ListItem>
                                             <asp:ListItem Value="3">Ambulante</asp:ListItem>
                                            <asp:ListItem Value="4">Familiares</asp:ListItem>
                                            <asp:ListItem Value="5">Propia Pagandose</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
						        </div>	
                                <div class="form-group col-md-6">
							        <label for="txtDNAnioOcupacion" class="form-control-label small">Año de Ocupacion</label>
							        <div>
								        <asp:TextBox ID="txtDNAnioOcupacion" runat="server" CssClass="form-control" Text="0"  MaxLength="4" ></asp:TextBox>
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="txtDNAnioOcupacion"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,4}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
							        </div>
						        </div>	
					    </div>
                          <div class="form-group row">   
						        <div class="form-group col-md-12">
							        <label for="txtDNNonComercio" class="form-control-label small">Nombre Galeria/Mercado/Centro Comercial/Campo Ferial etc.</label>
							        <div>
								         <asp:TextBox ID="txtDNNonComercio" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="100"></asp:TextBox>							
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvDNNonComercio" ControlToValidate="txtDNNonComercio" ErrorMessage="Ingrese Nombre Galeria/Mercado/Centro Comercial/Campo Ferial etc. del Negocio" ValidationGroup="DatosNegocio" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                    </div>
						        </div>	
                                
					    </div>
                         <div class="form-group row">   
                             	
                                <div class="form-group col-md-3">
							        <label for="txtDNTelFijo" class="form-control-label small">Telefono Fijo</label>
							        <div>
								        <asp:TextBox ID="txtDNTelFijo" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>		
                                          <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="txtDNTelFijo"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,8}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
							        </div>
						        </div>	
                                <div class="form-group col-md-3">
							        <label for="txtDNCelular" class="form-control-label small">Celular</label>
							        <div>
								        <asp:TextBox ID="txtDNCelular" runat="server" CssClass="form-control" Text="0" MaxLength="9"></asp:TextBox>
                                          <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="txtDNCelular"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,9}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
							        </div>
						        </div>
                                <div class="form-group col-md-6">
							        <label for="txtDNEmail" class="form-control-label small">E-Mail</label>
							        <div>
								        <asp:TextBox ID="txtDNEmail" runat="server" CssClass="form-control"  MaxLength="60"></asp:TextBox>							
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvDNEmail" ControlToValidate="txtDNEmail" ErrorMessage="Ingrese E-Mail del Negocio" ValidationGroup="DatosNegocio" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                    </div>
						        </div>
					    </div>
                        <hr />
                         <div class="form-group row">   
                             
                             <div class="form-group col-md-4">
                                 <label for="lblDNCue" class="form-control-label small"></label>
                                 <div>
                                    <label for="lblDNCue" class="form-control-label small">Datos Accionistas/Proprietarios</label>
                                 </div>
                             </div>
                             
					    </div>
                         <div class="form-group row">   
                              <div class="form-group col-md-4">
							        <label for="txtDNNombreAcc" class="form-control-label small">Nombre</label>
							        <div>
								        <asp:TextBox ID="txtDNNombreAcc" runat="server" CssClass="form-control" Style="text-transform: uppercase;" MaxLength="60"></asp:TextBox>							
							             <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvDNNombreAcc" ControlToValidate="txtDNNombreAcc" ErrorMessage="Ingrese Nombre del Accionista del Negocio" ValidationGroup="DatosNegocio" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                     </div>
						        </div>
                             <div class="form-group col-md-3">
							        <label for="txtDNRelacion" class="form-control-label small">Relacion</label>
							         <div>                                    
                                        <asp:DropDownList ID="ddlDNRelacion" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1">Accionista</asp:ListItem>
                                            <asp:ListItem Value="2">Accionista/Rep.Legal</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
						        </div>	
                               	
                                <div class="form-group col-md-2">
							        <label for="txtDNParticipacion" class="form-control-label small">Participacion (%)</label>
							        <div>
								        <asp:TextBox ID="txtDNParticipacion" runat="server" CssClass="form-control" Text="0"  MaxLength="3"></asp:TextBox>	
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                            ErrorMessage="Debe de ser números"
                                            ControlToValidate="txtDNParticipacion"
                                            ForeColor="Red"
                                            ValidationExpression="^[0-9]{1,3}$" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
							        </div>
						        </div>
                               <div class="form-group col-md-3">
							        <label for="txtDNCargo" class="form-control-label small">Cargo</label>
							         <div>                                    
                                        <asp:DropDownList ID="ddlCargo" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1">Socio</asp:ListItem>
                                            <asp:ListItem Value="2">Titular Gerente</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
						        </div>
					    </div>
					    <!--form-group-->
				    </div>
                          
                       
				    <!--IZQ-->
			    </ContentTemplate>
		    </asp:UpdatePanel>
	    </div>
	    <!--panel-body-->
     </div>
     <!--DATOS DEL NEGOCIO-->

        <h3 class="headerAcc">VALIDAR Y CONTINUAR</h3>
        <div>
            <asp:UpdatePanel ID="upGuardar" class="form-group row" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlListaNegraGuardar" CssClass="col-sm-12" Visible="false" runat="server">
                        <div class="form-group col-sm-12 alert alert-danger">
                            <span style="font-size: 1.5em; color: Tomato;" class="glyphicon glyphicon-exclamation-sign blink"></span>
                            <asp:Label Text="La Solicitud será analizada por el área de Riesgos por coincidencias en Lista Negra" Enabled="false" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>
                    <div class="col-sm-12">
                        <asp:Button runat="server" ID="btnValidar" Text="Validar y continuar" CssClass="btn btn-sm btn-primary" OnClick="btnGuardarClick" />
                        <br />
                        <br />
                        <asp:ValidationSummary runat="server" ID="vsError" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos:" ForeColor="Red" />
                        <asp:ValidationSummary runat="server" ID="vsSolicitudCredito" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos de la solicitud de credito:" ForeColor="Red" ValidationGroup="DatosSolicitudCredito" CssClass="small" Display="Dynamic" />
                        <asp:ValidationSummary runat="server" ID="vsDatosSolicitante" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos del Solicitante:" ForeColor="Red" ValidationGroup="DatosSolicitante" CssClass="small" Display="Dynamic" />
                        <asp:ValidationSummary runat="server" ID="vsDatosCony" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos del Cónyuge:" ForeColor="Red" ValidationGroup="DatosCony" CssClass="small" Display="Dynamic" />
                        <asp:ValidationSummary runat="server" ID="vsDatosLaborales" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos Laborales:" ForeColor="Red" ValidationGroup="DatosLaborales" CssClass="small" Display="Dynamic" />
                         <asp:ValidationSummary runat="server" ID="vsDatosNegocio" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos Negocio:" ForeColor="Red" ValidationGroup="DatosNegocio" CssClass="small" Display="Dynamic" />
                        <asp:ValidationSummary runat="server" ID="vsReferencias" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos de Referencias:" ForeColor="Red" ValidationGroup="Referencias" CssClass="small" Display="Dynamic" />
                    </div>
                    <asp:Panel ID="pnlError" class="col-sm-12" Visible="false" runat="server">
                        <div class="alert alert-danger" style="padding: 5px 15px 5px 15px; margin: 10px 0px;">
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress AssociatedUpdatePanelID="upGuardar" DisplayAfter="100" runat="server">
                <ProgressTemplate>
                    <div class="animationload">
                        <div class="osahanloading"></div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            //Binding Code
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded)
            function PageLoaded(sender, args) {
                $("#<%=ddlEmpresa.ClientID%>").select2({
                    language: "es", theme: "bootstrap", dropdownAutoWidth: false, width: 'auto',
                });
               // $("#PanelDNegocio").show();
            }
        });
        
    </script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            $("#rdoNuevo").click(function () { $("#divClienteExistente").hide(); });
            $("#rdoExistente").click(function () { $("#divClienteExistente").show(); });
        });
        
            function abrir() {
                $('#PanelDNegocio').show();
            }
            function ocultar() {
                $('#PanelDNegocio').hide();
            }
    </script>
</asp:Content>
