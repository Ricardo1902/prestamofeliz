﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class Site_Solicitudes_frmClientePeru : System.Web.UI.Page
{
    #region "Propiedades"
    private bool isEdicion
    {
        get { return (ViewState[this.UniqueID + "_isEdicion"] != null) ? (bool)ViewState[this.UniqueID + "_isEdicion"] : false; }
        set { ViewState[this.UniqueID + "_isEdicion"] = value; }
    }

    private int IdSucursal
    {
        get { return (Session["cve_sucurs"] != null) ? int.Parse(Session["cve_sucurs"].ToString()) : 7; }        
    }
    #endregion

    #region "Constantes"
    protected const string REGIMEN_INACTIVO = "OPCION INACTIVA";
    protected const string EMPRESA_SIN_CONFIGURACION = "Seleccione una situación laboral válida.";
    #endregion

    public partial class Referencia
    {
        public int idReferencia { get; set; }
        public string nombres { get; set; }
        public int parentesco { get; set; }
        public string parentescoDesc { get; set; }
        public string LadaRef { get; set; }
        public string TelRef { get; set; }
        public string direccion { get; set; }
        public string numext { get; set; }
        public int idcolonia { get; set; }
        public string CelRef { get; set; }
    }

    public class Lada
    {
        public Lada(string _Codigo, string _Estado)
        {
            Codigo = _Codigo;
            Estado = _Estado;
        }

        public string Codigo { get; set; }
        public string Estado { get; set; }
    }
    List<Lada> lstLada = new List<Lada>();

    protected void Page_Load(object sender, EventArgs e)
    {
        TBClientes[] cliente = null;
        List<ClientesReferencia> referenciasCliente = null;

        crearClavesLada();
        if (!IsPostBack)
        {
            Session["Referencias"] = null;
            int IdCliente = Convert.ToInt32(Request.QueryString["idcliente"]);
            int idSolicitud = Convert.ToInt32(Request.QueryString["idsolicitud"]);            
            int isE = Convert.ToInt32(Request.QueryString["isE"]);
            

            if (isE == 1)
            {
                this.isEdicion = true;
            }

            string folio = "";
            int idPromotor = 0;
            int idCanal = 0;
            string score = "";
            int Origen = 0;
            int idTipoCliente = 0;
            string categoriaCliente = "";
            int idProducto = 0;
            int idSubProducto = 0;
            int idTipoCredito = 0;

            if (idSolicitud > 0)
            {
                TBSolicitudes.DatosSolicitud[] Datos = null;

                using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
                {
                    Datos = wsSolicitud.ObtenerDatosSolicitud(idSolicitud);

                    if (Datos.Length > 0)
                    {
                        folio = Datos[0].pedido;
                        idPromotor = Datos[0].IDPromotor;
                        idCanal = Datos[0].IDCanalVenta;
                        score = Datos[0].ScoreExperian.ToString();
                        Origen = Datos[0].Origen_Id;
                        idProducto = Datos[0].idConvenio;
                        idSubProducto = Datos[0].IDSubProducto;
                    }
                }
            }
            else
            {
                // Solicitud de credito
                folio = Request.QueryString["folio"];
                idPromotor = Convert.ToInt32(Request.QueryString["idPromotor"]);
                idCanal = Convert.ToInt32(Request.QueryString["idCanal"]);
                score = Request.QueryString["score"];
                Origen = Convert.ToInt32(Request.QueryString["idOrigen"]);
                idTipoCliente = Convert.ToInt32(Request.QueryString["idTipoCliente"]);
                categoriaCliente = Request.QueryString["categoriaCliente"];
                idProducto = Convert.ToInt32(Request.QueryString["idProducto"]);
                idSubProducto = Convert.ToInt32(Request.QueryString["idSubProducto"]);
                idTipoCredito = Convert.ToInt32(Request.QueryString["idTipoCredito"]);
            }

            if (!String.IsNullOrEmpty(folio))
            {
                txtFolioSolicitud.Text = folio;
            }

            if (!String.IsNullOrEmpty(score))
            {
                txtScoreExperian.Text = score;
            }            

            using (wsSOPF.ClientesClient wsCliente = new ClientesClient())
            {
                cliente = wsCliente.ObtenerTBClientes(1, IdCliente);
                var tempReferencias = wsCliente.ObtenerReferenciasCliente(IdCliente);
                referenciasCliente = (tempReferencias != null) ? tempReferencias.ToList() : null;
                tempReferencias = null;
            }


            TBCATEstado[] estados = null;
            TBCATColonia[] colonias = null;
            TBCATColonia[] coloniasemp = null;
            TBCATCiudad[] ciudad = null;
            TBCATCiudad[] ciudademp = null;
            TBCATCiudad[] ciudades = null;
            TBCATCiudad[] ciudadesemp = null;

            SIPais[] paises = null;
            TipoPension[] tipoPension = null;
            TBCATTipoVade[] estadoCivil = null;
            TBCATTipoVade[] tipoVivienda = null;
            TBCATTipoVade[] nacionalidad = null;
            TBCATTipoVade[] parentesco = null;
            List<Referencia> referencias = new List<Referencia>();

            Combo[] Empresa_PE;
            Combo[] TipoZona_PE;
			Combo[] Direccion_PE;
			Combo[] SituacionLaboral_PE;
            Combo[] TipoCon_PE;
            Combo[] TipoDocumento_PE;
            Combo[] OrganoPago_PE;

            List<TBCatOrigen> Origenes;
            List<TBCATPromotor> Promotores;
            List<Combo> CanalVentas_PE;
            TBCATTipoConvenio[] TipoConvenio;
            List<TBCATSubProducto> SubProductos;
            List<Combo> Productos;

            DireccionCliente direccionCliente = new DireccionCliente();

            txtFechaSol.Text = DateTime.Now.ToString("dd/MM/yyyy");

            using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
			{
                //##### ##### ##### ##### ##### PERU
                Empresa_PE = wsCatalogo.ObtenerCombo_Empresa_PE();
                TipoZona_PE = wsCatalogo.ObtenerCombo_TipoZona_PE();
				Direccion_PE = wsCatalogo.ObtenerCombo_Direccion_PE();
				SituacionLaboral_PE = wsCatalogo.ObtenerCombo_SituacionLaboral_PE();
                TipoCon_PE = wsCatalogo.ObtenerCombo_SituacionLaboral_PE();
                TipoDocumento_PE = wsCatalogo.ObtenerCombo_TipoDocumento_PE();
                OrganoPago_PE = wsCatalogo.ObtenerCombo_OrganoPago_PE();
                Origenes = wsCatalogo.ObtenerTBCatOrigen(Accion: 1).ToList();
                Promotores = wsCatalogo.ObtenerPromotoresPorCanalVenta(this.IdSucursal, idCanal).ToList();
                CanalVentas_PE = wsCatalogo.ObtenerCombo_CanalVentas_PE().ToList();
                TipoConvenio = wsCatalogo.ObtenerTipoConvenio(2, int.Parse(Session["cve_sucurs"].ToString()), string.Empty, 0, 0);
                SubProductos = wsCatalogo.ObtenerSubProductosPorConvenio(IdConvenio: idProducto).ToList();
                //##### ##### ##### ##### ##### PERU

                estados = wsCatalogo.ObtenerTBCatEstado(0, 0, "", 0);
                if (estados.Length > 0)
                {
                    cboEstado.DataValueField = "IdEstado";
                    cboEstado.DataTextField = "Estado";
                    cboEstado.DataSource = estados;
                    cboEstado.DataBind();

                    cboEstadoNac.DataValueField = "IdEstado";
                    cboEstadoNac.DataTextField = "Estado";
                    cboEstadoNac.DataSource = estados;
                    cboEstadoNac.DataBind();

                    cboEstadoEmp.DataValueField = "IdEstado";
                    cboEstadoEmp.DataTextField = "Estado";
                    cboEstadoEmp.DataSource = estados;
                    cboEstadoEmp.DataBind();

                    ddlDNDepartamento.DataValueField = "IdEstado";
                    ddlDNDepartamento.DataTextField = "Estado";
                    ddlDNDepartamento.DataSource = estados;
                    ddlDNDepartamento.DataBind();

                    // Formulario Referencia
                    cboEstadoRef1.DataValueField = "IdEstado";
                    cboEstadoRef1.DataTextField = "Estado";
                    cboEstadoRef1.DataSource = estados;
                    cboEstadoRef1.DataBind();

                }

                if (TipoConvenio.Count() > 0)
                {
                    ddlTipoConvenio.DataSource = TipoConvenio;
                    ddlTipoConvenio.DataBind();
                    ddlTipoConvenio.SelectedValue = "2";

                    ddlTipoConvenio_SelectedIndexChanged(sender, e);

                    if (idProducto > 0)
                    {
                        ddlProducto.SelectedValue = idProducto.ToString();
                    }
                }

                SubProductos.Add(new TBCATSubProducto { IdSubProducto = -1, SubProducto = "--SELECCIONAR--" });
                if (SubProductos.Count > 0)
                {                    
                    ddlSubProducto.DataSource = SubProductos.OrderBy(sp => sp.IdSubProducto);
                    ddlSubProducto.DataBind();

                    if (idSubProducto > 0)
                    {
                        ddlSubProducto.SelectedValue = idSubProducto.ToString();
                    }
                }

                if (cliente.Length > 0 && IdCliente != 0)
                {
                    using (ClientesClient wsClientes = new ClientesClient())
                    {
                        int tempIdCliente = Convert.ToInt32(cliente[0].IdCliente);

                        DireccionCliente[] direcciones = wsClientes.ObtenerDireccionesCliente(tempIdCliente);

                        if (direcciones.Length > 0)
                        {
                            direccionCliente = direcciones[direcciones.Length - 1]; // Se toma la direccion mas reciente
                        }
                    }

                    int idColonia = Convert.ToInt32(cliente[0].IdColonia);

                    if (direccionCliente.idClienteDireccion > 0)
                    {
                        Session["idClienteDireccion"] = direccionCliente.idClienteDireccion;
                        idColonia = direccionCliente.idColonia;
                    }

                    colonias = wsCatalogo.ObtenerTbCatColoniaId(3, 0, Convert.ToInt32(idColonia));
                    ciudad = wsCatalogo.ObtenerTbCatCiudad(1, Convert.ToInt32(colonias[0].IdCiudad), "", 0, 0);
                    coloniasemp = wsCatalogo.ObtenerTbCatColoniaId(3, 0, Convert.ToInt32(cliente[0].IdCoOfic));

                    if (coloniasemp.Length > 0)
                    {
                        ciudademp = wsCatalogo.ObtenerTbCatCiudad(1, Convert.ToInt32(coloniasemp[0].IdCiudad), "", 0, 0);
                        ciudadesemp = wsCatalogo.ObtenerTbCatCiudad(101, Convert.ToInt32(coloniasemp[0].IdCiudad), "", 0, ciudademp[0].IdEstado);
                    }

                    ciudades = wsCatalogo.ObtenerTbCatCiudad(101, Convert.ToInt32(colonias[0].IdCiudad), "", 0, ciudad[0].IdEstado);
                    if (ciudades.Length > 0)
                    {
                        cboEstado.SelectedValue = ciudades[0].IdEstado.ToString();
                        int x;
                        for (x = 0; x <= lstLada.Count - 1; x++)
                        {
                            if (cboEstado.SelectedItem.Text.ToUpper() == lstLada[x].Estado)
                            {
                                if (lstLada[x].Codigo.Length > 1)
                                {
                                    txtLada.Text = lstLada[x].Codigo;
                                    txtTelCas.MaxLength = 6;
                                    revTelCas.ErrorMessage = "El teléfono debe ser 6 digitos.";
                                    revTelCas.ValidationExpression = "^[0-9]{6}$";
                                }
                                else
                                {
                                    txtTelCas.MaxLength = 7;
                                    revTelCas.ErrorMessage = "El teléfono debe ser 7 digitos.";
                                    revTelCas.ValidationExpression = "^[0-9]{7}$";
                                }
                                break;
                            }
                        }
                    }
                    // JRVB - 03/04/2018, Aplicar la validacion del numero de telefono de datos laborales.
                    if (ciudadesemp != null && ciudadesemp.Length > 0)
                    {
                        cboEstadoEmp.SelectedValue = ciudadesemp[0].IdEstado.ToString();
                        int x;
                        for (x = 0; x <= lstLada.Count - 1; x++)
                        {
                            if (cboEstadoEmp.SelectedItem.Text.ToUpper() == lstLada[x].Estado)
                            {
                                if (lstLada[x].Codigo.Length > 1)
                                {
                                    txtLadaEmp.Text = lstLada[x].Codigo;
                                    txtTelEmp.MaxLength = 6;
                                    revTelEmp.ErrorMessage = "El teléfono debe ser 6 digitos.";
                                    revTelEmp.ValidationExpression = "^[0-9]{6}$";
                                }
                                else
                                {
                                    txtTelEmp.MaxLength = 7;
                                    revTelEmp.ErrorMessage = "El teléfono debe ser 7 digitos.";
                                    revTelEmp.ValidationExpression = "^[0-9]{7}$";
                                }
                                break;
                            }
                        }
                    }

                    if (colonias.Length > 0)
                    {
                        cboColonia.DataValueField = "IdColonia";
                        cboColonia.DataTextField = "Colonia";
                        cboColonia.DataSource = colonias;
                        cboColonia.DataBind();
                    }

                    if (coloniasemp.Length > 0)
                    {
                        cboColoniaEmp.DataValueField = "IdColonia";
                        cboColoniaEmp.DataTextField = "Colonia";
                        cboColoniaEmp.DataSource = coloniasemp;
                        cboColoniaEmp.DataBind();
                    }

                    if (ciudades.Length > 0)
                    {
                        cboMunicipio.DataValueField = "IdCiudad";
                        cboMunicipio.DataTextField = "Ciudad";
                        cboMunicipio.DataSource = ciudades;
                        cboMunicipio.DataBind();
                    }

                    if (ciudadesemp != null && ciudadesemp.Length > 0)
                    {
                        cboMunicipioEmp.DataValueField = "IdCiudad";
                        cboMunicipioEmp.DataTextField = "Ciudad";
                        cboMunicipioEmp.DataSource = ciudadesemp;
                        cboMunicipioEmp.DataBind();
                    }
                }
				paises = wsCatalogo.ObtenerSIPais();
				tipoPension = wsCatalogo.ObtenerTipoPension(1);
				estadoCivil = wsCatalogo.ObtenerTBCATTipoVade(1);
				tipoVivienda = wsCatalogo.ObtenerTBCATTipoVade(3);
				nacionalidad = wsCatalogo.ObtenerTBCATTipoVade(6);
				parentesco = wsCatalogo.ObtenerTBCATTipoVade(4);
			}//wsCatalogo

            if (paises.Length > 0)
            {
                cboPais.DataValueField = "Clave";
                cboPais.DataTextField = "Pais";
                cboPais.DataSource = paises;
                cboPais.DataBind();

                // JRVB - 24/04/2018, Valor por defecto solicitado.
                var elPeru = paises.FirstOrDefault(x => x.Pais.ToUpper() == "PERU");
                cboPais.SelectedValue = (elPeru != null) ? elPeru.Clave.ToString() : string.Empty;
            }

            if (tipoPension.Length > 0)
            {
                cboTipoPen.DataValueField = "IdTipoPension";
                cboTipoPen.DataTextField = "Descripcion";
                cboTipoPen.DataSource = tipoPension;
                cboTipoPen.DataBind();
            }

            if (estadoCivil.Length > 0)
            {
                cboEstCiv.DataValueField = "IdTipoVade";
                cboEstCiv.DataTextField = "TipoVade";
                cboEstCiv.DataSource = estadoCivil;
                cboEstCiv.DataBind();
            }

            if (tipoVivienda.Length > 0)
            {
                cboTipViv.DataValueField = "IdTipoVade";
                cboTipViv.DataTextField = "TipoVade";
                cboTipViv.DataSource = tipoVivienda;
                cboTipViv.DataBind();
            }

            if (nacionalidad.Length > 0)
            {
                cboNacion.DataValueField = "IdTipoVade";
                cboNacion.DataTextField = "TipoVade";
                cboNacion.DataSource = nacionalidad;
                cboNacion.DataBind();
            }

            if (parentesco.Length > 0)
            {
                cboParPep.DataValueField = "IdTipoVade";
                cboParPep.DataTextField = "TipoVade";
                cboParPep.DataSource = parentesco;
                cboParPep.DataBind();

                cboParRef1.DataValueField = "IdTipoVade";
                cboParRef1.DataTextField = "TipoVade";
                cboParRef1.DataSource = parentesco;
                cboParRef1.DataBind();
            }
            if (TipoZona_PE.Length > 0)
            {
                ddlTipoZona.DataSource = TipoZona_PE;
                ddlTipoZona.DataValueField = "Valor";
                ddlTipoZona.DataTextField = "Descripcion";
                ddlTipoZona.DataBind();
            }
            if (Empresa_PE.Length > 0)
            {
                ddlEmpresa.DataSource = Empresa_PE;
                ddlEmpresa.DataValueField = "Valor";
                ddlEmpresa.DataTextField = "Descripcion";
                ddlEmpresa.DataBind();
                ddlEmpresa.Items.Insert(0, new ListItem("0-SELECCIONE", "NA"));
            }
            if (Direccion_PE.Length > 0)
            {
                ddlDireccion.DataSource = Direccion_PE;
                ddlDireccion.DataValueField = "Valor";
                ddlDireccion.DataTextField = "Descripcion";
                ddlDireccion.DataBind();
                ddlDNDireccion.DataSource = Direccion_PE;
                ddlDNDireccion.DataValueField = "Valor";
                ddlDNDireccion.DataTextField = "Descripcion";
                ddlDNDireccion.DataBind();
            }
            if (SituacionLaboral_PE.Length > 0)
            {
                ddlSituacionLaboral.DataSource = SituacionLaboral_PE;
                ddlSituacionLaboral.DataValueField = "Valor";
                ddlSituacionLaboral.DataTextField = "Descripcion";
                ddlSituacionLaboral.DataBind();
            }

            if (TipoCon_PE.Length > 0)
            {
                cboTipoCon.DataSource = TipoCon_PE;
                cboTipoCon.DataValueField = "Valor";
                cboTipoCon.DataTextField = "Descripcion";
                cboTipoCon.DataBind();
            }

            // Tipo de Negocio
            cboTipoInd.Items.Insert(0, new ListItem("Formal", "0"));
            cboTipoInd.Items.Insert(1, new ListItem("Informal", "1"));

            // Tipo de Empresa
            cboTipoEmp.Items.Insert(0, new ListItem("Publico", "0"));
            cboTipoEmp.Items.Insert(1, new ListItem("Privada", "1"));

            //Actividad
            //ddlDNActividad.Items.Insert(0, new ListItem("Confecciones en Cuero", "0"));
            //ddlDNActividad.Items.Insert(1, new ListItem("Fabricación de Prendas de Vestir", "1"));
            //ddlDNActividad.Items.Insert(2, new ListItem("Fabricación de Muebles", "2"));
            //ddlDNActividad.Items.Insert(3, new ListItem("Fabricación de Colchones", "3"));
            //ddlDNActividad.Items.Insert(4, new ListItem("Fabricación de Calzado", "4"));
            //ddlDNActividad.Items.Insert(5, new ListItem("Fabricación de Alimento para Animales", "5"));
            //ddlDNActividad.Items.Insert(6, new ListItem("Fabricación de Artículos Deportivos", "6"));
            //ddlDNActividad.Items.Insert(7, new ListItem("Fabricación de Artículos de Limpieza", "7"));
            //ddlDNActividad.Items.Insert(8, new ListItem("Fabricación de Pinturas y Productos Químicos", "8"));
            //ddlDNActividad.Items.Insert(9, new ListItem("Fabricación de Envases Plásticos", "9"));
            //ddlDNActividad.Items.Insert(10, new ListItem("Joyería", "10"));
            //ddlDNActividad.Items.Insert(11, new ListItem("Metalmecánica", "11"));
            //ddlDNActividad.Items.Insert(12, new ListItem("Panadería Pastelería", "12"));
            //ddlDNActividad.Items.Insert(13, new ListItem("Fabricación de Bicicletas y Coches", "13"));
            //ddlDNActividad.Items.Insert(14, new ListItem("Fabricación de Telas", "14"));

            ddlDNActividad.Items.Insert(0, new ListItem("Abarrotes al por Menor", "15"));
            ddlDNActividad.Items.Insert(1, new ListItem("Abarrotes al por Mayor", "16"));
            ddlDNActividad.Items.Insert(2, new ListItem("Artículos de Decoración y Regalos", "17"));
            ddlDNActividad.Items.Insert(3, new ListItem("Bazar y Librería", "18"));
            ddlDNActividad.Items.Insert(4, new ListItem("Bodegas", "19"));
            ddlDNActividad.Items.Insert(5, new ListItem("Calzado y Zapatillas", "20"));
            ddlDNActividad.Items.Insert(6, new ListItem("Carnes de Res y Aves", "21"));
            ddlDNActividad.Items.Insert(7, new ListItem("Distribuidor de Bebidas, Gaseosas", "22"));
            ddlDNActividad.Items.Insert(8, new ListItem("Ferreterías", "23"));
            ddlDNActividad.Items.Insert(9, new ListItem("Flores, Plantas y Afines", "24"));
            ddlDNActividad.Items.Insert(10, new ListItem("Mercería y Pasamanería", "25"));
            ddlDNActividad.Items.Insert(11, new ListItem("Minimarket", "26"));
            ddlDNActividad.Items.Insert(12, new ListItem("Alimento Balanceado", "27"));
            ddlDNActividad.Items.Insert(13, new ListItem("Artículos de Limpieza", "28"));
            ddlDNActividad.Items.Insert(14, new ListItem("Bisutería y Joyas", "29"));
            ddlDNActividad.Items.Insert(15, new ListItem("Artesanía", "30"));
            ddlDNActividad.Items.Insert(16, new ListItem("Colchones", "31"));
            ddlDNActividad.Items.Insert(17, new ListItem("Venta de Gas", "32"));
            ddlDNActividad.Items.Insert(18, new ListItem("Distribuidor de Huevos", "33"));
            ddlDNActividad.Items.Insert(19, new ListItem("Productos lácteos", "34"));
            ddlDNActividad.Items.Insert(20, new ListItem("Muebles", "35")); 
            ddlDNActividad.Items.Insert(21, new ListItem("Prendas de Vestir", "36"));
            ddlDNActividad.Items.Insert(22, new ListItem("Repuestos y Accesorios", "37"));
            ddlDNActividad.Items.Insert(23, new ListItem("Electrodomésticos", "38"));
            ddlDNActividad.Items.Insert(24, new ListItem("Juguetería", "39"));
            ddlDNActividad.Items.Insert(25, new ListItem("Lencería", "40"));
            ddlDNActividad.Items.Insert(26, new ListItem("Lubricantes", "41"));
            ddlDNActividad.Items.Insert(27, new ListItem("Plásticos y afines", "42"));
            ddlDNActividad.Items.Insert(28, new ListItem("Vidriería", "43"));
            ddlDNActividad.Items.Insert(29, new ListItem("Ópticas", "44"));
            ddlDNActividad.Items.Insert(30, new ListItem("Venta de bicicletas y coches", "45"));
            ddlDNActividad.Items.Insert(31, new ListItem("Equipos de cómputo/equipos electrónicos", "46"));
            ddlDNActividad.Items.Insert(32, new ListItem("Venta de productos por catalogo", "47"));
            ddlDNActividad.Items.Insert(33, new ListItem("Maderera", "48"));
            ddlDNActividad.Items.Insert(34, new ListItem("Venta de Frutas", "49"));
            ddlDNActividad.Items.Insert(35, new ListItem("Venta de comida", "50"));
            ddlDNActividad.Items.Insert(36, new ListItem("Jugueria", "51"));
            ddlDNActividad.Items.Insert(37, new ListItem("OTROS", "56"));

            ddlDNActividad.SelectedValue = "15";


            cboTipoPen.SelectedIndex = cboTipoPen.Items.IndexOf(cboTipoPen.Items.FindByText("N/A"));
            if (TipoDocumento_PE.Length > 0)
            {
                ddlTipoDoc.DataSource = TipoDocumento_PE;
                ddlTipoDoc.DataValueField = "Valor";
                ddlTipoDoc.DataTextField = "Descripcion";
                ddlTipoDoc.DataBind();
                ddlTipoDoc.SelectedValue = "1";

                ddlTipoDocCony.DataSource = TipoDocumento_PE;
                ddlTipoDocCony.DataValueField = "Valor";
                ddlTipoDocCony.DataTextField = "Descripcion";
                ddlTipoDocCony.DataBind();
            }
            camposConyuge(false);
            if (OrganoPago_PE.Length > 0)
            {
                ddlOrgano.DataValueField = "Valor";
                ddlOrgano.DataTextField = "Descripcion";
                ddlOrgano.DataSource = OrganoPago_PE;
                ddlOrgano.DataBind();
                ddlOrgano.Items.Insert(0, new ListItem("", "NA"));
            }

            Promotores.Add(new TBCATPromotor { IdPromotor = -1, Promotor = "--SELECCIONAR--" });
            if (Promotores.Count > 0)
            {                
                ddlPromotor.DataSource = Promotores.OrderBy(p => p.IdPromotor);
                ddlPromotor.DataValueField = "IdPromotor";
                ddlPromotor.DataTextField = "Promotor";
                ddlPromotor.DataBind();

                if (idPromotor > 0)
                {
                    ddlPromotor.SelectedValue = idPromotor.ToString();
                }
            }

            if (CanalVentas_PE.Count > 0)
            {
                CanalVentas_PE.Add(new Combo { Valor = -1, Descripcion = "--SELECCIONAR--" });
                ddlCanalVentas.DataSource = CanalVentas_PE.OrderBy(cv => cv.Valor);
                ddlCanalVentas.DataValueField = "Valor";
                ddlCanalVentas.DataTextField = "Descripcion";
                ddlCanalVentas.DataBind();

                if (idCanal > 0)
                {
                    ddlCanalVentas.SelectedValue = idCanal.ToString();
                }
            }

            if (Origenes.Count() > 0)
            {
                ddlOrigen.Items.Add(new ListItem { Value = "-1", Text = "--SELECCIONAR--" });

                Origenes.ForEach(o => {
                    ddlOrigen.Items.Add(new ListItem { Text = o.Origen, Value = o.IdOrigen.ToString() });
                });
                ddlOrigen.SelectedValue = Origen.ToString();

                if (Origen > 0)
                {
                    ddlOrigen.SelectedValue = Origen.ToString();
                }
            }
            //##### ##### ##### ##### #####      

            if (cliente.Length > 0 && IdCliente != 0)
            {
                txtNombreSolicitante.Text = cliente[0].Cliente;
                txtDNISolicitante.Text = cliente[0].NumeroDocumento;
                txtDNISolicitante.Enabled = false;
                txtNombreSolicitante.Text = cliente[0].Cliente;
                txtNombreSolicitante.Enabled = false;
                lnkSearchDNI.Visible = false;
                lnkClearDNI.Visible = true;
                txtTipoCliente.Text = "Cliente Existente";
                hdnTipoCliente.Value = "1";

                if (this.isEdicion)
                {
                    lnkClearDNI.Visible = false;
                    ddlProducto.Enabled = false;
                    ddlOrigen.Enabled = false;

                    // Ocultar la seccion "Busqueda Cliente"
                    hdrAccBusquedaCliente.Visible = false;
                    cntAccBusquedaCliente.Visible = false;
                }

                using (ClientesClient wsCliente = new ClientesClient())
                {
                    string categoria = wsCliente.ObtenerCategoriaCliente(IdCliente);
                    txtCategoriaCliente.Text = categoria;

                    using (wsSOPF.CreditoClient wsCredito = new wsSOPF.CreditoClient())
                    {
                        TipoCreditoCliente tipoCredito = wsCredito.ObtenerTipoCreditoCliente(IdCliente);
                        hdnTipoCredito.Value = tipoCredito.IdTipoCredito.ToString();
                        txtTipoCredito.Text = tipoCredito.Credito;
                    }
                }

            }

            if (cliente.Length > 0 && IdCliente != 0)
            {
                txtNombre.Text = cliente[0].Nombres;
                txtApePat.Text = cliente[0].Apellidop;
                txtApeMat.Text = cliente[0].Apellidom;
                rdSexo.SelectedValue = cliente[0].Sexo.ToString();
                txtOcupacion.Text = cliente[0].Ocupacion;
                txtFechaNacimiento.Text = cliente[0].FchNacimiento.ToString();

                // Validar Lista Negra, cliente Existente.
                ValidarListaNegraNombre(sender, e);

                // Fecha de Nacimiento y Edad
                if (cliente[0].FchNacimiento != null)
                {
                    txtFechaNacimiento.Text = cliente[0].FchNacimiento.ToString();
                    txtFechaNacimiento_TextChanged(sender, e);
                }

                if (direccionCliente.idClienteDireccion > 0)
                {
                    cboColonia.SelectedValue = direccionCliente.idColonia.ToString();
                    cboColoniaEmp.SelectedValue = direccionCliente.idColonia.ToString();
                    txtNumInt.Text = direccionCliente.numeroInterno;
                    txtNumExt.Text = direccionCliente.numeroExterno;
                    txtManzana.Text = direccionCliente.manzana;
                    txtLote.Text = direccionCliente.lote;
                    txtNombreZona.Text = direccionCliente.nombreZona;
                    txtNombreVia.Text = direccionCliente.nombreVia;

                    ddlTipoZona.SelectedValue = direccionCliente.tipoZona.ToString();
                    ddlDireccion.SelectedValue = direccionCliente.idTipoDireccion.ToString();
                }
                else
                {
                    cboColonia.SelectedValue = "0";

                    txtNumInt.Text = "";
                    txtNumExt.Text = "";

                    txtManzana.Text = "";
                    txtLote.Text = "";
                    txtNombreZona.Text = "";
                    txtNombreVia.Text = "";

                    ddlTipoZona.SelectedValue = "0";
                    ddlDireccion.SelectedValue = "0";

                    cboEstado.SelectedValue = "0";
                    cboEstado_SelectedIndexChange(sender, e);
                }

                cboEstadoNac.SelectedValue = cliente[0].IdCNacimiento.ToString();
                if (ciudademp != null && ciudademp.Length > 0)
                {
                    cboEstadoEmp.SelectedValue = ciudademp[0].IdEstado.ToString();
                }

                cboPais.SelectedValue = cliente[0].IdPaisNacimiento.ToString();
                cboTipoPen.SelectedValue = "0";
                cboMunicipio.SelectedValue = ciudad[0].IdCiudad.ToString();

                // Se remueve validacion de solicitud (ya que los datos del cliente se obtienen contienen la informacion mas actual)
                txtLada.Text = cliente[0].Lada;
                txtTelCas.Text = cliente[0].Telefono.ToString();
                txtTelCel.Text = cliente[0].Celular.ToString();

                txtReferenciaDomicilio.Text = cliente[0].ReferenciaDomicilio;
                if (ciudademp != null && ciudademp.Length > 0)
                {
                    cboMunicipioEmp.SelectedValue = ciudademp[0].IdCiudad.ToString();
                }

                cboEstCiv.SelectedValue = cliente[0].EstaCivil.ToString();

                string strEstadoCivil = cboEstCiv.SelectedItem.Text.ToUpper().Trim();

                camposConyuge((strEstadoCivil == "CASADO" || strEstadoCivil == "CONVIVIENTE/UNIÓN LIBRE"));

                txtNumDep.Text = cliente[0].DepeEcono.ToString();
                cboTipViv.SelectedValue = cliente[0].ViveCasa.ToString();
                cboNacion.SelectedValue = cliente[0].IdNacionalidad.ToString();
                if (cliente[0].IdConfiguracionEmpresa > 0)
                {
                    ObtenerConfiguracionEmpresaResponse Respuesta = new ObtenerConfiguracionEmpresaResponse();
                    ObtenerConfiguracionEmpresaRequest Peticion = new ObtenerConfiguracionEmpresaRequest()
                    {
                        Accion = "OBTENER_POR_ID",
                        IdConfiguracion = cliente[0].IdConfiguracionEmpresa
                    };
                    using (CatalogoClient wsCatalogo = new CatalogoClient())
                    {
                        Respuesta = wsCatalogo.ObtenerConfiguracionEmpresa(Peticion);
                    }
                    if (!Respuesta.Error && Respuesta.Resultado.Count() > 0)
                    {
                        ddlEmpresa.SelectedIndex = ddlEmpresa.Items.IndexOf(ddlEmpresa.Items.FindByValue(Respuesta.Resultado[0].IdEmpresa.ToString()));
                        ddlOrgano.SelectedIndex = ddlOrgano.Items.IndexOf(ddlOrgano.Items.FindByValue(Respuesta.Resultado[0].IdOrganoPago.ToString()));
                        txtNivelRiesgo.Text = Respuesta.Resultado[0].NivelRiesgo;
                        cboTipoPen.SelectedIndex = cboTipoPen.Items.IndexOf(cboTipoPen.Items.FindByValue(Respuesta.Resultado[0].IdRegimenPension.ToString()));
                        ddlSituacionLaboral.SelectedIndex = ddlSituacionLaboral.Items.IndexOf(ddlSituacionLaboral.Items.FindByValue(Respuesta.Resultado[0].IdSituacionLaboral.ToString()));
                        ddlSituacionLaboral_SelectedIndexChange(sender, e);
                        AlternarOpcionesSituacionLaboralRegimenPension(Respuesta.ComboSituacionLaboral, Respuesta.ComboRegimenPension);
                    }
                    else
                    {
                        throw new Exception(Respuesta.MensajeOperacion);
                    }
                }
                else if (cliente[0].Compania != null && cliente[0].RUC != null)
                {
                    bool MostrarEmpresaAnterior = true;
                    string situacion = string.Empty;
                    string regimen = string.Empty;
                    string RucEmpresa = cliente[0].RUC.ToString().Trim() + " - " + cliente[0].Compania.ToString().Trim();
                    int EmpresaAnteriorIndex = 0;
                    ListItem liSituacionLaboral = new ListItem();
                    liSituacionLaboral = ddlSituacionLaboral.Items.FindByValue(cliente[0].SituacionLaboral.ToString());
                    if (liSituacionLaboral != null && !string.IsNullOrWhiteSpace(liSituacionLaboral.Text) && liSituacionLaboral.Attributes["disabled"] != "disabled")
                    {
                        ddlSituacionLaboral.SelectedIndex = ddlSituacionLaboral.Items.IndexOf(ddlSituacionLaboral.Items.FindByValue(liSituacionLaboral.Value));
                    }
                    else
                    {
                        MostrarEmpresaAnterior = true;
                    }
                    situacion = ddlSituacionLaboral.Items.FindByValue(cliente[0].SituacionLaboral.ToString()).Text;
                    EmpresaAnteriorIndex = ddlEmpresa.Items.IndexOf(ddlEmpresa.Items.FindByText(RucEmpresa));
                    if (EmpresaAnteriorIndex > 0)
                    {
                        ddlEmpresa.SelectedIndex = EmpresaAnteriorIndex;
                        ddlEmpresa_SelectedIndexChanged(sender, e);
                    }
                    else
                    {
                        MostrarEmpresaAnterior = true;
                    }
                    ListItem liRegimenPension = new ListItem();
                    liRegimenPension = cboTipoPen.Items.FindByValue(cliente[0].RegimenPension.ToString());
                    if (liRegimenPension != null && !string.IsNullOrWhiteSpace(liRegimenPension.Text))
                    {
                        cboTipoPen.SelectedIndex = cboTipoPen.Items.IndexOf(cboTipoPen.Items.FindByValue(liRegimenPension.Value));
                    }
                    else
                    {
                        MostrarEmpresaAnterior = true;
                    }
                    regimen = liRegimenPension != null ? cboTipoPen.Items.FindByValue(cliente[0].RegimenPension.ToString()).Text : REGIMEN_INACTIVO;
                    if (MostrarEmpresaAnterior)
                    {
                        lblEmpresaSinConfiguracion.Visible = true;
                        lblEmpresaSinConfiguracion.Text = EMPRESA_SIN_CONFIGURACION;
                    }
                }
                txtPuesto.Text = cliente[0].Puesto;
                txtAntiguedad.Text = cliente[0].AntigOfic.ToString();                
                txtPlanillaVirtualUsuario.Text = cliente[0].UsuarioPlanillaVirtual.Trim();
                txtPlanillaVirtualClave.Text = cliente[0].ClavePlanillaVirtual.Trim();

                int IdEmpresa = 0;
                if (int.TryParse(ddlEmpresa.SelectedValue, out IdEmpresa))
                {
                    lblIdEmpresaCurrent.Text = IdEmpresa.ToString();
                    ActualizarConfiguracionCapturaPlanillaVirtual(IdEmpresa);
                }

                txtLadaEmp.Text = cliente[0].LadaOfic;
                txtTelEmp.Text = cliente[0].TeleOfic;
                txtAnexo.Text = cliente[0].ExteOfic;
                txtCalleEmp.Text = cliente[0].DireOfic;
                txtNumIntEmp.Text = "";
                txtNumExtEmp.Text = "";
                txtIngresoBruto.Text = cliente[0].IngresoBruto.ToString();
                txtIng.Text = cliente[0].Ingreso.ToString();
                txtOtrIng.Text = cliente[0].OtrosIngr.ToString();
                txtFuente.Text = cliente[0].FuenOtroi.ToString();
                txtDescuentosLey.Text = cliente[0].DescuentosLey.ToString();
                txtOtrosDescuentos.Text = cliente[0].OtrosDescuentos.ToString();

                ddlTipoDoc.SelectedValue = cliente[0].TipoDocumento.ToString();                

                txtNombreCony.Text = cliente[0].Conyuge;
                ddlTipoDocCony.SelectedValue = cliente[0].ConyTipoDocumento.ToString();
                txtNumDocCony.Text = cliente[0].ConyNumeroDocumento;
                txtDireccionCony.Text = cliente[0].ConyDireccion;
                txtTelefonoCony.Text = cliente[0].ConyTele;

                txtEmpresaCony.Text = cliente[0].ConyCompa;
                txtRUCCony.Text = cliente[0].ConyRUC;
                txtPuestoCony.Text = cliente[0].ConyPuest;
                txtAntiguedadCony.Text = cliente[0].ConyAntig.ToString();
                txtIngCony.Text = cliente[0].ConyIngre.ToString();
                txtDireccionLaboralCony.Text = cliente[0].ConyDireccionEmpresa;
                txtTelefonoLaboralCony.Text = cliente[0].ConyTelefonoEmpresa;
                //##### ##### ##### ##### ##### PERU

                txtemail.Text = cliente[0].Correo;
                txtNumIntEmp.Text = cliente[0].NumInteriorOfic;
                txtNumExtEmp.Text = cliente[0].NumExteriorOfic;
                txtDependenciaEmp.Text = cliente[0].DependenciaOfic;
                txtUbicacionEmp.Text = cliente[0].UbicacionOfic;

                if (cliente[0].PEP == 1)
                {
                    rdoPEP.SelectedIndex = 0;
                    txtNomPEP.Text = cliente[0].PEPNombre;
                    txtPuePEP.Text = cliente[0].PEPPuesto;
                    cboParPep.SelectedValue = cliente[0].PEPParentesco.ToString();
                    divPEP.Visible = true;
                }

                gvreferencias.DataSource = referenciasCliente;
                gvreferencias.DataBind();

                Session["Referencias"] = referenciasCliente;

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Accordion", "$(function(){ $('#divAccordion').accordion({active: 0}); });", true);
            }

            ddlDireccion_SelectedIndexChange(sender, e);
        }//!PostBack
        else
        {

        }
    } //load

    protected void ddlCanalVentas_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            List<TBCATPromotor> Promotores;
            Promotores = wsCatalogo.ObtenerPromotoresPorCanalVenta(this.IdSucursal, int.Parse(ddlCanalVentas.SelectedValue)).ToList();

            Promotores.Add(new TBCATPromotor { IdPromotor = -1, Promotor = "--SELECCIONAR--" });
            ddlPromotor.Items.Clear();
            ddlPromotor.DataSource = Promotores.OrderBy(p => p.IdPromotor);
            ddlPromotor.DataValueField = "IdPromotor";
            ddlPromotor.DataTextField = "Promotor";
            ddlPromotor.DataBind();
        }
    }
    protected void cboTipoEmp_SelectedIndexChanged(object sender, EventArgs e)
    {
        Combo[] Empresa_PE;
        int Tipo = Convert.ToInt32(cboTipoEmp.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            
            Empresa_PE = wsCatalogo.ObtenerCombo_Empresa_PE();

           
        }
        if (Empresa_PE.Length > 0)
        {
            ddlEmpresa.DataSource = Empresa_PE.Where(p => p.Tipo == Tipo).ToList();
            ddlEmpresa.DataValueField = "Valor";
            ddlEmpresa.DataTextField = "Descripcion";
            ddlEmpresa.DataBind();
            ddlEmpresa.Items.Insert(0, new ListItem("0-SELECCIONE", "NA"));
        }
    }
    protected void ddlProducto_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            List<TBCATSubProducto> SubProductos;
            SubProductos = wsCatalogo.ObtenerSubProductosPorConvenio(int.Parse(ddlProducto.SelectedValue)).ToList();

            SubProductos.Add(new TBCATSubProducto { IdSubProducto = -1, SubProducto = "--SELECCIONAR--" });
            ddlSubProducto.Items.Clear();
            ddlSubProducto.DataSource = SubProductos.OrderBy(p => p.IdSubProducto);
            ddlSubProducto.DataValueField = "IdSubProducto";
            ddlSubProducto.DataTextField = "SubProducto";
            ddlSubProducto.DataBind();

            string strProducto = ddlProducto.SelectedItem.Text.ToUpper().Trim();

            if (strProducto == "SUM") 
            {
               
                ddlSituacionLaboral.Items.FindByText("CAS").Enabled = false;
                ddlSituacionLaboral.Items.FindByText("Civil Activo").Enabled = false;
                ddlSituacionLaboral.Items.FindByText("Indefinido - Nombrado (728)").Enabled = false;
                ddlSituacionLaboral.Items.FindByText("Militar en Actividad").Enabled = false;
                ddlSituacionLaboral.Items.FindByText("Plazo fijo - Contratado (276)").Enabled = false;

                ddlSituacionLaboral.SelectedIndex = ddlSituacionLaboral.Items.IndexOf(ddlSituacionLaboral.Items.FindByText("Dependiente"));
                ddlSituacionLaboral_SelectedIndexChange(sender, e);
               // PanelDNegocio1.Visible = true;
                cboTipoEmp.Enabled = true;
                divTipoEmp.Visible = true;
                Combo[] Empresa_PE;
                Empresa_PE = wsCatalogo.ObtenerCombo_Empresa_PE();
                
                if (Empresa_PE.Length > 0)
                {
                    ddlEmpresa.DataSource = Empresa_PE.Where(p => p.Tipo == 0).ToList();
                    ddlEmpresa.DataValueField = "Valor";
                    ddlEmpresa.DataTextField = "Descripcion";
                    ddlEmpresa.DataBind();
                     ddlEmpresa.Items.Insert(0, new ListItem("0-SELECCIONE", "NA"));
                }
            }
           
            else
            {
                
                ddlSituacionLaboral.SelectedIndex = 1;
                ddlSituacionLaboral.Items.FindByText("CAS").Enabled = true;
                ddlSituacionLaboral.Items.FindByText("Civil Activo").Enabled = true;
                ddlSituacionLaboral.Items.FindByText("Indefinido - Nombrado (728)").Enabled = true;
                ddlSituacionLaboral.Items.FindByText("Militar en Actividad").Enabled = true;
                ddlSituacionLaboral.Items.FindByText("Plazo fijo - Contratado (276)").Enabled = true;

                ddlSituacionLaboral.SelectedIndex = ddlSituacionLaboral.Items.IndexOf(ddlSituacionLaboral.Items.FindByText("CAS"));
                ddlSituacionLaboral_SelectedIndexChange(sender, e);
               // PanelDNegocio1.Visible = false;
                cboTipoEmp.Enabled = false;
                divTipoEmp.Visible = false;
                Combo[] Empresa_PE;
                Empresa_PE = wsCatalogo.ObtenerCombo_Empresa_PE();

                if (Empresa_PE.Length > 0)
                {
                    ddlEmpresa.DataSource = Empresa_PE;
                    ddlEmpresa.DataValueField = "Valor";
                    ddlEmpresa.DataTextField = "Descripcion";
                    ddlEmpresa.DataBind();
                     ddlEmpresa.Items.Insert(0, new ListItem("0-SELECCIONE", "NA"));
                }
            }


        }
    }

    protected void txtFechaNacimiento_TextChanged(object sender, EventArgs e)
    {
        // Reiniciar control
        txtEdad.Text = string.Empty;

        // Calcular la Edad
        if (txtFechaNacimiento.Text.Trim() != string.Empty)
        {
            // Validamos que lo que capture el usuario sea una Fecha Valida
            DateTime dtFchNacimiento;
            if (DateTime.TryParseExact(txtFechaNacimiento.Text.Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dtFchNacimiento))
            {
                string Fecha = txtFechaNacimiento.Text.Substring(0, 10);
                int dia = Convert.ToInt32(Fecha.Substring(0, 2));
                int mes = Convert.ToInt32(Fecha.Substring(3, 2));
                int Año = Convert.ToInt32(Fecha.Substring(6, 4));

                int diaactual = DateTime.Now.Day;
                int MesActual = DateTime.Now.Month;
                int AnoActual = DateTime.Now.Year;

                int Edad = AnoActual - Año;
                if (mes > MesActual)
                {
                    Edad = Edad - 1;
                }
                if (mes == MesActual)
                {
                    if (dia > diaactual)
                    {
                        Edad = Edad - 1;
                    }
                }

                // Si es Menor de 18 años no se puede capturar
                if (Edad <= 17)
                {
                    txtEdad.Text = string.Empty;
                    txtFechaNacimiento.Text = string.Empty;
                }
                else
                    txtEdad.Text = Edad.ToString();
            }
            else
                txtFechaNacimiento.Text = string.Empty;
        }
    }

    private void crearClavesLada()
    {
        Lada itemLada;
        //Nada que excel no pueda resolver    
        itemLada = new Lada("41", "AMAZONAS"); lstLada.Add(itemLada);
        itemLada = new Lada("43", "ÁNCASH"); lstLada.Add(itemLada);
        itemLada = new Lada("83", "APURÍMAC"); lstLada.Add(itemLada);
        itemLada = new Lada("54", "AREQUIPA"); lstLada.Add(itemLada);
        itemLada = new Lada("66", "AYACUCHO"); lstLada.Add(itemLada);
        itemLada = new Lada("76", "CAJAMARCA"); lstLada.Add(itemLada);
        itemLada = new Lada("1", "CALLAO"); lstLada.Add(itemLada);
        itemLada = new Lada("84", "CUSCO"); lstLada.Add(itemLada);
        itemLada = new Lada("67", "HUANCAVELICA"); lstLada.Add(itemLada);
        itemLada = new Lada("62", "HUÁNUCO"); lstLada.Add(itemLada);
        itemLada = new Lada("56", "ICA"); lstLada.Add(itemLada);
        itemLada = new Lada("64", "JUNÍN"); lstLada.Add(itemLada);
        itemLada = new Lada("44", "LA LIBERTAD"); lstLada.Add(itemLada);
        itemLada = new Lada("74", "LAMBAYEQUE"); lstLada.Add(itemLada);
        itemLada = new Lada("1", "LIMA"); lstLada.Add(itemLada);
        itemLada = new Lada("65", "LORETO"); lstLada.Add(itemLada);
        itemLada = new Lada("82", "MADRE DE DIOS"); lstLada.Add(itemLada);
        itemLada = new Lada("53", "MOQUEGUA"); lstLada.Add(itemLada);
        itemLada = new Lada("63", "PASCO"); lstLada.Add(itemLada);
        itemLada = new Lada("73", "PIURA"); lstLada.Add(itemLada);
        itemLada = new Lada("51", "PUNO"); lstLada.Add(itemLada);
        itemLada = new Lada("42", "SAN MARTÍN"); lstLada.Add(itemLada);
        itemLada = new Lada("52", "TACNA"); lstLada.Add(itemLada);
        itemLada = new Lada("72", "TUMBES"); lstLada.Add(itemLada);
        itemLada = new Lada("61", "UCAYALI"); lstLada.Add(itemLada);
    }

    protected void ddlDNDepartamento_SelectedIndexChange(object sender, EventArgs e)
    {
        int idestado = 0;
        TBCATCiudad[] ciudades = null;

        idestado = Convert.ToInt32(ddlDNDepartamento.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            ciudades = wsCatalogo.ObtenerTbCatCiudadOrig(2, idestado, 0);
        }
        if (ciudades.Length > 0)
        {
            ddlDNProvincia.DataValueField = "IdCiudad";
            ddlDNProvincia.DataTextField = "Ciudad";
            ddlDNProvincia.DataSource = ciudades;
            ddlDNProvincia.DataBind();

            ddlDNProvincia_SelectedIndexChange(sender, e);
        }


    }

    protected void ddlDNProvincia_SelectedIndexChange(object sender, EventArgs e)
    {
        int idciudad = 0;
        TBCATColonia[] colonias = null;

        idciudad = Convert.ToInt32(ddlDNProvincia.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            colonias = wsCatalogo.ObtenerTbCatColonia(2, idciudad);
        }
        if (colonias.Length > 0)
        {
            ddlDNDistrito.DataValueField = "IdColonia";
            ddlDNDistrito.DataTextField = "Colonia";
            ddlDNDistrito.DataSource = colonias;
            ddlDNDistrito.DataBind();
        }
    }

    protected void cboEstado_SelectedIndexChange(object sender, EventArgs e)
    {
        int idestado = 0;
        TBCATCiudad[] ciudades = null;

        idestado = Convert.ToInt32(cboEstado.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            ciudades = wsCatalogo.ObtenerTbCatCiudadOrig(2, idestado, 0);
        }
        if (ciudades.Length > 0)
        {
            cboMunicipio.DataValueField = "IdCiudad";
            cboMunicipio.DataTextField = "Ciudad";
            cboMunicipio.DataSource = ciudades;
            cboMunicipio.DataBind();

            cboMunicipio_SelectedIndexChange(sender, e);
        }

        int x;
        txtLada.Text = "";
        for (x = 0; x <= lstLada.Count - 1; x++)
        {
            if (cboEstado.SelectedItem.Text.ToUpper() == lstLada[x].Estado)
            {
                if (lstLada[x].Codigo.Length > 1)
                {
                    txtLada.Text = lstLada[x].Codigo;
                    txtTelCas.MaxLength = 6;
                    revTelCas.ErrorMessage = "El teléfono debe ser 6 digitos.";
                    revTelCas.ValidationExpression = "^[0-9]{6}$";
                }
                else
                {
                    txtTelCas.MaxLength = 7;
                    revTelCas.ErrorMessage = "El teléfono debe ser 7 digitos.";
                    revTelCas.ValidationExpression = "^[0-9]{7}$";
                }
                break;
            }
        }

    }

    protected void cboMunicipio_SelectedIndexChange(object sender, EventArgs e)
    {
        int idciudad = 0;
        TBCATColonia[] colonias = null;

        idciudad = Convert.ToInt32(cboMunicipio.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            colonias = wsCatalogo.ObtenerTbCatColonia(2, idciudad);
        }
        if (colonias.Length > 0)
        {
            cboColonia.DataValueField = "IdColonia";
            cboColonia.DataTextField = "Colonia";
            cboColonia.DataSource = colonias;
            cboColonia.DataBind();
        }
    }

    protected void ddlDNSector_SelectedIndexChange(object sender, EventArgs e)
    {

        ddlDNActividad.Items.Clear();

        if (ddlDNSector.SelectedValue == "0")
        {

            ddlDNActividad.Items.Insert(0, new ListItem("Abarrotes al por Menor", "15"));
            ddlDNActividad.Items.Insert(1, new ListItem("Abarrotes al por Mayor", "16"));
            ddlDNActividad.Items.Insert(2, new ListItem("Artículos de Decoración y Regalos", "17"));
            ddlDNActividad.Items.Insert(3, new ListItem("Bazar y Librería", "18"));
            ddlDNActividad.Items.Insert(4, new ListItem("Bodegas", "19"));
            ddlDNActividad.Items.Insert(5, new ListItem("Calzado y Zapatillas", "20"));
            ddlDNActividad.Items.Insert(6, new ListItem("Carnes de Res y Aves", "21"));
            ddlDNActividad.Items.Insert(7, new ListItem("Distribuidor de Bebidas, Gaseosas", "22"));
            ddlDNActividad.Items.Insert(8, new ListItem("Ferreterías", "23"));
            ddlDNActividad.Items.Insert(9, new ListItem("Flores, Plantas y Afines", "24"));
            ddlDNActividad.Items.Insert(10, new ListItem("Mercería y Pasamanería", "25"));
            ddlDNActividad.Items.Insert(11, new ListItem("Minimarket", "26"));
            ddlDNActividad.Items.Insert(12, new ListItem("Alimento Balanceado", "27"));
            ddlDNActividad.Items.Insert(13, new ListItem("Artículos de Limpieza", "28"));
            ddlDNActividad.Items.Insert(14, new ListItem("Bisutería y Joyas", "29"));
            ddlDNActividad.Items.Insert(15, new ListItem("Artesanía", "30"));
            ddlDNActividad.Items.Insert(16, new ListItem("Colchones", "31"));
            ddlDNActividad.Items.Insert(17, new ListItem("Venta de Gas", "32"));
            ddlDNActividad.Items.Insert(18, new ListItem("Distribuidor de Huevos", "33"));
            ddlDNActividad.Items.Insert(19, new ListItem("Productos lácteos", "34"));
            ddlDNActividad.Items.Insert(20, new ListItem("Muebles", "35"));
            ddlDNActividad.Items.Insert(21, new ListItem("Prendas de Vestir", "36"));
            ddlDNActividad.Items.Insert(22, new ListItem("Repuestos y Accesorios", "37"));
            ddlDNActividad.Items.Insert(23, new ListItem("Electrodomésticos", "38"));
            ddlDNActividad.Items.Insert(24, new ListItem("Juguetería", "39"));
            ddlDNActividad.Items.Insert(25, new ListItem("Lencería", "40"));
            ddlDNActividad.Items.Insert(26, new ListItem("Lubricantes", "41"));
            ddlDNActividad.Items.Insert(27, new ListItem("Plásticos y afines", "42"));
            ddlDNActividad.Items.Insert(28, new ListItem("Vidriería", "43"));
            ddlDNActividad.Items.Insert(29, new ListItem("Ópticas", "44"));
            ddlDNActividad.Items.Insert(30, new ListItem("Venta de bicicletas y coches", "45"));
            ddlDNActividad.Items.Insert(31, new ListItem("Equipos de cómputo/equipos electrónicos", "46"));
            ddlDNActividad.Items.Insert(32, new ListItem("Venta de productos por catalogo", "47"));
            ddlDNActividad.Items.Insert(33, new ListItem("Maderera", "48"));
            ddlDNActividad.Items.Insert(34, new ListItem("Venta de Frutas", "49"));
            ddlDNActividad.Items.Insert(35, new ListItem("Venta de comida", "50"));
            ddlDNActividad.Items.Insert(36, new ListItem("Jugueria", "51"));
            ddlDNActividad.Items.Insert(37, new ListItem("OTROS", "56"));
        }
        else if (ddlDNSector.SelectedValue == "1")
        {
            ddlDNActividad.Items.Insert(0, new ListItem("Confecciones en Cuero", "0"));
            ddlDNActividad.Items.Insert(1, new ListItem("Fabricación de Prendas de Vestir", "1"));
            ddlDNActividad.Items.Insert(2, new ListItem("Fabricación de Muebles", "2"));
            ddlDNActividad.Items.Insert(3, new ListItem("Fabricación de Colchones", "3"));
            ddlDNActividad.Items.Insert(4, new ListItem("Fabricación de Calzado", "4"));
            ddlDNActividad.Items.Insert(5, new ListItem("Fabricación de Alimento para Animales", "5"));
            ddlDNActividad.Items.Insert(6, new ListItem("Fabricación de Artículos Deportivos", "6"));
            ddlDNActividad.Items.Insert(7, new ListItem("Fabricación de Artículos de Limpieza", "7"));
            ddlDNActividad.Items.Insert(8, new ListItem("Fabricación de Pinturas y Productos Químicos", "8"));
            ddlDNActividad.Items.Insert(9, new ListItem("Fabricación de Envases Plásticos", "9"));
            ddlDNActividad.Items.Insert(10, new ListItem("Joyería", "10"));
            ddlDNActividad.Items.Insert(11, new ListItem("Metalmecánica", "11"));
            ddlDNActividad.Items.Insert(12, new ListItem("Panadería Pastelería", "12"));
            ddlDNActividad.Items.Insert(13, new ListItem("Fabricación de Bicicletas y Coches", "13"));
            ddlDNActividad.Items.Insert(14, new ListItem("Fabricación de Telas", "14"));
            ddlDNActividad.Items.Insert(15, new ListItem("OTROS", "56"));
        }
        else if (ddlDNSector.SelectedValue == "2")
        {
            ddlDNActividad.Items.Insert(0, new ListItem("Servicios de Limpieza", "52"));
            ddlDNActividad.Items.Insert(1, new ListItem("Veterinaria", "53"));
            ddlDNActividad.Items.Insert(2, new ListItem("Lavandería, Tintorería", "54"));
            ddlDNActividad.Items.Insert(3, new ListItem("Servicio de confección/bordaduría", "55"));
            ddlDNActividad.Items.Insert(4, new ListItem("OTROS", "56"));
        }
    }

        protected void cboTipViv_SelectedIndexChange(object sender, EventArgs e)
    {
        int idTipVivd = 0;
        int idProducto = 0;
        idProducto = Convert.ToInt32(ddlProducto.SelectedValue);
        idTipVivd = Convert.ToInt32(cboTipViv.SelectedValue);
        if (idProducto == 11)
        {
            if (idTipVivd == 11)
            {
                PanArren.Visible = true;
                PanFam.Visible = false;
            }
            else if (idTipVivd == 10)
            {
                PanFam.Visible = true;
                PanArren.Visible = false;
            }
            else
            {
                PanArren.Visible = false;
                PanFam.Visible = false;
            }
        }
        else
        {
            PanArren.Visible = false;
            PanFam.Visible = false;
        }


        //using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        //{
        //    colonias = wsCatalogo.ObtenerTbCatColonia(2, idciudad);
        //}
        //if (colonias.Length > 0)
        //{
        //    cboColonia.DataValueField = "IdColonia";
        //    cboColonia.DataTextField = "Colonia";
        //    cboColonia.DataSource = colonias;
        //    cboColonia.DataBind();
        //}
    }

    protected void cboEstadoEmp_SelectedIndexChange(object sender, EventArgs e)
    {
        int idestado = 0;
        TBCATCiudad[] ciudades = null;

        idestado = Convert.ToInt32(cboEstadoEmp.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            ciudades = wsCatalogo.ObtenerTbCatCiudadOrig(2, idestado, 0);
        }
        if (ciudades.Length > 0)
        {
            cboMunicipioEmp.DataValueField = "IdCiudad";
            cboMunicipioEmp.DataTextField = "Ciudad";
            cboMunicipioEmp.DataSource = ciudades;
            cboMunicipioEmp.DataBind();

            cboMunicipioEmp_SelectedIndexChange(sender, e);
        }

        int x;
        txtLadaEmp.Text = "";
        for (x = 0; x <= lstLada.Count - 1; x++)
        {
            if (cboEstadoEmp.SelectedItem.Text.ToUpper() == lstLada[x].Estado)
            {
                if (lstLada[x].Codigo.Length > 1)
                {
                    txtLadaEmp.Text = lstLada[x].Codigo;
                    txtTelEmp.MaxLength = 6;
                    revTelEmp.ErrorMessage = "El teléfono debe ser 6 digitos.";
                    revTelEmp.ValidationExpression = "^[0-9]{6}$";
                }
                else
                {
                    txtTelEmp.MaxLength = 7;
                    revTelEmp.ErrorMessage = "El teléfono debe ser 7 digitos.";
                    revTelEmp.ValidationExpression = "^[0-9]{7}$";
                }
                break;
            }
        }
    }

    protected void cboMunicipioEmp_SelectedIndexChange(object sender, EventArgs e)
    {
        int idciudad = 0;
        TBCATColonia[] colonias = null;

        idciudad = Convert.ToInt32(cboMunicipioEmp.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            colonias = wsCatalogo.ObtenerTbCatColonia(2, idciudad);
        }
        if (colonias.Length > 0)
        {
            cboColoniaEmp.DataValueField = "IdColonia";
            cboColoniaEmp.DataTextField = "Colonia";
            cboColoniaEmp.DataSource = colonias;
            cboColoniaEmp.DataBind();
        }
    }

    protected void btnAgregarReferencia(object sender, EventArgs e)
    {
        pnlErrorReferencias.Visible = false;

        try
        {
            int longitud = 0;
            List<ClientesReferencia> referencias = (Session["Referencias"] == null) ? new List<ClientesReferencia>() : (List<ClientesReferencia>)Session["Referencias"];
            ClientesReferencia referencia = new ClientesReferencia();

            //if (String.IsNullOrEmpty(dboMunicipioRef1.SelectedValue)) throw new Exception("Seleccione un departamento");
            //else if (dboMunicipioRef1.SelectedValue == "0") throw new Exception("Seleccione una provincia");

            referencia.IdReferencia = Convert.ToInt32(hfIdReferencia.Value.Trim());
            referencia.Nombres = txtNombreRef1.Text.Trim().ToUpper();
            referencia.ApellidoPaterno = txtApellidoPaternoRef1.Text.Trim().ToUpper();
            referencia.ApellidoMaterno = txtApellidoMaternoRef1.Text.Trim().ToUpper();
            referencia.IdParentesco = Convert.ToInt32(cboParRef1.SelectedValue);
            referencia.Calle = txtCalleRef1.Text.Trim();
            referencia.NumeroExterior = txtNumExteriorRef1.Text.Trim();
            referencia.NumeroInterior = txtNumInteriorRef1.Text.Trim();
            referencia.IdColonia = !string.IsNullOrWhiteSpace(cboColoniaRef1.SelectedValue) ?
                Convert.ToInt32(cboColoniaRef1.SelectedValue) : 0;

            referencia.Lada = txtLadaTelRef1.Text.Trim();
            referencia.Telefono = txtTelRef1.Text.Trim();
            referencia.LadaCelular = txtLadaCelRef1.Text.Trim();
            referencia.TelefonoCelular = txtTelCelRef.Text.Trim();

            if (string.IsNullOrEmpty(referencia.Nombres))
                throw new Exception("Capture el nombre de la referencia");
            if (string.IsNullOrEmpty(referencia.ApellidoPaterno))
                throw new Exception("Capture al menos su apellido paterno");
            //if (string.IsNullOrEmpty(referencia.Calle))
            //    throw new Exception("Capture el nombre de la via");
            //if (string.IsNullOrEmpty(referencia.NumeroExterior))
            //    throw new Exception("Capture al menos el número exterior");
            //if (referencia.IdColonia <= 0)
            //    throw new Exception("Seleccione una colonia");

            longitud = referencia.Telefono.Length;
            if (longitud > 0)
            {
                if (longitud != 8)
                {
                    throw new Exception("Teléfono inválido");
                }
            }

            longitud = referencia.TelefonoCelular.Length;
            if (longitud > 0)
            {
                if (longitud != 9)
                {
                    throw new Exception("Celular inválido");
                }
            }

            if (string.IsNullOrEmpty(referencia.Telefono) && string.IsNullOrEmpty(referencia.TelefonoCelular))
                throw new Exception("Debe capturar algun medio de contacto, teléfono o celular.");

            referencias.Add(referencia);
            Session["Referencias"] = referencias;

            gvreferencias.DataSource = referencias;
            gvreferencias.DataBind();

            hfIdReferencia.Value = "0";
            txtNombreRef1.Text = "";
            txtApellidoPaternoRef1.Text = "";
            txtApellidoMaternoRef1.Text = "";
            txtCalleRef1.Text = "";
            txtNumExteriorRef1.Text = "";
            txtNumInteriorRef1.Text = "";
            cboEstadoRef1.ClearSelection();
            cboColoniaRef1.ClearSelection();
            dboMunicipioRef1.ClearSelection();
            txtLadaTelRef1.Text = "";
            txtLadaCelRef1.Text = "";
            txtTelRef1.Text = "";
            txtTelCelRef.Text = "";
        }
        catch (Exception ex)
        {
            lblErrorReferencias.Text = ex.Message;
            pnlErrorReferencias.Visible = true;
        }
    }

    protected void gvreferencias_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        List<ClientesReferencia> referencias = (Session["Referencias"] != null) ? ((List<ClientesReferencia>)Session["Referencias"]).ToList() : new List<ClientesReferencia>();
        ClientesReferencia referencia = new ClientesReferencia();

        int rowIndex = -1;

        int.TryParse(e.CommandArgument.ToString(), out rowIndex);
        if (rowIndex >= 0)
        {
            referencia = referencias[rowIndex];

            switch (e.CommandName)
            {
                case "Editar":
                    hfIdReferencia.Value = referencia.IdReferencia.ToString();
                    txtNombreRef1.Text = referencia.Nombres;
                    txtApellidoPaternoRef1.Text = referencia.ApellidoPaterno;
                    txtApellidoMaternoRef1.Text = referencia.ApellidoMaterno;
                    txtCalleRef1.Text = referencia.Calle;
                    txtNumExteriorRef1.Text = referencia.NumeroExterior;
                    txtNumInteriorRef1.Text = referencia.NumeroInterior;
                    cboParRef1.SelectedValue = referencia.IdParentesco.ToString();
                    txtLadaTelRef1.Text = referencia.Lada;
                    txtTelRef1.Text = string.Concat(referencia.Lada, referencia.Telefono);
                    txtLadaCelRef1.Text = referencia.LadaCelular;
                    txtTelCelRef.Text = string.Concat(referencia.LadaCelular, referencia.TelefonoCelular);
                    break;
                case "Eliminar":
                    hfIdReferencia.Value = "0";
                    break;
            }

            referencias.Remove(referencia);

            Session["Referencias"] = referencias;
            gvreferencias.DataSource = referencias;
            gvreferencias.DataBind();
        }
        else
        {
            throw new Exception("Ocurrio un error al identificar el registro a operar");
        }
    }

    protected void btnGuardarClick(object sender, EventArgs e)
    {
        List<ClientesReferencia> referencias = (Session["Referencias"] != null) ? (List<ClientesReferencia>)Session["Referencias"] : new List<ClientesReferencia>();
        string strddlProducto = ddlProducto.SelectedItem.Text.ToUpper().Trim();

        string strddlSituacionLaboral = ddlSituacionLaboral.SelectedItem.Text.ToUpper().Trim();

        valIconDatosSolicitudCredito.Visible = false;
        valIconDatosSolicitante.Visible = false;
        valIconDatosNegocio.Visible = false;
        valIconDatosCony.Visible = false;
        valIconDatosLaborales.Visible = false;
        valIconReferencias.Visible = false;
        pnlError.Visible = false;

        Page.Validate("DatosSolicitante");
        Page.Validate("DatosCony");
        Page.Validate("DatosLaborales");
        Page.Validate("DatosSolicitudCredito");
       
        if (strddlSituacionLaboral == "INDEPENDIENTE")
        {
            if (strddlProducto == "SUM")
            {
                Page.Validate("DatosNegocio");
                if (!IsValidGroup("DatosNegocio"))
                {
                   
                    valIconDatosNegocio.Visible = true;
                    upDatosNegocio.Update();
                }
            }
        }
        if (!IsValidGroup("DatosSolicitante"))
        {
            valIconDatosSolicitante.Visible = true;
            upDatosSolicitante.Update();
        }
       
        if (!IsValidGroup("DatosCony"))
        {
            valIconDatosCony.Visible = true;
            upDatosCony.Update();
        }

        if (!IsValidGroup("DatosLaborales"))
        {
            valIconDatosLaborales.Visible = true;
            upDatosLaborales.Update();
        }

        if (!IsValidGroup("DatosSolicitudCredito"))
        {
            valIconDatosSolicitudCredito.Visible = true;
            upSolicitudCredito.Update();
        }

        if (referencias.Count < 2)
        {
            cvReferencias.IsValid = false;
            valIconReferencias.Visible = true;
            upValReferencias.Update();
        }

        // Validar Custom Validators
        ValidarListaNegraDNI(sender, e);

        if (!cvDNIListaNegra.IsValid)
        {
            valIconDatosSolicitante.Visible = true;
            upDatosSolicitante.Update();
        }

        if (Page.IsValid)
        {
            try
            {
                TBClientes cliente = new TBClientes();

                int tempIdUsuario = 0;
                if (Session["UsuarioId"] != null) int.TryParse(Session["UsuarioId"].ToString(), out tempIdUsuario);

                cliente.IdUsuarioActualiza = tempIdUsuario;
                cliente.Nombres = txtNombre.Text.ToUpper().Trim();
                cliente.Apellidop = txtApePat.Text.ToUpper().Trim();
                cliente.Apellidom = txtApeMat.Text.ToUpper().Trim();
                cliente.Sexo = rdSexo.SelectedIndex;

                string Fecha = txtFechaNacimiento.Text.Substring(0, 10);
                int dia = Convert.ToInt32(Fecha.Substring(0, 2));
                int mes = Convert.ToInt32(Fecha.Substring(3, 2));
                int Año = Convert.ToInt32(Fecha.Substring(6, 4));

                cliente.FchNacimiento = txtFechaNacimiento.Text;
                cliente.IdCNacimiento = Convert.ToInt32(cboEstadoNac.SelectedValue);
                cliente.IdPaisNacimiento = Convert.ToInt32(cboPais.SelectedValue);
                cliente.Ocupacion = txtOcupacion.Text.ToUpper().Trim();

                //##### ##### ##### ##### ##### PERU
                cliente.TipoDocumento = Convert.ToInt32(ddlTipoDoc.SelectedValue);
                cliente.NumeroDocumento = txtDNISolicitante.Text.Trim();

                cliente.TipoDireccion = Convert.ToInt32(ddlDireccion.SelectedValue);
                cliente.NombreVia = txtNombreVia.Text.ToUpper().Trim();
                cliente.TipoZona = Convert.ToInt32(ddlTipoZona.SelectedValue);
                cliente.NombreZona = txtNombreZona.Text.ToUpper().Trim();

                try
                {
                    int IdEmpresa = 0;
                    int IdOrganoPago = 0;
                    int IdSituacionLaboral = 0;
                    int IdRegimenPension = 0;

                    if (int.TryParse(ddlEmpresa.SelectedValue, out IdEmpresa) &&
                        int.TryParse(ddlOrgano.SelectedValue, out IdOrganoPago) &&
                        int.TryParse(ddlSituacionLaboral.SelectedValue, out IdSituacionLaboral) &&
                        int.TryParse(cboTipoPen.SelectedValue, out IdRegimenPension))
                    {
                        string strSituacionLaboral = ddlSituacionLaboral.SelectedItem.Text.ToUpper();
                        if (strSituacionLaboral != "JUBILADO" && strSituacionLaboral != "PENSIONISTA")
                            IdRegimenPension = 0;

                      
                            ObtenerConfiguracionEmpresaRequest Peticion = new ObtenerConfiguracionEmpresaRequest()
                        {
                            Accion = "OBTENER_ID_POR_CAMPOS",
                            IdEmpresa = IdEmpresa,
                            IdOrgano = IdOrganoPago,
                            IdSituacionLaboral = IdSituacionLaboral,
                            IdRegimenPension = IdRegimenPension
                        };

                        ObtenerConfiguracionEmpresaResponse Respuesta = new ObtenerConfiguracionEmpresaResponse();
                        using (CatalogoClient wsCatalogo = new CatalogoClient())
                        {
                            Respuesta = wsCatalogo.ObtenerConfiguracionEmpresa(Peticion);
                        }

                        if (!Respuesta.Error && Respuesta.IdConfiguracionEmpresa > 0)
                        {
                            cliente.IdConfiguracionEmpresa = Respuesta.IdConfiguracionEmpresa;
                            cliente.SituacionLaboral = IdSituacionLaboral;
                            if (cboTipoPen.Enabled)
                                cliente.RegimenPension = IdRegimenPension;
                            else
                                cliente.RegimenPension = 0;
                            cliente.Compania = "";
                        }
                        else
                        {
                            throw new Exception(Respuesta.MensajeOperacion);
                        }
                    }
                    else
                    {
                        throw new Exception("Los datos de empresa son inválidos.");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Ha ocurrido un error al obtener la configuración de empresa. Error: " + (string.IsNullOrWhiteSpace(ex.Message) ? "conexión." : ex.Message));
                }

                //##### ##### ##### ##### ##### PERU

                cliente.Correo = txtemail.Text.Trim();
                cliente.CURP = "";
                cliente.Rfc = "";
                cliente.Direccion = "";

                cliente.NumInterior = txtNumInt.Text.ToUpper().Trim();
                cliente.NumExterior = txtNumExt.Text.ToUpper().Trim();
                cliente.IdColonia = Convert.ToInt32(cboColonia.SelectedValue);
                cliente.Manzana = txtManzana.Text.Trim().ToUpper();
                cliente.Lote = txtLote.Text.Trim().ToUpper();
                cliente.ReferenciaDomicilio = txtReferenciaDomicilio.Text.Trim().ToUpper();

                cliente.AntigDom = 0;

                cliente.Lada = txtLada.Text.Trim();
                cliente.Telefono = txtTelCas.Text.Trim();
                cliente.Celular = txtTelCel.Text.Trim();
                cliente.EstaCivil = Convert.ToInt32(cboEstCiv.SelectedValue);

                //SOLO PARA SUM
                if (strddlProducto == "SUM")
                {
                    string strcboTipViv = cboTipViv.SelectedItem.Text.ToUpper().Trim();
                    if (strcboTipViv == "ALQUILADA")
                    {
                        cliente.vNomArrendatarioCasa = txtNomArren.Text.ToUpper().Trim();
                        cliente.iAniosViviendoCasa = Convert.ToInt32(txtNumAnoViven.Text.Trim());

                    }
                    else
                    {
                        cliente.vNomArrendatarioCasa = "";
                        cliente.iAniosViviendoCasa = 0;
                    }
                    if (strcboTipViv == "FAMILIAR")
                    {
                        cliente.vNomFamCasa = txtNomFam.Text.ToUpper().Trim();
                        cliente.vParentescoCasa = txtParentesco.SelectedValue;
                    }
                    else
                    {
                        cliente.vNomFamCasa = "";
                        cliente.vParentescoCasa = "";
                    }
                    if (strddlSituacionLaboral == "DEPENDIENTE")
                    {
                        cliente.iTipContrato = Convert.ToInt32(cboTipoCon.SelectedValue);
                    }
                    else
                    {
                        cliente.iTipContrato = 0;
                    }
                    if (strddlSituacionLaboral == "INDEPENDIENTE")
                    {
                        
                        cliente.iTipNegocio = Convert.ToInt32(cboTipoInd.SelectedValue);

                        cliente.vDNNomCliente= txtDNCliente.Text.Trim().ToUpper();
                        cliente.vDNNomRazon= txtDNRazon.Text.Trim().ToUpper();
                        cliente.iDNTipPersoneria= Convert.ToInt32(ddlDNTipPer.SelectedValue);
                        cliente.vDNTipSociedad= ddlDNTipSoc.SelectedValue;
                        cliente.iDNTipNegocio= Convert.ToInt32(ddlDNTipNeg.SelectedValue);
                        cliente.iDNTipRegTributario= Convert.ToInt32(ddlDNRegTri.SelectedValue);
                        cliente.vDNRUC= txtDNRuc.Text.Trim();
                        cliente.iDNTipSector= Convert.ToInt32(ddlDNSector.SelectedValue);
                        cliente.iDNTipActividad= Convert.ToInt32(ddlDNActividad.SelectedValue);
                        cliente.iDNMesExperiencia= Convert.ToInt32(ddlDMGMes.SelectedValue);
                        cliente.iDNAnioExperiencia= Convert.ToInt32(txtDNGAno.Text.Trim());
                        cliente.iDNMesInicio= Convert.ToInt32(ddlDMAMes.SelectedValue);
                        cliente.iDNAnioInicio= Convert.ToInt32(txtDNAAno.Text.Trim());
                        cliente.iDNNumEmpleados= Convert.ToInt32(txtDNNumEmp.Text.Trim());
                        cliente.iDNNumSucursales= Convert.ToInt32(txtDNNumSuc.Text.Trim());
                        cliente.iDNTipDireccion= Convert.ToInt32(ddlDNDireccion.SelectedValue);
                        cliente.vDNVia= txtDNVia.Text.Trim().ToUpper();

                        cliente.iDNNumero= Convert.ToInt32(txtDNNro.Text.Trim());
                        cliente.vDNLote= txtDNLote.Text.Trim().ToUpper();
                        cliente.vDNManzana= TxtDNManzana.Text.Trim().ToUpper();
                        cliente.vDNInterior= TxtDNInterior.Text.Trim().ToUpper();
                        cliente.iDNPiso= Convert.ToInt32(TxtDNPiso.Text.Trim());
                        cliente.vDNZona= TxtZona.Text.Trim().ToUpper();
                        cliente.iDNIdDepartamento= Convert.ToInt32(ddlDNDepartamento.SelectedValue);
                        cliente.iDNIdProvincia= Convert.ToInt32(ddlDNProvincia.SelectedValue);
                        cliente.iDNIdDistrito= Convert.ToInt32(ddlDNDistrito.SelectedValue); 
                        cliente.vDNReferencia= TxtDNReferencia.Text.Trim().ToUpper();
                        cliente.iDNTipPropiedad= Convert.ToInt32(ddlDNTipPropiedad.SelectedValue);
                        cliente.iDNAnioOcupacion= Convert.ToInt32(txtDNAnioOcupacion.Text.Trim());
                        cliente.vDNNomComercio= txtDNNonComercio.Text.Trim().ToUpper();
                        cliente.iDNTelefono= Convert.ToInt32(txtDNTelFijo.Text.Trim());
                        cliente.iDNCelular= Convert.ToInt32(txtDNCelular.Text.Trim());
                        cliente.vDNEmail= txtDNEmail.Text.Trim().ToUpper();
                        cliente.vDNNomAccionista= txtDNNombreAcc.Text.Trim().ToUpper();
                        cliente.iDNTipRelacion= Convert.ToInt32(ddlDNRelacion.SelectedValue);
                        cliente.iDNParticipacion= Convert.ToInt32(txtDNParticipacion.Text.Trim());
                        cliente.iDNIdCargo= Convert.ToInt32(ddlCargo.SelectedValue);
                    }
                    else
                    {
                        cliente.iTipNegocio = 0;
                        cliente.vDNNomCliente = "";
                        cliente.vDNNomRazon = "";
                        cliente.iDNTipPersoneria = 0;
                        cliente.vDNTipSociedad = "";
                        cliente.iDNTipNegocio = 0;
                        cliente.iDNTipRegTributario = 0;
                        cliente.vDNRUC = "";
                        cliente.iDNTipSector = 0;
                        cliente.iDNTipActividad = 0;
                        cliente.iDNMesExperiencia = 0;
                        cliente.iDNAnioExperiencia = 0;
                        cliente.iDNMesInicio = 0;
                        cliente.iDNAnioInicio = 0;
                        cliente.iDNNumEmpleados = 0;
                        cliente.iDNNumSucursales = 0;
                        cliente.iDNTipDireccion = 0;
                        cliente.vDNVia = "";
                        cliente.iDNNumero = 0;
                        cliente.vDNLote = "";
                        cliente.vDNManzana = "";
                        cliente.vDNInterior = "";
                        cliente.iDNPiso = 0;
                        cliente.vDNZona = "";
                        cliente.iDNIdDepartamento = 0;
                        cliente.iDNIdProvincia = 0;
                        cliente.iDNIdDistrito = 0;
                        cliente.vDNReferencia = "";
                        cliente.iDNTipPropiedad = 0;
                        cliente.iDNAnioOcupacion = 0;
                        cliente.vDNNomComercio = "";
                        cliente.iDNTelefono = 0;
                        cliente.iDNCelular = 0;
                        cliente.vDNEmail = "";
                        cliente.vDNNomAccionista = "";
                        cliente.iDNTipRelacion = 0;
                        cliente.iDNParticipacion = 0;
                        cliente.iDNIdCargo = 0;
                    }
                }
                
                string strEstadoCivil = cboEstCiv.SelectedItem.Text.ToUpper().Trim();
                
                if (strEstadoCivil == "CASADO" || strEstadoCivil == "CONVIVIENTE/UNIÓN LIBRE")
                {
                    cliente.Conyuge = txtNombreCony.Text.Trim();
                    cliente.Conyuge = quitarEspacios(cliente.Conyuge);
                    cliente.ConyTipoDocumento = Convert.ToInt32(ddlTipoDocCony.SelectedValue);
                    cliente.ConyNumeroDocumento = txtNumDocCony.Text.Trim();
                    cliente.ConyDireccion = quitarEspacios(txtDireccionCony.Text);
                    cliente.ConyTele = txtTelefonoCony.Text.Trim();

                    cliente.ConyCompa = quitarEspacios(txtEmpresaCony.Text);
                    cliente.ConyRUC = quitarEspacios(txtRUCCony.Text);
                    cliente.ConyPuest = quitarEspacios(txtPuestoCony.Text);

                    if (string.IsNullOrEmpty(txtAntiguedadCony.Text))
                        cliente.ConyAntig = 0;
                    else
                        cliente.ConyAntig = Convert.ToInt32(txtAntiguedadCony.Text);

                    if (string.IsNullOrEmpty(txtIngCony.Text))
                        cliente.ConyIngre = 0;
                    else
                        cliente.ConyIngre = Convert.ToDecimal(txtIngCony.Text);

                    cliente.ConyDireccionEmpresa = quitarEspacios(txtDireccionLaboralCony.Text);
                    cliente.ConyTelefonoEmpresa = txtTelefonoLaboralCony.Text.Trim();
                }
                else
                {
                    cliente.Conyuge = "";
                    cliente.ConyTipoDocumento = Convert.ToInt32(ddlTipoDocCony.SelectedValue);
                    cliente.ConyNumeroDocumento = "";
                    cliente.ConyDireccion = "";
                    cliente.ConyTele = "";
                    cliente.ConyCompa = "";
                    cliente.ConyRUC = "";
                    cliente.ConyPuest = "";
                    cliente.ConyAntig = 0;
                    cliente.ConyIngre = 0;
                    cliente.ConyDireccionEmpresa = "";
                    cliente.ConyTelefonoEmpresa = "";
                }

                cliente.Sexo = Convert.ToInt32(rdSexo.SelectedValue);
                cliente.IFE = "";
                cliente.DepeEcono = (!string.IsNullOrEmpty(txtNumDep.Text.Trim())) ? Convert.ToInt32(txtNumDep.Text.Trim()) : 0;
                cliente.IdNacionalidad = Convert.ToInt32(cboNacion.SelectedValue);
                cliente.Puesto = txtPuesto.Text.ToUpper().Trim();
                cliente.AntigOfic = (!string.IsNullOrEmpty(txtAntiguedad.Text.Trim())) ? Convert.ToInt32(txtAntiguedad.Text.Trim()) : 0;
                cliente.UsuarioPlanillaVirtual = txtPlanillaVirtualUsuario.Text.Trim();
                cliente.ClavePlanillaVirtual = txtPlanillaVirtualClave.Text.Trim();
                cliente.NumEmplea = "";
                cliente.LadaOfic = txtLadaEmp.Text.Trim();
                cliente.TeleOfic = txtTelEmp.Text.Trim();
                cliente.ExteOfic = txtAnexo.Text.Trim();
                cliente.DireOfic = txtCalleEmp.Text.ToUpper().Trim();
                cliente.NombArre = "";
                cliente.LadaArre = "";
                cliente.TeleArre = "";
                cliente.NumInteriorOfic = txtNumIntEmp.Text.Trim();
                cliente.NumExteriorOfic = txtNumExtEmp.Text.Trim();
                cliente.DependenciaOfic = txtDependenciaEmp.Text.Trim();
                cliente.UbicacionOfic = txtUbicacionEmp.Text.Trim();

                if (rdoPEP.SelectedIndex == 0)
                {
                    cliente.PEP = 1;
                    cliente.PEPNombre = txtNomPEP.Text.Trim();
                    cliente.PEPPuesto = txtPuePEP.Text.Trim();
                    cliente.PEPParentesco = Convert.ToInt32(cboParPep.SelectedValue);
                }
                else
                {
                    cliente.PEP = 0;
                    cliente.PEPNombre = "";
                    cliente.PEPPuesto = "";
                    cliente.PEPParentesco = 0;
                }
                cliente.ViveCasa = Convert.ToInt32(cboTipViv.SelectedValue);

                if (cboColoniaEmp.SelectedIndex > 0)
                {
                    cliente.IdCoOfic = Convert.ToInt32(cboColoniaEmp.SelectedValue);
                }

                cliente.IngresoBruto = (!string.IsNullOrEmpty(txtIngresoBruto.Text.Trim())) ? Convert.ToDecimal(txtIngresoBruto.Text.Trim()) : 0;
                cliente.Ingreso = (!string.IsNullOrEmpty(txtIng.Text.Trim())) ? Convert.ToDecimal(txtIng.Text.Trim()) : 0;
                cliente.OtrosIngr = (!string.IsNullOrEmpty(txtOtrIng.Text.Trim())) ? Convert.ToDecimal(txtOtrIng.Text) : 0;
                cliente.FuenOtroi = txtFuente.Text.ToUpper().Trim();
                cliente.DescuentosLey = (!string.IsNullOrEmpty(txtDescuentosLey.Text.Trim())) ? Convert.ToDecimal(txtDescuentosLey.Text.Trim()) : 0;
                cliente.OtrosDescuentos = (!string.IsNullOrEmpty(txtOtrosDescuentos.Text.Trim())) ? Convert.ToDecimal(txtOtrosDescuentos.Text.Trim()) : 0;

                using (wsSOPF.ClientesClient wsCliente = new wsSOPF.ClientesClient())
                {
                    ResultadoOfboolean resultado = new ResultadoOfboolean();
                    string Ventana = "";
                    try
                    {
                        if (Convert.ToInt32(Request.QueryString["idcliente"]) != 0)                        
                            cliente.IdCliente = Convert.ToInt32(Request.QueryString["idcliente"]);                        
                        else                        
                            cliente.IdCliente = 0;                        

                        resultado = wsCliente.InsertarTBClientes(cliente);

                        if (resultado.CodigoStr == "1")
                        {
                            // En PERU no hay vistas exito.Visible = true;
                            int idsolicitud = 0;
                            if (Request.QueryString["idsolicitud"] != null)
                            {
                                idsolicitud = Convert.ToInt32(Request.QueryString["idsolicitud"].ToString());
                            }

                            int tempIdClienteDireccion = 0;
                            if (Session["idClienteDireccion"] != null)
                                int.TryParse(Session["idClienteDireccion"].ToString(), out tempIdClienteDireccion);

                            int tempIdCliente = 0;
                            if (cliente.IdCliente <= 0 && resultado.Mensaje != null && resultado.Mensaje.Length > 0)
                                int.TryParse(resultado.Mensaje.ToString(), out tempIdCliente);
                            else
                                tempIdCliente = (int)cliente.IdCliente;

                            SolicitudDireccion direccion = new SolicitudDireccion();
                            direccion.idClienteDireccion = tempIdClienteDireccion;
                            direccion.idCliente = tempIdCliente;
                            direccion.direccion = cliente.Direccion;
                            direccion.idTipoDireccion = cliente.TipoDireccion;
                            direccion.nombreVia = cliente.NombreVia;
                            direccion.numeroExterno = cliente.NumExterior;
                            direccion.numeroInterno = cliente.NumInterior;
                            direccion.manzana = cliente.Manzana;
                            direccion.lote = cliente.Lote;
                            direccion.tipoZona = cliente.TipoZona;
                            direccion.nombreZona = cliente.NombreZona;
                            direccion.idColonia = Convert.ToInt32(cliente.IdColonia);
                            direccion.IdUsuario = tempIdUsuario;

                            // Solicitud de credito
                            string folio = txtFolioSolicitud.Text;
                            int idPromotor = Convert.ToInt32(ddlPromotor.SelectedValue);
                            int idCanal = Convert.ToInt32(ddlCanalVentas.SelectedValue);
                            string score = txtScoreExperian.Text;
                            int idOrigen = Convert.ToInt32(ddlOrigen.SelectedValue);
                            int idTipoCliente = Convert.ToInt32(hdnTipoCliente.Value);
                            string categoriaCliente = txtCategoriaCliente.Text;
                            int idProducto = Convert.ToInt32(ddlProducto.SelectedValue);
                            int idSubProducto = Convert.ToInt32(ddlSubProducto.SelectedValue);
                            int idTipoCredito = Convert.ToInt32(hdnTipoCredito.Value);

                            SolicitudClient wsSolicitud = new SolicitudClient();
                            if (idsolicitud > 0)
                            {
                                direccion.idSolicitud = Convert.ToInt32(idsolicitud);
                                wsSolicitud.ActualizarDireccionSolicitud(direccion);
                                wsSolicitud.InsertarSolicitud_Peru(6, tempIdUsuario, new TBSolicitudes.SolicitudPeru {
                                    IDSolictud = idsolicitud,
                                    Pedido = folio,
                                    IDPromotor = idPromotor,
                                    IDCanalVenta = idCanal,
                                    ScoreExperian = decimal.Parse(score),
                                    IDOrigen = idOrigen,
                                    IDSubProducto = idSubProducto,
                                    PrimerPago = DateTime.Now,
                                    IDCliente = tempIdCliente,
                                    TipoCredito = idTipoCredito
                                });
                                Ventana = "frmEditarSolicitud.aspx?idcliente=" + resultado.Mensaje.ToString() 
                                    + "&idsolicitud=" + idsolicitud.ToString()
                                    + "&folio=" + folio
                                    + "&idPromotor=" + idPromotor.ToString()
                                    + "&idCanal=" + idCanal.ToString()
                                    + "&score=" + score
                                    + "&idOrigen=" + idOrigen.ToString()
                                    + "&idTipoCliente=" + idTipoCliente.ToString()
                                    + "&categoriaCliente=" + categoriaCliente
                                    + "&idProducto=" + idProducto.ToString()
                                    + "&idSubProducto=" + idSubProducto.ToString()
                                    + "&idTipoCredito=" + idTipoCredito.ToString();
                            }
                            else
                            {
                                if (direccion.idClienteDireccion > 0)
                                {
                                    wsSolicitud.ActualizarDireccionSolicitud(direccion);
                                }
                                else
                                {
                                    direccion.idSolicitud = 0;
                                    wsSolicitud.InsertarDireccionSolicitud(direccion);
                                }
                                Ventana = "frmTipoCredPeru.aspx?idCliente=" + resultado.Mensaje.ToString() 
                                    + "&idsolicitud=" + idsolicitud.ToString()
                                    + "&folio=" + folio
                                    + "&idPromotor=" + idPromotor.ToString()
                                    + "&idCanal=" + idCanal.ToString()
                                    + "&score=" + score
                                    + "&idOrigen=" + idOrigen.ToString()
                                    + "&idTipoCliente=" + idTipoCliente.ToString()
                                    + "&categoriaCliente=" + categoriaCliente
                                    + "&idProducto=" + idProducto.ToString()
                                    + "&idSubProducto=" + idSubProducto.ToString()
                                    + "&idTipoCredito=" + idTipoCredito.ToString();
                            }
                            if (referencias != null && referencias.Count > 0)
                            {
                                foreach (var referencia in referencias)
                                {
                                    referencia.IdCliente = (referencia.IdCliente <= 0) ? tempIdCliente : referencia.IdCliente;
                                }
                                wsCliente.ActualizarClientesReferenciaMasivo(tempIdUsuario, referencias.ToArray());
                            }

                            Response.Redirect(Ventana);
                        }
                        else
                        {
                            throw new Exception(resultado.Mensaje);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Ha ocurrido un error: " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                pnlError.Visible = true;
            }
        }

        upValDatosSolicitudCredito.Update();
        upValDatosSolicitante.Update();
        upValDatosCony.Update();
        upValDatosLaborales.Update();
        upValReferencias.Update();
        upValDatosNegocio.Update();
    }

    protected void btnBuscarCliente_Click(object sender, EventArgs e)
    {
        TBClientes.BusquedaCliente[] clientes = null;
        List<TBClientes.BusquedaCliente> listacliente = new List<TBClientes.BusquedaCliente>();

        using (wsSOPF.ClientesClient wsCliente = new ClientesClient())
        {
            clientes = wsCliente.BusquedaCliente(14, 0, 0, txtBuscarCliente.Text, DateTime.Now, DateTime.Now, 0);
        }

        gvClientes.DataSource = clientes;
        gvClientes.DataBind();
    }

    protected void gvClientes_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        GridView gv = (GridView)sender;
        gv.PageIndex = e.NewPageIndex;
        TBClientes.BusquedaCliente[] clientes = null;
        List<TBClientes.BusquedaCliente> listacliente = new List<TBClientes.BusquedaCliente>();

        using (wsSOPF.ClientesClient wsCliente = new ClientesClient())
        {
            clientes = wsCliente.BusquedaCliente(14, 0, 0, txtBuscarCliente.Text, DateTime.Now, DateTime.Now, 0);
        }

        if (clientes.Length > 0)
        {
            gvClientes.DataSource = clientes;
            gvClientes.DataBind();
        }
    }

    protected void gvClientes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onClick", "javascript:void redirectCliente(this);");
            e.Row.Attributes.Add("style", "cursor: pointer");
        }
    }

    protected void ddlDireccion_SelectedIndexChange(object sender, EventArgs e)
    {
        bool visible = (ddlDireccion.SelectedItem.Text == "N/A");

        pnlDatosVia.Visible = !visible;
        pnlDatosLote.Visible = visible;
    }

    protected void ddlTipoZona_SelectedIndexChange(object sender, EventArgs e)
    {
        string strTipoZona = ddlTipoZona.SelectedItem.Text.ToUpper();
        if (strTipoZona == "N/A" || strTipoZona == "N/A.")
        {
            txtNombreZona.Text = "";
            txtNombreZona.Enabled = false;
        }
        else
        {
            txtNombreZona.Enabled = true;
        }
    }

    protected void ddlSituacionLaboral_SelectedIndexChange(object sender, EventArgs e)
    {
        AlternarCamposPension();
        if (cboTipoCon.Enabled == true)
        {
            cboTipoCon.SelectedIndex = cboTipoCon.Items.IndexOf(cboTipoCon.Items.FindByText("CAS"));        
        }
        string strProducto = ddlProducto.SelectedItem.Text.ToUpper().Trim();
        string strSituLabo = ddlSituacionLaboral.SelectedItem.Text.ToUpper().Trim();
       
            ActualizarConfiguracionEmpresa(sender, e);
        if (strProducto == "SUM")
        {
            if (strSituLabo == "INDEPENDIENTE")
            {
                registrarScript("abrir();");
            }
            else
            {
                registrarScript("ocultar();");
            }
        }
        else
        {
            registrarScript("ocultar();");
        }
    }
    public void registrarScript(String pScript, String ScriptKey = "regscript")
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), pScript, true);
    }

    public void registrarScript(Page pPage, String pScript, String ScriptKey = "regscript")
    {
        ScriptManager.RegisterStartupScript(pPage, pPage.GetType(), Guid.NewGuid().ToString(), pScript, true);
    }
    protected void AlternarCamposPension()
    {
        string strSituacionLaboral = ddlSituacionLaboral.SelectedItem.Text.ToUpper();
        string strProducto = ddlProducto.SelectedItem.Text.ToUpper().Trim();


        if ((strProducto == "SUM" && strSituacionLaboral == "DEPENDIENTE") ||  (strProducto == "SUM" && strSituacionLaboral == "INDEPENDIENTE") || (strSituacionLaboral == "JUBILADO" || strSituacionLaboral == "PENSIONISTA"))
        {
            if (strProducto == "SUM" && strSituacionLaboral == "DEPENDIENTE")
            {
                cboTipoCon.Enabled = true;
                divTipoCon.Visible = true;
                cboTipoPen.Enabled = false;
                divTipoPension.Visible = false;
                cboTipoInd.Enabled = false;
                divTipoInd.Visible = false;
                cboTipoCon.Items.FindByText("Dependiente").Enabled = false;
                cboTipoCon.Items.FindByText("Independiente").Enabled = false;
                cboTipoCon.Items.FindByText("Jubilado").Enabled = false;
                cboTipoCon.Items.FindByText("Pensionista").Enabled = false;
            }
            else if (strProducto == "SUM" && strSituacionLaboral == "INDEPENDIENTE")
            {
                cboTipoInd.Enabled = true;
                divTipoInd.Visible = true;
                cboTipoCon.Enabled = false;
                divTipoCon.Visible = false;
                cboTipoPen.Enabled = false;
                divTipoPension.Visible = false;

            }
            else
            { 
                cboTipoPen.Enabled = true;
                divTipoPension.Visible = true;
                cboTipoCon.Enabled = false;
                divTipoCon.Visible = false;
                cboTipoInd.Enabled = false;
                divTipoInd.Visible = false;
                AlternarVisibilidadDireccionLaboral(false);
            }
        }
        else
        {
            cboTipoPen.Enabled = false;
            divTipoPension.Visible = false;
            cboTipoCon.Enabled = false;
            divTipoCon.Visible = false;
            cboTipoInd.Enabled = false;
            divTipoInd.Visible = false;
            AlternarVisibilidadDireccionLaboral(true);
        }
        upDatosLaborales.Update();
    }

    protected void rdoPEP_SelectedIndexChange(object sender, EventArgs e)
    {
        if (rdoPEP.SelectedIndex == 0)
        {
            divPEP.Visible = true;
        }
        else
        {
            divPEP.Visible = false;
            txtNomPEP.Text = "";
            txtPuePEP.Text = "";
        }
    }

    protected void lbtnListaNegra_Click(object sender, EventArgs e)
    {
        gvListaNegra.Visible = !gvListaNegra.Visible;
        lbtnListaNegra.Text = (gvListaNegra.Visible) ? lbtnListaNegra.Text.Replace("Mostrar", "Ocultar") : lbtnListaNegra.Text.Replace("Ocultar", "Mostrar");
    }

    protected void ValidarListaNegraNombre(object sender, EventArgs e)
    {
        pnlListaNegra.Visible = false;
        pnlListaNegraGuardar.Visible = false;

        if (!string.IsNullOrEmpty(txtNombre.Text.Trim()) && !string.IsNullOrEmpty(txtApePat.Text.Trim()) && !string.IsNullOrEmpty(txtApeMat.Text.Trim()))
        {
            using (wsSOPF.SolicitudClient ws = new SolicitudClient())
            {
                wsSOPF.TBSolicitudes.ListaNegra lnl = new TBSolicitudes.ListaNegra();
                lnl.Nombre = txtNombre.Text.Trim();
                lnl.ApellidoPaterno = txtApePat.Text.Trim();
                lnl.ApellidoMaterno = txtApeMat.Text.Trim();

                wsSOPF.ResultadoOfArrayOfTBSolicitudesListaNegraXs2b9qnq res = ws.ValidaListaNegra(lnl);

                if (res.Codigo > 0)
                    throw new Exception(res.Mensaje);

                if (res.ResultObject.Length > 0)
                {
                    pnlListaNegra.Visible = true;
                    pnlListaNegraGuardar.Visible = true;
                    gvListaNegra.DataSource = res.ResultObject;
                    gvListaNegra.DataBind();
                }
            }
        }
    }

    protected void ValidarListaNegraDNI(object sender, EventArgs e)
    {
        cvDNIListaNegra.IsValid = true;

        if (!string.IsNullOrEmpty(txtDNISolicitante.Text.Trim()))
        {
            using (wsSOPF.SolicitudClient ws = new SolicitudClient())
            {
                wsSOPF.TBSolicitudes.ListaNegra lnl = new TBSolicitudes.ListaNegra();
                lnl.DNI = txtDNISolicitante.Text.Trim();

                wsSOPF.ResultadoOfArrayOfTBSolicitudesListaNegraXs2b9qnq res = ws.ValidaListaNegra(lnl);

                if (res.Codigo > 0)
                    throw new Exception(res.Mensaje);

                if (res.ResultObject.Length > 0)
                {
                    cvDNIListaNegra.IsValid = false;
                }
            }
        }
    }

    protected void cboEstCiv_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strEstadoCivil = cboEstCiv.SelectedItem.Text.ToUpper().Trim();
        string strProducto = ddlProducto.SelectedItem.Text.ToUpper().Trim();

        if ((strEstadoCivil == "CASADO" & strProducto == "SUM") || (strEstadoCivil == "CASADO" || strEstadoCivil == "CONVIVIENTE/UNIÓN LIBRE"))
        {
            camposConyuge(true);
        }
        //else if (strEstadoCivil == "CASADO" & strProducto == "SUM")
        //{
        //    camposConyuge(true);
        //}
        else
        {
            camposConyuge(false);
        }
        upDatosCony.Update();
    }

    void camposConyuge(bool valor)
    {
        PanelConyu.Visible = valor;
        txtNombreCony.Enabled = valor;
        rfvNombreCony.Enabled = valor;
        /*
    txtApePatCony.Enabled = valor;
    rfvApePatCony.Enabled = valor;
    txtApeMatCony.Enabled = valor;
    */
        ddlTipoDocCony.Enabled = valor;
        txtNumDocCony.Enabled = valor;
        rfvNumDocCony.Enabled = valor;

        txtEmpresaCony.Enabled = valor;
        rfvEmpresaCony.Enabled = false;// valor;
        txtRUCCony.Enabled = valor;
        txtPuestoCony.Enabled = valor;
        rfvPuestoCony.Enabled = false;// valor;
        txtAntiguedadCony.Enabled = valor;
        revAntiguedadCony.Enabled = false;// valor;
        txtIngCony.Enabled = valor;
        rfvIngCony.Enabled = false;// valor;
        revIngCony.Enabled = false;// valor;

        txtDireccionCony.Enabled = valor;
        rfvDireccionCony.Enabled = false;// valor; 
        txtDireccionLaboralCony.Enabled = valor;
        rfvDireccionLaboralCony.Enabled = false;// valor;

        txtTelefonoCony.Enabled = valor;
        revTelefonoCony.Enabled = false;// valor;
        txtTelefonoLaboralCony.Enabled = valor;
        revTelefonoLaboralCony.Enabled = false;// valor;
    }

    string quitarEspacios(string str)
    {
        str = str.Trim();
        str = str.Replace(" ", "[]");
        str = str.Replace("][", "");
        str = str.Replace("[]", " ");
        return str;
    }

    public bool IsValidGroup(string Group)
    {
        bool obReturn = true;
        foreach (IValidator v in Page.GetValidators(Group))
        {
            if (!v.IsValid)
            {
                obReturn = false;
            }
        }
        return obReturn;
    }

    protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
    {
        ActualizarConfiguracionEmpresa(sender, e);
        upDatosLaborales.Update();
    }

    protected void ActualizarConfiguracionEmpresa(object sender, EventArgs e)
    {
        try
        {
            int IdEmpresa = 0;
            int IdSituacionLaboral = 0;
            if (int.TryParse(ddlEmpresa.SelectedValue, out IdEmpresa) && int.TryParse(ddlSituacionLaboral.SelectedValue, out IdSituacionLaboral))
            {
                // Actualizar config captura planilla virtual con el IdEmpresa obtenido
                ActualizarConfiguracionCapturaPlanillaVirtual(IdEmpresa);

                ObtenerConfiguracionEmpresaResponse Respuesta = new ObtenerConfiguracionEmpresaResponse();
                ObtenerConfiguracionEmpresaRequest Peticion = new ObtenerConfiguracionEmpresaRequest()
                {
                    Accion = "OBTENER_POR_CAMPOS",
                    IdEmpresa = IdEmpresa,
                    IdSituacionLaboral = IdSituacionLaboral
                };

                using (CatalogoClient wsCatalogo = new CatalogoClient())
                {
                    Respuesta = wsCatalogo.ObtenerConfiguracionEmpresa(Peticion);
                }

                if (!Respuesta.Error && Respuesta.Resultado.Count() > 0)
                {
                    lblEmpresaSinConfiguracion.Visible = false;
                    ddlOrgano.SelectedIndex = ddlOrgano.Items.IndexOf(ddlOrgano.Items.FindByValue(Respuesta.Resultado[0].IdOrganoPago.ToString()));
                    txtNivelRiesgo.Text = Respuesta.Resultado[0].NivelRiesgo;
                    cboTipoPen.SelectedIndex = cboTipoPen.Items.IndexOf(cboTipoPen.Items.FindByValue(Respuesta.Resultado[0].IdRegimenPension.ToString()));
                    ddlSituacionLaboral.SelectedIndex = ddlSituacionLaboral.Items.IndexOf(ddlSituacionLaboral.Items.FindByValue(Respuesta.Resultado[0].IdSituacionLaboral.ToString()));
                    AlternarCamposPension();
                    AlternarOpcionesSituacionLaboralRegimenPension(Respuesta.ComboSituacionLaboral, Respuesta.ComboRegimenPension);                    
                }
                else if (Respuesta.ComboSituacionLaboral.Count() > 0)
                {
                    lblEmpresaSinConfiguracion.Visible = true;
                    lblEmpresaSinConfiguracion.Text = EMPRESA_SIN_CONFIGURACION;
                    AlternarOpcionesSituacionLaboralRegimenPension(Respuesta.ComboSituacionLaboral, Respuesta.ComboRegimenPension);
                    LimpiarCamposEmpresa();
                }
                else
                {
                    lblEmpresaSinConfiguracion.Visible = false;
                    LimpiarCamposEmpresa();
                    throw new Exception(Respuesta.MensajeOperacion);
                }
            }
        }
        catch (Exception ex)
        {
            lblError.Text = "Ha ocurrido un error al obtener los datos de la empresa o la empresa no tiene configuraciones. Error: " + (string.IsNullOrWhiteSpace(ex.Message) ? "conexión." : ex.Message);
            pnlError.Visible = true;
        }
    }

    protected void ActualizarConfiguracionCapturaPlanillaVirtual(int IdEmpresa)
    {
        // Actualizar los controles de captura si se recibe un IdEmpresa diferente al actual.
        int IdEmpresaActual = 0;
        int.TryParse(lblIdEmpresaCurrent.Text, out IdEmpresaActual);

        if (IdEmpresa != IdEmpresaActual)
        {
            using (wsSOPF.CatalogoClient ws = new CatalogoClient())
            {
                CapturaPlanillaVirtualConfig config = ws.ObtenerConfiguracionCapturaPlanillaVirtual(IdEmpresa).ToList().FirstOrDefault();

                if (config != null)
                {
                    lblUsuarioPlanillaVirtual.Text = config.DescripcionCampoUsuario.Trim();
                    lblClavePlanillaVirtual.Text = config.DescripcionCampoClave.Trim();
                    pnlUsuarioPlanillaVirtual.Visible = config.VisibleCampoUsuario;
                    pnlClavePlanillaVirtual.Visible = config.VisibleCampoClave;

                    rfv_txtPlanillaVirtualUsuario.ErrorMessage = string.Format("Capture el campo {0}", config.DescripcionCampoUsuario);
                    rfv_txtPlanillaVirtualClave.ErrorMessage = string.Format("Capture el campo {0}", config.DescripcionCampoClave);

                    txtPlanillaVirtualUsuario.Text = string.Empty;
                    txtPlanillaVirtualClave.Text = string.Empty;
                }
                else
                {
                    pnlUsuarioPlanillaVirtual.Visible = false;
                    pnlClavePlanillaVirtual.Visible = false;

                    txtPlanillaVirtualUsuario.Text = string.Empty;
                    txtPlanillaVirtualClave.Text = string.Empty;
                }

                // Actualizar Empresa Current
                lblIdEmpresaCurrent.Text = IdEmpresa.ToString();
            }
        }
    }

    protected void LimpiarCamposEmpresa()
    {
        if (ddlOrgano.Items.Count > 0)
            ddlOrgano.SelectedIndex = ddlOrgano.Items.IndexOf(ddlOrgano.Items.FindByValue("NA"));
        txtNivelRiesgo.Text = "N/A";
    }

    protected void AlternarVisibilidadDireccionLaboral(bool valor)
    {
        pnlDireccionLaboral.Visible = valor;
        if (!valor)
            LimpiarCamposDireccionLaboral();
    }

    protected void LimpiarCamposDireccionLaboral()
    {
        cboColoniaEmp.Items.Clear();
        cboMunicipioEmp.Items.Clear();
        cboEstadoEmp.SelectedIndex = cboEstadoEmp.Items.IndexOf(cboEstadoEmp.Items.FindByText("0-SELECCIONE UNA OPCION"));
        txtCalleEmp.Text = "";
        txtNumIntEmp.Text = "0";
        txtNumExtEmp.Text = "0";
        txtDependenciaEmp.Text = "";
        txtUbicacionEmp.Text = "";
        txtLadaEmp.Text = "";
        txtTelEmp.Text = "";
        txtAnexo.Text = "";
    }

    protected void AlternarOpcionesSituacionLaboralRegimenPension(Combo[] SituacionLaboral, Combo[] RegimenPension)
    {
        List<Combo> lstSL = SituacionLaboral.ToList();
        List<Combo> lstRP = RegimenPension.ToList();
        foreach (ListItem item in ddlSituacionLaboral.Items)
        {
            if (!lstSL.Exists(sl => sl.Valor == Convert.ToInt32(item.Value)))
            {
                item.Attributes["disabled"] = "disabled";
            }
        }
        foreach (ListItem item in cboTipoPen.Items)
        {
            if (!lstRP.Exists(tp => tp.Valor == Convert.ToInt32(item.Value)))
            {
                item.Attributes["disabled"] = "disabled";
            }
        }
    }

    #region "Metodos AJAX"

    [WebMethod]
    public static string ConsultarDireccionGMaps(string Calle, string NumeroExt, string Colonia, string Municipio, string Estado)
    {
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        string Direccion = string.Empty;
        int NumBusqueda = 1;

        try
        {
            MAGResponse res;

            do
            {
                switch (NumBusqueda)
                {
                    case 1:
                        Direccion = string.Format("{0} {1}, {2}, {3}, {4}", Calle, NumeroExt, Colonia, Municipio, Estado);
                        break;
                    case 2:
                        Direccion = string.Format("{0}, {1}, {2}, {3}", Calle, Colonia, Municipio, Estado);
                        break;
                    case 3:
                        Direccion = string.Format("{0}, {1}, {2}", Colonia, Municipio, Estado);
                        break;
                }

                // Consultar la direccion con la API de google maps.
                res = GoogleMapsAPI.GetLocation(Direccion);

                if (res.success)
                    return serializer.Serialize(new { latitud = res.location.lat, longuitud = res.location.lng });

                NumBusqueda++;
            }
            while (NumBusqueda <= 3);

            // Sino se encontro la direccion se envia el ultimo error obtenido.
            throw new Exception(res.message);
        }
        catch (Exception ex)
        {
            HttpContext.Current.Response.StatusCode = 500;
            return serializer.Serialize(new { mensaje = ex.Message });
        }
    }

    #endregion    

    protected void cboEstadoRef1_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idEstado = 0;
        TBCATCiudad[] ciudades = null;

        idEstado = Convert.ToInt32(cboEstadoRef1.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            ciudades = wsCatalogo.ObtenerTbCatCiudadOrig(2, idEstado, 0);
        }
        if (ciudades.Length > 0)
        {
            dboMunicipioRef1.DataValueField = "IdCiudad";
            dboMunicipioRef1.DataTextField = "Ciudad";
            dboMunicipioRef1.DataSource = ciudades;
            dboMunicipioRef1.DataBind();
        }
        dboMunicipioRef1_SelectedIndexChanged(sender, e);
    }

    protected void dboMunicipioRef1_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idCiudad = 0;
        TBCATColonia[] colonias = null;

        idCiudad = Convert.ToInt32(dboMunicipioRef1.SelectedValue);
        using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
        {
            colonias = wsCatalogo.ObtenerTbCatColonia(2, idCiudad);
        }
        if (colonias.Length > 0)
        {
            cboColoniaRef1.DataValueField = "IdColonia";
            cboColoniaRef1.DataTextField = "Colonia";
            cboColoniaRef1.DataSource = colonias;
            cboColoniaRef1.DataBind();
        }
    }

    protected void lnkSearchDNI_Click(object sender, EventArgs e)
    {
        TBClientes.BusquedaCliente[] clientes = null;

        using (wsSOPF.ClientesClient wsCliente = new ClientesClient())
        {
            clientes = wsCliente.BusquedaCliente(15, 0, 0, txtDNISolicitante.Text, DateTime.Now, DateTime.Now, 0);

            if (clientes.Any())
            {
                int idCliente = clientes.FirstOrDefault().IdCliente;
                string basePage = "frmClientePeru.aspx?";
                string pageParams = getSolicitudParams();

                Response.Redirect(basePage + (
                    !String.IsNullOrEmpty(pageParams) ?
                        pageParams + "&idcliente=" + idCliente.ToString()
                        : "idcliente=" + idCliente.ToString()
                ), true);
            }
            else
            {
                txtNombreSolicitante.Text = "Cliente no existente";
                txtTipoCliente.Text = "Cliente Nuevo";
                hdnTipoCliente.Value = "0";
                hdnTipoCredito.Value = "1";
                txtTipoCredito.Text = "PRIMER CRÉDITO";
                txtCategoriaCliente.Text = "No aplica";                
                lnkSearchDNI.Visible = false;
                lnkClearDNI.Visible = true;
                txtDNISolicitante.Enabled = false;
            }
        }

        // Validar Lista Negra DNI
        ValidarListaNegraDNI(sender, e);
    }

    protected void lnkClearDNI_Click(object sender, EventArgs e)
    {
        int IdCliente = Convert.ToInt32(Request.QueryString["idcliente"]);

        if (IdCliente > 0)
        {
            string basePage = "frmClientePeru.aspx";

            Response.Redirect(basePage, true);
        }
        else
        {
            lnkSearchDNI.Visible = true;
            lnkClearDNI.Visible = false;
            txtNombreSolicitante.Enabled = true;
            txtDNISolicitante.Enabled = true;
            txtNombreSolicitante.Text = "";
            txtDNISolicitante.Text = "";
            txtTipoCliente.Text = "";
            hdnTipoCliente.Value = "-1";
            txtCategoriaCliente.Text = "";
            hdnTipoCredito.Value = "";
            txtTipoCredito.Text = "";            
        }

    }

    protected void ddlTipoConvenio_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<Combo> Productos;
        int idTipoConvenio = Convert.ToInt32(ddlTipoConvenio.SelectedValue);

        if (idTipoConvenio > 0)
        {
            using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
            {
                Productos = wsCatalogo.ObtenerCombo_Convenio(idTipoConvenio).ToList();

                if (Productos.Count > 0)
                {
                    Productos.Add(new Combo { Valor = -1, Descripcion = "--SELECCIONAR--" });
                    ddlProducto.DataSource = Productos.OrderBy(p => p.Valor);
                    ddlProducto.DataBind();
                }
            }
        }
    }

    private string getSolicitudParams()
    {
        List<string> solicitudParams = new List<string>();

        // Solicitud de credito
        string folio = txtFolioSolicitud.Text;
        int idPromotor = Convert.ToInt32(ddlPromotor.SelectedValue);
        int idCanal = Convert.ToInt32(ddlCanalVentas.SelectedValue);
        string score = txtScoreExperian.Text;
        int idOrigen = Convert.ToInt32(ddlOrigen.SelectedValue);
        int idTipoCliente = !String.IsNullOrEmpty(hdnTipoCliente.Value) ? Convert.ToInt32(hdnTipoCliente.Value) : 0;
        string categoriaCliente = txtCategoriaCliente.Text;
        int idProducto = Convert.ToInt32(ddlProducto.SelectedValue);
        int idSubProducto = Convert.ToInt32(ddlSubProducto.SelectedValue);
        int idTipoCredito = !String.IsNullOrEmpty(hdnTipoCredito.Value) ? Convert.ToInt32(hdnTipoCredito.Value) : 0;

        if (!String.IsNullOrEmpty(folio)) { solicitudParams.Add("folio=" + folio); }
        if (idPromotor > 0) { solicitudParams.Add("idPromotor=" + idPromotor.ToString()); }
        if (idCanal > 0) { solicitudParams.Add("idCanal=" + idCanal.ToString()); }
        if (!String.IsNullOrEmpty(score)) { solicitudParams.Add("score=" + score); }
        if (idOrigen > 0) { solicitudParams.Add("idOrigen=" + idOrigen.ToString()); }
        if (idTipoCliente > 0) { solicitudParams.Add("idTipoCliente=" + idTipoCliente.ToString()); }
        if (!String.IsNullOrEmpty(categoriaCliente)) { solicitudParams.Add("categoriaCliente=" + categoriaCliente); }
        if (idProducto > 0) { solicitudParams.Add("idProducto=" + idProducto.ToString()); }
        if (idSubProducto > 0) { solicitudParams.Add("idSubProducto=" + idSubProducto.ToString()); }
        if (idTipoCredito > 0) { solicitudParams.Add("idTipoCredito=" + idTipoCredito.ToString()); }

        if (solicitudParams.Count > 0) { return String.Join("&", solicitudParams); }

        return String.Empty;
    }    
}