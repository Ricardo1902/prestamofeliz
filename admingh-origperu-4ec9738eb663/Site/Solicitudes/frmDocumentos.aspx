﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmDocumentos.aspx.cs" Inherits="Site_Solicitudes_frmDocumentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="css/frmDocumentos.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />
    <script type="text/javascript" src="../../js/pdfobject-2.2.4.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap/js/alert.js"></script>
    <script type="text/javascript" src="js/frmDocumentos.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="divCorrecto" runat="server" class="alert alert-success" role="alert" visible="false">
        <asp:Label ID="lblCorrecto" runat="server" Text="Favor de subir correctamente los documentos necesarios"></asp:Label>
    </div>
    <div id="divError" runat="server" class="alert alert-danger" role="alert" visible="false">
        <asp:Label ID="lblError" runat="server" Text="Favor de subir correctamente los documentos necesarios"></asp:Label>
    </div>

    <div class="row datosCliente">
        <div class="col-md-5 form-group">
            <label>Cliente:</label>
            <span id="datCliente" class="form-control"></span>
        </div>
        <div class="col-md-2 form-group">
            <label>Celular:</label>
            <span id="datCliente-celular" class="form-control"></span>
        </div>
        <div class="col-md-5 form-group">
            <label>Correo electrónico:</label>
            <span id="datCliente-email" class="form-control"></span>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Documentos -&nbsp;
                <asp:Label ID="lblidSolicitud" runat="server" Text=""></asp:Label></h3>
        </div>
        <div class="panel-body">
            <div id="cargaDocs" class="row">
                <button id="btnGenerar" type="button" class="btn btn-success" onclick="onGenerarDocumentacion()" disabled="disabled">
                    <span class="glyphicon glyphicon-floppy-saved"></span>&nbsp;Generar documentación
                </button>
            </div>
            <div id="documentos" class="row documentos">
                <%--<div class="row documento" style="display: none">
                    <div class="col-md-9 col-sm-12">
                        <div class="form-group">
                            <label>Formato de Carta de autorización de descuento por planilla</label>
                            <span class="condicionado-motivo">CLIENTE SE EVALUA COMO NUEVO Y SE PERMITE RCI HASTA 30% PERO ESTA EN 34%. Agregar el recibo actual y la foto del cliente por que no se visualiza bien.</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 documento-acciones">
                        <span class="btn btn-sm btn-span"><span class="glyphicon glyphicon-ok"></span></span>
                        <button id="cargDoc_0" type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-upload"></span>&nbsp;Cargar archivo</button>
                    </div>
                </div>--%>
            </div>
            <input type="file" id="docSeleccion" onchange="onChangeDocSeleccion();" class="invisible" />
        </div>
    </div>

    <div id="modCargaDoc" tabindex="-1" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Carga de Archivos</h4>
                </div>
                <div class="modal-body">
                    <div id="modCarga_documentos" class="row breadcrumb">
                    </div>
                    <div id="modCarga_vistaPrevia" class="row"></div>
                </div>
                <div class="modal-footer">
                    <div class="col-com-derecha">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-pr"></div>
</asp:Content>
