﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using wsSOPF;

public partial class Site_Solicitudes_frmDocumentos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder strScript = new StringBuilder();
        try
        {
            if (Request.QueryString["descargar"] != null && Request.QueryString["idSolicitud"] != null)
            {
                DescargarExpediente();
                Response.End();
            }
            if (!IsPostBack)
            {
                int idUsuario = HttpContext.Current.ValidarUsuario();
                if (idUsuario <= 0) throw new Exception("La clave del usuario no es válida.");

                int idSolicitud = 0;
                if (Request.QueryString["idsolicitud"] != null)
                {
                    int.TryParse(Request.QueryString["idsolicitud"].ToString(), out idSolicitud);
                }
                if (idSolicitud > 0)
                {
                    lblidSolicitud.Text = idSolicitud.ToString();

                    TBSolicitudes.Documentos[] documentos = ObtenerDocumentos(idUsuario, idSolicitud);
                    strScript.AppendFormat("documentos={0};", JsonConvert.SerializeObject(documentos));
                    strScript.AppendFormat("archivosCargados={0};", JsonConvert.SerializeObject(ObtenerDocumentosArchivos(idUsuario, idSolicitud, documentos, false)));
                    strScript.AppendFormat("cliente={0};", JsonConvert.SerializeObject(DatosCliente(idUsuario, idSolicitud)));
                }
            }
        }
        catch (Exception ex)
        {
            this.MostrarMensaje(true, "Ha ocurrido un error");
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
    }

    #region Metodos Privados
    private void MostrarMensaje(bool error, string mensaje)
    {
        divCorrecto.Visible = !error;
        divError.Visible = error;

        if (error)
        {
            lblError.Text = mensaje;
        }
        else
        {
            lblCorrecto.Text = mensaje;
        }
    }

    public static TBSolicitudes.Documentos[] ObtenerDocumentos(int idUsuario, int idSolicitud)
    {
        TBSolicitudes.Documentos[] documentos = null;
        try
        {
            if (idSolicitud <= 0) return documentos;
            using (SolicitudClient wsSolicitud = new SolicitudClient())
            {
                ObtenerSolicitudResponse respSolicitud = wsSolicitud.ObtenerSolicitud(idUsuario, idSolicitud);
                if (!respSolicitud.Error && respSolicitud.Solicitud != null)
                {
                    if (respSolicitud.Solicitud.IdEstatus == (int)EstatusSolicitud.Condicionada)
                    {
                        //Si la solicitutd está condicionada, traerse los documentos condicionados.
                        documentos = wsSolicitud.DocumentosCondicionados(new DocumentosCondicionadosRequest
                        {
                            IdUsuario = idUsuario,
                            IdSolicitud = idSolicitud
                        });
                        if (documentos == null || documentos.Length == 0)
                        {
                            documentos = wsSolicitud.ObtenerDocumentos(idSolicitud);
                        }
                    }
                    else
                    {
                        documentos = wsSolicitud.ObtenerDocumentos(idSolicitud);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Utilidades.EscribirLog(ex.Message + Environment.NewLine + "Método: " + MethodBase.GetCurrentMethod().DeclaringType.Name + "\\" + MethodBase.GetCurrentMethod().Name +
                   Environment.NewLine + "Datos: " + JsonConvert.SerializeObject(new { idSolicitud }), JsonConvert.SerializeObject(ex));
        }
        return documentos;
    }

    public static List<DocumentoArchivos> ObtenerDocumentosArchivos(int idUsuario, int idSolicitud, TBSolicitudes.Documentos[] documentos, bool conArchivos)
    {
        List<DocumentoArchivos> documentosArchivos = null;
        try
        {
            if (documentos != null && documentos.Length > 0)
            {
                using (wsSOPF.SolicitudClient wsS = new SolicitudClient())
                {
                    DocumentosArchivosResponse docArchivosResp = wsS.DocumentosArchivos(new DocumentosArchivosRequest
                    {
                        IdsDocumento = documentos.Select(d => d.idDocumento).ToArray(),
                        ConArchivo = conArchivos,
                        IdSolicitud = idSolicitud,
                        IdUsuario = idUsuario
                    });

                    if (!docArchivosResp.Error && docArchivosResp.Documentos != null && docArchivosResp.Documentos.Length > 0)
                    {
                        documentosArchivos = docArchivosResp.Documentos.Select(d => d.IdDocumento).Distinct()
                            .Select(idDocumento => new DocumentoArchivos
                            {
                                IdDocumento = idDocumento
                            })
                            .ToList();

                        foreach (DocumentoArchivos documento in documentosArchivos)
                        {
                            documento.Archivos = docArchivosResp.Documentos.Where(d => d.IdDocumento == documento.IdDocumento)
                                                            .Select(da => new DocumentoVM
                                                            {
                                                                Id = documento.IdDocumento * da.IdDocumentoArchivo,
                                                                IdDocumentoArchivo = da.IdDocumentoArchivo,
                                                                NombreArchivo = da.NombreDocumento,
                                                                Tipo = da.TipoMedio,
                                                                Contenido = da.Contenido
                                                            })
                                                            .ToList();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Utilidades.EscribirLog(ex.Message + Environment.NewLine + "Método: " + MethodBase.GetCurrentMethod().DeclaringType.Name + "\\" + MethodBase.GetCurrentMethod().Name +
                   Environment.NewLine + "Datos: " + JsonConvert.SerializeObject(new { idSolicitud }), JsonConvert.SerializeObject(ex));
        }
        return documentosArchivos;
    }

    protected object DatosCliente(int idUsuario, int idSolicitud)
    {
        object datosCliente = null;
        try
        {
            using (wsSOPF.ClientesClient wsC = new ClientesClient())
            {
                BuscarClienteResponse resp = wsC.BuscarCliente(new BuscarClienteRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitud = idSolicitud
                });

                if (!resp.Error && resp.Cliente != null)
                {
                    datosCliente = new
                    {
                        Cliente = resp.Cliente.Nombres + " " + (resp.Cliente.ApellidoPaterno + " " + resp.Cliente.ApellidoMaterno ?? "").Trim(),
                        resp.Cliente.Celular,
                        resp.Cliente.Email
                    };
                }
            }
        }
        catch (Exception ex)
        {
            Utilidades.EscribirLog(ex.Message + Environment.NewLine + "Método: " + MethodBase.GetCurrentMethod().DeclaringType.Name + "\\" + MethodBase.GetCurrentMethod().Name +
                   Environment.NewLine + "Datos: " + JsonConvert.SerializeObject(new { idSolicitud }), JsonConvert.SerializeObject(ex));
        }
        return datosCliente;
    }

    protected void DescargarExpediente()
    {
        int idUsuario = 0;
        int idSolicitud = 0;

        try
        {
            if (Session["UsuarioId"] != null) int.TryParse(Session["UsuarioId"].ToString(), out idUsuario);
            if (idUsuario <= 0) throw new Exception();
            if (Request.QueryString["idSolicitud"] != null) int.TryParse(Request.QueryString["idSolicitud"].ToString(), out idSolicitud);
            string nombreArchivo = "Expediente_" + idSolicitud + ".pdf";

            ExpedienteResponse archivoResp = null;
            using (wsSOPF.SolicitudClient wsS = new SolicitudClient())
            {
                archivoResp = wsS.Expediente(new ExpedienteRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitud = idSolicitud,
                    IdDocumento = 0,
                    ConArchivos = true
                });
            }
            if (archivoResp != null && !archivoResp.Error && archivoResp.Documentos != null && archivoResp.Documentos.Length > 0)
            {
                byte[] byteArchivo = Convert.FromBase64String(archivoResp.Documentos[0].Contenido);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "filename=" + nombreArchivo);
                Response.BinaryWrite(byteArchivo);
            }
            else throw new Exception();
        }
        catch (Exception ex)
        {
            Utilidades.EscribirLog(ex.Message + Environment.NewLine + "Método: " + MethodBase.GetCurrentMethod().DeclaringType.Name + "\\" + MethodBase.GetCurrentMethod().Name +
                   Environment.NewLine + "Datos: " + JsonConvert.SerializeObject(new { idSolicitud }), JsonConvert.SerializeObject(ex));
            Response.Write("No fue posible obtener el archivo");
        }
        Response.Flush();
    }
    #endregion

    #region WebMethods
    #region GET
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ValidaEstatus()
    {
        bool error = false;
        string url = string.Empty;
        string mensaje = string.Empty;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válido.");
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static object ObtenerArchivos(int idSolicitud, int[] idsDocumentoArchivos)
    {
        bool error = false;
        string url = string.Empty;
        string mensaje = string.Empty;
        List<DocumentoArchivos> documentosArchivos = null;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válido.");
            }
            if (idSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es válido.");
            if (idsDocumentoArchivos == null || idsDocumentoArchivos.Length <= 0) new ValidacionExcepcion("La clave del archivo no es válido.");

            using (wsSOPF.SolicitudClient wsS = new SolicitudClient())
            {
                DocumentosArchivosResponse docArchivosResp = wsS.DocumentosArchivos(new DocumentosArchivosRequest
                {
                    IdsDocumentoArchivo = idsDocumentoArchivos,
                    ConArchivo = true,
                    IdSolicitud = idSolicitud,
                    IdUsuario = idUsuario
                });

                if (!docArchivosResp.Error && docArchivosResp.Documentos != null && docArchivosResp.Documentos.Length > 0)
                {
                    documentosArchivos = docArchivosResp.Documentos.Select(d => d.IdDocumento).Distinct()
                        .Select(idDocumento => new DocumentoArchivos
                        {
                            IdDocumento = idDocumento
                        })
                        .ToList();

                    foreach (DocumentoArchivos documento in documentosArchivos)
                    {
                        documento.Archivos = docArchivosResp.Documentos.Where(d => d.IdDocumento == documento.IdDocumento)
                                                        .Select(da => new DocumentoVM
                                                        {
                                                            Id = documento.IdDocumento * da.IdDocumentoArchivo,
                                                            IdDocumentoArchivo = da.IdDocumentoArchivo,
                                                            NombreArchivo = da.NombreDocumento,
                                                            Tipo = da.TipoMedio,
                                                            Contenido = da.Contenido
                                                        })
                                                        .ToList();
                    }
                }
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url, documentosArchivos };
    }
    #endregion

    #region POST
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object CargarArchivo(CargaArchivo archivo)
    {
        bool error = false;
        string url = string.Empty;
        string mensaje = string.Empty;
        object cargaResult = null;

        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            int idSolicitud = 0;
            if (HttpContext.Current.Request.QueryString["idSolicitud"] != null)
            {
                int.TryParse(HttpContext.Current.Request.QueryString["idSolicitud"], out idSolicitud);
            }
            if (idSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es válido.");

            if (archivo == null || archivo.Archivo == null || string.IsNullOrEmpty(archivo.Archivo.Contenido) || string.IsNullOrEmpty(archivo.Archivo.Tipo))
                throw new ValidacionExcepcion("No es posible cargar el archivo.");

            List<wsSOPF.DocumentoVM> cargaDoc = new List<wsSOPF.DocumentoVM>()
            {
                new wsSOPF.DocumentoVM
                {
                    IdDocumento = archivo.IdDocumento,
                    NombreDocumento = archivo.Archivo.NombreArchivo,
                    Contenido = archivo.Archivo.Contenido,
                    TipoMedio = archivo.Archivo.Tipo
                }
            };

            CargarDocumentoArchivoResponse cargaResp = null;
            using (wsSOPF.SolicitudClient wsS = new SolicitudClient())
            {
                cargaResp = wsS.CargarDocumentoArchivo(new CargarDocumentoArchivoRequest
                {
                    Documentos = cargaDoc.ToArray(),
                    IdSolicitud = idSolicitud,
                    IdUsuario = idSolicitud
                });
            }

            if (cargaResp != null && cargaResp.DocumentoArchivo != null && cargaResp.DocumentoArchivo.Length > 0)
            {
                cargaResult = new
                {
                    archivo.Archivo.Id,
                    archivo.IdDocumento,
                    cargaResp.DocumentoArchivo[0].IdDocumentoArchivo
                };

                mensaje = "Archivo cargado correctamente.";
            }
            else
            {
                error = true;
                mensaje = "Ocurrió un error mientras se realizaba la carga del archivo.";
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url, cargaResult };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object RemoverArchivo(int idDocumentoArchivo)
    {
        bool error = false;
        string url = string.Empty;
        string mensaje = string.Empty;

        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            int idSolicitud = 0;
            if (HttpContext.Current.Request.QueryString["idSolicitud"] != null)
            {
                int.TryParse(HttpContext.Current.Request.QueryString["idSolicitud"], out idSolicitud);
            }
            if (idSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es válido.");
            if (idDocumentoArchivo <= 0) throw new ValidacionExcepcion("La clave del archivo no es váldo.");

            using (wsSOPF.SolicitudClient wsS = new SolicitudClient())
            {
                Respuesta eliminarArchivoDocResp = wsS.EliminarDocumentoArchivo(new EliminarDocumentoArchivoRequest
                {
                    IdUsuario = idUsuario,
                    IdDocumentoArchivo = idDocumentoArchivo
                });

                error = eliminarArchivoDocResp.Error;
                mensaje = eliminarArchivoDocResp.MensajeOperacion;
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object GenerarDocumentacion(int totalArchivos)
    {
        bool error = false;
        string url = string.Empty;
        string mensaje;
        int documentoArchivosBD = 0;
        try
        {
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válida.");
            }
            int idSolicitud = 0;
            if (HttpContext.Current.Request.QueryString["idSolicitud"] != null)
            {
                int.TryParse(HttpContext.Current.Request.QueryString["idSolicitud"], out idSolicitud);
            }
            if (idSolicitud <= 0) throw new ValidacionExcepcion("El número de solicitud no es válido.");

            #region Verificar documentos
            TBSolicitudes.Documentos[] documentos = ObtenerDocumentos(idUsuario, idSolicitud);
            if (documentos != null && documentos.Length > 0)
            {
                List<DocumentoArchivos> documentosArchivos = ObtenerDocumentosArchivos(idUsuario, idSolicitud, documentos, false);
                if (documentosArchivos != null && documentosArchivos.Count > 0)
                {
                    foreach (DocumentoArchivos doc in documentosArchivos)
                    {
                        if (doc.Archivos != null)
                            documentoArchivosBD += doc.Archivos.Count;
                    }
                }
            }
            if (totalArchivos != documentoArchivosBD) throw new ValidacionExcepcion("Existen documentos que no pueden ser generados." +
                   "\nFavor de verificar que el proceso de carga de los archivos se hayan completado.");
            #endregion

            #region GenerarEnvio
            using (SolicitudClient wsS = new SolicitudClient())
            {
                Respuesta generaDocResp = wsS.GeneraDocumentoArchivoExpediente(new GeneraDocumentoArchivoExpedienteRequest
                {
                    IdSolicitud = idSolicitud,
                    IdUsuario = idUsuario
                });

                error = generaDocResp.Error;
                mensaje = string.IsNullOrEmpty(generaDocResp.MensajeOperacion) ?
                    generaDocResp.Error ? "Ocurrió un error al momento de generar la documentación" : "La carga de los documentos se ha realizado correctamente." : generaDocResp.MensajeOperacion;

                if (error) throw new ValidacionExcepcion(mensaje);

                //El expediente se cargó bien.
                #region ProcesoDigital/MC
                int resultado = 0;
                InicarSolicitudProcesoDigitalResponse iniciarProcesoD = wsS.IniciarProcesoOriginacionD(new InicarSolicitudProcesoDigitalRequest
                {
                    IdSolicitud = idSolicitud,
                    IdUsuario = idUsuario
                });

                if (iniciarProcesoD.Error) throw new Exception("Error: " + iniciarProcesoD.MensajeOperacion);

                if (!iniciarProcesoD.EnviaProcesoDigital)
                {
                    resultado = wsS.UpdEstProceso(3, idSolicitud, 8, idUsuario);
                }
                else
                {
                    mensaje += iniciarProcesoD.MensajeOperacion;
                }
                #endregion
            }
            #endregion
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url };
    }

    #endregion
    #endregion

    public class DocumentoArchivos
    {
        [JsonProperty("idDocumento")]
        public int IdDocumento { get; set; }
        [JsonProperty("archivos")]
        public List<DocumentoVM> Archivos { get; set; }
    }

    public class DocumentoVM
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("contenido")]
        public string Contenido { get; set; }
        [JsonProperty("nombreArchivo")]
        public string NombreArchivo { get; set; }
        [JsonProperty("tipo")]
        public string Tipo { get; set; }
        [JsonProperty("idDocumentoArchivo")]
        public int IdDocumentoArchivo { get; set; }
    }

    public class CargaArchivo
    {
        [JsonProperty("idDocumento")]
        public int IdDocumento { get; set; }
        [JsonProperty("archivo")]
        public DocumentoVM Archivo { get; set; }
    }
}