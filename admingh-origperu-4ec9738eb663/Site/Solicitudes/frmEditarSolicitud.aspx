﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmEditarSolicitud.aspx.cs" Inherits="Site_Solicitudes_frmEditarSolicitud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="css/frmEditarSolicitud.css?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>" rel="stylesheet" />
    <script type="text/javascript" src="js/frmEditarSolicitud.js?v=<%=DateTime.Now.ToString("yyMMddHHmm") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Informacion que Desea Editar</h3>
        </div>
        <div class="panel-body">
            <div class="col-md-6">
                <asp:RadioButtonList ID="rdTipoEdicion" CssClass="small" runat="server" OnSelectedIndexChanged="rdTipoEdicion_SelectedIndexChange"
                    AutoPostBack="true">
                    <asp:ListItem Value="1">&nbsp;Datos del Cliente&nbsp;</asp:ListItem>
                    <asp:ListItem Value="2">&nbsp;Datos del Crédito&nbsp;</asp:ListItem>
                    <asp:ListItem Value="3">&nbsp;Expediente&nbsp;</asp:ListItem>

                </asp:RadioButtonList>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-8">
                        <div id="lstProcesos" class="list-group">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer clearfix">
            <div class="col-sm-6">
                <asp:Button ID="btnGuardar" Text="Reactivar" CssClass="btn btn-sm btn-primary" runat="server"
                    OnClick="btnGuardarClick" />
                <asp:Button ID="btnCancelar" Text="Cancelar" CssClass="btn btn-sm btn-danger" runat="server"
                    OnClick="btnCancelar_Click" />
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlAdvertencia" CssClass="alert alert-warning col-md-12 col-sm-12" Style="padding: 4px 20px;" role="alert" Visible="false" runat="server">
        <asp:Label ID="lblAdvertencia" runat="server" Text="¿Está seguro(a) de cancelar la solicitud?"></asp:Label>
        <asp:Button ID="btnConfirmar" Text="Confirmar" CssClass="btn btn-sm btn-success" runat="server" OnClick="btnConfirmar_Click" />
        <asp:Button ID="btnRechazar" Text="Rechazar" CssClass="btn btn-sm btn-danger" runat="server" OnClick="btnRechazar_Click" />
    </asp:Panel>

    <asp:Panel ID="pnlError" CssClass="alert alert-danger col-md-6 col-sm-8" Style="padding: 4px 20px;" role="alert" Visible="false" runat="server">
        <asp:Label ID="lblError" runat="server"></asp:Label>
    </asp:Panel>
    <asp:Panel ID="pnlExito" CssClass="alert alert-success col-md-6 col-sm-8" Style="padding: 4px 20px;" role="alert" Visible="false" runat="server">
        <asp:Label ID="lblExito" runat="server"></asp:Label>
    </asp:Panel>

    <div id="modReinicio" class="modal fade" role="dialog">
        <div class="modal-dialog" role="dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reiniciar Proceso</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label for="txComenario">Motivo:</label>
                            <textarea id="txComentario" class="form-control" placeholder="motivo por el que se reinicia el proceso" cols="500" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-com-derecha">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="button" id="btnReiniciar" class="btn btn-success">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

