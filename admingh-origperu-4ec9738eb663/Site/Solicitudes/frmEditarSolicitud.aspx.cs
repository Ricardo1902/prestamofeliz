﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using wsSOPF;

public partial class Site_Solicitudes_frmEditarSolicitud : System.Web.UI.Page
{
    // 38 - GERENTE REGIONAL
    // 41 - GERENTE SUCURSAL
    protected string[] RolUsuariosPermitidosCancelar = new string[] { "38", "41" };
    // 21 - PRODUCCIONTI
    protected string[] UsuariosPermitidosCancelar = new string[] { "21" };
    private bool puedeCancelar = false;
    Dictionary<int, string> proceso = new Dictionary<int, string>()
    {
        {1, "glyphicon-camera" },
        {2, "glyphicon-pencil" }
    };

    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder strScript = new StringBuilder();
        if (!IsPostBack)
        {
            if (Session["UsuarioId"] != null && Session["Rol"] != null)
            {
                foreach (string x in UsuariosPermitidosCancelar)
                {
                    if (Session["UsuarioId"].ToString().Contains(x))
                    {
                        puedeCancelar = true;
                        break;
                    }
                }
                if (!puedeCancelar)
                {
                    foreach (string x in RolUsuariosPermitidosCancelar)
                    {
                        if (Session["Rol"].ToString().Contains(x))
                        {
                            puedeCancelar = true;
                            break;
                        }
                    }
                }
            }
            int idcliente = 0;
            int idsolicitud = 0;

            idcliente = Convert.ToInt32(Request.QueryString["idCliente"]);
            idsolicitud = Convert.ToInt32(Request.QueryString["idsolicitud"]);

            TBSolicitudes.DatosSolicitud[] DatosSol = null;
            using (wsSOPF.SolicitudClient wsSolicitudSol = new SolicitudClient())
            {
                DatosSol = wsSolicitudSol.ObtenerDatosSolicitud(idsolicitud);

                if (DatosSol != null && DatosSol.Length > 0)
                {
                    if (DatosSol[0].IdEstatus == 1)
                    {
                        btnGuardar.Visible = false;
                        if (DatosSol[0].IdEstatusProceso != 2) rdTipoEdicion.Enabled = false;
                    }
                }

                EstatusSolicitudProcesoDigitalResponse procesoDi = wsSolicitudSol.EstatusProcesoOriginacionD(idsolicitud);
                if (procesoDi != null && procesoDi.ProcesosDigital != null && procesoDi.ProcesosDigital.Length > 0)
                {
                    int ordenActual = procesoDi.ProcesosDigital.Where(p => p.ProcesoActual)
                        .OrderBy(po => po.Orden)
                        .Select(p => p.Orden)
                        .DefaultIfEmpty(0)
                        .FirstOrDefault();
                    var procesos = procesoDi.ProcesosDigital.Select(p => new
                    {
                        p.IdSolicitudProcesoDigital,
                        p.ProcesoDigital,
                        p.Estatus,
                        p.ProcesoActual,
                        Proceso = proceso.Where(pro => pro.Key == p.IdProcesoDigital).Select(v => v.Value).FirstOrDefault()
                    }).ToList();
                    strScript.AppendFormat("procesoDigital={0};", JsonConvert.SerializeObject(procesos));
                }
            }


            btnCancelar.Visible = puedeCancelar;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", strScript.ToString(), true);
    }

    protected void rdTipoEdicion_SelectedIndexChange(object sender, EventArgs e)
    {
        switch (rdTipoEdicion.SelectedValue)
        {
            case "1":
                this.Response.Redirect("frmClientePeru.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString() + "&isE=1");
                break;
            case "2":
                this.Response.Redirect("frmTipoCredPeru.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString() + "&isE=1");
                break;
            case "3":
                this.Response.Redirect("frmDocumentos.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString() + "&isE=1");
                break;
        }
    }

    protected void btnGuardarClick(object sender, EventArgs e)
    {
        try
        {
            pnlExito.Visible = false;
            pnlError.Visible = false;

            int Resultado = 1;

            using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
            {
                int idUsuario = 0;
                int idSolicitud = 0;
                if (Session["UsuarioId"] != null)
                {
                    int.TryParse(Session["UsuarioId"].ToString(), out idUsuario);
                }

                if (Request.QueryString["idsolicitud"] != null)
                {
                    int.TryParse(Request.QueryString["idsolicitud"].ToString(), out idSolicitud);
                }

                InicarSolicitudProcesoDigitalResponse iniciarProcesoD = wsSolicitud.IniciarProcesoOriginacionD(new InicarSolicitudProcesoDigitalRequest
                {
                    IdUsuario = idUsuario,
                    IdSolicitud = idSolicitud,
                    Reactivar = true
                });

                if (iniciarProcesoD.Error)
                {
                    lblError.Text = "Ocurrrió un error." + iniciarProcesoD.MensajeOperacion;
                    pnlError.Visible = true;
                }
                else if (!iniciarProcesoD.EnviaProcesoDigital)
                {
                    Resultado = wsSolicitud.UpdEstProceso(4, Convert.ToInt32(Request.QueryString["idsolicitud"]), 8, Convert.ToInt32(Session["UsuarioId"])); //primero la activo a 8 
                    Resultado = wsSolicitud.UpdEstProceso(3, Convert.ToInt32(Request.QueryString["idsolicitud"]), 8, Convert.ToInt32(Session["UsuarioId"]));

                    pnlExito.Visible = true;
                    lblExito.Text = "La solicitud se ha reactivado correctamente!";
                    rdTipoEdicion.Enabled = false;
                    btnGuardar.Enabled = false;
                    btnCancelar.Enabled = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "R1", "alert('La solicitud se ha reactivado correctamente!');" +
                        "window.location='lstDashboardSolicitudes.aspx';", true);
                }
                else
                {
                    pnlExito.Visible = true;
                    lblExito.Text = string.IsNullOrEmpty(iniciarProcesoD.MensajeOperacion) ? "La solicitud se ha reactivado correctamente!"
                        : iniciarProcesoD.MensajeOperacion;
                    rdTipoEdicion.Enabled = false;
                    btnGuardar.Enabled = false;
                    btnCancelar.Enabled = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "R1", "alert('La solicitud se ha reactivado correctamente!');" +
                        "window.location='lstDashboardSolicitudes.aspx'", true);
                }
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            pnlError.Visible = true;
        }
    }

    protected void CancelarSolicitud(object sender, EventArgs e)
    {
        try
        {
            pnlExito.Visible = false;
            pnlError.Visible = false;

            int Resultado = 1;

            using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
            {
                Resultado = wsSolicitud.UpdEstProceso(5, Convert.ToInt32(Request.QueryString["idsolicitud"]), 8, Convert.ToInt32(Session["UsuarioId"]));
                lblExito.Text = "La solicitud se ha cancelado correctamente";
                pnlExito.Visible = true;
                pnlAdvertencia.Visible = false;
                rdTipoEdicion.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            pnlError.Visible = true;
        }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        btnGuardar.Visible = false;
        btnCancelar.Visible = false;
        pnlAdvertencia.Visible = true;
    }

    protected void btnConfirmar_Click(object sender, EventArgs e)
    {
        CancelarSolicitud(sender, e);
    }

    protected void btnRechazar_Click(object sender, EventArgs e)
    {
        btnGuardar.Visible = true;
        btnCancelar.Visible = true;
        pnlAdvertencia.Visible = false;
    }

    #region WebMethods
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object ReiniciarProceso(ReiniciarProcesoRequest peticion)
    {
        bool error = false;
        string url = string.Empty;
        string mensaje;
        try
        {
            if (peticion == null) throw new ValidacionExcepcion("La petición no es correcta.");
            int idUsuario = HttpContext.Current.ValidarUsuario();
            if (idUsuario <= 0)
            {
                url = @"..\..\frmLogin.aspx";
                throw new ValidacionExcepcion("La clave del usuario no es válido.");
            }
            peticion.IdUsuario = idUsuario;
            if (peticion.IdSolicitud <= 0) throw new ValidacionExcepcion("La solicitud no es correcta.");
            if (peticion.IdSolicitudProcesoDigital <= 0) throw new ValidacionExcepcion("El proceso no es correcto.");
            if (string.IsNullOrEmpty(peticion.Comentario)) throw new ValidacionExcepcion("Ingrese un comentario.");

            using (SolicitudClient wsS = new SolicitudClient())
            {
                ReiniciarProcesoResponse reiniciarResp = wsS.ReiniciarProceso(peticion);
                if (reiniciarResp == null)
                {
                    error = true;
                    mensaje = "Ocurrió un error al intentar reiniciar el proceso. Contacte a soporte técnico.";
                }
                else
                {
                    error = reiniciarResp.Error;
                    mensaje = string.IsNullOrEmpty(reiniciarResp.MensajeOperacion) ? "El proceso se ha reiniciado." : reiniciarResp.MensajeOperacion;
                }
            }
        }
        catch (ValidacionExcepcion vex)
        {
            error = true;
            mensaje = vex.Message;
        }
        catch (Exception ex)
        {
            error = true;
            mensaje = ex.Message;
        }
        return new { error, mensaje, url };
    }
    #endregion
}
