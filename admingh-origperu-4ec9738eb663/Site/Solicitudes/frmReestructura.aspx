﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmReestructura.aspx.cs" Inherits="Solicitudes_frmReestructura" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPH1" runat="Server">
    <style type="text/css">
        #Clientes {
            width: 100%;
            height: 200px;
            float: left;
        }
    </style>
    <link href="../Styles/EstilosMaster.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <script src="../Styles/bootstrap-3.3.2-dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.11.2.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="../Scripts/moment.js" type="text/javascript"></script>
    <script src="../Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <!-- include your less or built css files  -->
    <!-- 
  bootstrap-datetimepicker-build.less will pull in "../bootstrap/variables.less" and "bootstrap-datetimepicker.less";
  or
  <link rel="stylesheet" href="/Content/bootstrap-datetimepicker.css" />
  -->
    <div class="h2">
        REESTRUCTURA
    </div>
    

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Creditos Relacionados</h3>
            </div>
            <div class="panel-body">
                <div class="izq">
                    <div class="form-group">
                        <div class="GridEncabezado">
                            Creditos Relacionados
                        </div>
                        <div class="GridContenedor" style="height: 320px">
                            <asp:GridView ID="gvreferencias" runat="server" Width="100%" AutoGenerateColumns="False"
                                Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                CssClass="Grid" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                DataKeyNames="nombres">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="idsolicitud"  />
                                    <asp:BoundField HeaderText="idcuenta"  />
                                    <asp:BoundField HeaderText="Saldo Capital" />
                                    <asp:BoundField HeaderText="InteresxCobrar" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div class="form-group row">
                            <label for="txtTotalLiquidar" class="col-sm-2 form-control-label">
                                Total a Liquidar</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtTotalLiquidar" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="txtTotalDep" class="col-sm-2 form-control-label">
                                Total Deposito</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtTotalDep" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    
                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <asp:Button ID="btnSiguienteTipo" runat="server" Text="Siguiente" CssClass="btn-primary" />
            </div>
        </div>

</asp:Content>
