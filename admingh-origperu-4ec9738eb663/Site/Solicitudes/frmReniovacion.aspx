﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmReniovacion.aspx.cs" Inherits="Solicitudes_frmReniovacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <style type="text/css">
        #Clientes {
            width: 100%;
            height: 200px;
            float: left;
        }
    </style>
    <!-- include your less or built css files  -->
    <!-- 
  bootstrap-datetimepicker-build.less will pull in "../bootstrap/variables.less" and "bootstrap-datetimepicker.less";
  or
  <link rel="stylesheet" href="/Content/bootstrap-datetimepicker.css" />
  -->
    <div class="h2">
        RENOVACION
    </div>
    

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Creditos Relacionados</h3>
            </div>
            <div class="panel-body">
                <div class="izq">
                    <div class="form-group">
                        <div class="GridEncabezado">
                            Creditos Relacionados
                        </div>
                        <div class="GridContenedor" style="height: 320px">
                            <asp:GridView ID="gvCreditos" runat="server" Width="100%" AutoGenerateColumns="False" EnableViewState="true"
                                Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                CssClass="table table-bordered table-striped  table-hover table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                DataKeyNames="IdSolicitud" Style="overflow-x: auto;" OnRowDataBound="GridView1_RowDataBound" >
                                <PagerStyle CssClass="pagination-ys" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ckhCredito" runat="server" AutoPostBack="true" OnCheckedChanged="chkview_CheckedChanged" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="idsolicitud" DataField="IdSolicitud" />
                                    <asp:BoundField HeaderText="idcuenta" DataField="IdCuenta" />
                                    <asp:BoundField HeaderText="Saldo Capital" DataField="capital" />
                                    <asp:BoundField HeaderText="Saldo Intere" DataField="interes" />
                                    <asp:BoundField HeaderText="Saldo Iva" DataField="iva" />
                                    <asp:BoundField HeaderText="Saldo Periodo" DataField="sdo_periodo" />
                                    <asp:BoundField HeaderText="Total Liquidar" DataField="TotalRenovar" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div class="form-group row">
                            <label for="txtTotalLiquidar" class="col-sm-2 form-control-label">
                                Total a Liquidar</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtTotalLiquidar" runat="server" CssClass="form-control" Enabled ="false"></asp:TextBox>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <asp:Button ID="btnSiguienteTipo" runat="server" Text="Siguiente" CssClass="btn-primary" OnClick="btnSiguiente_Click"/>
            </div>
        </div>

</asp:Content>

