﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;
using System.Data;

using System.Drawing;

public partial class Solicitudes_frmReniovacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtTotalLiquidar.Text = "0";
            int idcliente = 0;
            int idFolio = 0;
            idcliente = Convert.ToInt32(Request.QueryString["idcliente"]);
            idFolio = Convert.ToInt32(Request.QueryString["idFolio"]);
            TBCreditos.CreditoRelacionado[] Creditos = null;
            using (wsSOPF.CreditoClient wsCredito = new wsSOPF.CreditoClient())
            {
                if (idFolio > 0)
                {
                    Creditos = wsCredito.ObtenerCreditosRelacionados(8, idcliente);
                }
                else
                {
                    Creditos = wsCredito.ObtenerCreditosRelacionados(1, idcliente);
                }
                if (Creditos.Length > 0)
                {
                    gvCreditos.DataSource = Creditos;
                    gvCreditos.DataBind();
                }
            }
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drview = e.Row.DataItem as DataRowView;

            //Find checkbox and checked/Unchecked based on values
            CheckBox chkb = (CheckBox)e.Row.FindControl("ckhCredito");
            if (chkb.Checked) //find the chb value from data
            {
                // txtTotalLiquidar.Text = "-1";
            }
            else
            {
                //txtTotalLiquidar.Text = "-2";
            }
        }

    }
    protected void chkview_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow row = ((GridViewRow)((CheckBox)sender).NamingContainer);
        int index = row.RowIndex;
        CheckBox cb1 = (CheckBox)gvCreditos.Rows[index].FindControl("ckhCredito");
        if (cb1.Checked)
        {
            decimal totalLiquidar = Convert.ToDecimal(gvCreditos.Rows[index].Cells[7].Text);


            txtTotalLiquidar.Text = Convert.ToString(Convert.ToDecimal(txtTotalLiquidar.Text) + totalLiquidar);

        }
        else
        {
            decimal totalLiquidar = Convert.ToDecimal(gvCreditos.Rows[index].Cells[7].Text);
            txtTotalLiquidar.Text = Convert.ToString(Convert.ToDecimal(txtTotalLiquidar.Text) - totalLiquidar);
        }
        //here you can find your control and get value(Id).

    }
    protected void btnSiguiente_Click(object sender, EventArgs e)
    {
        //GridViewRow row = ((GridViewRow)((CheckBox)sender).NamingContainer);
        //int index = row.RowIndex;
        //CheckBox cb1 = (CheckBox)gvCreditos.Rows[index].FindControl("ckhCredito");

        int idcuenta = 0;
        decimal LiqCapital = 0;
        decimal interesDev = 0;
        int idCliente = Convert.ToInt32(Request.QueryString["idcliente"]);
        int idsolicitud = Convert.ToInt32(Request.QueryString["idsolicitud"]);
        int idFolio = Convert.ToInt32(Request.QueryString["idFolio"]);

        using (wsSOPF.CreditoClient wsCredito = new CreditoClient())
        {
            try
            {
                wsCredito.ActualizarCreditosRenovacion(3, idsolicitud, idcuenta, LiqCapital, interesDev, idCliente);
            }
            catch (Exception ex)
            {

            }
        }
        int Error = 0;
        idFolio = Convert.ToInt32(Request.QueryString["idFolio"]);
        foreach (GridViewRow row in gvCreditos.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (row.Cells[0].FindControl("ckhCredito") as CheckBox);
                if (chkRow.Checked)
                {
                    idcuenta = Convert.ToInt32(row.Cells[2].Text);
                    LiqCapital = Convert.ToDecimal(row.Cells[3].Text);
                    interesDev = Convert.ToDecimal(row.Cells[6].Text);

                    using (wsSOPF.CreditoClient wsCredito = new CreditoClient())
                    {
                        try 
                        { 
                            wsCredito.ActualizarCreditosRenovacion(1, 0, idcuenta, LiqCapital, interesDev, idCliente);
                            
                        }
                        catch(Exception ex)
                        {

                        }
                    }
                   
                }
                 else 
                    {
                        if(idFolio!=0)
                        {
                            Error = 1;
                            String Mensaje = "Debe seleccionar Todas las cuenta ya que es una renovacion pactada";
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('" + Mensaje + "');", true);
                            break;
                        }
                    }
                
            }
        }
            if(Error==0)
            { 
                Response.Redirect("frmDatosSolicitud.aspx?idcliente=" + Request.QueryString["idcliente"].ToString() + "&TipoCredito="+Request.QueryString["TipoCredito"].ToString() + "&idsolicitud=" + idsolicitud.ToString() + "&idFolio=" + idFolio.ToString());
            }

        

    }
}