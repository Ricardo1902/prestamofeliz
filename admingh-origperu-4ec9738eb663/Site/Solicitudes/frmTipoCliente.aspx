﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="frmTipoCliente.aspx.cs" Inherits="Solicitudes_frmTipoCliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <style type="text/css">
        .Clientes {
            width: 100%;
            height: 100px;
            float: left;
        }
    </style>

    
        <div class="center">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Seleccione el Tipo de Cliente</h3>
                </div>
                <div class="panel-body">
                    <asp:RadioButtonList ID="rdTipoCliente" runat="server" OnSelectedIndexChanged="rdTipoCliente_SelectedIndexChange"
                        AutoPostBack="true">
                        <asp:ListItem Value="1">Cliente Existente</asp:ListItem>
                        <asp:ListItem Value="2">Cliente Nuevo</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnSiguienteTipo" runat="server" Text="Siguiente" CssClass="btn-primary"
                        OnClick="btnSiguienteTipo_Click" />
                </div>
            </div>
            <br />
            <br />
            <div id="Buscar" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Busqueda de Cliente</h3>
                    </div>
                    <div class="panel-body" style="height: 600px">
                        <asp:Label ID="Label1" runat="server" Text="Cliente"></asp:Label>
                        <asp:TextBox ID="txtCliente" runat="server"></asp:TextBox>
                        <asp:Button ID="btnBuscarCliente" runat="server" Text="Buscar" CssClass="btn-info"
                            OnClick="btnBuscarCliente_Click" />
                        <div id="Clientes">
                            <div class="GridEncabezado">
                                Clientes
                            </div>
                            <div class="GridContenedor" style="height: 320px">
                                <asp:GridView ID="gvClientes" runat="server" Width="100%" AutoGenerateColumns="False"
                                    Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                                    CssClass="table table-bordered table-striped  table-hover table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                                    PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                                    DataKeyNames="clave" Style="overflow-x: auto;" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="gvClientes_PageIndexChanging">
                                    <PagerStyle CssClass="pagination-ys" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Clave" DataField="clave" />
                                        <asp:BoundField HeaderText="ClaveDesc" DataField="clavedesc" />
                                        <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                        <asp:BoundField HeaderText="Nombres" DataField="Nombres" />
                                        <asp:BoundField HeaderText="Ap. Paterno" DataField="apellidop" />
                                        <asp:BoundField HeaderText="Ap. Meterno" DataField="apellidom" />
                                        <asp:BoundField HeaderText="Fecha Nac." DataField="fch_Nacimiento" />
                                        <asp:BoundField HeaderText="Direccion" DataField="direccion" />
                                        <asp:BoundField HeaderText="Colonia" DataField="colonia" />
                                        <asp:BoundField HeaderText="Ciudad" DataField="ciudad" />
                                        <asp:BoundField HeaderText="Estado" DataField="estado" />
                                        <asp:BoundField HeaderText="CP" DataField="cp" />
                                        <asp:BoundField HeaderText="Id Cliente" DataField="IdCliente" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No se encontraron registros.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <asp:Button ID="btnSigueinteBusqueda" runat="server" Text="Siguiente" CssClass="btn-primary" />
                    </div>
                </div>
            </div>
        </div>

</asp:Content>
