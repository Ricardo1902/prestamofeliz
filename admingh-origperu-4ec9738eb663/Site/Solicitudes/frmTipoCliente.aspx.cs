﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Solicitudes_frmTipoCliente : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.Buscar.Visible = false;
            btnSiguienteTipo.Enabled = false;
        }
    }
    protected void rdTipoCliente_SelectedIndexChange(object sender, EventArgs e)
    {
        if (rdTipoCliente.SelectedIndex == 0)
        {
            this.Buscar.Visible = true;
            btnSiguienteTipo.Enabled = false;
        }
        else
        {
            this.Buscar.Visible = false;
            btnSiguienteTipo.Enabled = true;
        }
    }
    protected void btnSiguienteTipo_Click(object sender, EventArgs e)
    {
        this.Response.Redirect("frmCliente.aspx?idcliente=0&idsolicitud=0");
    }
    protected void btnBuscarCliente_Click(object sender, EventArgs e)
    {
        TBClientes.BusquedaCliente[] clientes = null;
        List<TBClientes.BusquedaCliente> listacliente = new List<TBClientes.BusquedaCliente>();

        using (wsSOPF.ClientesClient wsCliente = new ClientesClient())
        {
            clientes = wsCliente.BusquedaCliente(14, 0, 0, txtCliente.Text, DateTime.Now, DateTime.Now, 0);
        }

        if (clientes.Length > 0)
        {
            gvClientes.DataSource = clientes;
            gvClientes.DataBind();
        }

    }
    protected void GridView1_RowDataBound(object sender,GridViewRowEventArgs e)
    {
        //check the item being bound is actually a DataRow, if it is, 
        //wire up the required html events and attach the relevant JavaScripts
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "javascript:setMouseOverColor(this);";
            e.Row.Attributes["onmouseout"] = "javascript:setMouseOutColor(this);";
            e.Row.Attributes.Add("onClick", "javascript:void SelectRowCli(this);");
        }
    }
    protected void gvClientes_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        GridView gv = (GridView)sender;
        gv.PageIndex = e.NewPageIndex;
        TBClientes.BusquedaCliente[] clientes = null;
        List<TBClientes.BusquedaCliente> listacliente = new List<TBClientes.BusquedaCliente>();

        using (wsSOPF.ClientesClient wsCliente = new ClientesClient())
        {
            clientes = wsCliente.BusquedaCliente(14, 0, 0, txtCliente.Text, DateTime.Now, DateTime.Now, 0);
        }

        if (clientes.Length > 0)
        {
            gvClientes.DataSource = clientes;
            gvClientes.DataBind();
        }


    }
}