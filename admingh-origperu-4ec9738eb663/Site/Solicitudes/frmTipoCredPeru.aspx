﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmTipoCredPeru.aspx.cs" Inherits="Site_Solicitudes_frmTipoCredPeru" %>

<asp:Content ID="head" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link href="../../jQuery/jquery-ui.min.css" rel="stylesheet" />
  <link href="../../jQuery/jquery-ui.structure.min.css" rel="stylesheet" />
  <link href="../../jQuery/jquery-ui.theme.min.css" rel="stylesheet" />
  <script type="text/javascript" src="../../jQuery/external/jquery/jquery.js"></script>
  <script type="text/javascript" src="../../jQuery/jquery-ui.min.js"></script>
  <script type="text/javascript" src="../../js/parsley.min.js"></script>

  <script type="text/javascript">
      $(function () {
          $("#divAccordion").accordion({ collapsible: true, heightStyle: "content" });
          $('#aspnetForm').parsley();

          $(".digDecimal").change(function () {
              var valor = $(this).val();
              if ($.isNumeric(valor)){
                  $(this).val(addZeroes(valor));
                  $('#aspnetForm').parsley().validate();
              } else 
                  $(this).val("");
          });

          $(".headerAcc").click(function () {
              $(this).find(".blink").css("visibility", "hidden");
          });         

          if ($('#<%=txtNumTarjetaVisa.ClientID%>').length > 0) {
            document.getElementById('<%=txtNumTarjetaVisa.ClientID%>').addEventListener('input', function (e) {
                var target = e.target, position = target.selectionEnd, length = target.value.length;

                target.value = target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
                target.selectionEnd = position += ((target.value.charAt(position - 1) === ' ' && target.value.charAt(length - 1) === ' ' && length !== target.value.length) ? 1 : 0);
            });
          }
           if ($('#<%=txtNumTarjetaVisaEME.ClientID%>').length > 0) {
            document.getElementById('<%=txtNumTarjetaVisaEME.ClientID%>').addEventListener('input', function (e) {
                var target = e.target, position = target.selectionEnd, length = target.value.length;

                target.value = target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
                target.selectionEnd = position += ((target.value.charAt(position - 1) === ' ' && target.value.charAt(length - 1) === ' ' && length !== target.value.length) ? 1 : 0);
            });
        }
      });

      function completeCuentaBancaria()
      {
          var cuentaBancaria = "";
          $(".cuentaBancariaItem").each(function (index) {
              cuentaBancaria += $(this).val();              
          });
          $("#<%=txtCuentaDesembolso.ClientID%>").val(cuentaBancaria);
      }

      function completeCuentaBancariaEmergente()
      {
          var cuentaBancariaEmergente = "";
          $(".cuentaBancariaEmergenteItem").each(function (index) {
              cuentaBancariaEmergente += $(this).val();
          });
          $("#<%=txtCuentaDesembolso_Emergente.ClientID%>").val(cuentaBancariaEmergente);
      } 

      function addZeroes(num) {
          var value = Number(num);
          var res = num.split(".");
          if (num.indexOf('.') === -1) {
              value = value.toFixed(2);
              num = value.toString();
          } else if (res[1].length < 3) {
              value = value.toFixed(2);
              num = value.toString();
          }
          return num
      }      
  </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="divAccordion">
    
    <h3 class="headerAcc">
        DATOS DEL CR&Eacute;DITO
        <asp:UpdatePanel ID="upValDatosCredito" UpdateMode="Conditional" RenderMode="Inline" runat="server"><ContentTemplate>
                <span id="valIconDatosCredito" class="glyphicon glyphicon-exclamation-sign blink" visible="false" runat="server"></span>
        </ContentTemplate></asp:UpdatePanel>
    </h3>
    <div>
      <div class="panel-body">
            <asp:UpdateProgress AssociatedUpdatePanelID="upDatosCredito" DisplayAfter="100" runat="server">
                <ProgressTemplate>
                     <div id="loader-background"></div>
                     <div id="loader-content"></div>              
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upDatosCredito" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hdnFolio" runat="server" />
                <asp:HiddenField ID="hdnIdPromotor" runat="server" />
                <asp:HiddenField ID="hdnIdCanal" runat="server" />
                <asp:HiddenField ID="hdnScore" runat="server" />
                <asp:HiddenField ID="hdnOrigen" runat="server" />
                <asp:HiddenField ID="hdnIdTipoCliente" runat="server" />
                <asp:HiddenField ID="hdnCategoriaCliente" runat="server" />
                <asp:HiddenField ID="hdnIdSubProducto" runat="server" />
                <asp:HiddenField ID="hdnIdTipoCredito" runat="server" />
                <asp:HiddenField ID="hdnTipoCredito" runat="server" />
                <div class=""><!--izq-->
                    <div class="form-group row">
                        <div class="form-group col-md-6 col-lg-4">
                            <label for="txtTipoConvenio" class="form-control-label small">Método de cobro</label>
                            <div>
                                <asp:TextBox ID="txtTipoConvenio" runat="server" CssClass="form-control" MaxLength="3" Enabled="false"></asp:TextBox>                            
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="ddlConvenio" class="form-control-label small">Producto:</label>
                            <asp:DropDownList ID="ddlConvenio" DataValueField="Valor" DataTextField="Descripcion" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlConvenio_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>
                        <div class="form-group col-md-6 col-lg-4">
                            <label for="txtPlazo" class="form-control-label small">Plazo</label>
                            <div>
                                <asp:DropDownList ID="ddlProducto" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlProducto_SelectedIndexChanged" DataTextField="Producto" DataValueField="IdProducto">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtPlazo" runat="server" CssClass="form-control" MaxLength="3" OnTextChanged="txtCalcular_Click" AutoPostBack="true" Visible="false"></asp:TextBox>                            
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="form-group col-md-3">
                            <label for="txtImporteCredito" class="form-control-label small">Importe del Cr&eacute;dito</label>
                            <div>
                                <asp:TextBox ID="txtIDSolicitud" runat="server" CssClass="form-control" Style="text-transform: uppercase;" Enabled="false" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtPrimerVencimiento" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtImporteCredito" runat="server" CssClass="form-control digDecimal" Style="text-transform: uppercase;" onKeyPress="return EvaluateText('%f', this);" MaxLength="10" OnTextChanged="txtCalcular_Click" AutoPostBack="true"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvImporteCredito" ControlToValidate="txtImporteCredito" ErrorMessage="Capture el Importe" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="ddlSeguroDesgravamen"  class="form-control-label small">Incluir Seguro de Desgravamen</label>
                            <div>
                                <asp:DropDownList runat="server" ID="ddlSeguroDesgravamen" CssClass="form-control" OnSelectedIndexChanged="ddlSeguroDesgravamen_SelectedIndexChange" AutoPostBack="true">
                                    <asp:ListItem Text="NO" Value="0.00"></asp:ListItem>
                                    <asp:ListItem Text="SI - INDIVIDUAL" Value="3.30"></asp:ListItem>
                                    <asp:ListItem Text="SI - INCLUIR CÓNYUGE" Value="6.60"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="txtSeguroDesgravamen" class="form-control-label small">Seguro de Desgravamen</label>
                            <div>
                                <asp:TextBox ID="txtSeguroDesgravamen" runat="server" CssClass="form-control digDecimal" data-parsley-trigger="keyup" data-parsley-pattern="^[0-9]*\.[0-9]{2}$" Enabled="false" Text="0.00"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvSeguroDesgravamen" ControlToValidate="txtSeguroDesgravamen" ErrorMessage="Capture el seguro de desgravamen" Enabled="false" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="ddlFrePag"  class="form-control-label small">Frecuencia de Pago</label>
                            <div>
                                <asp:DropDownList runat="server" ID="ddlFrePag" CssClass="form-control" AutoPostBack="true">
                                    <asp:ListItem Text="MENSUAL" Value="MENSUAL"></asp:ListItem>
                                    <asp:ListItem Text="QUINCENAL" Value="QUINCENAL"></asp:ListItem>
                                    <asp:ListItem Text="SEMANAL" Value="SEMANAL"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlMontosLiquidacion" CssClass="form-group row" Visible="true" runat="server">
                        <div class="form-group col-md-3 col-lg-3">
		                    <label for="txtMontoLiquidacion" class="form-control-label small">Monto Liquidación</label>                            
                            <div class="input-group margin-bottom-sm">                                
                                <asp:TextBox ID="txtMontoLiquidacion" runat="server" CssClass="form-control" MaxLength="12" onKeyPress="return EvaluateText('%f', this);" OnTextChanged="txtCalcular_Click" AutoPostBack="true"></asp:TextBox>
                                <span class="input-group-addon"><asp:HyperLink ID="lnkLiquidacion" data-toggle="tooltip" title="Consultar Detalle" runat="server" Target="_blank"><i class="fas fa-search-dollar"></i></asp:HyperLink></span>
                            </div>    
                            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvTxtMontoLiquidacion" ControlToValidate="txtMontoLiquidacion" ErrorMessage="Capture el Monto de Liquidacion" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>                                                                                            
                        </div>
                        <div class="form-group col-md-3 col-lg-3">
		                    <label for="txtMontoDesembolso" class="form-control-label small">Neto a Desembolsar</label>
                            <div>
                                <asp:TextBox ID="txtMontoDesembolso" runat="server" CssClass="form-control" MaxLength="12" Enabled="false"></asp:TextBox>                                
                                <asp:CustomValidator ID="cvMontoDesembolsoLiquidacion" ForeColor="Red" ErrorMessage="Desembolso no puede ser Negativo" CssClass="small" ValidationGroup="DatosCredito" Display="Dynamic" runat="server"></asp:CustomValidator>
                            </div>
                        </div>
                        <div class="form-group col-md-3 col-lg-3">		                    
                            <div>
                                <br />
                                <asp:Label ID="lblPoliticaAmpliacion" runat="server" class=""></asp:Label>
                            </div>
                        </div>
                    </asp:Panel>
                    <hr />
                    <div class="form-group">                        
                        <div class="form-group row">
                            <div class="form-group col-md-4">
                                <label for="txtTasaInteres" class="form-control-label small">Tasa de Interés Anual</label>
                                <div>
                                    <asp:TextBox ID="txtTasaInteres" runat="server" CssClass="form-control digDecimal" data-parsley-trigger="keyup" data-parsley-pattern="^[0-9]*\.[0-9]{2}$" Enabled="false" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvTasaInteres" ControlToValidate="txtTasaInteres" ErrorMessage="Favor de introducir la tasa de interés anual" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="txtMontoGAT" class="form-control-label small">Monto GAT</label>
                                <div>
                                    <asp:TextBox ID="txtMontoGAT" runat="server" CssClass="form-control digDecimal" Text="10.00" Enabled="false"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvMontoGAT" ControlToValidate="txtMontoGAT" ErrorMessage="Favor de introducir el monto GAT" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="txtMontoUsoCanal" class="form-control-label small">Monto uso de Canal</label>
                                <div>
                                    <asp:TextBox ID="txtMontoUsoCanal" runat="server" CssClass="form-control digDecimal" data-parsley-trigger="keyup" data-parsley-pattern="^[0-9]*\.[0-9]{2}$" Enabled="false"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvMontoUsoCanal" ControlToValidate="txtMontoUsoCanal" ErrorMessage="Favor de introducir el monto de uso de canal" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>                                        
                    
                        <div class="form-group row">
                            <div class="form-group col-md-4">
                                <label for="txtMontoTotalFinanciar" class="form-control-label small">Monto total a Financiar</label>
                                <div>
                                    <asp:TextBox ID="txtMontoTotalFinanciar" runat="server" CssClass="form-control digDecimal" data-parsley-trigger="keyup" data-parsley-pattern="^[0-9]*\.[0-9]{2}$" Enabled="false"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvMontoTotalFinanciar" ControlToValidate="txtMontoTotalFinanciar" ErrorMessage="Calcule el monto total a financiar" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="txtCostoTotalCred" class="form-control-label small">Costo total del Cr&eacute;dito</label>
                                <div>
                                    <asp:TextBox ID="txtCostoTotalCred" runat="server" CssClass="form-control digDecimal" data-parsley-trigger="keyup" data-parsley-pattern="^[0-9]*\.[0-9]{2}$" Enabled="false"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvCostoTotalCred" ControlToValidate="txtCostoTotalCred" ErrorMessage="Calcule el costo total del crédito" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="txtMontoCuota" class="form-control-label small">Monto de Cuota</label>
                                <div>
                                    <asp:TextBox ID="txtMontoCuota" runat="server" CssClass="form-control digDecimal" data-parsley-trigger="keyup" data-parsley-pattern="^[0-9]*\.[0-9]{2}$" Enabled="false"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvMontoCuota" ControlToValidate="txtMontoCuota" ErrorMessage="Calcule el monto de cuota" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                </div>
                            </div>                   
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <asp:Label runat="server" ID="lblErrorCalcular"></asp:Label>
                                <asp:Button ID="btnCalcular" runat="server" Text="Calcular" CssClass="btn btn-sm btn-success" OnClick="btnCalcular_Click" />                                
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="form-group clearfix">
                        <div class="form-group col-sm-12">
                            <strong><asp:Label ID="lblDesembolsoDomiciliacion" CssClass="form-control-label small" Text="Desembolso/Domiciliacion" runat="server" /></strong>
                        </div>
                        <asp:Panel ID="pnlDomiciliacionBanco" CssClass="clearfix" runat="server">
                             <asp:Panel runat="server" ID="divMetDis" CssClass="form-group" Visible="false">   
                              <div class="form-group row">
                                  <div class="form-group col-md-4">
                                       <label for="cboMetDis" class="form-control-label small">Metodo de Dispersión</label>
                                       <div>
                                           <asp:DropDownList ID="cboMetDis" runat="server" CssClass="form-control">
                                           </asp:DropDownList>
                                       </div>    
                                   </div>  
                               </div>        
                           </asp:Panel>

                            <div class="form-group col-md-6">
                                <label for="ddlBanco" class="form-control-label small">Banco</label>
                                <div>
                                    <asp:DropDownList runat="server" id="ddlBanco" OnSelectedIndexChanged="ddlBanco_SelectedIndexChanged" CssClass="form-control" AutoPostBack="true">
                                        <asp:ListItem Selected="True" Value="CONTINENTAL" Text="CONTINENTAL"></asp:ListItem>
							            <asp:ListItem Value="INTERBANK" Text="INTERBANK"></asp:ListItem>
                                        <asp:ListItem Value="CREDITO" Text="CREDITO"></asp:ListItem>
                                        <asp:ListItem Value="SCOTIABANK" Text="SCOTIABANK"></asp:ListItem>
                                        <asp:ListItem Value="COMERCIO" Text="COMERCIO"></asp:ListItem>
                                        <asp:ListItem Value="RIPLEY" Text="RIPLEY"></asp:ListItem>
                                        <asp:ListItem Value="FALABELLA" Text="FALABELLA"></asp:ListItem>
                                        <asp:ListItem Value="PICHINCHA" Text="PICHINCHA"></asp:ListItem>
                                        <asp:ListItem Value="GNB" Text="GNB"></asp:ListItem>                                        
                                        <asp:ListItem Value="BANBIF" Text="BANBIF"></asp:ListItem>
                                         <asp:ListItem Value="MIBANCO" Text="MIBANCO"></asp:ListItem>
                                        <asp:ListItem Value="ALFIN" Text="ALFIN"></asp:ListItem>                                        
                                        <asp:ListItem Value="BANCO DE LA NACION" Text="BANCO DE LA NACION"></asp:ListItem>
                              <%--          <asp:ListItem Value="OTRO" Text="OTRO"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </div>

				            <div class="form-group col-md-6">
                                <label for="txtBanco" class="form-control-label small">Nombre Banco</label>
                                <div>
                                    <asp:TextBox ID="txtBanco" runat="server" CssClass="form-control" Style="text-transform: uppercase;" data-parsley-trigger="keyup" data-parsley-type="alphanum" Enabled="false" Text="CONTINENTAL"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvBanco" ControlToValidate="txtBanco" ErrorMessage="Favor de introducir el banco" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                </div>
                            </div>

				            <asp:Panel ID="pnlCuentaContinental" CssClass="clearfix" runat="server" Visible="false">
					            <div class="form-group col-md-3">
						            <label for="txtBCEntidad" class="form-control-label small">Entidad</label>
						            <div>
							            <asp:TextBox ID="txtBCEntidad" runat="server" CssClass="form-control cuentaBancariaItem" MaxLength="4" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancaria();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvEntidad" ControlToValidate="txtBCEntidad" ErrorMessage="Capture Entidad" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBCEntidad" ValidationExpression = "^[\s\S]{4,}$" 
                                             ErrorMessage="Capture 4 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>

					            <div class="form-group col-md-3">
						            <label for="txtBCOficina" class="form-control-label small">Oficina</label>
						            <div>
							            <asp:TextBox ID="txtBCOficina" runat="server" CssClass="form-control cuentaBancariaItem" MaxLength="4" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancaria();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvOficina" ControlToValidate="txtBCOficina" ErrorMessage="Capture Oficina" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBCOficina" ValidationExpression = "^[\s\S]{4,}$" 
                                             ErrorMessage="Capture 4 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>

					            <div class="form-group col-md-2">
						            <label for="txtBCDC" class="form-control-label small">DC</label>
						            <div>
							            <asp:TextBox ID="txtBCDC" runat="server" CssClass="form-control cuentaBancariaItem" MaxLength="2" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancaria();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvDC" ControlToValidate="txtBCDC" ErrorMessage="Capture DC" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBCDC" ValidationExpression = "^[\s\S]{2,}$" 
                                             ErrorMessage="Capture 2 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>

					            <div class="form-group col-md-4">
						            <label for="txtBCCuenta" class="form-control-label small">Cuenta</label>
						            <div>
							            <asp:TextBox ID="txtBCCuenta" runat="server" CssClass="form-control cuentaBancariaItem" MaxLength="10" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancaria();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="rfvCuenta" ControlToValidate="txtBCCuenta" ErrorMessage="Capture Cuenta" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBCCuenta" ValidationExpression = "^[\s\S]{10,}$" 
                                             ErrorMessage="Capture 10 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>
				            </asp:Panel>

                            <asp:Panel ID="pnlCuentaInterbank" runat="server" Visible="false">
                                <div class="form-group col-md-2">
						            <label for="txtBINoOficina" class="form-control-label small">No. Oficina</label>
						            <div>
							            <asp:TextBox ID="txtBINoOficina" runat="server" CssClass="form-control cuentaBancariaItem" MaxLength="3" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancaria();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="RequiredFieldValidator2" ControlToValidate="txtBINoOficina" ErrorMessage="Capure No. Oficina" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBINoOficina" ValidationExpression = "^[\s\S]{3,}$" 
                                             ErrorMessage="Capture 3 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>

					            <div class="form-group col-md-4">
						            <label for="txtBICuenta" class="form-control-label small">Cuenta</label>
						            <div>
							            <asp:TextBox ID="txtBICuenta" runat="server" CssClass="form-control cuentaBancariaItem" MaxLength="10" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancaria();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="RequiredFieldValidator3" ControlToValidate="txtBICuenta" ErrorMessage="Capture Cuenta" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBICuenta" ValidationExpression = "^[\s\S]{10,}$" 
                                             ErrorMessage="Capture 10 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>
                            </asp:Panel>				                                      			
            
                            <div class="form-group col-md-6">
                                <label for="txtCuentaDesembolso" class="form-control-label small">Cuenta Bancaria</label>
                                <div>
                                    <asp:TextBox ID="txtCuentaDesembolso" runat="server" CssClass="form-control" MaxLength="20" onKeyPress="return EvaluateText('%d', this);"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ForeColor="Red" ControlToValidate="txtCuentaDesembolso" ErrorMessage="Capture el Número de Cuenta Bancaria" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <asp:Panel ID="pnlCuentaOtroBanco" runat="server" Visible="false">
					            <div class="form-group col-md-6">
						            <label for="txtCuentaCCI" class="form-control-label small">Cuenta C.C.I.</label>
						            <div>
							            <asp:TextBox ID="txtCuentaCCI" runat="server" CssClass="form-control" MaxLength="20" onKeyPress="return EvaluateText('%d', this);"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ControlToValidate="txtCuentaCCI" ErrorMessage="Favor de introducir el Número de Cuenta C.C.I." ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
						            </div>
					            </div>
				            </asp:Panel> 

                        </asp:Panel> 
                        
                        <asp:Panel ID="pnlDomiciliacionVisa" runat="server">
                            <hr />
                            <div class="form-group row">                                
                               <div class="col-sm-1">
                                    <!-- Rounded switch -->
                                    <label class="switch">
                                      <asp:CheckBox ID="chkDomiciliacionVisa" AutoPostBack="true" runat="server" OnCheckedChanged="chkDomiciliacionVisa_CheckedChanged" />
                                      <span class="slider round"></span>
                                    </label>                                    
                                </div>
                                <div class="col-sm-4">
                                    <label class="form-control-label small">Domiciliacion</label>
                                </div>                                                                
                            </div>
                            <div class="form-group col-md-6">
                                <label for="txtNumTarjetaVisa" class="form-control-label small">Numero de Tarjeta</label>
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fab fa-cc-visa"></i></span>
                                    <asp:TextBox ID="txtNumTarjetaVisa" runat="server" CssClass="form-control" MaxLength="19" onKeyPress="return EvaluateText('%d', this);" onBlur="hideCvBINTarjetaVISA();"></asp:TextBox>                                    
                                </div>
                                <asp:RequiredFieldValidator ID="rfvTxtNumTarjetaVisa" runat="server" ForeColor="Red" ControlToValidate="txtNumTarjetaVisa" ErrorMessage="Capture el Numero de Tarjeta" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ControlToValidate="txtNumTarjetaVisa" ValidationExpression = "^[\s\S]{19,}$" 
                                            ErrorMessage="Capture los 16 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
                                <asp:CustomValidator id="cvBINTarjetaVISA" runat="server" ErrorMessage="Error BIN Tarjeta VISA" ValidationGroup="DatosCredito" CssClass="small" ForeColor="Red" Display="Dynamic" 
                                            EnableClientScript="False"></asp:CustomValidator>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="" class="form-control-label small">Mes Expiracion</label>                                                                                                 
                                <div>                                    
                                    <asp:DropDownList ID="ddlMesExpiraVisa" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="1">Enero</asp:ListItem>
                                        <asp:ListItem Value="2">Febrero</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Abril</asp:ListItem>
                                        <asp:ListItem Value="5">Mayo</asp:ListItem>
                                        <asp:ListItem Value="6">Junio</asp:ListItem>
                                        <asp:ListItem Value="7">Julio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                        <asp:ListItem Value="10">Octubre</asp:ListItem>
                                        <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                        <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group col-md-2">
                                <label for="" class="form-control-label small">Año Expiracion</label>                                                                                                 
                                <div>
                                    <asp:TextBox ID="txtAnioExpiraVisa" runat="server" CssClass="form-control" placeholder="AAAA" MaxLength="4" onKeyPress="return EvaluateText('%d', this);"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvTxtAnioExpiraVisa" runat="server" ForeColor="Red" ControlToValidate="txtAnioExpiraVisa" ErrorMessage="Capture el Año de Expiracion" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ControlToValidate="txtAnioExpiraVisa" ValidationExpression = "^[\s\S]{4,}$" 
                                            ErrorMessage="Capture 4 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
                                </div>                                
                            </div>

                            <div class="form-group col-md-2">
                                <label for="" class="form-control-label small">CVV</label>
                                <div>
                                    <asp:TextBox ID="txtCuentaCVV" runat="server" CssClass="form-control" MaxLength="4" onKeyPress="return EvaluateText('%d', this);"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtCuentaCVV" runat="server" ForeColor="Red" ControlToValidate="txtCuentaCVV" ErrorMessage="Capture el Número de CVV" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                               <asp:RegularExpressionValidator ControlToValidate="txtCuentaCVV" ValidationExpression = "^[\s\S]{3,}$" 
                                            ErrorMessage="Capture 3 o 4 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
                                    </div>
                            </div>


                        </asp:Panel>
                    </div>

                    <div class="form-group">
                        <hr />
                         <div class="form-group row">                                
                           <div class="col-sm-1">
                                <!-- Rounded switch -->
                                <label class="switch">
                                    <asp:CheckBox ID="chkCuentaDomiciliacionEmergente" AutoPostBack="true" runat="server" OnCheckedChanged="chkCuentaDomiciliacionEmergente_CheckedChanged" />
                                    <span class="slider round"></span>
                                </label>                                    
                            </div>
                            <div class="col-sm-4">
                                <label class="form-control-label small">Cuenta Domiciliacion Emergente</label>
                            </div>                                                                                   
                        </div>

                        <asp:Panel ID="pnlCuentaEmergente" CssClass="clearfix" Visible="false" runat="server">
                            <div class="form-group col-md-6">
                                <label for="ddlBanco" class="form-control-label small">Banco</label>
                                <div>
                                    <asp:DropDownList runat="server" id="ddlBanco_Emergente" OnSelectedIndexChanged="ddlBanco_Emergente_SelectedIndexChanged" CssClass="form-control" AutoPostBack="true">
                                         <asp:ListItem Selected="True" Value="CONTINENTAL" Text="CONTINENTAL"></asp:ListItem>
							            <asp:ListItem Value="INTERBANK" Text="INTERBANK"></asp:ListItem>
                                        <asp:ListItem Value="CREDITO" Text="CREDITO"></asp:ListItem>
                                        <asp:ListItem Value="SCOTIABANK" Text="SCOTIABANK"></asp:ListItem>
                                        <asp:ListItem Value="COMERCIO" Text="COMERCIO"></asp:ListItem>
                                        <asp:ListItem Value="RIPLEY" Text="RIPLEY"></asp:ListItem>
                                        <asp:ListItem Value="FALABELLA" Text="FALABELLA"></asp:ListItem>
                                        <asp:ListItem Value="PICHINCHA" Text="PICHINCHA"></asp:ListItem>
                                        <asp:ListItem Value="GNB" Text="GNB"></asp:ListItem>                                        
                                        <asp:ListItem Value="BANBIF" Text="BANBIF"></asp:ListItem>
                                         <asp:ListItem Value="MIBANCO" Text="MIBANCO"></asp:ListItem>
                                        <asp:ListItem Value="ALFIN" Text="ALFIN"></asp:ListItem>                                        
                                        <asp:ListItem Value="BANCO DE LA NACION" Text="BANCO DE LA NACION"></asp:ListItem>
                                       <%-- <asp:ListItem Value="OTRO" Text="OTRO"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </div>

				            <div class="form-group col-md-6">
                                <label for="txtBanco_Emergente" class="form-control-label small">Nombre Banco</label>
                                <div>
                                    <asp:TextBox ID="txtBanco_Emergente" runat="server" CssClass="form-control" Style="text-transform: uppercase;" data-parsley-trigger="keyup" data-parsley-type="alphanum" Enabled="false" Text="CONTINENTAL"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="RequiredFieldValidator5" ControlToValidate="txtBanco_Emergente" ErrorMessage="Favor de introducir el banco" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                </div>
                            </div>

				            <asp:Panel ID="pnlCuentaContinental_Emergente" CssClass="clearfix" runat="server" Visible="true">
					            <div class="form-group col-md-3">
						            <label for="txtBCEntidad_Emergente" class="form-control-label small">Entidad</label>
						            <div>
							            <asp:TextBox ID="txtBCEntidad_Emergente" runat="server" CssClass="form-control cuentaBancariaEmergenteItem" MaxLength="4" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancariaEmergente();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="RequiredFieldValidator6" ControlToValidate="txtBCEntidad_Emergente" ErrorMessage="Capture Entidad" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBCEntidad_Emergente" ValidationExpression = "^[\s\S]{4,}$" 
                                             ErrorMessage="Capture 4 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>

					            <div class="form-group col-md-3">
						            <label for="txtBCOficina_Emergente" class="form-control-label small">Oficina</label>
						            <div>
							            <asp:TextBox ID="txtBCOficina_Emergente" runat="server" CssClass="form-control cuentaBancariaEmergenteItem" MaxLength="4" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancariaEmergente();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="RequiredFieldValidator7" ControlToValidate="txtBCOficina_Emergente" ErrorMessage="Capture Oficina" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBCOficina_Emergente" ValidationExpression = "^[\s\S]{4,}$" 
                                             ErrorMessage="Capture 4 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>

					            <div class="form-group col-md-2">
						            <label for="txtBCDC_Emergente" class="form-control-label small">DC</label>
						            <div>
							            <asp:TextBox ID="txtBCDC_Emergente" runat="server" CssClass="form-control cuentaBancariaEmergenteItem" MaxLength="2" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancariaEmergente();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="RequiredFieldValidator8" ControlToValidate="txtBCDC_Emergente" ErrorMessage="Capture DC" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBCDC_Emergente" ValidationExpression = "^[\s\S]{2,}$" 
                                             ErrorMessage="Capture 2 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>

					            <div class="form-group col-md-4">
						            <label for="txtBCCuenta_Emergente" class="form-control-label small">Cuenta</label>
						            <div>
							            <asp:TextBox ID="txtBCCuenta_Emergente" runat="server" CssClass="form-control cuentaBancariaEmergenteItem" MaxLength="10" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancariaEmergente();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="RequiredFieldValidator9" ControlToValidate="txtBCCuenta_Emergente" ErrorMessage="Capture Cuenta" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBCCuenta_Emergente" ValidationExpression = "^[\s\S]{10,}$" 
                                             ErrorMessage="Capture 10 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>
				            </asp:Panel>

                            <asp:Panel ID="pnlCuentaInterbank_Emergente" runat="server" Visible="false">
                                <div class="form-group col-md-2">
						            <label for="txtBINoOficina_Emergente" class="form-control-label small">No. Oficina</label>
						            <div>
							            <asp:TextBox ID="txtBINoOficina_Emergente" runat="server" CssClass="form-control cuentaBancariaEmergenteItem" MaxLength="3" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancariaEmergente();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="RequiredFieldValidator10" ControlToValidate="txtBINoOficina_Emergente" ErrorMessage="Capure No. Oficina" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBINoOficina_Emergente" ValidationExpression = "^[\s\S]{3,}$" 
                                             ErrorMessage="Capture 3 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>

					            <div class="form-group col-md-4">
						            <label for="txtBICuenta_Emergente" class="form-control-label small">Cuenta</label>
						            <div>
							            <asp:TextBox ID="txtBICuenta_Emergente" runat="server" CssClass="form-control cuentaBancariaEmergenteItem" MaxLength="10" onKeyPress="return EvaluateText('%d', this);" onkeyup="completeCuentaBancariaEmergente();"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ID="RequiredFieldValidator12" ControlToValidate="txtBICuenta_Emergente" ErrorMessage="Capture Cuenta" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ControlToValidate="txtBICuenta_Emergente" ValidationExpression = "^[\s\S]{10,}$" 
                                             ErrorMessage="Capture 10 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
						            </div>
					            </div>
                            </asp:Panel>				                                      			
            
                            <div class="form-group col-md-6">
                                <label for="txtCuentaDesembolso_Emergente" class="form-control-label small">Cuenta Bancaria</label>
                                <div>
                                    <asp:TextBox ID="txtCuentaDesembolso_Emergente" runat="server" CssClass="form-control" MaxLength="20" onKeyPress="return EvaluateText('%d', this);"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ForeColor="Red" ControlToValidate="txtCuentaDesembolso_Emergente" ErrorMessage="Capture el Número de Cuenta Bancaria" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <asp:Panel ID="pnlCuentaOtroBanco_Emergente" runat="server" Visible="false">
					            <div class="form-group col-md-6">
						            <label for="txtCuentaCCI_Emergente" class="form-control-label small">Cuenta C.C.I.</label>
						            <div>
							            <asp:TextBox ID="txtCuentaCCI_Emergente" runat="server" CssClass="form-control" MaxLength="20" onKeyPress="return EvaluateText('%d', this);"></asp:TextBox>
							            <asp:RequiredFieldValidator runat="server" ForeColor="Red" ControlToValidate="txtCuentaCCI_Emergente" ErrorMessage="Favor de introducir el Número de Cuenta C.C.I." ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
						            </div>
					            </div>
				            </asp:Panel>  
                            </asp:Panel>
                              <asp:Panel ID="PanelSalto" runat="server" Visible="false">
                             <div class="form-group row">  
                                    <div class="form-group col-md-6">
                                <label for="txtNumTarjetaVisaEME" class="form-control-label small">Numero de Tarjeta</label>
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fab fa-cc-visa"></i></span>
                                    <asp:TextBox ID="txtNumTarjetaVisaEME" runat="server" CssClass="form-control" MaxLength="19" onKeyPress="return EvaluateText('%d', this);" onBlur="hideCvBINTarjetaVISAEME();"></asp:TextBox>                                    
                                     <asp:RequiredFieldValidator ID="rvftxtNumTarjetaVisaEME" runat="server" ForeColor="Red" ControlToValidate="txtNumTarjetaVisaEME" ErrorMessage="Capture el Numero de Tarjeta" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ControlToValidate="txtNumTarjetaVisaEME" ValidationExpression = "^[\s\S]{19,}$" 
                                            ErrorMessage="Capture los 16 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
                                     <asp:CustomValidator id="cvBINTarjetaVISAEME" runat="server" ErrorMessage="Error BIN Tarjeta VISA" ValidationGroup="DatosCredito" CssClass="small" ForeColor="Red" Display="Dynamic" 
                                            EnableClientScript="False"></asp:CustomValidator>
                                
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="" class="form-control-label small">Mes Expiracion</label>                                                                                                 
                                <div>                                    
                                    <asp:DropDownList ID="ddlMExpirationEME" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="1">Enero</asp:ListItem>
                                        <asp:ListItem Value="2">Febrero</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Abril</asp:ListItem>
                                        <asp:ListItem Value="5">Mayo</asp:ListItem>
                                        <asp:ListItem Value="6">Junio</asp:ListItem>
                                        <asp:ListItem Value="7">Julio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                        <asp:ListItem Value="10">Octubre</asp:ListItem>
                                        <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                        <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group col-md-2">
                                <label for="txtAnioExpiraVisaEME" class="form-control-label small">Año Expiracion</label>                                                                                                 
                                <div>
                                    <asp:TextBox ID="txtAnioExpiraVisaEME" runat="server" CssClass="form-control" placeholder="AAAA" MaxLength="4" onKeyPress="return EvaluateText('%d', this);"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvftxtAnioExpiraVisaEME" runat="server" ForeColor="Red" ControlToValidate="txtAnioExpiraVisaEME" ErrorMessage="Capture el Año de Expiracion" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                  <asp:RegularExpressionValidator ControlToValidate="txtAnioExpiraVisaEME" ValidationExpression = "^[\s\S]{4,}$" 
                                            ErrorMessage="Capture 4 Digitos" ValidationGroup="DatosCredito" ForeColor="Red" Display="Dynamic" CssClass="small" runat="server"></asp:RegularExpressionValidator>
                             </div>                                
                            </div>

                            <div class="form-group col-md-2">
                                <label for="txtCuentaCVVEME" class="form-control-label small">CVV</label>
                                <div>
                                    <asp:TextBox ID="txtCuentaCVVEME" runat="server" CssClass="form-control" MaxLength="4" onKeyPress="return EvaluateText('%d', this);"></asp:TextBox>
                              <asp:RequiredFieldValidator runat="server" ForeColor="Red" ControlToValidate="txtCuentaCVVEME" ErrorMessage="Capture el Número de CVV" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"></asp:RequiredFieldValidator>
                                
                              </div>
                            </div> 
                                    
                        </div>
				            </asp:Panel> 
                       
                        
                       
                    </div>

                    <asp:Panel ID="pnlEdoCuenta" runat="server">
                    <hr />                    
                    <div class="form-group">
                        <div class="form-group row">
                            <asp:Label ID="lblSaldoAnt" runat="server" CssClass="col-sm-3 form-control-label small"  Font-Bold="true">
                                Saldo Anterior EE.CC.</asp:Label>                        
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtSaldoAnt" runat="server" CssClass="form-control" Enabled="true" onKeyPress="return EvaluateText('%f', this);" MaxLength="10"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="txtSaldoAnt" ErrorMessage="Capture el Saldo Anterior EE.CC." ForeColor="Red" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"/>                                
                            </div>

                            <label for="txtFechaDeposito" class="col-sm-3 form-control-label small">
                                Fecha Deposito Haberes</label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtFechaDeposito" runat="server" CssClass="form-control" placeholder="(dd/mm/aaaa)"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtFechaDeposito" ErrorMessage="Capture Fecha Deposito" ForeColor="Red" ValidationGroup="DatosCredito" Display="Dynamic" CssClass="small"/>
                            </div>                            
                        </div>

                        </div>
                        <div class="form-group">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <asp:Button ID="btnCapturarInfoEdoCta" runat="server" Text="Capturar Info. EE.CC." CssClass="btn btn-sm btn-info" OnClick="btnCapturarInfoEdoCta_Click" Enabled="true" />
                                </div>
                                <div class="col-sm-10">
                                     <asp:Panel ID="pnlErrorCapturaEdoCta" class="col-sm-12" Visible="false" runat="server">                                
                                        <div class="alert alert-warning" style="padding:5px 15px 5px 15px; margin:0px 0px;">
                                            <asp:Label ID="lblErrorCapturaEdoCta" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>  
                                </div>
                            </div>

                            <div id="EdoCta" class="form-group row" visible="false" runat="server">
                                <table cellspacing="0" class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th><span class="form-control-label">Fecha</span></th>
                                            <th><span class="form-control-label">Saldo Inicial</span></th>
                                            <th><span class="form-control-label">Retiro</span></th>
                                            <th><span class="form-control-label">Deposito</span></th>
                                            <th><span class="form-control-label">Saldo Final</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtFecha1" runat="server" CssClass="form-control" Enabled="false" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSaldoInicial1" runat="server" CssClass="form-control" Enabled="false" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMontoRet1" runat="server" CssClass="form-control" Enabled="true" onKeyPress="return EvaluateText('%f', this);" MaxLength="10" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMontoDep1" runat="server" CssClass="form-control" Enabled="true" onKeyPress="return EvaluateText('%f', this);" MaxLength="10" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSaldoFinal1" runat="server" CssClass="form-control" Enabled="false" Text=""></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtFecha2" runat="server" CssClass="form-control" Enabled="false" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSdoIni2" runat="server" CssClass="form-control" Enabled="false" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMontoRet2" runat="server" CssClass="form-control" Enabled="true" onKeyPress="return EvaluateText('%f', this);" MaxLength="10" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMontoDep2" runat="server" CssClass="form-control" Enabled="true" onKeyPress="return EvaluateText('%f', this);" MaxLength="10" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSaldoFin2" runat="server" CssClass="form-control" Enabled="false" Text=""></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtFecha3" runat="server" CssClass="form-control" Enabled="false" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSdoIni3" runat="server" CssClass="form-control" Enabled="false" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMontoRet3" runat="server" CssClass="form-control" Enabled="true" onKeyPress="return EvaluateText('%f', this);" MaxLength="10" Text=""></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMontoDep3" runat="server" CssClass="form-control" Enabled="true" onKeyPress="return EvaluateText('%f', this);" MaxLength="10" Text=""></asp:TextBox>

                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSaldoFin3" runat="server" CssClass="form-control" Enabled="false" Text=""></asp:TextBox>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>                            
                                <div class="form-group clearfix">
                                    <div class="col">
                                        <asp:CustomValidator id="cvCapturaEdoCta" runat="server" ErrorMessage="<span class='fa fa-exclamation-circle fa-fw fa-lg'></span>&nbsp;No ha terminado la captura del estado de cuenta, haga click en 'Confirmar Info' para guardar o 'Reset' para descartar" ValidationGroup="CapturaEdoCuenta" CssClass="small alert alert-danger" style="padding:5px 15px 5px 15px; margin:0px 0px;" Display="Dynamic" 
                                            EnableClientScript="False"></asp:CustomValidator>
                                    </div>
                                    <div class="col pull-right">                                        
                                        <asp:Button ID="btnConfirmar" runat="server" Text="Confirmar Info" CssClass="btn btn-sm btn-success" OnClick="btnConfirmarClick" Enabled="true" />                            
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-sm btn-default" OnClick="btnResetClick" Enabled="true" />
                                    </div>
                                </div> 
                            </div>                                 
                        </div><!--form-group-->
                        </asp:Panel>
                    </div><!--izq-->
            </ContentTemplate>
            </asp:UpdatePanel>
        </div><!--panel-body-->
    </div><!--DATOS DEL CREDITO-->
    <h3>VALIDAR Y CONTINUAR</h3>
    <div>        
        <asp:UpdatePanel ID="upGuardar" UpdateMode="Conditional" RenderMode="Inline" runat="server" class="form-group row">
        <ContentTemplate>                                                    
            <div class="col-sm-12">
                <asp:Button runat="server" ID="btnValidar" Text="Validar y continuar" CssClass="btn btn-sm btn-primary" OnClick="btnGuardarClick" />      
                <br /><br />  
                <asp:ValidationSummary runat="server" ID="vsError" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos:" ForeColor="Red" />
                <asp:ValidationSummary runat="server" ID="vsTiporedito" DisplayMode="BulletList" HeaderText="Verifique los siguientes datos de Tipo Credito:" ForeColor="Red" ValidationGroup="TipoCredito" CssClass="small" />
                <asp:ValidationSummary runat="server" ID="vsDatosCredito" DisplayMode="BulletList" HeaderText="Verifique los siguientes Datos del Credito :" ForeColor="Red" ValidationGroup="DatosCredito" CssClass="small" />        
                </div>
            <asp:Panel ID="pnlError" class="col-sm-12" Visible="false" runat="server">                                
                <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </div>
            </asp:Panel>                            
        </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress AssociatedUpdatePanelID="upGuardar" DisplayAfter="100" runat="server">
            <ProgressTemplate>
                  <div class="animationload">
                    <div class="osahanloading"></div>                    
                </div>              
            </ProgressTemplate>
        </asp:UpdateProgress>        
    </div>         
  </div>
    <!--<div class="animationload">
            <div class="osahanloading"></div>
            <span class="osahanloading_delaytext small">La operacion esta tardando mas de los esperado, no cierre la ventana...</span> 
        </div>-->
<script type="text/javascript" src="/js/siteCommon.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // Activar Tooltips
        activarTooltips();
    });

    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_endRequest(function () {
        // Activar Tooltips
        activarTooltips();

        $(".digDecimal").change(function () {
            var valor = $(this).val();
            if ($.isNumeric(valor)) {
                $(this).val(addZeroes(valor));
                $('#aspnetForm').parsley().validate();
            } else
                $(this).val("");
        });

        if ($('#<%=txtNumTarjetaVisa.ClientID%>').length > 0) {
            document.getElementById('<%=txtNumTarjetaVisa.ClientID%>').addEventListener('input', function (e) {
                var target = e.target, position = target.selectionEnd, length = target.value.length;

                target.value = target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
                target.selectionEnd = position += ((target.value.charAt(position - 1) === ' ' && target.value.charAt(length - 1) === ' ' && length !== target.value.length) ? 1 : 0);
            });
        }
         if ($('#<%=txtNumTarjetaVisaEME.ClientID%>').length > 0) {
            document.getElementById('<%=txtNumTarjetaVisaEME.ClientID%>').addEventListener('input', function (e) {
                var target = e.target, position = target.selectionEnd, length = target.value.length;

                target.value = target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
                target.selectionEnd = position += ((target.value.charAt(position - 1) === ' ' && target.value.charAt(length - 1) === ' ' && length !== target.value.length) ? 1 : 0);
            });
        }
    });

    function hideCvBINTarjetaVISA()
    {
        if ($("#<%=this.cvBINTarjetaVISA.ClientID%>").length)
        {
            $("#<%=this.cvBINTarjetaVISA.ClientID%>").remove();
        }
    }
    function hideCvBINTarjetaVISAEME()
    {
        if ($("#<%=this.cvBINTarjetaVISAEME.ClientID%>").length)
        {
            $("#<%=this.cvBINTarjetaVISAEME.ClientID%>").remove();
        }
    }
</script>
</asp:Content>



