﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Globalization;
using wsSOPF;

public partial class Site_Solicitudes_frmTipoCredPeru : System.Web.UI.Page
{
    #region "Propiedades"

    public bool IsCapturaEdoCuenta
    {
        get { return (ViewState[this.UniqueID + "_IsCapturaEdoCuenta"] != null) ? (bool)ViewState[this.UniqueID + "_IsCapturaEdoCuenta"] : false; }
        set { ViewState[this.UniqueID + "_IsCapturaEdoCuenta"] = value; }
    }
  
    private bool isEdicion
    {
        get { return (ViewState[this.UniqueID + "_isEdicion"] != null) ? (bool)ViewState[this.UniqueID + "_isEdicion"] : false; }
        set { ViewState[this.UniqueID + "_isEdicion"] = value; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			ViewState["IDCliente"] = Request.QueryString["idcliente"];
            int isE = Convert.ToInt32(Request.QueryString["isE"]);
            string folio = "";
            int idPromotor = 0;
            int idCanal = 0;
            string score = "";
            int Origen = 0;
            int idTipoCliente = 0;
            string categoriaCliente = "";
            int idProducto = 0;
            int idSubProducto = 0;
            int idTipoCredito = 0;

            ddlConvenio.Enabled = true;
            pnlMontosLiquidacion.Visible = true;
            txtSaldoAnt.Visible = true;
            lblSaldoAnt.Visible = true;
            ddlFrePag.Enabled = true;

            if (isE == 1)
                this.isEdicion = true;

            txtPrimerVencimiento.Text = DateTime.Now.ToString("dd/MM/yyyy");
            
            TBCATTipoConvenio[] TipoConvenio;            
			Combo[] TipoCredito_PE;
			Combo[] CanalVentas_PE;
			Combo[] TipoEvaluacion_PE;
			TBCATPromotor[] Promotores;
            List<TBCatOrigen> Origenes;
            List<TBCATProducto> Plazos = new List<TBCATProducto>();
            List<Combo> Productos;

            using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
            {
                TipoConvenio = wsCatalogo.ObtenerTipoConvenio(2, int.Parse(Session["cve_sucurs"].ToString()), string.Empty, 0, 0);                
                Productos = wsCatalogo.ObtenerCombo_Convenio(2).ToList();
				TipoCredito_PE = wsCatalogo.ObtenerCombo_TipoCredito_PE();
				CanalVentas_PE = wsCatalogo.ObtenerCombo_CanalVentas_PE();
				TipoEvaluacion_PE = wsCatalogo.ObtenerCombo_TipoEvaluacion_PE();
				Promotores = wsCatalogo.ObtenerPromotores(1, 7);
                Origenes = wsCatalogo.ObtenerTBCatOrigen(Accion: 1).ToList();
            }

            int idsolicitud = Convert.ToInt32(Request.QueryString["idsolicitud"]);
            TBSolicitudes.DatosSolicitud[] Datos = null;     

            if (TipoConvenio.Count() > 0)
            {
                txtTipoConvenio.Text = TipoConvenio.FirstOrDefault(tc => tc.IdTipoConvenio == 2).TipoConvenio;
            }
            cboMetDis.Items.Insert(0, new ListItem("Transferencia", "TRANSFERENCIA"));
            cboMetDis.Items.Insert(1, new ListItem("Orden de Pago", "ORDEN DE PAGO"));
            cboMetDis.Items.Insert(2, new ListItem("Yape", "YAPE"));
            using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
			{

                txtMontoGAT.Text = wsSolicitud.ObtenerGAT().ToString();

				Datos = wsSolicitud.ObtenerDatosSolicitud(idsolicitud);
                
                if (Datos.Length > 0)
                {

                    cboMetDis.SelectedValue = Datos[0].vMetDispersion.ToString();
                    txtPrimerVencimiento.Text = Datos[0].PrimerPago.GetValueOrDefault().ToString("dd/MM/yyyy");
                    txtIDSolicitud.Text = Datos[0].IDSolictud.ToString();
                    txtFechaDeposito.Text = (Datos[0].FechaDeposito != null) ? Datos[0].FechaDeposito.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;

                    // Cargar TipoConvenio(Empresa), Convenio(Producto), Producto(Plazo)
                    ddlProducto.SelectedValue = Datos[0].IDProducto.ToString();                                        

                    
                    if (Datos[0].IDCredito.ToString() == "3") // AMPLIACION
                    {
                        txtMontoLiquidacion.Text = Datos[0].TMP_MontoLiquidacion.ToString();
                    }                                                

                    txtImporteCredito.Text = Datos[0].ImporteCredito.ToString();
                    txtPlazo.Text = Datos[0].Plazo.ToString();
                    txtTasaInteres.Text = Datos[0].TasaInteres.ToString();
                    ddlSeguroDesgravamen.SelectedIndex = Datos[0].TipoSeguroDesgravamen;
                    ddlSeguroDesgravamen_SelectedIndexChange(sender, e);

                    //txtMontoGAT.Text = Datos[0].MontoGAT.ToString();
                    txtMontoUsoCanal.Text = Convert.ToDecimal(Datos[0].MontoUsoCanal.ToString()).ToString();
                    if (txtMontoUsoCanal.Text == "0")
                        txtMontoUsoCanal.Text = "0.00";
                    txtMontoTotalFinanciar.Text = Datos[0].MontoTotalFinanciar.ToString();
                    txtCostoTotalCred.Text = Datos[0].CostoTotalCred.ToString();
                    txtMontoCuota.Text = Datos[0].MontoCuota.ToString();

                    txtMontoRet1.Text = decimal.Round(Datos[0].MontoRetDia1, 2, MidpointRounding.AwayFromZero).ToString();
                    txtMontoRet2.Text = decimal.Round(Datos[0].MontoRetDia2, 2, MidpointRounding.AwayFromZero).ToString();
                    txtMontoRet3.Text = decimal.Round(Datos[0].MontoRetDia3, 2, MidpointRounding.AwayFromZero).ToString();
                    txtMontoDep1.Text = decimal.Round(Datos[0].MonDepDia1, 2, MidpointRounding.AwayFromZero).ToString();
                    txtMontoDep2.Text = decimal.Round(Datos[0].MonDepDia2, 2, MidpointRounding.AwayFromZero).ToString();
                    txtMontoDep3.Text = decimal.Round(Datos[0].MonDepDia3, 2, MidpointRounding.AwayFromZero).ToString();

                    txtSaldoAnt.Text = decimal.Round(Datos[0].SaldoIniCta, 2, MidpointRounding.AwayFromZero).ToString();

                    txtSaldoInicial1.Text = txtSaldoAnt.Text;
                    txtSdoIni2.Text = decimal.Round(Datos[0].SaldoIniCta2, 2, MidpointRounding.AwayFromZero).ToString();
                    txtSdoIni3.Text = decimal.Round(Datos[0].SaldoIniCta3, 2, MidpointRounding.AwayFromZero).ToString();

                    txtSaldoFinal1.Text = decimal.Round(Datos[0].SaldoFinCta, 2, MidpointRounding.AwayFromZero).ToString();
                    txtSaldoFin2.Text = decimal.Round(Datos[0].SaldoFinCta2, 2, MidpointRounding.AwayFromZero).ToString();
                    txtSaldoFin3.Text = decimal.Round(Datos[0].SaldoFinCta3, 2, MidpointRounding.AwayFromZero).ToString();
                    if (Datos[0].idConvenio !=11)
                    {
                        btnCapturarInfoEdoCta_Click(sender, e);

                        btnConfirmarClick(sender, e);
                    }
                                                      

                    // En la Actualizacion de solicitud, seleccionamos por default otro banco y despues comprobamos si es una seleccion diferente.
                    txtBanco.Text = Datos[0].Banco;
                    ddlBanco.SelectedValue = "OTRO";
                    txtBanco.Enabled = true;
                    foreach (ListItem item in ddlBanco.Items)
                    {
                        if (item.Text.Trim().ToUpper() == txtBanco.Text.Trim().ToUpper())
                        {
                            ddlBanco.SelectedValue = item.Value;
                            txtBanco.Enabled = false;
                            break;
                        }
                    }

                    txtCuentaDesembolso.Text = Datos[0].CuentaDesembolso;
                    if (Datos[0].Banco.ToUpper() == "CONTINENTAL")
                    {
                        // JRVB - 05/04/2018, Se deja ya que en la informacion historica antes de este cambio no podemos asegurar que se capturaron los 20 digitos.
                        try
                        {
                            txtBCEntidad.Text = txtCuentaDesembolso.Text.Trim().Substring(0, 4);
                            txtBCOficina.Text = txtCuentaDesembolso.Text.Trim().Substring(4, 4);
                            txtBCDC.Text = txtCuentaDesembolso.Text.Trim().Substring(8, 2);
                            txtBCCuenta.Text = txtCuentaDesembolso.Text.Trim().Substring(10, 10);
                        }
                        catch
                        { }
                    }
                    else if (Datos[0].Banco.ToUpper() == "INTERBANK")
                    {
                        try
                        {
                            txtBINoOficina.Text = txtCuentaDesembolso.Text.Trim().Substring(0, 3);
                            txtBICuenta.Text = txtCuentaDesembolso.Text.Trim().Substring(3, 10);
                        }
                        catch
                        { }
                    }
                    else
                    {
                        txtCuentaCCI.Text = Datos[0].CuentaCCI;
                    }
                    
                    if (Datos[0].TipoDomiciliacion == 2)// VISA
                    {
                        chkDomiciliacionVisa.Checked = true;                        
                        txtNumTarjetaVisa.Text = (Datos[0].NumeroTarjetaVisa != null) ? Regex.Replace(Datos[0].NumeroTarjetaVisa, ".{4}", "$0 ").Trim() : string.Empty;
                        ddlMesExpiraVisa.SelectedValue = Datos[0].MesExpiraTarjetaVisa.ToString();
                        txtAnioExpiraVisa.Text = Datos[0].AnioExpiraTarjetaVisa.ToString();
                        txtCuentaCVV.Text = Datos[0].vCVV.ToString();
                    }

                    folio = Datos[0].pedido;
                    idPromotor = Datos[0].IDPromotor;
                    idCanal = Datos[0].IDCanalVenta;
                    score = Datos[0].ScoreExperian.ToString();
                    Origen = Datos[0].Origen_Id;
                    idTipoCliente = Datos[0].TipoCliente;
                    idProducto = Datos[0].idConvenio;
                    idSubProducto = Datos[0].IDSubProducto;
                    idTipoCredito = Datos[0].IDCredito;
                }
                else
                {
                    folio = Request.QueryString["folio"];
                    idPromotor = Convert.ToInt32(Request.QueryString["idPromotor"]);
                    idCanal = Convert.ToInt32(Request.QueryString["idCanal"]);
                    score = Request.QueryString["score"];
                    Origen = Convert.ToInt32(Request.QueryString["idOrigen"]);
                    idTipoCliente = Convert.ToInt32(Request.QueryString["idTipoCliente"]);
                    categoriaCliente = Request.QueryString["categoriaCliente"];
                    idProducto = Convert.ToInt32(Request.QueryString["idProducto"]);
                    idSubProducto = Convert.ToInt32(Request.QueryString["idSubProducto"]);
                    idTipoCredito = Convert.ToInt32(Request.QueryString["idTipoCredito"]);
                }
			}

            using (wsSOPF.CatalogoClient wsCatalogo = new CatalogoClient())
            {
                if (idProducto > 0 && idTipoCredito > 0)
                {
                    Plazos = wsCatalogo.ObtenerTBCATProducto(5, idProducto, string.Empty, 1, 0, idTipoCredito).ToList();
                }
            }

            if (Productos.Count > 0)
            {
                ddlConvenio.DataSource = Productos;
                ddlConvenio.DataBind();
                ddlConvenio.SelectedValue = idProducto.ToString();

                if (idProducto == 11)
                {
                    ddlConvenio.Enabled = false;
                    txtMontoGAT.Text = "7.50000";
                    txtSaldoAnt.Visible = false;
                    lblSaldoAnt.Visible = false;
                    btnCapturarInfoEdoCta.Visible = false;
                    cboMetDis.Enabled = true;
                    divMetDis.Visible = true;
                     ddlFrePag.SelectedValue = "QUINCENAL";
                    //ddlFrePag.SelectedValue = Datos[0].vFrecuenciaPago.ToString();
                    ddlFrePag.Enabled = false;
                    

                }
            }


            if (Plazos.Count > 0)
            {
                ddlProducto.DataSource = Plazos;
                ddlProducto.DataBind();
                ddlProducto_SelectedIndexChanged(sender, e);
            }


            

            hdnFolio.Value = folio;
            hdnIdPromotor.Value = idPromotor.ToString();
            hdnIdCanal.Value = idCanal.ToString();
            hdnScore.Value = score;
            hdnOrigen.Value = Origen.ToString();
            hdnIdTipoCliente.Value = idTipoCliente.ToString();
            hdnCategoriaCliente.Value = categoriaCliente;
            hdnIdSubProducto.Value = idSubProducto.ToString();
            hdnIdTipoCredito.Value = idTipoCredito.ToString();
            hdnTipoCredito.Value = TipoCredito_PE.FirstOrDefault(tc => tc.Valor == idTipoCredito).Descripcion;

           

            if (txtTipoConvenio.Text == "DÉBITO AUTOMÁTICO")
            {
                chkDomiciliacionVisa.Checked = true;

            }

            pnlCuentaOtroBanco.Visible = false;
            if (ddlBanco.SelectedValue == "CONTINENTAL")
            {
                pnlCuentaContinental.Visible = true;
                pnlCuentaInterbank.Visible = false;
            }
            else if (ddlBanco.SelectedValue == "INTERBANK")
            {
                pnlCuentaContinental.Visible = false;
                pnlCuentaInterbank.Visible = true;              
            }
            else
                pnlCuentaOtroBanco.Visible = true;

            ddlBanco_SelectedIndexChanged(sender, e);
            chkDomiciliacionVisa_CheckedChanged(sender, e);

            CargarCuentaDomiciliacionEmergente((Datos.Length > 0) ? Datos[0].CuentasDomiciliacionEmergentes.ToList() : new List<TBSolicitudes.CuentaDomiciliacionEmergente>(), sender, e);

            wsSOPF.ClientesClient ws = new ClientesClient();
            ResultadoOfint resultado = ws.ObtenerSolicitudActivaDeCliente(Convert.ToInt32(ViewState["IDCliente"]));

            lblPoliticaAmpliacion.Visible = false;
            if (resultado.Codigo == 0)
            {
                //Hay una solicitud activa?
                if (resultado.ResultObject > 0)
                {
                    using (SolicitudClient wsSolicitud = new SolicitudClient())
                    {
                        //Se esta editando una solicitud?
                        if (idsolicitud > 0)
                        {

                            SolicitudReestructuraCondiciones condiciones = new SolicitudReestructuraCondiciones();
                            condiciones.idSolicitud = idsolicitud;
                            condiciones.idEstatus = 0;
                            SolicitudReestructura reestructura = wsSolicitud.ObtenerReestructura(condiciones);

                            mostrarLiquidacionParaAmplicacion(reestructura.idSolicitudReestructura, reestructura.totalLiquidar, reestructura.fechaLiquidar);
                        }
                        else
                        {
                            consultarLiquidacion(resultado.ResultObject, DateTime.Now);
                        }

                        PoliticaAmpliacion politica = wsSolicitud.PoliticaAmplicacionParaCliente(Convert.ToInt32(ViewState["IDCliente"]));
                        lblPoliticaAmpliacion.Visible = true;

                        if (politica.id == 1)
                        {
                            lblPoliticaAmpliacion.CssClass = "label label-success";
                        }
                        else
                        {
                            lblPoliticaAmpliacion.CssClass = "label label-warning";
                        }
                        
                        lblPoliticaAmpliacion.Text = politica.descripcion;
                    }
                }
            }
            else
            {
                pnlError.Visible = true;
                lblError.Text = resultado.CodigoStr;
                btnValidar.Visible = false;
                upGuardar.Update();

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Accordion", "$(function(){ $('#divAccordion').accordion({active: 2}); });", true);
            }

            if (idProducto == 11)
            {               
                pnlMontosLiquidacion.Visible = false;
            }

        } // If Not PostBack
	}

    private void CargarCuentaDomiciliacionEmergente(List<TBSolicitudes.CuentaDomiciliacionEmergente> CuentasDomEmergentes, object sender, EventArgs e)
    {
        // Si existe la Cuenta Emergente
        if (CuentasDomEmergentes.Count() > 0)
        {
            TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente = CuentasDomEmergentes.FirstOrDefault();

            chkCuentaDomiciliacionEmergente.Checked = true;
            txtBanco_Emergente.Text = cuentaEmergente.Banco;
            ddlBanco_Emergente.SelectedValue = "OTRO";
            txtBanco_Emergente.Enabled = true;

            foreach (ListItem item in ddlBanco_Emergente.Items)
            {
                if (item.Text.Trim().ToUpper() == txtBanco_Emergente.Text.Trim().ToUpper())
                {
                    ddlBanco_Emergente.SelectedValue = item.Value;
                    txtBanco_Emergente.Enabled = false;
                    break;
                }
            } 
            txtNumTarjetaVisaEME.Text = (cuentaEmergente.NumeroTarjeta != null) ? Regex.Replace(cuentaEmergente.NumeroTarjeta, ".{4}", "$0 ").Trim() : string.Empty; ;
            ddlMExpirationEME.SelectedValue = cuentaEmergente.MesExpiraTarjeta.ToString();
            txtAnioExpiraVisaEME.Text = cuentaEmergente.AnioExpiraTarjeta.ToString();
            txtCuentaDesembolso_Emergente.Text = cuentaEmergente.Cuenta;
            txtCuentaCVVEME.Text = cuentaEmergente.Cvv.ToString();
            if (cuentaEmergente.Banco.ToUpper() == "CONTINENTAL")
            {
                // JRVB - 05/04/2018, Se deja ya que en la informacion historica antes de este cambio no podemos asegurar que se capturaron los 20 digitos.
                try
                {
                    txtBCEntidad_Emergente.Text = txtCuentaDesembolso_Emergente.Text.Trim().Substring(0, 4);
                    txtBCOficina_Emergente.Text = txtCuentaDesembolso_Emergente.Text.Trim().Substring(4, 4);
                    txtBCDC_Emergente.Text = txtCuentaDesembolso_Emergente.Text.Trim().Substring(8, 2);
                    txtBCCuenta_Emergente.Text = txtCuentaDesembolso_Emergente.Text.Trim().Substring(10, 10);
                }
                catch
                { }
            }
            else if (cuentaEmergente.Banco.ToUpper() == "INTERBANK")
            {
                try
                {
                    txtBINoOficina_Emergente.Text = txtCuentaDesembolso_Emergente.Text.Trim().Substring(0, 3);
                    txtBICuenta_Emergente.Text = txtCuentaDesembolso_Emergente.Text.Trim().Substring(3, 10);
                }
                catch
                { }
            }
            else
            {
                txtCuentaCCI_Emergente.Text = cuentaEmergente.CuentaCCI;
            }
        }

        ddlBanco_Emergente_SelectedIndexChanged(sender, e);
        chkCuentaDomiciliacionEmergente_CheckedChanged(sender, e);
    }

    protected void btnCapturarInfoEdoCta_Click(object sender, EventArgs e)
    {
        pnlErrorCapturaEdoCta.Visible = false;

        try
        {
            if (string.IsNullOrEmpty(txtFechaDeposito.Text.Trim()))
                throw new Exception("Debe capturar primero la Fecha Deposito de Haberes");

            if (string.IsNullOrEmpty(txtSaldoAnt.Text.Trim()))
                throw new Exception("Debe capturar primero el Saldo Anterior EE.CC.");

            DateTime FechaDeposito;

            if (DateTime.TryParseExact(txtFechaDeposito.Text.Trim(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out FechaDeposito))
            {
                txtFecha1.Text = FechaDeposito.ToString("dd/MM/yyyy");
                txtFecha2.Text = FechaDeposito.AddDays(1).ToString("dd/MM/yyyy");
                txtFecha3.Text = FechaDeposito.AddDays(2).ToString("dd/MM/yyyy");

                txtSaldoInicial1.Text = txtSaldoAnt.Text;

                this.EdoCta.Visible = true;
                txtFechaDeposito.Enabled = false;
                txtSaldoAnt.Enabled = false;
                btnCapturarInfoEdoCta.Enabled = false;
                this.IsCapturaEdoCuenta = true;
            }
            else
            {
                txtFechaDeposito.Text = string.Empty;
                throw new Exception("Formato de Fecha Deposito Haberes Incorrecto");
            }
        }
        catch (Exception ex)
        {
            lblErrorCapturaEdoCta.Text = ex.Message;
            pnlErrorCapturaEdoCta.Visible = true;
        }
    }

    protected void btnConfirmarClick(object sender, EventArgs e)
    {        
        txtMontoRet1.Text = (string.IsNullOrEmpty(txtMontoRet1.Text)) ? "0" : txtMontoRet1.Text;
        txtMontoRet2.Text = (string.IsNullOrEmpty(txtMontoRet2.Text)) ? "0" : txtMontoRet2.Text;
        txtMontoRet3.Text = (string.IsNullOrEmpty(txtMontoRet3.Text)) ? "0" : txtMontoRet3.Text;
        txtMontoDep1.Text = (string.IsNullOrEmpty(txtMontoDep1.Text)) ? "0" : txtMontoDep1.Text;
        txtMontoDep2.Text = (string.IsNullOrEmpty(txtMontoDep2.Text)) ? "0" : txtMontoDep2.Text;
        txtMontoDep3.Text = (string.IsNullOrEmpty(txtMontoDep3.Text)) ? "0" : txtMontoDep3.Text;

        txtMontoRet1.Enabled = false;
        txtMontoRet2.Enabled = false;
        txtMontoRet3.Enabled = false;
        txtMontoDep1.Enabled = false;
        txtMontoDep2.Enabled = false;
        txtMontoDep3.Enabled = false;

        txtSaldoFinal1.Text = Convert.ToString(Convert.ToDecimal(txtSaldoInicial1.Text) + Convert.ToDecimal(txtMontoDep1.Text) - Convert.ToDecimal(txtMontoRet1.Text));
        txtSdoIni2.Text = txtSaldoFinal1.Text;
        txtSaldoFin2.Text = Convert.ToString(Convert.ToDecimal(txtSdoIni2.Text) + Convert.ToDecimal(txtMontoDep2.Text) - Convert.ToDecimal(txtMontoRet2.Text));
        txtSdoIni3.Text = txtSaldoFin2.Text;
        txtSaldoFin3.Text = Convert.ToString(Convert.ToDecimal(txtSdoIni3.Text) + Convert.ToDecimal(txtMontoDep3.Text) - Convert.ToDecimal(txtMontoRet3.Text));

        btnConfirmar.Enabled = false;
        this.IsCapturaEdoCuenta = false;
    }

    protected void btnResetClick(object sender, EventArgs e)
    {
        this.EdoCta.Visible = false;
        btnCapturarInfoEdoCta.Enabled = true;
        btnConfirmar.Enabled = true;        

        txtFechaDeposito.Enabled = true;
        txtSaldoAnt.Enabled = true;

        txtMontoDep1.Text = string.Empty;
        txtMontoDep2.Text = string.Empty;
        txtMontoDep3.Text = string.Empty;

        txtMontoRet1.Text = string.Empty;
        txtMontoRet2.Text = string.Empty;
        txtMontoRet3.Text = string.Empty;

        txtSaldoInicial1.Text = string.Empty;
        txtSdoIni2.Text = string.Empty;
        txtSdoIni3.Text = string.Empty;

        txtSaldoFinal1.Text = string.Empty;
        txtSaldoFin2.Text = string.Empty;
        txtSaldoFin3.Text = string.Empty;

        txtFecha1.Text = string.Empty;
        txtFecha2.Text = string.Empty;
        txtFecha3.Text = string.Empty;

        txtMontoRet1.Enabled = true;
        txtMontoRet2.Enabled = true;
        txtMontoRet3.Enabled = true;
        txtMontoDep1.Enabled = true;
        txtMontoDep2.Enabled = true;
        txtMontoDep3.Enabled = true;

        this.IsCapturaEdoCuenta = false;
    }

    protected void btnGuardarClick(object sender, EventArgs e)
    {
        // Actualizar calculo para asegurar tener los valores actualizados.
        btnCalcular_Click(sender, e);
       
        valIconDatosCredito.Visible = false;        
        pnlError.Visible = false;

        Page.Validate("DatosCredito");        

        if (!IsValidGroup("DatosCredito"))
        {
            valIconDatosCredito.Visible = true;
            upDatosCredito.Update();
        }
            if (!txtSaldoAnt.Visible)
            {
                cvCapturaEdoCta.IsValid = true;
                RequiredFieldValidator11.IsValid = true;
                valIconDatosCredito.Visible = false;
                upDatosCredito.Update();
            }

            

        
        /*
                if (this.IsCapturaEdoCuenta)
                {
                    cvCapturaEdoCta.IsValid = false;
                    valIconDatosCredito.Visible = true;
                    upDatosCredito.Update();
                }*/

        // Evaluar los CustomValidator
        // 1. Si se capturo Numero de Tarjeta Validamos el BIN y la pagina es Valida(se pasaron las otras validaciones)
        if (txtNumTarjetaVisa.Text.Trim() != string.Empty && Page.IsValid)
        {
            if (!esValidoBINTarjetaVISA(txtNumTarjetaVisa.Text.Trim()))
            {                
                cvBINTarjetaVISA.IsValid = false;
                valIconDatosCredito.Visible = true;
                upDatosCredito.Update();
            }
        }
        
        // 2. Si el panel de montos de liquidacion esta visible evaluamos
        if (Page.IsValid && pnlMontosLiquidacion.Visible)
        {
            if (decimal.Parse(txtMontoDesembolso.Text.Trim()) < 0)
            {
                cvMontoDesembolsoLiquidacion.IsValid = false;
                valIconDatosCredito.Visible = true;
                upDatosCredito.Update();
            }
        }
        // 3. Si se capturo Numero de Tarjeta Validamos Emergente
        if (txtNumTarjetaVisaEME.Text.Trim() != string.Empty && Page.IsValid)
        {
            if (!esValidoBINTarjetaVISA(txtNumTarjetaVisaEME.Text.Trim()))
            {
                cvBINTarjetaVISAEME.IsValid = false;
                valIconDatosCredito.Visible = true;
                upDatosCredito.Update();
            }
        }
       
        if (Page.IsValid)
        {
            try
            {
                TBSolicitudes.SolicitudPeru _SolPeru = new TBSolicitudes.SolicitudPeru();
                _SolPeru.IDCliente = Convert.ToInt32(ViewState["IDCliente"]);
                _SolPeru.IDClienteA = Convert.ToInt32(ViewState["IDCliente"]);
                _SolPeru.IDAnalista = Convert.ToInt32(Session["UsuarioId"]);
                _SolPeru.IDSucursal = Convert.ToInt32(Session["cve_sucurs"]);

                // DATOS DEL CREDITO
                string strFecha;
                txtPrimerVencimiento.Text = txtPrimerVencimiento.Text.Replace("-", "/");
                strFecha = txtPrimerVencimiento.Text.Split('/')[2];
                strFecha += "/";
                strFecha += txtPrimerVencimiento.Text.Split('/')[1];
                strFecha += "/";
                strFecha += txtPrimerVencimiento.Text.Split('/')[0];

                _SolPeru.IDConvenio = Convert.ToInt32(ddlConvenio.SelectedValue);
                _SolPeru.IDProducto = Convert.ToInt32(ddlProducto.SelectedValue);
                _SolPeru.IDCredito = Convert.ToInt32(hdnIdTipoCredito.Value);//Convert.ToInt32(ddlTipoCredito.SelectedValue);
                _SolPeru.IDCanalVenta = Convert.ToInt32(hdnIdCanal.Value);//Convert.ToInt32(ddlCanalVentas.SelectedValue);
                _SolPeru.TipoCliente = Convert.ToInt32(hdnIdTipoCliente.Value);//ddlTipoCliente.SelectedIndex;
                _SolPeru.IDTipoEvaluacion = 1;//Convert.ToInt32(ddlTipoEvaluacion.SelectedValue);
                _SolPeru.IDPromotor = Convert.ToInt32(hdnIdPromotor.Value);//Convert.ToInt32(ddlPromotor.SelectedValue);
                _SolPeru.IDOrigen = Convert.ToInt32(hdnOrigen.Value);//Convert.ToInt32(ddlOrigen.SelectedValue);
                _SolPeru.ImporteCredito = Convert.ToDecimal(txtImporteCredito.Text);
                _SolPeru.Plazo = Convert.ToInt32(txtPlazo.Text);
                _SolPeru.TasaInteres = Convert.ToDecimal(txtTasaInteres.Text);
                _SolPeru.TipoSeguroDesgravamen = Convert.ToInt32(ddlSeguroDesgravamen.SelectedIndex);
                _SolPeru.SeguroDesgravamen = Convert.ToDecimal(txtSeguroDesgravamen.Text);
                _SolPeru.MontoGAT = Convert.ToDecimal(txtMontoGAT.Text);
                _SolPeru.MontoUsoCanal = Convert.ToDecimal(txtMontoUsoCanal.Text);
                _SolPeru.MontoTotalFinanciar = Convert.ToDecimal(txtMontoTotalFinanciar.Text);
                _SolPeru.CostoTotalCred = Convert.ToDecimal(txtCostoTotalCred.Text);
                _SolPeru.MontoCuota = Convert.ToDecimal(txtMontoCuota.Text);
                _SolPeru.Pedido = hdnFolio.Value;//txtFolioSolicitud.Text.Trim();
                _SolPeru.PrimerPago = Convert.ToDateTime(strFecha);
                _SolPeru.NumeroPoliza = "";//txtNumPoliza.Text;
                _SolPeru.NumeroCertificado = "";//txtNumCertificado.Text;
                _SolPeru.ReferenciaExperian = "";//txtReferenciaExperian.Text.Trim();
                _SolPeru.ScoreExperian = (!string.IsNullOrEmpty(hdnScore.Value.Trim())) ? decimal.Parse(hdnScore.Value.Trim()) : 0;
                _SolPeru.Deducciones = 0;
                _SolPeru.IDSubProducto = Convert.ToInt32(hdnIdSubProducto.Value);
                _SolPeru.vFrecuenciaPago = ddlFrePag.SelectedValue;
                _SolPeru.vMetDispersion = cboMetDis.SelectedItem.Text.ToUpper().Trim();
                if (hdnTipoCredito.Value == "AMPLIACIÓN")
                {
                    _SolPeru.TMPMontoLiquidacion = decimal.Parse(txtMontoLiquidacion.Text.Trim());
                }   

                // DESEMBOLSO
                _SolPeru.TipoDomiciliacion = 1;
                _SolPeru.Banco = txtBanco.Text.Trim().ToUpper();
                if (_SolPeru.Banco == "CONTINENTAL")
                {
                    _SolPeru.BCEntidad = txtBCEntidad.Text.Trim();
                    _SolPeru.BCOficina = txtBCOficina.Text.Trim();
                    _SolPeru.BCDC = txtBCDC.Text.Trim();
                    _SolPeru.BCCuenta = txtBCCuenta.Text.Trim();
                    txtCuentaDesembolso.Text = _SolPeru.BCEntidad + _SolPeru.BCOficina + _SolPeru.BCDC + _SolPeru.BCCuenta;
                }
                else if (_SolPeru.Banco == "INTERBANK")
                {
                    txtCuentaDesembolso.Text = txtBINoOficina.Text.Trim() + txtBICuenta.Text.Trim();
                }
                else
                {
                    _SolPeru.CuentaCCI = txtCuentaCCI.Text.Trim();
                }

                _SolPeru.CuentaDesembolso = txtCuentaDesembolso.Text.Trim();
                _SolPeru.CuentaDebito = string.Empty; // Este campo ya no se usa, se reemplaza por Cuenta Desembolso, ya que Pamela comenta que es el mismo con nombre diferente.

                // DOMICILIACION
                if (chkDomiciliacionVisa.Checked)
                {
                    _SolPeru.TipoDomiciliacion = 2;
                    _SolPeru.NumeroTarjetaVisa = txtNumTarjetaVisa.Text.Replace(" ", "");
                    _SolPeru.MesExpiraTarjetaVisa = int.Parse(ddlMesExpiraVisa.SelectedValue);
                    _SolPeru.AnioExpiraTarjetaVisa = int.Parse(txtAnioExpiraVisa.Text.Trim());
                    _SolPeru.vCVV = txtCuentaCVV.Text.Trim();
                }

                // DOMICILIACION EMERGENTE
                if (chkCuentaDomiciliacionEmergente.Checked)
                {
                    List<TBSolicitudes.CuentaDomiciliacionEmergente> cuentasEmergentes = new List<TBSolicitudes.CuentaDomiciliacionEmergente>();
                    TBSolicitudes.CuentaDomiciliacionEmergente cuentaEmergente = new TBSolicitudes.CuentaDomiciliacionEmergente();

                    cuentaEmergente.TipoDomiciliacion = 1;
                    cuentaEmergente.Banco = txtBanco_Emergente.Text.Trim().ToUpper();

                    switch (cuentaEmergente.Banco)
                    {
                        case "CONTINENTAL":
                            txtCuentaDesembolso_Emergente.Text = txtBCEntidad_Emergente.Text.Trim() + txtBCOficina_Emergente.Text.Trim() + txtBCDC_Emergente.Text.Trim() + txtBCCuenta_Emergente.Text.Trim();
                            break;
                        case "INTERBANK":
                            txtCuentaDesembolso_Emergente.Text = txtBINoOficina_Emergente.Text.Trim() + txtBICuenta_Emergente.Text.Trim();
                            break;
                        default:
                            cuentaEmergente.CuentaCCI = txtCuentaCCI_Emergente.Text.Trim();
                            break;
                    }

                    cuentaEmergente.Cuenta = txtCuentaDesembolso_Emergente.Text.Trim();
                    cuentaEmergente.NumeroTarjeta = txtNumTarjetaVisaEME.Text.Replace(" ", "");
                    cuentaEmergente.MesExpiraTarjeta = Convert.ToInt32(ddlMExpirationEME.SelectedValue);
                    cuentaEmergente.AnioExpiraTarjeta = Convert.ToInt32(txtAnioExpiraVisaEME.Text.Trim());
                    cuentaEmergente.Cvv = txtCuentaCVVEME.Text.Trim();
                    cuentasEmergentes.Add(cuentaEmergente);

                    _SolPeru.CuentasDomiciliacionEmergentes = cuentasEmergentes.ToArray();
                }

                if (!string.IsNullOrEmpty(txtFechaDeposito.Text.Trim()))
                {
                    string FechaDeposito = txtFechaDeposito.Text.Trim().Replace("-", "/");

                    System.Globalization.DateTimeFormatInfo amerikanskyFormat = new System.Globalization.CultureInfo("en-US", false).DateTimeFormat;
                    _SolPeru.fchDeposito = Convert.ToDateTime(FechaDeposito.Split('/')[1] + "/" + FechaDeposito.Split('/')[0] + "/" + FechaDeposito.Split('/')[2], amerikanskyFormat);
                }
                else
                {
                    _SolPeru.fchDeposito = null;
                }


                txtMontoRet1.Text = (string.IsNullOrEmpty(txtMontoRet1.Text)) ? "0" : txtMontoRet1.Text;
                txtMontoRet2.Text = (string.IsNullOrEmpty(txtMontoRet2.Text)) ? "0" : txtMontoRet2.Text;
                txtMontoRet3.Text = (string.IsNullOrEmpty(txtMontoRet3.Text)) ? "0" : txtMontoRet3.Text;
                txtMontoDep1.Text = (string.IsNullOrEmpty(txtMontoDep1.Text)) ? "0" : txtMontoDep1.Text;
                txtMontoDep2.Text = (string.IsNullOrEmpty(txtMontoDep2.Text)) ? "0" : txtMontoDep2.Text;
                txtMontoDep3.Text = (string.IsNullOrEmpty(txtMontoDep3.Text)) ? "0" : txtMontoDep3.Text;

                txtSaldoAnt.Text = (string.IsNullOrEmpty(txtSaldoAnt.Text)) ? "0" : txtSaldoAnt.Text;
                txtSdoIni2.Text = (string.IsNullOrEmpty(txtSdoIni2.Text)) ? "0" : txtSdoIni2.Text;
                txtSdoIni3.Text = (string.IsNullOrEmpty(txtSdoIni3.Text)) ? "0" : txtSdoIni3.Text;

                txtSaldoFinal1.Text = (string.IsNullOrEmpty(txtSaldoFinal1.Text)) ? "0" : txtSaldoFinal1.Text;
                txtSaldoFin2.Text = (string.IsNullOrEmpty(txtSaldoFin2.Text)) ? "0" : txtSaldoFin2.Text;
                txtSaldoFin3.Text = (string.IsNullOrEmpty(txtSaldoFin3.Text)) ? "0" : txtSaldoFin3.Text;
                          
                _SolPeru.MontoRetDia1 = Convert.ToDecimal(txtMontoRet1.Text);
                _SolPeru.MontoRetDia2 = Convert.ToDecimal(txtMontoRet2.Text);
                _SolPeru.MontoRetDia3 = Convert.ToDecimal(txtMontoRet3.Text);
                _SolPeru.SaldoIniCta = Convert.ToDecimal(txtSaldoAnt.Text);
                _SolPeru.SaldoFinCta = Convert.ToDecimal(txtSaldoFinal1.Text);
                _SolPeru.MonDepDia1 = Convert.ToDecimal(txtMontoDep1.Text);
                _SolPeru.MonDepDia2 = Convert.ToDecimal(txtMontoDep2.Text);
                _SolPeru.MonDepDia3 = Convert.ToDecimal(txtMontoDep3.Text);
                _SolPeru.SaldoIniCta2 = Convert.ToDecimal(txtSdoIni2.Text);
                _SolPeru.SaldoFinCta2 = Convert.ToDecimal(txtSaldoFin2.Text);
                _SolPeru.SaldoIniCta3 = Convert.ToDecimal(txtSdoIni3.Text);
                _SolPeru.SaldoFinCta3 = Convert.ToDecimal(txtSaldoFin3.Text);
                _SolPeru.TipoCredito = Convert.ToInt32(hdnIdTipoCredito.Value);//Convert.ToInt32(ddlTipoCredito.SelectedValue);

                int idSolicitudActiva = 0;
                using (wsSOPF.ClientesClient ws = new ClientesClient())
                {
                    ResultadoOfint resultado = ws.ObtenerSolicitudActivaDeCliente(Convert.ToInt32(ViewState["IDCliente"]));
                    idSolicitudActiva = resultado.ResultObject;
                }

                DataResultSolicitudes Resultado = new DataResultSolicitudes();
                using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
                {
                    int intAccion = 1;
                    if (txtIDSolicitud.Text.Trim() != "")
                    {
                        intAccion = 3;
                        _SolPeru.IDSolictud = Convert.ToInt32(txtIDSolicitud.Text.Trim());
                    }

                    Resultado = wsSolicitud.InsertarSolicitud_Peru(intAccion, Convert.ToInt32(Session["UsuarioId"]), _SolPeru);

                    // Validar errores de DB
                    if (Resultado.idMensaje == 0)
                        throw new Exception(Resultado.Mensaje);

                    // Si es Edicion (Condicionada)
                    if (this.isEdicion)
                    {
                        Response.Redirect("frmEditarSolicitud.aspx?idcliente=" + _SolPeru.IDCliente + "&idsolicitud=" + Resultado.Mensaje);
                    }                        
                    else
                    {
                        Response.Redirect("frmDocumentos.aspx?idsolicitud=" + Resultado.Mensaje);
                    }                        
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                pnlError.Visible = true;
            }
        }

        upValDatosCredito.Update();        
    }

	protected void ddlSeguroDesgravamen_SelectedIndexChange(object sender, EventArgs e)
	{			
        txtSeguroDesgravamen.Text = ddlSeguroDesgravamen.SelectedValue;
        btnCalcular_Click(sender, e);
    }

    protected void ddlProducto_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Obtener los datos del producto seleccionado.
        using (wsSOPF.CatalogoClient ws = new CatalogoClient())
        {
            if (ddlProducto.SelectedValue != string.Empty)
            {
                TBCATProducto ProductoSeleccionado = ws.ObtenerTBCATProducto(1, int.Parse(ddlProducto.SelectedValue), string.Empty, 1, 0, 0).ToList().FirstOrDefault();

                // Si el PRODUCTO es CONVENIO
                if (ddlConvenio.SelectedItem.Text.Contains("CONVENIO"))
                    pnlEdoCuenta.Visible = false;
                else
                    pnlEdoCuenta.Visible = true;

                txtPlazo.Text = ProductoSeleccionado.Pagos.ToString();
                txtTasaInteres.Text = decimal.Round(ProductoSeleccionado.Tasa, 2, MidpointRounding.AwayFromZero).ToString();
                btnCalcular_Click(sender, e);
                upDatosCredito.Update();
            }
        }        
    }

	protected void ddlBanco_SelectedIndexChanged(object sender, EventArgs e)
	{
        // Limpiar los controles a menos que el sender sea la Pagina(Load).
        if (((Control)sender).ID != "__Page")
        {
            txtCuentaDesembolso.Text = string.Empty;
            txtCuentaCCI.Text = string.Empty;
            txtCuentaCVV.Text = string.Empty;

            txtBINoOficina.Text = string.Empty;
            txtBICuenta.Text = string.Empty;

            txtBCEntidad.Text = string.Empty;
            txtBCOficina.Text = string.Empty;
            txtBCDC.Text = string.Empty;
            txtBCCuenta.Text = string.Empty;
        }

        if (ddlBanco.SelectedValue == "CONTINENTAL" || ddlBanco.SelectedValue == "INTERBANK")
		{
			txtBanco.Text = ddlBanco.SelectedValue;
			txtBanco.Enabled = false;
            txtCuentaDesembolso.Enabled = false;
            txtMontoUsoCanal.Text = "0.00";
		}	
		else
		{
			//txtBanco.Text = (((Control)sender).ID != "__Page") ? "": txtBanco.Text;
            txtBanco.Text = ddlBanco.SelectedValue;
            txtBanco.Enabled = false;
            txtCuentaDesembolso.Enabled = true;
            txtMontoUsoCanal.Text = "3.00";
		}

        if (ddlBanco.SelectedValue == "CONTINENTAL")
        {
            pnlCuentaContinental.Visible = true;            
            pnlCuentaInterbank.Visible = false;
            pnlCuentaOtroBanco.Visible = false;
        }
        else if (ddlBanco.SelectedValue == "INTERBANK")
        {
            pnlCuentaContinental.Visible = false;            
            pnlCuentaInterbank.Visible = true;
            pnlCuentaOtroBanco.Visible = false;
        }
        else
        {
            pnlCuentaContinental.Visible = false;
            pnlCuentaInterbank.Visible = false;
            pnlCuentaOtroBanco.Visible = true;

            txtBCEntidad.Text = string.Empty;
            txtBCOficina.Text = string.Empty;
            txtBCDC.Text = string.Empty;
            txtBCCuenta.Text = string.Empty;

            txtBINoOficina.Text = string.Empty;
            txtBICuenta.Text = string.Empty;
        }

        btnCalcular_Click(sender, e);
    }

    protected void ddlBanco_Emergente_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Limpiar los controles a menos que el sender sea la Pagina(Load).
        if (((Control)sender).ID != "__Page")
        {
            txtCuentaDesembolso_Emergente.Text = string.Empty;
            txtCuentaCCI_Emergente.Text = string.Empty;
            //txtCuentaCVV_Emergente.Text = string.Empty;

            txtBINoOficina_Emergente.Text = string.Empty;
            txtBICuenta_Emergente.Text = string.Empty;

            txtBCEntidad_Emergente.Text = string.Empty;
            txtBCOficina_Emergente.Text = string.Empty;
            txtBCDC_Emergente.Text = string.Empty;
            txtBCCuenta_Emergente.Text = string.Empty;
            txtNumTarjetaVisaEME.Text = string.Empty;
            txtAnioExpiraVisaEME.Text = string.Empty;
            txtCuentaCVVEME.Text = string.Empty;
            ddlMExpirationEME.SelectedIndex = 0;
        }

        if (ddlBanco_Emergente.SelectedValue == "CONTINENTAL" || ddlBanco_Emergente.SelectedValue == "INTERBANK")
        {
            txtBanco_Emergente.Text = ddlBanco_Emergente.SelectedValue;
            txtBanco_Emergente.Enabled = false;
            txtCuentaDesembolso_Emergente.Enabled = false;            
        }
        else
        {
            // txtBanco_Emergente.Text = (((Control)sender).ID != "__Page") ? "" : txtBanco_Emergente.Text;
            txtBanco_Emergente.Text = ddlBanco_Emergente.SelectedValue;
            txtBanco_Emergente.Enabled = false;
            txtCuentaDesembolso_Emergente.Enabled = true;            
        }

        if (ddlBanco_Emergente.SelectedValue == "CONTINENTAL")
        {
            pnlCuentaContinental_Emergente.Visible = true;
            pnlCuentaInterbank_Emergente.Visible = false;
            pnlCuentaOtroBanco_Emergente.Visible = false;
        }
        else if (ddlBanco_Emergente.SelectedValue == "INTERBANK")
        {
            pnlCuentaContinental_Emergente.Visible = false;
            pnlCuentaInterbank_Emergente.Visible = true;
            pnlCuentaOtroBanco_Emergente.Visible = false;
        }
        else
        {
            pnlCuentaContinental_Emergente.Visible = false;
            pnlCuentaInterbank_Emergente.Visible = false;
            pnlCuentaOtroBanco_Emergente.Visible = true;
           

            txtBCEntidad_Emergente.Text = string.Empty;
            txtBCOficina_Emergente.Text = string.Empty;
            txtBCDC_Emergente.Text = string.Empty;
            txtBCCuenta_Emergente.Text = string.Empty;

            txtBINoOficina_Emergente.Text = string.Empty;
            txtBICuenta_Emergente.Text = string.Empty;
        }        
    }

    protected void btnCalcular_Click(object sender, EventArgs e)
	{
		try
		{
            // Inicializar el campo Monto Liquidacion ante cualquier Cambio		
            txtMontoDesembolso.Text = string.Empty;

            string strImporte = txtImporteCredito.Text.Trim();
			string strPlazo = txtPlazo.Text.Trim();
			string strPrimerVencimiento = txtPrimerVencimiento.Text.Trim();
			
			if (string.IsNullOrEmpty(strImporte))
                throw new Exception();
			
			if (string.IsNullOrEmpty(strPlazo))			
                throw new Exception();
            
			if (string.IsNullOrEmpty(strPrimerVencimiento))			
                throw new Exception();            
			
			double dblErogacion;
            double decIGV = Convert.ToDouble(0.18);
            double decTasaInteres = Convert.ToDouble(txtTasaInteres.Text.Trim()) / 100;
            double decImporte = Convert.ToDouble(strImporte);
            double decPlazo = Convert.ToDouble(strPlazo);
            double decSeguro = Convert.ToDouble(txtSeguroDesgravamen.Text);

			decImporte = (decImporte + Convert.ToDouble(txtMontoUsoCanal.Text));

			dblErogacion = Convert.ToDouble(((1 + decIGV) * (decTasaInteres / 12)) * decImporte);
			dblErogacion = dblErogacion / (1 - (Math.Pow(Convert.ToDouble(1 + ((1 + decIGV) * (decTasaInteres) / 12)), Convert.ToDouble(-1 * decPlazo))));
            //dblErogacion = Math.Round(Convert.ToDouble(dblErogacion), 2);

			txtMontoTotalFinanciar.Text = decImporte.ToString();

            dblErogacion += (((decImporte * decSeguro) / 1000) * (1 + decIGV));
            dblErogacion += (Convert.ToDouble(txtMontoGAT.Text)) * (1 + decIGV);
			txtMontoCuota.Text = Math.Round(dblErogacion, 2).ToString();

			txtCostoTotalCred.Text = Math.Round(Math.Round(dblErogacion, 2) * decPlazo, 2).ToString();

            // Calcular el monto de desembolso para los casos en que sea una Ampliacion
            if (!string.IsNullOrEmpty(txtMontoLiquidacion.Text.Trim()))
            {
                txtMontoDesembolso.Text = Math.Round((decimal.Parse(txtImporteCredito.Text.Trim()) - decimal.Parse(txtMontoLiquidacion.Text.Trim())), 2).ToString();
            }
		}
		catch(Exception exc)
		{
            txtMontoTotalFinanciar.Text = string.Empty;
            txtMontoCuota.Text = string.Empty;
            txtCostoTotalCred.Text = string.Empty;
        }		
	}
       
    protected void txtCalcular_Click(object sender, EventArgs e)
    {
        btnCalcular_Click(sender, e);
    }

    protected void chkDomiciliacionVisa_CheckedChanged(object sender, EventArgs e)
    {
        // Permitir la animacion del Switch.
        System.Threading.Thread.Sleep(400);

        if (chkDomiciliacionVisa.Checked)
        {
            lblDesembolsoDomiciliacion.Text = "Desembolso";
            txtNumTarjetaVisa.Enabled = true;
            txtCuentaCVV.Enabled = true;
            ddlMesExpiraVisa.Enabled = true;
            txtAnioExpiraVisa.Enabled = true;

            rfvTxtNumTarjetaVisa.Enabled = true;
            rfvTxtAnioExpiraVisa.Enabled = true;
            rfvtxtCuentaCVV.Enabled = true;
        }
        else
        {
            lblDesembolsoDomiciliacion.Text = "Desembolso/Domiciliacion";
            txtNumTarjetaVisa.Enabled = false;
            txtCuentaCVV.Enabled = false;
            ddlMesExpiraVisa.Enabled = false;
            txtAnioExpiraVisa.Enabled = false;

            txtNumTarjetaVisa.Text = string.Empty;
            ddlMesExpiraVisa.SelectedIndex = 0;
            txtAnioExpiraVisa.Text = string.Empty;
            txtCuentaCVV.Text = string.Empty;

            rfvTxtNumTarjetaVisa.Enabled = false;
            rfvTxtAnioExpiraVisa.Enabled = false;
            rfvtxtCuentaCVV.Enabled = false;
        }
    }

    protected void chkCuentaDomiciliacionEmergente_CheckedChanged(object sender, EventArgs e)
    {
        // Permitir la animacion del Switch.
        System.Threading.Thread.Sleep(400);
        pnlCuentaEmergente.Visible = chkCuentaDomiciliacionEmergente.Checked;  
        PanelSalto.Visible= chkCuentaDomiciliacionEmergente.Checked;

        if (!chkCuentaDomiciliacionEmergente.Checked)
        {
            ddlBanco_Emergente.SelectedValue = "CONTINENTAL";
            ddlBanco_Emergente_SelectedIndexChanged(sender, e);            
        }
    }

    protected void txtNumTarjetaVisa_TextChanged(object sender, EventArgs e)
    {
        cvBINTarjetaVISA.Visible = false;
    }
    protected void txtNumTarjetaVisaEME_TextChanged(object sender, EventArgs e)
    {
        cvBINTarjetaVISAEME.Visible = false;
    }
    #region "Metodos"
    private void consultarLiquidacion(int idSolicitud, DateTime fecha)
    {
        using (wsSOPF.CreditoClient wsC = new CreditoClient())
        {
            wsSOPF.TBCreditos.Liquidacion liq = new TBCreditos.Liquidacion();
            liq.IdSolicitud = idSolicitud;
            liq.FechaLiquidar = new UtilsDateTimeR();

            wsSOPF.TBCreditos.Liquidacion liquidacionActiva = wsC.ObtenerLiquidacionActiva(liq);

            if (liquidacionActiva != null)
            {
                liq.FechaLiquidar = liquidacionActiva.FechaLiquidar;
            }

            // Calcular la Liquidacion
            wsSOPF.TBCreditos.CalculoLiquidacion resCalculoLiquidacion = wsC.CalcularLiquidacion(liq).ResultObject;
            mostrarLiquidacionParaAmplicacion(idSolicitud, resCalculoLiquidacion.TotalLiquidacion);
        }
    }

    private void mostrarLiquidacionParaAmplicacion(int idSolicitud, decimal monto, DateTime? fechaLiquidacion = null) {
        pnlMontosLiquidacion.Visible = true;
        txtMontoLiquidacion.Text = monto.ToString();
        txtMontoLiquidacion.Enabled = false;     

        string strFechaLiquidacion = "";
        if (fechaLiquidacion != null)
        {
            strFechaLiquidacion = "&fechaLiquidacion=" + ((DateTime)fechaLiquidacion).ToString("yyyy-MM-dd");
        }

        lnkLiquidacion.NavigateUrl = "../creditos/lstDashboardCreditos.aspx?idSolicitud=" + idSolicitud + strFechaLiquidacion;
    }

    public bool IsValidGroup(string Group)
    {
        bool obReturn = true;
        foreach (IValidator v in Page.GetValidators(Group))
        {
            if (!v.IsValid)
            {
                obReturn = false;
            }
        }
        return obReturn;
    }

    public bool esValidoBINTarjetaVISA(string NumeroTarjeta)
    {
        bool objReturn = true;

        using (wsSOPF.SolicitudClient ws = new SolicitudClient())
        {
            ResultadoOfboolean res = ws.ValidaTarjetaVISADomiciliacion(NumeroTarjeta);

            if (res.Codigo > 0)
            {
                objReturn = false;
                cvBINTarjetaVISA.Text = res.Mensaje;
            }
        }
        return objReturn;
    }
    #endregion       

    protected void ddlConvenio_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Cargar Combo Plazo
        using (wsSOPF.CatalogoClient ws = new CatalogoClient())
        {
            if (ddlConvenio.SelectedValue != string.Empty)
            {
                List<TBCATProducto> Plazos = ws.ObtenerTBCATProducto(5, int.Parse(ddlConvenio.SelectedValue), string.Empty, 1, 0, int.Parse(hdnIdTipoCredito.Value)).ToList();

                ddlProducto.DataSource = Plazos;
                ddlProducto.DataBind();
            }
        }

        ddlProducto_SelectedIndexChanged(sender, e);
    }
}