﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="frmTipoCredito.aspx.cs" Inherits="Solicitudes_frmTipoCredito" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    
        <div class="center">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Seleccione el Tipo de Credito</h3>
                </div>
                <div class="panel-body">
                    <%--<asp:RadioButtonList ID="rdTipoCredito" runat="server" OnSelectedIndexChanged="rdTipoCredito_SelectedIndexChange"
                        AutoPostBack="true">
                        
                        <asp:ListItem Value="3">Renovación</asp:ListItem>
                        <asp:ListItem Value="4">Renovación VIP</asp:ListItem>
                        
                    </asp:RadioButtonList>--%>

                    <label for="cboTipoCredito" class="col-sm-2 form-control-label">
                                Tipo de Crédito</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboTipoCredito" runat="server" CssClass="form-control">
                                </asp:DropDownList>

                            </div>

                </div>
                <div class="panel-footer">
                    <asp:Button ID="btnSiguienteTipo" runat="server" Text="Siguiente" CssClass="btn-primary"
                        OnClick="btnSiguienteTipo_Click" />
                </div>
            </div>
            <br />
            <br />

</asp:Content>

