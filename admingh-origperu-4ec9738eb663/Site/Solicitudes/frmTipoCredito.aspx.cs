﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Solicitudes_frmTipoCredito : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int idcliente = 0;
            int idsolicitud = 0;
            int idFolio = 0;

            idcliente = Convert.ToInt32(Request.QueryString["idCliente"]);
            idsolicitud = Convert.ToInt32(Request.QueryString["idsolicitud"]);
            idFolio = Convert.ToInt32(Request.QueryString["idFolio"]);

            TBCATTipoCredito[] TipoCredito = null;

            using (wsSOPF.CatalogoClient wsCatlogo = new CatalogoClient())
            {
                TipoCredito = wsCatlogo.ObtenerTBCatTipoCredito(idcliente);
                if (TipoCredito.Length > 0)
                {
                    cboTipoCredito.DataValueField = "idTipoCredito";
                    cboTipoCredito.DataTextField = "TipoCredito";
                    cboTipoCredito.DataSource = TipoCredito;
                    cboTipoCredito.DataBind();

                    if (idFolio > 0)
                    {
                        cboTipoCredito.SelectedValue = "3";
                        cboTipoCredito.Enabled = false;
                    }
                }
            }
        }     
    }

    protected void rdTipoCredito_SelectedIndexChange(object sender, EventArgs e)
    {
        
    }

    protected void btnSiguienteTipo_Click(object sender, EventArgs e)
    {
        switch (cboTipoCredito.SelectedValue)
        {
            case "1":
                this.Response.Redirect("frmDatosSolicitud.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&TipoCredito=1" + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString());
                break;
            case "2":
                this.Response.Redirect("frmDatosSolicitud.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&TipoCredito=2" + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString());
                break;
            case "3":
                this.Response.Redirect("frmReniovacion.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&TipoCredito=3" + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString() + "&idFolio=" + Request.QueryString["idFolio"].ToString());
                break;
            case "4":
                this.Response.Redirect("frmDatosSolicitud.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&TipoCredito=4" + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString());
                break;
            case "5":
                this.Response.Redirect("frmReniovacion.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&TipoCredito=5" + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString());
                break;
            case "6":
                this.Response.Redirect("frmReniovacion.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&TipoCredito=5" + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString());
                break;
            case "7":
                this.Response.Redirect("frmReniovacion.aspx?idcliente=" + Request.QueryString["idCliente"].ToString() + "&TipoCredito=5" + "&idsolicitud=" + Request.QueryString["idsolicitud"].ToString());
                break;
        }
        
    }
}