﻿var prm = Sys.WebForms.PageRequestManager.getInstance();

prm.add_endRequest(function () {
    // Activar Tooltips
    activarTooltips();
});

function acoplarValoresEnviarCodigo() {
    var hCod = document.getElementById('ctl00_MainContent_hfCodigo');
    hCod.value = document.getElementById('ctl00_MainContent_txCodigo').value;
    var hFechaLimitePago = document.getElementById('ctl00_MainContent_hfFechaLimitePago');
    hFechaLimitePago.value = document.getElementById('ctl00_MainContent_txFechaLimitePago').value;
}

function acoplarValoresConfirmarRecaudo() {
    var hCom = document.getElementById('ctl00_MainContent_hfComentario');
    hCom.value = document.getElementById('ctl00_MainContent_txComentario').value;
}