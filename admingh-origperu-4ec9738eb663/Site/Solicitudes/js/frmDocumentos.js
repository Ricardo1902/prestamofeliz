﻿var documentos = [];
var extSoporte = [".jpg", "jpeg", ".png", ".pdf"];
var archivosCargados = [];
const estado = { nuevo: 0, cargando: 1, cargado: 2, error: 3 };
$(document).on({
    ajaxStart: function () {
    },
    ajaxSend: function (event, jqxhr, settings) {
        if (settings.url.indexOf("frmDocumentos.aspx/CargarArchivo") === -1) {
            $("body").addClass("loading");
            $(".modal-pr").appendTo("body");
        } else {
            $("body").removeClass("loading");
        }
    },
    ajaxStop: function () { $("body").removeClass("loading"); }
});
$(document).ready(() => {
    datosCliente();
    cargarDocumentos();
    $("#modCargaDoc").on("hide.bs.modal", () => {
        $("#modCargaDoc").removeData('idDocumento');
        $("#modCargaDoc #modCarga_documentos").empty();
        $("#modCargaDoc #modCarga_vistaPrevia").empty();
    });
});
function cargarDocumentos() {
    let dDocumentos = $("#documentos");
    if (archivosCargados == null) archivosCargados = [];
    //dDocumentos.empty();
    $("#docSeleccion").prop('accept', `${extSoporte.map(o => { return o }).join(',')}`);
    if (documentos && documentos.length > 0) {
        documentos.forEach(d => {
            let documento = $("<div>", { class: 'row documento' });
            let col9 = $("<div>", { class: 'col-md-9 col-sm-12' });
            let formg = $("<div>", { class: 'form-group' });
            formg.append($("<label>", { text: d.Documento }));
            if (d.Condicionado) {
                formg.append($("<span>", { class: 'condicionado-motivo', id: `doc-con_${d.idDocumento}`, html: d.MotivoCondicionado }));
            }
            col9.append(formg);

            let col3 = $("<div>", { id: `docAccion_${d.idDocumento}`, class: 'col-md-3 col-sm-12 documento-acciones' });
            col3.append($("<span>", { class: 'btn btn-sm btn-info btn-span inactivo', onclick: `visualizarDocumentos(${d.idDocumento});` })
                .append($("<span>", { class: 'glyphicon glyphicon-search' })));
            col3.append($("<button>", { type: 'button', id: `cargaDoc_${d.idDocumento}`, class: 'btn btn-primary btn-sm', onclick: `cargarDocumento(${d.idDocumento});` })
                .append($("<span>", { class: 'glyphicon glyphicon-upload' }))
                .append("&nbsp;Cargar archivo"));
            col3.append($("<span>", { class: 'btn btn-sm btn-span inactivo archivo-carga' })
                .append($("<span>", { class: 'glyphicon glyphicon-refresh' })));

            documento.append(col9);
            documento.append(col3);
            dDocumentos.append(documento[0].outerHTML);
        });

        if (archivosCargados !== null && archivosCargados.length > 0) {
            for (let i = 0; i < archivosCargados.length; i++) {
                if (archivosCargados[i].archivos != null && archivosCargados[i].archivos.length > 0) {
                    $(`#docAccion_${archivosCargados[i].idDocumento} > span`).map((i, d) => {
                        $(d).removeClass("inactivo");
                    });
                    for (let j = 0; j < archivosCargados[i].archivos.length; j++) {
                        archivosCargados[i].archivos[j].estado = archivosCargados[i].archivos[j].idDocumentoArchivo > 0 ? estado.cargado : estado.nuevo;
                    }
                }
            }
            validaTotalDocumentos();
            validaArchivosCargados();
        }
    }
}
function cargarDocumento(idDocumento) {
    $("#docSeleccion").removeData('idDocumento');
    if (idDocumento > 0) {
        $('#docSeleccion').data('idDocumento', idDocumento);
        $(`#docSeleccion`).click();
    }
}
function visualizarDocumentos(idDocumento) {
    $('#modCargaDoc').data('idDocumento', idDocumento);
    $("#modCargaDoc .modal-header .modal-title").html("");
    if (documentos !== null) {
        let docP = documentos.find(d => d.idDocumento === idDocumento);
        if (docP != null) {
            $("#modCargaDoc .modal-header .modal-title").html(`Carga de Archivos: ${docP.Documento}`);
        }
    }
    if (archivosCargados !== null) {
        let o = archivosCargados.find(a => a.idDocumento === idDocumento);
        if (typeof o !== 'undefined' && o !== null) {
            comprobarArchivosMedios(o)
                .then(() => {
                    mostrarArchivos(o);
                    $('#modCargaDoc')
                        .appendTo('body')
                        .css('z-index', 90000)
                        .modal('show');
                });
        }
    }
}
function onChangeDocSeleccion() {
    validaEstatus();
    obtenerContenidoArchivo().then(contenido => {
        if (contenido !== null) {
            controlArchivosCargar(contenido, true, (resultadoOperacion, mensajeOperacion) => {
                if (resultadoOperacion === true) {
                    contenido.archivo.estado = estado.cargando;
                    cargarArchivo(contenido);
                    $(`#docAccion_${contenido.idDocumento}`).find('span.inactivo').map((o, d) => { $(d).removeClass('inactivo') });
                    let idDocumento = parseInt($('#modCargaDoc').data('idDocumento'));
                    if (!isNaN(idDocumento) && idDocumento > 0) {
                        let tipoDoc = encontrarTipoDocumento(idDocumento);
                        if (tipoDoc !== null) { mostrarArchivos(tipoDoc, true); }
                    }
                } else {
                    alert(mensajeOperacion);
                }
            });
        } else {
            alert("No fue posible realizar la carga de archivo.");
        }
    });
}
function obtenerContenidoArchivo() {
    return new Promise((resolve, reject) => {
        var file = $("#docSeleccion")[0].files[0];
        let idDocumento = $('#docSeleccion').data('idDocumento');
        $("#docSeleccion").removeData('idDocumento');
        if (file) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (e) {
                let archivo = {
                    idDocumento,
                    archivo: { id: Date.now(), nombreArchivo: file.name, contenido: e.target.result.substr(e.target.result.indexOf(',') + 1), tipo: file.type, fOrig: file, estado: estado.nuevo }
                };
                $("#docSeleccion").val("");
                resolve(archivo);
            }
            reader.onerror = function (e) {
                $("#docSeleccion").val("");
                resolve(null);
            }
        }
    });
}
function controlArchivosCargar(archivo, cargar, callback) {
    let posicion = -1;
    let resultadoOperacion = false;
    let mensajeOperacion = '';
    if (archivosCargados === undefined || archivosCargados === null) archivosCargados = [];
    archivosCargados.map((o, i) => {
        if (o.idDocumento === archivo.idDocumento) {
            posicion = i;
            return;
        }
    });

    if (cargar && posicion < 0) {
        archivosCargados.push({ idDocumento: archivo.idDocumento, archivos: [archivo.archivo] });
        resultadoOperacion = true;
    }
    if (cargar && posicion >= 0) {
        let posArchivo = -1;
        if (archivosCargados[posicion].archivos && archivosCargados[posicion].archivos !== null) {
            posArchivo = archivosCargados[posicion].archivos.findIndex(a => a.nombreArchivo === archivo.archivo.nombreArchivo);
        }
        if (cargar && posArchivo < 0) {
            archivosCargados[posicion].archivos.push(archivo.archivo);
            resultadoOperacion = true;
        } else {
            resultadoOperacion = false;
            mensajeOperacion = "Ya existe un archivo con el mismo nombre cargado.";
        }
    }
    else if (!cargar && archivosCargados.length > 0 && posicion >= 0) {
        archivosCargados.splice(posicion, 1);
        resultadoOperacion = true;
    }
    validaTotalDocumentos();
    if (resultadoOperacion === true) mensajeOperacion = cargar ? "Archivo cargado" : "Archivos eliminado";
    if (typeof callback !== 'undefined') callback(resultadoOperacion, mensajeOperacion);
}
function obtenerExtension(nombreArchivo) {
    let ultimaPos = file.name.lastIndexOf('.');
    let extension = file.name.substring(ultimaPos);
    return extension;
}
function mostrarArchivos(archivos, agregar = false) {
    if (agregar === typeof "undefined") agregar = false;
    if (!agregar) {
        $("#modCargaDoc #modCarga_documentos").empty();
        $("#modCargaDoc #modCarga_vistaPrevia").empty();
    }
    if (typeof archivos !== 'undefined' && archivos !== null) {
        if (typeof archivos.archivos !== 'undefined' && archivos.archivos !== null && archivos.archivos.length > 0) {
            let ultimo = archivos.archivos.length - 1;
            archivos.archivos.forEach((a, i) => {
                let render = true;
                if (agregar && i !== ultimo) render = false;
                if (render) {
                    let doc = $("<div>", { class: "doc-prev", onclick: `mostrarArchivoCompleto(${i}, event)` });
                    if (["image/png", "image/jpeg"].includes(a.tipo)) {
                        doc.append($("<img>", { src: `data:${a.tipo};base64,${a.contenido}`, title: a.nombreArchivo }));
                    } else if ("application/pdf" === a.tipo) {
                        doc.append($("<img>", { src: `../../Imagenes/Gestiones/pdf.ico`, title: a.nombreArchivo }));
                    }
                    $("#modCargaDoc #modCarga_documentos").append(doc[0].outerHTML);
                }
            });
        }
        if ($("#btnGenerar").length > 0) {
            if (agregar) $("#modCargaDoc #modCarga_documentos .doc-agregar").remove();
            $("#modCargaDoc #modCarga_documentos").append($("<div>", { class: "doc-agregar", onclick: `cargarDocumento(${archivos.idDocumento})` })
                .append($("<span>", { class: "glyphicon glyphicon-plus", value: "+" })));
        }
    }
}
function mostrarArchivoCompleto(posicion, e) {
    validaEstatus();
    docPrevSel(e.target.parentElement);
    $("#modCargaDoc #modCarga_vistaPrevia").empty();
    let idDocumento = parseInt($('#modCargaDoc').data('idDocumento'));
    let o = archivosCargados.find(a => a.idDocumento === idDocumento);
    if (typeof o !== 'undefined' && o !== null) {
        if (typeof o.archivos !== 'undefined' && o.archivos !== null && posicion >= 0 && posicion < o.archivos.length) {
            let doc = $("<div>", { class: "doc-prev-completo" });
            let a = o.archivos[posicion];
            $("#modCargaDoc #modCarga_vistaPrevia")
                .append($("<div>", { class: 'doc-prev-titulo alert alert-info', text: a.nombreArchivo })
                    .append($("<span>", { class: "glyphicon glyphicon-trash", onclick: `onRemoverClick(${posicion})` })));
            if (["image/png", "image/jpeg"].includes(a.tipo)) {
                doc.css("overflow", "auto");
                doc.append($("<img>", { src: `data:${a.tipo};base64,${a.contenido}`, title: a.nombreArchivo }));
            }
            else if ("application/pdf" === a.tipo) {
                let src = null;
                if (a.fOrig != null) {
                    src = URL.createObjectURL(a.fOrig);
                }
                else if (a.contenido != null && a.contenido.length > 0) {
                    src = URL.createObjectURL(b64Blob(a.contenido, a.tipo));
                }
                doc.append($("<embed>", { src: src, id: `archivo_doc_${idDocumento}_${posicion}`, class: 'atrchivo-doc-pdf', width: "100%" }));
            }

            $("#modCargaDoc #modCarga_vistaPrevia").append(doc[0].outerHTML);
        }
    }
}
function validaEstatus() {
    let parametros = {
        url: "frmDocumentos.aspx/ValidaEstatus",
        contentType: "application/json",
        dataType: "json",
        method: 'GET'
    };
    $.ajax(parametros)
        .done(function (data) {
            let resultado = data.d;
            if (resultado && resultado.url !== null && resultado.url.length > 0) {
                alert("La sesión se ha terminado. Vuelva a iniciar por favor.");
                window.location = resultado.url;
            }
        })
        .fail(function (err) {
        });
}
function onRemoverClick(posicion) {
    validaEstatus();
    let idDocumento = parseInt($('#modCargaDoc').data('idDocumento'));
    if (idDocumento > 0 && posicion >= 0) {
        if (archivosCargados !== null) {
            for (let i = 0; i < archivosCargados.length; i++) {
                if (archivosCargados[i].idDocumento === idDocumento && archivosCargados[i].archivo !== null) {
                    for (let j = 0; j < archivosCargados[i].archivos.length; j++) {
                        if (j === posicion) {
                            let parametros = {
                                url: "frmDocumentos.aspx/RemoverArchivo" + window.location.search,
                                method: 'POST',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({ idDocumentoArchivo: archivosCargados[i].archivos[j].idDocumentoArchivo })
                            };

                            $.ajax(parametros)
                                .done(function (data) {
                                    let resultado = data.d;
                                    if (resultado !== null) {
                                        if (resultado && resultado.url !== null && resultado.url.length > 0) {
                                            alert("La sesión se ha terminado. Vuelva a iniciar por favor.");
                                            window.location = resultado.url;
                                        }
                                        else {
                                            alert(resultado.mensaje);
                                            if (!resultado.error) {
                                                archivosCargados[i].archivos.splice(j, 1);
                                                mostrarArchivos(archivosCargados[i]);
                                                validaArchivosCargados();
                                            } else {
                                                alert((resultado.mensaje && resultado.mensaje.length > 0) ? resultado.mensaje : "La acción se realizó parcialmente, revise se hayan guardado todos los cambios.");
                                            }
                                        }
                                    } else {
                                        alert("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.");
                                    }
                                })
                                .fail(function (error) {
                                    alert("Ocurrio un error al intentar eliminar el documento");
                                });
                            return;
                        }
                    }
                }
            }
        }
    }
}
function onGenerarDocumentacion() {
    $('#cargaDocs .alert').alert('close')
    if (archivosCargados === null || archivosCargados.length === 0) {
        mensaje("Proporcione al menos un documento para generar la documentación", 'alert-danger');
        return;
    }

    $("#btnGenerar").prop("disabled", true);
    let totalArchivos = 0;
    for (let i = 0; i < archivosCargados.length; i++) {
        if (archivosCargados[i].archivos !== null && archivosCargados[i].archivos.length > 0)
            totalArchivos += archivosCargados[i].archivos.length;
    }

    let parametros = {
        url: "frmDocumentos.aspx/GenerarDocumentacion" + window.location.search,
        method: 'POST',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({ totalArchivos: totalArchivos })
    };

    $.ajax(parametros)
        .done(function (data) {
            let resultado = data.d;
            if (resultado !== null) {
                if (resultado && resultado.url !== null && resultado.url.length > 0) {
                    alert("La sesión se ha terminado. Vuelva a iniciar por favor.");
                    window.location = resultado.url;
                }
                else {
                    alert(resultado.mensaje);
                    if (!resultado.error) {
                        mensaje((resultado.mensaje && resultado.mensaje.length > 0) ? resultado.mensaje : "Los documentos fueron generados correctamente.", 'alert-success');
                        $("#btnGenerar").remove();
                        $(".documento .documento-acciones button").remove();
                        let qs = new URLSearchParams(window.location.search);
                        let solicitud = qs.has("idsolicitud") ? qs.get("idsolicitud") : "0";
                        $("#cargaDocs").append($("<input>", {
                            type: "button", value: "Ver Expediente", class: "btn btn-primary",
                            onclick: `window.open('frmDocumentos.aspx?descargar=true&idSolicitud=${solicitud}','width=1000,height=500,resizable=1,scrollbars=yes');`
                        }));
                    } else {
                        mensaje((resultado.mensaje && resultado.mensaje.length > 0) ? resultado.mensaje : "La acción se realizó parcialmente, revise se hayan guardado todos los cambios.", 'alert-warning');
                    }
                }
            } else {
                mensaje("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.", 'alert-warning');
            }
        })
        .fail(function (error) {
            mensaje("Ocurrio un error al intentar generar la documentación.", 'alert-danger');
        });
}
function datosCliente() {
    if (cliente && cliente !== null) {
        $("#datCliente").html(cliente.Cliente);
        $("#datCliente-celular").html(cliente.Celular);
        $("#datCliente-email").html(cliente.Email);
    }
}
function mensaje(mensaje, tipo) {
    let domId = Date.now();
    $("#cargaDocs").append($("<div>", { id: `${domId}`, class: 'alert alert-dismissible fade in', role: 'alert' })
        .append($("<button>", { type: 'button', class: 'close', "data-dismiss": "alert", "aria-label": "close" })
            .append($("<span>", { "aria-hidden": true, html: "&times;" })))
        .append(mensaje));
    if (tipo && tipo !== null) $(`#${domId}`).addClass(tipo);
    else $(`#${domId}`).addClass('alert-success');
    $(`#${domId}`).alert();
}
function validaTotalDocumentos() {
    if (archivosCargados && archivosCargados !== null && archivosCargados.length > 0) {
        $("#btnGenerar").prop('disabled', false);
    } else {
        $("#btnGenerar").prop('disabled', true);
    }
}
function docPrevSel(dom) {
    $("#modCarga_documentos .doc-prev").removeClass("doc-prev-activo");
    if (dom != null) {
        $(dom).addClass("doc-prev-activo");
    }
}
function validaArchivosCargados() {
    if (archivosCargados !== null && archivosCargados.length > 0) {
        for (let i = 0; i < archivosCargados.length; i++) {
            if (archivosCargados[i].archivos === null || archivosCargados[i].archivos.length === 0) {
                $(`#docAccion_${archivosCargados[i].idDocumento} > span`).map((i, d) => {
                    $(d).addClass("inactivo");
                });
                controlArchivosCargar({ idDocumento: archivosCargados[i].idDocumento }, false);
            }
            else {
                let docAccion = $(`#docAccion_${archivosCargados[i].idDocumento}`).find('span.archivo-carga')[0];
                if (typeof docAccion !== "undefined" && docAccion !== null) {
                    $(docAccion).removeClass("correcto").removeClass("error");
                    let error = false;
                    let totalEnCurso = 0;
                    for (let j = 0; j < archivosCargados[i].archivos.length; j++) {
                        if (archivosCargados[i].archivos[j].estado === estado.error) {
                            error = true;
                            break;
                        } else if (archivosCargados[i].archivos[j].estado === estado.cargando) {
                            totalEnCurso++;
                            break;
                        }
                    }

                    $(docAccion).find("span.glyphicon").map((i, d) => {
                        $(d).removeClass("glyphicon-refresh").removeClass("glyphicon-remove").removeClass("glyphicon-ok");
                        if (error) { $(d).addClass("glyphicon-remove"); $(docAccion).addClass("error"); }
                        else if (totalEnCurso === 0) { $(d).addClass("glyphicon-ok"); $(docAccion).addClass("correcto"); }
                        else $(d).addClass("glyphicon-refresh");
                    });
                }
            }
        }
    }
}
function encontrarTipoDocumento(idDocumento) {
    if (isNaN(idDocumento) || idDocumento <= 0 || archivosCargados === null || archivosCargados.length === 0) return null;
    for (let i = 0; i < archivosCargados.length; i++) {
        if (archivosCargados[i].idDocumento === idDocumento) return archivosCargados[i];
    }
    return null;
}
function cargarArchivo(documentoArchivo) {
    if (documentoArchivo !== null && documentoArchivo.archivo !== null) {
        let id = documentoArchivo.archivo.id;
        let parametros = {
            url: "frmDocumentos.aspx/CargarArchivo" + window.location.search,
            method: 'POST',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                archivo: {
                    idDocumento: documentoArchivo.idDocumento,
                    archivo: {
                        id: id,
                        contenido: documentoArchivo.archivo.contenido,
                        nombreArchivo: documentoArchivo.archivo.nombreArchivo,
                        tipo: documentoArchivo.archivo.tipo
                    }
                }
            })
        };

        $.ajax(parametros)
            .done(function (data) {
                let resultado = data.d;
                let documentoArchivos = encontrarTipoDocumento(documentoArchivo.idDocumento);
                let idDocumentoArchivo = 0;

                if (resultado !== null) {
                    if (resultado && resultado.url !== null && resultado.url.length > 0) {
                        alert("La sesión se ha terminado. Vuelva a iniciar por favor.");
                        window.location = resultado.url;
                    }
                    else {
                        if (!resultado.error && resultado.cargaResult !== null) {
                            idDocumentoArchivo = resultado.cargaResult.IdDocumentoArchivo;
                            id = resultado.cargaResult.Id;
                        } else {
                            mensaje(resulado.mensaje, 'alert-warning');
                        }
                    }
                } else {
                    mensaje("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.", 'alert-warning');
                }

                if (documentoArchivos !== null && documentoArchivos.archivos !== null) {
                    for (let i = 0; i < documentoArchivos.archivos.length; i++) {
                        if (documentoArchivos.archivos[i].id === id) {
                            documentoArchivos.archivos[i].idDocumentoArchivo = idDocumentoArchivo;
                            documentoArchivos.archivos[i].estado = idDocumentoArchivo > 0 ? estado.cargado : estado.error;
                        }
                    }
                }
                validaArchivosCargados();
            })
            .fail(function (error) {
                mensaje("Ocurrio un error al intentar enviar el archivo.", 'alert-danger');
            });
    }
}
function b64Blob(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);
        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
}
function comprobarArchivosMedios(o) {
    return new Promise((resolve, reject) => {
        if (o !== null && o.archivos !== null && o.archivos.length > 0) {
            let idsArchivosObtener = [];
            for (let i = 0; i < o.archivos.length; i++) {
                if (o.archivos[i].idDocumentoArchivo > 0 && (o.archivos[i].contenido === null || o.archivos[i].contenido.length === 0)) {
                    idsArchivosObtener.push(o.archivos[i].idDocumentoArchivo);
                }
            }
            if (idsArchivosObtener.length > 0) {
                let qs = new URLSearchParams(window.location.search);
                let idSolicitud = qs.has("idsolicitud") ? qs.get("idsolicitud") : 0;
                let parametros = {
                    url: "frmDocumentos.aspx/ObtenerArchivos",
                    contentType: "application/json",
                    dataType: "json",
                    method: 'GET',
                    data: { idSolicitud, idsDocumentoArchivos: JSON.stringify(idsArchivosObtener) }
                };
                $.ajax(parametros)
                    .done(function (data) {
                        let resultado = data.d;
                        if (resultado && resultado.url !== null && resultado.url.length > 0) {
                            alert("La sesión se ha terminado. Vuelva a iniciar por favor.");
                            window.location = resultado.url;
                        } else if (resultado.documentosArchivos !== null && resultado.documentosArchivos.length > 0) {
                            for (let i = 0; i < resultado.documentosArchivos.length; i++) {
                                if (resultado.documentosArchivos[i].IdDocumento === o.idDocumento) {
                                    if (resultado.documentosArchivos[i].Archivos !== null) {
                                        for (let j = 0; j < resultado.documentosArchivos[i].Archivos.length; j++) {
                                            for (let k = 0; k < o.archivos.length; k++) {
                                                if (resultado.documentosArchivos[i].Archivos[j].IdDocumentoArchivo === o.archivos[k].idDocumentoArchivo) {
                                                    o.archivos[k].contenido = resultado.documentosArchivos[i].Archivos[j].Contenido;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        resolve();
                    })
                    .fail(function (err) {
                        resolve();
                    });
            } else resolve();
        } else resolve();
    })
}