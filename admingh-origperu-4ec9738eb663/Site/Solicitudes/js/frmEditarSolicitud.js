﻿$(document).ready(() => {
    var proc = $('#lstProcesos');
    proc.empty();
    if (typeof procesoDigital != 'undefined') {
        procesoDigital.forEach((o) => {
            let proceso = $('<button>', { id: o.IdSolicitudProcesoDigital, type: 'button', class: 'list-group-item', onclick: `reiniciar(this.id);` });
            let icono = $('<span>', { class: 'glyphicon' });
            if (o.ProcesoActual) $(proceso).addClass("list-group-item-danger");
            if (o.Proceso) icono.addClass(o.Proceso);
            proceso.append(icono).append(`&nbsp;Reiniciar ${o.ProcesoDigital}`);
            proc.append(proceso);
        });
    }
    $('#btnReiniciar').click(reiniciarPro);
    $("#modReinicio").on("hide.bs.modal", () => {
        $(this).removeData('proceso')
        $('#modReinicio #txComentario').val('');
    });
});
function reiniciar(v) {
    if (v > 0) {
        $('#modReinicio').data('proceso', v);
        $('#modReinicio')
            .appendTo('body')
            .css('z-index', 2000)
            .modal('show');
    }
}
function reiniciarPro() {
    let idProceso = $('#modReinicio').data('proceso');
    $("#btnReiniciar").prop('disabled', true);
    let qs = new URLSearchParams(window.location.search);
    if (idProceso && idProceso > 0) {
        let peticion = {
            IdSolicitud: qs.has('idsolicitud') ? qs.get('idsolicitud') : 0,
            IdSolicitudProcesoDigital: idProceso,
            Comentario: $('#modReinicio #txComentario').val()
        }
        if (peticion.Comentario == '') { alert('Ingrese un comentario por favor.'); return; }
        let parametros = {
            url: `frmEditarSolicitud.aspx/ReiniciarProceso`,
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ peticion: peticion })
        }

        $.ajax(parametros)
            .done(function (data) {
                let resultado = data.d;
                if (resultado != null) {
                    if (resultado.url != null && resultado.url.length > 0) {
                        window.location = resultado.url;
                    } else {
                        alert(resultado.mensaje);
                    }
                } else {
                    alert("La acción se realizó parcialmente, revise se hayan guardado todos los cambios.")
                }
                window.location.reload();
            })
            .fail(function (error) {
                $("#btnReiniciar").prop('disabled', false);
                alert("Ocurrio un error al intentar realizar la petición.")
            });
    }
}