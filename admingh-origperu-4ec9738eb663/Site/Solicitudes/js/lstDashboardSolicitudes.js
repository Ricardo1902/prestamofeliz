﻿function procesoDigital(idSolicitud) {
    let body = $("#divProcesoDigital .modal-body");
    $(body).empty();
    $(body).append($("<p>", { text: "Cargando..." }));
    $("#divProcesoDigital").appendTo('body')
        .css("z-index", 9999)
        .modal({ keyboard: false });
    let parametros = {
        url: "lstDashboardSolicitudes.aspx/EstatusProcesoDigital",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { idSolicitud: idSolicitud }
    };
    $.ajax(parametros)
        .done(function (data) {
            resultadoGestion = [];
            if (data.d != null) {
                let procesos = data.d.procesoDigital;
                let body = $("#divProcesoDigital .modal-body");
                $(body).empty();
                if (procesos != null && procesos.length > 0) {
                    procesos.forEach(p => {
                        $(body).append($("<div>", { class: 'row' })
                            .append($("<span>", { text: p.Proceso, class: 'col-md-6 col-sm-6 procesoDigital' }))
                            .append($("<span>", { text: moment(p.FechaRegistro).format("DD/MM/YYYY HH:mm"), class: 'col-md-6 col-sm-6 fechaEvento' }))
                            .append($("<span>", { text: p.Accion, class: 'col-md-12 col-sm-12 accionEvento' })));
                    });
                } else {
                    $(body).append($("<p>", { text: "No se encontraron registros." }));
                }
            }
        })
        .fail(function (err) {
            console.log(err);
        });
}