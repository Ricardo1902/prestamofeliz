﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="lstDashboardSolicitudes.aspx.cs" Inherits="lstDashboardSolicitudes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
      <link href="../../js/datatables/1.10.19/jquery.dataTables.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.css"/>
    <script src="../../js/datatables/js/jquery-3.3.1.js"></script>
    <script src="../../js/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../../js/datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/datatables.min.js"></script>        
    <script src="../../js/bootstrap.min.js"></script>
     <script src="../../js/Solicitudes/Solicitud.js?v=<%=AppSettings.VersionPub %>"></script>


<asp:UpdateProgress AssociatedUpdatePanelID="upMain" DisplayAfter="200" runat="server">
    <ProgressTemplate>                 
        <div id="loader-background"></div>
        <div id="loader-content"></div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel ID="upMain" runat="server">
<ContentTemplate>
    <asp:Panel ID="pnlBuscarSolicitud" runat="server">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Seguimiento de Solicitudes</h3>
            </div>
            <div class="panel-body">
                <asp:Panel ID="pnlError" class="col-sm-12" Visible="false" runat="server">                                
                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px;">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </div>
                    <hr />
                </asp:Panel>
                <div class="form-group col-md-3">
                    <label class="small">Filtro Solicitudes:</label>
                    <asp:DropDownList ID="cmbPertenece" runat="server" CssClass="form-control" AutoPostBack="true"
                        OnSelectedIndexChanged="cmbPertenece_SelectedIndexChanged">
                        <asp:ListItem Text="Solo Mias" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Todas" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="form-group col-md-6">
                    <label class="small">Filtro Promotor:</label>
                    <asp:DropDownList ID="cmbPromotor" runat="server" CssClass="form-control" AutoPostBack="true"
                        OnSelectedIndexChanged="cmbPromotor_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>                    

                <div class="form-group col-md-3">
                    <label class="small">Filtro Sucursal:</label>
                    <asp:DropDownList ID="cmbSucursal" runat="server" CssClass="form-control" AutoPostBack="true"
                        OnSelectedIndexChanged="cmbSucursal_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>                    

                <div class="form-group col-md-3">
                    <label class="small">Estatus:</label>
                    <asp:DropDownList ID="cmbEstatus" runat="server" CssClass="form-control" AutoPostBack="true"
                        OnSelectedIndexChanged="cmbEstatus_SelectedIndexChanged">
                        <asp:ListItem Text="Todas" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Activa" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Cancelada" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Declinada" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Pendiente" Value="4"></asp:ListItem>
                        <asp:ListItem Text="Operada" Value="5"></asp:ListItem>
                        <asp:ListItem Text="Condicionada" Value="6"></asp:ListItem>
                        <asp:ListItem Text="Declina por Sistema" Value="7"></asp:ListItem>
                    </asp:DropDownList>
                </div>                    

                <div class="form-group col-md-6">
                    <label class="small">Búsqueda:</label>
                    <asp:TextBox ID="txtIdSolicitud" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="form-group col-sm-12">
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn btn-sm btn-primary " OnClick="btnBuscar_Click" />
                </div>

                <div class="form-group col-sm-12">
                    <div style="height: auto; width: auto; overflow-x: auto;">
                        <asp:GridView ID="gvSolicitudes" runat="server" Width="100%" AutoGenerateColumns="False"
                            Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                            CssClass="table table-bordered table-striped  table-hover table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                            OnPageIndexChanging="gvSolicitudes_PageIndexChanging" PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                            DataKeyNames="IdSolicitud,IdCliente" Style="overflow-x: auto;" OnRowCommand="gvSolicitudes_RowCommand" OnRowDataBound="gvSolicitudes_RowDataBound">
                            <PagerStyle CssClass="pagination-ty" />
                            <Columns>
                                 <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fab fa-superpowers fa-lg'></i>">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnVerCronograma" CommandName="VerCronograma" CommandArgument='<%# Eval("IdSolicitud") %>' CssClass="btn btn-xs" data-toggle="tooltip" title="Ver Cronograma" Visible="true" runat="server">
                                                <i class="far fa-calendar-alt fa-lg" style="color: #888888;"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lbtnSolPDF" CommandName="VerSol_PDF"   CommandArgument='<%# Eval("IdSolicitud") %>'  CssClass="btn btn-xs" data-toggle="tooltip" title="Imprimir Solicitud" Visible="true" runat="server">
                                                <i class="fas fa-print fa-lg" style="color: #888888;"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                </asp:TemplateField>   
                                <asp:BoundField HeaderText="Id Cliente" DataField="IdCliente" Visible="false" />
                                <asp:TemplateField HeaderText="Solicitud">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="btnDetalle" CommandName="Editar"><%#Eval("IdSolicitud") %></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Fecha Solicitud" DataField="FchSolicitud" DataFormatString="{0:dd/MM/yyyy hh:mm tt}" />
                                <asp:BoundField HeaderText="Cliente" DataField="Cliente" />
                                <asp:BoundField HeaderText="Promotor" DataField="Promotor" />
                                
                                <asp:BoundField HeaderText="Capital" DataField="capital" DataFormatString="{0:c2}" />
                                <asp:BoundField HeaderText="Estatus" DataField="Estatus" />
                                <asp:BoundField HeaderText="Fecha Estatus" DataField="" />
                                <asp:BoundField HeaderText="Asignado" DataField="Asignado" />
                                <asp:BoundField HeaderText="Observaciones" DataField="Observacion" />
                                <asp:TemplateField HeaderText="Comentarios">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="btnComentarios" CommandName="Comentarios"><%#Eval("Comentarios") %></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="TipoComision" DataField="TipoComision" />                                                               
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron solicitudes
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>                  
            </div>               
        </div>
    </asp:Panel>
    
    <asp:Panel ID="pnlComentariosSolicitud" Visible="false" runat="server">                 
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Comentarios de la Solicitud <asp:Label ID="lblIdSolicitud" runat="server" /></h3>
            </div>
            <div class="panel-body">
                <label>Comentarios:</label>
                <asp:TextBox ID="txtComenariosSolicitud" runat="server" TextMode="MultiLine" Rows="4" CssClass="form-control"></asp:TextBox>
                    
                <br />
                <asp:Button ID="btnGrabaComentarios" runat="server" Text="Guardar" CssClass="btn btn-sm btn-success " OnClick="btnGrabaComentarios_Click" AutoPostBack="true" />
                <asp:Button ID="btnCerrarComentarios" runat="server" Text="Regresar" CssClass="btn btn-sm btn-primary " OnClick="btnCerrarComentarios_Click" AutoPostBack="true" />
                    
                <asp:Panel ID="pnlError_Comentarios" class="form-group col-sm-12" Visible="false" runat="server">                                
                    <div class="alert alert-danger" style="padding:5px 15px 5px 15px; margin:10px 0px;">
                        <asp:Label ID="lblError_Comentarios" runat="server"></asp:Label>
                    </div>
                </asp:Panel> 

                <div style="height: auto; width: auto; margin-top:15px;">
                    <asp:GridView ID="gvComentariosSolicitud" runat="server" Width="100%" AutoGenerateColumns="False"
                        Height="100px" HeaderStyle-CssClass="GridHead" RowStyle-CssClass="GridRow" AlternatingRowStyle-CssClass="GridRowAlternate"
                        CssClass="table table-bordered table-striped  table-hover table-responsive" EmptyDataRowStyle-CssClass="GridEmptyData" GridLines="None" AllowPaging="true"
                        OnPageIndexChanging="gvComentariosSolicitud_PageIndexChanging" PageSize="10" PagerStyle-CssClass="GridPager" RowStyle-Wrap="true" HeaderStyle-Wrap="false"
                        DataKeyNames="IdSolicitud">
                        <PagerStyle CssClass="pagination-ys" />
                        <Columns>
                            <asp:BoundField HeaderText="Solicitud" DataField="IdSolicitud" />
                            <asp:BoundField HeaderText="Fecha Alta" DataField="Fec_Alta" ItemStyle-Wrap="false" />
                            <asp:BoundField HeaderText="Comentarios" DataField="comentarios" />
                            <asp:BoundField HeaderText="Usuario" DataField="Nom_Usuario" />
                        </Columns>
                        <EmptyDataTemplate>
                            No se encontraron comentarios.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </div>        
    </asp:Panel>

    <asp:Panel ID="pnlCronogramaSolicitud" Visible="false" runat="server">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Cronograma de la Solicitud <asp:Label ID="lblIdSolicitud_Cronograma" runat="server" /></h3>
            </div>
            <div class="panel-body">
                <asp:LinkButton ID="lbtnRegresar_Cronograma" CssClass="btn btn-sm btn-default hidden-print" OnClick="lbtnRegresar_Cronograma_Click" runat="server">
                    <i class="fas fa-arrow-alt-circle-left fa-lg"></i> Regresar
                </asp:LinkButton>
                <button class="btn btn-default btn-sm hidden-print" onclick="printPF();"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Imprimir</button>
                <hr class="hidden-print" />

                <div class="col-sm-12">
                    <blockquote style="background-color:#F8F8F8;">
                        <p class="text-center">Cronograma de Pagos Preliminar</p>
                    </blockquote>
                </div>

                <div class="col-sm-9 col-print-9">   
                    <label class="small">Cliente</label>        
                    <pre><asp:Label ID="lblCliente_Cronograma" runat="server"></asp:Label></pre>
                </div>

                <div class="col-sm-3 col-print-3">
                    <label class="small">DNI</label>           
                    <pre><asp:Label ID="lblDNI_Cronograma" runat="server"></asp:Label></pre>
                </div>                

                <div class="col-sm-12 col-print-12">   
                    <label class="small">Dirección</label>        
                    <pre><asp:Label ID="lblDireccionCliente_Cronograma" runat="server"></asp:Label></pre>
                </div>               
                                    
                <div class="col-sm-3 col-print-3"> 
                    <label class="small">Monto Credito</label>          
                    <pre><asp:Label ID="lblMontoDesembolsado_Cronograma" runat="server"></asp:Label></pre>
                </div>

                <div class="col-sm-3 col-print-3"> 
                    <label class="small">Tasa Efectiva Anual</label>          
                    <pre><asp:Label ID="lblTEA_Cronograma" runat="server"></asp:Label></pre>
                </div>

                <div class="col-sm-3 col-print-3"> 
                    <label class="small">Tasa Costo Efectiva Anual</label>          
                    <pre><asp:Label ID="lblTCEA_Cronograma" runat="server"></asp:Label></pre>
                </div>

                <div class="col-sm-3 col-print-3"> 
                    <label class="small">Total Interes</label>          
                    <pre><asp:Label ID="lblTotalInteres_Cronograma" runat="server"></asp:Label></pre>
                </div>                                    

                <div class="col-sm-3 col-print-3"> 
                    <label class="small">Numero Cuotas</label>          
                    <pre><asp:Label ID="lblNumeroCuotas_Cronograma" runat="server"></asp:Label></pre>
                </div>                                                     

                <div class="col-sm-3 col-print-3"> 
                    <label class="small">Fecha 1er. Vcto.</label>          
                    <pre><asp:Label ID="lblFechaPrimerPago_Cronograma" runat="server"></asp:Label></pre>
                </div>  

                <div class="col-sm-3 col-print-3"> 
                    <label class="small">Fecha Ult. Vcto.</label>          
                    <pre><asp:Label ID="lblFechaUltimoPago_Cronograma" runat="server"></asp:Label></pre>
                </div>  
                
                <div class="col-sm-3 col-print-3"> 
                    <label class="small">Fecha Desembolso</label>          
                    <pre class="visible-print-block"><asp:Label ID="lblFechaDesembolso_Cronograma" runat="server"></asp:Label></pre>
                    <div class="input-group hidden-print">
                        <asp:TextBox ID="txtFechaDesembolso_Cronograma" OnTextChanged="txtFechaDesembolso_Cronograma_TextChanged" AutoPostBack="true" CssClass="form-control" placeholder="(dd/mm/aaaa)" MaxLength="10" Enabled="false" runat="server"/>                    
                        <span class="input-group-addon">
                            <asp:LinkButton ID="btnFechaDesembolso" runat="server">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </asp:LinkButton>
                        </span>
                    </div>
                    <ajaxtoolKit:CalendarExtender ID="ceFechaDesembolso_Cronograma" Format="dd/MM/yyyy" TargetControlID="txtFechaDesembolso_Cronograma" PopupButtonID="btnFechaDesembolso"  runat="server"></ajaxtoolKit:CalendarExtender>
                </div> 

                <div class="col-sm-12 col-print-12">
                    <hr />
                    <div style="height:auto; width: auto; overflow-x: auto;">                                                    
                        <asp:GridView ID="gvRecibos_Cronograma" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="small"                            
                            CssClass="table table-bordered table-striped table-responsive table-condensed" EmptyDataRowStyle-CssClass="GridEmptyData">                                                                    
                            <Columns>
                                <asp:BoundField  HeaderText="N° Cuota" DataField="NoRecibo" />      
                                <asp:BoundField  HeaderText="Fecha" DataField="FechaRecibo" DataFormatString="{0:d}" />                      
                                <asp:BoundField  HeaderText="Capital Inicial" DataField="CapitalInicial" DataFormatString="{0:c2}" />                                
                                <asp:BoundField  HeaderText="Capital Amortizado" DataField="Capital" DataFormatString="{0:c2}" />
                                <asp:BoundField  HeaderText="Interes" DataField="Interes" DataFormatString="{0:c2}" />
                                <asp:BoundField  HeaderText="IGV" DataField="IGV" DataFormatString="{0:c2}" />
                                <asp:BoundField  HeaderText="Seguro" DataField="Seguro" DataFormatString="{0:c2}" />
                                <asp:BoundField  HeaderText="GAT" DataField="GAT" DataFormatString="{0:c2}" />                                
                                <asp:BoundField  HeaderText="Saldo Capital" DataField="SaldoCapital" DataFormatString="{0:c2}" />
                                <asp:BoundField  HeaderText="Cuota" DataField="Cuota" DataFormatString="{0:c2}" />
                            </Columns>                                                
                            <EmptyDataTemplate>
                                Hubo un problema al cargar los pagos del cronograma
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>  
                </div>                

                <div class="col-sm-12 col-print-12">                    
                    <p>
                        Condiciones:
                    </p>
                    <p class="small">
                        <asp:Label ID="lblCondiciones" runat="server"></asp:Label>
                    </p>
                    <p class="text-center" style="margin-top: 85px;">
                        _________________________________________
                    </p>
                    <p class="text-center small">
                        Firma Cliente
                    </p>
                </div>
            </div>
        </div>  
    </asp:Panel> 
    <asp:HiddenField ID="filtrarSucursal" runat="server" />
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
