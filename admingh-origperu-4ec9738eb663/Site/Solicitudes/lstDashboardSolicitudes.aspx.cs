﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using wsSOPF;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.Services;
using System.Web.Script.Services;

public partial class lstDashboardSolicitudes : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{        
		/**Seccion para verificar que se deba filtrar por sucursal**/		
	    if (Convert.ToInt32(Session["Rol"].ToString()) == 17 || Convert.ToInt32(Session["Rol"].ToString()) == 2)
		{
			filtrarSucursal.Value = "1";
		}
		else
		{
			filtrarSucursal.Value = "0";
		}
		//List<TBClientes.BusquedaCliente> listacliente = new List<TBClientes.BusquedaCliente>();

		if (!Page.IsPostBack)
		{
			llenarSucursales();
			//llenarPromotor(1,0);

			LlenaDatagrid(0, "1");
		}
	}		

	protected void cmbEstatus_SelectedIndexChanged(object sender, EventArgs e)
	{
		LlenaDatagrid();
	}

	protected void cmbPertenece_SelectedIndexChanged(object sender, EventArgs e)
	{
		LlenaDatagrid();
	}

	protected void cmbPromotor_SelectedIndexChanged(object sender, EventArgs e)
	{
		LlenaDatagrid();
	}


	protected void btnBuscar_Click(object sender, EventArgs e)
	{

		LlenaDatagrid();

	}

	protected void cmbSucursal_SelectedIndexChanged(object sender, EventArgs e)
	{

		LlenaDatagrid(0, "1");
	}

    protected void gvSolicitudes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Mostramos la "accion" VerCronograma solo si la solicitud esta en estatus Activa.
            int indexEstatus = ControlsHelper.GetColumnIndexByName(e.Row, "Estatus");

            LinkButton lbtnVerCronograma = (LinkButton)e.Row.FindControl("lbtnVerCronograma");

            LinkButton lbtnSolPDF = (LinkButton)e.Row.FindControl("lbtnSolPDF");
            

            lbtnVerCronograma.Visible = (e.Row.Cells[indexEstatus].Text == "A" || e.Row.Cells[indexEstatus].Text == "SC");

            lbtnSolPDF.Visible = (e.Row.Cells[indexEstatus].Text == "A" || e.Row.Cells[indexEstatus].Text == "SC");

            // TEMPORAL, DESHABILITAMOS la "accion" VerCronograma para los Convenios "CONVENIO"
            DataRow dr = ((DataRowView)e.Row.DataItem).Row;
            if (dr["Convenio"].ToString() == "CONVENIO")
                lbtnVerCronograma.Enabled = false;
        }
    }

    protected void gvSolicitudes_RowCommand(object sender, GridViewCommandEventArgs e)
	{
        try
        {
            Session["IdSolicitud"] = null;
            if (e.CommandName == "Editar")
            {
                int IdSolicitud = 0;
                int idCliente = 0;

                LinkButton comando = e.CommandSource as LinkButton;
                GridViewRow registro = comando.Parent.Parent as GridViewRow;
                int.TryParse(gvSolicitudes.DataKeys[registro.RowIndex]["IdSolicitud"].ToString(), out IdSolicitud);
                int.TryParse(gvSolicitudes.DataKeys[registro.RowIndex]["IdCliente"].ToString(), out idCliente);
                Session["IdSolicitud"] = IdSolicitud;
                               
                if (cmbEstatus.SelectedValue == "6")
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("frmEditarSolicitud.aspx?idcliente=" + idCliente.ToString() + "&idsolicitud=" + IdSolicitud, false);
                }

                if (cmbEstatus.SelectedValue == "1")
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    TBSolicitudes.DatosSolicitud[] DatosSol = null;
                    using (wsSOPF.SolicitudClient wsSolicitudSol = new SolicitudClient())
                    {
                        DatosSol = wsSolicitudSol.ObtenerDatosSolicitud(IdSolicitud);

                        if (DatosSol.Length > 0)
                        {
                            if (DatosSol[0].IdAsignado == 0)
                            {
                                if (DatosSol[0].IdEstatusProceso == 2)
                                    Response.Redirect("frmEditarSolicitud.aspx?idcliente=" + idCliente.ToString() + "&idsolicitud=" + IdSolicitud, false);
                                else
                                    Response.Redirect("frmDocumentos.aspx?idcliente=" + idCliente.ToString() + "&idsolicitud=" + IdSolicitud, false);
                            }
                            else
                            {
                                lblError.Text = "La solicitud cuenta con un analista asignado";
                                pnlError.Visible = true;
                            }
                        }
                    }
                }
            }

            if (e.CommandName == "Comentarios")
            {
                int idSolicitud = 0;
                int idrole = Convert.ToInt32(Session["Rol"].ToString());
                LinkButton comando = e.CommandSource as LinkButton;
                GridViewRow registro = comando.Parent.Parent as GridViewRow;
                int.TryParse(gvSolicitudes.DataKeys[registro.RowIndex]["IdSolicitud"].ToString(), out idSolicitud);
                lblIdSolicitud.Text = idSolicitud.ToString();


                using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
                {
                    wsSolicitud.ActualizaComentariosSol(idSolicitud, idrole, 1);
                }

                pnlError_Comentarios.Visible = false;
                txtComenariosSolicitud.Text = "";
                pnlBuscarSolicitud.Visible = false;
                pnlComentariosSolicitud.Visible = true;
                LlenaGridComentarios();
            }

            if (e.CommandName == "VerCronograma")
            {
                int idSolicitud = int.Parse(e.CommandArgument.ToString());
                lblIdSolicitud_Cronograma.Text = idSolicitud.ToString();

                txtFechaDesembolso_Cronograma.Text = DateTime.Today.ToString("dd/MM/yyyy");
                CargarCronograma();

                pnlBuscarSolicitud.Visible = false;
                pnlCronogramaSolicitud.Visible = true;
            }

            if (e.CommandName == "VerSol_PDF")
            {
                int idSolicitud = int.Parse(e.CommandArgument.ToString());
                lblIdSolicitud_Cronograma.Text = idSolicitud.ToString();

                registrarScript("generar_descarga(" + idSolicitud + ");");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
	}

    public void registrarScript(String pScript, String ScriptKey = "regscript")
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), pScript, true);
    }

    public void registrarScript(Page pPage, String pScript, String ScriptKey = "regscript")
    {
        ScriptManager.RegisterStartupScript(pPage, pPage.GetType(), Guid.NewGuid().ToString(), pScript, true);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static object DescargarDoc(int idSolicitud)
    {
        object Response = null;

        try
        {            
            ImpresionDocumentoRequest valSol = new ImpresionDocumentoRequest();
            
            using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
            {
                valSol.IdSolicitud = idSolicitud;
                valSol.IdUsuario = 21;
                valSol.RegresaUrl = false;
                valSol.Version = 0;
                
               var RptSol = wsSolicitud.ImpresionSolicitudRPT(valSol);
                
                if (RptSol != null)
                {
                    string contenido = string.Empty;
                    string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    string url = RptSol.Resultado.Url;
                    string filename = RptSol.Resultado.NombreArchivo;
                     contenido = Convert.ToBase64String(RptSol.Resultado.ContenidoArchivo);
                    Response = new
                    {
                        Error = false,
                        Mensaje = "El reporte se ha generado exitosamente.",
                        NombreArchivoNuevo= filename,
                        Contenido = contenido,
                        Tipo = RptSol.Resultado.TipoContenido,
                        Longitud = contenido.Length.ToString()
                    };
                }
            }          
        }
        catch (Exception ex)
        {
            return ex;
        }
        return Response;
    }

    protected void gvSolicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvSolicitudes.PageIndex = e.NewPageIndex;
        LlenaDatagrid();
    }

    #region "Comentarios"

    protected void btnGrabaComentarios_Click(object sender, EventArgs e)
    {
        try
        {
            if (lblIdSolicitud.Text != "")
            {
                if (txtComenariosSolicitud.Text != "")
                {
                    int idUsuario = Convert.ToInt32(Session["UsuarioId"].ToString());
                    int idrole = Convert.ToInt32(Session["Rol"].ToString());

                    using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
                    {
                        wsSolicitud.AltaComentariosSolicitud(Convert.ToInt32(lblIdSolicitud.Text), 0, txtComenariosSolicitud.Text, idUsuario, idrole);
                    }

                    LlenaGridComentarios();
                    LlenaDatagrid();
                    btnCerrarComentarios_Click(sender, e);
                }
                else
                {
                    throw new Exception("Favor de capturar el comentario");
                }
            }
            else
            {
                throw new Exception("Favor de seleccionar la solicitud");
            }
        }
        catch (Exception ex)
        {
            pnlError_Comentarios.Visible = true;
            lblError_Comentarios.Text = ex.Message;
        }
    }    

    protected void btnCerrarComentarios_Click(object sender, EventArgs e)
    {
        txtComenariosSolicitud.Text = "";
        pnlBuscarSolicitud.Visible = true;
        pnlComentariosSolicitud.Visible = false;
    }

    protected void gvComentariosSolicitud_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvComentariosSolicitud.PageIndex = e.NewPageIndex;
        LlenaGridComentarios();
    }

    #endregion

    #region "Cronograma"

    protected void lbtnRegresar_Cronograma_Click(object sender, EventArgs e)
    {
        pnlBuscarSolicitud.Visible = true;
        pnlCronogramaSolicitud.Visible = false;
    }

    protected void txtFechaDesembolso_Cronograma_TextChanged(object sender, EventArgs e)
    {
        lblFechaDesembolso_Cronograma.Text = txtFechaDesembolso_Cronograma.Text;
        CargarCronograma();
    }

    #endregion

    #region "Metodos"

    private void LlenaDatagrid(int PageIndex = 0, string bandera = "0")
    {
        gvSolicitudes.DataSource = null;
        DataSet solicitudes = new DataSet();
        int idSusursal = 0, idUsuario = 0, idrole = 0, promotor = 0;

        try
        {
            int Pertenece = Convert.ToInt32(cmbPertenece.SelectedItem.Value);
            int EstatusSol = Convert.ToInt32(cmbEstatus.SelectedItem.Value);
            if (Pertenece == 0)
            {
                idSusursal = 0;
                idUsuario = 0;
                idrole = 0;
                if (filtrarSucursal.Value == "1")
                {
                    idSusursal = Convert.ToInt32(Session["compania"].ToString());
                }
                else
                {
                    idSusursal = Convert.ToInt32(cmbSucursal.SelectedItem.Value);
                }
            }
            else
            {
                idrole = Convert.ToInt32(Session["Rol"].ToString());
                if (filtrarSucursal.Value == "1")
                {
                    idSusursal = Convert.ToInt32(Session["compania"].ToString());
                }
                else
                {

                    idSusursal = Convert.ToInt32(cmbSucursal.SelectedItem.Value);
                }
                //idSusursal =Convert.ToInt32(Session["compania"].ToString());
                idUsuario = Convert.ToInt32(Session["UsuarioId"].ToString());
            }

            //llñenamos promotor
            //checamos si hubo un cambio en llenarSucursales o si se recarga la pagina para filtrar promotores
            if (Convert.ToInt32(bandera) == 1)
            {
                if (filtrarSucursal.Value == "1")
                {
                    if (idrole == 2)
                    {
                        llenarPromotor(2, idUsuario);
                    }
                    else
                    {
                        llenarPromotor(1, idSusursal);
                    }
                }
                else
                {
                    if (idSusursal == 0)
                    {
                        llenarPromotor(2, idUsuario);
                    }
                    else
                    {
                        llenarPromotor(1, idSusursal);
                    }
                }
            }

            //llenarSucursales();
            //obyenemos promotor de sucursal elejida
            if (cmbPromotor.SelectedItem.Value != null)
            {
                promotor = Convert.ToInt32(cmbPromotor.SelectedItem.Value);
            }

            using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
            {
                solicitudes = wsSolicitud.ObtenerTBSolicitudes(32, promotor, EstatusSol, txtIdSolicitud.Text, "", "", idSusursal, idUsuario, idrole);
            } //int Accion, int cuenta, int idcliente, string cliente, string fchInicial, string fchFinal, int idsucursal, int idconvenio, int idlinea

            if (solicitudes.Tables[0].Rows.Count > 0)
            {
                gvSolicitudes.DataSource = solicitudes.Tables[0];
                gvSolicitudes.DataBind();
            }
            else
            {
                gvSolicitudes.DataSource = null;
                gvSolicitudes.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void llenarPromotor(int Accion, int parametrox)
    {

        //int idSusursal = Convert.ToInt32(Session["compania"].ToString());
        TBCATPromotor[] promotor = null;
        using (wsSOPF.CatalogoClient wsPromotor = new CatalogoClient())
        {
            promotor = wsPromotor.ObtenerPromotores(Accion, parametrox);

        }
        int rolusuario = Convert.ToInt32(Session["Rol"].ToString());
        if (promotor.Count() > 0)
        {
            //List<Country> countries = ...;

            cmbPromotor.DataSource = null;//Vaciar comboBox
                                          //cmbPromotor.Items.Add(new KeyValuePair<string, string>("0", "Todos"));

            cmbPromotor.DataTextField = "Promotor";//Indicar qué propiedad se verá en la lista
            cmbPromotor.DataValueField = "IdPromotor";//Indicar qué valor tendrá cada ítem
            cmbPromotor.DataSource = promotor;//Asignar la propiedad DataSource
            cmbPromotor.DataBind();
            //cmbPromotor.Items[0].Text = "Todos";
            cmbPromotor.Items.Add(new System.Web.UI.WebControls.ListItem("Todos", "0"));
            //cmbPromotor.Items[0].Value = "0";    
            cmbPromotor.SelectedIndex = 0;

            //ponemos como seleccionado el promotor que se haya loqgueado.

            //int idSusursal = Convert.ToInt32(Session["compania"].ToString());
            //TBCATPromotor[] promotor1 = null;
            //using (wsSOPF.CatalogoClient wsPromotor = new CatalogoClient())
            //{
            //    promotor1 = wsPromotor.ObtenerPromotores(Accion, parametrox);

            //}

            //if (rolusuario == 2)
            //{
            //    cmbPromotor.Enabled = false;


            //    int j = 0;

            //    if (idSusursal > 0 && idSusursal != null)
            //    {

            //        for (j = 0; j <= cmbPromotor.Items.Count - 1; j++)
            //        {
            //            if (Convert.ToString(cmbPromotor.Items[j].Value.Trim()) == Convert.ToString(idSusursal))
            //            {
            //                cmbPromotor.SelectedIndex = idSusursal;
            //                break;
            //            }

            //        }
            //    }
            //    else
            //    {
            //        cmbSucursal.SelectedIndex = 0;
            //    }
            //}

        }
        else
        {
            cmbPromotor.Items.Clear();
            cmbPromotor.Items.Add(new System.Web.UI.WebControls.ListItem("Sin promotores", "-1"));
            //cmbPromotor.Items[0].Value = "0";    
            cmbPromotor.SelectedIndex = -1;
            //cmbPromotor.DataSource = null;
        }

        if (rolusuario == 2)
        {
            cmbPromotor.Enabled = false;
        }




    }

    private void llenarSucursales()
    {
        int rolusuario = Convert.ToInt32(Session["Rol"].ToString());
        int idUsuario = Convert.ToInt32(Session["UsuarioId"].ToString());

        TBCATSucursal[] sucursalList = null;
        using (wsSOPF.UsuarioClient wsSucursal = new UsuarioClient())
        {
            wsSOPF.TBCATSucursal sucursal = new TBCATSucursal();
            sucursal.IdSucursal = idUsuario;
            sucursal.Accion = 101;
            sucursal.Sucursal = "";
            sucursal.IdEstatus = 0;
            sucursal.IdAfiliado = 0;

            sucursalList = wsSucursal.ObtenerSucursal(sucursal);

            if (sucursalList.Count() > 0)
            {
                //List<Country> countries = ...;

                cmbSucursal.DataSource = null;//Vaciar comboBox
                                              //cmbPromotor.Items.Add(new KeyValuePair<string, string>("0", "Todos"));

                cmbSucursal.DataTextField = "Sucursal";//Indicar qué propiedad se verá en la lista
                cmbSucursal.DataValueField = "IdSucursal";//Indicar qué valor tendrá cada ítem
                cmbSucursal.DataSource = sucursalList;//Asignar la propiedad DataSource
                cmbSucursal.DataBind();
                //cmbPromotor.Items[0].Text = "Todos";
                cmbSucursal.Items.Add(new System.Web.UI.WebControls.ListItem("Todos", "0"));
                //cmbPromotor.Items[0].Value = "0";    
                cmbSucursal.SelectedIndex = 0;




                // cmbTipoComision.Items.Clear();
                //cmbTipoComision.DataTextField = ds.Tables[0].Columns["Descripcion"].ToString();
                //cmbTipoComision.DataValueField = ds.Tables[0].Columns["Valor"].ToString();

                //cmbTipoComision.DataSource = ds.Tables[0].DefaultView;
                //cmbTipoComision.DataBind();
            }

            //ponemos como seleccionada la suucursal ala cual pertenece el usuario.
            int idSusursal = Convert.ToInt32(Session["compania"].ToString());

            if (rolusuario == 17 || rolusuario == 2)
            {
                cmbSucursal.Enabled = false;


                int j = 0;

                if (idSusursal > 0 && idSusursal != null)
                {

                    for (j = 0; j <= cmbSucursal.Items.Count - 1; j++)
                    {
                        if (Convert.ToString(cmbSucursal.Items[j].Value.Trim()) == Convert.ToString(idSusursal))
                        {
                            cmbSucursal.SelectedIndex = idSusursal;
                            break;
                        }

                    }
                }
                else
                {
                    cmbSucursal.SelectedIndex = 0;
                }


            }





        }
    }

    private void LlenaGridComentarios()
    {
        DataSet comentarios = new DataSet();

        try
        {
            using (wsSOPF.SolicitudClient wsSolicitud = new SolicitudClient())
            {
                comentarios = wsSolicitud.ConComentariosSolicitud(int.Parse(lblIdSolicitud.Text), 0, 1);
            }

            gvComentariosSolicitud.DataSource = null;
            if (comentarios.Tables[0].Rows.Count > 0)
                gvComentariosSolicitud.DataSource = comentarios;

            gvComentariosSolicitud.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void CargarCronograma()
    {
        using (wsSOPF.SolicitudClient ws = new SolicitudClient())
        {
            DateTime FechaDesembolso = DateTime.Today;
            DateTime.TryParseExact(txtFechaDesembolso_Cronograma.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out FechaDesembolso);

            CronogramaPagos kronos = ws.Solicitudes_ObtenerCronograma(int.Parse(lblIdSolicitud_Cronograma.Text), FechaDesembolso);

            lblDNI_Cronograma.Text = kronos.DNICliente;
            lblCliente_Cronograma.Text = string.Format("{0} {1} {2}", kronos.NombreCliente, kronos.ApPaternoCliente, kronos.ApMaternoCliente);
            lblDireccionCliente_Cronograma.Text = kronos.DireccionCliente;
            lblMontoDesembolsado_Cronograma.Text = kronos.MontoDesembolsar.ToString("C", CultureInfo.CurrentCulture);
            lblTEA_Cronograma.Text = String.Format("{0:P2}", kronos.TasaAnual/100);
            lblTCEA_Cronograma.Text = String.Format("{0:P2}", kronos.TasaCostoAnual/100);
            lblTotalInteres_Cronograma.Text = kronos.TotalInteres.ToString("C", CultureInfo.CurrentCulture);
            lblNumeroCuotas_Cronograma.Text = kronos.Plazo.ToString();
            lblFechaDesembolso_Cronograma.Text = kronos.FechaDesembolso.ToString("dd/MM/yyyy");
            lblFechaPrimerPago_Cronograma.Text = kronos.PrimerFechaPago.ToString("dd/MM/yyyy");
            lblFechaUltimoPago_Cronograma.Text = kronos.UltimaFechaPago.ToString("dd/MM/yyyy");
            lblCondiciones.Text = kronos.Condiciones.Trim();

            // Si el Monto del Seguro es 0, ocultamos la columna            
            ((DataControlField)gvRecibos_Cronograma.Columns
               .Cast<DataControlField>()
               .Where(fld => (fld.HeaderText == "Seguro"))
               .SingleOrDefault()).Visible = (kronos.Recibos.Sum(x => x.Seguro) > 0);

            gvRecibos_Cronograma.DataSource = kronos.Recibos;
            gvRecibos_Cronograma.DataBind();
        }
    }
    
    #endregion        
}
