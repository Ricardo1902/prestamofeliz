﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Site_Soporte_MesaDeAyuda : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			TBCATUsuario[] usuario = null;
			string strUsuario = "";
			string strCorreo = "";

			using (wsSOPF.UsuarioClient wsUsuario = new UsuarioClient())
			{
				usuario = wsUsuario.ObtenerUsuario(2, Convert.ToInt32(Session["UsuarioId"]), "");
				if (usuario.Length == 1)
				{
					strUsuario = usuario[0].Usuario;
					strCorreo = usuario[0].Email.Trim();
				}
			}

			if (!string.IsNullOrEmpty(strUsuario))
			{
				if (string.IsNullOrEmpty(strCorreo))
					strCorreo = "servicedesk@prestamofeliz.com.mx";

				string strURL = "";
				strURL = "https://adsis.corporativomty.com/ticket-alta-ext.html?";
				strURL += "iu=0";
				strURL += "&nk=" + strUsuario;
				strURL += "&co=" + strCorreo;
				strURL += "&is=5";

				string strScript = "";
				strScript = "var prm = { idU: 0, NK: '" + strUsuario + "', CO: '" + strCorreo + "' };";
				strScript += "var strURL = '" + strURL + "';";
				strScript += "$('#divIframe').append('<iframe class=\"iFrameAdsis\" src=\"" + strURL + "\" style=\"width:100%;height: 800px; \"></iframe>');";
				strScript += "setTimeout(function () {$(\".iFrameAdsis\").attr('src', strURL);}, 1000);";

				string strJS = "$(function(){" + strScript +"});";
				Page.ClientScript.RegisterStartupScript(this.GetType(), "scriptLogin", strJS, true);

				//iframeSoporte.Attributes["src"] = strURL;
			}
		}
		catch (Exception ex)
		{

		}
	}
}