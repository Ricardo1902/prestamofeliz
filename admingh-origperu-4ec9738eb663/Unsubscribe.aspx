﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Unsubscribe.aspx.cs" Inherits="Unsubscribe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Unsubscribe</title>
</head>
<body>
    <div style="display: flex; flex-direction: column; width: 100%; height: 90vh; justify-content: center; overflow: hidden; align-items: center;">
        <img style="margin-bottom: 10px;" src="Imagenes/Prestamo-Feliz-logo-Print.png" />
        <div style="background-color: #eee; padding: 25px; border-radius: 4px;">
            <h3>Se ha desuscrito exitosamente al correo</h3>
        </div>
    </div>
</body>
</html>
