﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using wsSOPF;

public partial class Unsubscribe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HubbleClient hubbleClient = new HubbleClient();

        try
        {
            string email = Request.QueryString["email"].ToString();
            string template = Request.QueryString["template"].ToString();

            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(template))
            {
                hubbleClient.UnsubscribeEmail(email, template);
            }
        }
        catch (Exception ex)
        {

        }

        hubbleClient.Close();
    }
}