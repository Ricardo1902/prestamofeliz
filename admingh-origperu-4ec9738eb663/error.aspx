﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="error.aspx.cs" Inherits="error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Error</title>

     <link rel="shortcut icon" type="text/css" href="Imagenes/MenuLog/icono.ico" />
    <link rel="Stylesheet" type="text/css" href="Css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="Css/bootstrap.min.css"  />
    <link rel="stylesheet" type="text/css" href="Css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/Site.css"  /> 
 

    <!-- JS Archivos-->
    <script type="text/javascript" src="js/jquery-2.1.4.min.js" ></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- JS Archivos-->
    <script type="text/javascript" language="JavaScript">

        /*Ocultar Error en Login*/
        function hideError() {
                        document.getElementById("lblError").style.display = "none";
        }
        </script>


    <script type="text/javascript">
        setTimeout(function () { location.href = "frmlogin.aspx"; }, 10000);
    </script>
</head>
<body>

    <div class="container container-table">
        <div class="row vertical-center-row">

            <div class="col-md-12 col-xs-12 col-ms-12 col-lg-6 col-centered">
                <div class="panel  panel-default">
                    <div class="panel-heading panel-heading-custom text-center"><span id="Titulo" runat="server"></span></div>
                    <div class="panel-body">
                        <form id="frmMain" runat="server">
                            <div style="text-align: center">
                                <asp:Image ID="Image1" runat="server" ImageUrl="Imagenes/MenuLog/Logotipo.png"
                                    Width="100px" Height="100px" />
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-centered">
                                    <div class="form-group text-center">
                                         <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-centered">
                                    <div class="form-group text-center">
                                        <p><a href="frmLogin.aspx">Haga clic aqu&iacute; para regresar a la pantalla inicial</a></p>
                                    </div>
                                </div>
                            </div>
                          

                        </form>
                        
                            
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>
</html>
