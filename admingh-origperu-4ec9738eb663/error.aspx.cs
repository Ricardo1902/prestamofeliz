﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Text;
using System.Configuration;

public partial class error : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Titulo.InnerText = ConfigurationManager.AppSettings["Titulo"];
        Page.Title = ConfigurationManager.AppSettings["Titulo"];

        cls_Rep _cls_Rep = new cls_Rep();      
      Boolean blnSession;
      blnSession = (Session["UsuarioError"] != null);
      if (blnSession)
      {
        lblMensaje.Text = "Ha ocurrido el siguiente error en el sistema: " + Session["lastErrorMessage"].ToString();
        _cls_Rep.Rep_Error(Convert.ToInt32(Session["UsuarioError"].ToString()), Session["PaginaError"].ToString(), Session["lastErrorMessage"].ToString());

        if (Session["UsuarioId"] != null)
        {
          cls_Rep Obj_Rep = new cls_Rep();
          System.Data.DataSet DS_ValUser = new System.Data.DataSet();
          DS_ValUser = Obj_Rep.Val_Usuario("", "", Convert.ToInt32(Session["UsuarioId"]), 5, 0);

          FormsAuthentication.SignOut();
          Session.Clear();
          Session["identificado"] = false;
        }
      }
      else
      {
        lblMensaje.Text = "Ha finalizado el tiempo permitido para su sesión";
        _cls_Rep.Rep_Error(0,"0", "Timeout Sesión");
      }     
    }
}