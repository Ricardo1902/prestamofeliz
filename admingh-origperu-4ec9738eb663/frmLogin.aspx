﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmLogin.aspx.cs" Inherits="frmLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <link rel="shortcut icon" type="text/css" href="Imagenes/MenuLog/icono.ico" />
    <link rel="Stylesheet" type="text/css" href="Css/bootstrap.min.css" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="Css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/Site.css"  /> 
 
    <!-- CSS Locales -->
    <style type="text/css">       
        .alert .alert-icon-col {
            min-width: 15px;
            max-width: 15px;
            padding-top:3px;           
        }

        .alert-link {
            /* i find 700 to bold */
            font-weight: 600;
        }
    </style>

    <!-- JS Archivos-->
    <script type="text/javascript" src="js/jquery-2.1.4.min.js" ></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- JS Archivos-->
    <script type="text/javascript" language="JavaScript">
        /* Llamada AJAX login */
        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(startRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);
        });        
 
        function startRequest(sender, e) {
            //disable button during the AJAX call
            if ($("#<%=pnlLogin.ClientID%>").length) {
                document.getElementById('<%=txtUsuario.ClientID%>').disabled = true;
                document.getElementById('<%=txtContraseña.ClientID%>').disabled = true;

                $('#<%=btnEntrar.ClientID%>').button('loading');
            }

            if ($("#<%=pnlResetClave.ClientID%>").length) {
                document.getElementById('<%=txtResetClave.ClientID%>').disabled = true;
                document.getElementById('<%=txtResetClaveConfirmar.ClientID%>').disabled = true;

                $('#<%=btnResetClave.ClientID%>').button('loading');
            }
        }
        function endRequest(sender, e) {
            //re-enable button once the AJAX call has completed
            if ($("#<%=pnlLogin.ClientID%>").length) {
                document.getElementById('<%=txtUsuario.ClientID%>').disabled = false;
                document.getElementById('<%=txtContraseña.ClientID%>').disabled = false;

                $('#<%=btnEntrar.ClientID%>').button('reset');
            }

            if ($("#<%=pnlResetClave.ClientID%>").length) {
                document.getElementById('<%=txtResetClave.ClientID%>').disabled = false;
                document.getElementById('<%=txtResetClaveConfirmar.ClientID%>').disabled = false;

                $('#<%=btnResetClave.ClientID%>').button('reset');
            }
        }

        /*Ocultar Error en Login*/
        function hideError() {
            document.getElementById("divError").style.display = "none";
        }

        /*Verificar el Navegador*/
        function checkNaveg() {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                alert("Para utilizar la aplicación favor de usar los navegadores Chrome o Mozilla Firefox.");
                location.href = "http://www.grupogarsa.com/";

            }
            else {
                var trident = ua.indexOf('Trident/');
                if (trident > 0) {
                    // IE 11 => return version number
                    var rv = ua.indexOf('rv:');
                    alert("Para utilizar la aplicación favor de usar los navegadores Chrome o Mozilla Firefox.");
                    location.href = "http://www.grupogarsa.com/";
                }

                var edge = ua.indexOf('Edge/');
                if (edge > 0) {
                    // Edge (IE 12+) => return version number
                    alert("Para utilizar la aplicación favor de usar los navegadores Chrome o Mozilla Firefox.");
                    location.href = "http://www.grupogarsa.com/";
                }
            }

            return false;
        }
    </script>

</head>
<body onload="checkNaveg()">
    <div class="container container-table">
        <div class="row vertical-center-row">
            <div class="col-md-8 col-xs-12 col-ms-12 col-lg-6 col-centered">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-custom text-center"><span id="Titulo" runat="server"></span></div>
                    <div class="panel-body">
                        <form id="form1" runat="server">
                            <asp:ScriptManager ID="sm_main" runat="server"></asp:ScriptManager>
                            
                            <asp:UpdatePanel ID="upMain" runat="server">
                            <ContentTemplate>
                            <div style="text-align: center; margin:20px 0px;">
                            <asp:Image ID="Image1" runat="server" ImageUrl="Imagenes/MenuLog/Logotipo.png" Width="100px" Height="100px" />
                            </div>
                            <asp:Panel ID="pnlLogin" runat="server">
                                <div class="row">
                                    <div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 col-centered">
                                        <div class="form-group">
                                            <div class="input-group margin-bottom-sm">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                <asp:TextBox CssClass="form-control" ID="txtUsuario" placeholder="Usuario" onblur="hideError();" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8 col-sm-8 col-md-6 col-lg-6 col-centered">
                                        <div class="form-group">
                                            <div class="input-group margin-bottom-sm">
                                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                                <asp:TextBox CssClass="form-control" ID="txtContraseña" placeholder="Contraseña" onblur="hideError();" Text="" runat="server" TextMode="Password"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:20px;">
                                    <div class="col-xs-8 col-sm-8 col-md-6 col-lg-4 col-centered">
                                        <div class="form-group btn-group btn-group-justified">
                                            <div class="btn-group" role="group">
                                                <asp:Button ID="btnEntrar" runat="server" Text="Entrar" CssClass="btn btn-primary" data-loading-text="Verificando..." OnClick="btnClick_Validar" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="pnlResetClave" Visible="false" runat="server">
                                <div class="form-group row">
                                    <div class="col-xs-11 col-sm-8 col-md-10 col-lg-10 col-centered text-center">
                                        Debe cambiar su contraseña para continuar.<br />
                                        Capture y confirme su nueva contraseña de acceso.
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 col-centered">
                                        <div class="form-group">
                                            <div class="input-group margin-bottom-sm">
                                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                                <asp:TextBox CssClass="form-control" ID="txtResetClave" placeholder="Nueva Contraseña" onblur="hideError();" Text="" TextMode="Password" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 col-centered">
                                        <div class="form-group">
                                            <div class="input-group margin-bottom-sm">
                                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                                <asp:TextBox CssClass="form-control" ID="txtResetClaveConfirmar" placeholder="Confirme Contraseña" onblur="hideError();" Text="" TextMode="Password" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:20px;">
                                    <div class="col-sm-8 col-md-6 col-lg-6 col-centered">
                                        <div class="form-group btn-group btn-group-justified">
                                            <div class="btn-group" role="group">
                                                <asp:Button ID="btnResetClave" runat="server" Text="Actualizar Contraseña" CssClass="btn btn-success " data-loading-text="Procesando..." OnClick="btnResetClave_Validar" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div id="divError" class="row" style="display: none;"  runat="server">                                   
                                <div class="col-xs-8 alert alert-danger col-centered" style="padding:4px 5px;" role="alert">
                                    <div class="row">
                                        <div class="col-xs-1 col-xs-offset-1 alert-icon-col">
                                            <span class="fa fa-exclamation-circle fa-fw"></span>
                                        </div>
                                        <div class="col-xs-9">
                                            <small><asp:Label ID="lblError" runat="server"></asp:Label></></small>
                                        </div>
                                    </div>
                                </div>                                                                                 
                            </div>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

</body>
</html>
