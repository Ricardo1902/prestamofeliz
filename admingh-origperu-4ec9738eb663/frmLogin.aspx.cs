﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

using System.IO;

public partial class frmLogin : System.Web.UI.Page
{
    cls_Rep Obj_Rep = new cls_Rep();
    static SqlConnection conBd;

    #region "Propiedades"

    private int IdUsuarioLogin
    {
        get
        {
            return (ViewState[this.UniqueID + "_IdUsuarioLogin"] != null) ? (int)ViewState[this.UniqueID + "_IdUsuarioLogin"] : 0;
        }

        set
        {
            ViewState[this.UniqueID + "_IdUsuarioLogin"] = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Titulo.InnerText = ConfigurationManager.AppSettings["Titulo"];
        Page.Title = ConfigurationManager.AppSettings["Titulo"];

        if (!IsPostBack)
        {
            txtContraseña.Attributes.Add("onblur", "hideError()");
            txtUsuario.Attributes.Add("onblur", "hideError()");
            if (Session["UsuarioId"] != null)
            {
                cls_Rep Obj_Rep = new cls_Rep();
                System.Data.DataSet DS_ValUser = new System.Data.DataSet();
                DS_ValUser = Obj_Rep.Val_Usuario("", "", Convert.ToInt32(Session["UsuarioId"]), 5, 0);

                FormsAuthentication.SignOut();
                Session.Clear();
                Session["identificado"] = false;
            }
        }
    }

    protected void btnClick_Validar(object sender, EventArgs e)
    {
        lblError.Text = string.Empty;
        divError.Attributes.Add("style", "display:none;");

        try
        {
            Session["SistemaId"] = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SistemaID"].ToString());

            // Validar campos (credenciales) no esten vacios
            if (string.IsNullOrEmpty(txtUsuario.Text.Trim()))
                throw new Exception("Capture el Usuario");

            if (string.IsNullOrEmpty(txtContraseña.Text.Trim()))
                throw new Exception("Capture la Contraseña");

            string Token = "0";
            int Tokeni = 0;

            if (Session["Tk"] != null)
            {
                Token = Decrypt(Session["Tk"].ToString());

                if (Token.IndexOf("§") >= 0)
                {
                    Token = Token.Substring(Token.IndexOf("§") + 1, Token.Length - Token.IndexOf("§") - 1);

                    if (Token.IndexOf("§") >= 0)
                    {
                        Token = Token.Substring(0, Token.IndexOf("§"));
                    }
                }
                Tokeni = Convert.ToInt32(Token);
            }

            DataSet DS_ValUser = new DataSet();
            DS_ValUser = Obj_Rep.Val_Usuario(txtUsuario.Text.ToUpper(), txtContraseña.Text.ToUpper(), 0, 0, Tokeni);

            if (DS_ValUser.Tables.Count > 0)
            {
                if (Convert.ToInt32(DS_ValUser.Tables[0].Rows[0]["Exito"].ToString()) == 0)
                {
                    throw new Exception("Usuario y/o contraseña incorrectos");
                }
                else
                {
                    if (Convert.ToInt32(DS_ValUser.Tables[0].Rows[0]["Exito"].ToString()) == -1)
                    {
                        throw new Exception("Este equipo no tiene permisos");
                    }
                    else
                    {
                        Session["UsuarioId"] = DS_ValUser.Tables[0].Rows[0]["Id_Usuario"].ToString();
                        Session["compania"] = DS_ValUser.Tables[0].Rows[0]["compania_Id"].ToString();
                        Session["NombreUsuario"] = DS_ValUser.Tables[0].Rows[0]["Nombre"].ToString();
                        Session["Correo"] = DS_ValUser.Tables[0].Rows[0]["Correo"].ToString();
                        Session["cve_sucurs"] = DS_ValUser.Tables[0].Rows[0]["Compania_Id"].ToString();
                        Session["Rol"] = DS_ValUser.Tables[0].Rows[0]["Perfil_Id"].ToString();

                        // Establecer la Imagen Perfil
                        Session["ImagenPerfil"] = "/Imagenes/user.png";
                        try
                        {
                            foreach (string file in Directory.GetFiles(Server.MapPath("~/Imagenes/ImagenPerfil/")))
                            {
                                if (Path.GetFileNameWithoutExtension(file) == Session["UsuarioId"].ToString())
                                {
                                    Session["ImagenPerfil"] = "/Imagenes/ImagenPerfil/" + Path.GetFileName(file);
                                    break;
                                }
                            }
                        }
                        catch { }

                        // Reset Clave
                        if (DS_ValUser.Tables[0].Rows[0]["Inicializar"].ToString() == "1")
                        {
                            this.IdUsuarioLogin = int.Parse(Session["UsuarioId"].ToString());
                            pnlLogin.Visible = false;
                            pnlResetClave.Visible = true;
                        }
                        else
                        {
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            Response.Redirect("Site/Principal.aspx", false);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;

            divError.Attributes.Add("style", "display:block;");
            inicializaControles();
        }
    }

    protected void btnResetClave_Validar(object sender, EventArgs e)
    {
        lblError.Text = string.Empty;
        divError.Attributes.Add("style", "display:none;");

        try
        {
            if (string.IsNullOrEmpty(txtResetClave.Text.Trim()))
                throw new Exception("Capture la Nueva Contraseña");
            else if (string.IsNullOrEmpty(txtResetClaveConfirmar.Text.Trim()))
                throw new Exception("Confirme la Contraseña");
            else if (txtResetClave.Text.Trim() != txtResetClaveConfirmar.Text.Trim())
                throw new Exception("Las Contraseñas no Coinciden");

            string Token = "0";
            int Tokeni = 0;

            if (Session["Tk"] != null)
            {
                Token = Decrypt(Session["Tk"].ToString());

                if (Token.IndexOf("§") >= 0)
                {
                    Token = Token.Substring(Token.IndexOf("§") + 1, Token.Length - Token.IndexOf("§") - 1);

                    if (Token.IndexOf("§") >= 0)
                    {
                        Token = Token.Substring(0, Token.IndexOf("§"));
                    }
                }
                Tokeni = Convert.ToInt32(Token);
            }

            DataSet DS_ValUser = new DataSet();
            DS_ValUser = Obj_Rep.Val_Usuario(string.Empty, txtResetClave.Text.ToUpper(), this.IdUsuarioLogin, 8, Tokeni);

            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.Redirect("Site/Principal.aspx", false);
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;

            divError.Attributes.Add("style", "display:block;");
            inicializaControles();
        }
    }

    private void inicializaControles()
    {
        txtContraseña.Text = string.Empty;
        txtUsuario.Text = string.Empty;

        txtResetClave.Text = string.Empty;
        txtResetClaveConfirmar.Text = string.Empty;
    }

    /// <summary>
    /// Función de prueba, no accesible desde la API para desencriptar el Token
    /// </summary>
    /// <param name="cipherText">Este parámetro espera Texto de un array Base 64 (no el array)</param>
    /// <returns></returns>
    /// 
    public static string Decrypt(string cipherText)
    {
        try
        {
            var keybytes = Encoding.UTF8.GetBytes("PPfiKllDe1+5qTa0");
            var iv = Encoding.UTF8.GetBytes("8080808080808080");

            var encrypted = Convert.FromBase64String(cipherText);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
            return string.Format(decriptedFromJavascript);
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    /// <summary>
    /// Función de prueba, no accesible desde la API para encriptar el Token
    /// </summary>
    /// <param name="Text"> Este parámetro espera texto cómún</param>
    /// <returns></returns>
    public static string Encrypt(string Text)
    {

        var keybytes = Encoding.UTF8.GetBytes("PPfiKllDe1+5qTa0");
        var iv = Encoding.UTF8.GetBytes("8080808080808080");
        var encryption = EncryptStringToBytes(Text, keybytes, iv);
        return Convert.ToBase64String(encryption);
    }


    /// <summary>
    /// Función No accesible por otras clases, funciones base desde donde se corre el proceso de desencripción
    /// </summary>
    /// <param name="cipherText"></param>
    /// <param name="key"></param>
    /// <param name="iv"></param>
    /// <returns></returns>
    private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
    {
        // Check arguments.
        if (cipherText == null || cipherText.Length <= 0)
        {
            throw new ArgumentNullException("cipherText");
        }
        if (key == null || key.Length <= 0)
        {
            throw new ArgumentNullException("key");
        }
        if (iv == null || iv.Length <= 0)
        {
            throw new ArgumentNullException("key");
        }

        // Declare the string used to hold
        // the decrypted text.
        string plaintext = null;

        // Create an RijndaelManaged object
        // with the specified key and IV.
        using (var rijAlg = new RijndaelManaged())
        {
            //Settings
            rijAlg.Mode = CipherMode.CBC;
            rijAlg.Padding = PaddingMode.PKCS7;
            rijAlg.FeedbackSize = 128;

            rijAlg.Key = key;
            rijAlg.IV = iv;

            // Create a decrytor to perform the stream transform.
            var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
            try
            {
                // Create the streams used for decryption.
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {

                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();

                        }

                    }
                }
            }
            catch
            {
                plaintext = "-1";
            }
        }

        return plaintext;
    }

    /// <summary>
    /// Función No accesible por otras clases, funciones base desde donde se corre el proceso de encripción
    /// </summary>
    /// <param name="plainText"></param>
    /// <param name="key"></param>
    /// <param name="iv"></param>
    /// <returns></returns>
    private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
    {
        // Check arguments.
        if (plainText == null || plainText.Length <= 0)
        {
            throw new ArgumentNullException("plainText");
        }
        if (key == null || key.Length <= 0)
        {
            throw new ArgumentNullException("key");
        }
        if (iv == null || iv.Length <= 0)
        {
            throw new ArgumentNullException("key");
        }
        byte[] encrypted;
        // Create a RijndaelManaged object
        // with the specified key and IV.
        using (var rijAlg = new RijndaelManaged())
        {
            rijAlg.Mode = CipherMode.CBC;
            rijAlg.Padding = PaddingMode.PKCS7;
            rijAlg.FeedbackSize = 128;

            rijAlg.Key = key;
            rijAlg.IV = iv;

            // Create a decrytor to perform the stream transform.
            var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

            // Create the streams used for encryption.
            using (var msEncrypt = new MemoryStream())
            {
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (var swEncrypt = new StreamWriter(csEncrypt))
                    {
                        //Write all data to the stream.
                        swEncrypt.Write(plainText);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }
        }

        // Return the encrypted bytes from the memory stream.
        return encrypted;
    }
}