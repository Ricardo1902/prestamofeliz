﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;



public partial class frmLogout : System.Web.UI.Page
{
    cls_Rep Obj_Rep = new cls_Rep();
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet DS_ValUser = new DataSet();
        DS_ValUser = Obj_Rep.Val_Usuario("", "", Convert.ToInt32(Session["UsuarioId"]), 5,0);

        FormsAuthentication.SignOut();
        Session.Clear();
        Session["identificado"] = false;
        Response.Redirect("~/frmLogin.aspx");
    }
}

