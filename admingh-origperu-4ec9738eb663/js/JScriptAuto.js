
function ValidaCuentaBancaria() {

    var op = $('#ctl00_MainContent_CuentaBancaria_CuentaBancaria_TCompa').val();
    if (op == 1) {
        $('#ctl00_MainContent_Banco_Id_TTitulo').hide();
        $('#ctl00_MainContent_Banco_Id_Banco_Id_TCompa').hide();
        $('#ctl00_MainContent_Clabe_TTitulo').hide();
        $('#ctl00_MainContent_Clabe_Clabe_TTextBox').hide();
        $('#ctl00_MainContent_Plastico_TTitulo').hide();
        $('#ctl00_MainContent_Plastico_Plastico_TTextBox').hide();
    } else {
        $('#ctl00_MainContent_Banco_Id_TTitulo').show();
        $('#ctl00_MainContent_Banco_Id_Banco_Id_TCompa').show();
        $('#ctl00_MainContent_Clabe_TTitulo').show();
        $('#ctl00_MainContent_Clabe_Clabe_TTextBox').show();
        $('#ctl00_MainContent_Plastico_TTitulo').show();
        $('#ctl00_MainContent_Plastico_Plastico_TTextBox').show();
    }

}

function ValidaCredInfonavit() {

    var op = $('#ctl00_MainContent_CreditoInfonavit_CreditoInfonavit_TCombo').val();
    if (op == 2) {
        $('#ctl00_MainContent_NumeroCredito_TTitulo').hide();
        $('#ctl00_MainContent_NumeroCredito_NumeroCredito_TTextBox').hide();
        $('#ctl00_MainContent_TipoFactor_TTitulo').hide();
        $('#ctl00_MainContent_TipoFactor_TipoFactor_TTextBox').hide();
        $('#ctl00_MainContent_FactorDescuento_TTitulo').hide();
        $('#ctl00_MainContent_FactorDescuento_FactorDescuento_TTextBox').hide();
    } else {
        $('#ctl00_MainContent_NumeroCredito_TTitulo').show();
        $('#ctl00_MainContent_NumeroCredito_NumeroCredito_TTextBox').show();
        $('#ctl00_MainContent_TipoFactor_TTitulo').show();
        $('#ctl00_MainContent_TipoFactor_TipoFactor_TTextBox').show();
        $('#ctl00_MainContent_FactorDescuento_TTitulo').show();
        $('#ctl00_MainContent_FactorDescuento_FactorDescuento_TTextBox').show();
    }

}

function ValidaTipoRecibo() {
    var Sel_TipoRecibo = $('#ctl00_MainContent_TipoRecibo_Id_TipoRecibo_Id_TCombo').val();
    if (Sel_TipoRecibo == 1) {
        $('#ctl00_MainContent_RecibosDetPDF_TTitulo').hide();
        $('#ctl00_MainContent_RecibosDetPDF_TFileUpload').hide();
        $('#ctl00_MainContent_RecibosDet_TTitulo').show();
        $('#ctl00_MainContent_RecibosDet_Label2').show();
        $('#ctl00_MainContent_RecibosDet_TFileUpload').show();
    } else if (Sel_TipoRecibo == 2) {
        $('#ctl00_MainContent_RecibosDetPDF_TTitulo').show();
        $('#ctl00_MainContent_RecibosDetPDF_TFileUpload').show();
        $('#ctl00_MainContent_RecibosDet_TTitulo').hide();
        $('#ctl00_MainContent_RecibosDet_Label2').hide();
        $('#ctl00_MainContent_RecibosDet_TFileUpload').hide();
    } else {
        $('#ctl00_MainContent_RecibosDetPDF_TTitulo').hide();
        $('#ctl00_MainContent_RecibosDetPDF_TFileUpload').hide();
        $('#ctl00_MainContent_RecibosDet_TTitulo').hide();
        $('#ctl00_MainContent_RecibosDet_Label2').hide();
        $('#ctl00_MainContent_RecibosDet_TFileUpload').hide();
    }
}


function validaSueldo(){
    var op = $('#ctl00_MainContent_Sueldo_Sueldo_TTextBox').val();
    if (op == "") {
        $('#ctl00_MainContent_Sueldo_Sueldo_TTextBox').val(0.00);
    }
}

function validaSueldoPeriodo() {
    var op = $('#ctl00_MainContent_SueldoPeriodo_SueldoPeriodo_TTextBox').val();
    if (op == "") {
        $('#ctl00_MainContent_SueldoPeriodo_SueldoPeriodo_TTextBox').val(0.00);
    }
}

function zIndexRange() {
	var highestZ;
	var lowestZ;
	var onefound = false;
	var divs = document.getElementsByTagName('*');
	if (!divs.length) { return Array(highestZ, lowestZ); }
	for (var i = 0; i < divs.length; i++) {
		if (divs[i].style.position && divs[i].style.zIndex) {
			if (!onefound) {
				highestZ = lowestZ = parseInt(divs[i].style.zIndex);
				onefound = true;
			}
			else {
				var ii = parseInt(divs[i].style.zIndex);
				if (ii > highestZ) { highestZ = ii; }
				if (ii < lowestZ) { lowestZ = ii; }
			}
		}
	}
	return Array(highestZ, lowestZ);
}

window.alert = function (msg, Tipo, objiFrame) {

	var rangeZ = zIndexRange();

	if (objiFrame != undefined) {
		var strSrc = ""; var strWidth = ""; var strHeight = "";
		if ("src" in objiFrame) {
			if (objiFrame.src != "") {
				strSrc = objiFrame.src;
				$('.modalIF').attr("data-src", strSrc);
				$('.modalIF').click();
				//trucos y + trucos
				$(".modal-backdrop").css("z-index", -1);
				$(".modal.modal-wide iframe").css("height", "95%");
				$("#myModalIF").css("padding-top", "0px");
				//trucos y + trucos                
			}
		}
		return;
	}

    if (Tipo == undefined) {
        Tipo = 0;
    }

    if (Tipo == -1)
        return;

    //document.getElementById('contentE').style.backgroundColor = "#FEFEFE"; //"#FF8000";
    if (document.getElementById("ErrorL")) {
    document.getElementById('ErrorL').innerHTML = msg;
    }
    if (Tipo == 0) {
        document.getElementById('TitleE').innerHTML = 'Exito!';
        document.getElementById('IconE').src = '../Imagenes/Alertas/correct.png';
        
    }
    if (Tipo == 1) {

        
        document.getElementById('TitleE').innerHTML = 'Error!';
        document.getElementById('IconE').src = '../Imagenes/Alertas/error.png';

    }
    if (Tipo == 2) {
        document.getElementById('TitleE').innerHTML = 'Atencion!';
        document.getElementById('IconE').src = '../Imagenes/Alertas/warning.png';

    }
    if (Tipo == 3) {
        document.getElementById('TitleE').innerHTML = 'Informacion!';
        document.getElementById('IconE').src = '../Imagenes/Alertas/information.png';

    }
    if (Tipo == 4) {


        document.getElementById('TitleE').innerHTML = 'Excepcion!';
        document.getElementById('IconE').src = '../Imagenes/Alertas/error.png';

    }

	document.getElementById('myModal').style.display = "block";
	$(".modal").css("z-index", rangeZ[0] + 1);
};




function closeError() {

  
        document.getElementById('myModal').style.display = "none";
        if (document.getElementById('TitleE').innerHTML == 'Exito!' || document.getElementById('TitleE').innerHTML == 'Excepcion!')

        { return; window.location.href = "../Principal.aspx"; }

   
}







$(function () {

	$('.modalIF').on('click', function (e) {
		var src = $(this).attr('data-src');
		var width = $(this).attr('data-width') || 640;
		var height = $(this).attr('data-height') || 360;
		var allowfullscreen = $(this).attr('data-video-fullscreen');

		$("#myModalIF iframe").attr({
			'src': src,
			'height': height,
			'width': width,
			'allowfullscreen': ''
		});
	});

	$(".modal-wide").on("show.bs.modal", function () {
		var height = $(window).height() - 50;
		$(this).find(".modal-body").css("max-height", height);
	});

	$('#myModalIF').on('hidden.bs.modal', function () {
		$(this).find('iframe').html("");
		$(this).find('iframe').attr("src", "");
	});

    //    $('#Sueldo').maskMoney();
    $('.MaskLS').maskMoney();
    /*if (document.getElementById('ctl00_MainContent_Sueldo_Sueldo_TTextBox')) //este if es lo que no se como hacer 
    {
        $('.MaskMoney').maskMoney();
    }
    if (document.getElementById("ctl00_MainContent_SueldoPeriodo_SueldoPeriodo_TTextBox")) //este if es lo que no se como hacer 
    {
        $('#ctl00_MainContent_SueldoPeriodo_SueldoPeriodo_TTextBox').maskMoney();
    }*/
    $('Button1').click(function () {
        /*var op = $('#ctl00_MainContent_CuentaBancaria_CuentaBancaria_TCompa').val();
        if (op == 0) {
            alert("Favor de Seleccionar un de las Opciones de Cuenta Bancaria.",1);
        }

        if (document.getElementById('ctl00_MainContent_Sueldo_Sueldo_TTextBox')) //este if es lo que no se como hacer 
        {
            $("#ctl00_MainContent_Sueldo_Sueldo_TTextBox").maskMoney('destroy');
        }
        if (document.getElementById("ctl00_MainContent_SueldoPeriodo_SueldoPeriodo_TTextBox")) //este if es lo que no se como hacer 
        {
            $("#ctl00_MainContent_SueldoPeriodo_SueldoPeriodo_TTextBox").maskMoney('destroy');
        }*/
    });

})
