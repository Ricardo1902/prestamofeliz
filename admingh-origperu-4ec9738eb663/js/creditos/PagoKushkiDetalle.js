﻿$(document).on({
    ajaxStart: function () { $("#divLoader").removeClass("hidden"); },
    ajaxStop: function () { $("#divLoader").addClass("hidden"); }
});

let pagosMasivosDetalle = [];
const dataTableEspanol = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registro(s)",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registro(s) del _START_ al _END_ de un total de _TOTAL_ registro(s)",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Dar clic para ordenar la columna de manera ascendente",
        "sSortDescending": ": Dar clic para ordenar la columna de manera descendente"
    }
};

$(document).ready(function () {
    $("#txIdArchivo").val(getParameterByName("pagoMasivoKushki"));
    CargarPagoMasivoKushkiDetalle();    
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function CargarPagoMasivoKushkiDetalle() {
    let datos = {
        pagoMasivo: Number($("#txIdArchivo").val().trim())
    };
    let parametros = {
        url: "PagoKushkiDetalle.aspx/ObtenerPagoMasivoKushkiDetalle",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: datos
    };
    $.ajax(parametros)
        .done(function (data) {
            let respPagos = data.d;
            if (respPagos !== null) {
                pagosMasivosDetalle = respPagos;
                MostrarPagosMasivosKushkiDetalle();
            } else {
                alert("No fue posible obtener el detalle de los pagos realizados.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de cargar los pagos realizados.");
        });
}

function MostrarPagosMasivosKushkiDetalle() {
    var table = $("#tbPagoMasivoKushkiDetalle").dataTable({
        responsive: true,
        destroy: true,
        data: pagosMasivosDetalle,
        dom: 'Bfrtip',
        buttons: [
            'excelHtml5'
        ],
        scrollX: true,
        columns: [            
            { title: 'IdSolicitud', data: 'IdProducto' },
            { title: 'Monto', data: 'Monto' },
            { title: 'Número de tarjeta', data: 'NoTarjeta' },
            { title: 'Nombre Titular', data: 'NombreTarjetahabiente' },
            { title: 'Telefono', data: 'Telefono' },
            { title: 'Estatus', data: 'Estatus' },
            { title: 'Descripción Estatus', data: 'DescripcionAccion' },
            { title: 'Id de Transaccion', data: 'IdTransaccion' },
            { title: 'Numero de Rastreo', data: 'NumeroRastreo' }
        ],
        columnDefs: [
            { width: "100px", targets: 1, className: "dt-body-right" },
            { width: "150px", targets: 2 },
            { width: "250px", targets: 3 },
            { width: "150px", targets: 4 },
            { width: "150px", targets: 5 },
            { width: "350px", targets: 6 },
            { width: "150px", targets: 7 },
            { width: "150px", targets: 8}
        ],
        language: dataTableEspanol
    });

    $(".dt-buttons button").addClass("btn btn-sm btn-default").prepend($("<i class='fas fa-file-excel' style='margin-right:5px'></i>")).button();
}