﻿$('body').bind('copy paste', function (e) {
});



const dataTableEspanol = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registro(s)",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registro(s) del _START_ al _END_ de un total de _TOTAL_ registro(s)",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Dar clic para ordenar la columna de manera ascendente",
        "sSortDescending": ": Dar clic para ordenar la columna de manera descendente"
    }
};
const dataTableEspanol2 = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registro(s)",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "",
    "sInfoEmpty": "",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Dar clic para ordenar la columna de manera ascendente",
        "sSortDescending": ": Dar clic para ordenar la columna de manera descendente"
    }
};


var archivosCarga = [];

$(document).ready(function () {
    $('#txFechaDesde').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    $("#txFechaDesde").val(moment().add(-5, 'day').format('DD/MM/YYYY'));
    $('#txFechaHasta').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    $("#txFechaHasta").val(moment().format('DD/MM/YYYY'));
    $("#txFechaHasta").data('DateTimePicker').maxDate(moment().format('DD/MM/YYYY'));
    CargarPagoMasivoKushki(); 
    $("#btnCargaArchivo").bind("click", CargarExcel);
    $("#btnRecargar").bind("click", CargarPagoMasivoKushki);
    $("#btnVista").bind("click", VistaPrevia);
    $("#btnDescargar").hide();
    $("#btnValidar").bind("click", Validar);
});

class Excel {
    constructor(content) {
        this.content = content;
    }

    header(){
        return this.content[0];
    }

    rows() {
        return new RowCollection(this.content.slice(1, this.content.length));
    }
}

class RowCollection {

    constructor(rows) {
        this.rows = rows;
    }

    first() {
        return this.rows[0];
    }

    get() {
        return this.rows;
    }

    count() {
        return this.rows.length;
    }
}
class ExcelPorDescripcion {
    constructor(content) {
        this.content = content;
    }

    header() {
        return this.content[0];
    }

    rows() {
        return new RowCollectionPorDescripcion(this.content.slice(1, this.content.length));
    }
}
class RowCollectionPorDescripcion {

    constructor(rows) {
        this.rows = rows;
    }

    first() {
        return new Row(this.rows[0]);
    }

    get(index) {
        return new Row(this.rows[index]);
    }

    count() {
        return this.rows.length;
    }
}
class Row {
    constructor(row) {
        this.row = row;
    }
    IdProducto() {
        return this.row[0].toString();
    }
    Monto() {
        return this.row[1];
    }
    NumeroTarjeta() {
        return this.row[2].toString();
    }
    NombreTarjetahabiente() {
        return this.row[3].toString();
    }
    Telefono() {
        return this.row[4].toString();
    }
       
}
class ExcelPrinter {
    static print(tableId, excel) {
      
        const table = document.getElementById(tableId);
       // $("#tbPagoMasivoKushkiVistaPreliminar").remove();
        //table.querySelector("thead>tr").remove();
        //table.querySelector("tbody").remove();

        //var thea = document.createElement("thead");
        //var bod = document.createElement("tbody");
        //var tr = document.createElement("tr");
        //thea.appendChild(tr);
        //table.appendChild(thea);
        //table.appendChild(bod);

        //table.document.createElement("tbody");
      //  table.closest('tr').remove().remove();
       // const table = tableId;
        table.querySelector("thead>tr").innerHTML = "";
        table.querySelector("tbody").innerHTML = "";
        excel.header().forEach(title => {
            table.querySelector("thead>tr").innerHTML += `<td>${title}</td>`
        });

        for (let index = 0; index < excel.rows().count();index++) {
            const row = excel.rows().get(index);
          //  excel.rows().forEach(row => {
             //   table.querySelector("tbody>tr").innerHTML += `<td>${row}</td><br />`
          //  });
            table.querySelector("tbody").innerHTML += `
            <tr>
                <td>${row.IdProducto()}</td>
                <td>${row.Monto()}</td>
                <td>${row.NumeroTarjeta()}</td>
            </tr>
            `
        }
       
    }
}
async function VistaPrevia() {
    const excelInput = document.getElementById('excel-input')

    if (excelInput.files.length != 0) {
        var nombreDoc = document.getElementById('excel-input').files[0].name;
        const content = await readXlsxFile(excelInput.files[0]);

        var excel = new Excel(content);
        var numDatos = excel.rows().count();
        for (let index = 0; index < excel.rows().count(); index++) {
            row = excel.rows().get(index);
        }

        let parametros = {
            url: "PagosKushki.aspx/MostrarArchivos",
            contentType: "application/json",
            dataType: "json",
            method: 'POST',
            data: JSON.stringify({
                row: row,
                numDatos: numDatos,
                nombreDoc: nombreDoc
            })
        };
        $.ajax(parametros)
            .done(function (data) {
                let respPagos = data.d;
                if (respPagos.length != 0) {
                    archivosCarga = respPagos;
                    MostrarArchivos();
                    $("#btnDescargar").hide();
                } else {
                    $("#tbPagoMasivoKushkiVistaPreliminar").empty();
                    $("#btnDescargar").hide();
                    Swal.fire("Mensaje", "No hay datos en el archivo", "success");
                }
            })
            .fail(function (err) {
                alert("Ocurrió un error al intentar obtener los pagos fallidos.");
            });
    }
    else {
        Swal.fire("Advertencia", "Seleccione un archivo", "warning");
    }

    
}
async  function CargarExcel() {
    var row;

    const excelInput = document.getElementById('excel-input');

    if (excelInput.files.length != 0) {

        const content = await readXlsxFile(excelInput.files[0]);

        var nombreDoc = document.getElementById('excel-input').files[0].name;
        let parametros = {
            url: "PagosKushki.aspx/RecuperarIdComercioMoneda",
            contentType: "application/json",
            dataType: "json",
            method: 'POST'
        };
        $.ajax(parametros)
            .done(function (data) {

                let resultado = data.d;
                let Mone = data.d.Moneda;
                let id = data.d.IdComercio;
                if (resultado =! "") {
                    const kushki = new Kushki({
                        merchantId: id,
                        inTestEnvironment: false,
                    });
                    var TipoMoneda = 0;
                    var moneda = Mone;
                    if (moneda == 'PEN') {
                        TipoMoneda = 1;
                    }
                    var f1 = new Date();
                    dia = '' + f1.getDate();
                    mes = '' + (f1.getMonth() + 1);
                    anio = f1.getFullYear();
                    if (dia.length < 2) dia = '0' + dia;
                    if (mes.length < 2) mes = '0' + mes;
                    var FechaActual = anio + "-" + mes + "-" + dia;
                    var cont = 0;


                    var excel = new Excel(content);
                    var excelPorDecripcion = new ExcelPorDescripcion(content);
                    var numDatos = excel.rows().count();
                    for (let index = 0; index < excel.rows().count(); index++) {
                        row = excel.rows().get(index);
                    }

                    let parametros = {
                        url: "PagosKushki.aspx/Validar",
                        contentType: "application/json",
                        dataType: "json",
                        method: 'POST',
                        data: JSON.stringify({
                            row: row,
                            numDatos: numDatos,
                            nombreDoc: nombreDoc
                        })
                    };
                    
                    $.ajax(parametros)
                        .done(function (data) {
                            let respPagos = data.d;
                            if (respPagos.Error === true) {
                                Swal.fire({
                                    title: 'Procesar pagos',
                                    text: '¿Quieres continuar con el pago de los datos correctos?',
                                    icon: 'question',
                                    showDenyButton: true,
                                    confirmButtonText: 'Continuar',
                                    confirmButtonColor: '#3085d6',
                                    denyButtonColor: '#d33',
                                    denyButtonText: 'No cancelar',
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        let parametros = {
                                            url: "PagosKushki.aspx/ProcesarSoloCorrectos",
                                            contentType: "application/json",
                                            dataType: "json",
                                            method: 'POST',
                                            data: JSON.stringify({
                                                row: row,
                                                numDatos: numDatos,
                                                nombreDoc: nombreDoc,
                                                row2: row
                                            })
                                        };
                                        $.ajax(parametros)
                                            .done(function (data) {
                                                CargarPagoMasivoKushki();
                                                let respPagos = data.d;
                                                var IdPeticion = respPagos.Resultado.IdPeticion;
                                                numDatos = respPagos.Resultado.ndatos;
                                                var datos = respPagos.Resultado.rowdev;
                                                for (let index = 0; index < numDatos; index++) {
                                                    var tarjeta = datos[index][2];
                                                    var monto = datos[index][1];
                                                    var IdProducto = datos[index][0];
                                                    var nombre = datos[index][3];
                                                    var Telefono = datos[index][4];
                                                    const model = {
                                                        Monto: monto,
                                                        NumeroTarjeta: tarjeta,
                                                        fecha: FechaActual,
                                                        IdProducto: IdProducto,
                                                        TipoMoneda: TipoMoneda,
                                                        Moneda: moneda,
                                                        Telefono: Telefono,
                                                        NombreTarjetahabiente: nombre,
                                                        IdPagoMasivo: IdPeticion
                                                    };

                                                    let parametros = {
                                                        url: "PagosKushki.aspx/RecuperarIdSuscripcionKushki",
                                                        contentType: "application/json",
                                                        dataType: "json",
                                                        method: 'POST',
                                                        data: JSON.stringify({
                                                            tarjeta: tarjeta
                                                        })
                                                    };
                                                    $.ajax(parametros)
                                                        .done(function (data) {
                                                            let subscriptionId = data.d;
                                                            if (subscriptionId != "") {


                                                                let parametros = {
                                                                    url: "PagosKushki.aspx/PagarKushki",
                                                                    contentType: "application/json",
                                                                    dataType: "json",
                                                                    method: 'POST',
                                                                    data: JSON.stringify({
                                                                        subscriptionId: subscriptionId,
                                                                        pago: model
                                                                    })
                                                                };
                                                                $.ajax(parametros)
                                                                    .done(function (data) {

                                                                            cont = cont + 1;
                                                                            if (cont == numDatos) {
                                                                                TerminoProcesar(IdPeticion);
                                                                            } else {
                                                                                CargarPagoMasivoKushki();
                                                                            }
                                                                        
                                                                    })
                                                                    .fail(function (err) {
                                                                        alert("Ocurrió un error al momento de cargar los pagos realizados.");
                                                                    });



                                                            }

                                                        })
                                                        .fail(function (err) {
                                                            alert("Ocurrió un error al momento de cargar los pagos realizados.");
                                                        });
                                                }

                                                if (numDatos == 0) {
                                                    TerminoProcesar(IdPeticion);
                                                }
                                            })
                                            .fail(function (err) {
                                                alert("Ocurrió un error al momento de cargar el excel.");
                                            });


                                    }
                                });
                            } else {
                                CargarPagoMasivoKushki();
                                var IdPeticion = respPagos.Resultado.IdPeticion;
                                for (let index = 0; index < excelPorDecripcion.rows().count(); index++) {
                                    var row2 = excelPorDecripcion.rows().get(index);
                                    var tarjeta = row2.NumeroTarjeta();
                                    var monto = row2.Monto();
                                    var nombre = row2.NombreTarjetahabiente();
                                    var Telefono = row2.Telefono();
                                    var IdProducto = row2.IdProducto();
                                    const model = {
                                        Monto: monto,
                                        NumeroTarjeta: tarjeta,
                                        fecha: FechaActual,
                                        IdProducto: IdProducto,
                                        TipoMoneda: TipoMoneda,
                                        Moneda: moneda,
                                        NombreTarjetahabiente: nombre,
                                        Telefono: Telefono,
                                        IdPagoMasivo: IdPeticion
                                    };
                                    let parametros = {
                                        url: "PagosKushki.aspx/RecuperarIdSuscripcionKushki",
                                        contentType: "application/json",
                                        dataType: "json",
                                        method: 'POST',
                                        data: JSON.stringify({
                                            tarjeta: tarjeta
                                        })
                                    };
                                    $.ajax(parametros)
                                        .done(function (data) {
                                            let subscriptionId = data.d;
                                            if (subscriptionId != "") {


                                                let parametros = {
                                                    url: "PagosKushki.aspx/PagarKushki",
                                                    contentType: "application/json",
                                                    dataType: "json",
                                                    method: 'POST',
                                                    data: JSON.stringify({
                                                        subscriptionId: subscriptionId,
                                                        pago: model
                                                    })
                                                };
                                                $.ajax(parametros)
                                                    .done(function (data) {

                                                            cont = cont + 1;
                                                            if (cont == numDatos) {
                                                                TerminoProcesar(IdPeticion);
                                                            } else {
                                                                CargarPagoMasivoKushki();
                                                            }
                                                        
                                                    })
                                                    .fail(function (err) {
                                                        alert("Ocurrió un error al momento de cargar los pagos realizados.");
                                                    });


                                            }

                                        })
                                        .fail(function (err) {
                                            alert("Ocurrió un error al momento de cargar los pagos realizados.");
                                        });

                                }

                            }

                        })
                        .fail(function (err) {
                            alert("Ocurrió un error al momento de cargar el excel.");
                        });

                } else {

                    Swal.fire("Advertencia", "Ocurrió un error al momento de cargar el Id del Comercio", "warning");
                }


            })
            .fail(function (err) {
                alert("Ocurrió un error al momento de cargar el Id del Comercio.");
            });

       
    }
    else {
        Swal.fire("Advertencia", "Seleccione un archivo", "warning");
    }
    

   
}
function TerminoProcesar(IdPeticion) {

    let parametros = {
        url: "PagosKushki.aspx/TerminoProceso",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ IdPagoMasivo: IdPeticion })
    };
    $.ajax(parametros)
        .done(function (data) {
            Swal.fire("Mensaje", "Se termino de procesar los datos", "success");
            CargarPagoMasivoKushki();
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de cargar los pagos realizados.");
        });
}



function CargarPagoMasivoKushki() {
    if (!document.getElementById("txFechaDesde").value || !document.getElementById("txFechaHasta").value) {
        alert("Debe seleccionar un rango de fechas.");
        return false;
    }

    const peticion = {
        FechaDesde: $("#txFechaDesde").val(),
        FechaHasta: $("#txFechaHasta").val()
    };

    let parametros = {
        url: "PagosKushki.aspx/ObtenerPagoMasivoKushki",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ peticion: peticion })
    };
    $.ajax(parametros)
        .done(function (data) {
            let respPagos = data.d;
            if (respPagos !== null) {
                archivosCarga = respPagos;
                MostrarArchivosCarga();
            } else {
                Swal.fire("Mensaje", "No fue posible obtener los pagos realizados. Revise que el rango de fechas sea válido.", "warning");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de cargar los pagos realizados.");
        });
}

function MostrarArchivosCarga() {
    $("#tbArchivosMasivosKushki").dataTable({
        responsive: true,
        destroy: true,
        data: archivosCarga,
        scrollX: true,
        columns: [
            {
                title: '<i class="fas fa-angle-down"></i>', data: 'IdPagoMasivoKushki', "render": function (data, type, row, meta) {
                    if (type === "display") {
                        let strCampo = "";
                        return strCampo;
                    } else { return ""; }
                }
            },
            {
                title: '<i class="fas fa-file-download"></i>', data: 'IdPagoMasivoKushki', "render": function (data, type, row, meta) {
                    if (type === "display") {
                        let strCampo = `<span id='spnDescargaExcelFallido'
                        style = 'color:#EC7063; cursor: pointer; display: ${row.Estatus === "Terminado" ? 'block' : 'none'}'
                        title = 'Descargar Excel de Pagos Fallidos' onclick = 'descargarExcelFallidos(${data}, "${row.NombreArchivo.split(' ').join('_')}")'><span
                        class='fas fa-file-excel'></span></span >`;
                        return strCampo;
                    } else { return ""; }
                }
            },
            {
                title: 'Nombre Archivo', data: 'IdPagoMasivoKushki', "render": function (data, type, row, meta) {
                    if (type === "display") {
                        let strCampo = "<a href='PagoKushkiDetalle.aspx?pagoMasivoKushki=" + data + "'>" + row.NombreArchivo + "</a>";
                        return strCampo;
                    } else { return ""; }
                }
            },            
            { title: 'Fecha de Carga', data: 'FechaRegistro' },
            { title: 'Reg. Tot.', data: 'TotalRegistros' },
            { title: 'Reg. Proc.', data: 'TotalProcesado' },
            { title: 'Reg. Apro.', data: 'TotalAprobados' },
            { title: 'Reg. No Proc.', data: 'TotalNoProcesado' },
            { title: 'Monto Tot.', data: 'MontoTotal' },
            { title: 'Monto Cob.', data: 'MontoCobrado' },
            { title: '% Cobrado', data: 'PorcentajeCobrado' },
            { title: 'Estatus de Carga', data: 'Estatus' },
            { title: 'Ultima Actualización', data: 'UltimaActualizacion' },
            { title: 'Mensajes/Errores', data: 'Mensaje' },
            { title: 'IdArchivo', data: 'IdPagoMasivoKushki' }
        ],
        columnDefs: [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 5 },
            { visible: false, targets: 13 },
             { visible: false, targets: 1 }
        ],
        "order": [[ 14, "desc" ]],
        language: dataTableEspanol,
        "rowCallback": function (row, data, index) {
            if (data.Estatus == "Cargando") {
                $('td', row).css('background-color', 'Yellow');
            } else if(data.Estatus == "En proceso") {
                 $('td', row).css('background-color', 'Green');
            }
        }
    });
}

 async function descargarExcelFallidos() {
    var row;
    var nombreDoc = document.getElementById('excel-input').files[0].name;
   
    const excelInput = document.getElementById('excel-input')

    const content = await readXlsxFile(excelInput.files[0]);

    var excel = new Excel(content);
    var numDatos = excel.rows().count();
    for (let index = 0; index < excel.rows().count(); index++) {
        row = excel.rows().get(index);
    }

    let parametros = {
        url: "PagosKushki.aspx/DescargarExcelFallidos",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({
            row: row,
            numDatos: numDatos,
            nombreDoc: nombreDoc
        })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivoNuevo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);
                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                Swal.fire("Mensaje", "No fue posible obtener los pagos fallidos.", "warning");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener los pagos fallidos.");
        });
}

async function Validar() {
    var row;
   

    const excelInput = document.getElementById('excel-input')

    if (excelInput.files.length != 0) {
        $("#divLoader").removeClass("hidden");
        var nombreDoc = document.getElementById('excel-input').files[0].name;
        const content = await readXlsxFile(excelInput.files[0]);

        var excel = new Excel(content);
        var numDatos = excel.rows().count();
        for (let index = 0; index < excel.rows().count(); index++) {
            row = excel.rows().get(index);
        }

        let parametros = {
            url: "PagosKushki.aspx/MostrarFallidos",
            contentType: "application/json",
            dataType: "json",
            method: 'POST',
            data: JSON.stringify({
                row: row,
                numDatos: numDatos,
                nombreDoc: nombreDoc
            })
        };
        $.ajax(parametros)
            .done(function (data) {
                let respPagos = data.d;
                if (respPagos.length != 0) {
                    archivosCarga = respPagos;
                    MostrarArchivosFallidos();
                    $("#btnDescargar").show();
                    $("#divLoader").addClass("hidden");
                } else {
                    $("#tbPagoMasivoKushkiVistaPreliminar").empty();
                    $("#btnDescargar").hide();
                    $("#divLoader").addClass("hidden");
                    Swal.fire("Mensaje", "Todos los datos son validos", "success");
                }
            })
            .fail(function (err) {
                alert("Ocurrió un error al intentar obtener los pagos fallidos.");
            });

    } else {
        Swal.fire("Advertencia", "Seleccione un archivo", "warning");
    }
    
 }

function MostrarArchivosFallidos() {
    $("#tbPagoMasivoKushkiVistaPreliminar").dataTable({
        responsive: true,
        destroy: true,
        data: archivosCarga,
        columns: [
            { title: 'Linea', data: 'Linea' },
            { title: 'Solicitud', data: 'Solicitud' },
            { title: 'Monto', data: 'Monto' },
            { title: 'NoTarjeta', data: 'NoTarjeta' },
            { title: 'NombreTarjetahabiente', data: 'NombreTarjetahabiente' },
            { title: 'Telefono', data: 'Telefono' },
            { title: 'Mensaje', data: 'Mensaje' }
        ],
        language: dataTableEspanol2
    });
}
function MostrarArchivos() {
    $("#tbPagoMasivoKushkiVistaPreliminar").dataTable({
        responsive: true,
        destroy: true,
        data: archivosCarga,
        columns: [
            { title: 'Linea', data: 'fecha' },
            { title: 'Solicitud', data: 'IdProducto' },
            { title: 'Monto', data: 'Monto' },
            { title: 'NoTarjeta', data: 'NumeroTarjeta' },
            { title: 'NombreTarjetahabiente', data: 'NombreTarjetahabiente' },
            { title: 'Telefono', data: 'Telefono' },
            { title: 'Mensaje',data:'Moneda' }
        ], columnDefs: [
            { visible: false, targets: 0 },
            { visible: false, targets: 6 }
        ],
        language: dataTableEspanol2
    });
}
