﻿$('body').bind('copy paste', function (e) {
    //e.preventDefault(); return false;
});

$(document).on({
    ajaxStart: function () { $("#divLoader").removeClass("hidden"); },
    ajaxStop: function () { $("#divLoader").addClass("hidden"); }
});

const dataTableEspanol = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registro(s)",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registro(s) del _START_ al _END_ de un total de _TOTAL_ registro(s)",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Dar clic para ordenar la columna de manera ascendente",
        "sSortDescending": ": Dar clic para ordenar la columna de manera descendente"
    }
};

var archivosCarga = [];

$(document).ready(function () {
    $('#txFechaDesde').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    $("#txFechaDesde").val(moment().add(-5, 'day').format('DD/MM/YYYY'));
    $('#txFechaHasta').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    $("#txFechaHasta").val(moment().format('DD/MM/YYYY'));
    $("#txFechaHasta").data('DateTimePicker').maxDate(moment().format('DD/MM/YYYY'));
    CargarPagoMasivo();
    $("#btnRecargar").bind("click", CargarPagoMasivo);
});

function CargarPagoMasivo() {
    if (!document.getElementById("txFechaDesde").value || !document.getElementById("txFechaHasta").value) {
        alert("Debe seleccionar un rango de fechas.");
        return false;
    }

    const peticion = {
        FechaDesde: $("#txFechaDesde").val(),
        FechaHasta: $("#txFechaHasta").val()
    };

    let parametros = {
        url: "PagosVisanet.aspx/ObtenerPagoMasivo",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ peticion: peticion })
    };
    $.ajax(parametros)
        .done(function (data) {
            let respPagos = data.d;
            if (respPagos !== null) {
                archivosCarga = respPagos;
                MostrarArchivosCarga();
            } else {
                alert("No fue posible obtener los pagos realizados. Revise que el rango de fechas sea válido.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de cargar los pagos realizados.");
        });
}

function MostrarArchivosCarga() {
    $("#tbArchivosMasivos").dataTable({
        responsive: true,
        destroy: true,
        data: archivosCarga,
        scrollX: true,
        columns: [
            {
                title: '<i class="fas fa-angle-down"></i>', data: 'IdPagoMasivo', "render": function (data, type, row, meta) {
                    if (type === "display") {
                        let strCampo = "";
                        return strCampo;
                    } else { return ""; }
                }
            },
            {
                title: '<i class="fas fa-file-download"></i>', data: 'IdPagoMasivo', "render": function (data, type, row, meta) {
                    if (type === "display") {
                        let strCampo = `<span id='spnDescargaExcelFallido'
                        style = 'color:#EC7063; cursor: pointer; display: ${row.Estatus === "Terminado" ? 'block' : 'none'}'
                        title = 'Descargar Excel de Pagos Fallidos' onclick = 'descargarExcelFallidos(${data}, "${row.NombreArchivo.split(' ').join('_')}")'><span
                        class='fas fa-file-excel'></span></span >`;
                        return strCampo;
                    } else { return ""; }
                }
            },
            {
                title: 'Nombre Archivo', data: 'IdPagoMasivo', "render": function (data, type, row, meta) {
                    if (type === "display") {
                        let strCampo = "<a href='PagoVisanetDetalle.aspx?pagoMasivo=" + data + "'>" + row.NombreArchivo + "</a>";
                        return strCampo;
                    } else { return ""; }
                }
            },            
            { title: 'Fecha de Carga', data: 'FechaRegistro' },
            { title: 'Reg. Tot.', data: 'TotalRegistros' },
            { title: 'Reg. Proc.', data: 'TotalProcesado' },
            { title: 'Reg. Apro.', data: 'TotalAprobados' },
            { title: 'Monto', data: 'MontoTotal' },
            { title: 'Estatus de Carga', data: 'Estatus' },
            { title: 'Ultima Actualización', data: 'UltimaActualizacion' },
            { title: 'Mensajes/Errores', data: 'Mensaje' },
            { title: 'IdArchivo', data: 'IdPagoMasivo' }
        ],
        columnDefs: [
            /*{ width: "250px", targets: 1 },
            { width: "120px", targets: 2 },
            { width: "120px", targets: 5 },
            { width: "140px", targets: 6 },
            { width: "300px", targets: 8 },*/
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 2 },
            { "orderable": false, "targets": 5 },
            { visible: false, targets: 10 }
        ],
        "order": [[ 11, "desc" ]],
        language: dataTableEspanol     
    });
}

function descargarExcelFallidos(IdPagoMasivo, NombreArchivo) {
    let datos = {
        IdPagoMasivo: IdPagoMasivo,
        NombreArchivo: NombreArchivo
    };
    let parametros = {
        url: "PagosVisanet.aspx/DescargarExcelFallidos",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ datos: datos })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivoNuevo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);
                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                alert("No fue posible obtener los pagos fallidos.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener los pagos fallidos.");
        });
}