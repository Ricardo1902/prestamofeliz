﻿$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); $(".modal-pr").appendTo("body"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});

var cartera = [],
    carteraTotales = {},
    gestores = [],
    esAdmin = false,
    tempBusqueda = null;

const prefijoSoles = 'S/ ';
const dataTableGeneral = {
    dom: '<"toolbar"><"dataTables_filter">Brtip',
    destroy: true,
    ordering: false,
    searching: false,
    columns: [
        {
            data: "FechaCredito", title: "F. Cred", searchable: false, exportable: true, render: function (data) {
                if (data) { return moment(data).format('YYYY/MM/DD'); } else return null;
            }
        },
        {
            data: "IdSolicitud", title: "Solicitud", render: function (data, type, row, meta) {
                if (type === 'display') {
                    data = '<a href="../Gestiones/Gestion.aspx?idsolicitud=' + data + '">' + data + '</a>';
                }

                return data;
            }
        },
        { data: "Gestor", title: "Gestor", visible: false, searchable: false },
        { data: "MontoCredito", title: "Monto C.", className: "dt-right dt-nowrap", searchable: false, render: $.fn.dataTable.render.number(',', '.', 2, prefijoSoles) },
        {
            data: "Cuota", title: "Imp. Cuota", className: "dt-right dt-nowrap", searchable: false, render: $.fn.dataTable.render.number(',', '.', 2, prefijoSoles)
        },
        { data: "Empresa", title: "Empresa", searchable: false },
        { data: "Cliente", title: "Cliente", visible: false },
        { data: "DNI", title: "DNI", visible: false },
        { data: "Telefono", title: "Telefono", searchable: false, visible: false },
        { data: "Celular", title: "Celular", searchable: false, visible: false },
        { data: "Correo", title: "Correo", searchable: false, visible: false },
        { data: "TelefonoOficina", title: "Telefono Oficina", searchable: false, visible: false },
        { data: "ExtensionOficina", title: "Anexo Oficina", searchable: false, visible: false },
        { data: "DireccionOficina", title: "Direccion Oficina", searchable: false, visible: false },
        { data: "NumeroExteriorOficina", title: "Numero Exterior Oficina", searchable: false, visible: false },
        { data: "NumeroInteriorOficina", title: "Numero Interior Oficina", searchable: false, visible: false },
        { data: "DependenciaOficina", title: "Dependencia Oficina", searchable: false, visible: false },
        { data: "ReferenciaDomicilio", title: "Referencia Domicilio", searchable: false, visible: false },
        {
            data: "FechaProximoPago", title: "Prox. Pago", searchable: false, render: function (data) {
                if (data) { return moment(data).format('YYYY/MM/DD'); } else return null;
            }
        },
        { data: "RecibosVencidos", title: "Cuotas V", className: "dt-center", searchable: false },
        { data: "SaldoVencido", title: "Saldo V", className: "dt-right dt-nowrap", searchable: false, render: $.fn.dataTable.render.number(',', '.', 2, prefijoSoles) },
        { data: "DiasVencidos", title: "Días V", className: "dt-center", searchable: false },
        { data: "BucketMora", title: "Bucket", className: "dt-center", searchable: false },
        { data: "TipoCredito", title: "Tipo Cred", searchable: false },
        {
            data: "FechaUltPago", title: "Ult. Pago", searchable: false, render: function (data) {
                if (data) { return moment(data).format('YYYY/MM/DD'); } else return null;
            }
        },
        {
            data: "FechaUltGestion", title: "Ult. Ges", searchable: false, render: function (data) {
                if (data) { return moment(data).format('YYYY/MM/DD'); } else return null;
            }
        },
        { data: "ResultadoGestion", title: "Resultado Ges", searchable: false },
        { data: "TieneAcuerdo", title: "Acuerdo Pago", visible: false, searchable: false },
        { data: "TienePromesaPago", title: "Compromiso Pago", visible: false, searchable: false },
        {
            data: null, title: "Indicadores", searchable: false, render: function (data, type, row, meta) {
                let indicadores = '';
                if (type === 'display') {
                    //Indicador para acuerdo de pago vigente
                    if (row.TieneAcuerdo)
                        indicadores += '<span class="tooltip-dt btn btn-xs" data-toggle="tooltip" title="Acuerdo de pago vigente"><span class="fas fa-handshake icono-indicador"></span></span>';
                    //Ejemplo para agregar mas indicadores
                    if (row.TienePromesaPago)
                        indicadores += '<span class="tooltip-dt btn btn-xs" data-toggle="tooltip" title="Compromso de pago vigente"><span class="fas fa-money-bill-wave icono-indicador"></span></span>';
                }

                return indicadores;
            }
        }],
    responsive: {
        details: {
            renderer: function (api, rowIdx, columns) {
                var data = $.map(columns, function (col, i) {
                    return col.hidden ?
                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                        '<td>' + col.title + ':' + '</td> ' +
                        '<td>' + col.data + '</td>' +
                        '</tr>' :
                        '';
                }).join('');

                return data ?
                    $('<table/>').append(data) :
                    false;
            }
        }
    },
    columnDefs: [],
    drawCallback: function () {
        $(".pop-over-dt").popover({
            title: "Comentario",
            placement: "left",
            trigger: "focus",
            animation: true,
            container: "body"
        });
        $(".tooltip-dt").tooltip({ container: "body" });
    },
    createdRow: function (row, data) {
        var pagosVencidos = data.RecibosVencidos;
        if (pagosVencidos > 0 && pagosVencidos < 4) { $(row).css({ 'background-color': '#fff2cc', 'color': '#8a6d3b' }); }
    },
    buttons: {
        csv: {
            extend: 'csv',
            filename: 'Cartera Asignada',
            text: '<i class="fas fa-file-csv export-button export-button-csv"></i>',
            charset: 'UTF-8',
            bom: true,
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28],
                modifier: { search: 'applied' }
            }
        },
        excel: {
            extend: 'excel',
            filename: 'Cartera Asignada',
            text: '<i class="fas fa-file-excel export-button export-button-excel"></i>',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28],
                modifier: { search: 'applied' }
            }
        },
        pdf: {
            extend: 'pdfHtml5',
            filename: 'Cartera Asignada',
            orientation: 'landscape',
            text: '<i class="fas fa-file-pdf export-button export-button-pdf"></i>',
            title: 'Cartera Asignada',
            pageSize: 'TABLOID',
            exportOptions: {
                columns: [0, 1, 3, 4, 6, 7, 9, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28],
                modifier: { search: 'applied' }
            }
        }
    },
    language: español,
    scrollX: false,
    autoWidth: true
};

const dataTableAdmin = {
    ...dataTableGeneral,
    buttons: [
        dataTableGeneral.buttons.csv,
        dataTableGeneral.buttons.excel,
        dataTableGeneral.buttons.pdf
    ]
};

const dataTableGestor = {
    ...dataTableGeneral,
    dom: '<"dataTables_filter">Brtip',
    buttons: [
        {
            ...dataTableGeneral.buttons.csv,
            exportOptions: {
                columns: [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26],
                modifier: { search: 'applied' }
            }
        },
        {
            ...dataTableGeneral.buttons.excel,
            exportOptions: {
                columns: [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26],
                modifier: { search: 'applied' }
            }
        },
        {
            ...dataTableGeneral.buttons.pdf,
            exportOptions: {
                columns: [':visible', 9],
                modifier: { search: 'applied' }
            }
        }
    ]
};

$(document).ready(function () {
    closeNav();
    obtenerCartera(null, null);

    let hoy = moment().locale("es").format("LL");
    let lblFechaActual = $("#lblFechaActual");

    if (hoy && hoy != "Invalid Date") lblFechaActual.text(hoy);
    else lblFechaActual.text("");

    $("#secTodas").bind("click", onClickSeccionAsignaciones);
    $("#secAsignadas").bind("click", onClickSeccionAsignaciones);
    $("#secGestionadas").bind("click", onClickSeccionAsignaciones);
    $("#secPendientes").bind("click", onClickSeccionAsignaciones);

    resetSeleccionAsignaciones();
});

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) return false;
    }
    return true;
}
function poblarControlSeleccion(control, arregloValores, textoMostrar, valorOpcion) {
    try {
        let objeto = $(control);
        if (
            objeto !== null &&
            arregloValores !== null &&
            arregloValores.length > 0
        ) {
            $(objeto).empty();
            if (!arregloValores.find(objeto => objeto[valorOpcion] == 0))
                $(objeto).append(
                    $("<option>", { value: 0 }).text("0 - SELECCIONE UNA OPCION")
                );
            arregloValores.forEach(function (opcion) {
                $(objeto).append(
                    $("<option>", { value: opcion[valorOpcion] }).text(
                        opcion[textoMostrar]
                    )
                );
            });
        }
    } catch (e) {
        console.log(e);
    }
}

function onChangeSelGestor(e) {
    obtenerCartera(tempBusqueda, null);
}
function onKeyUpTxtBusquedaCartera(e) {
    if (e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
            buscarCartera();
        }
    }
}
function onClickBtnBusquedaCartera(e) {
    buscarCartera();
}
function onClickSeccionAsignaciones(e) {

    let control = e.currentTarget;

    if (control && control.id) {
        let divResumen = $(".resumen-ca");
        let grupoAsignaciones = control.attributes.grupo;

        if (divResumen) {
            let secciones = divResumen.children();
            let seccionActiva = false;

            if (secciones && !isEmpty(secciones)) {
                secciones.toArray().forEach(sec => {
                    if (sec.id === control.id) {
                        seccionActiva = toggleSeccionAsignaciones(control.id);
                    } else { $("#" + sec.id).removeClass("active"); }
                });

                if (seccionActiva)
                    obtenerCartera(tempBusqueda, grupoAsignaciones ? grupoAsignaciones.value : null);
                else { toggleSeccionAsignaciones("secTodas"); obtenerCartera(tempBusqueda, null); }
            }
        }
    }
}

function obtenerCartera(busqueda, grupoAsignaciones) {

    let accion = 1, seleccionGestor = 0, configTabla = {};

    if (esAdmin) {
        accion = 0;
        let selGestor = $("#selGestor");
        if (selGestor) { seleccionGestor = selGestor.val(); }
        configTabla = dataTableAdmin;
    } else { configTabla = dataTableGestor; }

    var peticion = {
        Accion: accion,
        IdGestor: seleccionGestor,
        Busqueda: busqueda
    };

    if (grupoAsignaciones != null || grupoAsignaciones != undefined) {
        let grupo = parseInt(grupoAsignaciones);
        if (grupo != null || grupo != undefined) peticion.GrupoAsignaciones = grupoAsignaciones;
    } else {
        resetSeleccionAsignaciones();
    }

    $.ajax({
        type: "POST",
        url: "CarteraAsignada.aspx/ObtenerCarteraAsignada",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ peticion: peticion })
    })
        .done(function (data) {
            if (!isEmpty(data) && !isEmpty(data.d)) {
                cartera = data.d.cartera;
                carteraTotales = data.d.totales;
                poblarTablaCartera({ ...configTabla, data: cartera });
                inicializarTotales();
                mostrarFiltroAdministrador({ idGestor: seleccionGestor });
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener la información correspondiente. " + err.responseJSON.Message);
        });
}
function pruebaObtenerCarteraServerSide(busqueda, grupoAsignaciones) {

    let accion = 1, seleccionGestor = 0, configTabla = {};

    if (esAdmin) {
        accion = 0;
        let selGestor = $("#selGestor");
        if (selGestor) { seleccionGestor = selGestor.val(); }
        configTabla = dataTableAdmin;
    } else { configTabla = dataTableGestor; }

    var peticion = {
        Accion: accion,
        IdGestor: seleccionGestor,
        Busqueda: busqueda
    };

    if (grupoAsignaciones != null || grupoAsignaciones != undefined) {
        let grupo = parseInt(grupoAsignaciones);
        if (grupo != null || grupo != undefined) peticion.GrupoAsignaciones = grupoAsignaciones;
    } else {
        resetSeleccionAsignaciones();
    }

    poblarTablaCartera({
        ...configTabla, ordering: false, serverSide: true, ajax: {
            url: "CarteraAsignada.aspx/ObtenerCarteraAsignadaPaginada",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d,
                    peticion: peticion
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d != undefined) {
                    if (json.d.url.length > 0)
                        window.location = json.d.url;
                    if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                }

                cartera = json.d.data;
                carteraTotales = json.d.totales;
                inicializarTotales();

                return JSON.stringify(json.d);
            }
        }
    });
    mostrarFiltroAdministrador({ idGestor: seleccionGestor });
}
function poblarTablaCartera(config) {
    var table = $("#tCarteraAsignada").DataTable({ ...config });

    $("div.dataTables_filter").html(
        '<div class="form-inline">' +
        '<div class="form-group">' +
        '<label id="lblGestor" for="txtBusquedaCartera">Buscar:</label>' +
        '<div class="input-group">' +
        '<input id="txtBusquedaCartera" name="txtBusquedaCartera" type="text" class="form-control", placholder="IdSolicitud, DNI o Cliente">' +
        '<span class="input-group-btn">' +
        '<button id="btnBusquedaCartera" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '</div>'
    );

    $("#txtBusquedaCartera").val(tempBusqueda);
    $("#txtBusquedaCartera").bind("keyup", onKeyUpTxtBusquedaCartera);
    $("#btnBusquedaCartera").bind("click", onClickBtnBusquedaCartera);

    table.on('buttons-action', function (e, buttonApi, dataTable, node, config) {
    });
}
function inicializarTotales() {
    let regTotales = 0, asignadas = 0, gestionadas = 0, pendientes = 0;

    let bqTodas = $("#bqTodas"),
        bqAsignadas = $("#bqAsignadas"),
        bqGestionadas = $("#bqGestionadas"),
        bqPendientes = $("#bqPendientes");

    if (carteraTotales && !isEmpty(carteraTotales)) {

        regTotales = carteraTotales["CuentasTotales"];
        asignadas = carteraTotales["AsignacionesTotales"];
        gestionadas = carteraTotales["AsignacionesTerminadas"];
        pendientes = carteraTotales["AsignacionesPendientes"];

        if (!regTotales || regTotales < 0)
            regTotales = 0;
        if (!asignadas || asignadas < 0)
            asignadas = 0;
        if (!gestionadas || gestionadas < 0)
            gestionadas = 0;
        if (!pendientes || pendientes < 0)
            pendientes = 0;
    }

    bqTodas.text(regTotales);
    bqAsignadas.text(asignadas);
    bqGestionadas.text(gestionadas);
    bqPendientes.text(pendientes);
    $("#rowTotales").show("fast");
}
function buscarCartera() {
    let busqueda = $("#txtBusquedaCartera").val().trim();
    tempBusqueda = busqueda;

    obtenerCartera(busqueda, null);
}
function toggleSeccionAsignaciones(control) {
    let esActivo = false;
    let seccion = $("#" + control);
    if (seccion && seccion.hasClass("active")) { seccion.removeClass("active"); }
    else { seccion.addClass("active"); esActivo = true; }

    return esActivo;
}
function resetSeleccionAsignaciones() {
    let divResumen = $(".resumen-ca");

    if (divResumen) {
        let secciones = divResumen.children();

        if (secciones && !isEmpty(secciones)) {
            secciones.toArray().forEach(sec => {
                $("#" + sec.id).removeClass("active");
            });
            $("#secTodas").addClass("active");
        }
    }
}

function mostrarFiltroAdministrador(config) {
    if (esAdmin === true) {
        $("div.toolbar").html(
            '<div class="col-md-6">' +
            '<div class="form-group">' +
            '<label id="lblGestor" for="selGestor">Gestor</label>' +
            '<select id="selGestor" name="selGestor" class="selectpicker" data-live-search="true">' +
            '<option value="0" data-thumbnail="images/icon-chrome.png">0 - SELECCIONE UNA OPCIÓN</option>' +
            '</select>' +
            '</div>' +
            '</div>'
        );

        var selGestorNombre = "#selGestor";
        var selGestor = $(selGestorNombre);

        poblarControlSeleccion(selGestorNombre, gestores, "Nombre", "IdGestor");
        selGestor.val(config.idGestor);
        selGestor.selectpicker();
        selGestor.bind('change', onChangeSelGestor);
    }
}
