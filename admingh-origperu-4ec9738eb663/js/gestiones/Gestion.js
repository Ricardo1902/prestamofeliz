﻿$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); $(".modal-pr").appendTo("body"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});

var liquidacionDetalle = [];
var tablaAmortizacion = [];
var pagosRealizados = [];
var notificaciones = [];
var notificacionPromociones = [];
var destinos = [];
var bitacora = [];
var idSolicitud = 0;
var notifActual = 0;
var notifPromoActual = 0;
var notifGestion = {};

var tiposArchivo = [];
var documentosDestino = [];
var servidoresArchivo = [];
var archivosGestion = [];
var tiposGestion = [];
var resultadosGestionPorTipo = [];

//Gestion Telefonica
var tipoContactos = [];
var resultadoGestion = [];
var configAgenda = {};

var idCuenta = 0;
var rolUsuarioElevado = 0;
var cuotaCliente = 0;
var cuotasVencidas = 0;

const TIPO_GESTION_ACUERDO = "Acuerdo de Pago";
const TIPO_GESTION_REESTRUCTURA = "Restructura de Pagos";
const TIPO_GESTION_COMPROMISO = "Compromiso de Pago";

const dataTableEspanol = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registro(s)",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registro(s) del _START_ al _END_ de un total de _TOTAL_ registro(s)",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Da clic para ordenar la columna de manera ascendente",
        "sSortDescending": ": Da clic para ordenar la columna de manera descendente"
    }
};

$(document).ready(function () {
    MostrarNotificaciones();
    MostrarNotificacionPromociones();
    MostrarDestinos();
    llenarListaArchivosGestion();

    //$("#txIdCuenta").val(getParameterByName("IdCuenta"));
    $("#btExpediente").bind("click", OnbtExpedienteClick);
    $("#btVerBalance").bind("click", OnbtVerBalanceClick);
    $("#btVerLiquidacion").bind("click", OnbtVerLiquidacionClick);
    $("#btDescargarNotificacion").bind("click", OnbtDescargarNotificacionClick);
    $("#btActualizarNotificacion").bind("click", OnbtActualizarNotificacionClick);
    $("#btDescargarNotificacionPromocion").bind("click", OnbtDescargarNotificacionPromocionClick);
    $("#btActualizarNotificacionPromocion").bind("click", OnbtActualizarNotificacionPromocionClick);
    $("#selNotificacion").bind("change", onSelNotificacionChange);
    $("#selNotificacionPromocion").bind("change", onSelNotificacionPromocionChange);
    $("#selDestino").bind("change", onSelDestinoChange);
    $("#btBitacora").bind("click", OnbtBitacoraClick);
    $("#btEscanearNotificacion").bind("click", onBtEscanearNotificacionClick);
    $("#btnGuardarGestion").bind('click', onClickBtnGuardarGestion);
    $("#btnGuardarRecordatorio").bind('click', onClickBtnGuardarRecordatorio);
    $("#btLiquidar").bind('click', onClickBtLiquidar);

    //Archivos Gestion
    _archivosGestion.initializarCampos();

    //Envio Notificaciones
    ObtenerEstadoNotificacion();
    ObtenerBitacoraNotificacion();

    poblarControlSeleccion("#selTipoGestion_AltaGestion", tiposGestion, 'Tipo', 'IdTipoGestion');
    poblarControlSeleccion("#selTipoCobro_AltaGestion", tiposCobro, 'TipoCobro', 'IdTipoCobro');

    //Generar Recordatorio
    $('#txtFecha_recordatorio').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    $("#txtFecha_recordatorio").on("dp.hide", configHoraRecordatorio);
    $("#txtFecha_recordatorio").data("DateTimePicker").minDate(moment().format("DD/MM/YYYY"));
    $('#txtHora_recordatorio').datetimepicker({ locale: 'es', format: 'hh:mm a' });

    //Gestion Telefonica
    mostrarResultadosGestion();
    $("#btnGuardarGestTel").click(gestionTelefonica);
    $("#opAgenda").click(agendarFecha);
    $('#txFechaAgendaSel').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    if (configAgenda.HoraMinAgenda != null) {
        let fechaMin = moment();
        if (configAgenda.HoraMaxAgenda == null) configAgenda.HoraMaxAgenda = moment(moment().format("YYYY-MM-DD") + " 23:00").format();
        if (!(moment(configAgenda.HoraMinAgenda).format("HH:mm") > moment().format("HH:mm") < moment(configAgenda.HoraMaxAgenda).format("HH:mm"))) {
            if (moment().format("HH:mm") >= moment(configAgenda.HoraMaxAgenda).format("HH:mm"))
                fechaMin.add(1, 'day');
        }
        $("#txFechaAgendaSel").data("DateTimePicker").minDate(moment(fechaMin).format("DD/MM/YYYY"));
    }
    $("#txFechaAgendaSel").on("dp.hide", horasValidas);
    $('#txHoraAgendaSel').datetimepicker({ locale: 'es', format: 'hh:mm a' });
    if (configAgenda.HoraMaxAgenda != null) {
        $("#txHoraAgendaSel").data("DateTimePicker").maxDate(moment(configAgenda.HoraMaxAgenda).format("hh:mm a"));
    }
    $("#btnAgendar").click(guardarAgenda);
    $("#opLlamar").on("DOMSubtreeModified", function () {
        if ($(this).prop("disabled")) {
            selTelefonoContacto();
        }
    });
    $("#btnSelGestor").click(function () {
        gestorNotificar($("#selGestores").val(), true);
    });
    $("#chSoloAmiAg").click(function () {
        let sel = $("#chSoloAmiAg").prop("checked");
        $("#dGestores").collapse(sel ? "hide" : "show");
        if (sel) {
            configAgenda.Gestores = [];
            gestoresAsignados();
            gestoresNotificar();
        }
    });
    $("#modalFechaAgenda").on("hide.bs.modal", function () {
        validaAgenda();
    });
    tipoTelefonoContactos();
    gestiones();
    gestoresAsignados();
    $("#txFecha_gestion").datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    $("#txFecha_gestion").data("DateTimePicker").minDate(moment().format("DD/MM/YYYY"));
    $("#txFecha_gestion").on("dp.hide", horasValidasGestion);
    $("#txHora_gestion").datetimepicker({ locale: 'es', format: 'HH:mm' });
    $("#txHora_gestion").data("DateTimePicker").minDate(moment().add(5, 'minutes').format("HH:mm"));
    //Fecha Liquidación
    if (rolUsuarioElevado > 0) {
        $('#txFechaLiquidacion_Liquidacion').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
        $("#txFechaLiquidacion_Liquidacion").data("DateTimePicker").minDate(moment(moment()).format("DD/MM/YYYY"));
        $("#txFechaLiquidacion_Liquidacion").data("DateTimePicker").maxDate(moment(moment().add(30, 'day')).format("DD/MM/YYYY"));
        $("#txFechaLiquidacion_Liquidacion").on("dp.hide", diasValidosLiquidacion);
        $('#txFechaLiquidacion_Liquidacion').prop("disabled", false);
        $('#btLiquidar').prop("disabled", false);
    }
    else {
        $('#btLiquidar').hide();
    }
    //Evento para esconder popovers abiertos
    $('body').on('click', function (e) {
        if ($(e.target).data('toggle') !== 'popover'
            && $(e.target).parents('.popover.in').length === 0) {
            $('.pop-over-dt').popover('hide');
        }
    });

    //Fecha Citacion
    $('#txFechaCitacion').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    $("#txFechaCitacion").data("DateTimePicker").minDate(moment(moment()).format("DD/MM/YYYY"));
    $("#txHoraCitacion").datetimepicker({ locale: 'es', format: 'hh:mm a' });
    //Fecha Promocion
    $('#txVigenciaPromocion').datetimepicker({ locale: 'es', format: 'DD/MM/YYYY' });
    $("#txVigenciaPromocion").data("DateTimePicker").minDate(moment(moment()).format("DD/MM/YYYY"));
    //JCGC: SALIMOS A PRODUCCIÓN CON LA PROMOCIÓN DESHABILITADA 03/09/20
    $('#selNotificacionPromocion > option[value="6"]').attr('disabled', 'disabled');
    $("#btnCronograma").click(cronogramaPagos);
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function MostrarNotificaciones() {
    $("#selNotificacion").empty();
    if (notificaciones != null && notificaciones.length > 0) {
        notificaciones.forEach(function (notificacion) {
            $('#selNotificacion')
                .append($('<option>', { value: notificacion.IdNotificacion })
                    .text(notificacion.Notificacion));
        });
    }
}

function MostrarNotificacionPromociones() {
    $("#selNotificacionPromocion").empty();
    if (notificacionPromociones != null && notificacionPromociones.length > 0) {
        notificacionPromociones.forEach(function (notificacionPromocion) {
            $('#selNotificacionPromocion')
                .append($('<option>', { value: notificacionPromocion.IdNotificacion })
                    .text(notificacionPromocion.Notificacion));
        });
    }
}

function MostrarDestinos() {
    $("#selDestino").empty();
    if (destinos != null && destinos.length > 0) {
        destinos.forEach(function (destino) {
            $('#selDestino')
                .append($('<option>', { value: destino.IdDestinoNotificacion })
                    .text(destino.DestinoNotificacion));
        });
    }
}

function OnbtExpedienteClick() {
    ObtenerExpediente();
}

function OnbtVerBalanceClick() {
    ObtenerBalance();
}

function OnbtVerLiquidacionClick() {
    ObtenerLiquidacion('');
}

function ObtenerExpediente() {
    window.open(`frmGestiones.aspx?descarga=1&idSolicitud=${idSolicitud}`, '_ExpdienteDigital', 'width=1000,height=500,resizable=1,scrollbars=yes');
    return false;
}

function ObtenerBalance() {
    let parametros = {
        url: "Gestion.aspx/ObtenerBalance",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { idsolicitud: idSolicitud }
    };

    $.ajax(parametros)
        .done(function (data) {
            if (data.d !== null) {
                balance = data.d;
                tablaAmortizacion = balance.TablaAmortizacion;
                pagosRealizados = balance.PagosRealizados;
                if (tablaAmortizacion.length <= 0)
                    return alert('No se pudo recuperar la información de la tabla de amortización.');

                var tableTA = $("#tTablaAmortizacion").DataTable({
                    destroy: true,
                    searching: false,
                    data: tablaAmortizacion,
                    columns: [
                        { data: "NoRecibo", title: "Recibo" },
                        { data: "FechaRecibo", title: "Fecha" },
                        { data: "CapitalInicial", title: "CapitalInicial" },
                        { data: "CapitalAmortizado", title: "Capital" },
                        { data: "Interes", title: "Interes" },
                        { data: "IGV", title: "IGV" },
                        { data: "GAT", title: "GAT" },
                        { data: "SaldoCapital", title: "SaldoCapital" },
                        { data: "Cuota", title: "Cuota" }
                    ],
                    language: español,
                    scrollX: true,
                    autoWidth: true,
                    order: [[0, "asc"]]
                });
                var tablePR = $("#tPagosRealizados").DataTable({
                    destroy: true,
                    searching: false,
                    data: pagosRealizados,
                    columns: [
                        { data: "Cuota", title: "Cuota" },
                        { data: "Fecha", title: "Fecha" },
                        { data: "CapitalInicial", title: "CapitalInicial" },
                        { data: "Capital", title: "Capital" },
                        { data: "Interes", title: "Interes" },
                        { data: "IGV", title: "IGV" },
                        { data: "Seguro", title: "Seguro" },
                        { data: "GAT", title: "GAT" },
                        { data: "IdEstatus", title: "IdEstatus" },
                        { data: "EstatusClave", title: "EstatusClave" },
                        { data: "EstatusDesc", title: "EstatusDesc" },
                        { data: "SaldoRecibo", title: "SaldoRecibo" },
                        { data: "TotalRecibo", title: "TotalRecibo" },
                        { data: "CanalPagoClave", title: "CanalPagoClave" },
                        { data: "CanalPagoDesc", title: "CanalPagoDesc" },
                        { data: "FechaPago", title: "FechaPago" },
                        { data: "MontoPago", title: "MontoPago" }
                    ],
                    language: español,
                    scrollX: true,
                    autoWidth: true,
                    order: [[1, "asc"]]
                });

                $("#modalVerBalance").modal("show");
                $('#modalVerBalance').css('display', 'block');
                tableTA.columns.adjust().draw();
                tablePR.columns.adjust().draw();
                $("#modalVerBalance").appendTo("body");
            } else {
                alert("No fue posible obtener el balance. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener el balance. " + err.responseJSON.Message);
        });
}

function ObtenerLiquidacion(fechaLiquidacion) {
    const peticion = {
        idSolicitud: parseInt(idSolicitud),
        fechaLiquidacion: fechaLiquidacion
    };
    let parametros = {
        url: "Gestion.aspx/ObtenerLiquidacion",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ peticion: peticion })
    };

    $.ajax(parametros)
        .done(function (data) {
            if (data.d !== null) {
                liquidacion = data.d;
                liquidacionDetalle = liquidacion.DetalleLiquidacion;
                if (liquidacionDetalle.length <= 0)
                    return alert('No se pudo recuperar la información de liquidación.');
                $("#lblIdSolicitud_Liquidacion").text(idSolicitud);
                $("#lblDNI_Liquidacion").text(liquidacion.DNICliente);
                $("#lblCliente_Liquidacion").text(liquidacion.NombreCliente);
                $("#lblMontoCredito_Liquidacion").text(liquidacion.MontoCredito);
                $("#lblCuota_Liquidacion").text(liquidacion.Cuota);
                $("#lblTotalLiquidar_Liquidacion").text(liquidacion.TotalLiquidacion);
                $("#lblFechaCredito_Liquidacion").text(liquidacion.FechaCredito);
                $("#lblFechaPrimerPago_Liquidacion").text(liquidacion.FechaPrimerPago);
                $("#txFechaLiquidacion_Liquidacion").val(liquidacion.FechaLiquidacion);
                var table = $("#tLiquidacion").DataTable({
                    destroy: true,
                    searching: false,
                    data: liquidacionDetalle,
                    columns: [
                        { data: "Recibo", title: "Cuota" },
                        { data: "FechaRecibo", title: "Fecha" },
                        { data: "LiquidaCapital", title: "Capital" },
                        { data: "LiquidaInteres", title: "Interes" },
                        { data: "LiquidaIGV", title: "IGV" },
                        { data: "LiquidaSeguro", title: "Seguro" },
                        { data: "LiquidaGAT", title: "GAT" },
                        { data: "LiquidaTotal", title: "Pagar" }
                    ],
                    language: español,
                    scrollX: true,
                    autoWidth: true,
                    order: [[0, "asc"]]
                });
                $("#modalVerLiquidacion").modal("show");
                $('#modalVerLiquidacion').css('display', 'block');
                table.columns.adjust().draw();
                $("#modalVerLiquidacion").appendTo("body");
            } else {
                alert("No fue posible obtener la liquidación. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener la liquidación. " + err.responseJSON.Message);
        });
}

function OnbtDescargarNotificacionClick() {
    if (parseInt($("#selDestino").val()) <= 0) {
        alert("Debe seleccionar una notificación y destino válidos.");
        return false;
    }
    if ($("#selNotificacion").val() === "5" && (!document.getElementById("txFechaCitacion").value || !document.getElementById("txHoraCitacion").value)) {
        alert("Debe seleccionar una fecha y hora para la cita.");
        return false;
    }
    const notificacion = {
        IdUsuario: 0,
        IdSolicitud: parseInt(idSolicitud),
        Clave: notificaciones.filter(x => x.IdNotificacion == $("#selNotificacion").val())[0].Clave,
        IdDestino: parseInt($("#selDestino").val()),
        FechaCitacion: $("#txFechaCitacion").val(),
        HoraCitacion: $("#txHoraCitacion").val(),
        Actualizar: false,
        Correo: $("#chkEnvioCorreo").prop('checked'),
        Courier: $("#chkCourier").prop('checked')
    };
    let parametros = {
        url: "Gestion.aspx/GenerarNotificacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ notificacion: notificacion })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);
                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                alert("No fue posible obtener la notificación.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de intentar obtener la notificación. " + err.responseJSON.Message);
        });
}

function OnbtActualizarNotificacionClick() {
    if (parseInt($("#selDestino").val()) <= 0) {
        alert("Debe seleccionar una notificación y destino válidos.");
        return false;
    }
    if ($("#selNotificacion").val() === "5" && (!document.getElementById("txFechaCitacion").value || !document.getElementById("txHoraCitacion").value)) {
        alert("Debe seleccionar una fecha y hora para la cita.");
        return false;
    }
    const notificacion = {
        IdUsuario: 0,
        IdSolicitud: parseInt(idSolicitud),
        Clave: notificaciones.filter(x => x.IdNotificacion == $("#selNotificacion").val())[0].Clave,
        IdDestino: parseInt($("#selDestino").val()),
        FechaCitacion: $("#txFechaCitacion").val(),
        HoraCitacion: $("#txHoraCitacion").val(),
        Actualizar: true,
        Correo: $("#chkEnvioCorreo").prop('checked'),
        Courier: $("#chkCourier").prop('checked')
    };
    let parametros = {
        url: "Gestion.aspx/GenerarNotificacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ notificacion: notificacion })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);
                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                alert("No fue posible obtener la notificación.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de intentar obtener la notificación.");
        });
}

function OnbtDescargarNotificacionPromocionClick() {
    if (parseInt($("#selDestino").val()) <= 0) {
        alert("Debe seleccionar un destino válido.");
        return false;
    }
    if ($("#selNotificacionPromocion").val() === "1" && (!document.getElementById("txDescuentoPromocion").value || !document.getElementById("txVigenciaPromocion").value)) {
        alert("Debe seleccionar un descuento y una vigencia para la promoción.");
        return false;
    }
    const notificacion = {
        IdUsuario: 0,
        IdSolicitud: parseInt(idSolicitud),
        Clave: notificacionPromociones.filter(x => x.IdNotificacion == $("#selNotificacionPromocion").val())[0].Clave,
        IdDestino: parseInt($("#selDestino").val()),
        DescuentoDeuda: parseFloat($("#txDescuentoPromocion").val()),
        FechaVigencia: $("#txVigenciaPromocion").val(),
        Actualizar: false,
        Correo: $("#chkEnvioCorreo").prop('checked'),
        Courier: $("#chkCourier").prop('checked')
    };
    let parametros = {
        url: "Gestion.aspx/GenerarNotificacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ notificacion: notificacion })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);
                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                alert("No fue posible obtener la promoción.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de intentar obtener la promoción.");
        });
}

function OnbtActualizarNotificacionPromocionClick() {
    if (parseInt($("#selDestino").val()) <= 0) {
        alert("Debe seleccionar un destino válido.");
        return false;
    }
    if ($("#selNotificacionPromocion").val() === "1" && (!document.getElementById("txDescuentoPromocion").value || !document.getElementById("txVigenciaPromocion").value)) {
        alert("Debe seleccionar un descuento y una vigencia para la promoción.");
        return false;
    }
    const notificacion = {
        IdUsuario: 0,
        IdSolicitud: parseInt(idSolicitud),
        Clave: notificacionPromociones.filter(x => x.IdNotificacion == $("#selNotificacionPromocion").val())[0].Clave,
        IdDestino: parseInt($("#selDestino").val()),
        DescuentoDeuda: parseFloat($("#txDescuentoPromocion").val()),
        FechaVigencia: $("#txVigenciaPromocion").val(),
        Actualizar: true,
        Correo: $("#chkEnvioCorreo").prop('checked'),
        Courier: $("#chkCourier").prop('checked')
    };
    let parametros = {
        url: "Gestion.aspx/GenerarNotificacion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ notificacion: notificacion })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var file = new Blob([byteArray], { type: resp.Tipo + ';base64' });
                    let datos = window.URL.createObjectURL(file);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivo;
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);
                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                alert("No fue posible obtener la promoción.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de intentar obtener la promoción.");
        });
}

function ObtenerEstadoNotificacion() {
    let parametros = {
        url: "Gestion.aspx/ObtenerEstadoNotificacion",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { idsolicitud: idSolicitud }
    };

    $.ajax(parametros)
        .done(function (data) {
            if (data.d !== null) {
                let resNotificacion = data.d;
                notifActual = resNotificacion.IdNotificacion;
                $("#selNotificacion").val(resNotificacion.IdNotificacion);
                $("#selDestino").val(resNotificacion.Destino === 0 ? -1 : resNotificacion.Destino);
                $("#chkEnvioCorreo").prop('checked', (resNotificacion.Correo ? true : false));
                $("#chkCourier").prop('checked', resNotificacion.EsCourier);
                RevisaNotificacionCitacion();
                RevisaNotificacionPromocionReduccionMora();
                ValidarPermisosImpresion();
            } else {
                alert("No fue posible obtener el estado de envios de notificación. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener el estado de envios de notificación. " + err.responseJSON.Message);
        });
}

function ValidarPermisosImpresion() {
    // BLOQUEAR NOTIFICACIONES POSTERIORES A LA NOTIFICACION ACTUAL
    //if (typeof notificaciones !== "undefined" && notificaciones !== null && notificaciones.length !== null && notificaciones.length > 0) {
    //    let ordenActual = notificaciones.filter(x => x.IdNotificacion == notifActual)[0].Orden;
    //    let opcsNotifsBloquear = notificaciones.filter(x => x.Orden > ordenActual).map(x => x.IdNotificacion);
    //    $("#selNotificacion > option").each(function () {
    //        if (jQuery.inArray(parseInt(this.value), opcsNotifsBloquear) !== -1) {
    //            $('#selNotificacion > option[value="' + this.value + '"]').attr('disabled', 'disabled');
    //            //TODO: NO SE VE REFLEJADO EL CURSOR not-allowed Y NO ACTUALIZA
    //            $('#selNotificacion > option[value="' + this.value + '"]').css('cursor', 'not-allowed');
    //        }
    //    });
    //}
}

function onSelNotificacionChange() {
    if ($("#selNotificacionPromocion").val() !== "5") {
        CargaDatosNotificacion($("#selNotificacion").val());
    }
    RevisaNotificacionCitacion();
    ValidarPermisosImpresion();
}

function onSelNotificacionPromocionChange() {
    RevisaNotificacionPromocionReduccionMora();
}

function RevisaNotificacionCitacion() {
    if ($("#selNotificacion").val() === "5") {
        if (parseInt($("#selDestino").val()) > 0) {
            $("#dNotificacionCitacion").show();
            CargaDatosNotificacion($("#selNotificacion").val());
        }
    }
    else
        $("#dNotificacionCitacion").hide();
}

function RevisaNotificacionPromocionReduccionMora() {
    if ($("#selNotificacionPromocion").val() === "6") {
        if (parseInt($("#selDestino").val()) > 0) {
            $("#dNotificacionPromocion").show();
            CargaDatosNotificacion($("#selNotificacionPromocion").val());
        }
    }
    else
        $("#dNotificacionPromocion").hide();
}

function CargaDatosNotificacion(idnotificacion) {
    let parametros = {
        url: "Gestion.aspx/ObtenerDatosNotificacion",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { idsolicitud: idSolicitud, idnotificacion: idnotificacion, iddestino: parseInt($("#selDestino").val()) }
    };

    $.ajax(parametros)
        .done(function (data) {
            if (data.d !== null) {
                let resNotificacion = data.d;
                switch (idnotificacion) {
                    case "5":
                        $("#txFechaCitacion").val(resNotificacion.FechaCitacion);
                        $("#txHoraCitacion").val(resNotificacion.HoraCitacion);
                        break;
                    case "6":
                        $("#txDescuentoPromocion").val(resNotificacion.DescuentoDeuda);
                        $("#txVigenciaPromocion").val(resNotificacion.FechaVigencia);
                        break;
                    default:
                        break;
                }
                $("#chkEnvioCorreo").prop('checked', (resNotificacion.Correo ? true : false));
                $("#chkCourier").prop('checked', resNotificacion.EsCourier);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener los datos de la citación. " + err.responseJSON.Message);
        });
}

function onSelDestinoChange() {
    if ($("#selNotificacionPromocion").val() !== "5" || $("#selNotificacionPromocion").val() !== "6") {
        CargaDatosNotificacion($("#selNotificacion").val());
    }
    RevisaNotificacionCitacion();
    RevisaNotificacionPromocionReduccionMora();
}

function ValidaMonto(obj) {
    let valido = false;
    valido = (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46;
    if (event.charCode == 46) {
        valido = !$(obj).val().includes(".");
    }
    if (valido && $(obj).val().includes(".")) {
        let str = $(obj).val().split('.');
        valido = !(str[1].length + 1 > 2);
    }
    return valido;
}

function OnbtBitacoraClick() {
    ObtenerBitacoraNotificacion();
    if (bitacora.length <= 0)
        return false;
    $("#modalBitacoraNotificacion").modal("show");
    var table = $('#tBitacora').DataTable();
    $('#modalBitacoraNotificacion').css('display', 'block');
    table.columns.adjust().draw();
    $("#modalBitacoraNotificacion").appendTo("body");
}

function ObtenerBitacoraNotificacion() {
    let parametrosBitacora = {
        url: "Gestion.aspx/ObtenerBitacoraNotificacion",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { idsolicitud: idSolicitud }
    };

    $.ajax(parametrosBitacora)
        .done(function (data) {
            if (data.d !== null) {
                bitacora = data.d;
                $("#tBitacora").DataTable({
                    destroy: true,
                    searching: false,
                    data: bitacora,
                    columns: [
                        { data: "Notificacion", title: "Notificacion" },
                        { data: "Destino", title: "Destino" },
                        { data: "Accion", title: "Acción" },
                        { data: "Usuario", title: "Usuario" },
                        { data: "Fecha", title: "Fecha" }
                    ],
                    language: español,
                    scrollX: true,
                    autoWidth: true,
                    order: [[4, "des"]]
                });
            } else {
                alert("No fue posible obtener la bitácora de notificaciones, o no hay movimientos registrados. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener la bitácora de notificaciones. " + err.responseJSON.Message);
        });
}

function onBtEscanearNotificacionClick() {
    var modalArchivos = $("#modalArchivosGestion");
    modalArchivos.appendTo("body");
    modalArchivos.modal("show");
}


function onClickBtnIngresarGestion() {
    limpiarCamposNuevaGestion();
    var modalNuevaGestion = $("#modalNuevaGestion");
    modalNuevaGestion.appendTo("body");
    modalNuevaGestion.modal("show");
    $(".tipoCobro_gestion").hide();
    $(".monto_gestion").hide();
    $(".fecha_gestion").hide();
    $(".resultado_gestion").hide();
}
function onChangeSelTipoGestionAlta() {
    $(".tipoCobro_gestion").hide();
    $(".monto_gestion").hide();
    $(".fecha_gestion").hide();
    $(".resultado_gestion").hide();

    var seleccionTipo = parseInt($("#selTipoGestion_AltaGestion").val());
    var nombreTipo = $("#selTipoGestion_AltaGestion option:selected").text();

    if (nombreTipo) {
        switch (nombreTipo) {
            case TIPO_GESTION_REESTRUCTURA:
                location.href = '/site/Gestiones/frmPeticionReestructura.aspx?idsolicitud=' + idSolicitud;
                return;
            case TIPO_GESTION_ACUERDO:
                $("body").addClass("loading"); $(".modal-pr").appendTo("body");
                location.href = '/site/Gestiones/AcuerdoPago.aspx?idsolicitud=' + idSolicitud;
                return;
            case "Compromiso de Pago":
                $(".fecha_gestion").show();
                $('.hora-gestion').hide();
                break;
        }
    }

    if (tipoGestionDom != undefined && tipoGestionDom != null && tipoGestionDom.includes(seleccionTipo)) {
        $(".monto_gestion").show();
        $(".fecha_gestion").show();
        $('.hora-gestion').show();
    } else if (tiposGestionPromesa != undefined && tiposGestionPromesa != null && tiposGestionPromesa.includes(seleccionTipo)) {
        $(".tipoCobro_gestion").show();
        if (nombreTipo != TIPO_GESTION_COMPROMISO)
            $(".monto_gestion").show();
        $(".fecha_gestion").show();
    }
    else {
        $(".resultado_gestion").show();
        obtenerResultadosGestionPorTipo(seleccionTipo, function (data) {
            poblarControlSeleccion("#selResultadoGestion_AltaGestion", data, "Descripcion", "IdResultado");
        });
    }
}
function onClickBtnGuardarGestion() {
    var seleccionTipo = parseInt($("#selTipoGestion_AltaGestion").val());
    var nombreTipo = $("#selTipoGestion_AltaGestion option:selected").text();

    if (!seleccionTipo) {
        alert("Por favor, seleccione una descripcion de gestion.");
        return;
    }

    let peticion = {
        IdCuenta: idCuenta,
        IdTipoGestion: seleccionTipo
    };

    if (tipoGestionDom != undefined && tipoGestionDom != null && tipoGestionDom.includes(seleccionTipo)) {

        let monto = parseFloat($("#txtMonto_AltaGestion").val());
        if (!monto || monto <= 0) { alert("Por favor, ingrese un monto a domiciliar."); return; }
        peticion.MontoDomiciliar = monto;

        let fechaDomiciliar = moment($("#txFecha_gestion").val(), "DD-MM-YYYY").format("DD-MM-YYYY");
        if (fechaDomiciliar == "") { alert("Por favor, Capture la fecha a domiciliar."); return; }
        let horaDomiciliar = $("#txHora_gestion").val();
        if (horaDomiciliar == "") { alert("Por favor, Capture la hora a domiciliar."); return; }
        let fechaHoraDom = moment(fechaDomiciliar + " " + horaDomiciliar, "DD/MM/YYYY HH:mm a");
        if (fechaHoraDom._d == "Invalid Date") { alert("La fecha y hora de domiciliación no son validos"); return; }
        peticion.FechaDomiciliar = fechaHoraDom.format();
    } else if (tiposGestionPromesa != undefined && tiposGestionPromesa != null && tiposGestionPromesa.includes(seleccionTipo)) {

        if (nombreTipo == TIPO_GESTION_COMPROMISO && cuotasVencidas != 1) { alert("Solo es posible registrar un compromiso de pago cuando el cliente debe 1 cuota."); return; }

        let seleccionTipoCobro = parseInt($("#selTipoCobro_AltaGestion").val());
        if (!seleccionTipoCobro) { alert("Por favor, seleccione un medio de cobro"); return; }
        peticion.IdTipoCobro = seleccionTipoCobro;

        let monto = parseFloat($("#txtMonto_AltaGestion").val());
        if (nombreTipo == TIPO_GESTION_COMPROMISO) { monto = cuotaCliente; }
        if (!monto || monto <= 0) { alert("Por favor, ingrese un monto."); return; }
        peticion.MontoPromesa = monto;

        let fechaCompromiso = moment($("#txFecha_gestion").val(), "DD-MM-YYYY").format();
        if (fechaCompromiso == "") { alert("Por favor, capture la fecha."); return; }
        peticion.FechaPromesa = fechaCompromiso;
    }
    else {
        var seleccionResultado = parseInt($("#selResultadoGestion_AltaGestion").val());
        if (!seleccionResultado) {
            alert("Por favor, seleccione un resultado de gestion.");
            return;
        }
        peticion.IdResultadoGestion = seleccionResultado;
    }

    guardarGestion(peticion, function (data) {
        var respuestaGestion = parseInt(data);
        if (respuestaGestion > 0) {
            $('#tgestiones').DataTable().ajax.reload();
            $("#modalNuevaGestion").modal("hide");
            limpiarCamposNuevaGestion();
        }
    });
}
function onClickBtnGuardarRecordatorio() {

    var peticion = {
        IdGestion: notifGestion.IdGestion,
        IdTipoGestion: notifGestion.IdTipoGestion,
        IdTipoNotificacion: notifGestion.IdTipoNotificacion
    };

    let checkEmail = $("#chkEmail").prop("checked");
    //TODO: Agregar los demas medios a la validacion
    if (!checkEmail) { alert("Por favor, seleccione un medio de envío."); return; }

    let fechaRecordatorio = $("#txtFecha_recordatorio").val();
    if (fechaRecordatorio == "" || fechaRecordatorio == undefined || fechaRecordatorio == null) { alert("Por favor, capture la fecha del recordatorio."); return; }

    let horaRecordatorio = $("#txtHora_recordatorio").val();
    if (horaRecordatorio == "" || horaRecordatorio == undefined || horaRecordatorio == null) { alert("Por favor, capture la hora del recordatorio."); return; }
    peticion.FechaRecordatorio = fechaRecordatorio;

    let fechaFinal = moment(fechaRecordatorio + " " + horaRecordatorio, "DD/MM/YYYY HH:mm a");
    if (fechaFinal._d == "Invalid Date") { alert("La fecha y hora no son validos"); return; }

    peticion.FechaEnviar = fechaFinal.format();
    peticion.EnviarCliente = false;

    guardarRecordatorioGestion(peticion, function (data) {
        if (!data.Error) {
            $('#tgestiones').DataTable().ajax.reload();
            $("#modalRecordatorio").modal("hide");
            limpiarCamposRecordatorio();
            notifGestion = {};
        } else {
            alert(data.MensajeOperacion);
        }
    });

}

function limpiarCamposNuevaGestion() {
    $("#selTipoGestion_AltaGestion").val(0);
    $("#selTipoCobro_AltaGestion").val(0);
    $("#txtMonto_AltaGestion").val("");
    $("#selResultadoGestion_AltaGestion").val(0);
    $("#txtMonto_AltaGestion").val("");
    $("#txFecha_gestion").val("");
    $("#txHora_gestion").val("");
}
function limpiarCamposRecordatorio() {
    $("#chkEmail").prop("checked", false);
    $("#chkSMS").prop("checked", false);
    $("#chkWhatsapp").prop("checked", false);
    $("#ChkIVR").prop("checked", false);
    $("#txtFecha_recordatorio").val("");
    $("#txtHora_recordatorio").val("");
}

function obtenerResultadosGestionPorTipo(idTipoGestion, callback) {
    let parametros = {
        url: "Gestion.aspx/ObtenerResultadoGestionPorTipo",
        contentType: "application/json",
        dataType: "json",
        method: 'GET',
        data: { idTipoGestion: idTipoGestion }
    };

    $.ajax(parametros)
        .done(function (data) {
            if (data.d !== null) {
                callback(data.d);
            } else {
                alert("No fue posible obtener los resultados de gestion. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar obtener los resultados de gestion. " + err.responseJSON.Message);
        });
}
function guardarGestion(peticion, callback) {
    let parametros = {
        url: "Gestion.aspx/InsertarGestion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ gestion: peticion })
    };

    $.ajax(parametros)
        .done(function (data) {
            if (data.d !== null) {
                callback(data.d);
            } else {
                alert("No fue posible guardar la nueva gestión. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar guardar la nueva gestion. " + err.responseJSON.Message);
        });
}
function guardarRecordatorioGestion(peticion, callback) {
    let parametros = {
        url: "Gestion.aspx/RegistrarNotificacionGestion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ peticion: peticion })
    };

    $.ajax(parametros)
        .done(function (data) {
            if (data.d !== null) {
                callback(data.d);
            } else {
                alert("No fue posible generar el recordatorio. " + data.MensajeOperacion);
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al intentar generar el recordatorio. " + err.responseJSON.Message);
        });
}

//Gestion Telefonica
function mostrarResultadosGestion() {
    $("#selEstadoLlamada").empty();
    if (resultadoGestion != null && resultadoGestion.length > 0) {
        resultadoGestion.forEach(function (resultado) {
            $('#selEstadoLlamada')
                .append($('<option>', { value: resultado.IdResultado })
                    .text(resultado.Descripcion));
        });
    }
}
function tipoTelefonoContactos() {
    let qs = new URLSearchParams(window.location.search);
    let solicitud = qs.has("idsolicitud") ? qs.get("idsolicitud") : 0;
    $('#selTelefono').empty();
    let parametros = {
        url: "Gestion.aspx/TipoTelefonoContactos",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'GET',
        data: { idSolicitud: solicitud, idTipoContacto: 0 }
    };
    $("#tContactos tbody").empty();
    $.ajax(parametros)
        .done(function (respuesta) {
            let resp = respuesta.d;
            if (resp != undefined) {
                if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                if (resp.contactos != null) {
                    resp.contactos.forEach((contacto) => {
                        let extRef = [];
                        let reg = `<tr>`
                        reg += `<td>${contacto.Telefono}</td><td>${contacto.TipoTelefono}${contacto.Parentesco != null && contacto.Parentesco !== "" ? " / " + contacto.Parentesco : ""}</td>`;
                        reg += `<td>`;
                        if (contacto.Extension != null && contacto.Extension !== "") extRef.push(`Ext - ${contacto.Extension}`);
                        if (contacto.NombreContacto != null && contacto.NombreContacto !== "") extRef.push(`${contacto.NombreContacto}`);
                        if (extRef.length > 0) {
                            reg += extRef.join(" / ");
                        }
                        reg += `</td><td class="selTelefono"><input type="checkbox" value="${contacto.Telefono}" onclick="selContacto(this)" class="checkbox-inline" /></td></tr>`;
                        $("#tContactos tbody").append(reg);
                    });
                }
            } else {
                alert("Ocurrió un error al momento de actualizar los datos.");
            }
        })
        .fail(function (error) {
            alert("Ocurrió un error al momento actualizar los datos.");
        });
}
function selTelefonoContacto() {
    let activo = $("#tContactos input[type=checkbox]:checked").val() > 0 ? true : false;
    $("#selEstadoLlamada").prop("disabled", !activo);
    $("#textObservacion").prop("disabled", !activo);
    $("#opAgenda").prop("disabled", !activo);
    $("#btnGuardarGestTel").prop("disabled", !activo);
}
function gestionTelefonica() {
    let gestionTel = {
        IdCuenta: idCuenta,
        IdTipoContacto: 0,
        IdResultadoGestion: $("#selEstadoLlamada").val()
    }
    let agendar = {};
    agendar.Telefono = $("#tContactos input[type=checkbox]:checked").val();
    agendar.Comentario = $("#textObservacion").val();
    $("#tContactos tbody tr").map(function (i, o) {
        let cols = $(o).find("td");
        if (cols != null) {
            if (cols[0].innerHTML == agendar.Telefono) {
                agendar.Tipo = cols[1].innerHTML;
                agendar.AnexoReferencia = cols[2].innerHTML;
            }
        }
    });
    if ($("#txFechaAgenda").val() != "") {
        agendar.Fch_DevLlamada = moment($("#txFechaAgenda").val(), "DD-MM-YYYY hh:mm a").format();
    }
    if (gestionTel.IdResultadoGestion <= 0) { alert("Capture el resultado de la llamada"); return };
    if ($("#textObservacion").val() == "") { alert("Capture un comentario"); return; }
    gestionTel.Comentario = `Llamada al teléfono: ${agendar.Telefono} - ${agendar.AnexoReferencia}. ${$("#textObservacion").val()}`;
    if (configAgenda.Gestores != null && configAgenda.Gestores.length > 0)
        agendar.IdGestoresNotificar = configAgenda.Gestores;
    let parametros = {
        url: "Gestion.aspx/GestionTelefonica",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ gestionTel: gestionTel, devolverLlamada: agendar })
    };
    $.ajax(parametros)
        .done(function (respuesta) {
            let resp = respuesta.d;
            if (resp != undefined) {
                if (resp.url != null && resp.url.length > 0) window.location = resp.url;
                if (resp.mensaje != null && resp.mensaje.length > 0) alert(resp.mensaje);
                nuevoGestionTel();
            } else {
                alert("Ocurrió un error al momento de guardar la gestión telefónica");
            }
        })
        .fail(function (error) {
            alert("Ocurrió un error al momento intentar guardar la gestión telefónica. Vuela a intentar.");
        });
}
function nuevoGestionTel() {
    $("#txFechaAgenda").val("");
    $("#selEstadoLlamada").val("0");
    $("#textObservacion").val("");
    $("#txFechaAgendaSel").val("");
    $("#txHoraAgendaSel").val("");
    $("#txCometnarioAg").val("");
    $("#tContactos input[type=checkbox]:checked").map(function (i, o) {
        $(o).prop("checked", false);
    });
    $("#opLlamar").prop("disabled", true);
    configAgenda.Gestores = [];
    gestoresAsignados();
    gestoresNotificar();
    $('#tgestiones').DataTable().ajax.reload();
}
function agendarFecha() {
    $("#modalFechaAgenda").modal('show').appendTo("body");
    $("#modalFechaAgenda").css("z-index", 9999);
}
function gestiones() {
    var table = $('#tgestiones').DataTable({
        dom: '<"toolbar">fBrtip',
        destroy: true,
        searching: false,
        ordering: false,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "Gestion.aspx/CargarGestiones",
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                return JSON.stringify({
                    model: d,
                    idCuenta: idCuenta
                });
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.d != undefined) {
                    if (json.d.url.length > 0)
                        window.location = json.d.url;
                    if (json.d.data == undefined || json.d.data == null) json.d.data = [];
                }
                return JSON.stringify(json.d);
            }
        },
        columns: [
            { data: "IdGestion", title: "Descripción de Gestión", visible: false },
            { data: "idTipoGestion", title: "Tipo Gestion", visible: false },
            { data: "Tipo", title: "Tipificación" },
            {
                data: "fch_Gestion", title: "Fecha", width: "82px", render: function (data) {
                    if (data) {
                        return moment(data).format('DD/MM/YY HH:mm');
                    }
                    else return null;
                }
            },
            { data: "Monto", title: "Monto" },
            { data: "ResultadoGestion", title: "Resultado de gestión" },
            {
                data: "Comentario", title: "Comentario", className: "dt-nowrap contenido-elipsis", render: function (data, type) {
                    if (type == "display" && data != null && data != undefined) {
                        return '<span class="pop-over-dt" data-toggle="popover" data-content="' + data + '">' + data + '</span>';
                    }

                    return null;
                }
            },
            //{
            //    data: "GeneraRecordatorio", title: "Generar recordatorio",
            //    //"className": "dt-center",
            //    render: function (data, type, row, meta) {
            //        var controles = null;
            //        if ((data && !row.EstatusPromesa) || (data && row.EstatusPromesa != null && row.EstatusPromesa == 1)) {
            //            if (type === 'display') {
            //                controles = '<button type="button" class="btn btn-info" onclick="generarRecordatorio(' + row.IdGestion + ', ' + row.idTipoGestion + ')" ><span class="fa fa-exclamation-circle"></span></button>';
            //                if (row.FechaUltimaNotificacion && row.FechaProgUltNotificacion) {
            //                    let fechaNotificacion = moment(row.FechaUltimaNotificacion).locale('es').format('LLLL');
            //                    let fechaEnvio = moment(row.FechaProgUltNotificacion).locale('es').format('LLLL');
            //                    controles += '<span class="tooltip-dt" data-toggle="tooltip" title="Ultimo recordatorio registrado: ' + fechaNotificacion + ' para enviarse: ' + fechaEnvio + '"><span class="glyphicon glyphicon-bell icono-notif"></span></span>'
            //                }
            //                return controles;
            //            }
            //        }
            //        return null;
            //    }
            //},
            {
                data: "NombreGestor", title: "Ultima gestión", render: function (data, type) {
                    if (type == "display") {
                        if (data != null && data != undefined)
                            return data;
                        else
                            return '<i>No Disponible</i>';
                    }

                    return null;
                }
            }
        ],
        columnDefs: [],
        drawCallback: function () {
            $(".pop-over-dt").popover({
                title: "Comentario",
                placement: "left",
                trigger: "focus",
                animation: true,
                container: "body"
            });
            $(".tooltip-dt").tooltip({ container: "body" });
        },
        //createdRow: function (row, data, index) {},
        language: español,
        autoWidth: true,
        order: [[0, "asc"]]
    });
    table.columns.adjust().draw();

    table.on('click', function (e) {
        if ($('.pop-over-dt').length > 1)
            $('.pop-over-dt').popover('hide');
        $(e.target).popover('toggle');

    });

    $("div.toolbar").html(
        '<div class="col-md-6">' +
        '<button id="btnIngresarGestion" class="btn btn-info" type="button">Ingresar Gestion</button>' +
        '</div>'
    );
    $("#btnIngresarGestion").bind("click", onClickBtnIngresarGestion);
    $("#selTipoGestion_AltaGestion").bind("change", onChangeSelTipoGestionAlta);
}
function selContacto(o) {
    let checks = $("#tContactos input[type=checkbox]:checked");
    if (checks.length > 1) {
        checks.map(function (i, d) {
            if ($(d).val() != $(o).val()) {
                $(d).prop("checked", !$(o).prop("checked"));
            }
        });
    }
    $("#opLlamar").prop("disabled", !$(o).prop("checked"));
}
function horasValidas(e) {
    $('#txHoraAgendaSel').val("");
    if (configAgenda.HoraMinAgenda != null && configAgenda.HoraMaxAgenda != null) {
        let fsel = moment($('#txFechaAgendaSel').val(), "DD-MM-YYYY");
        let factual = moment();
        let horaMin = moment();
        if (fsel.format("YYYY-MM-DD") > factual.format("YYYY-MM-DD")
            || factual.format("HH:mm") <= moment(configAgenda.HoraMinAgenda).format("HH:mm")) horaMin = moment(configAgenda.HoraMinAgenda).format("HH:mm");
        else horaMin = factual.format("HH:mm");

        $('#txHoraAgendaSel').data("DateTimePicker").minDate(horaMin);
    }
}
function horasValidasGestion(e) {
    let fsel = moment($('#txFecha_gestion').val(), "DD-MM-YYYY");
    let factual = moment();
    let horaMin = moment();
    if (fsel.format("YYYY-MM-DD") <= factual.format("YYYY-MM-DD")) {
        $('#txHora_gestion').val("");
        horaMin = factual.add(5, 'minutes').format("HH:mm");
        $('#txHora_gestion').data("DateTimePicker").minDate(horaMin);
    } else {
        $('#txHora_gestion').data("DateTimePicker").minDate("00:00")
    }
}
function guardarAgenda() {
    if (!validaAgenda()) {
        $("#modalFechaAgenda").modal("hide");
    }
}
function validaAgenda() {
    $("#txFechaAgenda").val("");
    $("#textObservacion").val("");
    $("#modalFechaAgenda .has-error").map((i, o) => {
        $(o).removeClass("has-error");
    });
    if ($("#txFechaAgendaSel").val() == "") $("#txFechaAgendaSel").parent().addClass("has-error");
    if ($("#txHoraAgendaSel").val() == "") $("#txHoraAgendaSel").parent().addClass("has-error");
    if ($("#txCometnarioAg").val() == "") $("#txCometnarioAg").parent().addClass("has-error");
    let errores = $("#modalFechaAgenda .has-error").length > 0;
    if (!errores) {
        $("#textObservacion").val($("#txCometnarioAg").val());
        $("#txFechaAgenda").val(`${$("#txFechaAgendaSel").val()} ${$("#txHoraAgendaSel").val()}`);
    }
    return errores;
}
function gestoresAsignados() {
    $("#selGestores").empty();
    if (gestoresA != undefined && gestoresA != null) {
        gestoresA.forEach((o, i) => {
            if (configAgenda.Gestores != null && configAgenda.Gestores.length > 0 && configAgenda.Gestores.includes(o.IdGestor.toString())) return;
            $("#selGestores").append($("<option>", { value: o.IdGestor, text: o.Nombre }));
        });
    }
    $("#selGestores").val("0");
}
function gestorNotificar(idGestor, seleccionado) {
    if (configAgenda.Gestores == undefined || configAgenda.Gestores == null) configAgenda.Gestores = [];
    if (idGestor <= 0) return;
    let posicion = -1;
    configAgenda.Gestores.map((o, i) => {
        if (o == idGestor) {
            posicion = i;
            return;
        }
    });
    if (seleccionado && posicion < 0) configAgenda.Gestores.push(idGestor);
    else if (!seleccionado && configAgenda.Gestores.length > 0 && posicion >= 0) configAgenda.Gestores.splice(posicion, 1);
    gestoresAsignados();
    gestoresNotificar();
}
function gestoresNotificar() {
    $("#gestoreNotificar").empty();
    if (configAgenda.Gestores != null && configAgenda.Gestores.length > 0
        && gestoresA != null && gestoresA.length > 0) {
        configAgenda.Gestores.forEach((o, i) => {
            for (let i = 0; i < gestoresA.length; i++) {
                if (gestoresA[i].IdGestor.toString() == o) {
                    let gest = `<li class="list-group-item">
                        <span class="glyphicon glyphicon-user"></span>&nbsp;${gestoresA[i].Nombre}
                        <i class="remover glyphicon glyphicon-remove" onclick="gestorNotificar(${o}, false);"></i>
                        </li > `;
                    $("#gestoreNotificar").append(gest);
                    return;
                }
            }
        });
    }
}
function diasValidosLiquidacion() {
    let fsel = moment($('#txFechaLiquidacion_Liquidacion').val(), "DD-MM-YYYY");
    let factual = moment();
    let fechaMax = moment().add(30, 'day');
    if (fsel.format("YYYY-MM-DD") > fechaMax.format("YYYY-MM-DD"))
        $('#txFechaLiquidacion_Liquidacion').val(fechaMax.format("DD/MM/YYYY"));
    else if (fsel.format("YYYY-MM-DD") < factual.format("YYYY-MM-DD"))
        $('#txFechaLiquidacion_Liquidacion').val(factual.format("DD/MM/YYYY"));
    ObtenerLiquidacion(fsel.format('DD-MM-YYYY'));
}
function onClickBtLiquidar() {
    $('#btLiquidar').prop("disabled", true);
    var fecha = $('#txFechaLiquidacion_Liquidacion').val();
    var mFecha = moment(fecha, 'DD/MM/YYYY');
    var mFechaStr = mFecha.format('YYYY-MM-DD');
    let strFechaLiquidacion = "&fechaLiquidacion=" + mFechaStr + "&esGestion=1";
    window.location.replace("../../Site/Creditos/lstDashboardCreditos.aspx?idSolicitud=" + idSolicitud + strFechaLiquidacion);
    $("body").addClass("loading"); $(".modal-pr").appendTo("body");
}
function generarRecordatorio(idGestion, idTipoGestion) {
    notifGestion = {};
    if (!idGestion) { alert("La gestion no es válida"); return; }
    if (!idTipoGestion) { alert("La descripción de la gestión no es válida"); return; }
    if (tiposNotPorTipoGestion == undefined || tiposNotPorTipoGestion == null) { alert("Ocurrio un error al cargar la configuracion del tipo de recordatorio"); return; }

    var tipo = tiposNotPorTipoGestion.find(tn => tn.IdTipoGestion == idTipoGestion);
    if (tipo == undefined || tipo == null || isEmpty(tipo)) { alert("No es posible generar un recordatorio para este tipo de gestion."); return; }

    notifGestion = {
        IdGestion: idGestion,
        IdTipoGestion: idTipoGestion,
        IdTipoNotificacion: tipo.IdTipoNotificacion
    };

    limpiarCamposRecordatorio();
    var modalNuevaGestion = $("#modalRecordatorio");
    modalNuevaGestion.appendTo("body");
    modalNuevaGestion.modal("show");
}
function configHoraRecordatorio(e) {
    var txtHora = $('#txtHora_recordatorio');
    txtHora.val("");
    txtHora.datetimepicker({ locale: 'es', format: 'hh:mm a' });
    let fechaRecordatorio = moment($("#txtFecha_recordatorio").val(), "DD-MM-YYYY");
    let hoy = moment();

    if (fechaRecordatorio.format("YYYY-MM-DD") > hoy.format("YYYY-MM-DD")) {
        txtHora.data("DateTimePicker").minDate(moment(fechaRecordatorio._d.setHours(0, 0, 0, 0)).format("HH:mm"));
    } else {
        txtHora.data("DateTimePicker").minDate(hoy.format("HH:mm"));
    }

}
function cronogramaPagos() {
    let id = Date.now()
    let tabla = `
    <div id=${id} class="modal fade" role="dialog" style="z-index:99999">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="alertdialog" style="width:82%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cronograma de pagos</h4>
                </div>
                <div class="modal-body">
                    <iframe src="../Creditos/frmTablaAmortizacion.aspx?idSolicitud=${idSolicitud}" style="border:none;width:100%;height:500px;overflow-x: hidden; overflow-y: scroll">
                    </iframe>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cerrar" />
                </div>
            </div>
        </div>
    </div>`;
    $(tabla).modal('show')
        .on("shown.bs.modal", function (e) {
        })
        .on("hidden.bs.modal", function (e) {
            $(`#${id}`).remove();
        });
}