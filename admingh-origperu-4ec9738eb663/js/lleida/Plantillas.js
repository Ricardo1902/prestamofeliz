﻿$('body').bind('copy paste', function (e) {
    //e.preventDefault(); return false;
});

$(document).on({
    ajaxStart: function () { $("#divLoader").removeClass("hidden"); },
    ajaxStop: function () { $("#divLoader").addClass("hidden"); }
});

const dataTableEspanol = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registro(s)",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registro(s) del _START_ al _END_ de un total de _TOTAL_ registro(s)",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Dar clic para ordenar la columna de manera ascendente",
        "sSortDescending": ": Dar clic para ordenar la columna de manera descendente"
    }
};

var plantillas = [];

$(document).ready(function () {
    CargarPlantillas();
    $("#btnRecargar").bind("click", CargarPlantillas);
});

function CargarPlantillas() {
    let parametros = {
        url: "Plantillas.aspx/CargarPlantillas",
        contentType: "application/json",
        dataType: "json",
        method: 'GET'
    };
    $.ajax(parametros)
        .done(function (data) {
            let resp = data.d;
            if (resp !== null) {
                plantillas = resp;
                MostrarPlantillas();
            } else {
                alert("No fue posible obtener las plantillas.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de obtener las plantillas.");
        });
}

function asignarPlantilla(plantilla) {
    let parametros = {
        url: "Plantillas.aspx/AsignarPlantilla",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ plantilla: plantilla })
    };
    $.ajax(parametros)
        .done(function (data) {
            let resp = data.d;
            if (resp !== null) {
                if (resp.Error) {
                    alert(resp.MensajeOperacion);
                }
                else {
                    CargarPlantillas();
                }
            } else {
                alert("No fue posible asignar la plantilla.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de asignar la plantilla.");
        });
}

function activarPlantilla(plantilla) {
    let parametros = {
        url: "Plantillas.aspx/ActivarPlantilla",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ plantilla: plantilla })
    };
    $.ajax(parametros)
        .done(function (data) {
            let resp = data.d;
            if (resp !== null) {
                if (resp.Error) {
                    alert(resp.MensajeOperacion);
                }
                else {
                    CargarPlantillas();
                }
            } else {
                alert("No fue posible activar la plantilla.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de activar la plantilla.");
        });
}

function desactivarPlantilla(plantilla) {
    let parametros = {
        url: "Plantillas.aspx/DesactivarPlantilla",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({ plantilla: plantilla })
    };
    $.ajax(parametros)
        .done(function (data) {
            let resp = data.d;
            if (resp !== null) {
                if (resp.Error) {
                    alert(resp.MensajeOperacion);
                }
                else {
                    CargarPlantillas();
                }
            } else {
                alert("No fue posible desactivar la plantilla.");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error al momento de desactivar la plantilla.");
        });
}

function MostrarPlantillas() {
    $("#tbPlantillas").dataTable({
        responsive: true,
        destroy: true,
        data: plantillas,
        scrollX: true,
        columns: [
            { title: 'IdPlantilla', data: 'IdPlantilla' },
            { title: 'Nombre', data: 'Nombre' },
            { title: 'Estatus', data: 'Estatus' },
            { title: 'Activo', data: 'Activo' },
            {
                mRender: function (data, type, row) {
                    if (row.Activo)
                        return '<a class="btn btn-success btn-sm">ASIGNADA</a>';
                    else if (row.Estatus == 'disabled')
                        return '<a class="btn btn-default btn-sm">DESACTIVADO</a>';
                    else
                        return '<a class="btn btn-info btn-sm" onclick="asignarPlantilla(' + row.IdPlantilla + ')" data-id="' + row.IdPlantilla + '">ASIGNAR</a>';
                }
            },
            {
                mRender: function (data, type, row) {
                    if (!row.Activo) {
                        if (row.Estatus == "disabled")
                            return '<a class="btn btn-primary btn-sm" onclick="activarPlantilla(' + row.IdPlantilla + ')" data-id="' + row.IdPlantilla + '">ACTIVAR</a>';
                        else
                            return '<a class="btn btn-danger btn-sm" onclick="desactivarPlantilla(' + row.IdPlantilla + ')" data-id="' + row.IdPlantilla + '">DESACTIVAR</a>';
                    }
                    else {
                        return '<a class="btn btn-default btn-sm">EN USO</a>';
                    }
                }
            }
        ],
        columnDefs: [
            { "orderable": false, "targets": 0 },
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 2, "visible": false },
            { "orderable": false, "targets": 3, "visible": false },
            { "orderable": false, "targets": 4 }
        ],
        "order": [[1, "asc"]],
        language: dataTableEspanol
    });
}