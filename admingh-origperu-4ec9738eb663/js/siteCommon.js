﻿function EvaluateText(cadena, obj) {
    opc = false;
    if (cadena == "%d")
        if (event.keyCode > 47 && event.keyCode < 58)
            opc = true;
    if (cadena == "%f") {
        if (event.keyCode > 47 && event.keyCode < 58)
            opc = true;
        if (obj.value.search("[.*]") == -1 && obj.value.length != 0)
            if (event.keyCode == 46)
                opc = true;
    }
    if (opc == false)
        event.returnValue = false;
}

function tabSetActive(t) {    
    var ul = $(t).parent().parent();

    $(ul).find('li').removeClass("active");
    $(t).parent().addClass("active");       
}

function formatCurrency(total) {
    var neg = false;
    if (total < 0) {
        neg = true;
        total = Math.abs(total);
    }
    return (neg ? "-S/" : 'S/') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
}

function printPF() {    
    window.print();
}
function activarTooltips()
{
    // Activar Tooltips    
    $('[data-toggle="tooltip"]').tooltip()   
}