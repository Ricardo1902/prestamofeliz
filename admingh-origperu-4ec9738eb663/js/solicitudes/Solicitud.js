﻿$(document).ready(function () {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function generar_descarga(IdSolicitud) {   
    let parametros = {
        url: "lstDashboardSolicitudes.aspx/DescargarDoc",
        contentType: "application/json",
        dataType: "json",
        method: 'POST',
        data: JSON.stringify({
            idSolicitud: IdSolicitud
        })
    };
    $.ajax(parametros)
        .done(function (data) {
            if (data.d) {
                let resp = data.d;
                if (!resp.Error) {
                    var byteCharacters = atob(resp.Contenido);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);
                    var blob = new Blob([byteArray], { type: resp.Tipo });
                    //var blob = new Blob([resp.Contenido], { type: 'application/pdf' });
                    let datos = window.URL.createObjectURL(blob);
                    let oA = document.createElement('a');
                    oA.href = datos;
                    oA.download = resp.NombreArchivoNuevo;
                   // document.body.appendChild(a);
                    oA.click();
                    setTimeout(function () {
                        window.URL.revokeObjectURL(datos);
                    }, 100);                    
                }
                else {
                    alert(resp.Mensaje);
                }
            } else {
                Swal.fire("Mensaje", "No fue posible obtener los datos.", "warning");
            }
        })
        .fail(function (err) {
            alert("Ocurrió un error.");
        });
}

var prm = Sys.WebForms.PageRequestManager.getInstance();
prm.add_endRequest(function () {
    // Activar Tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
});