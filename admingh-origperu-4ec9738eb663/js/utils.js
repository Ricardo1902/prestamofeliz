﻿Array.prototype.indexOfAttribute = function (attr, value) {
    for (var i = 0; i < this.length; i += 1) {
        if (this[i][attr] === value) {
            return i;
        }
    }
    return -1;
};
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) return false;
    }
    return true;
}
function esFechaValida(d) {
    return d instanceof Date && !isNaN(d);
}
function poblarControlSeleccion(
    control,
    arregloValores,
    textoMostrar,
    valorOpcion
) {
    try {
        let objeto = $(control);
        if (
            objeto !== null &&
            arregloValores !== null &&
            arregloValores.length > 0
        ) {
            $(objeto).empty();
            if (!arregloValores.find(objeto => objeto[valorOpcion] == 0))
                $(objeto).append(
                    $("<option>", { value: 0 }).text("Seleccione una opcion")
                );
            arregloValores.forEach(function (opcion) {
                $(objeto).append(
                    $("<option>", { value: opcion[valorOpcion] }).text(
                        opcion[textoMostrar]
                    )
                );
            });
        }
    } catch (e) {
        console.log(e);
    }
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
function getDateFromAspNetFormat(date) {
    const re = /^\/Date\((-?[\+\d]+)\)\/$/;
    const m = re.exec(date);
    if (m && m.length > 1) {
        return parseInt(m[1]);
    }
    return null;
}
function LastDayOfMonth(Year, Month) {
    return (new Date((new Date(Year, Month + 1, 1)) - 1)).getDate();
}